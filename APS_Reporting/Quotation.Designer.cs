namespace APS_Reporting
{
    partial class Quotation
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Quotation));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.picgb1 = new Telerik.Reporting.PictureBox();
            this.picbg2 = new Telerik.Reporting.PictureBox();
            this.txtCustomerName = new Telerik.Reporting.TextBox();
            this.txtAddress1 = new Telerik.Reporting.TextBox();
            this.txtAddress2 = new Telerik.Reporting.TextBox();
            this.txtMobile = new Telerik.Reporting.TextBox();
            this.txtPhone = new Telerik.Reporting.TextBox();
            this.txtEmail = new Telerik.Reporting.TextBox();
            this.txtQuoteNo = new Telerik.Reporting.TextBox();
            this.txtIssueDate = new Telerik.Reporting.TextBox();
            this.txtSolarAdvisor = new Telerik.Reporting.TextBox();
            this.txtSalesRepPhone = new Telerik.Reporting.TextBox();
            this.txtSalesRepEmail = new Telerik.Reporting.TextBox();
            this.txtSystemSize = new Telerik.Reporting.TextBox();
            this.txtPanels = new Telerik.Reporting.TextBox();
            this.txtPanelName = new Telerik.Reporting.TextBox();
            this.txtInverter = new Telerik.Reporting.TextBox();
            this.txtInverterName = new Telerik.Reporting.TextBox();
            this.txtSubsidy40 = new Telerik.Reporting.TextBox();
            this.txtSubsidy20 = new Telerik.Reporting.TextBox();
            this.txtExtraCost = new Telerik.Reporting.TextBox();
            this.txtOtherCost = new Telerik.Reporting.TextBox();
            this.txtTotalPaybleAmount = new Telerik.Reporting.TextBox();
            this.txtDeposit = new Telerik.Reporting.TextBox();
            this.txtDiscom = new Telerik.Reporting.TextBox();
            this.txtDivision = new Telerik.Reporting.TextBox();
            this.txtStuctureHeight = new Telerik.Reporting.TextBox();
            this.txtQuoteNotes = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(594D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.picgb1,
            this.picbg2,
            this.txtCustomerName,
            this.txtAddress1,
            this.txtAddress2,
            this.txtMobile,
            this.txtPhone,
            this.txtEmail,
            this.txtQuoteNo,
            this.txtIssueDate,
            this.txtSolarAdvisor,
            this.txtSalesRepPhone,
            this.txtSalesRepEmail,
            this.txtSystemSize,
            this.txtPanels,
            this.txtPanelName,
            this.txtInverter,
            this.txtInverterName,
            this.txtSubsidy40,
            this.txtSubsidy20,
            this.txtExtraCost,
            this.txtOtherCost,
            this.txtTotalPaybleAmount,
            this.txtDeposit,
            this.txtDiscom,
            this.txtDivision,
            this.txtStuctureHeight,
            this.txtQuoteNotes});
            this.detail.Name = "detail";
            // 
            // picgb1
            // 
            this.picgb1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.026D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.picgb1.MimeType = "image/jpeg";
            this.picgb1.Name = "picgb1";
            this.picgb1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picgb1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picgb1.Value = ((object)(resources.GetObject("picgb1.Value")));
            // 
            // picbg2
            // 
            this.picbg2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.picbg2.MimeType = "image/jpeg";
            this.picbg2.Name = "picbg2";
            this.picbg2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picbg2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picbg2.Value = ((object)(resources.GetObject("picbg2.Value")));
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.092D), Telerik.Reporting.Drawing.Unit.Cm(4.426D));
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.733D), Telerik.Reporting.Drawing.Unit.Cm(0.666D));
            this.txtCustomerName.Style.Font.Name = "Verdana";
            this.txtCustomerName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtCustomerName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustomerName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtCustomerName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomerName.Value = "";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.092D), Telerik.Reporting.Drawing.Unit.Cm(5.359D));
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.733D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress1.Style.Font.Name = "Verdana";
            this.txtAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtAddress1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtAddress1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress1.Value = "";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.092D), Telerik.Reporting.Drawing.Unit.Cm(5.759D));
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.733D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress2.Style.Font.Name = "Verdana";
            this.txtAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtAddress2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtAddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress2.Value = "";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.092D), Telerik.Reporting.Drawing.Unit.Cm(6.826D));
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.867D), Telerik.Reporting.Drawing.Unit.Cm(0.733D));
            this.txtMobile.Style.Font.Name = "Verdana";
            this.txtMobile.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtMobile.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtMobile.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtMobile.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMobile.Value = "";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.959D), Telerik.Reporting.Drawing.Unit.Cm(6.826D));
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.867D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtPhone.Style.Font.Name = "Verdana";
            this.txtPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtPhone.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPhone.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPhone.Value = "";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.092D), Telerik.Reporting.Drawing.Unit.Cm(7.626D));
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.733D), Telerik.Reporting.Drawing.Unit.Cm(0.666D));
            this.txtEmail.Style.Font.Name = "Verdana";
            this.txtEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtEmail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtEmail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtEmail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtEmail.Value = "";
            // 
            // txtQuoteNo
            // 
            this.txtQuoteNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.959D), Telerik.Reporting.Drawing.Unit.Cm(3.826D));
            this.txtQuoteNo.Name = "txtQuoteNo";
            this.txtQuoteNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.933D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtQuoteNo.Style.Font.Name = "Verdana";
            this.txtQuoteNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtQuoteNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtQuoteNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtQuoteNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtQuoteNo.Value = "";
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.959D), Telerik.Reporting.Drawing.Unit.Cm(4.626D));
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.933D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtIssueDate.Style.Font.Name = "Verdana";
            this.txtIssueDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtIssueDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtIssueDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtIssueDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtIssueDate.Value = "";
            // 
            // txtSolarAdvisor
            // 
            this.txtSolarAdvisor.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.959D), Telerik.Reporting.Drawing.Unit.Cm(5.492D));
            this.txtSolarAdvisor.Name = "txtSolarAdvisor";
            this.txtSolarAdvisor.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.933D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtSolarAdvisor.Style.Font.Name = "Verdana";
            this.txtSolarAdvisor.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtSolarAdvisor.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtSolarAdvisor.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSolarAdvisor.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSolarAdvisor.Value = "";
            // 
            // txtSalesRepPhone
            // 
            this.txtSalesRepPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.959D), Telerik.Reporting.Drawing.Unit.Cm(6.359D));
            this.txtSalesRepPhone.Name = "txtSalesRepPhone";
            this.txtSalesRepPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.933D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtSalesRepPhone.Style.Font.Name = "Verdana";
            this.txtSalesRepPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtSalesRepPhone.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtSalesRepPhone.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSalesRepPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSalesRepPhone.Value = "";
            // 
            // txtSalesRepEmail
            // 
            this.txtSalesRepEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.959D), Telerik.Reporting.Drawing.Unit.Cm(7.226D));
            this.txtSalesRepEmail.Name = "txtSalesRepEmail";
            this.txtSalesRepEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.933D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtSalesRepEmail.Style.Font.Name = "Verdana";
            this.txtSalesRepEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtSalesRepEmail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtSalesRepEmail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSalesRepEmail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSalesRepEmail.Value = "";
            // 
            // txtSystemSize
            // 
            this.txtSystemSize.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.959D), Telerik.Reporting.Drawing.Unit.Cm(8.559D));
            this.txtSystemSize.Name = "txtSystemSize";
            this.txtSystemSize.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.333D), Telerik.Reporting.Drawing.Unit.Cm(0.733D));
            this.txtSystemSize.Style.Color = System.Drawing.Color.White;
            this.txtSystemSize.Style.Font.Bold = true;
            this.txtSystemSize.Style.Font.Name = "Verdana";
            this.txtSystemSize.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtSystemSize.Style.LineColor = System.Drawing.Color.Black;
            this.txtSystemSize.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSystemSize.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtSystemSize.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSystemSize.Value = "";
            // 
            // txtPanels
            // 
            this.txtPanels.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(9.292D));
            this.txtPanels.Name = "txtPanels";
            this.txtPanels.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtPanels.Style.Font.Name = "Verdana";
            this.txtPanels.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanels.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanels.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanels.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanels.Value = "";
            // 
            // txtPanelName
            // 
            this.txtPanelName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.626D), Telerik.Reporting.Drawing.Unit.Cm(9.292D));
            this.txtPanelName.Name = "txtPanelName";
            this.txtPanelName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.4D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtPanelName.Style.Font.Name = "Verdana";
            this.txtPanelName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanelName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanelName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelName.Value = "";
            // 
            // txtInverter
            // 
            this.txtInverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(10.159D));
            this.txtInverter.Name = "txtInverter";
            this.txtInverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtInverter.Style.Font.Name = "Verdana";
            this.txtInverter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverter.Value = "";
            // 
            // txtInverterName
            // 
            this.txtInverterName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.626D), Telerik.Reporting.Drawing.Unit.Cm(10.159D));
            this.txtInverterName.Name = "txtInverterName";
            this.txtInverterName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.4D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtInverterName.Style.Font.Name = "Verdana";
            this.txtInverterName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverterName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterName.Value = "";
            // 
            // txtSubsidy40
            // 
            this.txtSubsidy40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(11.826D));
            this.txtSubsidy40.Name = "txtSubsidy40";
            this.txtSubsidy40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtSubsidy40.Style.Font.Name = "Verdana";
            this.txtSubsidy40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtSubsidy40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSubsidy40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSubsidy40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSubsidy40.Value = "";
            // 
            // txtSubsidy20
            // 
            this.txtSubsidy20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(12.626D));
            this.txtSubsidy20.Name = "txtSubsidy20";
            this.txtSubsidy20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtSubsidy20.Style.Font.Name = "Verdana";
            this.txtSubsidy20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtSubsidy20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSubsidy20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSubsidy20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSubsidy20.Value = "";
            // 
            // txtExtraCost
            // 
            this.txtExtraCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(13.492D));
            this.txtExtraCost.Name = "txtExtraCost";
            this.txtExtraCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtExtraCost.Style.Font.Name = "Verdana";
            this.txtExtraCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtExtraCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtExtraCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtExtraCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtExtraCost.Value = "";
            // 
            // txtOtherCost
            // 
            this.txtOtherCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.826D), Telerik.Reporting.Drawing.Unit.Cm(14.159D));
            this.txtOtherCost.Name = "txtOtherCost";
            this.txtOtherCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtOtherCost.Style.Font.Name = "Verdana";
            this.txtOtherCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtOtherCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtOtherCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtOtherCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtOtherCost.Value = "";
            // 
            // txtTotalPaybleAmount
            // 
            this.txtTotalPaybleAmount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.092D), Telerik.Reporting.Drawing.Unit.Cm(14.826D));
            this.txtTotalPaybleAmount.Name = "txtTotalPaybleAmount";
            this.txtTotalPaybleAmount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtTotalPaybleAmount.Style.Font.Bold = true;
            this.txtTotalPaybleAmount.Style.Font.Name = "Verdana";
            this.txtTotalPaybleAmount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtTotalPaybleAmount.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalPaybleAmount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtTotalPaybleAmount.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalPaybleAmount.Value = "";
            // 
            // txtDeposit
            // 
            this.txtDeposit.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.092D), Telerik.Reporting.Drawing.Unit.Cm(15.559D));
            this.txtDeposit.Name = "txtDeposit";
            this.txtDeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtDeposit.Style.Font.Name = "Verdana";
            this.txtDeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtDeposit.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtDeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDeposit.Value = "";
            // 
            // txtDiscom
            // 
            this.txtDiscom.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.559D), Telerik.Reporting.Drawing.Unit.Cm(19.826D));
            this.txtDiscom.Name = "txtDiscom";
            this.txtDiscom.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.933D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtDiscom.Style.Font.Name = "Verdana";
            this.txtDiscom.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtDiscom.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDiscom.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.txtDiscom.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtDiscom.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscom.Value = "";
            // 
            // txtDivision
            // 
            this.txtDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.559D), Telerik.Reporting.Drawing.Unit.Cm(20.759D));
            this.txtDivision.Name = "txtDivision";
            this.txtDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.933D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtDivision.Style.Font.Name = "Verdana";
            this.txtDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDivision.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDivision.Value = "";
            // 
            // txtStuctureHeight
            // 
            this.txtStuctureHeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.559D), Telerik.Reporting.Drawing.Unit.Cm(21.626D));
            this.txtStuctureHeight.Name = "txtStuctureHeight";
            this.txtStuctureHeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.933D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtStuctureHeight.Style.Font.Name = "Verdana";
            this.txtStuctureHeight.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtStuctureHeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtStuctureHeight.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2.5D);
            this.txtStuctureHeight.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtStuctureHeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtStuctureHeight.Value = "";
            // 
            // txtQuoteNotes
            // 
            this.txtQuoteNotes.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.426D), Telerik.Reporting.Drawing.Unit.Cm(19.826D));
            this.txtQuoteNotes.Name = "txtQuoteNotes";
            this.txtQuoteNotes.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.933D), Telerik.Reporting.Drawing.Unit.Cm(2.667D));
            this.txtQuoteNotes.Style.Font.Name = "Verdana";
            this.txtQuoteNotes.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtQuoteNotes.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtQuoteNotes.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(6.4D);
            this.txtQuoteNotes.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtQuoteNotes.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtQuoteNotes.Value = "";
            // 
            // Quotation
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Quotation";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox picgb1;
        private Telerik.Reporting.PictureBox picbg2;
        private Telerik.Reporting.TextBox txtCustomerName;
        private Telerik.Reporting.TextBox txtAddress1;
        private Telerik.Reporting.TextBox txtAddress2;
        private Telerik.Reporting.TextBox txtMobile;
        private Telerik.Reporting.TextBox txtPhone;
        private Telerik.Reporting.TextBox txtEmail;
        private Telerik.Reporting.TextBox txtQuoteNo;
        private Telerik.Reporting.TextBox txtIssueDate;
        private Telerik.Reporting.TextBox txtSolarAdvisor;
        private Telerik.Reporting.TextBox txtSalesRepPhone;
        private Telerik.Reporting.TextBox txtSalesRepEmail;
        private Telerik.Reporting.TextBox txtSystemSize;
        private Telerik.Reporting.TextBox txtPanels;
        private Telerik.Reporting.TextBox txtPanelName;
        private Telerik.Reporting.TextBox txtInverter;
        private Telerik.Reporting.TextBox txtInverterName;
        private Telerik.Reporting.TextBox txtSubsidy40;
        private Telerik.Reporting.TextBox txtSubsidy20;
        private Telerik.Reporting.TextBox txtExtraCost;
        private Telerik.Reporting.TextBox txtOtherCost;
        private Telerik.Reporting.TextBox txtTotalPaybleAmount;
        private Telerik.Reporting.TextBox txtDeposit;
        private Telerik.Reporting.TextBox txtDiscom;
        private Telerik.Reporting.TextBox txtDivision;
        private Telerik.Reporting.TextBox txtStuctureHeight;
        private Telerik.Reporting.TextBox txtQuoteNotes;
    }
}