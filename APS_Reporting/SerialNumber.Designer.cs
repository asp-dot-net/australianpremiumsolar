namespace APS_Reporting
{
    partial class SerialNumber
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SerialNumber));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.txtApplicationNo = new Telerik.Reporting.TextBox();
            this.tblPanels = new Telerik.Reporting.Table();
            this.Panels1 = new Telerik.Reporting.TextBox();
            this.tblInverter = new Telerik.Reporting.Table();
            this.Inverter1 = new Telerik.Reporting.TextBox();
            this.txtProjectNo = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.txtCustName,
            this.txtApplicationNo,
            this.tblPanels,
            this.tblInverter,
            this.txtProjectNo});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(4.7D));
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.txtCustName.Style.Font.Name = "Verdana";
            this.txtCustName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtCustName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustName.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtCustName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtCustName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustName.Value = "";
            // 
            // txtApplicationNo
            // 
            this.txtApplicationNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(5.5D));
            this.txtApplicationNo.Name = "txtApplicationNo";
            this.txtApplicationNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.9D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.txtApplicationNo.Style.Font.Name = "Verdana";
            this.txtApplicationNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtApplicationNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtApplicationNo.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtApplicationNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtApplicationNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtApplicationNo.Value = "";
            // 
            // tblPanels
            // 
            this.tblPanels.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.3D)));
            this.tblPanels.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7D)));
            this.tblPanels.Body.SetCellContent(0, 0, this.Panels1);
            tableGroup1.Name = "tableGroup";
            this.tblPanels.ColumnGroups.Add(tableGroup1);
            this.tblPanels.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Panels1});
            this.tblPanels.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(9.3D));
            this.tblPanels.Name = "tblPanels";
            tableGroup3.Name = "group1";
            tableGroup2.ChildGroups.Add(tableGroup3);
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup";
            this.tblPanels.RowGroups.Add(tableGroup2);
            this.tblPanels.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.tblPanels.Style.Font.Name = "Verdana";
            this.tblPanels.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.tblPanels.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // Panels1
            // 
            this.Panels1.Name = "Panels1";
            this.Panels1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.Panels1.Style.Font.Name = "Verdana";
            this.Panels1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.Panels1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.Panels1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.Panels1.StyleName = "";
            this.Panels1.Value = "=Fields.Panels";
            // 
            // tblInverter
            // 
            this.tblInverter.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.6D)));
            this.tblInverter.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7D)));
            this.tblInverter.Body.SetCellContent(0, 0, this.Inverter1);
            tableGroup4.Name = "tableGroup";
            this.tblInverter.ColumnGroups.Add(tableGroup4);
            this.tblInverter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Inverter1});
            this.tblInverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.5D), Telerik.Reporting.Drawing.Unit.Cm(9.3D));
            this.tblInverter.Name = "tblInverter";
            tableGroup6.Name = "group1";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup";
            this.tblInverter.RowGroups.Add(tableGroup5);
            this.tblInverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.tblInverter.Style.Font.Name = "Verdana";
            this.tblInverter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.tblInverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // Inverter1
            // 
            this.Inverter1.Name = "Inverter1";
            this.Inverter1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.Inverter1.Style.Font.Name = "Verdana";
            this.Inverter1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.Inverter1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.Inverter1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.Inverter1.StyleName = "";
            this.Inverter1.Value = "=Fields.Inverter";
            // 
            // txtProjectNo
            // 
            this.txtProjectNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(4D));
            this.txtProjectNo.Name = "txtProjectNo";
            this.txtProjectNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtProjectNo.Style.Font.Name = "Verdana";
            this.txtProjectNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtProjectNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtProjectNo.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtProjectNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtProjectNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNo.Value = "";
            // 
            // SerialNumber
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SerialNumber";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox txtCustName;
        private Telerik.Reporting.TextBox txtApplicationNo;
        private Telerik.Reporting.Table tblPanels;
        private Telerik.Reporting.TextBox Panels1;
        private Telerik.Reporting.Table tblInverter;
        private Telerik.Reporting.TextBox Inverter1;
        private Telerik.Reporting.TextBox txtProjectNo;
    }
}