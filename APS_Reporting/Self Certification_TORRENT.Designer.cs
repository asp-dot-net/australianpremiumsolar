namespace APS_Reporting
{
	partial class Self_Certification_TORRENT
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Self_Certification_TORRENT));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.Pic1 = new Telerik.Reporting.PictureBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.Pic3 = new Telerik.Reporting.PictureBox();
            this.Pic4 = new Telerik.Reporting.PictureBox();
            this.txtConsumerNo = new Telerik.Reporting.TextBox();
            this.txtAddress2 = new Telerik.Reporting.TextBox();
            this.txtAddress1 = new Telerik.Reporting.TextBox();
            this.txtSubDivision = new Telerik.Reporting.TextBox();
            this.txtConsumerNoBSI = new Telerik.Reporting.TextBox();
            this.txtCustomer = new Telerik.Reporting.TextBox();
            this.txtGUVNLNo = new Telerik.Reporting.TextBox();
            this.txtPhase1 = new Telerik.Reporting.TextBox();
            this.txtKW1 = new Telerik.Reporting.TextBox();
            this.txtKV = new Telerik.Reporting.TextBox();
            this.txtLoad = new Telerik.Reporting.TextBox();
            this.txt1 = new Telerik.Reporting.TextBox();
            this.txtPanelName = new Telerik.Reporting.TextBox();
            this.txtTotalKW = new Telerik.Reporting.TextBox();
            this.txtNoPanel = new Telerik.Reporting.TextBox();
            this.txtStockSize = new Telerik.Reporting.TextBox();
            this.txtSeries = new Telerik.Reporting.TextBox();
            this.txtModelNo = new Telerik.Reporting.TextBox();
            this.txtSolarPV = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.txtInverterSerialNo = new Telerik.Reporting.TextBox();
            this.txtInverterKW = new Telerik.Reporting.TextBox();
            this.txtInverterPhase = new Telerik.Reporting.TextBox();
            this.txtInverterModelNo = new Telerik.Reporting.TextBox();
            this.txtInverterName = new Telerik.Reporting.TextBox();
            this.txtManufacturer = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage2 = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage3 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1188D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Pic1,
            this.pictureBox1,
            this.Pic3,
            this.Pic4,
            this.txtConsumerNo,
            this.txtGUVNLNo,
            this.txtCustomer,
            this.txtConsumerNoBSI,
            this.txtSubDivision,
            this.txtAddress1,
            this.txtAddress2,
            this.txtLoad,
            this.txtKV,
            this.txtKW1,
            this.txtPhase1,
            this.txt1,
            this.txtSolarPV,
            this.txtModelNo,
            this.txtSeries,
            this.txtStockSize,
            this.txtNoPanel,
            this.txtTotalKW,
            this.txtPanelName,
            this.textBox3,
            this.textBox5,
            this.txtInverterName,
            this.txtInverterModelNo,
            this.txtInverterPhase,
            this.txtInverterKW,
            this.txtInverterSerialNo,
            this.txtManufacturer,
            this.txtConsumerNoPage2,
            this.txtConsumerNoPage3});
            this.detail.Name = "detail";
            // 
            // Pic1
            // 
            this.Pic1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Pic1.MimeType = "image/jpeg";
            this.Pic1.Name = "Pic1";
            this.Pic1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic1.Value = ((object)(resources.GetObject("Pic1.Value")));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // Pic3
            // 
            this.Pic3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Mm(594D));
            this.Pic3.MimeType = "image/jpeg";
            this.Pic3.Name = "Pic3";
            this.Pic3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic3.Value = ((object)(resources.GetObject("Pic3.Value")));
            // 
            // Pic4
            // 
            this.Pic4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1D));
            this.Pic4.MimeType = "image/jpeg";
            this.Pic4.Name = "Pic4";
            this.Pic4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic4.Value = ((object)(resources.GetObject("Pic4.Value")));
            // 
            // txtConsumerNo
            // 
            this.txtConsumerNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(2.7D));
            this.txtConsumerNo.Name = "txtConsumerNo";
            this.txtConsumerNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.933D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNo.Style.Font.Name = "Verdana";
            this.txtConsumerNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNo.Value = "";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(8.4D));
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.533D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress2.Style.Font.Name = "Verdana";
            this.txtAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtAddress2.TextWrap = false;
            this.txtAddress2.Value = "";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(7.5D));
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.533D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress1.Style.Font.Name = "Verdana";
            this.txtAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtAddress1.TextWrap = false;
            this.txtAddress1.Value = "";
            // 
            // txtSubDivision
            // 
            this.txtSubDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.2D), Telerik.Reporting.Drawing.Unit.Cm(6.7D));
            this.txtSubDivision.Name = "txtSubDivision";
            this.txtSubDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtSubDivision.Style.Font.Name = "Verdana";
            this.txtSubDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSubDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSubDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtSubDivision.Value = "";
            // 
            // txtConsumerNoBSI
            // 
            this.txtConsumerNoBSI.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(6.7D));
            this.txtConsumerNoBSI.Name = "txtConsumerNoBSI";
            this.txtConsumerNoBSI.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.467D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoBSI.Style.Font.Name = "Verdana";
            this.txtConsumerNoBSI.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoBSI.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoBSI.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoBSI.Value = "";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(5.9D));
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.8D), Telerik.Reporting.Drawing.Unit.Cm(0.467D));
            this.txtCustomer.Style.Font.Name = "Verdana";
            this.txtCustomer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustomer.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtCustomer.Value = "";
            // 
            // txtGUVNLNo
            // 
            this.txtGUVNLNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.txtGUVNLNo.Name = "txtGUVNLNo";
            this.txtGUVNLNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.7D), Telerik.Reporting.Drawing.Unit.Cm(0.467D));
            this.txtGUVNLNo.Style.Font.Name = "Verdana";
            this.txtGUVNLNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtGUVNLNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtGUVNLNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtGUVNLNo.Value = "";
            // 
            // txtPhase1
            // 
            this.txtPhase1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(9.9D));
            this.txtPhase1.Name = "txtPhase1";
            this.txtPhase1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtPhase1.Style.Font.Name = "Verdana";
            this.txtPhase1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPhase1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPhase1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtPhase1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtPhase1.Value = "";
            // 
            // txtKW1
            // 
            this.txtKW1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(9.9D));
            this.txtKW1.Name = "txtKW1";
            this.txtKW1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtKW1.Style.Font.Name = "Verdana";
            this.txtKW1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKW1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtKW1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKW1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtKW1.Value = "";
            // 
            // txtKV
            // 
            this.txtKV.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.8D), Telerik.Reporting.Drawing.Unit.Cm(9D));
            this.txtKV.Name = "txtKV";
            this.txtKV.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtKV.Style.Font.Name = "Verdana";
            this.txtKV.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKV.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtKV.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKV.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtKV.Value = "";
            // 
            // txtLoad
            // 
            this.txtLoad.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(9.059D));
            this.txtLoad.Name = "txtLoad";
            this.txtLoad.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtLoad.Style.Font.Name = "Verdana";
            this.txtLoad.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtLoad.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtLoad.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtLoad.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtLoad.Value = "";
            // 
            // txt1
            // 
            this.txt1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(13.9D));
            this.txt1.Name = "txt1";
            this.txt1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txt1.Style.BackgroundColor = System.Drawing.Color.White;
            this.txt1.Style.Color = System.Drawing.Color.Black;
            this.txt1.Style.Font.Name = "Verdana";
            this.txt1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txt1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txt1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt1.Value = "";
            // 
            // txtPanelName
            // 
            this.txtPanelName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtPanelName.Name = "txtPanelName";
            this.txtPanelName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.txtPanelName.Style.Font.Name = "Verdana";
            this.txtPanelName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPanelName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanelName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelName.Value = "";
            // 
            // txtTotalKW
            // 
            this.txtTotalKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtTotalKW.Name = "txtTotalKW";
            this.txtTotalKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtTotalKW.Style.Font.Name = "Verdana";
            this.txtTotalKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtTotalKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtTotalKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalKW.Value = "";
            // 
            // txtNoPanel
            // 
            this.txtNoPanel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.6D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtNoPanel.Name = "txtNoPanel";
            this.txtNoPanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtNoPanel.Style.Font.Name = "Verdana";
            this.txtNoPanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtNoPanel.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtNoPanel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtNoPanel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNoPanel.Value = "";
            // 
            // txtStockSize
            // 
            this.txtStockSize.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtStockSize.Name = "txtStockSize";
            this.txtStockSize.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtStockSize.Style.Font.Name = "Verdana";
            this.txtStockSize.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtStockSize.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtStockSize.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtStockSize.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtStockSize.Value = "";
            // 
            // txtSeries
            // 
            this.txtSeries.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.1D), Telerik.Reporting.Drawing.Unit.Cm(13.2D));
            this.txtSeries.Name = "txtSeries";
            this.txtSeries.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.txtSeries.Style.Font.Name = "Verdana";
            this.txtSeries.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSeries.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSeries.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSeries.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSeries.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtSeries.Value = "";
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.txtModelNo.Style.Font.Name = "Verdana";
            this.txtModelNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtModelNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtModelNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtModelNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtModelNo.Value = "";
            // 
            // txtSolarPV
            // 
            this.txtSolarPV.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
            this.txtSolarPV.Name = "txtSolarPV";
            this.txtSolarPV.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtSolarPV.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.txtSolarPV.Style.Color = System.Drawing.Color.Black;
            this.txtSolarPV.Style.Font.Name = "Verdana";
            this.txtSolarPV.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSolarPV.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSolarPV.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSolarPV.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSolarPV.Value = "";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(16.1D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox3.Style.Font.Name = "Verdana";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Yes";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(20.4D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.Font.Name = "Verdana";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "";
            // 
            // txtInverterSerialNo
            // 
            this.txtInverterSerialNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.3D), Telerik.Reporting.Drawing.Unit.Cm(20D));
            this.txtInverterSerialNo.Name = "txtInverterSerialNo";
            this.txtInverterSerialNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.txtInverterSerialNo.Style.Font.Name = "Verdana";
            this.txtInverterSerialNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterSerialNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterSerialNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterSerialNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterSerialNo.Value = "";
            // 
            // txtInverterKW
            // 
            this.txtInverterKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.8D), Telerik.Reporting.Drawing.Unit.Cm(20.3D));
            this.txtInverterKW.Name = "txtInverterKW";
            this.txtInverterKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInverterKW.Style.Font.Name = "Verdana";
            this.txtInverterKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtInverterKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterKW.Value = "";
            // 
            // txtInverterPhase
            // 
            this.txtInverterPhase.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.5D), Telerik.Reporting.Drawing.Unit.Cm(20D));
            this.txtInverterPhase.Name = "txtInverterPhase";
            this.txtInverterPhase.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.txtInverterPhase.Style.Font.Name = "Verdana";
            this.txtInverterPhase.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterPhase.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterPhase.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtInverterPhase.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterPhase.Value = "";
            // 
            // txtInverterModelNo
            // 
            this.txtInverterModelNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(20D));
            this.txtInverterModelNo.Name = "txtInverterModelNo";
            this.txtInverterModelNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.txtInverterModelNo.Style.Font.Name = "Verdana";
            this.txtInverterModelNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterModelNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterModelNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterModelNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterModelNo.Value = "";
            // 
            // txtInverterName
            // 
            this.txtInverterName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(20D));
            this.txtInverterName.Name = "txtInverterName";
            this.txtInverterName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.txtInverterName.Style.Font.Name = "Verdana";
            this.txtInverterName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterName.Value = "";
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.8D), Telerik.Reporting.Drawing.Unit.Cm(16.8D));
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtManufacturer.Style.Font.Name = "Verdana";
            this.txtManufacturer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtManufacturer.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtManufacturer.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtManufacturer.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtManufacturer.Value = "";
            // 
            // txtConsumerNoPage2
            // 
            this.txtConsumerNoPage2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(61.9D));
            this.txtConsumerNoPage2.Name = "txtConsumerNoPage2";
            this.txtConsumerNoPage2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoPage2.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage2.Value = "";
            // 
            // txtConsumerNoPage3
            // 
            this.txtConsumerNoPage3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.1D), Telerik.Reporting.Drawing.Unit.Cm(91.5D));
            this.txtConsumerNoPage3.Name = "txtConsumerNoPage3";
            this.txtConsumerNoPage3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtConsumerNoPage3.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage3.Value = "";
            // 
            // Self_Certification_TORRENT
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Self_Certification_TORRENT";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

            }
		#endregion
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox Pic1;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox Pic3;
        private Telerik.Reporting.PictureBox Pic4;
        private Telerik.Reporting.TextBox txtConsumerNo;
        private Telerik.Reporting.TextBox txtGUVNLNo;
        private Telerik.Reporting.TextBox txtCustomer;
        private Telerik.Reporting.TextBox txtConsumerNoBSI;
        private Telerik.Reporting.TextBox txtSubDivision;
        private Telerik.Reporting.TextBox txtAddress1;
        private Telerik.Reporting.TextBox txtAddress2;
        private Telerik.Reporting.TextBox txtLoad;
        private Telerik.Reporting.TextBox txtKV;
        private Telerik.Reporting.TextBox txtKW1;
        private Telerik.Reporting.TextBox txtPhase1;
        private Telerik.Reporting.TextBox txt1;
        private Telerik.Reporting.TextBox txtSolarPV;
        private Telerik.Reporting.TextBox txtModelNo;
        private Telerik.Reporting.TextBox txtSeries;
        private Telerik.Reporting.TextBox txtStockSize;
        private Telerik.Reporting.TextBox txtNoPanel;
        private Telerik.Reporting.TextBox txtTotalKW;
        private Telerik.Reporting.TextBox txtPanelName;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox txtInverterName;
        private Telerik.Reporting.TextBox txtInverterModelNo;
        private Telerik.Reporting.TextBox txtInverterPhase;
        private Telerik.Reporting.TextBox txtInverterKW;
        private Telerik.Reporting.TextBox txtInverterSerialNo;
        private Telerik.Reporting.TextBox txtManufacturer;
        private Telerik.Reporting.TextBox txtConsumerNoPage2;
        private Telerik.Reporting.TextBox txtConsumerNoPage3;
    }
}