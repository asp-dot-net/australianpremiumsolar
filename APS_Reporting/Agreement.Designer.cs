namespace APS_Reporting
{
    partial class Agreement
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agreement));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.Pic1 = new Telerik.Reporting.PictureBox();
            this.Pic2 = new Telerik.Reporting.PictureBox();
            this.Pic3 = new Telerik.Reporting.PictureBox();
            this.Pic4 = new Telerik.Reporting.PictureBox();
            this.Pic5 = new Telerik.Reporting.PictureBox();
            this.Pic6 = new Telerik.Reporting.PictureBox();
            this.Pic7 = new Telerik.Reporting.PictureBox();
            this.Pic8 = new Telerik.Reporting.PictureBox();
            this.txtCustomer = new Telerik.Reporting.TextBox();
            this.txtAddress = new Telerik.Reporting.TextBox();
            this.txtDiscomName = new Telerik.Reporting.TextBox();
            this.txtDivision = new Telerik.Reporting.TextBox();
            this.txtCustomer1 = new Telerik.Reporting.TextBox();
            this.txtKW = new Telerik.Reporting.TextBox();
            this.txtDiscomName1 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.txtKW1 = new Telerik.Reporting.TextBox();
            this.txtDiscomShortName = new Telerik.Reporting.TextBox();
            this.txtKW2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(2376D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Pic1,
            this.Pic2,
            this.Pic3,
            this.Pic4,
            this.Pic5,
            this.Pic6,
            this.Pic7,
            this.Pic8,
            this.txtCustomer,
            this.txtAddress,
            this.txtDiscomName,
            this.txtDivision,
            this.txtCustomer1,
            this.txtKW,
            this.txtDiscomName1,
            this.textBox1,
            this.txtKW1,
            this.txtDiscomShortName,
            this.txtKW2});
            this.detail.Name = "detail";
            this.detail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // Pic1
            // 
            this.Pic1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Pic1.MimeType = "image/jpeg";
            this.Pic1.Name = "Pic1";
            this.Pic1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic1.Value = ((object)(resources.GetObject("Pic1.Value")));
            // 
            // Pic2
            // 
            this.Pic2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.Pic2.MimeType = "image/jpeg";
            this.Pic2.Name = "Pic2";
            this.Pic2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic2.Value = ((object)(resources.GetObject("Pic2.Value")));
            // 
            // Pic3
            // 
            this.Pic3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.4D));
            this.Pic3.MimeType = "image/jpeg";
            this.Pic3.Name = "Pic3";
            this.Pic3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic3.Value = ((object)(resources.GetObject("Pic3.Value")));
            // 
            // Pic4
            // 
            this.Pic4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1D));
            this.Pic4.MimeType = "image/jpeg";
            this.Pic4.Name = "Pic4";
            this.Pic4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic4.Value = ((object)(resources.GetObject("Pic4.Value")));
            // 
            // Pic5
            // 
            this.Pic5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.8D));
            this.Pic5.MimeType = "image/jpeg";
            this.Pic5.Name = "Pic5";
            this.Pic5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic5.Value = ((object)(resources.GetObject("Pic5.Value")));
            // 
            // Pic6
            // 
            this.Pic6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(148.5D));
            this.Pic6.MimeType = "image/jpeg";
            this.Pic6.Name = "Pic6";
            this.Pic6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic6.Value = ((object)(resources.GetObject("Pic6.Value")));
            // 
            // Pic7
            // 
            this.Pic7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(178.2D));
            this.Pic7.MimeType = "image/jpeg";
            this.Pic7.Name = "Pic7";
            this.Pic7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic7.Value = ((object)(resources.GetObject("Pic7.Value")));
            // 
            // Pic8
            // 
            this.Pic8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(207.9D));
            this.Pic8.MimeType = "image/jpeg";
            this.Pic8.Name = "Pic8";
            this.Pic8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.Pic8.Value = ((object)(resources.GetObject("Pic8.Value")));
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(7.6D));
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtCustomer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtCustomer.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.txtCustomer.Value = "";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.3D), Telerik.Reporting.Drawing.Unit.Cm(8.7D));
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress.Value = "";
            // 
            // txtDiscomName
            // 
            this.txtDiscomName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(10.2D));
            this.txtDiscomName.Name = "txtDiscomName";
            this.txtDiscomName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtDiscomName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDiscomName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDiscomName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtDiscomName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName.Value = "";
            // 
            // txtDivision
            // 
            this.txtDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.9D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.txtDivision.Name = "txtDivision";
            this.txtDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDivision.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDivision.Value = "";
            // 
            // txtCustomer1
            // 
            this.txtCustomer1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(12.9D));
            this.txtCustomer1.Name = "txtCustomer1";
            this.txtCustomer1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtCustomer1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtCustomer1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustomer1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomer1.Value = "";
            // 
            // txtKW
            // 
            this.txtKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.7D), Telerik.Reporting.Drawing.Unit.Cm(13.8D));
            this.txtKW.Name = "txtKW";
            this.txtKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW.Value = "";
            // 
            // txtDiscomName1
            // 
            this.txtDiscomName1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(13.8D));
            this.txtDiscomName1.Name = "txtDiscomName1";
            this.txtDiscomName1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtDiscomName1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDiscomName1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDiscomName1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtDiscomName1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName1.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.9D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "230";
            // 
            // txtKW1
            // 
            this.txtKW1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.8D), Telerik.Reporting.Drawing.Unit.Cm(16.4D));
            this.txtKW1.Name = "txtKW1";
            this.txtKW1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtKW1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtKW1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtKW1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW1.Value = "";
            // 
            // txtDiscomShortName
            // 
            this.txtDiscomShortName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.2D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
            this.txtDiscomShortName.Name = "txtDiscomShortName";
            this.txtDiscomShortName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtDiscomShortName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDiscomShortName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDiscomShortName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtDiscomShortName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomShortName.Value = "";
            // 
            // txtKW2
            // 
            this.txtKW2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(19.4D));
            this.txtKW2.Name = "txtKW2";
            this.txtKW2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtKW2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtKW2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtKW2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW2.Value = "";
            // 
            // Agreement
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Agreement";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox Pic1;
        private Telerik.Reporting.PictureBox Pic2;
        private Telerik.Reporting.PictureBox Pic3;
        private Telerik.Reporting.PictureBox Pic4;
        private Telerik.Reporting.PictureBox Pic5;
        private Telerik.Reporting.PictureBox Pic6;
        private Telerik.Reporting.PictureBox Pic7;
        private Telerik.Reporting.PictureBox Pic8;
        private Telerik.Reporting.TextBox txtCustomer;
        private Telerik.Reporting.TextBox txtAddress;
        private Telerik.Reporting.TextBox txtDiscomName;
        private Telerik.Reporting.TextBox txtDivision;
        private Telerik.Reporting.TextBox txtCustomer1;
        private Telerik.Reporting.TextBox txtKW;
        private Telerik.Reporting.TextBox txtDiscomName1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox txtKW1;
        private Telerik.Reporting.TextBox txtDiscomShortName;
        private Telerik.Reporting.TextBox txtKW2;
    }
}