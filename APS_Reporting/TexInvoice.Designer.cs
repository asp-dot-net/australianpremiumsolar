namespace APS_Reporting
{
    partial class TexInvoice
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TexInvoice));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.picBg = new Telerik.Reporting.PictureBox();
            this.txtProjectNo = new Telerik.Reporting.TextBox();
            this.txtPaymentDate = new Telerik.Reporting.TextBox();
            this.txtName = new Telerik.Reporting.TextBox();
            this.txtMobile = new Telerik.Reporting.TextBox();
            this.txtPhone = new Telerik.Reporting.TextBox();
            this.txtEmail = new Telerik.Reporting.TextBox();
            this.txtInstallAddress = new Telerik.Reporting.TextBox();
            this.txtAddress = new Telerik.Reporting.TextBox();
            this.txtPanels = new Telerik.Reporting.TextBox();
            this.txtInverter = new Telerik.Reporting.TextBox();
            this.txtPanelsName = new Telerik.Reporting.TextBox();
            this.txtInverterName = new Telerik.Reporting.TextBox();
            this.txtlessSubsidy = new Telerik.Reporting.TextBox();
            this.txtTotalNetCost = new Telerik.Reporting.TextBox();
            this.txtPaidToDate = new Telerik.Reporting.TextBox();
            this.txtBalanceDue = new Telerik.Reporting.TextBox();
            this.txtTotalCost = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.picBg,
            this.txtProjectNo,
            this.txtPaymentDate,
            this.txtName,
            this.txtMobile,
            this.txtPhone,
            this.txtEmail,
            this.txtInstallAddress,
            this.txtAddress,
            this.txtPanels,
            this.txtInverter,
            this.txtPanelsName,
            this.txtInverterName,
            this.txtlessSubsidy,
            this.txtTotalNetCost,
            this.txtPaidToDate,
            this.txtBalanceDue,
            this.txtTotalCost});
            this.detail.Name = "detail";
            // 
            // picBg
            // 
            this.picBg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.picBg.MimeType = "image/jpeg";
            this.picBg.Name = "picBg";
            this.picBg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picBg.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picBg.Value = ((object)(resources.GetObject("picBg.Value")));
            // 
            // txtProjectNo
            // 
            this.txtProjectNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(2.9D));
            this.txtProjectNo.Name = "txtProjectNo";
            this.txtProjectNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtProjectNo.Style.Font.Name = "Verdana";
            this.txtProjectNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtProjectNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtProjectNo.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtProjectNo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtProjectNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtProjectNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNo.Value = "";
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(2.9D));
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtPaymentDate.Style.Font.Name = "Verdana";
            this.txtPaymentDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPaymentDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPaymentDate.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPaymentDate.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPaymentDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPaymentDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPaymentDate.Value = "";
            // 
            // txtName
            // 
            this.txtName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.txtName.Name = "txtName";
            this.txtName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtName.Style.Font.Name = "Verdana";
            this.txtName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtName.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtName.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtName.Value = "";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(6.3D));
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtMobile.Style.Font.Name = "Verdana";
            this.txtMobile.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtMobile.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtMobile.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtMobile.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMobile.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtMobile.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMobile.Value = "";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.7D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtPhone.Style.Font.Name = "Verdana";
            this.txtPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPhone.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPhone.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPhone.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPhone.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPhone.Value = "";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.7D), Telerik.Reporting.Drawing.Unit.Cm(6.3D));
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtEmail.Style.Font.Name = "Verdana";
            this.txtEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtEmail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtEmail.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtEmail.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtEmail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtEmail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtEmail.Value = "";
            // 
            // txtInstallAddress
            // 
            this.txtInstallAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(9.2D));
            this.txtInstallAddress.Name = "txtInstallAddress";
            this.txtInstallAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallAddress.Style.Font.Name = "Verdana";
            this.txtInstallAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstallAddress.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtInstallAddress.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtInstallAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInstallAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallAddress.Value = "";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(9.9D));
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtAddress.Style.Font.Name = "Verdana";
            this.txtAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtAddress.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress.Value = "";
            // 
            // txtPanels
            // 
            this.txtPanels.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
            this.txtPanels.Name = "txtPanels";
            this.txtPanels.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtPanels.Style.Font.Name = "Verdana";
            this.txtPanels.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanels.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtPanels.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPanels.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPanels.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanels.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanels.Value = "";
            // 
            // txtInverter
            // 
            this.txtInverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
            this.txtInverter.Name = "txtInverter";
            this.txtInverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtInverter.Style.Font.Name = "Verdana";
            this.txtInverter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtInverter.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtInverter.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtInverter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverter.Value = "";
            // 
            // txtPanelsName
            // 
            this.txtPanelsName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
            this.txtPanelsName.Name = "txtPanelsName";
            this.txtPanelsName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtPanelsName.Style.Font.Name = "Verdana";
            this.txtPanelsName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanelsName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelsName.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPanelsName.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPanelsName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanelsName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelsName.Value = "";
            // 
            // txtInverterName
            // 
            this.txtInverterName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
            this.txtInverterName.Name = "txtInverterName";
            this.txtInverterName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtInverterName.Style.Font.Name = "Verdana";
            this.txtInverterName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverterName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterName.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtInverterName.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtInverterName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterName.Value = "";
            // 
            // txtlessSubsidy
            // 
            this.txtlessSubsidy.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(13.5D));
            this.txtlessSubsidy.Name = "txtlessSubsidy";
            this.txtlessSubsidy.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtlessSubsidy.Style.Font.Name = "Verdana";
            this.txtlessSubsidy.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtlessSubsidy.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtlessSubsidy.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtlessSubsidy.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtlessSubsidy.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtlessSubsidy.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtlessSubsidy.Value = "";
            // 
            // txtTotalNetCost
            // 
            this.txtTotalNetCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(14.4D));
            this.txtTotalNetCost.Name = "txtTotalNetCost";
            this.txtTotalNetCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtTotalNetCost.Style.Font.Name = "Verdana";
            this.txtTotalNetCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtTotalNetCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalNetCost.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtTotalNetCost.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtTotalNetCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtTotalNetCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalNetCost.Value = "";
            // 
            // txtPaidToDate
            // 
            this.txtPaidToDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(15.3D));
            this.txtPaidToDate.Name = "txtPaidToDate";
            this.txtPaidToDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtPaidToDate.Style.Font.Name = "Verdana";
            this.txtPaidToDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPaidToDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPaidToDate.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtPaidToDate.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPaidToDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPaidToDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPaidToDate.Value = "";
            // 
            // txtBalanceDue
            // 
            this.txtBalanceDue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(16.2D));
            this.txtBalanceDue.Name = "txtBalanceDue";
            this.txtBalanceDue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtBalanceDue.Style.Font.Name = "Verdana";
            this.txtBalanceDue.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtBalanceDue.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtBalanceDue.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtBalanceDue.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtBalanceDue.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtBalanceDue.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBalanceDue.Value = "";
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16D), Telerik.Reporting.Drawing.Unit.Cm(12.3D));
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtTotalCost.Style.Color = System.Drawing.Color.White;
            this.txtTotalCost.Style.Font.Bold = true;
            this.txtTotalCost.Style.Font.Name = "Verdana";
            this.txtTotalCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtTotalCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalCost.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.txtTotalCost.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtTotalCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtTotalCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalCost.Value = "";
            // 
            // PaymentReceipt
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "PaymentReceipt";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox picBg;
        private Telerik.Reporting.TextBox txtProjectNo;
        private Telerik.Reporting.TextBox txtPaymentDate;
        private Telerik.Reporting.TextBox txtName;
        private Telerik.Reporting.TextBox txtMobile;
        private Telerik.Reporting.TextBox txtPhone;
        private Telerik.Reporting.TextBox txtEmail;
        private Telerik.Reporting.TextBox txtInstallAddress;
        private Telerik.Reporting.TextBox txtAddress;
        private Telerik.Reporting.TextBox txtPanels;
        private Telerik.Reporting.TextBox txtInverter;
        private Telerik.Reporting.TextBox txtPanelsName;
        private Telerik.Reporting.TextBox txtInverterName;
        private Telerik.Reporting.TextBox txtlessSubsidy;
        private Telerik.Reporting.TextBox txtTotalNetCost;
        private Telerik.Reporting.TextBox txtPaidToDate;
        private Telerik.Reporting.TextBox txtBalanceDue;
        private Telerik.Reporting.TextBox txtTotalCost;
    }
}