namespace APS_Reporting
{
    partial class Picklist
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Picklist));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.picPage1bg = new Telerik.Reporting.PictureBox();
            this.picPage2bg = new Telerik.Reporting.PictureBox();
            this.txtProjectNo = new Telerik.Reporting.TextBox();
            this.txtCustomerName = new Telerik.Reporting.TextBox();
            this.txtContNo = new Telerik.Reporting.TextBox();
            this.txtAltNo = new Telerik.Reporting.TextBox();
            this.txtInstalledAddress = new Telerik.Reporting.TextBox();
            this.txtApplicationNo = new Telerik.Reporting.TextBox();
            this.txtSystemSize = new Telerik.Reporting.TextBox();
            this.txtStruHeight = new Telerik.Reporting.TextBox();
            this.txtInstallationDate = new Telerik.Reporting.TextBox();
            this.txtPanelDescription = new Telerik.Reporting.TextBox();
            this.txtInverterDescription = new Telerik.Reporting.TextBox();
            this.txtNoPanels = new Telerik.Reporting.TextBox();
            this.txtNoInverter = new Telerik.Reporting.TextBox();
            this.txt3 = new Telerik.Reporting.TextBox();
            this.txt4 = new Telerik.Reporting.TextBox();
            this.txt5 = new Telerik.Reporting.TextBox();
            this.txt6 = new Telerik.Reporting.TextBox();
            this.txt7 = new Telerik.Reporting.TextBox();
            this.txt8 = new Telerik.Reporting.TextBox();
            this.txt9 = new Telerik.Reporting.TextBox();
            this.txt10 = new Telerik.Reporting.TextBox();
            this.txt11 = new Telerik.Reporting.TextBox();
            this.txt12 = new Telerik.Reporting.TextBox();
            this.txt13 = new Telerik.Reporting.TextBox();
            this.txt14 = new Telerik.Reporting.TextBox();
            this.txt15 = new Telerik.Reporting.TextBox();
            this.txt16 = new Telerik.Reporting.TextBox();
            this.txt17 = new Telerik.Reporting.TextBox();
            this.txt18 = new Telerik.Reporting.TextBox();
            this.txt19 = new Telerik.Reporting.TextBox();
            this.txt20 = new Telerik.Reporting.TextBox();
            this.txt21 = new Telerik.Reporting.TextBox();
            this.txt22 = new Telerik.Reporting.TextBox();
            this.txt23 = new Telerik.Reporting.TextBox();
            this.txt24 = new Telerik.Reporting.TextBox();
            this.txt25 = new Telerik.Reporting.TextBox();
            this.txt26 = new Telerik.Reporting.TextBox();
            this.txt27 = new Telerik.Reporting.TextBox();
            this.txt28 = new Telerik.Reporting.TextBox();
            this.txt29 = new Telerik.Reporting.TextBox();
            this.txt30 = new Telerik.Reporting.TextBox();
            this.txt31 = new Telerik.Reporting.TextBox();
            this.txt32 = new Telerik.Reporting.TextBox();
            this.txt33 = new Telerik.Reporting.TextBox();
            this.txt34 = new Telerik.Reporting.TextBox();
            this.txt35 = new Telerik.Reporting.TextBox();
            this.txt36 = new Telerik.Reporting.TextBox();
            this.txt37 = new Telerik.Reporting.TextBox();
            this.txt38 = new Telerik.Reporting.TextBox();
            this.txt39 = new Telerik.Reporting.TextBox();
            this.txt40 = new Telerik.Reporting.TextBox();
            this.txt41 = new Telerik.Reporting.TextBox();
            this.txt42 = new Telerik.Reporting.TextBox();
            this.txt43 = new Telerik.Reporting.TextBox();
            this.txt44 = new Telerik.Reporting.TextBox();
            this.txt45 = new Telerik.Reporting.TextBox();
            this.txt46 = new Telerik.Reporting.TextBox();
            this.txt47 = new Telerik.Reporting.TextBox();
            this.txtInstallerName = new Telerik.Reporting.TextBox();
            this.txtInstallerNo = new Telerik.Reporting.TextBox();
            this.txtTotalCost = new Telerik.Reporting.TextBox();
            this.txtBalOwing = new Telerik.Reporting.TextBox();
            this.txtCPName = new Telerik.Reporting.TextBox();
            this.txtCPContactNo = new Telerik.Reporting.TextBox();
            this.txtPaidDate = new Telerik.Reporting.TextBox();
            this.txtInstallerNotes = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(594D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.picPage1bg,
            this.picPage2bg,
            this.txtProjectNo,
            this.txtCustomerName,
            this.txtContNo,
            this.txtAltNo,
            this.txtInstalledAddress,
            this.txtApplicationNo,
            this.txtSystemSize,
            this.txtStruHeight,
            this.txtInstallationDate,
            this.txtPanelDescription,
            this.txtInverterDescription,
            this.txtNoPanels,
            this.txtNoInverter,
            this.txt3,
            this.txt4,
            this.txt5,
            this.txt6,
            this.txt7,
            this.txt8,
            this.txt9,
            this.txt10,
            this.txt11,
            this.txt12,
            this.txt13,
            this.txt14,
            this.txt15,
            this.txt16,
            this.txt17,
            this.txt18,
            this.txt19,
            this.txt20,
            this.txt21,
            this.txt22,
            this.txt23,
            this.txt24,
            this.txt25,
            this.txt26,
            this.txt27,
            this.txt28,
            this.txt29,
            this.txt30,
            this.txt31,
            this.txt32,
            this.txt33,
            this.txt34,
            this.txt35,
            this.txt36,
            this.txt37,
            this.txt38,
            this.txt39,
            this.txt40,
            this.txt41,
            this.txt42,
            this.txt43,
            this.txt44,
            this.txt45,
            this.txt46,
            this.txt47,
            this.txtInstallerName,
            this.txtInstallerNo,
            this.txtTotalCost,
            this.txtBalOwing,
            this.txtCPName,
            this.txtCPContactNo,
            this.txtPaidDate,
            this.txtInstallerNotes});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Verdana";
            // 
            // picPage1bg
            // 
            this.picPage1bg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.picPage1bg.MimeType = "image/jpeg";
            this.picPage1bg.Name = "picPage1bg";
            this.picPage1bg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picPage1bg.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picPage1bg.Style.Font.Name = "Verdana";
            this.picPage1bg.Value = ((object)(resources.GetObject("picPage1bg.Value")));
            // 
            // picPage2bg
            // 
            this.picPage2bg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.picPage2bg.MimeType = "image/jpeg";
            this.picPage2bg.Name = "picPage2bg";
            this.picPage2bg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picPage2bg.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picPage2bg.Style.Font.Name = "Verdana";
            this.picPage2bg.Value = ((object)(resources.GetObject("picPage2bg.Value")));
            // 
            // txtProjectNo
            // 
            this.txtProjectNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(3.3D));
            this.txtProjectNo.Multiline = true;
            this.txtProjectNo.Name = "txtProjectNo";
            this.txtProjectNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtProjectNo.Style.Font.Name = "Verdana";
            this.txtProjectNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtProjectNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtProjectNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNo.Value = "";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(4.2D));
            this.txtCustomerName.Multiline = true;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCustomerName.Style.Font.Name = "Verdana";
            this.txtCustomerName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCustomerName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustomerName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomerName.Value = "";
            // 
            // txtContNo
            // 
            this.txtContNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.txtContNo.Multiline = true;
            this.txtContNo.Name = "txtContNo";
            this.txtContNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtContNo.Style.Font.Name = "Verdana";
            this.txtContNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtContNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtContNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtContNo.Value = "";
            // 
            // txtAltNo
            // 
            this.txtAltNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.txtAltNo.Multiline = true;
            this.txtAltNo.Name = "txtAltNo";
            this.txtAltNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtAltNo.Style.Font.Name = "Verdana";
            this.txtAltNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtAltNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAltNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAltNo.Value = "";
            // 
            // txtInstalledAddress
            // 
            this.txtInstalledAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(6D));
            this.txtInstalledAddress.Multiline = true;
            this.txtInstalledAddress.Name = "txtInstalledAddress";
            this.txtInstalledAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.txtInstalledAddress.Style.Font.Name = "Verdana";
            this.txtInstalledAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstalledAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstalledAddress.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.txtInstalledAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtInstalledAddress.Value = "";
            // 
            // txtApplicationNo
            // 
            this.txtApplicationNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.5D), Telerik.Reporting.Drawing.Unit.Cm(3.3D));
            this.txtApplicationNo.Multiline = true;
            this.txtApplicationNo.Name = "txtApplicationNo";
            this.txtApplicationNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtApplicationNo.Style.Font.Name = "Verdana";
            this.txtApplicationNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtApplicationNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtApplicationNo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtApplicationNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtApplicationNo.Value = "";
            // 
            // txtSystemSize
            // 
            this.txtSystemSize.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.5D), Telerik.Reporting.Drawing.Unit.Cm(4.2D));
            this.txtSystemSize.Multiline = true;
            this.txtSystemSize.Name = "txtSystemSize";
            this.txtSystemSize.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtSystemSize.Style.Font.Name = "Verdana";
            this.txtSystemSize.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtSystemSize.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSystemSize.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSystemSize.Value = "";
            // 
            // txtStruHeight
            // 
            this.txtStruHeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.5D), Telerik.Reporting.Drawing.Unit.Cm(5D));
            this.txtStruHeight.Multiline = true;
            this.txtStruHeight.Name = "txtStruHeight";
            this.txtStruHeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtStruHeight.Style.Font.Name = "Verdana";
            this.txtStruHeight.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtStruHeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtStruHeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtStruHeight.Value = "";
            // 
            // txtInstallationDate
            // 
            this.txtInstallationDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.5D), Telerik.Reporting.Drawing.Unit.Cm(5.9D));
            this.txtInstallationDate.Multiline = true;
            this.txtInstallationDate.Name = "txtInstallationDate";
            this.txtInstallationDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallationDate.Style.Font.Name = "Verdana";
            this.txtInstallationDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallationDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstallationDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallationDate.Value = "";
            // 
            // txtPanelDescription
            // 
            this.txtPanelDescription.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(9D));
            this.txtPanelDescription.Multiline = true;
            this.txtPanelDescription.Name = "txtPanelDescription";
            this.txtPanelDescription.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtPanelDescription.Style.Font.Name = "Verdana";
            this.txtPanelDescription.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanelDescription.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelDescription.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelDescription.Value = "";
            // 
            // txtInverterDescription
            // 
            this.txtInverterDescription.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(10D));
            this.txtInverterDescription.Multiline = true;
            this.txtInverterDescription.Name = "txtInverterDescription";
            this.txtInverterDescription.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtInverterDescription.Style.Font.Name = "Verdana";
            this.txtInverterDescription.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverterDescription.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterDescription.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterDescription.Value = "";
            // 
            // txtNoPanels
            // 
            this.txtNoPanels.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(9D));
            this.txtNoPanels.Multiline = true;
            this.txtNoPanels.Name = "txtNoPanels";
            this.txtNoPanels.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtNoPanels.Style.Font.Name = "Verdana";
            this.txtNoPanels.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtNoPanels.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtNoPanels.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtNoPanels.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNoPanels.Value = "";
            // 
            // txtNoInverter
            // 
            this.txtNoInverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(10D));
            this.txtNoInverter.Multiline = true;
            this.txtNoInverter.Name = "txtNoInverter";
            this.txtNoInverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtNoInverter.Style.Font.Name = "Verdana";
            this.txtNoInverter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtNoInverter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtNoInverter.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtNoInverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNoInverter.Value = "";
            // 
            // txt3
            // 
            this.txt3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(11.7D));
            this.txt3.Multiline = true;
            this.txt3.Name = "txt3";
            this.txt3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt3.Style.Font.Name = "Verdana";
            this.txt3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(1D);
            this.txt3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt3.Value = "";
            // 
            // txt4
            // 
            this.txt4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(12.4D));
            this.txt4.Multiline = true;
            this.txt4.Name = "txt4";
            this.txt4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt4.Style.Font.Name = "Verdana";
            this.txt4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(1D);
            this.txt4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt4.Value = "";
            // 
            // txt5
            // 
            this.txt5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
            this.txt5.Multiline = true;
            this.txt5.Name = "txt5";
            this.txt5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt5.Style.Font.Name = "Verdana";
            this.txt5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt5.Value = "";
            // 
            // txt6
            // 
            this.txt6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(13.7D));
            this.txt6.Multiline = true;
            this.txt6.Name = "txt6";
            this.txt6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt6.Style.Font.Name = "Verdana";
            this.txt6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt6.Value = "";
            // 
            // txt7
            // 
            this.txt7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
            this.txt7.Multiline = true;
            this.txt7.Name = "txt7";
            this.txt7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt7.Style.Font.Name = "Verdana";
            this.txt7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt7.Value = "";
            // 
            // txt8
            // 
            this.txt8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(15D));
            this.txt8.Multiline = true;
            this.txt8.Name = "txt8";
            this.txt8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt8.Style.Font.Name = "Verdana";
            this.txt8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt8.Value = "";
            // 
            // txt9
            // 
            this.txt9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(15.6D));
            this.txt9.Multiline = true;
            this.txt9.Name = "txt9";
            this.txt9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt9.Style.Font.Name = "Verdana";
            this.txt9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt9.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt9.Value = "";
            // 
            // txt10
            // 
            this.txt10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(16.2D));
            this.txt10.Multiline = true;
            this.txt10.Name = "txt10";
            this.txt10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt10.Style.Font.Name = "Verdana";
            this.txt10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt10.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt10.Value = "";
            // 
            // txt11
            // 
            this.txt11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(16.9D));
            this.txt11.Multiline = true;
            this.txt11.Name = "txt11";
            this.txt11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt11.Style.Font.Name = "Verdana";
            this.txt11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt11.Value = "";
            // 
            // txt12
            // 
            this.txt12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(17.5D));
            this.txt12.Multiline = true;
            this.txt12.Name = "txt12";
            this.txt12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt12.Style.Font.Name = "Verdana";
            this.txt12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt12.Value = "";
            // 
            // txt13
            // 
            this.txt13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(18.1D));
            this.txt13.Multiline = true;
            this.txt13.Name = "txt13";
            this.txt13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt13.Style.Font.Name = "Verdana";
            this.txt13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt13.Value = "";
            // 
            // txt14
            // 
            this.txt14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(18.8D));
            this.txt14.Multiline = true;
            this.txt14.Name = "txt14";
            this.txt14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt14.Style.Font.Name = "Verdana";
            this.txt14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt14.Value = "";
            // 
            // txt15
            // 
            this.txt15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(19.4D));
            this.txt15.Multiline = true;
            this.txt15.Name = "txt15";
            this.txt15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt15.Style.Font.Name = "Verdana";
            this.txt15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt15.Value = "";
            // 
            // txt16
            // 
            this.txt16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(20.1D));
            this.txt16.Multiline = true;
            this.txt16.Name = "txt16";
            this.txt16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt16.Style.Font.Name = "Verdana";
            this.txt16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt16.Value = "";
            // 
            // txt17
            // 
            this.txt17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(21.6D));
            this.txt17.Multiline = true;
            this.txt17.Name = "txt17";
            this.txt17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt17.Style.Font.Name = "Verdana";
            this.txt17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt17.Value = "";
            // 
            // txt18
            // 
            this.txt18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(22.2D));
            this.txt18.Multiline = true;
            this.txt18.Name = "txt18";
            this.txt18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt18.Style.Font.Name = "Verdana";
            this.txt18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt18.Value = "";
            // 
            // txt19
            // 
            this.txt19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(22.9D));
            this.txt19.Multiline = true;
            this.txt19.Name = "txt19";
            this.txt19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt19.Style.Font.Name = "Verdana";
            this.txt19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt19.Value = "";
            // 
            // txt20
            // 
            this.txt20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(23.5D));
            this.txt20.Multiline = true;
            this.txt20.Name = "txt20";
            this.txt20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt20.Style.Font.Name = "Verdana";
            this.txt20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt20.Value = "";
            // 
            // txt21
            // 
            this.txt21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(24.1D));
            this.txt21.Multiline = true;
            this.txt21.Name = "txt21";
            this.txt21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt21.Style.Font.Name = "Verdana";
            this.txt21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt21.Value = "";
            // 
            // txt22
            // 
            this.txt22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(24.8D));
            this.txt22.Multiline = true;
            this.txt22.Name = "txt22";
            this.txt22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt22.Style.Font.Name = "Verdana";
            this.txt22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt22.Value = "";
            // 
            // txt23
            // 
            this.txt23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(25.4D));
            this.txt23.Multiline = true;
            this.txt23.Name = "txt23";
            this.txt23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt23.Style.Font.Name = "Verdana";
            this.txt23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt23.Value = "";
            // 
            // txt24
            // 
            this.txt24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(26D));
            this.txt24.Multiline = true;
            this.txt24.Name = "txt24";
            this.txt24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt24.Style.Font.Name = "Verdana";
            this.txt24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt24.Value = "";
            // 
            // txt25
            // 
            this.txt25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(26.7D));
            this.txt25.Multiline = true;
            this.txt25.Name = "txt25";
            this.txt25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt25.Style.Font.Name = "Verdana";
            this.txt25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt25.Value = "";
            // 
            // txt26
            // 
            this.txt26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(27.3D));
            this.txt26.Multiline = true;
            this.txt26.Name = "txt26";
            this.txt26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt26.Style.Font.Name = "Verdana";
            this.txt26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt26.Value = "";
            // 
            // txt27
            // 
            this.txt27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(27.9D));
            this.txt27.Multiline = true;
            this.txt27.Name = "txt27";
            this.txt27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt27.Style.Font.Name = "Verdana";
            this.txt27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt27.Value = "";
            // 
            // txt28
            // 
            this.txt28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(31.3D));
            this.txt28.Multiline = true;
            this.txt28.Name = "txt28";
            this.txt28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt28.Style.Font.Name = "Verdana";
            this.txt28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt28.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt28.Value = "";
            // 
            // txt29
            // 
            this.txt29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(32D));
            this.txt29.Multiline = true;
            this.txt29.Name = "txt29";
            this.txt29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt29.Style.Font.Name = "Verdana";
            this.txt29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt29.Value = "";
            // 
            // txt30
            // 
            this.txt30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
            this.txt30.Multiline = true;
            this.txt30.Name = "txt30";
            this.txt30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt30.Style.Font.Name = "Verdana";
            this.txt30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt30.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt30.Value = "";
            // 
            // txt31
            // 
            this.txt31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(33.3D));
            this.txt31.Multiline = true;
            this.txt31.Name = "txt31";
            this.txt31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt31.Style.Font.Name = "Verdana";
            this.txt31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt31.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt31.Value = "";
            // 
            // txt32
            // 
            this.txt32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(33.9D));
            this.txt32.Multiline = true;
            this.txt32.Name = "txt32";
            this.txt32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt32.Style.Font.Name = "Verdana";
            this.txt32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt32.Value = "";
            // 
            // txt33
            // 
            this.txt33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(34.6D));
            this.txt33.Multiline = true;
            this.txt33.Name = "txt33";
            this.txt33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt33.Style.Font.Name = "Verdana";
            this.txt33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt33.Value = "";
            // 
            // txt34
            // 
            this.txt34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(35.2D));
            this.txt34.Multiline = true;
            this.txt34.Name = "txt34";
            this.txt34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt34.Style.Font.Name = "Verdana";
            this.txt34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt34.Value = "";
            // 
            // txt35
            // 
            this.txt35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(35.8D));
            this.txt35.Multiline = true;
            this.txt35.Name = "txt35";
            this.txt35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt35.Style.Font.Name = "Verdana";
            this.txt35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt35.Value = "";
            // 
            // txt36
            // 
            this.txt36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(36.4D));
            this.txt36.Multiline = true;
            this.txt36.Name = "txt36";
            this.txt36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt36.Style.Font.Name = "Verdana";
            this.txt36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt36.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt36.Value = "";
            // 
            // txt37
            // 
            this.txt37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(37.1D));
            this.txt37.Multiline = true;
            this.txt37.Name = "txt37";
            this.txt37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt37.Style.Font.Name = "Verdana";
            this.txt37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt37.Value = "";
            // 
            // txt38
            // 
            this.txt38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(37.7D));
            this.txt38.Multiline = true;
            this.txt38.Name = "txt38";
            this.txt38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt38.Style.Font.Name = "Verdana";
            this.txt38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt38.Value = "";
            // 
            // txt39
            // 
            this.txt39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(38.3D));
            this.txt39.Multiline = true;
            this.txt39.Name = "txt39";
            this.txt39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt39.Style.Font.Name = "Verdana";
            this.txt39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt39.Value = "";
            // 
            // txt40
            // 
            this.txt40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(39D));
            this.txt40.Multiline = true;
            this.txt40.Name = "txt40";
            this.txt40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt40.Style.Font.Name = "Verdana";
            this.txt40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt40.Value = "";
            // 
            // txt41
            // 
            this.txt41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(39.7D));
            this.txt41.Multiline = true;
            this.txt41.Name = "txt41";
            this.txt41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt41.Style.Font.Name = "Verdana";
            this.txt41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt41.Value = "";
            // 
            // txt42
            // 
            this.txt42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(41.1D));
            this.txt42.Multiline = true;
            this.txt42.Name = "txt42";
            this.txt42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt42.Style.Font.Name = "Verdana";
            this.txt42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt42.Value = "";
            // 
            // txt43
            // 
            this.txt43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(41.8D));
            this.txt43.Multiline = true;
            this.txt43.Name = "txt43";
            this.txt43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt43.Style.Font.Name = "Verdana";
            this.txt43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt43.Value = "";
            // 
            // txt44
            // 
            this.txt44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(42.5D));
            this.txt44.Multiline = true;
            this.txt44.Name = "txt44";
            this.txt44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt44.Style.Font.Name = "Verdana";
            this.txt44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt44.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt44.Value = "";
            // 
            // txt45
            // 
            this.txt45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(43.1D));
            this.txt45.Multiline = true;
            this.txt45.Name = "txt45";
            this.txt45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt45.Style.Font.Name = "Verdana";
            this.txt45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt45.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt45.Value = "";
            // 
            // txt46
            // 
            this.txt46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(43.7D));
            this.txt46.Multiline = true;
            this.txt46.Name = "txt46";
            this.txt46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt46.Style.Font.Name = "Verdana";
            this.txt46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt46.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt46.Value = "";
            // 
            // txt47
            // 
            this.txt47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(44.3D));
            this.txt47.Multiline = true;
            this.txt47.Name = "txt47";
            this.txt47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txt47.Style.Font.Name = "Verdana";
            this.txt47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txt47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt47.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txt47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txt47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt47.Value = "";
            // 
            // txtInstallerName
            // 
            this.txtInstallerName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(45.9D));
            this.txtInstallerName.Multiline = true;
            this.txtInstallerName.Name = "txtInstallerName";
            this.txtInstallerName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallerName.Style.Font.Name = "Verdana";
            this.txtInstallerName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallerName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstallerName.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtInstallerName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInstallerName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallerName.Value = "";
            // 
            // txtInstallerNo
            // 
            this.txtInstallerNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(46.9D));
            this.txtInstallerNo.Multiline = true;
            this.txtInstallerNo.Name = "txtInstallerNo";
            this.txtInstallerNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtInstallerNo.Style.Font.Name = "Verdana";
            this.txtInstallerNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallerNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstallerNo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtInstallerNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInstallerNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallerNo.Value = "";
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(48D));
            this.txtTotalCost.Multiline = true;
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtTotalCost.Style.Font.Name = "Verdana";
            this.txtTotalCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtTotalCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalCost.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtTotalCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtTotalCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalCost.Value = "";
            // 
            // txtBalOwing
            // 
            this.txtBalOwing.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(49D));
            this.txtBalOwing.Multiline = true;
            this.txtBalOwing.Name = "txtBalOwing";
            this.txtBalOwing.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtBalOwing.Style.Font.Name = "Verdana";
            this.txtBalOwing.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtBalOwing.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtBalOwing.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtBalOwing.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtBalOwing.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBalOwing.Value = "";
            // 
            // txtCPName
            // 
            this.txtCPName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.8D), Telerik.Reporting.Drawing.Unit.Cm(45.8D));
            this.txtCPName.Multiline = true;
            this.txtCPName.Name = "txtCPName";
            this.txtCPName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtCPName.Style.Font.Name = "Verdana";
            this.txtCPName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCPName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCPName.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtCPName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtCPName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCPName.Value = "";
            // 
            // txtCPContactNo
            // 
            this.txtCPContactNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.8D), Telerik.Reporting.Drawing.Unit.Cm(46.9D));
            this.txtCPContactNo.Multiline = true;
            this.txtCPContactNo.Name = "txtCPContactNo";
            this.txtCPContactNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtCPContactNo.Style.Font.Name = "Verdana";
            this.txtCPContactNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCPContactNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCPContactNo.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtCPContactNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtCPContactNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCPContactNo.Value = "";
            // 
            // txtPaidDate
            // 
            this.txtPaidDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.8D), Telerik.Reporting.Drawing.Unit.Cm(47.9D));
            this.txtPaidDate.Multiline = true;
            this.txtPaidDate.Name = "txtPaidDate";
            this.txtPaidDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtPaidDate.Style.Font.Name = "Verdana";
            this.txtPaidDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPaidDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPaidDate.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtPaidDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPaidDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPaidDate.Value = "";
            // 
            // txtInstallerNotes
            // 
            this.txtInstallerNotes.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.9D), Telerik.Reporting.Drawing.Unit.Cm(51.3D));
            this.txtInstallerNotes.Multiline = true;
            this.txtInstallerNotes.Name = "txtInstallerNotes";
            this.txtInstallerNotes.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.2D), Telerik.Reporting.Drawing.Unit.Cm(5.7D));
            this.txtInstallerNotes.Style.Font.Name = "Verdana";
            this.txtInstallerNotes.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInstallerNotes.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInstallerNotes.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtInstallerNotes.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInstallerNotes.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtInstallerNotes.Value = "";
            // 
            // Picklist
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Picklist";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox picPage1bg;
        private Telerik.Reporting.PictureBox picPage2bg;
        private Telerik.Reporting.TextBox txtProjectNo;
        private Telerik.Reporting.TextBox txtCustomerName;
        private Telerik.Reporting.TextBox txtContNo;
        private Telerik.Reporting.TextBox txtAltNo;
        private Telerik.Reporting.TextBox txtInstalledAddress;
        private Telerik.Reporting.TextBox txtApplicationNo;
        private Telerik.Reporting.TextBox txtSystemSize;
        private Telerik.Reporting.TextBox txtStruHeight;
        private Telerik.Reporting.TextBox txtInstallationDate;
        private Telerik.Reporting.TextBox txtPanelDescription;
        private Telerik.Reporting.TextBox txtInverterDescription;
        private Telerik.Reporting.TextBox txtNoPanels;
        private Telerik.Reporting.TextBox txtNoInverter;
        private Telerik.Reporting.TextBox txt3;
        private Telerik.Reporting.TextBox txt4;
        private Telerik.Reporting.TextBox txt5;
        private Telerik.Reporting.TextBox txt6;
        private Telerik.Reporting.TextBox txt7;
        private Telerik.Reporting.TextBox txt8;
        private Telerik.Reporting.TextBox txt9;
        private Telerik.Reporting.TextBox txt10;
        private Telerik.Reporting.TextBox txt11;
        private Telerik.Reporting.TextBox txt12;
        private Telerik.Reporting.TextBox txt13;
        private Telerik.Reporting.TextBox txt14;
        private Telerik.Reporting.TextBox txt15;
        private Telerik.Reporting.TextBox txt16;
        private Telerik.Reporting.TextBox txt17;
        private Telerik.Reporting.TextBox txt18;
        private Telerik.Reporting.TextBox txt19;
        private Telerik.Reporting.TextBox txt20;
        private Telerik.Reporting.TextBox txt21;
        private Telerik.Reporting.TextBox txt22;
        private Telerik.Reporting.TextBox txt23;
        private Telerik.Reporting.TextBox txt24;
        private Telerik.Reporting.TextBox txt25;
        private Telerik.Reporting.TextBox txt26;
        private Telerik.Reporting.TextBox txt27;
        private Telerik.Reporting.TextBox txt28;
        private Telerik.Reporting.TextBox txt29;
        private Telerik.Reporting.TextBox txt30;
        private Telerik.Reporting.TextBox txt31;
        private Telerik.Reporting.TextBox txt32;
        private Telerik.Reporting.TextBox txt33;
        private Telerik.Reporting.TextBox txt34;
        private Telerik.Reporting.TextBox txt35;
        private Telerik.Reporting.TextBox txt36;
        private Telerik.Reporting.TextBox txt37;
        private Telerik.Reporting.TextBox txt38;
        private Telerik.Reporting.TextBox txt39;
        private Telerik.Reporting.TextBox txt40;
        private Telerik.Reporting.TextBox txt41;
        private Telerik.Reporting.TextBox txt42;
        private Telerik.Reporting.TextBox txt43;
        private Telerik.Reporting.TextBox txt44;
        private Telerik.Reporting.TextBox txt45;
        private Telerik.Reporting.TextBox txt46;
        private Telerik.Reporting.TextBox txt47;
        private Telerik.Reporting.TextBox txtInstallerName;
        private Telerik.Reporting.TextBox txtInstallerNo;
        private Telerik.Reporting.TextBox txtTotalCost;
        private Telerik.Reporting.TextBox txtBalOwing;
        private Telerik.Reporting.TextBox txtCPName;
        private Telerik.Reporting.TextBox txtCPContactNo;
        private Telerik.Reporting.TextBox txtPaidDate;
        private Telerik.Reporting.TextBox txtInstallerNotes;
    }
}