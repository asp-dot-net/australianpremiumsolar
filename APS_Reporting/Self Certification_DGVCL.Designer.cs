namespace APS_Reporting
{
    partial class Self_Certification_DGVCL
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Self_Certification_DGVCL));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.PicPage1 = new Telerik.Reporting.PictureBox();
            this.PicPage2 = new Telerik.Reporting.PictureBox();
            this.picPage3 = new Telerik.Reporting.PictureBox();
            this.picPage4 = new Telerik.Reporting.PictureBox();
            this.picPage5 = new Telerik.Reporting.PictureBox();
            this.txtConsumerNo = new Telerik.Reporting.TextBox();
            this.txtGUVNLNo = new Telerik.Reporting.TextBox();
            this.txtCustomer = new Telerik.Reporting.TextBox();
            this.txtConsumerNoBSI = new Telerik.Reporting.TextBox();
            this.txtCircle = new Telerik.Reporting.TextBox();
            this.txtDivision = new Telerik.Reporting.TextBox();
            this.txtSubDivision = new Telerik.Reporting.TextBox();
            this.txtAddress1 = new Telerik.Reporting.TextBox();
            this.txtAddress2 = new Telerik.Reporting.TextBox();
            this.txtLoad = new Telerik.Reporting.TextBox();
            this.txtKV = new Telerik.Reporting.TextBox();
            this.txtKW1 = new Telerik.Reporting.TextBox();
            this.txtPhase1 = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage2 = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage3 = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage4 = new Telerik.Reporting.TextBox();
            this.txtConsumerNoPage5 = new Telerik.Reporting.TextBox();
            this.txt1 = new Telerik.Reporting.TextBox();
            this.txtSolarPV = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.txtModelNo = new Telerik.Reporting.TextBox();
            this.txtSeries = new Telerik.Reporting.TextBox();
            this.txtStockSize = new Telerik.Reporting.TextBox();
            this.txtNoPanel = new Telerik.Reporting.TextBox();
            this.txtTotalKW = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.txtInverterName = new Telerik.Reporting.TextBox();
            this.txtInverterModelNo = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.txtInverterPhase = new Telerik.Reporting.TextBox();
            this.txtInverterKW = new Telerik.Reporting.TextBox();
            this.txtInverterSerialNo = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.txtPanelName = new Telerik.Reporting.TextBox();
            this.txtDisComName = new Telerik.Reporting.TextBox();
            this.txtDiscomName2 = new Telerik.Reporting.TextBox();
            this.txtDiscomName3 = new Telerik.Reporting.TextBox();
            this.txtDiscomName4 = new Telerik.Reporting.TextBox();
            this.txtDiscomName5 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1485D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.PicPage1,
            this.PicPage2,
            this.picPage3,
            this.picPage4,
            this.picPage5,
            this.txtConsumerNo,
            this.txtGUVNLNo,
            this.txtCustomer,
            this.txtConsumerNoBSI,
            this.txtCircle,
            this.txtDivision,
            this.txtSubDivision,
            this.txtAddress1,
            this.txtAddress2,
            this.txtLoad,
            this.txtKV,
            this.txtKW1,
            this.txtPhase1,
            this.txtConsumerNoPage2,
            this.txtConsumerNoPage3,
            this.txtConsumerNoPage4,
            this.txtConsumerNoPage5,
            this.txt1,
            this.txtSolarPV,
            this.textBox1,
            this.textBox2,
            this.txtModelNo,
            this.txtSeries,
            this.txtStockSize,
            this.txtNoPanel,
            this.txtTotalKW,
            this.textBox3,
            this.textBox4,
            this.txtInverterName,
            this.txtInverterModelNo,
            this.textBox5,
            this.txtInverterPhase,
            this.txtInverterKW,
            this.txtInverterSerialNo,
            this.textBox6,
            this.txtPanelName,
            this.txtDisComName,
            this.txtDiscomName2,
            this.txtDiscomName3,
            this.txtDiscomName4,
            this.txtDiscomName5});
            this.detail.Name = "detail";
            // 
            // PicPage1
            // 
            this.PicPage1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PicPage1.MimeType = "image/jpeg";
            this.PicPage1.Name = "PicPage1";
            this.PicPage1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.PicPage1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicPage1.Value = ((object)(resources.GetObject("PicPage1.Value")));
            // 
            // PicPage2
            // 
            this.PicPage2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.PicPage2.MimeType = "image/jpeg";
            this.PicPage2.Name = "PicPage2";
            this.PicPage2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.PicPage2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicPage2.Value = ((object)(resources.GetObject("PicPage2.Value")));
            // 
            // picPage3
            // 
            this.picPage3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.4D));
            this.picPage3.MimeType = "image/jpeg";
            this.picPage3.Name = "picPage3";
            this.picPage3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picPage3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picPage3.Value = ((object)(resources.GetObject("picPage3.Value")));
            // 
            // picPage4
            // 
            this.picPage4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1D));
            this.picPage4.MimeType = "image/jpeg";
            this.picPage4.Name = "picPage4";
            this.picPage4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picPage4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picPage4.Value = ((object)(resources.GetObject("picPage4.Value")));
            // 
            // picPage5
            // 
            this.picPage5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.8D));
            this.picPage5.MimeType = "image/jpeg";
            this.picPage5.Name = "picPage5";
            this.picPage5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picPage5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picPage5.Value = ((object)(resources.GetObject("picPage5.Value")));
            // 
            // txtConsumerNo
            // 
            this.txtConsumerNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(2.492D));
            this.txtConsumerNo.Name = "txtConsumerNo";
            this.txtConsumerNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.933D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNo.Style.Font.Name = "Verdana";
            this.txtConsumerNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNo.Value = "";
            // 
            // txtGUVNLNo
            // 
            this.txtGUVNLNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.959D), Telerik.Reporting.Drawing.Unit.Cm(5.692D));
            this.txtGUVNLNo.Name = "txtGUVNLNo";
            this.txtGUVNLNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.067D), Telerik.Reporting.Drawing.Unit.Cm(0.467D));
            this.txtGUVNLNo.Style.Font.Name = "Verdana";
            this.txtGUVNLNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtGUVNLNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtGUVNLNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtGUVNLNo.Value = "";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.026D), Telerik.Reporting.Drawing.Unit.Cm(6.626D));
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(0.467D));
            this.txtCustomer.Style.Font.Name = "Verdana";
            this.txtCustomer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustomer.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtCustomer.Value = "";
            // 
            // txtConsumerNoBSI
            // 
            this.txtConsumerNoBSI.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.426D), Telerik.Reporting.Drawing.Unit.Cm(7.426D));
            this.txtConsumerNoBSI.Name = "txtConsumerNoBSI";
            this.txtConsumerNoBSI.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.467D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoBSI.Style.Font.Name = "Verdana";
            this.txtConsumerNoBSI.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoBSI.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoBSI.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoBSI.Value = "";
            // 
            // txtCircle
            // 
            this.txtCircle.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.826D), Telerik.Reporting.Drawing.Unit.Cm(7.426D));
            this.txtCircle.Name = "txtCircle";
            this.txtCircle.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.474D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtCircle.Style.Font.Name = "Verdana";
            this.txtCircle.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCircle.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCircle.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtCircle.TextWrap = false;
            this.txtCircle.Value = "";
            // 
            // txtDivision
            // 
            this.txtDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.2D), Telerik.Reporting.Drawing.Unit.Cm(7.426D));
            this.txtDivision.Name = "txtDivision";
            this.txtDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtDivision.Style.Font.Name = "Verdana";
            this.txtDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtDivision.TextWrap = false;
            this.txtDivision.Value = "";
            // 
            // txtSubDivision
            // 
            this.txtSubDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.359D), Telerik.Reporting.Drawing.Unit.Cm(7.426D));
            this.txtSubDivision.Name = "txtSubDivision";
            this.txtSubDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtSubDivision.Style.Font.Name = "Verdana";
            this.txtSubDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSubDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSubDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtSubDivision.Value = "";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.559D), Telerik.Reporting.Drawing.Unit.Cm(8.159D));
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.533D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress1.Style.Font.Name = "Verdana";
            this.txtAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtAddress1.Value = "";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.559D), Telerik.Reporting.Drawing.Unit.Cm(8.892D));
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.533D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress2.Style.Font.Name = "Verdana";
            this.txtAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtAddress2.Value = "";
            // 
            // txtLoad
            // 
            this.txtLoad.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8D), Telerik.Reporting.Drawing.Unit.Cm(9.759D));
            this.txtLoad.Name = "txtLoad";
            this.txtLoad.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtLoad.Style.Font.Name = "Verdana";
            this.txtLoad.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtLoad.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtLoad.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtLoad.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtLoad.Value = "";
            // 
            // txtKV
            // 
            this.txtKV.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.4D), Telerik.Reporting.Drawing.Unit.Cm(9.811D));
            this.txtKV.Name = "txtKV";
            this.txtKV.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtKV.Style.Font.Name = "Verdana";
            this.txtKV.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKV.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtKV.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKV.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtKV.Value = "";
            // 
            // txtKW1
            // 
            this.txtKW1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(10.6D));
            this.txtKW1.Name = "txtKW1";
            this.txtKW1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtKW1.Style.Font.Name = "Verdana";
            this.txtKW1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKW1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtKW1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKW1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtKW1.Value = "";
            // 
            // txtPhase1
            // 
            this.txtPhase1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.8D), Telerik.Reporting.Drawing.Unit.Cm(10.6D));
            this.txtPhase1.Name = "txtPhase1";
            this.txtPhase1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtPhase1.Style.Font.Name = "Verdana";
            this.txtPhase1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPhase1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPhase1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtPhase1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtPhase1.Value = "";
            // 
            // txtConsumerNoPage2
            // 
            this.txtConsumerNoPage2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(32.2D));
            this.txtConsumerNoPage2.Name = "txtConsumerNoPage2";
            this.txtConsumerNoPage2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoPage2.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage2.Value = "";
            // 
            // txtConsumerNoPage3
            // 
            this.txtConsumerNoPage3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(61.9D));
            this.txtConsumerNoPage3.Name = "txtConsumerNoPage3";
            this.txtConsumerNoPage3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoPage3.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage3.Value = "";
            // 
            // txtConsumerNoPage4
            // 
            this.txtConsumerNoPage4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(92.1D));
            this.txtConsumerNoPage4.Name = "txtConsumerNoPage4";
            this.txtConsumerNoPage4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoPage4.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage4.Value = "";
            // 
            // txtConsumerNoPage5
            // 
            this.txtConsumerNoPage5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.7D), Telerik.Reporting.Drawing.Unit.Cm(121.8D));
            this.txtConsumerNoPage5.Name = "txtConsumerNoPage5";
            this.txtConsumerNoPage5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtConsumerNoPage5.Style.Font.Name = "Verdana";
            this.txtConsumerNoPage5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNoPage5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtConsumerNoPage5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNoPage5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtConsumerNoPage5.Value = "";
            // 
            // txt1
            // 
            this.txt1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(14.1D));
            this.txt1.Name = "txt1";
            this.txt1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.txt1.Style.BackgroundColor = System.Drawing.Color.White;
            this.txt1.Style.Color = System.Drawing.Color.Black;
            this.txt1.Style.Font.Name = "Verdana";
            this.txt1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txt1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txt1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txt1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txt1.Value = "";
            // 
            // txtSolarPV
            // 
            this.txtSolarPV.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtSolarPV.Name = "txtSolarPV";
            this.txtSolarPV.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtSolarPV.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.txtSolarPV.Style.Color = System.Drawing.Color.Black;
            this.txtSolarPV.Style.Font.Name = "Verdana";
            this.txtSolarPV.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSolarPV.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSolarPV.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSolarPV.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSolarPV.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(14.1D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox1.Style.Color = System.Drawing.Color.Black;
            this.textBox1.Style.Font.Name = "Verdana";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(14.6D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox2.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox2.Style.Color = System.Drawing.Color.Black;
            this.textBox2.Style.Font.Name = "Verdana";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "";
            // 
            // txtModelNo
            // 
            this.txtModelNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtModelNo.Name = "txtModelNo";
            this.txtModelNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtModelNo.Style.Font.Name = "Verdana";
            this.txtModelNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtModelNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtModelNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtModelNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtModelNo.Value = "";
            // 
            // txtSeries
            // 
            this.txtSeries.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.1D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtSeries.Name = "txtSeries";
            this.txtSeries.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtSeries.Style.Font.Name = "Verdana";
            this.txtSeries.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtSeries.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtSeries.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.txtSeries.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtSeries.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtSeries.Value = "";
            // 
            // txtStockSize
            // 
            this.txtStockSize.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtStockSize.Name = "txtStockSize";
            this.txtStockSize.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtStockSize.Style.Font.Name = "Verdana";
            this.txtStockSize.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtStockSize.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtStockSize.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtStockSize.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtStockSize.Value = "";
            // 
            // txtNoPanel
            // 
            this.txtNoPanel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.6D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtNoPanel.Name = "txtNoPanel";
            this.txtNoPanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtNoPanel.Style.Font.Name = "Verdana";
            this.txtNoPanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtNoPanel.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtNoPanel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtNoPanel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNoPanel.Value = "";
            // 
            // txtTotalKW
            // 
            this.txtTotalKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtTotalKW.Name = "txtTotalKW";
            this.txtTotalKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtTotalKW.Style.Font.Name = "Verdana";
            this.txtTotalKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtTotalKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtTotalKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalKW.Value = "";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(16.6D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.Font.Name = "Verdana";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Yes";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(21.1D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox4.Style.Font.Name = "Verdana";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Yes";
            // 
            // txtInverterName
            // 
            this.txtInverterName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.txtInverterName.Name = "txtInverterName";
            this.txtInverterName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.txtInverterName.Style.Font.Name = "Verdana";
            this.txtInverterName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterName.Value = "";
            // 
            // txtInverterModelNo
            // 
            this.txtInverterModelNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.txtInverterModelNo.Name = "txtInverterModelNo";
            this.txtInverterModelNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.txtInverterModelNo.Style.Font.Name = "Verdana";
            this.txtInverterModelNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterModelNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterModelNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterModelNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterModelNo.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(19.9D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.Font.Name = "Verdana";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "";
            // 
            // txtInverterPhase
            // 
            this.txtInverterPhase.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.txtInverterPhase.Name = "txtInverterPhase";
            this.txtInverterPhase.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.txtInverterPhase.Style.Font.Name = "Verdana";
            this.txtInverterPhase.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterPhase.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterPhase.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtInverterPhase.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterPhase.Value = "";
            // 
            // txtInverterKW
            // 
            this.txtInverterKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.1D), Telerik.Reporting.Drawing.Unit.Cm(19.8D));
            this.txtInverterKW.Name = "txtInverterKW";
            this.txtInverterKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtInverterKW.Style.Font.Name = "Verdana";
            this.txtInverterKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtInverterKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterKW.Value = "";
            // 
            // txtInverterSerialNo
            // 
            this.txtInverterSerialNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.2D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.txtInverterSerialNo.Name = "txtInverterSerialNo";
            this.txtInverterSerialNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.txtInverterSerialNo.Style.Font.Name = "Verdana";
            this.txtInverterSerialNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtInverterSerialNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterSerialNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtInverterSerialNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterSerialNo.Value = "";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox6.Style.Font.Name = "Verdana";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "";
            // 
            // txtPanelName
            // 
            this.txtPanelName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.txtPanelName.Name = "txtPanelName";
            this.txtPanelName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.txtPanelName.Style.Font.Name = "Verdana";
            this.txtPanelName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtPanelName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.txtPanelName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelName.Value = "";
            // 
            // txtDisComName
            // 
            this.txtDisComName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(2.4D));
            this.txtDisComName.Name = "txtDisComName";
            this.txtDisComName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDisComName.Style.BackgroundColor = System.Drawing.Color.Black;
            this.txtDisComName.Style.Color = System.Drawing.Color.White;
            this.txtDisComName.Style.Font.Bold = true;
            this.txtDisComName.Style.Font.Name = "Verdana";
            this.txtDisComName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDisComName.Style.Font.Underline = true;
            this.txtDisComName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDisComName.Value = "";
            // 
            // txtDiscomName2
            // 
            this.txtDiscomName2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(32.1D));
            this.txtDiscomName2.Name = "txtDiscomName2";
            this.txtDiscomName2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDiscomName2.Style.BackgroundColor = System.Drawing.Color.Black;
            this.txtDiscomName2.Style.Color = System.Drawing.Color.White;
            this.txtDiscomName2.Style.Font.Bold = true;
            this.txtDiscomName2.Style.Font.Name = "Verdana";
            this.txtDiscomName2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDiscomName2.Style.Font.Underline = true;
            this.txtDiscomName2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName2.Value = "";
            // 
            // txtDiscomName3
            // 
            this.txtDiscomName3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(61.8D));
            this.txtDiscomName3.Name = "txtDiscomName3";
            this.txtDiscomName3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDiscomName3.Style.BackgroundColor = System.Drawing.Color.Black;
            this.txtDiscomName3.Style.Color = System.Drawing.Color.White;
            this.txtDiscomName3.Style.Font.Bold = true;
            this.txtDiscomName3.Style.Font.Name = "Verdana";
            this.txtDiscomName3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDiscomName3.Style.Font.Underline = true;
            this.txtDiscomName3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName3.Value = "";
            // 
            // txtDiscomName4
            // 
            this.txtDiscomName4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(92D));
            this.txtDiscomName4.Name = "txtDiscomName4";
            this.txtDiscomName4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDiscomName4.Style.BackgroundColor = System.Drawing.Color.Black;
            this.txtDiscomName4.Style.Color = System.Drawing.Color.White;
            this.txtDiscomName4.Style.Font.Bold = true;
            this.txtDiscomName4.Style.Font.Name = "Verdana";
            this.txtDiscomName4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDiscomName4.Style.Font.Underline = true;
            this.txtDiscomName4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDiscomName4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName4.Value = "";
            // 
            // txtDiscomName5
            // 
            this.txtDiscomName5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(121.7D));
            this.txtDiscomName5.Name = "txtDiscomName5";
            this.txtDiscomName5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtDiscomName5.Style.BackgroundColor = System.Drawing.Color.Black;
            this.txtDiscomName5.Style.Color = System.Drawing.Color.White;
            this.txtDiscomName5.Style.Font.Bold = true;
            this.txtDiscomName5.Style.Font.Name = "Verdana";
            this.txtDiscomName5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDiscomName5.Style.Font.Underline = true;
            this.txtDiscomName5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDiscomName5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDiscomName5.Value = "";
            // 
            // Self_Certification_DGVCL
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Self_Certification_DGVCL";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox PicPage1;
        private Telerik.Reporting.PictureBox PicPage2;
        private Telerik.Reporting.PictureBox picPage3;
        private Telerik.Reporting.PictureBox picPage4;
        private Telerik.Reporting.PictureBox picPage5;
        private Telerik.Reporting.TextBox txtConsumerNo;
        private Telerik.Reporting.TextBox txtGUVNLNo;
        private Telerik.Reporting.TextBox txtCustomer;
        private Telerik.Reporting.TextBox txtConsumerNoBSI;
        private Telerik.Reporting.TextBox txtCircle;
        private Telerik.Reporting.TextBox txtDivision;
        private Telerik.Reporting.TextBox txtSubDivision;
        private Telerik.Reporting.TextBox txtAddress1;
        private Telerik.Reporting.TextBox txtAddress2;
        private Telerik.Reporting.TextBox txtLoad;
        private Telerik.Reporting.TextBox txtKV;
        private Telerik.Reporting.TextBox txtKW1;
        private Telerik.Reporting.TextBox txtPhase1;
        private Telerik.Reporting.TextBox txtConsumerNoPage2;
        private Telerik.Reporting.TextBox txtConsumerNoPage3;
        private Telerik.Reporting.TextBox txtConsumerNoPage4;
        private Telerik.Reporting.TextBox txtConsumerNoPage5;
        private Telerik.Reporting.TextBox txt1;
        private Telerik.Reporting.TextBox txtSolarPV;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox txtModelNo;
        private Telerik.Reporting.TextBox txtSeries;
        private Telerik.Reporting.TextBox txtStockSize;
        private Telerik.Reporting.TextBox txtNoPanel;
        private Telerik.Reporting.TextBox txtTotalKW;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox txtInverterName;
        private Telerik.Reporting.TextBox txtInverterModelNo;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox txtInverterPhase;
        private Telerik.Reporting.TextBox txtInverterKW;
        private Telerik.Reporting.TextBox txtInverterSerialNo;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox txtPanelName;
        private Telerik.Reporting.TextBox txtDisComName;
        private Telerik.Reporting.TextBox txtDiscomName2;
        private Telerik.Reporting.TextBox txtDiscomName3;
        private Telerik.Reporting.TextBox txtDiscomName4;
        private Telerik.Reporting.TextBox txtDiscomName5;
    }
}