namespace APS_Reporting
{
    partial class Agreement_Torrent
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agreement_Torrent));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.Pic1 = new Telerik.Reporting.PictureBox();
            this.Pic2 = new Telerik.Reporting.PictureBox();
            this.Pic3 = new Telerik.Reporting.PictureBox();
            this.Pic4 = new Telerik.Reporting.PictureBox();
            this.Pic5 = new Telerik.Reporting.PictureBox();
            this.txtAddress = new Telerik.Reporting.TextBox();
            this.txtAddress2 = new Telerik.Reporting.TextBox();
            this.txtCustomer = new Telerik.Reporting.TextBox();
            this.txtConsumerNo = new Telerik.Reporting.TextBox();
            this.txtCustomer2 = new Telerik.Reporting.TextBox();
            this.txtAddress3 = new Telerik.Reporting.TextBox();
            this.txtAddress4 = new Telerik.Reporting.TextBox();
            this.txtKW = new Telerik.Reporting.TextBox();
            this.txtKW2 = new Telerik.Reporting.TextBox();
            this.txtKW4 = new Telerik.Reporting.TextBox();
            this.txtCustomer5 = new Telerik.Reporting.TextBox();
            this.txtDivision = new Telerik.Reporting.TextBox();
            this.txtConsumerNo2 = new Telerik.Reporting.TextBox();
            this.txtCustomer3 = new Telerik.Reporting.TextBox();
            this.txtCustMobileNo = new Telerik.Reporting.TextBox();
            this.txtCustEmail = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1485D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.Pic1,
            this.Pic2,
            this.Pic3,
            this.Pic4,
            this.Pic5,
            this.txtAddress,
            this.txtAddress2,
            this.txtCustomer,
            this.txtConsumerNo,
            this.txtCustomer2,
            this.txtAddress3,
            this.txtAddress4,
            this.txtKW,
            this.txtKW2,
            this.txtKW4,
            this.txtCustomer5,
            this.txtDivision,
            this.txtConsumerNo2,
            this.txtCustomer3,
            this.txtCustMobileNo,
            this.txtCustEmail});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Verdana";
            this.detail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // Pic1
            // 
            this.Pic1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.Pic1.MimeType = "image/jpeg";
            this.Pic1.Name = "Pic1";
            this.Pic1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic1.Style.Font.Name = "Verdana";
            this.Pic1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Pic1.Value = ((object)(resources.GetObject("Pic1.Value")));
            // 
            // Pic2
            // 
            this.Pic2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic2.MimeType = "image/jpeg";
            this.Pic2.Name = "Pic2";
            this.Pic2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic2.Style.Font.Name = "Verdana";
            this.Pic2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Pic2.Value = ((object)(resources.GetObject("Pic2.Value")));
            // 
            // Pic3
            // 
            this.Pic3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.4D));
            this.Pic3.MimeType = "image/jpeg";
            this.Pic3.Name = "Pic3";
            this.Pic3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic3.Style.Font.Name = "Verdana";
            this.Pic3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Pic3.Value = ((object)(resources.GetObject("Pic3.Value")));
            // 
            // Pic4
            // 
            this.Pic4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1D));
            this.Pic4.MimeType = "image/jpeg";
            this.Pic4.Name = "Pic4";
            this.Pic4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic4.Style.Font.Name = "Verdana";
            this.Pic4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Pic4.Value = ((object)(resources.GetObject("Pic4.Value")));
            // 
            // Pic5
            // 
            this.Pic5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.8D));
            this.Pic5.MimeType = "image/jpeg";
            this.Pic5.Name = "Pic5";
            this.Pic5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.Pic5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.Pic5.Style.Font.Name = "Verdana";
            this.Pic5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Pic5.Value = ((object)(resources.GetObject("Pic5.Value")));
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.959D), Telerik.Reporting.Drawing.Unit.Cm(6.8D));
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.txtAddress.Style.Font.Name = "Verdana";
            this.txtAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress.TextWrap = false;
            this.txtAddress.Value = "";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.959D), Telerik.Reporting.Drawing.Unit.Cm(7.267D));
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.067D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtAddress2.Style.Font.Name = "Verdana";
            this.txtAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtAddress2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress2.TextWrap = false;
            this.txtAddress2.Value = "";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.959D), Telerik.Reporting.Drawing.Unit.Cm(6.3D));
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.2D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtCustomer.Style.Font.Name = "Verdana";
            this.txtCustomer.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustomer.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomer.Value = "";
            // 
            // txtConsumerNo
            // 
            this.txtConsumerNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.626D), Telerik.Reporting.Drawing.Unit.Cm(5.333D));
            this.txtConsumerNo.Name = "txtConsumerNo";
            this.txtConsumerNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtConsumerNo.Style.Font.Name = "Verdana";
            this.txtConsumerNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtConsumerNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtConsumerNo.Value = "";
            // 
            // txtCustomer2
            // 
            this.txtCustomer2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.759D), Telerik.Reporting.Drawing.Unit.Cm(10.467D));
            this.txtCustomer2.Name = "txtCustomer2";
            this.txtCustomer2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.667D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtCustomer2.Style.Font.Name = "Verdana";
            this.txtCustomer2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustomer2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomer2.TextWrap = false;
            this.txtCustomer2.Value = "";
            // 
            // txtAddress3
            // 
            this.txtAddress3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.026D), Telerik.Reporting.Drawing.Unit.Cm(10.933D));
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.267D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtAddress3.Style.Font.Name = "Verdana";
            this.txtAddress3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtAddress3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress3.TextWrap = false;
            this.txtAddress3.Value = "";
            // 
            // txtAddress4
            // 
            this.txtAddress4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.892D), Telerik.Reporting.Drawing.Unit.Cm(11.4D));
            this.txtAddress4.Name = "txtAddress4";
            this.txtAddress4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.933D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtAddress4.Style.Font.Name = "Verdana";
            this.txtAddress4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtAddress4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtAddress4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress4.TextWrap = false;
            this.txtAddress4.Value = "";
            // 
            // txtKW
            // 
            this.txtKW.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.226D), Telerik.Reporting.Drawing.Unit.Cm(10.867D));
            this.txtKW.Name = "txtKW";
            this.txtKW.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtKW.Style.Font.Name = "Verdana";
            this.txtKW.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKW.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKW.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW.Value = "";
            // 
            // txtKW2
            // 
            this.txtKW2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(13.2D));
            this.txtKW2.Name = "txtKW2";
            this.txtKW2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtKW2.Style.Font.Name = "Verdana";
            this.txtKW2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKW2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKW2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW2.Value = "";
            // 
            // txtKW4
            // 
            this.txtKW4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.3D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
            this.txtKW4.Name = "txtKW4";
            this.txtKW4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.533D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtKW4.Style.Font.Name = "Verdana";
            this.txtKW4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtKW4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtKW4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtKW4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtKW4.Value = "";
            // 
            // txtCustomer5
            // 
            this.txtCustomer5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.959D), Telerik.Reporting.Drawing.Unit.Cm(106.533D));
            this.txtCustomer5.Name = "txtCustomer5";
            this.txtCustomer5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.267D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtCustomer5.Style.Font.Name = "Verdana";
            this.txtCustomer5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustomer5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomer5.TextWrap = false;
            this.txtCustomer5.Value = "";
            // 
            // txtDivision
            // 
            this.txtDivision.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.492D), Telerik.Reporting.Drawing.Unit.Cm(5.333D));
            this.txtDivision.Name = "txtDivision";
            this.txtDivision.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtDivision.Style.Font.Name = "Verdana";
            this.txtDivision.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtDivision.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtDivision.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtDivision.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDivision.TextWrap = false;
            this.txtDivision.Value = "";
            // 
            // txtConsumerNo2
            // 
            this.txtConsumerNo2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.226D), Telerik.Reporting.Drawing.Unit.Cm(122.2D));
            this.txtConsumerNo2.Name = "txtConsumerNo2";
            this.txtConsumerNo2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.133D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtConsumerNo2.Style.Font.Name = "Verdana";
            this.txtConsumerNo2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtConsumerNo2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtConsumerNo2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtConsumerNo2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtConsumerNo2.TextWrap = false;
            this.txtConsumerNo2.Value = "";
            // 
            // txtCustomer3
            // 
            this.txtCustomer3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.226D), Telerik.Reporting.Drawing.Unit.Cm(123D));
            this.txtCustomer3.Name = "txtCustomer3";
            this.txtCustomer3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.133D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtCustomer3.Style.Font.Name = "Verdana";
            this.txtCustomer3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustomer3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustomer3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustomer3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustomer3.TextWrap = false;
            this.txtCustomer3.Value = "";
            // 
            // txtCustMobileNo
            // 
            this.txtCustMobileNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.226D), Telerik.Reporting.Drawing.Unit.Cm(131.067D));
            this.txtCustMobileNo.Name = "txtCustMobileNo";
            this.txtCustMobileNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.133D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtCustMobileNo.Style.Font.Name = "Verdana";
            this.txtCustMobileNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustMobileNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustMobileNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustMobileNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustMobileNo.TextWrap = false;
            this.txtCustMobileNo.Value = "";
            // 
            // txtCustEmail
            // 
            this.txtCustEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.226D), Telerik.Reporting.Drawing.Unit.Cm(132.667D));
            this.txtCustEmail.Name = "txtCustEmail";
            this.txtCustEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.133D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtCustEmail.Style.Font.Name = "Verdana";
            this.txtCustEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txtCustEmail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtCustEmail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtCustEmail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustEmail.TextWrap = false;
            this.txtCustEmail.Value = "";
            // 
            // Agreement_Torrent
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Agreement_Torrent";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox Pic1;
        private Telerik.Reporting.PictureBox Pic2;
        private Telerik.Reporting.PictureBox Pic3;
        private Telerik.Reporting.PictureBox Pic4;
        private Telerik.Reporting.PictureBox Pic5;
        private Telerik.Reporting.TextBox txtAddress;
        private Telerik.Reporting.TextBox txtAddress2;
        private Telerik.Reporting.TextBox txtCustomer;
        private Telerik.Reporting.TextBox txtConsumerNo;
        private Telerik.Reporting.TextBox txtCustomer2;
        private Telerik.Reporting.TextBox txtAddress3;
        private Telerik.Reporting.TextBox txtAddress4;
        private Telerik.Reporting.TextBox txtKW;
        private Telerik.Reporting.TextBox txtKW2;
        private Telerik.Reporting.TextBox txtKW4;
        private Telerik.Reporting.TextBox txtCustomer5;
        private Telerik.Reporting.TextBox txtDivision;
        private Telerik.Reporting.TextBox txtConsumerNo2;
        private Telerik.Reporting.TextBox txtCustomer3;
        private Telerik.Reporting.TextBox txtCustMobileNo;
        private Telerik.Reporting.TextBox txtCustEmail;
    }
}