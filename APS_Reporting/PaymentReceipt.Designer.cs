namespace APS_Reporting
{
    partial class PaymentReceipt
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentReceipt));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.picBg1 = new Telerik.Reporting.PictureBox();
            this.txtProjectNo = new Telerik.Reporting.TextBox();
            this.txtDate = new Telerik.Reporting.TextBox();
            this.txtCustName = new Telerik.Reporting.TextBox();
            this.txtCustMobile = new Telerik.Reporting.TextBox();
            this.txtCustPhone = new Telerik.Reporting.TextBox();
            this.txtCustEmail = new Telerik.Reporting.TextBox();
            this.txtAddress1 = new Telerik.Reporting.TextBox();
            this.txtAddress2 = new Telerik.Reporting.TextBox();
            this.txtPanelName = new Telerik.Reporting.TextBox();
            this.txtPanels = new Telerik.Reporting.TextBox();
            this.txtInverter = new Telerik.Reporting.TextBox();
            this.txtInverterName = new Telerik.Reporting.TextBox();
            this.txtTotalCost = new Telerik.Reporting.TextBox();
            this.txtLessSubsidy = new Telerik.Reporting.TextBox();
            this.txtNetCost = new Telerik.Reporting.TextBox();
            this.txtPaidtoDate = new Telerik.Reporting.TextBox();
            this.txtBalanceDue = new Telerik.Reporting.TextBox();
            this.txtBalanceDua2 = new Telerik.Reporting.TextBox();
            this.tblPaymentDetails = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.picBg1,
            this.txtProjectNo,
            this.txtDate,
            this.txtCustName,
            this.txtCustMobile,
            this.txtCustPhone,
            this.txtCustEmail,
            this.txtAddress1,
            this.txtAddress2,
            this.txtPanelName,
            this.txtPanels,
            this.txtInverter,
            this.txtInverterName,
            this.txtTotalCost,
            this.txtLessSubsidy,
            this.txtNetCost,
            this.txtPaidtoDate,
            this.txtBalanceDue,
            this.txtBalanceDua2,
            this.tblPaymentDetails});
            this.detail.Name = "detail";
            // 
            // picBg1
            // 
            this.picBg1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.picBg1.MimeType = "image/jpeg";
            this.picBg1.Name = "picBg1";
            this.picBg1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.picBg1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picBg1.Value = ((object)(resources.GetObject("picBg1.Value")));
            // 
            // txtProjectNo
            // 
            this.txtProjectNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.492D), Telerik.Reporting.Drawing.Unit.Cm(2.9D));
            this.txtProjectNo.Name = "txtProjectNo";
            this.txtProjectNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.326D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtProjectNo.Style.Font.Name = "Verdana";
            this.txtProjectNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtProjectNo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtProjectNo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNo.Value = "";
            // 
            // txtDate
            // 
            this.txtDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.026D), Telerik.Reporting.Drawing.Unit.Cm(2.892D));
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.267D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtDate.Style.Font.Name = "Verdana";
            this.txtDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDate.Value = "";
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.959D), Telerik.Reporting.Drawing.Unit.Cm(5.359D));
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.867D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtCustName.Style.Font.Name = "Verdana";
            this.txtCustName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCustName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustName.Value = "";
            // 
            // txtCustMobile
            // 
            this.txtCustMobile.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.959D), Telerik.Reporting.Drawing.Unit.Cm(6.226D));
            this.txtCustMobile.Name = "txtCustMobile";
            this.txtCustMobile.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.867D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtCustMobile.Style.Font.Name = "Verdana";
            this.txtCustMobile.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCustMobile.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustMobile.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustMobile.Value = "";
            // 
            // txtCustPhone
            // 
            this.txtCustPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.692D), Telerik.Reporting.Drawing.Unit.Cm(5.359D));
            this.txtCustPhone.Name = "txtCustPhone";
            this.txtCustPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.867D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtCustPhone.Style.Font.Name = "Verdana";
            this.txtCustPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCustPhone.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustPhone.Value = "";
            // 
            // txtCustEmail
            // 
            this.txtCustEmail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.692D), Telerik.Reporting.Drawing.Unit.Cm(6.226D));
            this.txtCustEmail.Name = "txtCustEmail";
            this.txtCustEmail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.867D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtCustEmail.Style.Font.Name = "Verdana";
            this.txtCustEmail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtCustEmail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtCustEmail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtCustEmail.Value = "";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.959D), Telerik.Reporting.Drawing.Unit.Cm(9.292D));
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtAddress1.Style.Font.Name = "Verdana";
            this.txtAddress1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtAddress1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress1.Value = "";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.959D), Telerik.Reporting.Drawing.Unit.Cm(9.826D));
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(0.533D));
            this.txtAddress2.Style.Font.Name = "Verdana";
            this.txtAddress2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtAddress2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtAddress2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAddress2.Value = "";
            // 
            // txtPanelName
            // 
            this.txtPanelName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.492D), Telerik.Reporting.Drawing.Unit.Cm(13.026D));
            this.txtPanelName.Name = "txtPanelName";
            this.txtPanelName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.333D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtPanelName.Style.Font.Name = "Verdana";
            this.txtPanelName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanelName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanelName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanelName.Value = "";
            // 
            // txtPanels
            // 
            this.txtPanels.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.692D), Telerik.Reporting.Drawing.Unit.Cm(13.026D));
            this.txtPanels.Name = "txtPanels";
            this.txtPanels.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.867D), Telerik.Reporting.Drawing.Unit.Cm(0.726D));
            this.txtPanels.Style.Font.Name = "Verdana";
            this.txtPanels.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPanels.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPanels.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPanels.Value = "";
            // 
            // txtInverter
            // 
            this.txtInverter.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.626D), Telerik.Reporting.Drawing.Unit.Cm(14.292D));
            this.txtInverter.Name = "txtInverter";
            this.txtInverter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.867D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtInverter.Style.Font.Name = "Verdana";
            this.txtInverter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverter.Value = "";
            // 
            // txtInverterName
            // 
            this.txtInverterName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.426D), Telerik.Reporting.Drawing.Unit.Cm(14.292D));
            this.txtInverterName.Name = "txtInverterName";
            this.txtInverterName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.4D), Telerik.Reporting.Drawing.Unit.Cm(0.667D));
            this.txtInverterName.Style.Font.Name = "Verdana";
            this.txtInverterName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtInverterName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtInverterName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInverterName.Value = "";
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.959D), Telerik.Reporting.Drawing.Unit.Cm(12.226D));
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.267D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtTotalCost.Style.Color = System.Drawing.Color.White;
            this.txtTotalCost.Style.Font.Bold = true;
            this.txtTotalCost.Style.Font.Name = "Verdana";
            this.txtTotalCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtTotalCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtTotalCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtTotalCost.Value = "";
            // 
            // txtLessSubsidy
            // 
            this.txtLessSubsidy.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.892D), Telerik.Reporting.Drawing.Unit.Cm(13.426D));
            this.txtLessSubsidy.Name = "txtLessSubsidy";
            this.txtLessSubsidy.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.333D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtLessSubsidy.Style.Font.Name = "Verdana";
            this.txtLessSubsidy.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtLessSubsidy.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtLessSubsidy.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtLessSubsidy.Value = "";
            // 
            // txtNetCost
            // 
            this.txtNetCost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.892D), Telerik.Reporting.Drawing.Unit.Cm(14.359D));
            this.txtNetCost.Name = "txtNetCost";
            this.txtNetCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.333D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.txtNetCost.Style.Font.Name = "Verdana";
            this.txtNetCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtNetCost.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtNetCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNetCost.Value = "";
            // 
            // txtPaidtoDate
            // 
            this.txtPaidtoDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.892D), Telerik.Reporting.Drawing.Unit.Cm(15.226D));
            this.txtPaidtoDate.Name = "txtPaidtoDate";
            this.txtPaidtoDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.333D), Telerik.Reporting.Drawing.Unit.Cm(0.867D));
            this.txtPaidtoDate.Style.Font.Name = "Verdana";
            this.txtPaidtoDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtPaidtoDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtPaidtoDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPaidtoDate.Value = "";
            // 
            // txtBalanceDue
            // 
            this.txtBalanceDue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.892D), Telerik.Reporting.Drawing.Unit.Cm(16.159D));
            this.txtBalanceDue.Name = "txtBalanceDue";
            this.txtBalanceDue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.333D), Telerik.Reporting.Drawing.Unit.Cm(0.867D));
            this.txtBalanceDue.Style.Font.Name = "Verdana";
            this.txtBalanceDue.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtBalanceDue.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtBalanceDue.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBalanceDue.Value = "";
            // 
            // txtBalanceDua2
            // 
            this.txtBalanceDua2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.3D), Telerik.Reporting.Drawing.Unit.Cm(22.7D));
            this.txtBalanceDua2.Name = "txtBalanceDua2";
            this.txtBalanceDua2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.txtBalanceDua2.Style.Color = System.Drawing.Color.White;
            this.txtBalanceDua2.Style.Font.Bold = true;
            this.txtBalanceDua2.Style.Font.Name = "Verdana";
            this.txtBalanceDua2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.txtBalanceDua2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.txtBalanceDua2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBalanceDua2.Value = "";
            // 
            // tblPaymentDetails
            // 
            this.tblPaymentDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.085D)));
            this.tblPaymentDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.435D)));
            this.tblPaymentDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.953D)));
            this.tblPaymentDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.897D)));
            this.tblPaymentDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.029D)));
            this.tblPaymentDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.979D)));
            this.tblPaymentDetails.Body.SetCellContent(2, 0, this.textBox2);
            this.tblPaymentDetails.Body.SetCellContent(2, 1, this.textBox4);
            this.tblPaymentDetails.Body.SetCellContent(2, 2, this.textBox6);
            this.tblPaymentDetails.Body.SetCellContent(0, 0, this.textBox1);
            this.tblPaymentDetails.Body.SetCellContent(0, 1, this.textBox3);
            this.tblPaymentDetails.Body.SetCellContent(0, 2, this.textBox5);
            this.tblPaymentDetails.Body.SetCellContent(1, 0, this.textBox7);
            this.tblPaymentDetails.Body.SetCellContent(1, 1, this.textBox8);
            this.tblPaymentDetails.Body.SetCellContent(1, 2, this.textBox9);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "tableGroup1";
            tableGroup3.Name = "tableGroup2";
            this.tblPaymentDetails.ColumnGroups.Add(tableGroup1);
            this.tblPaymentDetails.ColumnGroups.Add(tableGroup2);
            this.tblPaymentDetails.ColumnGroups.Add(tableGroup3);
            this.tblPaymentDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox4,
            this.textBox6,
            this.textBox1,
            this.textBox3,
            this.textBox5,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.tblPaymentDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(19.7D));
            this.tblPaymentDetails.Name = "tblPaymentDetails";
            tableGroup5.Name = "group1";
            tableGroup6.Name = "group2";
            tableGroup7.Name = "group";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.ChildGroups.Add(tableGroup7);
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup";
            this.tblPaymentDetails.RowGroups.Add(tableGroup4);
            this.tblPaymentDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.473D), Telerik.Reporting.Drawing.Unit.Cm(2.905D));
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.085D), Telerik.Reporting.Drawing.Unit.Cm(0.979D));
            this.textBox2.Style.Font.Name = "Verdana";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.435D), Telerik.Reporting.Drawing.Unit.Cm(0.979D));
            this.textBox4.Style.Font.Name = "Verdana";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.953D), Telerik.Reporting.Drawing.Unit.Cm(0.979D));
            this.textBox6.Style.Font.Name = "Verdana";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.085D), Telerik.Reporting.Drawing.Unit.Cm(0.897D));
            this.textBox1.Style.Font.Name = "Verdana";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "=Fields.FormattedInvoicePayDate";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.435D), Telerik.Reporting.Drawing.Unit.Cm(0.897D));
            this.textBox3.Style.Font.Name = "Verdana";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "=Fields.FormattedInvoicePayTotal";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.953D), Telerik.Reporting.Drawing.Unit.Cm(0.897D));
            this.textBox5.Style.Font.Name = "Verdana";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.InvoicePayMethod";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.085D), Telerik.Reporting.Drawing.Unit.Cm(1.029D));
            this.textBox7.Style.Font.Name = "Verdana";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.435D), Telerik.Reporting.Drawing.Unit.Cm(1.029D));
            this.textBox8.Style.Font.Name = "Verdana";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.953D), Telerik.Reporting.Drawing.Unit.Cm(1.029D));
            this.textBox9.Style.Font.Name = "Verdana";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            // 
            // PaymentReceipt
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "PaymentReceipt";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox picBg1;
        private Telerik.Reporting.TextBox txtProjectNo;
        private Telerik.Reporting.TextBox txtDate;
        private Telerik.Reporting.TextBox txtCustName;
        private Telerik.Reporting.TextBox txtCustMobile;
        private Telerik.Reporting.TextBox txtCustPhone;
        private Telerik.Reporting.TextBox txtCustEmail;
        private Telerik.Reporting.TextBox txtAddress1;
        private Telerik.Reporting.TextBox txtAddress2;
        private Telerik.Reporting.TextBox txtPanelName;
        private Telerik.Reporting.TextBox txtPanels;
        private Telerik.Reporting.TextBox txtInverter;
        private Telerik.Reporting.TextBox txtInverterName;
        private Telerik.Reporting.TextBox txtTotalCost;
        private Telerik.Reporting.TextBox txtLessSubsidy;
        private Telerik.Reporting.TextBox txtNetCost;
        private Telerik.Reporting.TextBox txtPaidtoDate;
        private Telerik.Reporting.TextBox txtBalanceDue;
        private Telerik.Reporting.TextBox txtBalanceDua2;
        private Telerik.Reporting.Table tblPaymentDetails;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
    }
}