﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="reports.ascx.cs" Inherits="includes_reports" %>
<asp:DropDownList ID="ddlReport" runat="server" aria-controls="DataTables_Table_0" class="myval"
    Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
    <asp:ListItem Value="salesreport">Sales Report</asp:ListItem>
    <asp:ListItem Value="noinstalldate">NoInstall Date</asp:ListItem>
    <asp:ListItem Value="installdate">Install Date</asp:ListItem>
    <asp:ListItem Value="panelscount">Panel Graph</asp:ListItem>
    <asp:ListItem Value="accountreceive">Account Receive</asp:ListItem>
    <asp:ListItem Value="paymentstatus">Payment Status</asp:ListItem>
    <asp:ListItem Value="weeklyreport">Weekly Report</asp:ListItem>
    <asp:ListItem Value="leadassignreport">Lead Assign Report</asp:ListItem>
    <asp:ListItem Value="stockreport">Stock Report</asp:ListItem>
    <asp:ListItem Value="leadtrackreport">Lead Track Report</asp:ListItem>
    <asp:ListItem Value="panelstock">Panel Stock Report</asp:ListItem>
    <asp:ListItem Value="rooftypecount">Roof Type Report</asp:ListItem>
    <asp:ListItem Value="capkwcount">Capacity Report</asp:ListItem>
    <asp:ListItem Value="statustrack">Status Track</asp:ListItem>
    <asp:ListItem Value="formbaydocsreport">Formbay Docs Report</asp:ListItem>
</asp:DropDownList>