<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="conversation.ascx.cs" Inherits="includes_controls_conversation" %>

<link href="../../css/style.css" rel="stylesheet" />
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<script>
    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

          function (isConfirm) {
              if (isConfirm) {
                  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  eval(defaultAction);

                  //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  return true;
              } else {
                  // swal("Cancelled", "Your imaginary file is safe :)", "error");
                  return false;
              }
          });
    }
            </script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#<%=btnAddDetail.ClientID %>').click(function () {
            formValidate();
        });
    });
    function doMyAction() {
        $('#<%=btnAddDetail.ClientID %>').click(function () {
            formValidate();
        });
    }
</script>
<%--<asp:UpdatePanel runat="server" ID="conv">
    <ContentTemplate>--%>
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            function pageLoaded() {
                $(".myvalconvert").select2({
                    //placeholder: "select",
                    allowclear: true
                });

                //$('.i-checks').iCheck({
                //    checkboxClass: 'icheckbox_square-green',
                //    radioClass: 'iradio_square-green'
                //});
            }
        </script>
 
        <section class="row m-b-md">
            <div class="col-sm-12">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate yscroll">
                        <div class="">
                            <div class="col-md-12">
                                <!-- start: FORM VALIDATION 1 PANEL -->
                                <div class="panel panel-default">
                                    <!--<div class="panel-heading">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Add Note
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-expand" href="#"><i class="icon-resize-full"></i>
                                    </a>
                                </div>
                            </div>-->
                                    <div class="lightgraybgarea">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblFirstName" runat="server" class="col-sm-12">
                                                <strong>Note </strong></asp:Label>
                                                        <div class="col-sm-12 marginbtm15">
                                                            <asp:TextBox ID="txtnotes" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                          
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtnotes" Display="Dynamic" ValidationGroup="conversation"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lblLastName" runat="server" class="col-sm-2 control-label">
                                                <strong></strong></asp:Label>
                                                        <div class="col-sm-12 marginbtm15">
                                                            <asp:DropDownList ID="ddlEmployees" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalconvert">
                                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                            </asp:DropDownList>
                                                         
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-md-6 center-text">
                                                    <div class="form-group ">
                                                        <span class="name">&nbsp;</span><span>
                                                            <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAddDetail" runat="server" OnClick="btnAddDetail_Click"
                                                                Text="Add" ValidationGroup="conversation" />
                                                            <asp:Button class="btn btn-purple resetbutton btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                                CausesValidation="false" Text="Reset" ValidationGroup="conversation" />

                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: FORM VALIDATION 1 PANEL -->
                            </div>
                        </div>

                        <div id="divInstallgrid" runat="server">
                            <div class="">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="GridView1" DataKeyNames="ConversationID" runat="server" AllowPaging="true" PagerStyle-HorizontalAlign="Right"
                                        PageSize="50" AutoGenerateColumns="false" CssClass="table table-bordered table-hover" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDeleting="GridView1_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                <ItemTemplate>
                                                    <%#Eval("Detail")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Entered By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="160px">
                                                <ItemTemplate>
                                                    <%#Eval("Employee")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="180px" HeaderStyle-CssClass="brdrgrayright">
                                                <ItemTemplate>
                                                    <%#Eval("CreateDate","{0:dd MMM yyyy HH:mm tt}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="Delete" HeaderStyle-CssClass="brdrgrayright">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server"  CssClass="btn btn-danger btn-xs" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("ConversationID")%>'  >
                                                        <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>

                                                     
                                                </ItemTemplate>
                                                <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                         <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                        <EmptyDataRowStyle Font-Bold="True" />
                                        <RowStyle CssClass="GridviewScrollItem" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>

 <!--Danger Modal Templates-->
<asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
            <!--End Danger Modal Templates-->