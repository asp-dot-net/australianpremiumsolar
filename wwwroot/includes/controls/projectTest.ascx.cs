﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Globalization;
using System.IO;
using System.Configuration;
using AjaxControlToolkit;

public partial class includes_controls_projectTest : System.Web.UI.UserControl
{
    decimal capacity;
    decimal rebate;
    decimal SystemCapacity;
    decimal stcno;
    decimal output;
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        UserControl tuc1 = (UserControl)Page.LoadControl("~/includes/controls/ProjectTest.ascx");

        PanSuccess.Visible = false;
        PanError.Visible = false;
        string ProjectID = string.Empty;
        txtOpenDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //if (ddlPanel.SelectedValue == "")
        //{
        //    txtZoneRt.Text = string.Empty;
        //}
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
            //BindInvoice();
            SttblProjects st12 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (!string.IsNullOrEmpty(st12.QuoteSent))
                txtQuoteSent.Text = Convert.ToDateTime(st12.QuoteSent).ToShortDateString();
            if (!string.IsNullOrEmpty(st12.QuoteAccepted))
                txtQuoteAcceptedQuote.Text = Convert.ToDateTime(st12.QuoteAccepted).ToShortDateString();

            if (!string.IsNullOrEmpty(st12.FDA))
            {
                txtDepositReceived.Enabled = true;
            }
            if (!string.IsNullOrEmpty(st12.DepositReceived))
            {
                txtDepositReceived.Text = st12.DepositReceived;
                txtActiveDate.Enabled = true;
            }
            if (!string.IsNullOrEmpty(st12.DepositeAmont))
            {
                decimal oth = Convert.ToDecimal(st12.DepositeAmont);
                decimal o1a = Math.Round(oth, 2);
                txtDepoReq.Text = o1a.ToString();
            }
            if (!string.IsNullOrEmpty(st12.ActiveDate))
            {
                txtActiveDate.Text = st12.ActiveDate;
            }
        }
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateSale.Visible = false;
                }
            }
            //activemethod();
            //if (Roles.IsUserInRole("SalesRep"))
            //{
            //    if (st2.DepositReceived != string.Empty)
            //    {
            //        btnUpdateSale.Visible = false;
            //    }
            //}
            if ((Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("PostInstaller")))
            {
                btnUpdateSale.Visible = true;
                Pan1.Enabled = true;
                Pan2.Enabled = true;
                Pan3.Enabled = true;
                Pan4.Enabled = false;
                Pan5.Enabled = false;
                Pan6.Enabled = false;
                Pan7.Enabled = false;
                Pan8.Enabled = false;
            }
            if ((Roles.IsUserInRole("PreInstaller")))
            {
                Pan4.Enabled = true;
            }
            //  txtqty2.Text = "0";
            //txtqty3.Text = "0";

            ListItem item5 = new ListItem();
            item5.Text = "Select";
            item5.Value = "";
            ddlSalesRep.Items.Clear();
            ddlSalesRep.Items.Add(item5);

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string SalesTeam = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }


                if (SalesTeam != string.Empty)
                {
                    ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                }
            }
            else
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            }
            //ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectByUserASC(userid);
            ddlSalesRep.DataValueField = "EmployeeID";
            ddlSalesRep.DataTextField = "fullname";
            ddlSalesRep.DataMember = "fullname";
            ddlSalesRep.DataBind();
            try
            {
                if (st2.SalesRep != null && st2.SalesRep != "")
                {
                    ddlSalesRep.SelectedValue = st2.SalesRep;
                }
                else
                {
                    ddlSalesRep.SelectedValue = st.EmployeeID;
                }
            }
            catch { }

            if (st2.ProjectTypeID == "8")
            {
                divsupplier.Visible = true;
            }
            else
            {
                divsupplier.Visible = false;
            }

            try
            {
                if (!string.IsNullOrEmpty(st2.InstallState))
                {
                    if (st2.InstallState != "VIC")
                    {
                        divVicRebate.Visible = false;
                    }
                }
            }
            catch { }
        }
        if (!IsPostBack)
        {
            BindProjectSale();
            BindInvoice();
        }
        BindQuote();


    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        //GridView1.DataSource = dt;
        //GridView1.DataBind();
    }
    public void BindProjectSale()
    {

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            // status();
            divMtce.Visible = false;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            lblpaymentstatus.Text = st.InvoiceStatus;

            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            //if (Roles.IsUserInRole("PreInstaller"))
            //{
            //    PanAddUpdate.Enabled = true;
            //}
            //if (Roles.IsUserInRole("PostInstaller"))
            //{
            //    PanAddUpdate.Enabled = false;
            //}
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlEmployee.Enabled = false;
                ddlElecDistAppBy.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            if (st.TotalQuotePrice != null && st.TotalQuotePrice != "") //7 NET TOTAL
            {
                decimal quotprice = Convert.ToDecimal(st.TotalQuotePrice);
                decimal t1 = Math.Round(quotprice, 2);
                txtnettotal.Text = t1.ToString();

            }

            if (st.TotalPayableAmount != null && st.TotalPayableAmount != "")
            {
                decimal quotprice = Convert.ToDecimal(st.TotalPayableAmount);
                decimal t1 = Math.Round(quotprice, 2);
                txttotalamt.Text = t1.ToString();

            }

            if (st.ServiceValue != null && st.ServiceValue != "")//1 ACTUAL COST
            {
                decimal quotprice = Convert.ToDecimal(st.ServiceValue);
                decimal t1 = Math.Round(quotprice, 2);
                txtRebate.Text = t1.ToString();

            }
            if (st.VarOther != null && st.VarOther != "")//6 EleStructureCost
            {
                decimal other = Convert.ToDecimal(st.VarOther);
                decimal o1 = Math.Round(other, 2);
                txtOther.Text = o1.ToString();
            }
            if (st.TwentySubsidy != null && st.TwentySubsidy != "")
            {
                decimal other = Convert.ToDecimal(st.TwentySubsidy);  //2 20 Subsidy Amount
                decimal o1 = Math.Round(other, 2);
                twntysubsidy.Text = o1.ToString();

            }

            if (st.FourtySubsidy != null && st.FourtySubsidy != "")  //40  Subsidy Amount
            {
                decimal other8 = Convert.ToDecimal(st.FourtySubsidy);
                decimal gb = Math.Round(other8, 2);
                fortysubsidy.Text = gb.ToString();
            }

            if (st.RECRebate != null && st.RECRebate != "")  //4 Actual Subsidy Amount
            {
                decimal oth = Convert.ToDecimal(st.RECRebate);
                decimal o1a = Math.Round(oth, 2);
                txtactualamt.Text = o1a.ToString();
            }
            txtInvoiceDoc.Text = st.InvoiceDoc;
            txtinvoiceno.Text = st.InvoiceNumber;
            txtInvoiceNumber.Text = st.InvoiceNumber;
            if (st.InvoiceSent != null && st.InvoiceSent != "")
            {
                string Invsent = Convert.ToDateTime(st.InvoiceSent).ToShortDateString();
                txtinvoicesent.Text = Invsent;
                txtInvoiceSentquote.Text = Invsent;
            }
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            if (dtCount.Rows.Count > 0)
            {
                lblTotalPaidAmount.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
            }
            //txttotalamt.Text = st.TotalPayableAmount;
            //txtRebate.Text = st.ServiceValue;
            BindSaleDropDown();
            if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }

            try
            {

                if (st.SalesType == string.Empty)
                {
                    rblboth.Checked = true;
                }
                if (st.SalesType == "1")
                {
                    rblonlypanel.Checked = true;
                }
                if (st.SalesType == "2")
                {
                    rblonlyinverter.Checked = true;
                }
                if (st.SalesType == "3")
                {
                    rblboth.Checked = true;
                }
            }
            catch
            {
            }

            try
            {
                //Response.Write(st.PanelBrandID);
                //Response.End();
                ddlPanel.SelectedValue = st.PanelBrandID;
            }
            catch { }

            txtPanelBrand.Text = st.PanelBrand;
            //Response.Write(st.PanelBrand);
            //Response.End();
            txtWatts.Text = st.PanelOutput;

            txtSTCMult.Text = st.STCMultiplier;
            txtPanelModel.Text = st.PanelModel;
            txtNoOfPanels.Text = st.NumberPanels;
            txtSystemCapacity.Text = st.SystemCapKW;




            //try
            //{            
            ddlInverter.SelectedValue = st.InverterDetailsID;
            if (st.InverterDetailsID != null && st.InverterDetailsID != "")
            {
                SttblStockItems sti = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                txtsize.Text = sti.StockSize;
            }
            //}
            //catch
            //{
            //}
            txtInverterBrand.Text = st.InverterBrand;
            txtSeries.Text = st.InverterSeries;
            try
            {
                ddlInverter2.SelectedValue = st.SecondInverterDetailsID;

                if (st.ThirdInverterDetailsID != "")
                {
                    ddlInverter3.SelectedValue = st.ThirdInverterDetailsID;
                }

            }
            catch
            {
            }
            txtSTCNo.Text = st.STCNumber;

            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                //try
                //{
                txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                //}
                //catch { }
            }

            txtInverterModel.Text = st.InverterModel;
            txtqty.Text = st.inverterqty;
            //txtsize.Text = st.StockSize;
            //Response.Write(st.inverterqty2);
            //Response.End();
            txtqty2.Text = st.inverterqty2;
            txtqty3.Text = st.inverterqty3;


            txtInverterOutput.Text = st.InverterOutput;
            //try
            //{
            //ddlElecDist.SelectedValue = st.ElecDistributorID;
            //}
            //catch
            //{
            //}
            txtApprovalRef.Text = st.ElecDistApprovelRef;
            //try
            //{
            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
            //}
            //catch
            //{
            //}
            txtRegPlanNo.Text = st.RegPlanNo;
            //try
            //{

            if (st.HouseTypeID != null)
            {
                ddlHouseType.SelectedValue = st.HouseTypeID;
            }

            //}
            //catch
            //{
            //}
            //try
            //{
            if (st.RoofTypeID != null)
            {
                ddlRoofType.SelectedValue = st.RoofTypeID;
            }
            //}
            //catch
            //{ }
            //try
            //{
            ddlRoofAngle.SelectedValue = st.RoofAngleID;
            //}
            //catch
            //{ }
            txtLotNum.Text = st.LotNumber;
            txtNMINumber.Text = st.NMINumber;
            txtPeakMeterNo.Text = st.MeterNumber1;
            txtOffPeakMeters.Text = st.MeterNumber2;
            txtMeterPhase.Text = st.MeterPhase;
            try
            {
                ddlestimatevalue.SelectedValue = st.estimatevalue_id;
            }
            catch (Exception ex)
            {

            }
            txtMeterEstimate.Text = st.Meter_Estimate;
            txtEstimatedPaid.Text = st.Estimate_paid;
            txtestimatepaymentdue.Text = st.Payment_due_Date;
            txtstatusfornetmeter.Text = st.Status_For_Net_Meter;

            try
            {
                ddlmeterupgrade.SelectedValue = st.meterupgrade;
            }
            catch
            {
            }


            chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
            chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

            if (st.InstallPostCode != "")
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {

                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                        //Response.End();
                        txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                    }
                }
            }
            try
            {
                txtElecDistApplied.Text = Convert.ToDateTime(st.ElecDistApplied).ToShortDateString();
            }
            catch { }
            ddlElecDistApplyMethod.SelectedValue = st.ElecDistApplyMethod;
            txtElecDistApplySentFrom.Text = st.ElecDistApplySentFrom;

            txtgedano.Text = st.GEDANumber;
            txtdistributorapplicationnumber.Text = st.DistributerApplicationNumber;
            txtsubsidypcrno.Text = st.SubsidyPCRNumber;

            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);
            txtSPAInvoiceNumber.Text = st2.SPAInvoiceNumber;
            try
            {
                txtSPAPaid.Text = Convert.ToDateTime(st2.SPAPaid).ToShortDateString();
            }
            catch { }
            ddlSPAPaidBy.SelectedValue = st2.SPAPaidBy;
            try
            {

                txtSPAPaidAmount.Text = SiteConfiguration.ChangeCurrencyVal(st2.SPAPaidAmount);
            }
            catch { }

            ddlSPAPaidMethod.SelectedValue = st2.SPAPaidMethod;
            if (st.ElecDistributor == "14")
            {
                divSPA.Visible = true;
            }
            else
            {
                divSPA.Visible = false;
            }
            if (ddlElecDist.SelectedValue == "19" && st.InstallState == "TAS")
            {
                if (st.SurveyCerti != string.Empty)
                {
                    if (st.SurveyCerti == "1")
                    {
                        rblSurveyCerti.SelectedValue = "1";
                    }
                    if (st.SurveyCerti == "2")
                    {
                        rblSurveyCerti.SelectedValue = "2";
                    }
                    divSurveyCerti.Visible = true;
                }
                else
                {
                    divSurveyCerti.Visible = false;
                }

                if (st.CertiApprove != string.Empty)
                {
                    if (st.CertiApprove == "1")
                    {
                        rblCertiApprove.SelectedValue = "1";
                    }
                    if (st.CertiApprove == "2")
                    {
                        rblCertiApprove.SelectedValue = "2";
                    }
                    divCertiApprove.Visible = true;
                }
                else
                {
                    divCertiApprove.Visible = false;
                }
            }

            if (st.ElecDistAppBy == string.Empty)
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                try
                {
                    ddlElecDistAppBy.SelectedValue = stEmp.EmployeeID;
                }
                catch { }
            }
            else
            {
                try
                {
                    ddlElecDistAppBy.SelectedValue = st.ElecDistAppBy;
                }
                catch { }
            }
            try
            {
                txtElecDistAppDate.Text = Convert.ToDateTime(st.ElecDistAppDate).ToShortDateString();
            }
            catch { }

            txtVicAppRefNo.Text = st.VicAppReference;
            txtVicNote.Text = st.VicRebateNote;
            ddlvicrebate.SelectedValue = st.VicRebate;
            try
            {
                txtVicdate.Text = Convert.ToDateTime(st.VicDate).ToShortDateString();
            }
            catch { }

            if (Roles.IsUserInRole("Installation Manager"))
            {
                if (st.ProjectStatusID == "3")
                {
                    if (st.ElecDistributorID == "12")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                    else if (st.ElecDistributorID == "13")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                        txtLotNum.Enabled = false;
                        txtRegPlanNo.Enabled = false;
                    }
                    else
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                }
            }
            if (st.ProjectTypeID == "8")
            {
                divMtce.Visible = true;
                divPanelDetail.Visible = true;
                divInverterDetail.Visible = true;
                divSitedetails.Visible = false;
                divDistAppDetail.Visible = false;
                divMeterDetails.Visible = false;
            }
            else
            {
                divMtce.Visible = false;
            }
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
            {
                //Panel1.Enabled = false;
                Panel1.Attributes.Add("style", "pointer-events: none;");
            }
            else
            {
                //Panel1.Enabled = true;
                //Panel1.Attributes.Add("", "");
            }
            if (st.InvoiceNumber == string.Empty && st.InvoiceDoc == string.Empty)
            {
                btnCreateInvoice.Visible = true;
                btnOpenInvoice.Visible = false;
                //InvoicePayments1.Visible = false;
            }
            else
            {
                btnCreateInvoice.Visible = false;
                btnOpenInvoice.Visible = true;
                //InvoicePayments1.Visible = true;

            }

            //}
            //BindInvoice();
        }
    }
    //public void status()
    //{
    //    string ProjectID = "";
    //    if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
    //    {
    //        ProjectID = Request.QueryString["proid"];
    //    }

    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

    //    DataTable dt_Count = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
    //    string totalprice = stPro.TotalQuotePrice;
    //    decimal TotalPrice = 0;
    //    if (totalprice != string.Empty)
    //    {
    //        TotalPrice = Convert.ToDecimal(totalprice);
    //    }
    //    decimal totalpay = 0;
    //    if (dt_Count.Rows.Count > 0)
    //    {
    //        if (dt_Count.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
    //        {
    //            totalpay = Convert.ToDecimal(dt_Count.Rows[0]["InvoicePayTotal"].ToString());
    //            decimal balown = Convert.ToDecimal(totalprice) - Convert.ToDecimal(totalpay);
    //            TextBox txtBalanceOwing = this.Parent.FindControl("txtBalanceOwing") as TextBox;
    //            if (txtBalanceOwing != null)
    //            {

    //                try
    //                {
    //                    txtBalanceOwing.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
    //                }
    //                catch { }

    //            }
    //        }
    //    }
    //    if (TotalPrice - totalpay > 0)
    //    {
    //        // lblpaymentstatus.Text=
    //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
    //    }
    //    else
    //    {
    //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
    //    }
    //    Label lblpaymentstatus = this.Parent.FindControl("lblpaymentstatus") as Label;
    //    if (lblpaymentstatus != null)
    //    {
    //        if (TotalPrice == 0)
    //        {
    //            lblpaymentstatus.Text = " ";
    //        }
    //        else
    //        {
    //            if (TotalPrice - totalpay > 0)
    //            {
    //                lblpaymentstatus.Text = "Owing";
    //            }
    //            else
    //            {
    //                lblpaymentstatus.Text = "Fully Paid";
    //            }
    //        }
    //    }




    //}
    public void BindSaleDropDown()
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);

        //String Inverter1ID;

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";

        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlPanel.Items.Clear();
        ddlPanel.Items.Add(item1);

        DataTable dtPanel = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            //Response.Write(State);
            //Response.End();
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
        }
        else
        {

            dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
        }

        ddlPanel.DataSource = dtPanel;
        ddlPanel.DataValueField = "StockItemID";
        ddlPanel.DataMember = "StockItem";
        ddlPanel.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string PanelID = stProj.PanelBrandID;
            if (!String.IsNullOrEmpty(PanelID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(PanelID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem P1 = new ListItem(); // Example List
                        P1.Text = dt.Rows[0]["StockItem"].ToString();
                        P1.Value = PanelID;
                        ddlPanel.Items.Add(P1);
                    }
                }
            }
        }
        ddlPanel.DataBind();


        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlInverter.Items.Clear();
        ddlInverter.Items.Add(item2);

        DataTable dtInverter = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
        }
        else
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlInverter.DataSource = dtInverter;
        ddlInverter.DataValueField = "StockItemID";
        ddlInverter.DataMember = "StockItem";
        ddlInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter1ID = stProj.InverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter1ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter1ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I1 = new ListItem(); // Example List
                        I1.Text = dt.Rows[0]["StockItem"].ToString();
                        I1.Value = Inverter1ID;
                        ddlInverter.Items.Add(I1);
                    }
                }
            }
        }
        ddlInverter.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlInverter2.Items.Clear();
        ddlInverter2.Items.Add(item3);

        ddlInverter2.DataSource = dtInverter;
        ddlInverter2.DataValueField = "StockItemID";
        ddlInverter2.DataMember = "StockItem";
        ddlInverter2.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter2ID = stProj.SecondInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter2ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter2ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter2ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I2 = new ListItem(); // Example List
                        I2.Text = dt.Rows[0]["StockItem"].ToString();
                        I2.Value = Inverter2ID;
                        ddlInverter2.Items.Add(I2);
                    }
                }
            }
        }
        ddlInverter2.DataBind();


        ListItem item9 = new ListItem();
        item9.Text = "Select";
        item9.Value = "";
        ddlInverter3.Items.Clear();
        ddlInverter3.Items.Add(item9);

        ddlInverter3.DataSource = dtInverter;
        ddlInverter3.DataValueField = "StockItemID";
        ddlInverter3.DataMember = "StockItem";
        ddlInverter3.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter3ID = stProj.ThirdInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter3ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter3ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter3ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I3 = new ListItem(); // Example List
                        I3.Text = dt.Rows[0]["StockItem"].ToString();
                        I3.Value = Inverter3ID;
                        ddlInverter3.Items.Add(I3);
                    }
                }
            }
        }
        ddlInverter3.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlHouseType.Items.Clear();
        ddlHouseType.Items.Add(item4);

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlRoofType.Items.Clear();
        ddlRoofType.Items.Add(item5);

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();



        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlRoofAngle.Items.Clear();
        ddlRoofAngle.Items.Add(item6);

        ddlRoofAngle.DataSource = ClstblRoofAngles.tblRoofAngles_SelectASC();
        ddlRoofAngle.DataValueField = "RoofAngleID";
        ddlRoofAngle.DataMember = "RoofAngle";
        ddlRoofAngle.DataTextField = "RoofAngle";
        ddlRoofAngle.DataBind();

        ListItem itemestimatevalue = new ListItem();
        itemestimatevalue.Text = "Select";
        itemestimatevalue.Value = "";
        ddlestimatevalue.Items.Clear();
        ddlestimatevalue.Items.Add(itemestimatevalue);

        ddlestimatevalue.DataSource = ClsProjectSale.tbl_EstimateValue_Select();
        ddlestimatevalue.DataValueField = "id";
        ddlestimatevalue.DataMember = "EstimateValue";
        ddlestimatevalue.DataTextField = "EstimateValue";
        ddlestimatevalue.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        System.Web.UI.WebControls.ListItem itemPaid = new System.Web.UI.WebControls.ListItem();
        itemPaid.Text = "Select";
        itemPaid.Value = "";
        ddlSPAPaidBy.Items.Clear();
        ddlSPAPaidBy.Items.Add(itemPaid);

        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                if (dr["SalesTeamID"].ToString() != string.Empty)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
            }
            if (SalesTeam != string.Empty)
            {
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {

            if (SalesTeam != string.Empty)
            {
                ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlSPAPaidBy.DataValueField = "EmployeeID";
        ddlSPAPaidBy.DataMember = "fullname";
        ddlSPAPaidBy.DataTextField = "fullname";
        ddlSPAPaidBy.DataBind();

        System.Web.UI.WebControls.ListItem itemMethod = new System.Web.UI.WebControls.ListItem();
        itemMethod.Text = "Select";
        itemMethod.Value = "";
        ddlSPAPaidMethod.Items.Clear();
        ddlSPAPaidMethod.Items.Add(itemMethod);

        ddlSPAPaidMethod.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
        ddlSPAPaidMethod.DataValueField = "FPTransTypeID";
        ddlSPAPaidMethod.DataMember = "FPTransType";
        ddlSPAPaidMethod.DataTextField = "FPTransType";
        ddlSPAPaidMethod.DataBind();

        System.Web.UI.WebControls.ListItem item11 = new System.Web.UI.WebControls.ListItem();
        item11.Text = "Select";
        item11.Value = "";
        ddlElecDistApplyMethod.Items.Clear();
        ddlElecDistApplyMethod.Items.Add(item11);

        ddlElecDistApplyMethod.DataSource = ClsProjectSale.tblElecDistApplyMethod_SelectASC();
        ddlElecDistApplyMethod.DataValueField = "ElecDistApplyMethodID";
        ddlElecDistApplyMethod.DataMember = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataTextField = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataBind();


        ddlpickthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlpickthrough.DataValueField = "PickUpID";
        ddlpickthrough.DataMember = "PickUpID";
        ddlpickthrough.DataTextField = "PickUp";
        ddlpickthrough.DataBind();

        ddlsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlsentthrough.DataValueField = "PickUpID";
        ddlsentthrough.DataMember = "PickUpID";
        ddlsentthrough.DataTextField = "PickUp";
        ddlsentthrough.DataBind();

        ddlhsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlhsentthrough.DataValueField = "PickUpID";
        ddlhsentthrough.DataMember = "PickUpID";
        ddlhsentthrough.DataTextField = "PickUp";
        ddlhsentthrough.DataBind();


        System.Web.UI.WebControls.ListItem item12 = new System.Web.UI.WebControls.ListItem();
        item12.Text = "Select";
        item12.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item12);

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
            if (SalesTeam != string.Empty)
            {
                ddlEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataMember = "fullname";
            ddlEmployee.DataTextField = "fullname";
            ddlEmployee.DataBind();


            ddlEmployee.SelectedValue = stEmp.EmployeeID;
        }
        catch { }


        System.Web.UI.WebControls.ListItem item13 = new System.Web.UI.WebControls.ListItem();
        item13.Text = "Select";
        item13.Value = "";
        ddlElecDistAppBy.Items.Clear();
        ddlElecDistAppBy.Items.Add(item13);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlElecDistAppBy.DataValueField = "EmployeeID";
            ddlElecDistAppBy.DataMember = "fullname";
            ddlElecDistAppBy.DataTextField = "fullname";
            ddlElecDistAppBy.DataBind();
        }
        catch
        { }


        string EleDist = stProj.StreetState;
        //if (stProj.InstallState == "NSW")
        //{
        //    EleDist = "select * from tblElecDistributor where NSW=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "SA")
        //{
        //    EleDist = "select * from tblElecDistributor where SA=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "QLD")
        //{
        //    EleDist = "select * from tblElecDistributor where QLD=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "VIC")
        //{
        //    EleDist = "select * from tblElecDistributor where VIC=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "WA")
        //{
        //    EleDist = "select * from tblElecDistributor where WA=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "ACT")
        //{
        //    EleDist = "select * from tblElecDistributor where ACT=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "TAS")
        //{
        //    EleDist = "select * from tblElecDistributor where TAS=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "NT")
        //{
        //    EleDist = "select * from tblElecDistributor where NT=1 order by ElecDistributor asc";
        //}

        //DataTable dtElecDist = ClsProjectSale(EleDist);
        //if (dtElecDist.Rows.Count > 0)
        //{
        ListItem item7 = new ListItem();
        item7.Text = "Select";
        item7.Value = "";
        ddlElecDist.Items.Clear();
        ddlElecDist.Items.Add(item7);

        //ddlElecDist.DataSource = dtElecDist;
        ddlElecDist.DataSource = ClsProjectSale.ap_form_element_execute(EleDist);
        ddlElecDist.DataValueField = "ElecDistributorID";
        ddlElecDist.DataMember = "ElecDistributor";
        ddlElecDist.DataTextField = "ElecDistributor";
        ddlElecDist.DataBind();
        // }

        string EleRet = stProj.StreetState;
        //if (stProj.InstallState == "NSW")
        //{
        //    EleRet = "select * from tblElecRetailer where NSW=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "SA")
        //{
        //    EleRet = "select * from tblElecRetailer where SA=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "QLD")
        //{
        //    EleRet = "select * from tblElecRetailer where QLD=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "VIC")
        //{
        //    EleRet = "select * from tblElecRetailer where VIC=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "WA")
        //{
        //    EleRet = "select * from tblElecRetailer where WA=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "ACT")
        //{
        //    EleRet = "select * from tblElecRetailer where ACT=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "TAS")
        //{
        //    EleRet = "select * from tblElecRetailer where TAS=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "NT")
        //{
        //    EleRet = "select * from tblElecRetailer where NT=1 order by ElecRetailer asc";
        //}

        //DataTable dtElecRet = ClsProjectSale.ap_form_element_execute(EleRet);
        //if (dtElecRet.Rows.Count > 0)
        //{
        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlElecRetailer.Items.Clear();
        ddlElecRetailer.Items.Add(item8);

        ddlElecRetailer.DataSource = ClsProjectSale.ap_form_element_executeEleRetailer(EleRet); ;
        ddlElecRetailer.DataValueField = "ElecRetailerID";
        ddlElecRetailer.DataMember = "ElecRetailer";
        ddlElecRetailer.DataTextField = "ElecRetailer";
        ddlElecRetailer.DataBind();
        //}

        ListItem item21 = new ListItem();
        item21.Text = "Select";
        item21.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item21);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();
        try
        {
            ddlProjectMtceReasonID.SelectedValue = "2";
        }
        catch { }


        ListItem item22 = new ListItem();
        item22.Text = "Select";
        item22.Value = "";
        //ddlProjectMtceReasonSubID.Items.Clear();
        //ddlProjectMtceReasonSubID.Items.Add(item22);

        //ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        //ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        //ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        //ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        //ddlProjectMtceReasonSubID.DataBind();

        //ddlProjectMtceReasonSubID.SelectedValue = "2";

        ListItem item25 = new ListItem();
        item25.Text = "Select";
        item25.Value = "";
        ddlProjectMtceCallID.Items.Clear();
        ddlProjectMtceCallID.Items.Add(item25);

        //ddlProjectMtceCallID.DataSource = ClstblProjects.tblProjectMtceCall_SelectASC();
        //ddlProjectMtceCallID.DataValueField = "ProjectMtceCallID";
        //ddlProjectMtceCallID.DataMember = "ProjectMtceCall";
        //ddlProjectMtceCallID.DataTextField = "ProjectMtceCall";
        //ddlProjectMtceCallID.DataBind();

        //ddlProjectMtceCallID.SelectedValue = "4";

        ListItem item26 = new ListItem();
        item26.Text = "Select";
        item26.Value = "";
        ddlProjectMtceStatusID.Items.Clear();
        ddlProjectMtceStatusID.Items.Add(item26);

        if (stProj.ProjectTypeID != "8")
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectTop4();
        }
        else
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectLast4();
        }
        ddlProjectMtceStatusID.DataValueField = "ProjectMtceStatusID";
        ddlProjectMtceStatusID.DataMember = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataTextField = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataBind();

        //ddlProjectMtceStatusID.SelectedValue = "2";
    }

    protected void btnUpdateSale_Click(object sender, EventArgs e)
    {

        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string salestype = "";
        if (rblonlypanel.Checked == true)
        {
            salestype = "1";
        }
        if (rblonlyinverter.Checked == true)
        {
            salestype = "2";
        }
        if (rblonlyinverter.Checked == true)
        {
            salestype = "3";
        }

        string PanelBrandID = ddlPanel.SelectedValue; //ID
        string PanelBrand = txtPanelBrand.Text;
        string PanelOutput = txtWatts.Text;
        //string RECRebate = txtRebate.Text;
        decimal totalsubsiydy1 = 0;
        if (fortysubsidy.Text != null && fortysubsidy.Text != "" && twntysubsidy.Text != null && twntysubsidy.Text != "")
        {
            decimal totalsubsiydy = Convert.ToDecimal(fortysubsidy.Text) + Convert.ToDecimal(twntysubsidy.Text);

            totalsubsiydy1 = Math.Round(totalsubsiydy, 2);
        }
        string RECRebate = totalsubsiydy1.ToString();
        double TotalCost = 0;
        if (txtnettotal.Text != null && txtnettotal.Text != "")
        {
            TotalCost = Convert.ToDouble(totalsubsiydy1) + Convert.ToDouble(txtnettotal.Text);

        }
        string STCMultiplier = txtSTCMult.Text;
        string STCZoneRating = txtZoneRt.Text;
        string PanelModel = txtPanelModel.Text;
        string NumberPanels = txtNoOfPanels.Text;
        string SystemCapKW = txtSystemCapacity.Text;
        string subsidypcrno = txtsubsidypcrno.Text;

        string PanelDetails = "";
        if (ddlPanel.SelectedValue != string.Empty)
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            if (ddlPanel.SelectedValue != "")
            {
                PanelDetails = txtNoOfPanels.Text + " X " + txtWatts.Text + " Watt " + stPanel.StockItem + " Panels. (" + txtPanelModel.Text + ")";
            }
        }

        string InverterDetailsID = ddlInverter.SelectedValue; //Id
        string InverterBrand = txtInverterBrand.Text;
        string InverterSeries = txtSeries.Text;
        string SecondInverterDetailsID = ddlInverter2.SelectedValue;
        string ThirdInverterDetailsID = ddlInverter3.SelectedValue;
        string STCNumber = txtSTCNo.Text;

        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        string STCValue = "0";
        if (dtSTCValue.Rows.Count > 0)
        {
            STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
        }
        string InverterModel = txtInverterModel.Text;
        string inverterqty = txtqty.Text;
        string inverterqty2 = txtqty2.Text;
        string inverterqty3 = txtqty3.Text;
        string InverterOutput = txtInverterOutput.Text;
        string Size = txtsize.Text;
        string InverterDetails = "";

        bool res = Attachfile();
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
        }
        if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
        }

        string SystemDetails = "";
        if (ddlPanel.SelectedValue != "")
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            SystemDetails = txtNoOfPanels.Text + " X " + stPanel.StockItem;
        }

        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SystemDetails += " + " + stInverter.StockItem;
        }

        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            SystemDetails += " + " + stInverter2.StockItem;
        }

        string ElecDistributorID = ddlElecDist.SelectedValue;
        string ElecDistApprovelRef = txtApprovalRef.Text;
        string ElecRetailerID = ddlElecRetailer.SelectedValue;
        string RegPlanNo = txtRegPlanNo.Text;
        string HouseTypeID = ddlHouseType.SelectedValue;
        string RoofTypeID = ddlRoofType.SelectedValue;
        string RoofAngleID = ddlRoofAngle.SelectedValue;
        string LotNumber = txtLotNum.Text;
        string NMINumber = txtNMINumber.Text;
        string MeterNumber1 = txtPeakMeterNo.Text;
        string MeterNumber2 = txtOffPeakMeters.Text;
        string MeterPhase = txtMeterPhase.Text;
        string MeterEnoughSpace = Convert.ToString(chkEnoughMeterSpace.Checked);
        string OffPeak = Convert.ToString(chkIsSystemOffPeak.Checked);

        string SPAInvoiceNumber = txtSPAInvoiceNumber.Text;
        string SPAPaid = txtSPAPaid.Text.Trim();
        string SPAPaidBy = ddlSPAPaidBy.SelectedValue;
        string SPAPaidAmount = txtSPAPaidAmount.Text;
        string SPAPaidMethod = ddlSPAPaidMethod.SelectedValue;

        string ElecDistApplied = txtElecDistApplied.Text.Trim();
        string ElecDistApplyMethod = ddlElecDistApplyMethod.SelectedValue;
        string ElecDistApplySentFrom = txtElecDistApplySentFrom.Text;

        string FlatPanels = txtFlatPanels.Text.Trim();
        string PitchedPanels = txtPitchedPanels.Text.Trim();
        string SurveyCerti = rblSurveyCerti.SelectedValue;
        string CertiApprove = rblCertiApprove.SelectedValue;
        string ElecDistAppDate = txtElecDistAppDate.Text.Trim();
        string ElecDistAppBy = ddlElecDistAppBy.SelectedValue;

        string VicAppref = txtVicAppRefNo.Text;
        string VicDate = txtVicdate.Text;
        string VicNote = txtVicNote.Text;
        string VicRebate = ddlvicrebate.SelectedValue;

        //Response.Write(stPro.InstallPostCode);
        //Response.End();
        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stPro.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;



        if (txtNoOfPanels.Text != stPro.NumberPanels)
        {
            //bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);

            //add by jigar chokshi
            //bool sucSaleTotalQuote = ClsProjectSale.tblProjects_UpdateTotalQuotePrice(ProjectID, "0");
            int sucpanelslog = ClsProjectSale.tblPanelsUser_log_insert(ProjectID, txtNoOfPanels.Text, userid);
        }
        //Response.Write(ddlInverter2.SelectedValue + "==" + SecondInverterDetailsID);
        //Response.End();
        if (SecondInverterDetailsID == "")
        {
            SecondInverterDetailsID = "0";
        }
        if (ThirdInverterDetailsID == "")
        {
            ThirdInverterDetailsID = "0";
        }
        string VarOther = "0.00";
        string VarDepo = "0.00";

        //double NetTotalCost = 0;
        //if (txtnettotal.Text!=null && txtnettotal.Text!="")
        //{
        //    double NetTotalCost1 = Convert.ToDouble(totalsubsiydy1) + Convert.ToDouble(txtnettotal.Text);
        //    NetTotalCost = Math.Round(NetTotalCost1, 2);
        //}
        if (txtOther.Text != null && txtOther.Text != "")
        {
            double other1 = Convert.ToDouble(txtOther.Text);
            other1 = Math.Round(other1, 2);
            VarOther = Convert.ToString(other1);
        }
        if (txtDepoReq.Text != null && txtDepoReq.Text != "")
        {
            double other1 = Convert.ToDouble(txtDepoReq.Text);
            other1 = Math.Round(other1, 2);
            VarDepo = Convert.ToString(other1);
        }
        //string NetTotal = TotalCost.ToString();
        ////string NetTotal = txtnettotal.Text;
        string ActualCost = txtRebate.Text;
        string TotalPayableAmt = txttotalamt.Text;
        string NetTotal = txtnettotal.Text;
        string subsidyF = fortysubsidy.Text;
        string subsidyT = twntysubsidy.Text;
        bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate, STCMultiplier, STCZoneRating, PanelModel, NumberPanels, SystemCapKW, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID, RoofTypeID, RoofAngleID, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace, OffPeak, UpdatedBy, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailsID);
        bool sucPrice = ClsProjectSale.tblProjects_UpdateProjectPrice(ProjectID, VarOther, VarDepo);
        //bool sucNetTotal = ClsProjectSale.tblProjects_UpdateNetTotal(ProjectID, NetTotal);
        bool subsidy = ClsProjectSale.tbl_projectsUpdatesubsidy(ProjectID, subsidyF, subsidyT);
        ClsProjectSale.Update_tblProjects_SubsidyPCRNo(ProjectID, subsidypcrno);
        ClsProjectSale.Update_tblProjects_MeterDetails(ProjectID, ddlestimatevalue.SelectedValue, txtEstimatedPaid.Text, txtEstimatedPaid.Text, txtestimatepaymentdue.Text, txtstatusfornetmeter.Text);
        ClsProjectSale.Update_tblProjects_DistributerApplicationNumber(ProjectID, txtdistributorapplicationnumber.Text, txtgedano.Text);
        // add by roshni
        bool successSale_meterupgrade = ClsProjectSale.tblProjects_UpdateSale_meterupgrade(ProjectID, ddlmeterupgrade.SelectedValue);//

        bool successSale_inverterqty = ClsProjectSale.tblProjects_UpdateSale_inverterqty(ProjectID, inverterqty, inverterqty2, inverterqty3);

        bool success_VicRebate = ClsProjectSale.tblProjects_UpdateSale_VicRebate(ProjectID, VicAppref, VicDate, VicNote, VicRebate);

        //  bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);

        //bool suc3 = ClstblStockItems.updatestockitem(ddlInverter.SelectedValue,Size);

        ClsProjectSale.tblProjects_UpdateSalesType(ProjectID, salestype);

        int Tracksuccess = ClstblTrackPanelDetails.tblTrackPanelDetails_Insert(ProjectID, DateTime.Now.ToString(), userid, PanelBrandID, PanelBrand, RECRebate, NumberPanels, PanelOutput, STCMultiplier, SystemCapKW, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, InverterModel, InverterOutput);

        if (ddlthroughtype.SelectedValue != string.Empty)
        {
            if (ddlthroughtype.SelectedValue == "1")
            {
                bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, txtoldserialno.Text, txtlogdement.Text, txtlogdementdate.Text, txttrackingno.Text, txtnewserialno.Text, txterrormessage.Text, txtfaultydate.Text, ddlpickthrough.SelectedValue, txtdeliveryreceived.Text, txtsentsupplier.Text, ddlsentthrough.SelectedValue, txtinvoicesent.Text, txtinvoiceno.Text, ddlinstallerpickup.SelectedValue);
            }
            if (ddlthroughtype.SelectedValue == "2")
            {
                bool scc_update = ClsProjectSale.tblProjects_Update_InverterReplacementDetail(ProjectID, ddlthroughtype.SelectedValue, htxtoldserialno.Text, txthlogdementno.Text, txthlogdementdate.Text, "", txthnewserialno.Text, txtherrormesage.Text, "", "", txtdeliveryrec.Text, txt_hsentsupplierdate.Text, ddlhsentthrough.SelectedValue, txt_incoicesent_date.Text, txt_invoiceno.Text, "");
                //ddlinstallerpickup,txth_faultyunit,
            }
        }

        if (stPro.ProjectTypeID == "8")
        {
            ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, txtOpenDate.Text.Trim(), ddlProjectMtceReasonID.SelectedValue, ddlProjectMtceReasonSubID.SelectedValue, ddlProjectMtceCallID.SelectedValue, ddlProjectMtceStatusID.SelectedValue, Convert.ToString(chkWarranty.Checked), txtCustomerInput.Text, txtFaultIdentified.Text, txtActionRequired.Text, txtWorkDone.Text);
        }

        DataTable dt = ClsProjectSale.tblProjects_CheckProjectActiveBefore(ProjectID);
        if (dt.Rows.Count == 0)//It would check if the project had status 'Active' before.It would allow only if status of project was never 'Active'
        {
            activemethod();
        }
        bool amt = ClsProjectSale.tblprojects_Uddatprice(ProjectID, ActualCost, TotalPayableAmt, NetTotal);

        if (successSale)
        {
            BindProjectSale();
            BindProjects();
            SetAdd1();
        }
        else
        {
            SetError1();
        }
        UpdateQuote();

        if (txtDepositReceived.Text != string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
        }
        if (txtActiveDate.Text != string.Empty)
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        }
        string script = "<script type=\"text/javascript\">alert('Hello');</script>";

        Page page = HttpContext.Current.CurrentHandler as Page;

        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))

        {

            page.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", script);

        }
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "<script type='text/javascript'>callSomething('" + st.ApplicaionStatus + "');</script>", false);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp1", "<script type='text/javascript'>callProjectSTatus('" + st.ProjectSTatus + "');</script>", false);

        //Response.Redirect(Request.RawUrl);

        //PanSuccess.Visible = true;
    }
    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (stPro.ElecDistributorID == "13")
        {
            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                //Response.Write(stPro.DocumentVerified);
                //Response.End();
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    //Response.Write(stPro.DocumentVerified );
                    //Response.End();
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {

                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {

                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {

                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {

                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.RegPlanNo == string.Empty || stPro.LotNumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else if (stPro.ElecDistributorID == "12")
        {

            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }
        else
        {
            if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
        }

    }
    protected void ddlPanel_SelectedIndexChanged(object sender, EventArgs e)
    {


    }

    protected void ddlInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        inverteroutput();

    }

    protected void ddlInverter2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        inverteroutput();
    }

    protected void ddlElecDist_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        if (ddlElecDist.SelectedValue != string.Empty)
        {
            if (ddlElecDist.SelectedValue == "14")
            {
                divSPA.Visible = true;
            }
            else
            {
                divSPA.Visible = false;
            }

            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlElecDist.SelectedValue == "19" && stPro.InstallState == "TAS")
            {
                divSurveyCerti.Visible = true;
                divCertiApprove.Visible = true;
            }
            else
            {
                divSurveyCerti.Visible = false;
                divCertiApprove.Visible = false;
            }

            //DataTable dtEnergex = ClstblElecDistributor.tblElecDistributor_SelectEnergex();
            //if (dtEnergex.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtEnergex.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorNMINumber.Visible = true;
            //    }
            //    else
            //    {
            //        RequiredFieldValidatorNMINumber.Visible = false;
            //    }
            //}

            //DataTable dtErgon = ClstblElecDistributor.tblElecDistributor_SelectErgon();
            //if (dtErgon.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtErgon.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorElecRetailer.Visible = true;
            //        RequiredFieldValidatorRegPlanNo.Visible = true;
            //    }
            //}
        }
        else
        {
            divSurveyCerti.Visible = false;
            divCertiApprove.Visible = false;
        }
    }

    protected void txtNoOfPanels_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        //string ProjectID = Request.QueryString["proid"];
        //SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //PanAddUpdate.Visible = true;
        //
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        PanAddUpdate.Visible = true;
        if (txtNoOfPanels.Text != string.Empty && txtWatts.Text != string.Empty)
        {
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;
            txtSystemCapacity.Text = Convert.ToString(capacity);

            //string installbooking = stPro.InstallBookingDate;
            //double cap = Convert.ToDouble(capacity);

            //SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
            //if (cap < 3)
            //{
            //    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
            //    var subcap2 = 0.00;
            //    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
            //    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
            //    fortysubsidy.Text = Convert.ToString(subcap1);
            //    twntysubsidy.Text = Convert.ToString(subcap2);
            //    txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
            //    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));
            //}
            //else
            //{
            //    var cap1 = cap - 3;
            //    var subcap1 = 0.00;
            //    var subcap2 = cap1;
            //    var subcap3 = 0.00;

            //    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
            //    if (subcap2 <= 10)
            //    {
            //        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
            //    }
            //    else
            //    {
            //        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

            //        var cap2 = cap - 10;

            //        if (cap2 > 0)
            //        {
            //            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
            //        }
            //    }
            //    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
            //    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
            //    fortysubsidy.Text = Convert.ToString(subcap1);
            //    twntysubsidy.Text = Convert.ToString(subcap2);
            //    txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
            //    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
            //}

        }

        decimal InvSize = 0;
        decimal PnlSysCap = 0;
        if (txtSystemCapacity.Text != null && txtSystemCapacity.Text != "")
        {
            PnlSysCap = Convert.ToDecimal(txtSystemCapacity.Text);
        }
        if (txtsize.Text != null && txtsize.Text != "")
        {
            InvSize = Convert.ToDecimal(txtsize.Text);
        }
        decimal min = 0;

        if (InvSize < PnlSysCap)
        {
            min = InvSize;
            if (txtNoOfPanels.Text != string.Empty)
            {
                // capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * InvSize) / 1000;
                //txtSystemCapacity.Text = Convert.ToString(capacity);

                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(InvSize);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                var actCost1 = cap * Convert.ToDouble(stpricenew.PricePerKW);
                txtRebate.Text = actCost1.ToString();
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    //txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));


                    double totalAmt = subcap1 + subcap2;
                    double totlamt1 = Math.Round(totalAmt, 2);

                    txtactualamt.Text = Convert.ToString(totlamt1);
                    double a1 = actCost - (subcap1 + subcap2);
                    double a2 = Math.Round(a1, 2);
                    txttotalamt.Text = Convert.ToString(a2);
                    txtnettotal.Text = Convert.ToString(a2);

                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                    double actamt = subcap1 + subcap2 + subcap3;
                    double acmat1 = Math.Round(actamt, 2);
                    txtactualamt.Text = Convert.ToString(acmat1);
                    double ttal = actCost - (subcap1 + subcap2 + subcap3);
                    double ttl1 = Math.Round(ttal, 2);
                    txttotalamt.Text = Convert.ToString(ttl1);
                    txtnettotal.Text = Convert.ToString(ttl1);
                }

            }
        }
        else
        {
            min = PnlSysCap;
            if (txtNoOfPanels.Text != string.Empty)
            {
                //capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * PnlSysCap) / 1000;
                //txtSystemCapacity.Text = Convert.ToString(capacity);


                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(PnlSysCap);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));

                    double totalAmt = subcap1 + subcap2;
                    double totlamt1 = Math.Round(totalAmt, 2);

                    txtactualamt.Text = Convert.ToString(totlamt1);
                    double a1 = actCost - (subcap1 + subcap2);
                    double a2 = Math.Round(a1, 2);
                    txttotalamt.Text = Convert.ToString(a2);
                    txtnettotal.Text = Convert.ToString(a2);
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    double actamt = subcap1 + subcap2 + subcap3;
                    double amt1 = Math.Round(subcap1, 2);
                    txtactualamt.Text = Convert.ToString(amt1);
                    double total_Amt = actCost - (subcap1 + subcap2 + subcap3);
                    double total1 = Math.Round(total_Amt, 2);
                    txttotalamt.Text = Convert.ToString(total1);
                    txtnettotal.Text = Convert.ToString(total1);
                }

            }
        }
    }
    protected void ddlRoofType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        if (ddlRoofType.SelectedValue != string.Empty)
        {
            if (ddlRoofType.SelectedValue == "8") // Tin Flat
            {
                ddlRoofAngle.Enabled = false;
                ddlRoofAngle.SelectedValue = "";
            }
            else
            {
                ddlRoofAngle.Enabled = true;
            }
            if (ddlRoofType.SelectedValue == "7") // Tin + Tile  
            {
                divFlatPanels.Visible = true;
                divPitchedPanels.Visible = true;
                lblFlatPanels.Text = "Panels on Tin";
                lblPitchedPanels.Text = "Panels on Tile";
            }
            else if (ddlRoofType.SelectedValue == "9") // Tin Pitched + Flat
            {
                divFlatPanels.Visible = true;
                divPitchedPanels.Visible = true;
                lblFlatPanels.Text = "Panels on Flat";
                lblPitchedPanels.Text = "Panels on Pitched";
            }
            else
            {
                divFlatPanels.Visible = false;
                divPitchedPanels.Visible = false;
                lblFlatPanels.Text = "";
                lblPitchedPanels.Text = "";
            }
        }
    }
    protected void ddlthroughtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlthroughtype.SelectedValue == "1")
        {
            divsupplierthrough.Visible = true;
            divinhousethrough.Visible = false;
        }
        else
        {
            divinhousethrough.Visible = true;
            divsupplierthrough.Visible = false;
        }

    }
    protected void ddlPanel_SelectedIndexChanged1(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        PanAddUpdate.Visible = true;

        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st2.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        if (ddlPanel.SelectedValue != string.Empty)
        {

            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            txtPanelBrand.Text = st.StockManufacturer;
            txtPanelModel.Text = st.StockModel;
            txtWatts.Text = st.StockSize;

            if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
            {
                SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000;
                txtSystemCapacity.Text = Convert.ToString(SystemCapacity);
            }

            txtSTCMult.Text = "1";
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                try
                {
                    txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                }
                catch { }
            }
            capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * Convert.ToDecimal(txtWatts.Text)) / 1000;
            string installbooking = stPro.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                if (dt.Rows.Count > 0)
                {
                    Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);
                }
                //-------------------------------------------------------------------
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //---------------------------------------------------------------------
            }
            else
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                //Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                //stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);

                //--------------------------------------------------------------------------
                //string date = Convert.ToDateTime(installbooking).ToShortDateString();
                //DateTime dt1 = Convert.ToDateTime("31/12/2017");
                //DateTime dt2 = Convert.ToDateTime(date);
                //int year = dt2.Year;
                //if (dt2 <= dt1)
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 14);
                //}
                //else
                //{
                //    stcno = ((capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
                //}
                //-----------------------------------------------------------------------------
            }
            //stcno = (Convert.ToDecimal(capacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
            //stcno = (Convert.ToDecimal(SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * 13);
            //int myInt = (int)Math.Round(stcno);
            int myInt = (int)Math.Floor(stcno);
            txtSTCNo.Text = Convert.ToString(myInt);
            rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
            txtRebate.Text = Convert.ToString(rebate);
        }
        else
        {
            //txtPanelBrand.Text = string.Empty;
            //txtPanelModel.Text = string.Empty;
            //txtWatts.Text = string.Empty;
        }
        //string ProjectID = Request.QueryString["proid"];
        //SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        PanAddUpdate.Visible = true;
        decimal InvSize = 0;
        decimal PnlSysCap = 0;
        if (txtSystemCapacity.Text != null && txtSystemCapacity.Text != "")
        {
            PnlSysCap = Convert.ToDecimal(txtSystemCapacity.Text);
        }
        if (txtsize.Text != null && txtsize.Text != "")
        {
            InvSize = Convert.ToDecimal(txtsize.Text);
        }
        decimal min = 0;
        if (InvSize < PnlSysCap)
        {
            min = InvSize;
            if (txtNoOfPanels.Text != string.Empty)
            {
                // capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * InvSize) / 1000;
                //txtSystemCapacity.Text = Convert.ToString(capacity);

                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(InvSize);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                var actCost1 = cap * Convert.ToDouble(stpricenew.PricePerKW);
                txtRebate.Text = actCost1.ToString();
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    //txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));

                    double totalAmt = subcap1 + subcap2;
                    double totlamt1 = Math.Round(totalAmt, 2);

                    txtactualamt.Text = Convert.ToString(totlamt1);
                    double a1 = actCost - (subcap1 + subcap2);
                    double a2 = Math.Round(a1, 2);
                    txttotalamt.Text = Convert.ToString(a2);
                    txtnettotal.Text = Convert.ToString(a2);
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                    double actamt = subcap1 + subcap2 + subcap3;
                    double acmat1 = Math.Round(actamt, 2);
                    txtactualamt.Text = Convert.ToString(acmat1);
                    double ttal = actCost - (subcap1 + subcap2 + subcap3);
                    double ttl1 = Math.Round(ttal, 2);
                    txttotalamt.Text = Convert.ToString(ttl1);
                    txtnettotal.Text = Convert.ToString(ttl1);
                }

            }
        }
        else
        {
            min = PnlSysCap;
            if (txtNoOfPanels.Text != string.Empty)
            {
                // capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * PnlSysCap) / 1000;
                // txtSystemCapacity.Text = Convert.ToString(capacity);

                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(min);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));

                    double totalAmt = subcap1 + subcap2;
                    double totlamt1 = Math.Round(totalAmt, 2);

                    txtactualamt.Text = Convert.ToString(totlamt1);
                    double a1 = actCost - (subcap1 + subcap2);
                    double a2 = Math.Round(a1, 2);
                    txttotalamt.Text = Convert.ToString(a2);
                    txtnettotal.Text = Convert.ToString(a2);
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    //txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                    double actamt = subcap1 + subcap2 + subcap3;
                    double acmat1 = Math.Round(actamt, 2);
                    txtactualamt.Text = Convert.ToString(acmat1);
                    double ttal = actCost - (subcap1 + subcap2 + subcap3);
                    double ttl1 = Math.Round(ttal, 2);
                    txttotalamt.Text = Convert.ToString(ttl1);
                    txtnettotal.Text = Convert.ToString(ttl1);

                }

            }
        }
    }

    public void inverteroutput()
    {
        if (ddlInverter.SelectedValue != null && ddlInverter.SelectedValue != "")
        {
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            if (ddlInverter.SelectedValue != string.Empty)
            {
                txtInverterBrand.Text = st.StockManufacturer;
                txtInverterModel.Text = st.StockModel;
                txtSeries.Text = st.StockSeries;
                txtsize.Text = st.StockSize;
            }
            else
            {
                txtInverterBrand.Text = "";
                txtInverterModel.Text = "";
                txtSeries.Text = "";
                txtsize.Text = "0.00";
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            PanAddUpdate.Visible = true;
            decimal InvSize = 0;
            decimal PnlSysCap = 0;
            if (txtSystemCapacity.Text != null && txtSystemCapacity.Text != "")
            {
                PnlSysCap = Convert.ToDecimal(txtSystemCapacity.Text);
            }
            if (txtsize.Text != null && txtsize.Text != "")
            {
                InvSize = Convert.ToDecimal(txtsize.Text);
            }
            decimal min = 0;
            if (InvSize < PnlSysCap)
            {
                min = InvSize;
                if (txtNoOfPanels.Text != string.Empty)
                {
                    // capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * InvSize) / 1000;
                    //txtSystemCapacity.Text = Convert.ToString(capacity);

                    string installbooking = stPro.InstallBookingDate;
                    double cap = Convert.ToDouble(InvSize);

                    SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                    var actCost1 = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    txtRebate.Text = actCost1.ToString();
                    if (cap < 3)
                    {
                        var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                        var subcap2 = 0.00;
                        //txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                        var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                        fortysubsidy.Text = Convert.ToString(subcap1);
                        twntysubsidy.Text = Convert.ToString(subcap2);
                        double actamt = subcap1 + subcap2;
                        double amt1 = Math.Round(subcap1, 2);
                        txtactualamt.Text = Convert.ToString(amt1);
                        double total_Amt = actCost - (subcap1 + subcap2);
                        double total1 = Math.Round(total_Amt, 2);
                        txttotalamt.Text = Convert.ToString(total1);
                        txtnettotal.Text = Convert.ToString(total1);
                        //txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                        //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));
                    }
                    else
                    {
                        var cap1 = cap - 3;
                        var subcap1 = 0.00;
                        var subcap2 = cap1;
                        var subcap3 = 0.00;

                        subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                        if (subcap2 <= 10)
                        {
                            subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                        }
                        else
                        {
                            subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                            var cap2 = cap - 10;

                            if (cap2 > 0)
                            {
                                subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                            }
                        }
                        txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                        var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                        fortysubsidy.Text = Convert.ToString(subcap1);
                        twntysubsidy.Text = Convert.ToString(subcap2);
                        double actamt = subcap1 + subcap2 + subcap3;
                        double amt1 = Math.Round(subcap1, 2);
                        txtactualamt.Text = Convert.ToString(amt1);
                        double total_Amt = actCost - (subcap1 + subcap2 + subcap3);
                        double total1 = Math.Round(total_Amt, 2);
                        txttotalamt.Text = Convert.ToString(total1);
                        txtnettotal.Text = Convert.ToString(total1);
                        //txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                        //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                    }

                }
            }
            else
            {
                min = PnlSysCap;
                if (txtNoOfPanels.Text != string.Empty)
                {
                    //capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * PnlSysCap) / 1000;
                    //txtSystemCapacity.Text = Convert.ToString(capacity);

                    string installbooking = stPro.InstallBookingDate;
                    double cap = Convert.ToDouble(PnlSysCap);

                    SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                    if (cap < 3)
                    {
                        var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                        var subcap2 = 0.00;
                        txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                        var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                        fortysubsidy.Text = Convert.ToString(subcap1);
                        twntysubsidy.Text = Convert.ToString(subcap2);
                        double totalAmt = subcap1 + subcap2;
                        double totlamt1 = Math.Round(totalAmt, 2);

                        txtactualamt.Text = Convert.ToString(totlamt1);
                        double a1 = actCost - (subcap1 + subcap2);
                        double a2 = Math.Round(a1, 2);
                        txttotalamt.Text = Convert.ToString(a2);
                        txtnettotal.Text = Convert.ToString(a2);
                    }
                    else
                    {
                        var cap1 = cap - 3;
                        var subcap1 = 0.00;
                        var subcap2 = cap1;
                        var subcap3 = 0.00;

                        subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                        if (subcap2 <= 10)
                        {
                            subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                        }
                        else
                        {
                            subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;


                            var cap2 = cap - 10;





                            if (cap2 > 0)
                            {
                                subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                            }
                        }
                        txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                        var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                        fortysubsidy.Text = Convert.ToString(subcap1);
                        twntysubsidy.Text = Convert.ToString(subcap2);
                        double actamt = subcap1 + subcap2 + subcap3;
                        double acmat1 = Math.Round(actamt, 2);
                        txtactualamt.Text = Convert.ToString(acmat1);
                        double ttal = actCost - (subcap1 + subcap2 + subcap3);
                        double ttl1 = Math.Round(ttal, 2);
                        txttotalamt.Text = Convert.ToString(ttl1);
                        txtnettotal.Text = Convert.ToString(ttl1);
                    }

                }
            }
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    protected void ddlInverter3_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        inverteroutput();
    }


    protected void chkSignedQuote_CheckedChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        if (chkSignedQuote.Checked == true)
        {
            divSQ.Visible = true;
            if (st.SQ == string.Empty)
            {
                //RequiredFieldValidatorSQ.Visible = true;
            }
            else
            {
                // RequiredFieldValidatorSQ.Visible = false;
            }
        }
        else
        {
            divSQ.Visible = false;
            //RequiredFieldValidatorSQ.Visible = false;
        }
        BindScript();
    }


    protected void lnksendmessage_Click(object sender, EventArgs e)
    {
        StUtilities st3 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String Siteurl = st3.siteurl;

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        string ProjectNumber = st.ProjectNumber;
        string Token = ClsAdminSiteConfiguration.RandomStrongToken();
        DateTime TokenDate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
        string tokendate = Convert.ToString(TokenDate);
        string IsExpired = "false";
        string IsOwner = "true";
        string SignFlag = "false";

        string userid = Membership.GetUser(Context.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string empid = st_emp.EmployeeID;

        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
        if (dtQuote.Rows.Count > 0)
        {
            string Quotepdfno = dtQuote.Rows[0]["ProjectQuoteDoc"].ToString();


            int success = Clstbl_TokenData.tbl_TokenData_Insert(Token, tokendate, ProjectID, ProjectNumber, IsExpired, IsOwner, empid, Quotepdfno, SignFlag);

            if (success > 0)
            {
                #region SendMail

                TextWriter txtWriter = new StringWriter() as TextWriter;
                StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = stU.from;
                String Subject = "Signature Request - " + ConfigurationManager.AppSettings["SiteName"].ToString();

                Server.Execute("~/mailtemplate/SignatureLinkMail.aspx?proid=" + ProjectID + "&rt=" + Token, txtWriter);

                Utilities.SendMail(from, stContact.ContEmail, Subject, txtWriter.ToString());
                //Utilities.SendMail(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString());

                //string filepath = "http://localhost:57341//userfiles//quotedoctest//6558Quotation.pdf";
                //Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath);
                try
                {
                }
                catch
                {
                }

                #endregion

                //#region Send Sms
                //SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);


                //if (st2.ContMobile.Substring(0, 1) == "0")
                //{
                //    st2.ContMobile = st2.ContMobile.Substring(1);
                //}
                //Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectById(success.ToString());
                //st2.ContMobile = "+61" + st2.ContMobile;
                //string mobileno = st2.ContMobile;      // "+919979156818";  //contactno;
                //string OwnerName = st2.ContFirst + " " + st2.ContLast;
                //string messagetext = "Hello " + OwnerName + ",\nPlease open this URL to upload your signature on DocNo: " + sttok.QDocNo + ":\n" + Siteurl + "Signature.aspx?proid=" + ProjectID + "&rt=" + Token + "\n\n Arise Solar.";

                //if (mobileno != string.Empty && messagetext != string.Empty)
                //{
                //    string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                //    string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                //    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);//919879111739  +61451831980
                //    var message = twilio.SendMessage("+61418671214", mobileno, messagetext);
                //    //var message = twilio.SendMessage("+61418671214", "+61402262170", messagetext);
                //    //Response.Write(message.Sid);

                //    if (message.RestException == null)
                //    {
                //        // status = "success";
                //        MsgSendSuccess();
                //    }
                //    else
                //    {
                //        // status = "error";
                //        SetError1();
                //    }

                //}

                //#endregion


            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();//Create quote first then send the message.
        }



    }

    protected void lnksendbrochures_Click(object sender, EventArgs e)
    {
        try
        {
            string ProjectID = Request.QueryString["proid"];

            #region SendMail
            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Australian Premium Solar Quote";

            Server.Execute("~/mailtemplate/BrochureAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            string[] filepath = new string[10];
            string[] PDFName = new string[10];
            int FileCount = 0;
            int exist = 0;

            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

            if (!string.IsNullOrEmpty(st.PanelBrandID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.PanelBrandID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.InverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.InverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        exist++;
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.SecondInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.SecondInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }
            if (!string.IsNullOrEmpty(st.ThirdInverterDetailsID))
            {
                try
                {
                    SttblStockItems st1 = ClstblStockItems.tblStockItems_SelectByStockItemID(st.ThirdInverterDetailsID);
                    if (!string.IsNullOrEmpty(st1.FileName))
                    {
                        PDFName[FileCount] = st1.StockItem + "_Brochure.pdf";

                        filepath[FileCount] = pdfURL + "StockItemPDF/" + st1.StockItemID + "_" + st1.FileName;
                        FileCount++;
                    }
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }
            }

            int QuoteDocNo = ClstblProjects.tblProjectQuotes_SelectTOPDocNoByProjectID(ProjectID);
            if (QuoteDocNo > 0)
            {
                try
                {
                    exist++;
                    PDFName[FileCount] = st.ProjectNumber + "_Quotation.pdf";
                    filepath[FileCount] = pdfURL + "quotedoc/" + QuoteDocNo + "Quotation.pdf";
                    FileCount++;
                }
                catch
                {
                    PDFName[FileCount] = string.Empty;
                    filepath[FileCount] = string.Empty;
                }

            }

            if (exist == 3)//It sends mail only when one panel,one inverter and quote all 3 pdfs are available.
            {
                //if (!string.IsNullOrEmpty(st.SalesRep))
                //{
                //    SttblEmployees stemploy = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.SalesRep);
                //    Utilities.SendMailWithMultipleAttachmentWithReplyList(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount, stemploy.EmpEmail, stemploy.EmpFirst + " " + stemploy.EmpLast);
                //}
                //else
                //{
                //    Utilities.SendMailWithMultipleAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName, FileCount);
                //}
                Utilities.SendMailWithMultipleAttachmentWithReplyList(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName, FileCount, "kiran.sawant@meghtechnologies.com", "Kiran Sawant");
                MsgSendSuccess();
                //Console.WriteLine("Hello");
            }
            else
            {
                if (QuoteDocNo <= 0)
                {
                    MsgInfo("Please generate Quotation first.");
                }
                else
                {
                    MsgBrochureNotAvaialble();
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
        }
    }

    public void MsgSendSuccess()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunMsgSendSuccess();", true);
    }
    public void MsgInfo(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyBluefun('{0}');", msg), true);
    }
    public void MsgBrochureNotAvaialble()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunBrochuresDoesNotExist();", true);
    }

    protected void btnCreateQuote_Click(object sender, EventArgs e)
    {
        try
        {
            string QuoteID = string.Empty;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;

            string ProjectID = Request.QueryString["proid"];


            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            string ProjectNumber = st.ProjectNumber;
            //DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectTop1(ProjectID);
            DataTable dtTop1 = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);
            if (dtTop1.Rows.Count > 0)
            {
            }
            else
            {
                bool sucQoute = ClstblProjects.tblProjects_UpdateQuoteSent(ProjectID);
            }

            BindProjectQuote();

            int success = ClstblProjects.tblProjectQuotes_Insert(ProjectID, ProjectNumber, EmployeeID);
            string QuoteDoc = "Quotation.pdf";

            if (Convert.ToString(success) != "")
            {
                QuoteID = Convert.ToString(success);
                QuoteDoc = Convert.ToString(success) + QuoteDoc;
                bool suc = ClstblProjects.tblProjectQuotes_UpdateProjectQuoteDoc(Convert.ToString(success), QuoteDoc);
            }

            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
            TextWriter txtWriter = new StringWriter() as TextWriter;

            if (st.ProjectTypeID == "8")
            {
                if (stCust.ResCom == "2")
                {
                    Server.Execute("~/mailtemplate/mtcecommquote.aspx?id=" + ProjectID, txtWriter);
                }
                else
                {
                    Server.Execute("~/mailtemplate/mtcequote.aspx?id=" + ProjectID, txtWriter);
                }
            }
            else
            {
                if (stCust.ResCom == "2")
                {

                    //Telerik_reports.generate_quatation(ProjectID, QuoteID);

                    ClsTelerik_reports.Generate_Quotation(ProjectID, QuoteID);
                    //Telerik_reports.generate_quatation(ProjectID, QuoteID);

                    //Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);
                }
                else
                {

                    //Telerik_reports.generate_quatation(ProjectID, QuoteID);

                    ClsTelerik_reports.Generate_Quotation(ProjectID, QuoteID);
                    //Telerik_reports.generate_quatation(ProjectID, QuoteID);

                    // Server.Execute("~/mailtemplate/quatationdesign.aspx?id=" + ProjectID, txtWriter);//quatationnew
                    //Server.Execute("~/mailtemplate/receipt.aspx?id=" + ProjectID, txtWriter);//quatationdesign
                }
            }

            String htmlText = txtWriter.ToString();

            //HTMLExportToPDF(htmlText, ProjectID + "Quote.pdf");
            // SavePDF(htmlText, QuoteDoc);
            BindQuote();
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    public void BindQuote()
    {

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(Request.QueryString["proid"]);
            if (dtQuote.Rows.Count > 0)
            {
                divQuotes2.Visible = true;
                divQuotes.Visible = true;
                rptQuote.DataSource = dtQuote;
                rptQuote.DataBind();
                if (dtQuote.Rows.Count > 5)
                {
                    divquo.Attributes.Add("style", "overflow-x: scroll; height: 200px !important;");
                }
                else
                {
                    divquo.Attributes.Add("style", "");
                }


            }
            else
            {
                divQuotes2.Visible = false;
                divQuotes.Visible = false;
            }
        }
    }

    public void BindInvoice()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            //txtInvoiceNumber.Text = st.InvoiceNumber;
            txtInvoiceDoc.Text = st.InvoiceDoc;
            txtinvoiceno.Text = st.InvoiceNumber;
            txtInvoiceNumber.Text = st.InvoiceNumber;
            txtinvoicesent.Text = st.InvoiceSent;
            txtInvoiceSentquote.Text = st.InvoiceSent;
        }
    }

    public void BindProjectQuote()
    {
        BindScript();
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);
        if (st.ProjectStatusID == "8")
        {
            //cvTodate.Visible = false;
        }
        if (Roles.IsUserInRole("Administrator"))
        {
            txtInvoiceNumber.Enabled = true;
            txtInvoiceDoc.Enabled = true;
            txtInvoiceSentquote.Enabled = true;
            divDepRec.Enabled = true;
        }
        else
        {
            txtInvoiceNumber.Enabled = false;
            txtInvoiceDoc.Enabled = false;
            txtInvoiceSentquote.Enabled = false;
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            // ImageDepRec.Visible = true;
            txtDepositReceived.Enabled = true;
        }
        else
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PreInstaller"))
            {
                if (st2.FinanceWithDepositID == "1")
                {
                    txtDepositReceived.Enabled = true;
                    //  ImageDepRec.Visible = true;

                    if (st.InvoiceNumber == string.Empty)
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                    else
                    {
                        txtDepositReceived.Enabled = true;
                        //   ImageDepRec.Visible = true;
                    }
                }
                else
                {
                    DataTable dtDR = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
                    if (dtDR.Rows.Count > 0)
                    {
                        txtDepositReceived.Enabled = true;
                        //   ImageDepRec.Visible = true;
                    }
                    else
                    {
                        //   ImageDepRec.Visible = false;
                        txtDepositReceived.Enabled = false;
                    }
                }
            }
            else
            {
                //ImageDepRec.Visible = false;
                txtDepositReceived.Enabled = false;
            }
        }

        if (st.ProjectStatusID == "3")
        {
            RequiredFieldValidatorDepRec.Visible = true;
        }
        else
        {
            RequiredFieldValidatorDepRec.Visible = false;
        }
        //if (st.ProjectStatusID == "10" || st.ProjectStatusID == "5")
        //{
        //    RequiredFieldValidatorAD.Visible = true;
        //}
        //else
        //{
        //    RequiredFieldValidatorAD.Visible = false;
        //}

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            if (st.ProjectStatusID == "5")
            {

                PanAddUpdate.Enabled = false;
            }
        }
        if (Roles.IsUserInRole("Finance"))
        {
            PanAddUpdate.Enabled = false;
        }
        DataTable dt_inv1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (dt_inv1.Rows.Count > 0)
        {

            if (dt_inv1.Rows[0]["InvoicePayDate"].ToString() != string.Empty)
            {
                hiddenExpiryDate.Value = Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]).AddDays(-1).ToString();//System.DateTime.Now.AddDays(-1).ToString();
                //CompareValidatorDepRec.ValueToCompare = DateTime.Today.Date.ToString("dd/MM/yyyy"); //string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dt_inv1.Rows[0]["InvoicePayDate"]));
            }
        }
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("DSales Manager"))
        {
            //CompareValidatorActiveDate.Visible = false;
            //CompareValidatorActiveDate.ValueToCompare = txtDepositReceived.Text;
            //CompareValidatorDepRec.Visible = false;
            if (txtDepositReceived.Text != string.Empty)
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString();
                    hdndeprec.Value = Convert.ToDateTime(txtDepositReceived.Text).AddDays(-1).ToString();
                }
            }
            else
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    hdndeprec.Value = DateTime.Now.AddDays(-1).ToString();
                }
            }
        }
        else
        {
            //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            //CompareValidatorActiveDate.ValueToCompare = txtDepositReceived.Text;
            if (txtDepositReceived.Text != string.Empty)
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString();
                    hdndeprec.Value = Convert.ToDateTime(txtDepositReceived.Text).AddDays(-1).ToString();
                }
            }
            else
            {
                if (st.ProjectStatusID != "8")
                {
                    //CompareValidatorActiveDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    hdndeprec.Value = DateTime.Now.AddDays(-1).ToString();
                }
            }
            if (st.DepositReceived == string.Empty)
            {
                //CompareValidatorDepRec.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            }
        }

        if (Roles.IsUserInRole("Accountant"))
        {
            PanAddUpdate.Enabled = true;
            Panel2.Enabled = true;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Maintenance"))
        {
            PanAddUpdate.Enabled = true;
        }
        if (Roles.IsUserInRole("SalesRep"))
        {
        }
        if (Roles.IsUserInRole("STC"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            PanAddUpdate.Enabled = true;
            PanelMain.Enabled = true;
            Panel1.Enabled = false;
            Panel2.Enabled = true;
            Panel31.Enabled = false;
            Panel32.Enabled = false;
            //Panel4.Enabled = false;
            Panel5.Enabled = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            PanAddUpdate.Enabled = true;
            divDepRec.Enabled = false;
            PanActiveDate.Enabled = false;
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            if (st.ProjectStatusID == "3")
            {
                chkSignedQuote.Enabled = false;
                txtDepositReceived.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
        {
            PanAddUpdate.Enabled = false;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            //string CSalesTeamID = stEmpC.SalesTeamID;
            string CSalesTeamID = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                }
                CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
            }

            if (Request.QueryString["proid"] != string.Empty)
            {
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                string EmpType = stEmp.EmpType;
                //string SalesTeamID = stEmp.SalesTeamID;
                string SalesTeam = "";
                DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale1.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale1.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }

                if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                {
                    PanAddUpdate.Enabled = true;
                }
                else
                {
                    PanAddUpdate.Enabled = false;
                }
            }
        }
        string ProjectID = Request.QueryString["proid"];

        BindQuote();
        BindInvoice();

        if (st.InvoiceNumber == string.Empty && st.InvoiceDoc == string.Empty)
        {
            btnCreateInvoice.Visible = true;
            btnOpenInvoice.Visible = false;
            InvoicePayments1.Visible = false;
        }
        else
        {
            btnCreateInvoice.Visible = false;
            btnOpenInvoice.Visible = true;
            InvoicePayments1.Visible = true;

        }
        try
        {
            //Response.Write(Convert.ToDateTime(st.QuoteSent).ToShortDateString());
            //Response.End();
            txtQuoteSent.Text = Convert.ToDateTime(st.QuoteSent).ToShortDateString();
        }
        catch
        {
        }
        try
        {
            txtssdate.Text = Convert.ToDateTime(st.SSActiveDate).ToShortDateString();
        }
        catch { }
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator"))
        {
            txtQuoteSent.Enabled = true;
            PanQuoteSent.Enabled = true;
        }

        try
        {
            txtQuoteAcceptedQuote.Text = Convert.ToDateTime(st.QuoteAccepted).ToShortDateString();
        }
        catch { }
        txtInvoiceDoc.Text = st.InvoiceDoc;
        //txtinvoiceno.Text = st.InvoiceNumber;
        txtInvoiceSentquote.Text = st.InvoiceSent;
        txtInvoiceNumber.Text = st.InvoiceNumber;

        if (st.InvoiceSent != string.Empty)
        {
            try
            {
                txtInvoiceSentquote.Text = Convert.ToDateTime(st.InvoiceSent).ToShortDateString();
            }
            catch { }
        }
        else
        {
            DataTable dtInvDate = ClstblInvoicePayments.tblInvoicePayments_SelectTop1ByProjectID(ProjectID);
            if (dtInvDate.Rows.Count > 0)
            {
                try
                {
                    txtInvoiceSentquote.Text = Convert.ToDateTime(dtInvDate.Rows[0]["InvoicePayDate"].ToString()).ToShortDateString();
                }
                catch { }
            }
        }
        try
        {
            txtDepositReceived.Text = Convert.ToDateTime(st.DepositReceived).ToShortDateString();
        }
        catch { }

        chkSignedQuote.Checked = Convert.ToBoolean(st.SignedQuote);
        if (st.SQ != string.Empty)
        {
            lblSQ.Visible = true;
            lblSQ.Text = st.SQ;
            //lblSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
            lblSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;
            //   RequiredFieldValidatorSQ.Visible = false;
        }
        else
        {
            lblSQ.Visible = false;
        }

        chkMeterBoxPhotosSaved.Checked = Convert.ToBoolean(st.MeterBoxPhotosSaved);

        if (st.MP != string.Empty)
        {
            lblMP.Visible = true;
            lblMP.Text = st.MP;
            //lblMP.NavigateUrl = "~/userfiles/MP/" + st.MP;
            lblMP.NavigateUrl = pdfURL + "MP/" + st.MP;
            // RequiredFieldValidatorMP.Visible = false;
        }
        else
        {
            lblMP.Visible = false;
        }
        chkElecBillSaved.Checked = Convert.ToBoolean(st.ElecBillSaved);

        if (st.EB != string.Empty)
        {
            lblEB.Visible = true;
            lblEB.Text = st.EB;
            //lblEB.NavigateUrl = "~/userfiles/EB/" + st.EB;
            lblEB.NavigateUrl = pdfURL + "EB/" + st.EB;
            // RequiredFieldValidatorEB.Visible = false;
        }
        else
        {
            lblEB.Visible = false;
        }
        if (st.beatquote != string.Empty)
        {
            chkbeatquote.Checked = Convert.ToBoolean(st.beatquote);
        }
        if (st.beatquotedoc != string.Empty)
        {
            lblbeat.Visible = true;
            lblbeat.Text = st.beatquotedoc;
            lblbeat.NavigateUrl = pdfURL + "BeatQuote/" + st.beatquotedoc;
            //  RequiredFieldValidatorbeat.Visible = false;
        }
        else
        {
            lblbeat.Visible = false;
        }

        if (st.nearmap != string.Empty)
        {
            chknearmap.Checked = Convert.ToBoolean(st.nearmap);
        }

        if (st.nearmapdoc != string.Empty)
        {
            lblnearmap.Visible = true;
            lblnearmap.Text = st.nearmapdoc;
            lblnearmap.NavigateUrl = pdfURL + "NearMap/" + st.nearmapdoc;
            //RequiredFieldValidatormap.Visible = false;
        }
        else
        {
            lblnearmap.Visible = false;
        }

        chkProposedDesignSaved.Checked = Convert.ToBoolean(st.ProposedDesignSaved);
        if (st.PD != string.Empty)
        {
            lblPD.Visible = true;
            lblPD.Text = st.PD;
            //lblPD.NavigateUrl = "~/userfiles/PD/" + st.PD;
            lblPD.NavigateUrl = pdfURL + "PD/" + st.PD;
            // RequiredFieldValidatorPD.Visible = false;
        }
        else
        {
            lblPD.Visible = false;
        }
        chkPaymentReceipt.Checked = Convert.ToBoolean(st.PaymentReceipt);
        if (st.PR != string.Empty)
        {
            lblPR.Visible = true;
            lblPR.Text = st.PR;
            //lblPR.NavigateUrl = "~/userfiles/PR/" + st.PR;
            lblPR.NavigateUrl = pdfURL + "PR/" + st.PR;
            // RequiredFieldValidatorPR.Visible = false;
        }
        else
        {
            lblPR.Visible = false;
        }


        if (st.PanelBrandID != "")
        {
            txtPanelDetails.Text = st.NumberPanels + " X " + st.PanelOutput + " Watt " + st.PanelBrandName + " Panels.(" + st.PanelModel + ")";
        }
        if (st.InverterDetailsID != "")
        {
            txtInverterDetails.Text = "One " + st.InverterDetailsName + " KW Inverter. Plus " + st.SecondInverterDetails + " KW Inverter.";
        }
        if (st.SignedQuote == "True")
        {
            divSQ.Visible = true;
        }
        else
        {
            divSQ.Visible = false;
        }
        if (st.MeterBoxPhotosSaved == "True")
        {
            // divMP.Visible = true;

        }
        else
        {
            // divMP.Visible = false;
        }
        if (st.beatquote == "True")
        {
            //Spanbeat.Visible = true;
        }
        else
        {
            //Spanbeat.Visible = false;
        }
        if (st.ElecBillSaved == "True")
        {
            //divEB.Visible = true;
        }
        else
        {
            // divEB.Visible = false;
        }
        if (st.ProposedDesignSaved == "True")
        {
            //  divPD.Visible = true;
        }
        else
        {
            //divPD.Visible = false;
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            txtActiveDate.Enabled = true;
            PanActiveDate.Enabled = true;
        }
        else
        {
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller")))
            {
                if (st.ElecDistributorID == "12")
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.meterupgrade != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
                else if (st.ElecDistributorID == "13")
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.RegPlanNo != string.Empty && st.LotNumber != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.meterupgrade != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
                else
                {
                    if (chkSignedQuote.Checked == true && txtDepositReceived.Text != string.Empty && txtQuoteAcceptedQuote.Text != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty)
                    {
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            txtActiveDate.Enabled = true;
                            PanActiveDate.Enabled = true;
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                txtActiveDate.Enabled = true;
                                PanActiveDate.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        txtActiveDate.Enabled = false;
                        PanActiveDate.Enabled = false;
                    }
                }
            }
            else
            {
                txtActiveDate.Enabled = false;
                PanActiveDate.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            txtDepositReceived.Enabled = true;
            //  ImageDepRec.Visible = true;
        }
        else
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Installation Manager"))
            {
                if (st.HouseType != string.Empty && st.RoofTypeID != string.Empty && st.ElecDistributorID != string.Empty)
                {
                    txtDepositReceived.Enabled = true;
                    //  ImageDepRec.Visible = true;
                }
                else
                {
                    txtDepositReceived.Enabled = false;
                    // ImageDepRec.Visible = false;
                }
            }
        }
        try
        {
            txtActiveDate.Text = Convert.ToDateTime(st.ActiveDate).ToShortDateString();
        }
        catch { }
        try
        {
            //Response.Write(Convert.ToBoolean(st.readyactive.ToString()).ToString());
            //Response.End();
            if (st.readyactive.ToString() == "1")
            {
                chkactive.Checked = true;
            }
            else
            {
                chkactive.Checked = false;
            }
            //Convert.ToBoolean(st.readyactive.ToString());
        }
        catch { }
        DataTable dt_inv_recpt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        try
        {
            if (dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString() != string.Empty)
            {
                divrecept.Visible = true;
                lblreceipt.Text = dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString();
            }

        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        try
        {
            lblTotalPaidAmount.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
        }
        catch { }

        string totalprice = st.TotalQuotePrice;
        decimal TotalPrice = 0;
        if (totalprice != string.Empty)
        {
            TotalPrice = Convert.ToDecimal(totalprice);
        }
        decimal totalpay = 0;
        if (dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                totalpay = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            }
        }
        if (TotalPrice == 0)
        {
            lblpaymentstatus.Text = " ";
        }
        else
        {
            if (TotalPrice - totalpay > 0)
            {
                lblpaymentstatus.Text = "Owing";
            }
            else
            {
                lblpaymentstatus.Text = "Fully Paid";
            }
        }

        if (st.InvoiceNumber != string.Empty)
        {
            Panel31.Enabled = true;
            Panel32.Enabled = true;
            Panel33.Enabled = true;
        }
        else
        {
            Panel31.Enabled = false;
            Panel32.Enabled = false;
            Panel33.Enabled = false;
        }
        DataTable dt_inv = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (dt_inv.Rows.Count > 0)
        {
            Panel31.Enabled = true;
            Panel32.Enabled = true;
        }
        else
        {
            Panel31.Enabled = false;
            Panel32.Enabled = false;
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PostInstaller"))
        {
            btnOpenInvoice.Enabled = true;
            //imgbtnPerforma.Visible = true;
        }
        else
        {
            btnOpenInvoice.Enabled = true;
            // imgbtnPerforma.Visible = false;
        }
        //if (Roles.IsUserInRole("Administrator"))
        //{
        //    PanActiveDate.Enabled = true;
        //    divDepRec.Enabled = true;

        //}
        //else
        //{
        //    PanActiveDate.Enabled = false;
        //    divDepRec.Enabled = false;
        //    if (st.ActiveDate == string.Empty)
        //    {
        //        txtActiveDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //    }
        //    if (st.DepositReceived == string.Empty)
        //    {
        //        txtDepositReceived.Text = DateTime.Now.AddHours(14).ToShortDateString();
        //    }
        //}

        BindScript();

    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(Panel4, this.GetType(), "MyAction", "checkopen();", true);

    }

    protected void rptQuote_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndProjectQuoteID = (HiddenField)e.Item.FindControl("hndProjectQuoteID");
            HyperLink hypDoc = (HyperLink)e.Item.FindControl("hypDoc");
            LinkButton btnSendAMail = (LinkButton)e.Item.FindControl("btnSendAMail");
            Image Image2 = (Image)e.Item.FindControl("Image2");
            //Label lblsignchked = (Label)e.Item.FindControl("lblsignchked");
            //Label lblsignUnChked = (Label)e.Item.FindControl("lblsignUnChked");
            // System.Web.UI.WebControls.CheckBox chkSerialNo = (System.Web.UI.WebControls.CheckBox)e.Item.FindControl("chkSerialNo");


            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(hndProjectQuoteID.Value);

            int Signed = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, st.ProjectQuoteDoc);
            if (Signed == 1)
            {
                //lblsignchked.Visible = true;
                //lblsignUnChked.Visible = false;               
                btnSendAMail.Visible = true;
                btnSendAMail.PostBackUrl = "~/admin/adminfiles/company/company.aspx?m=pro&compid=" + Request.QueryString["compid"] + "&proid=" + Request.QueryString["proid"];
                Image2.Visible = true;

            }
            else
            {
                //lblsignchked.Visible = false;
                //lblsignUnChked.Visible = true;
                btnSendAMail.Visible = false;
                Image2.Visible = false;
            }

            hypDoc.NavigateUrl = pdfURL + "quotedoc/" + st.QuoteDoc;
        }
    }


    protected void rptQuote_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        if (e.CommandName.ToString() == "MailAttachedPDf")
        {
            string QuoteId = e.CommandArgument.ToString();

            #region SendMail

            TextWriter txtWriter = new StringWriter() as TextWriter;
            StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            string from = stU.from;
            String Subject = "Australian Premium Solar Signed Quotation PDF";

            Server.Execute("~/mailtemplate/QuotationAttachedMail.aspx?proid=" + ProjectID, txtWriter);

            SttblProjectQuotes st = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteID(QuoteId);
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(st2.ContactID);

            string PDFName = st2.ProjectNumber + "_" + st.QuoteDoc;

            string filepath = pdfURL + "quotedoc/" + st.QuoteDoc;
            // Utilities.SendMailWithAttachment(from, stContact.ContEmail, Subject, txtWriter.ToString(), filepath, PDFName);

            //string filepath = pdfURL + "quotedoc/" + st.QuoteDoc;
            Utilities.SendMailWithAttachment(from, "deep@meghtechnologies.com", Subject, txtWriter.ToString(), filepath, PDFName);
            MsgSendSuccess();
            try
            {
            }
            catch
            {
            }

            #endregion

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string ssactivedate = txtssdate.Text.Trim();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        bool sucQuote1 = false;

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (ssactivedate != string.Empty)
        {
            sucQuote1 = ClsProjectSale.tblProjects_UpdateQuote_SSActiveDate(ProjectID, ssactivedate);

            if (sucQuote1)
            {
                if (txtssdate.Text != string.Empty)
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("22", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "22");

                }
            }
        }
        BindProjects();
        if (sucQuote1)
        {
            PanSuccess.Visible = true;
        }
        else
        {
            PanError.Visible = true;
        }
        BindData();
        BindProjectQuote();


        /*Quote*/
        //string ProjectID = Request.QueryString["proid"];
        //string QuoteAccepted = txtQuoteAcceptedQuote.Text.Trim();
        //string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        //string InvoiceNumber = txtInvoiceNumber.Text;
        //string InvoiceSent = txtInvoiceSentquote.Text.Trim();
        //string DepositReceived = txtDepositReceived.Text.Trim();
        //string InvoiceDoc = txtInvoiceDoc.Text;
        //string MeterBoxPhotosSaved = Convert.ToString(chkMeterBoxPhotosSaved.Checked);
        //string ElecBillSaved = Convert.ToString(chkElecBillSaved.Checked);
        //string ProposedDesignSaved = Convert.ToString(chkProposedDesignSaved.Checked);
        //string PaymentReceipt = Convert.ToString(chkPaymentReceipt.Checked);
        //string nearmap = Convert.ToString(chknearmap.Checked);

        //string ActiveDate = txtActiveDate.Text.Trim();

        //string SQ = "";
        //string MP = "";
        //string EB = "";
        //string PD = "";
        //string PR = "";
        //string beat = "";
        //string map = "";


        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //if (chkactive.Checked)
        //{
        //    bool sucyactive = ClstblProjects.tblProjects_Updatereadyactive(ProjectID, chkactive.Checked.ToString());
        //}
        //if (st.ActiveDate == string.Empty)
        //{
        //    if (txtDepositReceived.Text != string.Empty)
        //    {
        //        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
        //        if (dtStatusDR.Rows.Count > 0)
        //        {
        //        }
        //        else
        //        {
        //            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //        }

        //        {
        //            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
        //            ClstblCustomers.tblCustomers_UpdateCustType("3", Request.QueryString["compid"]);
        //        }
        //    }
        //    else
        //    {
        //        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "2");
        //        if (dtStatusDR.Rows.Count > 0)
        //        {
        //            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //        }

        //        {
        //            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
        //        }
        //    }
        //}

        //if (chkSignedQuote.Checked == true)
        //{


        //    if (fuSQ.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("SQ", st.SQ);
        //        SQ = ProjectID + fuSQ.FileName;
        //        fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
        //        bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
        //        SiteConfiguration.UploadPDFFile("SQ", SQ);
        //        SiteConfiguration.deleteimage(SQ, "SQ");

        //    }

        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("SQ", st.SQ);
        //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

        //}

        //if (chkMeterBoxPhotosSaved.Checked == true)
        //{

        //    if (fuMP.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("MP", st.MP);
        //        MP = ProjectID + fuMP.FileName;
        //        fuMP.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\MP\\") + MP);
        //        bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, MP);
        //        SiteConfiguration.UploadPDFFile("MP", MP);
        //        SiteConfiguration.deleteimage(MP, "MP");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("MP", st.MP);
        //    bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, "");

        //}
        //if (chkElecBillSaved.Checked == true)
        //{

        //    if (fuEB.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("EB", st.EB);
        //        EB = ProjectID + fuEB.FileName;
        //        fuEB.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EB\\") + EB);
        //        bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, EB);
        //        SiteConfiguration.UploadPDFFile("EB", EB);
        //        SiteConfiguration.deleteimage(EB, "EB");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("EB", st.EB);
        //    bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, "");

        //}
        //if (chkProposedDesignSaved.Checked == true)
        //{
        //    if (fuPD.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("PD", st.PD);
        //        PD = ProjectID + fuPD.FileName;
        //        fuPD.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PD\\") + PD);
        //        bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, PD);
        //        SiteConfiguration.UploadPDFFile("PD", PD);
        //        SiteConfiguration.deleteimage(PD, "PD");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("PD", st.PD);
        //    bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, "");

        //}

        //if (chkPaymentReceipt.Checked == true)
        //{
        //    // RequiredFieldValidatorPR.Visible = true;
        //    if (fuPR.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("PR", st.PR);
        //        PR = ProjectID + fuPR.FileName;
        //        fuPR.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PR\\") + PR);
        //        bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, PR);
        //        SiteConfiguration.UploadPDFFile("PR", PR);
        //        SiteConfiguration.deleteimage(PR, "PR");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("PR", st.PR);
        //    bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, "");

        //}
        //if (chkbeatquote.Checked == true)
        //{
        //    // RequiredFieldValidatorbeat.Visible = true;
        //    if (fubeat.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
        //        beat = ProjectID + fubeat.FileName;
        //        fubeat.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\BeatQuote\\") + beat);
        //        bool sucbeat = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, beat);
        //        SiteConfiguration.UploadPDFFile("BeatQuote", beat);
        //        SiteConfiguration.deleteimage(beat, "BeatQuote");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
        //    bool sucMP = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, "");

        //}

        //if (chknearmap.Checked == true)
        //{
        //    //RequiredFieldValidatormap.Visible = true;
        //    if (funearmap.HasFile)
        //    {
        //        SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
        //        map = ProjectID + funearmap.FileName;
        //        //Response.Write(map);
        //        //Response.End();
        //        funearmap.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
        //        bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);

        //        SiteConfiguration.UploadPDFFile("NearMap", map);
        //        SiteConfiguration.deleteimage(map, "NearMap");
        //    }
        //}
        //else
        //{
        //    SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
        //    bool sucMP = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");

        //}

        //string UpdatedBy = stEmp.EmployeeID;
        //bool sucQuote = ClsProjectSale.tblProjects_UpdateQuote(ProjectID, QuoteAccepted, SignedQuote, InvoiceNumber, InvoiceSent, DepositReceived, InvoiceDoc, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, UpdatedBy, PaymentReceipt, ActiveDate);
        //bool succ_beatquote = ClsProjectSale.tblProjects_Updatebeatquote(ProjectID, chkbeatquote.Checked.ToString());
        //bool succ_nearmap = ClsProjectSale.tblProjects_UpdateQuotenearmapcheck(ProjectID, chknearmap.Checked.ToString());
        //string mtcepaperwork = "";
        //if (FUmtcepaperwork.HasFile)
        //{
        //    try
        //    {
        //        SiteConfiguration.DeletePDFFile("MtcePaperWork", st.mtcepaperwork);
        //    }
        //    catch
        //    {
        //    }

        //    mtcepaperwork = ProjectID + FUmtcepaperwork.FileName;
        //    FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/MtcePaperWork/") + mtcepaperwork);
        //    bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, mtcepaperwork);
        //    SiteConfiguration.UploadPDFFile("MtcePaperWork", mtcepaperwork);
        //    SiteConfiguration.deleteimage(mtcepaperwork, "MtcePaperWork");
        //}
        //else
        //{
        //    bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, st.mtcepaperwork);
        //}

        //TextWriter txtWriter = new StringWriter() as TextWriter;
        //StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");

        //SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);

        //string from = stU.from;
        //string mailto = stCont.ContEmail;
        //string subject = "Thank you for choosing Australian Premium Solar";

        //if (st.InstallCompleted == string.Empty)
        //{
        //    if (txtActiveDate.Text != string.Empty)
        //    {
        //        if (st.SurveyCerti == "1")
        //        {
        //            if (st.CertiApprove == "1")
        //            {
        //                DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
        //                if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
        //                {
        //                    if (dtStatusDR.Rows.Count > 0)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //                    }
        //                    if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
        //                    {
        //                        if (st.ProjectStatusID != "11")
        //                        {
        //                            if (st.InstallState == "VIC")
        //                            {
        //                                if (!string.IsNullOrEmpty(st.VicAppReference))
        //                                {
        //                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                                }
        //                            }
        //                            else
        //                            {
        //                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                            }
        //                        }
        //                        Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
        //                        try
        //                        {
        //                            Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
        //                                                     }
        //                        catch
        //                        {
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (st.DocumentVerified == "True")
        //                    {
        //                        if (dtStatusDR.Rows.Count > 0)
        //                        {
        //                        }
        //                        else
        //                        {
        //                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //                        }
        //                        if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
        //                        {
        //                            if (st.ProjectStatusID != "11")
        //                            {
        //                                if (st.InstallState == "VIC")
        //                                {
        //                                    if (!string.IsNullOrEmpty(st.VicAppReference))
        //                                    {
        //                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                                }
        //                            }
        //                            Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
        //                            try
        //                            {
        //                                Utilities.SendMail(from, mailto, subject, txtWriter.ToString());

        //                            }
        //                            catch
        //                            {
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
        //            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
        //            {
        //                if (dtStatusDR.Rows.Count > 0)
        //                {
        //                }
        //                else
        //                {
        //                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //                }
        //                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
        //                {
        //                    if (st.ProjectStatusID != "11")
        //                    {
        //                        if (st.InstallState == "VIC")
        //                        {
        //                            if (!string.IsNullOrEmpty(st.VicAppReference))
        //                            {
        //                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                            }
        //                        }
        //                        else
        //                        {
        //                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                        }
        //                    }
        //                    Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
        //                    try
        //                    {
        //                        Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
        //                        // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
        //                    }
        //                    catch
        //                    {
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (st.DocumentVerified == "True")
        //                {
        //                    if (dtStatusDR.Rows.Count > 0)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //                    }
        //                    if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
        //                    {
        //                        if (st.ProjectStatusID != "11")
        //                        {
        //                            if (st.InstallState == "VIC")
        //                            {
        //                                if (!string.IsNullOrEmpty(st.VicAppReference))
        //                                {
        //                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                                }
        //                            }
        //                            else
        //                            {
        //                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //                            }
        //                        }
        //                        Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
        //                        try
        //                        {
        //                            Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
        //                            // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
        //                        }
        //                        catch
        //                        {
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        if (txtDepositReceived.Text != string.Empty)
        //        {
        //            DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
        //            if (dtStatusDR.Rows.Count > 0)
        //            {
        //                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        //            }
        //            //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
        //            {
        //                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
        //            }
        //        }
        //    }
        //}

    }
    public void BindData()
    {
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        lblSQ.Visible = true;
        lblSQ.Text = st.SQ;
        //lblSQ.NavigateUrl = "~/userfiles/SQ/" + st.SQ;
        lblSQ.NavigateUrl = pdfURL + "SQ/" + st.SQ;

        lblMP.Visible = true;
        lblMP.Text = st.MP;
        //lblMP.NavigateUrl = "~/userfiles/MP/" + st.MP;
        lblMP.NavigateUrl = pdfURL + "MP/" + st.MP;

        lblEB.Visible = true;
        lblEB.Text = st.EB;
        //lblEB.NavigateUrl = "~/userfiles/EB/" + st.EB;
        lblEB.NavigateUrl = pdfURL + "EB/" + st.EB;

        lblPD.Visible = true;
        lblPD.Text = st.PD;
        //lblPD.NavigateUrl = "~/userfiles/PD/" + st.PD;
        lblPD.NavigateUrl = pdfURL + "PD/" + st.PD;

        lblPR.Visible = true;
        lblPR.Text = st.PR;
        //lblPR.NavigateUrl = "~/userfiles/PR/" + st.PR;
        lblPR.NavigateUrl = pdfURL + "PR/" + st.PR;

        lblbeat.Visible = true;
        lblbeat.Text = st.beatquotedoc;
        lblbeat.NavigateUrl = pdfURL + "BeatQuote/" + st.beatquotedoc;
    }

    protected void btnCreateInvoice_Click(object sender, EventArgs e)
    {

        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //Response.Write(ProjectID);
        //Response.End();
        string InvoiceDoc = ProjectID + "Quotation.pdf";
        if (ProjectID != "")
        {
            bool suc = ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, st.ProjectNumber);

            if (suc)
            {
                Session["btnShowHide"] = "true";
                btnCreateInvoice.Visible = false;
                btnOpenInvoice.Visible = true;
            }
            else
            {
                btnCreateInvoice.Visible = true;
                btnOpenInvoice.Visible = false;
            }
        }
        //Response.Redirect(Request.RawUrl);
        Button btnAdd = InvoicePayments1.FindControl("btnInvPay") as Button;

        if (btnAdd != null)
        {
            btnAdd.Visible = true;
        }
        BindInvoice();
        BindProjectQuote();
    }

    protected void btnOpenInvoice_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        ClsTelerik_reports.Generate_PaymentReceipt(ProjectID);
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //if (st.ProjectTypeID == "8")
        //{
        //    // Temporarily Commented to check Report //
        //    Telerik_reports.generate_mtceinvoice(ProjectID, "Download");

        //    //Telerik_reports.generate_PDFReport();

        //}
        //else
        //{
        //    // Temporarily Commented to check Report //
        //    Telerik_reports.generate_Taxinvoice(ProjectID, "Download");

        //    // Telerik_reports.generate_PDFReport();
        //}

        BindProjectQuote();
        BindInvoice();
        BindScript();
    }

    public void UpdateQuote()
    {
        string ProjectID = Request.QueryString["proid"];
        string QuoteAccepted = txtQuoteAcceptedQuote.Text.Trim();
        string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        string InvoiceNumber = txtInvoiceNumber.Text;
        string InvoiceSent = txtInvoiceSentquote.Text.Trim();
        string DepositReceived = txtDepositReceived.Text.Trim();
        string InvoiceDoc = txtInvoiceDoc.Text;
        //string WelcomeLetterDone = Convert.ToString(chkWelcomeLetterDone.Checked);
        string MeterBoxPhotosSaved = Convert.ToString(chkMeterBoxPhotosSaved.Checked);
        //Response.Write("grgersh");
        //Response.End();
        string ElecBillSaved = Convert.ToString(chkElecBillSaved.Checked);
        string ProposedDesignSaved = Convert.ToString(chkProposedDesignSaved.Checked);
        string PaymentReceipt = Convert.ToString(chkPaymentReceipt.Checked);
        string nearmap = Convert.ToString(chknearmap.Checked);

        string ActiveDate = txtActiveDate.Text.Trim();

        string SQ = "";
        string MP = "";
        string EB = "";
        string PD = "";
        string PR = "";
        string beat = "";
        string map = "";


        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //====
        if (chkactive.Checked)
        {
            bool sucyactive = ClstblProjects.tblProjects_Updatereadyactive(ProjectID, chkactive.Checked.ToString());
        }
        //tblProjects_Updatereadyactive
        //======
        /* ----------------------- Deposite Rec Status ----------------------- */
        if (st.ActiveDate == string.Empty)
        {
            if (txtDepositReceived.Text != string.Empty)
            {
                DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                if (dtStatusDR.Rows.Count > 0)
                {
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                    ClstblCustomers.tblCustomers_UpdateCustType("3", Request.QueryString["compid"]);
                }
            }
            else
            {
                DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "2");
                if (dtStatusDR.Rows.Count > 0)
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                {
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
                }
            }
        }
        /* -------------------------------------------------------------- */

        if (chkSignedQuote.Checked == true)
        {
            // Response.Write("hii");
            //if (st.SQ == string.Empty)
            //{
            //    RequiredFieldValidatorSQ.Visible = true;
            //}
            //else
            //{
            //    RequiredFieldValidatorSQ.Visible = false;
            //}

            if (fuSQ.HasFile)
            {
                SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                SQ = ProjectID + fuSQ.FileName;
                fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
                bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
                SiteConfiguration.UploadPDFFile("SQ", SQ);
                SiteConfiguration.deleteimage(SQ, "SQ");

            }
            //else
            //{
            //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, st.SQ);
            //}
        }
        else
        {
            SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

            //RequiredFieldValidatorSQ.Visible = false;
        }

        if (chkMeterBoxPhotosSaved.Checked == true)
        {
            // RequiredFieldValidatorMP.Visible = true;
            if (fuMP.HasFile)
            {
                SiteConfiguration.DeletePDFFile("MP", st.MP);
                MP = ProjectID + fuMP.FileName;
                fuMP.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\MP\\") + MP);
                bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, MP);
                SiteConfiguration.UploadPDFFile("MP", MP);
                SiteConfiguration.deleteimage(MP, "MP");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("MP", st.MP);
            bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, "");
            // RequiredFieldValidatorMP.Visible = false;
        }
        if (chkElecBillSaved.Checked == true)
        {
            //RequiredFieldValidatorEB.Visible = true;
            if (fuEB.HasFile)
            {
                SiteConfiguration.DeletePDFFile("EB", st.EB);
                EB = ProjectID + fuEB.FileName;
                fuEB.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EB\\") + EB);
                bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, EB);
                SiteConfiguration.UploadPDFFile("EB", EB);
                SiteConfiguration.deleteimage(EB, "EB");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("EB", st.EB);
            bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, "");
            // RequiredFieldValidatorEB.Visible = false;
        }
        if (chkProposedDesignSaved.Checked == true)
        {
            // RequiredFieldValidatorPD.Visible = true;
            if (fuPD.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PD", st.PD);
                PD = ProjectID + fuPD.FileName;
                fuPD.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PD\\") + PD);
                bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, PD);
                SiteConfiguration.UploadPDFFile("PD", PD);
                SiteConfiguration.deleteimage(PD, "PD");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PD", st.PD);
            bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, "");
            // RequiredFieldValidatorPD.Visible = false;
        }

        if (chkPaymentReceipt.Checked == true)
        {
            // RequiredFieldValidatorPR.Visible = true;
            if (fuPR.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PR", st.PR);
                PR = ProjectID + fuPR.FileName;
                fuPR.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PR\\") + PR);
                bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, PR);
                SiteConfiguration.UploadPDFFile("PR", PR);
                SiteConfiguration.deleteimage(PR, "PR");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PR", st.PR);
            bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, "");
            // RequiredFieldValidatorPR.Visible = false;
        }
        if (chkbeatquote.Checked == true)
        {
            // RequiredFieldValidatorbeat.Visible = true;
            if (fubeat.HasFile)
            {
                SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
                beat = ProjectID + fubeat.FileName;
                fubeat.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\BeatQuote\\") + beat);
                bool sucbeat = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, beat);
                SiteConfiguration.UploadPDFFile("BeatQuote", beat);
                SiteConfiguration.deleteimage(beat, "BeatQuote");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
            bool sucMP = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, "");
            // RequiredFieldValidatorbeat.Visible = false;
        }

        if (chknearmap.Checked == true)
        {
            //RequiredFieldValidatormap.Visible = true;
            if (funearmap.HasFile)
            {
                SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
                map = ProjectID + funearmap.FileName;
                //Response.Write(map);
                //Response.End();
                funearmap.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
                bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);

                SiteConfiguration.UploadPDFFile("NearMap", map);
                SiteConfiguration.deleteimage(map, "NearMap");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
            bool sucMP = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
            //RequiredFieldValidatormap.Visible = false;
        }

        string UpdatedBy = stEmp.EmployeeID;
        bool sucQuote = ClsProjectSale.tblProjects_UpdateQuote(ProjectID, QuoteAccepted, SignedQuote, InvoiceNumber, InvoiceSent, DepositReceived, InvoiceDoc, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, UpdatedBy, PaymentReceipt, ActiveDate);
        bool succ_beatquote = ClsProjectSale.tblProjects_Updatebeatquote(ProjectID, chkbeatquote.Checked.ToString());
        bool succ_nearmap = ClsProjectSale.tblProjects_UpdateQuotenearmapcheck(ProjectID, chknearmap.Checked.ToString());
        string mtcepaperwork = "";
        if (FUmtcepaperwork.HasFile)
        {
            try
            {
                SiteConfiguration.DeletePDFFile("MtcePaperWork", st.mtcepaperwork);
            }
            catch
            {
            }

            mtcepaperwork = ProjectID + FUmtcepaperwork.FileName;
            FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/MtcePaperWork/") + mtcepaperwork);
            bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, mtcepaperwork);
            SiteConfiguration.UploadPDFFile("MtcePaperWork", mtcepaperwork);
            SiteConfiguration.deleteimage(mtcepaperwork, "MtcePaperWork");
        }
        else
        {
            //bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, st.mtcepaperwork);
        }
        /* ----------------------- Active Status ----------------------- */
        TextWriter txtWriter = new StringWriter() as TextWriter;
        StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");

        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //Response.Write(st.ContactID);
        //Response.End();
        string from = stU.from;
        string mailto = stCont.ContEmail;
        string subject = "Thank you for choosing Australian Premium Solar";
        //===for test
        ////Server.Execute("~/mailtemplete/activemail.aspx?Customer=" + st.Customer, txtWriter);
        ////try
        ////{

        ////  //  Response.Write(from + "======" + mailto + "======" + subject + "======" + txtWriter.ToString());
        ////    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());

        ////}
        ////catch
        ////{
        ////}
        // Response.End();
        //=
        if (st.InstallCompleted == string.Empty)
        {
            if (txtActiveDate.Text != string.Empty)
            {
                if (st.SurveyCerti == "1")
                {
                    if (st.CertiApprove == "1")
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                try
                                {
                                    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //   Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                }
                                catch
                                {
                                }
                            }
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID != "11")
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        else
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    try
                                    {
                                        Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                        //  Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                    if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                    {
                        if (dtStatusDR.Rows.Count > 0)
                        {
                        }
                        else
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        }
                        if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                        {
                            if (st.ProjectStatusID != "11")
                            {
                                if (st.InstallState == "VIC")
                                {
                                    if (!string.IsNullOrEmpty(st.VicAppReference))
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                else
                                {
                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                }
                            }
                            Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                            try
                            {
                                Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                try
                                {
                                    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (txtDepositReceived.Text != string.Empty)
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                    }
                }
            }
        }
        /* ------------------------------------------------------------- */
        //=====put by roshni====///
        //if (st.SignedQuote != string.Empty && st.DepositReceived != string.Empty && st.QuoteAccepted != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.DocumentVerified != string.Empty)
        //{
        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //}


        //======


        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Rep"))
        {
            try
            {
                ClstblProjects.tblProjects_UpdateQuoteSentDate(ProjectID, txtQuoteSent.Text);
            }
            catch
            {
            }
        }

        if (sucQuote)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindScript();
        BindData();
        BindProjectQuote();
        activemethod();
        BindProjects();
    }
    protected void btnUpdateQuote_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string QuoteAccepted = txtQuoteAcceptedQuote.Text.Trim();
        string SignedQuote = Convert.ToString(chkSignedQuote.Checked);
        string InvoiceNumber = txtInvoiceNumber.Text;
        string InvoiceSent = txtInvoiceSentquote.Text.Trim();
        string DepositReceived = txtDepositReceived.Text.Trim();
        string InvoiceDoc = txtInvoiceDoc.Text;
        //string WelcomeLetterDone = Convert.ToString(chkWelcomeLetterDone.Checked);
        string MeterBoxPhotosSaved = Convert.ToString(chkMeterBoxPhotosSaved.Checked);
        //Response.Write("grgersh");
        //Response.End();
        string ElecBillSaved = Convert.ToString(chkElecBillSaved.Checked);
        string ProposedDesignSaved = Convert.ToString(chkProposedDesignSaved.Checked);
        string PaymentReceipt = Convert.ToString(chkPaymentReceipt.Checked);
        string nearmap = Convert.ToString(chknearmap.Checked);

        string ActiveDate = txtActiveDate.Text.Trim();

        string SQ = "";
        string MP = "";
        string EB = "";
        string PD = "";
        string PR = "";
        string beat = "";
        string map = "";


        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //====
        if (chkactive.Checked)
        {
            bool sucyactive = ClstblProjects.tblProjects_Updatereadyactive(ProjectID, chkactive.Checked.ToString());
        }
        //tblProjects_Updatereadyactive
        //======
        /* ----------------------- Deposite Rec Status ----------------------- */
        if (st.ActiveDate == string.Empty)
        {
            if (txtDepositReceived.Text != string.Empty)
            {
                DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                if (dtStatusDR.Rows.Count > 0)
                {
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                    ClstblCustomers.tblCustomers_UpdateCustType("3", Request.QueryString["compid"]);
                }
            }
            else
            {
                DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "2");
                if (dtStatusDR.Rows.Count > 0)
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                {
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "2");
                }
            }
        }
        /* -------------------------------------------------------------- */

        if (chkSignedQuote.Checked == true)
        {
            // Response.Write("hii");
            //if (st.SQ == string.Empty)
            //{
            //    RequiredFieldValidatorSQ.Visible = true;
            //}
            //else
            //{
            //    RequiredFieldValidatorSQ.Visible = false;
            //}

            if (fuSQ.HasFile)
            {
                SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                SQ = ProjectID + fuSQ.FileName;
                fuSQ.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/SQ/") + SQ);
                bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
                SiteConfiguration.UploadPDFFile("SQ", SQ);
                SiteConfiguration.deleteimage(SQ, "SQ");

            }
            //else
            //{
            //    bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, st.SQ);
            //}
        }
        else
        {
            SiteConfiguration.DeletePDFFile("SQ", st.SQ);
            bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, "");

            //RequiredFieldValidatorSQ.Visible = false;
        }

        if (chkMeterBoxPhotosSaved.Checked == true)
        {
            // RequiredFieldValidatorMP.Visible = true;
            if (fuMP.HasFile)
            {
                SiteConfiguration.DeletePDFFile("MP", st.MP);
                MP = ProjectID + fuMP.FileName;
                fuMP.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\MP\\") + MP);
                bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, MP);
                SiteConfiguration.UploadPDFFile("MP", MP);
                SiteConfiguration.deleteimage(MP, "MP");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("MP", st.MP);
            bool sucMP = ClsProjectSale.tblProjects_UpdateMP(ProjectID, "");
            // RequiredFieldValidatorMP.Visible = false;
        }
        if (chkElecBillSaved.Checked == true)
        {
            //RequiredFieldValidatorEB.Visible = true;
            if (fuEB.HasFile)
            {
                SiteConfiguration.DeletePDFFile("EB", st.EB);
                EB = ProjectID + fuEB.FileName;
                fuEB.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\EB\\") + EB);
                bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, EB);
                SiteConfiguration.UploadPDFFile("EB", EB);
                SiteConfiguration.deleteimage(EB, "EB");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("EB", st.EB);
            bool sucEB = ClsProjectSale.tblProjects_UpdateEB(ProjectID, "");
            // RequiredFieldValidatorEB.Visible = false;
        }
        if (chkProposedDesignSaved.Checked == true)
        {
            // RequiredFieldValidatorPD.Visible = true;
            if (fuPD.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PD", st.PD);
                PD = ProjectID + fuPD.FileName;
                fuPD.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PD\\") + PD);
                bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, PD);
                SiteConfiguration.UploadPDFFile("PD", PD);
                SiteConfiguration.deleteimage(PD, "PD");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PD", st.PD);
            bool sucPD = ClsProjectSale.tblProjects_UpdatePD(ProjectID, "");
            // RequiredFieldValidatorPD.Visible = false;
        }

        if (chkPaymentReceipt.Checked == true)
        {
            // RequiredFieldValidatorPR.Visible = true;
            if (fuPR.HasFile)
            {
                SiteConfiguration.DeletePDFFile("PR", st.PR);
                PR = ProjectID + fuPR.FileName;
                fuPR.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\PR\\") + PR);
                bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, PR);
                SiteConfiguration.UploadPDFFile("PR", PR);
                SiteConfiguration.deleteimage(PR, "PR");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("PR", st.PR);
            bool sucPR = ClsProjectSale.tblProjects_UpdatePR(ProjectID, "");
            // RequiredFieldValidatorPR.Visible = false;
        }
        if (chkbeatquote.Checked == true)
        {
            // RequiredFieldValidatorbeat.Visible = true;
            if (fubeat.HasFile)
            {
                SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
                beat = ProjectID + fubeat.FileName;
                fubeat.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\BeatQuote\\") + beat);
                bool sucbeat = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, beat);
                SiteConfiguration.UploadPDFFile("BeatQuote", beat);
                SiteConfiguration.deleteimage(beat, "BeatQuote");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("BeatQuote", st.beatquotedoc);
            bool sucMP = ClsProjectSale.tblProjects_UpdateBeatQuoteDoc(ProjectID, "");
            // RequiredFieldValidatorbeat.Visible = false;
        }

        if (chknearmap.Checked == true)
        {
            //RequiredFieldValidatormap.Visible = true;
            if (funearmap.HasFile)
            {
                SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
                map = ProjectID + funearmap.FileName;
                //Response.Write(map);
                //Response.End();
                funearmap.SaveAs(Request.PhysicalApplicationPath + ("\\userfiles\\NearMap\\") + map);
                bool sucmap = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, map);

                SiteConfiguration.UploadPDFFile("NearMap", map);
                SiteConfiguration.deleteimage(map, "NearMap");
            }
        }
        else
        {
            SiteConfiguration.DeletePDFFile("NearMap", st.nearmapdoc);
            bool sucMP = ClsProjectSale.tblProjects_Updatenearmapdoc(ProjectID, "");
            //RequiredFieldValidatormap.Visible = false;
        }

        string UpdatedBy = stEmp.EmployeeID;
        bool sucQuote = ClsProjectSale.tblProjects_UpdateQuote(ProjectID, QuoteAccepted, SignedQuote, InvoiceNumber, InvoiceSent, DepositReceived, InvoiceDoc, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, UpdatedBy, PaymentReceipt, ActiveDate);
        bool succ_beatquote = ClsProjectSale.tblProjects_Updatebeatquote(ProjectID, chkbeatquote.Checked.ToString());
        bool succ_nearmap = ClsProjectSale.tblProjects_UpdateQuotenearmapcheck(ProjectID, chknearmap.Checked.ToString());
        string mtcepaperwork = "";
        if (FUmtcepaperwork.HasFile)
        {
            try
            {
                SiteConfiguration.DeletePDFFile("MtcePaperWork", st.mtcepaperwork);
            }
            catch
            {
            }

            mtcepaperwork = ProjectID + FUmtcepaperwork.FileName;
            FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/MtcePaperWork/") + mtcepaperwork);
            bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, mtcepaperwork);
            SiteConfiguration.UploadPDFFile("MtcePaperWork", mtcepaperwork);
            SiteConfiguration.deleteimage(mtcepaperwork, "MtcePaperWork");
        }
        else
        {
            bool sucSQ = ClstblProjects.tblProjects_Updatemtcepaperwork(ProjectID, st.mtcepaperwork);
        }
        /* ----------------------- Active Status ----------------------- */
        TextWriter txtWriter = new StringWriter() as TextWriter;
        StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");

        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //Response.Write(st.ContactID);
        //Response.End();
        string from = stU.from;
        string mailto = stCont.ContEmail;
        string subject = "Thank you for choosing Australian Premium Solar";
        //===for test
        ////Server.Execute("~/mailtemplete/activemail.aspx?Customer=" + st.Customer, txtWriter);
        ////try
        ////{

        ////  //  Response.Write(from + "======" + mailto + "======" + subject + "======" + txtWriter.ToString());
        ////    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());

        ////}
        ////catch
        ////{
        ////}
        // Response.End();
        //=
        if (st.InstallCompleted == string.Empty)
        {
            if (txtActiveDate.Text != string.Empty)
            {
                if (st.SurveyCerti == "1")
                {
                    if (st.CertiApprove == "1")
                    {
                        DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                        if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                try
                                {
                                    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    //   Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                }
                                catch
                                {
                                }
                            }
                        }
                        else
                        {
                            if (st.DocumentVerified == "True")
                            {
                                if (dtStatusDR.Rows.Count > 0)
                                {
                                }
                                else
                                {
                                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                                }
                                if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                                {
                                    if (st.ProjectStatusID != "11")
                                    {
                                        if (st.InstallState == "VIC")
                                        {
                                            if (!string.IsNullOrEmpty(st.VicAppReference))
                                            {
                                                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                            }
                                        }
                                        else
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                    try
                                    {
                                        Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                        //  Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                    if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
                    {
                        if (dtStatusDR.Rows.Count > 0)
                        {
                        }
                        else
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        }
                        if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                        {
                            if (st.ProjectStatusID != "11")
                            {
                                if (st.InstallState == "VIC")
                                {
                                    if (!string.IsNullOrEmpty(st.VicAppReference))
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                else
                                {
                                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                }
                            }
                            Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                            try
                            {
                                Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            if (dtStatusDR.Rows.Count > 0)
                            {
                            }
                            else
                            {
                                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                            }
                            if (chkSignedQuote.Checked.ToString() == true.ToString() && txtQuoteAcceptedQuote.Text != string.Empty && st.NMINumber != string.Empty && st.ElecDistApprovelRef != string.Empty)
                            {
                                if (st.ProjectStatusID != "11")
                                {
                                    if (st.InstallState == "VIC")
                                    {
                                        if (!string.IsNullOrEmpty(st.VicAppReference))
                                        {
                                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                        }
                                    }
                                    else
                                    {
                                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                                    }
                                }
                                Server.Execute("~/mailtemplate/activemail.aspx?Customer=" + st.Customer, txtWriter);
                                try
                                {
                                    Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                                    // Utilities.SendMail(from, "roshnisuvagiya@meghtechnologies.com", subject, txtWriter.ToString());
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (txtDepositReceived.Text != string.Empty)
                {
                    DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
                    if (dtStatusDR.Rows.Count > 0)
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    }
                    //if (Convert.ToDateTime(st.DepositReceived).ToShortDateString() != Convert.ToDateTime(txtDepositReceived.Text).ToShortDateString())
                    {
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                    }
                }
            }
        }
        /* ------------------------------------------------------------- */
        //=====put by roshni====///
        //if (st.SignedQuote != string.Empty && st.DepositReceived != string.Empty && st.QuoteAccepted != string.Empty && st.ElecDistApprovelRef != string.Empty && st.NMINumber != string.Empty && st.DocumentVerified != string.Empty)
        //{
        //    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        //}


        //======


        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Rep"))
        {
            try
            {
                ClstblProjects.tblProjects_UpdateQuoteSentDate(ProjectID, txtQuoteSent.Text);
            }
            catch
            {
            }
        }
        if (st.ProjectStatusID == "2")
        {
            if (txtActiveDate.Text != null && txtActiveDate.Text != "")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
            }
        }
        if (st.ProjectStatusID == "3")
        {
            if (txtDepositReceived.Text != null && txtDepositReceived.Text != "")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
            }
        }

        if (sucQuote)
        {
            BindScript();
            BindData();
            BindProjectQuote();
            activemethod();
            BindProjects();
            SetAdd1();
            //activemethod();

            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }

    }

    protected void txtOther_TextChanged(object sender, EventArgs e)
    {
        string value1 = txttotalamt.Text;
        string value2 = txtOther.Text;
        if (value1 != null && value1 != "" && value2 != null && value2 != "")
        {
            decimal total = Convert.ToDecimal(value1) + Convert.ToDecimal(value2);

            decimal gstAmt1 = Math.Round(total, 2);
            txtnettotal.Text = gstAmt1.ToString();
        }
    }
    //protected void txtDepoReq_TextChanged(object sender, EventArgs e)
    //{
    //    string value1 = txttotalamt.Text;
    //    string value2 = txtDepoReq.Text;
    //    if (!string.IsNullOrEmpty(value1) && !string.IsNullOrEmpty(value2))
    //    {
    //        decimal total = Convert.ToDecimal(value1) - Convert.ToDecimal(value2);

    //        decimal gstAmt1 = Math.Round(total, 2);
    //        txtnettotal.Text = gstAmt1.ToString();
    //    }
    //}

    protected void txtsize_TextChanged(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        PanAddUpdate.Visible = true;
        decimal InvSize = 0;
        decimal PnlSysCap = 0;
        if (txtSystemCapacity.Text != null && txtSystemCapacity.Text != "")
        {
            PnlSysCap = Convert.ToDecimal(txtSystemCapacity.Text);
        }

        decimal min = 0;
        if (InvSize < PnlSysCap)
        {
            min = InvSize;
            if (txtNoOfPanels.Text != string.Empty)
            {
                // capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * InvSize) / 1000;
                //txtSystemCapacity.Text = Convert.ToString(capacity);

                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(InvSize);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                var actCost1 = cap * Convert.ToDouble(stpricenew.PricePerKW);
                txtRebate.Text = actCost1.ToString();
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    //txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                }

            }
        }
        else
        {
            min = PnlSysCap;
            if (txtNoOfPanels.Text != string.Empty)
            {
                capacity = (Convert.ToDecimal(txtNoOfPanels.Text) * PnlSysCap) / 1000;
                txtSystemCapacity.Text = Convert.ToString(capacity);

                string installbooking = stPro.InstallBookingDate;
                double cap = Convert.ToDouble(capacity);

                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    txtactualamt.Text = Convert.ToString(subcap1 + subcap2);
                    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtRebate.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    fortysubsidy.Text = Convert.ToString(subcap1);
                    twntysubsidy.Text = Convert.ToString(subcap2);
                    txtactualamt.Text = Convert.ToString(subcap1 + subcap2 + subcap3);
                    txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2 + subcap3));
                }

            }
        }
    }


    public bool Attachfile()
    {
        bool result = false;
        string ProjectID = Request.QueryString["proid"];
        string CustomerID = Request.QueryString["compid"];
        string ApplicationNo = txtdistributorapplicationnumber.Text;
        string ApplicationDate = txtElecDistApplied.Text;
        string DocumentId = "19";
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Filename = "";
        string PDFFilename = "";
        try
        {
            if (FUmtcepaperwork.HasFile)
            {
                PDFFilename = string.Empty;
                Filename = FUmtcepaperwork.FileName;
                //SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                PDFFilename = ProjectID + "_" + Filename;
                FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                result = true;
            }
            else
            {
                PDFFilename = hdnFileName.Value.ToString();
                //SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                FUmtcepaperwork.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);

                result = true;
            }
        }
        catch (Exception ex)
        {
            //throw ex;
        }
        var filepath = ProjectID + "_" + Filename;

        int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, ApplicationNo, PDFFilename, UpdatedBy, DateTime.Now);
        return result;
    }

}