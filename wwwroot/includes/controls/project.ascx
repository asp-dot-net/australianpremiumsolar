<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="project.ascx.cs" Inherits="includes_controls_project" %>
<%@ Register Src="projectdetail.ascx" TagName="projectdetail" TagPrefix="uc1" %>
<%@ Register Src="projectprice.ascx" TagName="projectprice" TagPrefix="uc2" %>
<%--<%@ Register Src="projectsale.ascx" TagName="projectsale" TagPrefix="uc3" %>--%>
<%@ Register Src="projectquote.ascx" TagName="projectquote" TagPrefix="uc4" %>
<%@ Register Src="projectpreinst.ascx" TagName="projectpreinst" TagPrefix="uc5" %>
<%@ Register Src="projectpostinst.ascx" TagName="projectpostinst" TagPrefix="uc6" %>
<%@ Register Src="projectelecinv.ascx" TagName="projectelecinv" TagPrefix="uc7" %>
<%@ Register Src="projectrefund.ascx" TagName="projectrefund" TagPrefix="uc8" %>
<%@ Register Src="projectforms.ascx" TagName="projectforms" TagPrefix="uc9" %>
<%@ Register Src="projectfinance.ascx" TagName="projectfinance" TagPrefix="uc10" %>
<%@ Register Src="projectstc.ascx" TagName="projectstc" TagPrefix="uc11" %>
<%@ Register Src="projectdocs.ascx" TagName="projectdocs" TagPrefix="uc12" %>
<%@ Register Src="projectmtce.ascx" TagName="projectmtce" TagPrefix="uc13" %>
<%@ Register Src="projectmaintenance.ascx" TagName="projectmaintenance" TagPrefix="uc14" %>
<%--<%@ Register Src="projectvicdoc.ascx" TagName="projectvicdoc" TagPrefix="uc15" %>--%>
<%@ Register Src="projectTest.ascx" TagName="projectProject1" TagPrefix="uc16" %>
<%@ Register Src="Projectdocuments.ascx" TagName="Projectdocuments" TagPrefix="uc17" %>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<%-- <script src="../../assets/js/jquery.min.js"></script> --%>


<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }
</style>
<script>

    function formValidate() {

        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function InActiveLoader() {
        //hide the modal popup - the update progress
        $('.loading-container').addClass('loading-inactive');
    }
    function ActiveLoader() {
        //shows the modal popup - the update progress
        $('.loading-container').removeClass('loading-inactive');
    }
</script>

<script type="text/javascript">


    function InstallAutopostback() {

        $("#<%=txtInstallAddressline.ClientID %>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "address.aspx",
                    dataType: "jsonp",
                    data: {
                        s: 'auto',
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                //                    log(ui.item ?
                //					"Selected: " + ui.item.label :
                //					"Nothing selected, input was " + this.value);
            },
            open: function () {
                //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    }

    function getDetailInstallParsedAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'address',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data != null) {
                    document.getElementById("<%= txtInstallAddress.ClientID %>").value = data.StreetLine;
                    document.getElementById("<%= txtStreetPostCode.ClientID %>").value = data.Postcode;
                    document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                    document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                    document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                    document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                    document.getElementById("<%= ddlCity.ClientID %>").value = data.Suburb;
                    document.getElementById("<%= ddlStreetState.ClientID %>").value = data.State;
                    document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                    document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                }
                validateAddress();
            }
        });
    }

    function validateProjectAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'validate',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data == true) {
                    document.getElementById("validaddressid").style.display = "block";
                    document.getElementById("invalidaddressid").style.display = "none";

                    document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                }
                else {
                    document.getElementById("validaddressid").style.display = "none";
                    document.getElementById("invalidaddressid").style.display = "block";
                    document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                }
            }
        });
    }
    function ChkFun(source, args) {
        validateProjectAddress();
        var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

        if (elem == '1') {
            args.IsValid = true;
        }
        else {

            args.IsValid = false;
        }
    }


</script>

<%--<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>--%>

<section class="row m-b-md customerProjectTab" id="panprojects" runat="server">
    <div class="col-sm-12">
        <%--<h3 class="m-b-xs text-black">
            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>&nbsp;Projects</h3>--%>
        <div class="contactsarea boxPadding">
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litError" runat="server"
                        Text="Transaction Failed."></asp:Literal></strong>
                </div>
                <%-- <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
                <div class="alert alert-danger" id="PanAddressValid" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Address is not verified. Please verify the address first.</strong>
                </div>
            </div>
            <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default formGrid">
                            <div class="panel-body">
                                <div class="paddall">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <h4 class="formHeading widget-caption">Add New Project</h4>

                                            <div class="">
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Project Type<span class="symbol required"></span>
                                                            </label>
                                                        </span>
                                                        <div class="drpValidate">

                                                            <asp:DropDownList ID="ddlProjectTypeID" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlProjectTypeID_SelectedIndexChanged" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="ddlProjectTypeID" Display="Dynamic" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-3" id="divSalesRep" runat="server" visible="false">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Sales Rep<span class="symbol required"></span>
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="ddlSalesRep" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div runat="server" class="form-group col-sm-3" id="divoldproject">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Old Project No.
                                                            </label>
                                                        </span><span>
                                                            <%--<asp:TextBox ID="txtOldProjectNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>--%>
                                                            <asp:DropDownList runat="server" ID="ddllinkprojectid" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="ddllinkprojectid" Display="Dynamic" Visible="false" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Manual Quote No.
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtManualQuoteNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Contact<span class="symbol required"></span>
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlContact" runat="server" aria-controls="DataTables_Table_0" class="myvalcomp"
                                                                AppendDataBoundItems="true">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="ddlContact" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4 dateimgarea">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Project Opened
                                                            </label>
                                                        </span><span class="dateimg">
                                                            <asp:TextBox ID="txtProjectOpened" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Install Site
                                                            </label>
                                                        </span>
                                                        <div id="Div1" class="form-group" visible="false" runat="server">
                                                            <asp:HiddenField ID="hndaddress" runat="server" />
                                                            <asp:TextBox ID="txtInstallAddressline" runat="server" CssClass="form-control" onblur="getDetailInstallParsedAddress();"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="txtInstallAddressline" Display="Dynamic" Enabled="false"></asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="CustomInstallAddressline" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                        </div>


                                                        <asp:Panel runat="server" ID="PanInstallAddress">
                                                            <asp:TextBox ID="txtInstallAddress" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                ControlToValidate="txtInstallAddress" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>
                                                        </asp:Panel>


                                                        <asp:HiddenField ID="hndstreetno" runat="server" />
                                                        <asp:HiddenField ID="hndstreetname" runat="server" />
                                                        <asp:HiddenField ID="hndstreettype" runat="server" />
                                                        <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                        <asp:HiddenField ID="hndunittype" runat="server" />
                                                        <asp:HiddenField ID="hndunitno" runat="server" />
                                                        <div id="validaddressid" style="display: none">
                                                            <%-- <img src="../../../images/check.png" alt="check">--%>
                                                            <i class="fa fa-check"></i>
                                                            Address is valid.
                                                        </div>
                                                        <div id="invalidaddressid" style="display: none">
                                                            <%--<img src="../../../images/x.png" alt="cross">--%>
                                                            <i class="fa fa-close"></i>
                                                            Address is invalid.
                                                        </div>
                                                        <div class="onelindiv row">


                                                            <div class="form-group col-md-6" id="divNearby" runat="server" style="display: none">
                                                                <asp:Label ID="Label10" runat="server">Near by</asp:Label>
                                                                <div>
                                                                    <asp:TextBox ID="txtformbayNearby" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayNearby_TextChanged"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="txtformbayNearby" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6" visible="false" id="divsttype" runat="server" style="display: none">
                                                                <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                                <div id="Div8" class="drpValidate" runat="server">
                                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnTextChanged="ddlformbaystreettype_SelectedIndexChanged">

                                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>




                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <div class="" id="divStreetno" runat="server">
                                                            <asp:Label ID="Label12" runat="server">Address 1</asp:Label>
                                                            <div>
                                                                <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group spicaldivin col-md-4 streatfield" id="divstname" runat="server">
                                                        <asp:Label ID="Label3" runat="server">Address 2</asp:Label>


                                                        <div class="autocompletedropdown">
                                                            <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                            <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                                ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                                ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="form-group col-md-4" id="Div9" runat="server">
                                                        <asp:Label ID="Label15" runat="server">Street State</asp:Label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlStreetState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <asp:ListItem Value="0"> State</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="ddlStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4" id="Div24" runat="server" style="display: none">
                                                        <asp:Label ID="Label35" runat="server">District</asp:Label>
                                                        <div class="autocompletedropdown">
                                                            <asp:DropDownList ID="ddlDistrict" runat="server" AppendDataBoundItems="true" CssClass="myvalproject">
                                                                <asp:ListItem Value="0">Select District</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4" id="Div3" runat="server">
                                                        <asp:Label ID="Label4" runat="server">Taluka</asp:Label>
                                                        <div>
                                                            <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox" OnTextChanged="ddltaluka_TextChanged" AutoPostBack="true" onchange="ActiveLoader()"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                    <div class="col-sm-4 form-group" id="seq" runat="server">
                                                        <asp:Label ID="lblMobile" runat="server"> City</asp:Label>
                                                        <div class="autocompletedropdown">
                                                            <div>
                                                                <asp:TextBox runat="server" ID="ddlCity" Enabled="true" class="form-control modaltextbox" AutoPostBack="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddlCity" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllCity"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                
                                                 
                                                    <div class="form-group col-md-4 padleftzero">
                                                        <div class="drpValidate">
                                                            <div class="marginbtm15 col-md-16" id="Div10" runat="server">
                                                                <asp:Label ID="Label16" runat="server">Post Code</asp:Label>
                                                                <div>
                                                                    <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="true" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="ddlformbaypostalcode_SelectedIndexChanged"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="txtStreetPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4 textareaboxheight">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Project Notes
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtProjectNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4 textareaboxheight">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Installer Notes
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4 textareaboxheight">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Notes for Installation Department
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtMeterInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="dividerLine"></div>
                                    </div>
                                    <div class="col-md-12 textcenterbutton">
                                        <div class="textRight">
                                            <asp:Button class="btn largeButton greenBtn addwhiteicon redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                Text="Add" CausesValidation="true" ValidationGroup="project" />
                                            <asp:Button class="btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                Text="Save" Visible="true" />
                                            <asp:Button class="btn largeButton whiteBtn resetbutton btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                CausesValidation="false" Text="Reset" />
                                            <asp:Button class="btn largeButton blueBtn calcelwhiteicon btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                CausesValidation="false" Text="Cancel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="PanAddUpdate">
                <div class="contacttoparea" id="PanSearch" runat="server">
                    <div class="form-inline searchfinal">
                        <div class="paddall15 Responsive-search wid100">
                            <div class="flexRow">
                                <div class="leftarea1">
                                    <div class="showdata">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>

                                                <td>


                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myvalcomp">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="">
                                    <div id="divrightbtn" runat="server" style="text-align: right;">
                                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false"
                                            OnClick="lnkAdd_Click" CssClass="btn largeButton blueBtn"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

                <div class="contactbottomarea finalgrid documentGrid">
                    <div class="tableblack tableminpadd">
                        <div class="table-responsive" id="PanGrid" runat="server" visible="false">
                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server"
                                OnPageIndexChanging="GridView1_PageIndexChanging" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" AutoGenerateColumns="false"
                                OnSorting="GridView1_Sorting" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowCommand="GridView1_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                        <ItemTemplate>
                                            <%#Eval("ProjectNumber")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Installed Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                            <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                            <%#Eval("ProjectAddress")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Gov. Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                        <ItemTemplate>
                                            <%#Eval("Applcation_Status")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="hypStatus" runat="server" CommandName="Status" CommandArgument='<%#Eval("ProjectID") %>'
                                                CausesValidation="false"><%#Eval("ProjectStatus")%></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                        <ItemTemplate>
                                            <%#Eval("updatedbyempname")%>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" HeaderText="Actions" HeaderStyle-CssClass="documentLabel center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank" CssClass="btn-info btn btngray"
                                                NavigateUrl='<%# "~/admin/adminfiles/project/project.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                          <%--  <img src="<%=SiteURL%>/images/icon_detail.png" />--%>
                                            <i class="fa fa-link"></i>Detail
                                            </asp:HyperLink>
                                            <asp:HyperLink ID="gvbtnDetail2" runat="server" data-toggle="tooltip" data-placement="top" Visible="false" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                          <%--  <img src="<%=SiteURL%>/images/icon_detail.png" />--%>
                                            <i class="fa fa-link"></i>Detail
                                            </asp:HyperLink>

                                            <!--DELETE Modal Templates-->
                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btngray" CausesValidation="false"
                                                CommandName="Delete" CommandArgument='<%#Eval("ProjectID")%>'>
                                            <i class="fa fa-trash"></i> Delete
                                            </asp:LinkButton>

                                            <!--END DELETE Modal Templates-->


                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </div>
    </div>
</section>




<!--Danger Modal Templates-->
<asp:Button ID="btndelete" Style="display: none;" runat="server" />
<cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
    PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
</cc1:ModalPopupExtender>
<div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message">

    <div class="modal-dialog ">
        <div class=" modal-content ">
            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
            <div class="modal-header text-center">
                <div class="modalHead">
                    <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-trash popupIcon"></i>Delete</h4>
                </div>
            </div>

            <%-- <label id="ghh" runat="server"></label> --%>
            <div class="modal-body ">
                <div class="formainline formGrid">
                    <div class="">
                        <span class="name disblock">
                            <label class="control-label">
                                Are You Sure You want to Delete This Entry?
                            </label>
                        </span>
                        <div class="">
                            <div class="form-group spicaldivin " runat="server">
                                <div class="col-sm-12">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 textRight">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandName="deleteRow" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<asp:HiddenField ID="hdndelete" runat="server" />
<!--End Danger Modal Templates-->




<asp:Button ID="Button2" Style="display: none;" runat="server" />
<cc1:ModalPopupExtender ID="ModalPopupExtenderAddressExistency" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
    OkControlID="btnOKAddress" TargetControlID="Button2">
</cc1:ModalPopupExtender>
<div id="divAddressCheck" runat="server" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div style="float: right">
                    <button id="ibtnCancel" runat="server" onclick="btnCancle_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                </div>
                <h4 class="modal-title" id="H4">Duplicate Address</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                        <tbody>
                            <tr align="center">
                                <td>
                                    <h4 class="noline" style="color: black"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                        This looks like a Duplicate Entry.</b></h4>
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="tablescrolldiv">
                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                            <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server"
                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Customers">
                                        <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground statuspopup"
    CancelControlID="ibtnCancelAddress" DropShadow="false" PopupControlID="divAddress"
    OkControlID="btnOKAddress" TargetControlID="btnNULLAddress">
</cc1:ModalPopupExtender>
<div id="divAddress" runat="server" style="display: none; z-index: 999!important;" class="modal_popup">
    <div class="modal-dialog" style="width: 350px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <div style="float: right">
                    <asp:LinkButton ID="ibtnCancelAddress" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                   Close
                    </asp:LinkButton>
                </div>
                <h4 class="modal-title" id="H2">
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    Update Address</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                        summary="">
                        <tbody>
                            <tr style="text-align: center">
                                <td>
                                    <h3 class="noline"><b>Address is not verified. Please verify the address first.</b> </h3>
                                </td>
                            </tr>
                            <tr style="text-align: center">
                                <td>
                                    <asp:Button ID="btnYesAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnYesAddress_Click"
                                        Text="Yes" CausesValidation="false" />
                                    <asp:Button ID="btnCancleAddress" runat="server" OnClick="btnCancleAddress_Click" CausesValidation="false"
                                        CssClass="btn btn-danger btn-rounded" Text="Cancle" />

                                    <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULLAddress" Style="display: none;" runat="server" />




<cc1:ModalPopupExtender ID="ModalPopupExtenderStatus" runat="server" BackgroundCssClass="modalbackground statuspopup"
    CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
    OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
</cc1:ModalPopupExtender>
<div id="divUpdateStatus" runat="server" style="display: none; z-index: 999!important;" class="modal_popup addDocumentPopup">
    <div class="modal-dialog" style="width: 500px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <div class="modalHead">
                    <h4 class="modal-title" id="myModalLabel">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        <i class="pe-7s-diskette popupIcon"></i>Update Status</h4>
                    <div>
                        <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal">Close
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="modal-body formGrid">
                <div class="panel-body">
                    <div class="formainline dropDownwid100">
                        <div class="form-group">
                            <label class="control-label">
                                Status:
                            </label>
                            <asp:DropDownList ID="ddlProjectStatusID" runat="server" Width="150px" OnSelectedIndexChanged="ddlProjectStatusID_SelectedIndexChanged"
                                AppendDataBoundItems="true" AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="status" ControlToValidate="ddlProjectStatusID" Display="Dynamic"></asp:RequiredFieldValidator>

                        </div>

                        <div class="form-group" id="trHold" runat="server" visible="false">
                            <label>
                                OnHold Reason:
                            </label>
                            <asp:DropDownList ID="ddlProjectOnHoldID" runat="server" Width="150px" AppendDataBoundItems="true"
                                aria-controls="DataTables_Table_0" class="myvalcomp">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                ControlToValidate="ddlProjectOnHoldID" Display="Dynamic" ValidationGroup="status"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group" id="trCancel" runat="server" visible="false">
                            <label>
                                Cancel Reason:
                            </label>
                            <asp:DropDownList ID="ddlProjectCancelID" runat="server" AppendDataBoundItems="true"
                                Width="150px" aria-controls="DataTables_Table_0" class="myvalcomp">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                ValidationGroup="status" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group modaldesc" id="trComment" runat="server" visible="false">
                            <label>
                                Comment:
                            </label>
                            <asp:TextBox ID="txtComment" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                        </div>

                        <div class="form-group modaldesc" id="trPreCancelReason" runat="server" visible="false">
                            <label>
                                Pre Cancel Reason
                            </label>
                            <asp:TextBox ID="txtPreCancelReason" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                        </div>



                        <div class="textRight">
                            <asp:Button ID="ibtnUpdateStatus" runat="server" Text="Update" OnClick="ibtnUpdateStatus_Onclick"
                                class="btn largeButton greenBtn savewhiteicon btnsaveicon" ValidationGroup="status" />
                            <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                                CssClass="btn" Text=" OK " />
                        </div>
                    </div>
                </div>
            </div>
        </div>
 HEAD
    </div>
</div>
<asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
<asp:HiddenField ID="hndStatusProjectID" runat="server" />
<div class="row nopadding" id="divprojecttab" runat="server" visible="false" style="margin-top: 20px;">
    <div class="col-md-12 ">
        <!-- start: ALERTS PANEL -->
        <div class="tabintab companytab">
            <div class="panel panel-default">
                <div class="panel-heading printpage" style="padding-bottom: 18px;">
                    <span style="display: inline-block; padding-top: 6px;">
                        <asp:Literal ID="ltproject" runat="server"></asp:Literal></span>

                    <div class="pull-right fontsize13">
                        <asp:ImageButton ID="btnCheckActive" runat="server" ImageUrl="~/images/btn_check_active2.png"
                            CausesValidation="false" OnClick="btnCheckActive_Click" CssClass="btnimagecheckactivity margintopminus" />
                        <asp:LinkButton ID="lnkclose" CausesValidation="false" runat="server"
                            OnClick="lnkclose_Click" CssClass="btn btn-maroon"><i class="fa fa-backward"></i>Back</asp:LinkButton>
                    </div>
                </div>
                <div class="panel-body lightgrayback">
                    <div class="row">
                        <div class="col-md-12" style="position: relative;">
                            <asp:Panel ID="Panel2" runat="server">
                                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    AutoPostBack="true">
                                    <cc1:TabPanel ID="TabDetail" runat="server" HeaderText="Detail" CssClass="printpage" TabIndex="0">
                                        <ContentTemplate>
                                            <uc1:projectdetail ID="projectdetail1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabprojectProject" runat="server" HeaderText="Project" CssClass="printpage" TabIndex="14">
                                        <ContentTemplate>
                                            <uc14:projectmaintenance ID="projectmaintenance1" runat="server" />
                                            <uc16:projectProject1 ID="projectProject1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <%--<cc1:TabPanel ID="TabMaintenance" runat="server" HeaderText="Sale" TabIndex="1" OnClientClick="setActiveTabByTitle(this);">
        <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
        <asp:HiddenField ID="hndStatusProjectID" runat="server" />
        <div class="row nopadding" id="divprojecttab" runat="server" visible="false" style="margin-top: 20px;">
            <div class="col-md-12 ">
                <!-- start: ALERTS PANEL -->
                <div class="tabintab companytab">
                    <div class="panel panel-default" style="border: 0;">
                        <div class="panel-heading printpage flexRow" style="padding-bottom: 18px;    border: 0;">
                            <span style="display: inline-block; padding-top: 6px;">
                                <asp:Literal ID="ltproject" runat="server"></asp:Literal></span>

                            <div class="flexRow">
                                <asp:ImageButton ID="btnCheckActive" runat="server" ImageUrl="~/images/btn_check_active2.png"
                                    CausesValidation="false" OnClick="btnCheckActive_Click" CssClass="btnimagecheckactivity margintopminus" style="margin-right:8px;"/>
                                <asp:LinkButton ID="lnkclose" CausesValidation="false" runat="server"
                                    OnClick="lnkclose_Click" CssClass="btn btn-info btngray"><i class="fa fa-backward"></i>Back</asp:LinkButton>
                            </div>
                        </div>
                        <div class="panel-body lightgrayback">
                            <div class="row">
                                <div class="col-md-12" style="position: relative;">
                                    <asp:Panel ID="Panel2" runat="server">
                                        <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            AutoPostBack="true">
                                            <cc1:TabPanel ID="TabDetail" runat="server" HeaderText="Detail" CssClass="printpage" TabIndex="0">
                                                <ContentTemplate>
                                                    <uc1:projectdetail ID="projectdetail1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabprojectProject" runat="server" HeaderText="Sale" CssClass="printpage" TabIndex="14">
                                                <ContentTemplate>
                                                    <uc14:projectmaintenance ID="projectmaintenance1" runat="server" />
                                                    <uc16:projectProject1 ID="projectProject1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <%--<cc1:TabPanel ID="TabMaintenance" runat="server" HeaderText="Sale" TabIndex="1" OnClientClick="setActiveTabByTitle(this);">

                                        <ContentTemplate>
                                            <uc14:projectmaintenance ID="projectmaintenance1" runat="server" />
                                            <uc3:projectsale ID="projectsale1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>--%>

                                    <%--   <cc1:TabPanel ID="TabPrice" runat="server" HeaderText="Price" TabIndex="2">
                                        <ContentTemplate>
                                            <uc2:projectprice ID="projectprice1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>--%>
                                    <cc1:TabPanel ID="TabQuote" runat="server" HeaderText="Quote" TabIndex="3">
                                        <ContentTemplate>
                                            <uc4:projectquote ID="projectquote1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>

                                    <cc1:TabPanel ID="TabPreInst" runat="server" HeaderText="Pre-Inst" TabIndex="7">
                                        <ContentTemplate>
                                            <uc5:projectpreinst ID="projectpreinst1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>

                                    <cc1:TabPanel ID="TabPostInst" runat="server" HeaderText="Post-Inst" TabIndex="8">
                                        <%--<ContentTemplate>
                                            <uc6:projectpostinst ID="projectpostinst1" runat="server" />
                                        </ContentTemplate>--%>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabElecInv" runat="server" HeaderText="Elec Inv" TabIndex="9">
                                        <ContentTemplate>
                                            <uc7:projectelecinv ID="projectelecinv1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabSTC" runat="server" HeaderText="STC" TabIndex="6">
                                        <ContentTemplate>
                                            <uc11:projectstc ID="projectstc1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabMtce" runat="server" HeaderText="Mtce" TabIndex="10">
                                        <ContentTemplate>
                                            <uc13:projectmtce ID="projectmtce1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabDocuments" runat="server" HeaderText="Documents" TabIndex="10">
                                        <ContentTemplate>
                                            <uc17:Projectdocuments ID="documents1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabVicDoc" runat="server" HeaderText="Vic Doc" TabIndex="13">
                                        <ContentTemplate>
                                            <%--<uc15:projectvicdoc ID="projectvicdoc1" runat="server" />--%>
                                        </ContentTemplate>
                                    </cc1:TabPanel>

                                    <cc1:TabPanel ID="TabFinance" runat="server" HeaderText="Finance" TabIndex="4">
                                        <ContentTemplate>
                                            <uc10:projectfinance ID="projectfinance1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabForms" runat="server" HeaderText="Forms" TabIndex="5">
                                        <ContentTemplate>
                                            <uc9:projectforms ID="projectforms1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>

                                    <%--  <cc1:TabPanel ID="TabDocs" runat="server" HeaderText="Documents" TabIndex="11">
                                        <ContentTemplate>
                                            <uc12:projectdocs ID="projectdocs1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>--%>
                                    <%--<cc1:TabPanel ID="TabRefund" runat="server" HeaderText="Refund" TabIndex="12" >
                                        <ContentTemplate>
                                            <uc8:projectrefund ID="projectrefund1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>--%>
                                </cc1:TabContainer>
                            </asp:Panel>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
    CancelControlID="ibtnCancelActive" DropShadow="false" PopupControlID="divAddActive"
    OkControlID="btnOKActive" TargetControlID="btnNULLActive">
</cc1:ModalPopupExtender>
<div id="divAddActive" runat="server">
    <div class="modal-dialog" style="width: 350px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <div style="float: right">
                    <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false"
                        runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="ibtnCancelActive_Onclick">
                   Close
                    </asp:LinkButton>
                </div>
                <h4 class="modal-title" id="H1">Project Status</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div class="formainline">
                        <div class="form-group">
                            <h3 class="martopzero">
                                <asp:Label ID="lblActive" runat="server" Visible="true"></asp:Label></h3>
                        </div>
                        <div>
                            <table class="form-group table table-bordered table-striped">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="lblSignedQuote" runat="server"></asp:Label>
                                        </b>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="lblDepositeRec" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="lblQuoteAccepted" runat="server"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="lblEleDest" runat="server"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="approveref" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblApprovalRef" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="NMINumber" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblNMINumber" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="meterupgrade" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblmeterupgrade" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="RegPlanNo" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblRegPlanNo" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="LotNum" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblLotNum" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="DocumentVerified" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblDocumentVerified" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>
                                <tr id="VicAppRefference" runat="server" visible="false">
                                    <td>
                                        <b>
                                            <asp:Label ID="lblVicAppRefference" runat="server" Visible="false"></asp:Label>
                                        </b>

                                    </td>
                                </tr>


                            </table>

                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnOKActive" Style="display: none; visible: false;" runat="server"
                                CssClass="btn" Text=" OK " />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULLActive" Style="display: none;" runat="server" />



<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');

        }
        function endrequesthandler(sender, args) {
            $('.loading-container').addClass('loading-inactive');
        }

        function pageLoadedpro() {
            $('.loading-container').removeClass('loading-inactive');
        }
    </script>
</div>
<%--  </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnAdd" />
    </Triggers>
</asp:UpdatePanel>--%>
<%--<asp:updateprogress id="updateprogress2" runat="server" displayafter="0">
        <progresstemplate>
    <div class="loading-container">
        <div class="loader"></div>
        </div>
        </progresstemplate>
    </asp:updateprogress>
    <cc1:modalpopupextender id="newmodalpopup" runat="server" targetcontrolid="updateprogress2"
        popupcontrolid="updateprogress2" backgroundcssclass="modalpopup" />--%>


