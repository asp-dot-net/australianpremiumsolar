﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class includes_role : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        BindDropDown();
        BindRole();
    }
    public void BindRole()
    {
        
       
    }
    public void BindDropDown()
    {
        ddlLocation.DataSource = ClstblEmployees.tblCompanyLocations_SelectAsc();
        ddlLocation.DataMember = "CompanyLocation";
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        ddlEmployeeStatusID.DataSource = ClstblEmployees.tblEmployeeStatus_select();
        ddlEmployeeStatusID.DataMember = "EmployeeStatus";
        ddlEmployeeStatusID.DataTextField = "EmployeeStatus";
        ddlEmployeeStatusID.DataValueField = "EmployeeStatusID";
        ddlEmployeeStatusID.DataBind();

        lstrole.DataSource = ClstblEmployees.SpRolesGetDataByAsc();
        lstrole.DataMember = "RoleName";
        lstrole.DataTextField = "RoleName";
        lstrole.DataValueField = "RoleName";
        lstrole.DataBind();

        DataTable dtTeam = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlSalesTeamID.DataSource = dtTeam;
        ddlSalesTeamID.DataValueField = "SalesTeamID";
        ddlSalesTeamID.DataTextField = "SalesTeam";
        ddlSalesTeamID.DataMember = "SalesTeam";
        ddlSalesTeamID.DataBind();

        //ddlTeam.DataSource = dtTeam;
        //ddlTeam.DataMember = "SalesTeam";
        //ddlTeam.DataTextField = "SalesTeam";
        //ddlTeam.DataValueField = "SalesTeamID";
        //ddlTeam.DataBind();

        //ddlsearchrole.DataSource = ClstblEmployees.SpRolesGetDataByAsc();
        //ddlsearchrole.DataMember = "RoleName";
        //ddlsearchrole.DataTextField = "RoleName";
        //ddlsearchrole.DataValueField = "RoleName";
        //ddlsearchrole.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string username = txtuname.Text.Trim();
        string password = txtcpassword.Text.Trim();
        string StartTime = txtStartTime.Text;
        string BreakTime = txtBreakTime.Text;
        string EndTime = txtEndTime.Text;
        string EmployeeStatusID = ddlEmployeeStatusID.SelectedValue;
        string Location = ddlLocation.SelectedValue;
        string ActiveEmp = Convert.ToString(chkActiveEmp.Checked);
        string GSTPayment = Convert.ToString(chkGSTPayment.Checked);

        
        

        foreach (ListItem itemval in lstrole.Items)
        {
            if (itemval.Selected)
            {
                Roles.AddUserToRole(username, itemval.Value);
            }
        }

       // Membership.CreateUser(username, password, EmpEmail);

        string userid = Membership.GetUser(username).ProviderUserKey.ToString();
        
    }

}