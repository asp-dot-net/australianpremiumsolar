﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Collections;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Net;
//using FormbayClientApi;

public partial class includes_controls_projectpreinst : System.Web.UI.UserControl
{
    protected static string address;
    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
        public string CustomerUserID { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string guidvalue { get; set; }
        public string code { get; set; }
    }

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("VendorJobId")]
        public int VendorJobId { get; set; }

        [JsonProperty("Arisesolarid")]
        public int Arisesolarid { get; set; }

        [JsonProperty("BasicDetails")]
        public BasicDetails BasicDetails { get; set; }

        [JsonProperty("InstallerView")]
        public InstallerView InstallerView { get; set; }

        [JsonProperty("ElectricianView")]
        public ElectricianView ElectricianView { get; set; }

        [JsonProperty("DesignerView")]
        public DesignerView DesignerView { get; set; }

        [JsonProperty("JobInstallationDetails")]
        public JobInstallationDetails JobInstallationDetails { get; set; }

        [JsonProperty("JobOwnerDetails")]
        public JobOwnerDetails JobOwnerDetails { get; set; }

        [JsonProperty("JobSystemDetails")]
        public JobSystemDetails JobSystemDetails { get; set; }

        [JsonProperty("JobSTCDetails")]
        public JobStcDetails JobStcDetails { get; set; }

        [JsonProperty("panel")]
        public Panel[] Panel { get; set; }

        [JsonProperty("inverter")]
        public Inverter[] Inverter { get; set; }

        [JsonProperty("lstJobNotes")]
        public LstJobNote[] LstJobNotes { get; set; }

        [JsonProperty("lstCustomDetails")]
        public LstCustomDetail[] LstCustomDetails { get; set; }

        public string quickformGuid { get; set; }

    }
    public partial class BasicDetails
    {
        [JsonProperty("JobType")]
        public int JobType { get; set; }
        [JsonProperty("Jobstatus")]
        public int Jobstatus { get; set; }

        [JsonProperty("formid")]
        public int formid { get; set; }

        [JsonProperty("Createddate")]
        public DateTime Createddate { get; set; }

        [JsonProperty("RefNumber")]
        public string RefNumber { get; set; }

        [JsonProperty("JobStage")]
        public int JobStage { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("strInstallationDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StrInstallationDate { get; set; }

        [JsonProperty("Priority")]
        public int Priority { get; set; }
    }

    public partial class InstallerView
    {
        [JsonProperty("Installerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class ElectricianView
    {
        [JsonProperty("Electricianid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class DesignerView
    {
        [JsonProperty("Designerid")]
        public string Installerid { get; set; }

        [JsonProperty("Customerid")]
        public int Customerid { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        [JsonProperty("IsCompleteUnit")]
        public string IsCompleteUnit { get; set; }
        [JsonProperty("IsMoreSGU")]
        public string IsMoreSGU { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class Inverter
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Series")]
        public string Series { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }
        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("noofinverter")]
        public int noofinverter { get; set; }
    }

    public partial class JobInstallationDetails
    {
        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("Streetaddress")]
        public string Streetaddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("AddressID")]
        public int AddressId { get; set; }

        [JsonProperty("PostalAddressID")]
        public int PostalAddressId { get; set; }

        [JsonProperty("Inst_UnitNo")]
        public string Inst_UnitNo { get; set; }

        [JsonProperty("Inst_UnitType")]
        public string Inst_UnitType { get; set; }

        [JsonProperty("Inst_StreetNo")]
        public string Inst_StreetNo { get; set; }

        [JsonProperty("Inst_StreetName")]
        public string Inst_StreetName { get; set; }


        [JsonProperty("Inst_StreetType")]
        public string Inst_StreetType { get; set; }

        [JsonProperty("Inst_StreetAddress")]
        public string Inst_StreetAddress { get; set; }


        [JsonProperty("Inst_Suburb")]
        public string Inst_Suburb { get; set; }

        [JsonProperty("Inst_State")]
        public string Inst_State { get; set; }

        [JsonProperty("Inst_PostCode")]
        public string Inst_PostCode { get; set; }


        [JsonProperty("PropertyType")]
        public string PropertyType { get; set; }

        [JsonProperty("SingleMultipleStory")]
        public string SingleMultipleStory { get; set; }

        [JsonProperty("Rooftype")]
        public string Rooftype { get; set; }

        [JsonProperty("RegPlanNo")]
        public string RegPlanNo { get; set; }

        [JsonProperty("LotNo")]
        public string LotNo { get; set; }

        [JsonProperty("InstallingNewPanel")]
        public string InstallingNewPanel { get; set; }

        [JsonProperty("ExistingSystem")]
        public bool ExistingSystem { get; set; }

        [JsonProperty("ExistingSystemSize")]
        public string ExistingSystemSize { get; set; }

        [JsonProperty("NoOfPanels")]
        public int NoOfPanels { get; set; }

        [JsonProperty("SystemLocation")]
        public string SystemLocation { get; set; }

        [JsonProperty("AdditionalInstallationInformation")]
        public string AdditionalInstallationInformation { get; set; }

        [JsonProperty("DistributorID")]
        public int DistributorID { get; set; }

        [JsonProperty("ElectricityProviderID")]
        public int ElectricityProviderID { get; set; }

        [JsonProperty("NMI")]
        public string NMI { get; set; }

        [JsonProperty("MeterNumber")]
        public string MeterNumber { get; set; }

        [JsonProperty("PhaseProperty")]
        public string PhaseProperty { get; set; }

    }

    public partial class JobOwnerDetails
    {
        [JsonProperty("OwnerType")]
        public string OwnerType { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }



        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }


        [JsonProperty("StreetTypeID")]
        public string StreetTypeId { get; set; }

        [JsonProperty("StreetAddress")]
        public string StreetAddress { get; set; }


        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }


        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
    }

    public partial class JobStcDetails
    {
        [JsonProperty("TypeOfConnection")]
        public string TypeOfConnection { get; set; }

        [JsonProperty("SystemMountingType")]
        public string SystemMountingType { get; set; }

        [JsonProperty("DeemingPeriod")]
        public string DeemingPeriod { get; set; }

        [JsonProperty("MultipleSGUAddress")]
        public string MultipleSguAddress { get; set; }

        [JsonProperty("Location")]
        public string Location { get; set; }

        //[JsonProperty("panel")]
        //public JobStcDetailsPanelBrand panel { get; set; }

        //[JsonProperty("inverter")]
        //public JobStcDetailsPanelBrand inverter { get; set; }


    }
    public partial class JobStcDetailsPanelBrand
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }
    }

    public partial class JobSystemDetails
    {
        [JsonProperty("SystemModel")]
        public string SystemModel { get; set; }

        [JsonProperty("SystemSize")]
        public decimal SystemSize { get; set; }

        [JsonProperty("Systemtype")]
        public int Systemtype { get; set; }

        [JsonProperty("ConnectionType")]
        public int ConnectionType { get; set; }

        [JsonProperty("MountingType")]
        public int MountingType { get; set; }

        [JsonProperty("SerialNumbers")]
        public string SerialNumbers { get; set; }

        [JsonProperty("NoOfPanel")]
        public int NoOfPanel { get; set; }

        [JsonProperty("InstallationType")]
        public string InstallationType { get; set; }

    }

    public partial class LstCustomDetail
    {
        [JsonProperty("VendorJobCustomFieldId")]
        public long VendorJobCustomFieldId { get; set; }

        [JsonProperty("FieldValue")]
        public string FieldValue { get; set; }

        [JsonProperty("FieldName")]
        public string FieldName { get; set; }
    }

    public partial class LstJobNote
    {
        [JsonProperty("VendorJobNoteId")]
        public string VendorJobNoteId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

    public partial class Panel
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }

        [JsonProperty("size")]
        public string size { get; set; }
        [JsonProperty("STC")]
        public string STC { get; set; }
    }

    public class fileUploadClass
    {
        public string fileName { get; set; }
        public string discomeMeterNo { get; set; }
        public string certifcateNo { get; set; }
        public DateTime datetime { get; set; }
    }
    #endregion
    protected DataTable rpttable;
    static int MaxAttribute = 1;
    protected string SiteURL;
    private object installbookingtracker;
    HiddenField hdnSysDetails = new HiddenField();
    HiddenField hdnQty = new HiddenField();
    HiddenField hiddStockOrderitemID = new HiddenField();
    HiddenField hdnStockCategoryId = new HiddenField();
    HiddenField hdnStockItem = new HiddenField();
    //protected void chkElecDistOK_CheckedChanged(object sender, EventArgs e)
    //{
    //    //AjaxControlToolkit.ModalPopupExtender m = this.Parent.FindControl("projectpreinst") as AjaxControlToolkit.ModalPopupExtender;    


    //    //ScriptManager.RegisterStartupScript(UpdatePanel1,
    //    //                                   this.GetType(),
    //    //                                   "MyAction",
    //    //                                   "doMyAction();",
    //    //                                   true);
    //    string ProjectID = "";

    //    if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
    //    {
    //        ProjectID = Request.QueryString["proid"];
    //    }
    //    else
    //    {
    //        ProjectID = HiddenField2.Value;
    //    }
    //    if (ProjectID != string.Empty)
    //    {
    //        SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //        if (stProject.InstallState == "VIC")
    //        {
    //            if (!string.IsNullOrEmpty(stProject.VicAppReference))
    //            {
    //                divVicAppRefRebate.Visible = false;
    //                PanAddUpdate.Visible = true;
    //                if (chkElecDistOK.Checked == true)
    //                {
    //                    //UserControlButtonClicked(sender);
    //                    divElecDistApproved.Visible = true;
    //                    if (txtElecDistApproved.Text == string.Empty)
    //                    {

    //                        //RequiredFieldValidatorEDA.Visible = true;
    //                    }
    //                    else
    //                    {
    //                       // RequiredFieldValidatorEDA.Visible = false;
    //                    }

    //                    txtInstallBookingDate.Enabled = true;
    //                    //  ImageInstallBookingDate.Enabled = true;
    //                    //rblAM1.Enabled = true;
    //                    rblAM1.Attributes.Add("onclick", "return true;");
    //                    rblAM2.Attributes.Add("onclick", "return true;");
    //                    rblPM1.Attributes.Add("onclick", "return true;");
    //                    rblPM2.Attributes.Add("onclick", "return true;");
    //                    //rblAM2.Enabled = true;
    //                    //rblPM1.Enabled = true;
    //                    //rblPM2.Enabled = true;
    //                    txtInstallDays.Enabled = true;
    //                }
    //                else
    //                {
    //                    divElecDistApproved.Visible = false;
    //                  //  RequiredFieldValidatorEDA.Visible = false;

    //                    txtInstallBookingDate.Enabled = false;
    //                    //   ImageInstallBookingDate.Enabled = false;
    //                    rblAM1.Attributes.Add("onclick", "return false;");
    //                    rblAM2.Attributes.Add("onclick", "return false;");
    //                    rblPM1.Attributes.Add("onclick", "return false;");
    //                    rblPM2.Attributes.Add("onclick", "return false;");
    //                    //rblAM2.Enabled = false;
    //                    //rblPM1.Enabled = false;
    //                    //rblPM2.Enabled = false;
    //                    txtInstallDays.Enabled = false;

    //                    txtInstallBookingDate.Text = string.Empty;
    //                    txtElecDistApproved.Text = string.Empty;
    //                    rblAM1.Checked = false;
    //                    rblAM2.Checked = false;
    //                    rblPM1.Checked = false;
    //                    rblPM2.Checked = false;
    //                }
    //                BindScript();
    //                //UserControlButtonClicked(sender);
    //            }
    //            else
    //            {
    //                divVicAppRefRebate.Visible = true;
    //                chkElecDistOK.Checked = false;
    //                // PanAddUpdate.Visible = false;
    //            }
    //        }
    //        else
    //        {
    //            divVicAppRefRebate.Visible = false;
    //            PanAddUpdate.Visible = true;
    //            if (chkElecDistOK.Checked == true)
    //            {
    //                //UserControlButtonClicked(sender);
    //                divElecDistApproved.Visible = true;
    //                if (txtElecDistApproved.Text == string.Empty)
    //                {

    //                    //RequiredFieldValidatorEDA.Visible = true;
    //                }
    //                else
    //                {
    //                    //RequiredFieldValidatorEDA.Visible = false;
    //                }

    //                txtInstallBookingDate.Enabled = true;
    //                //  ImageInstallBookingDate.Enabled = true;
    //                //rblAM1.Enabled = true;
    //                rblAM1.Attributes.Add("onclick", "return true;");
    //                rblAM2.Attributes.Add("onclick", "return true;");
    //                rblPM1.Attributes.Add("onclick", "return true;");
    //                rblPM2.Attributes.Add("onclick", "return true;");
    //                //rblAM2.Enabled = true;
    //                //rblPM1.Enabled = true;
    //                //rblPM2.Enabled = true;
    //                txtInstallDays.Enabled = true;
    //            }
    //            else
    //            {
    //                divElecDistApproved.Visible = false;
    //               // RequiredFieldValidatorEDA.Visible = false;

    //                txtInstallBookingDate.Enabled = false;
    //                //   ImageInstallBookingDate.Enabled = false;
    //                rblAM1.Attributes.Add("onclick", "return false;");
    //                rblAM2.Attributes.Add("onclick", "return false;");
    //                rblPM1.Attributes.Add("onclick", "return false;");
    //                rblPM2.Attributes.Add("onclick", "return false;");
    //                //rblAM2.Enabled = false;
    //                //rblPM1.Enabled = false;
    //                //rblPM2.Enabled = false;
    //                txtInstallDays.Enabled = false;

    //                txtInstallBookingDate.Text = string.Empty;
    //                txtElecDistApproved.Text = string.Empty;
    //                rblAM1.Checked = false;
    //                rblAM2.Checked = false;
    //                rblPM1.Checked = false;
    //                rblPM2.Checked = false;
    //            }
    //            BindScript();
    //            //UserControlButtonClicked(sender);
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        //UserControlButtonClicked(sender);
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
        //rvreqby.MinimumValue = SiteConfiguration.FromSqlDate(Convert.ToDateTime(st2.ElecLicenceExpires));
        //rvreqby.MaximumValue = SiteConfiguration.FromSqlDate(DateTime.Now.AddYears(200));
        if (!IsPostBack)
        {
            MaxAttribute = 1;
            BindAddedAttribute();
            bindrepeater();
        }
        lnlError.Text = "";

        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];

        }

        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (st.PanelBrandID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;

            }
            if (st.DiscomDate != "")
            {
                TextBox1.Text = st.DiscomDate;

            }
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                var data = ClstblDocuments.tblGetCustomerDocuments(Request.QueryString["compid"], "20");

                List<fileUploadClass> fileUploadClassList = new List<fileUploadClass>();
                fileUploadClassList = (from DataRow dr in data.Rows
                                       select new fileUploadClass()
                                       {
                                           fileName = dr["FileUpload"].ToString(),
                                           discomeMeterNo = dr["Number"].ToString(),
                                           datetime = Convert.ToDateTime(dr["UploadedDateTime"].ToString())
                                       }).OrderByDescending(x => x.datetime).ToList();

                if (fileUploadClassList.Count > 0)
                {
                    fileDiscom.Text = fileUploadClassList.OrderByDescending(x => x.datetime).Select(x => x.fileName).FirstOrDefault().ToString();
                }

                var data12 = ClstblDocuments.tblGetCustomerDocuments(Request.QueryString["compid"], "19");

                fileUploadClassList = (from DataRow dr in data12.Rows
                                       select new fileUploadClass()
                                       {
                                           fileName = dr["FileUpload"].ToString(),
                                           discomeMeterNo = dr["Number"].ToString(),
                                           datetime = Convert.ToDateTime(dr["UploadedDateTime"].ToString())
                                       }).OrderByDescending(x => x.datetime).ToList();

                if (fileUploadClassList.Count > 0)
                {
                    fileBidirectional.Text = fileUploadClassList.OrderByDescending(x => x.datetime).Select(x => x.fileName).FirstOrDefault().ToString();
                }

            }
            if (st.Bi_directionalDate != "")
            {
                TextBox2.Text = st.Bi_directionalDate;

            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                hdnQty.Value = st.NumberPanels.ToString();
                hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                hdnStockItem.Value = st.StockItemID.ToString();
                hdnStockCategoryId.Value = st.StockCategoryID.ToString();
                //st.
            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
            {
                txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                hdnQty.Value = st.NumberPanels.ToString();
                hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                hdnStockItem.Value = st.StockItemID.ToString();
                hdnStockCategoryId.Value = st.StockCategoryID.ToString();

            }
            //BindStockItem();

            if (String.IsNullOrEmpty(stProject.InstallBookingDate))
            {
                panelPickListBtn.Enabled = false;

            }
            if (!String.IsNullOrEmpty(stProject.quickformGuid))
            {
                btnquickfrom.Visible = false;
                btnquickfromUpdate.Visible = true;
            }
            else
            {
                btnquickfrom.Visible = true;
                btnquickfromUpdate.Visible = false;
            }
            //Response.Write(Request.QueryString["compid"]);
            //Response.End();
        }

    }

    //public void BindStockItem()
    //{
    //    DataTable dtStockItem = ClstblStockItems.tblStockItems_SelectAll();
    //    DropDownList ddlStockItem = new DropDownList();
    //    ddlStockItem.DataSource = dtStockItem;
    //    ddlStockItem.DataTextField = "StockItem";
    //    ddlStockItem.DataValueField = "StockItemID";
    //    ddlStockItem.DataBind();

    //}
    public void datevalidator()
    {
        //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
        //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
    }
    //public void datevalidator1()
    //{
    //    DateRange.MinimumValue = DateTime.Now.Date.AddYears(-1).ToString();
    //    DateRange.MaximumValue = DateTime.Now.Date.AddYears(200).ToString();
    //}
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            string compId = Request.QueryString["compid"];
            if (compId != null && compId != "")
            {
                //dt = ClstblProjects.tblProjects_SelectByUCustomerID(compId);
                //GridView1.DataSource = dt;
                //GridView1.DataBind();
            }
        }

    }


    public void BindProjectPreInst(string proid)
    {

        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        if (proid != string.Empty)
        {
            BindDropDown();
            //string proid = Request.QueryString["proid"];
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {


                SttblCustomers st1 = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);

                if (st1.isformbayadd != string.Empty)
                {
                    if (Convert.ToBoolean(st1.isformbayadd) == true)
                    {
                        txtInstallAddressline.Enabled = true;
                        custompreadd.Enabled = true;
                        txtInstallAddress.Enabled = false;
                        //ddlStreetCity.Enabled = false;
                        //AutoComplete_pre.Enabled = false;
                    }
                    else if (Convert.ToBoolean(st1.isformbayadd) == false)
                    {
                        txtInstallAddressline.Enabled = false;
                        custompreadd.Enabled = false;
                        txtInstallAddress.Enabled = true;
                        ddlStreetCity.Enabled = true;
                        //AutoComplete_pre.Enabled = true;
                    }
                }
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
            SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(st.ContactID);
            sttblprojectqtypanelinverter stpanelqty = ClstblProjects.tblProjects_SelectByProjectqty(st.ProjectNumber);
            sttblprojectqtypanelinverter stinverterqty = ClstblProjects.tblProjects_SelectByProjectinverterqty(st.ProjectNumber);
            txtnoofpanel.Text = stpanelqty.StockQuantitypanel;
            txtnoofinverter.Text = stinverterqty.StockQuantityinverter;
            txtdiscommeterno.Text = st.DiscomMeterno;
            txtSolarMeterNumber.Text = st.SolarMeterNumber;
            DateTime today = DateTime.Today;
            txtStreetPostCode.Text = st.StreetPostCode;
            txtsysdetails1.Text = st.NumberPanels + " X " + st.PanelBrandName + " + " + st.inverterqty + " X " + st.InverterDetailsName;
            txtStreetCity.Text = st.InstallCity;
            txtState1.Text = st.InstallState;
            txtAddress1.Text = st.street_number;
            txtAddress2.Text = st.street_name;
            try
            {
                txtinstCompDate.Text = Convert.ToDateTime(st.InstallCompleted).ToShortDateString();
            }
            catch { }
            try
            {
                ddlStreetState.SelectedValue = st.StreetState;
                if (st.StreetState != null && st.StreetState != "")
                {
                    ddlDistrict.Items.Clear();
                    ListItem lst = new ListItem();
                    lst.Value = "0";
                    lst.Text = "Select District";
                    ddlDistrict.Items.Add(lst);

                    ddlDistrict.DataSource = ClstblState.tbl_DistrictName_ByStateName(st.StreetState);
                    ddlDistrict.DataMember = "DistrictName";
                    ddlDistrict.DataTextField = "DistrictName";
                    ddlDistrict.DataValueField = "DistrictName";
                    ddlDistrict.DataBind();
                    ddlDistrict.SelectedValue = st.StreetDistrict;
                    ddltaluka.Text = st.Streettaluka;
                    ddlStreetCity.Text = st.StreetCity;
                }
                //ddlformbaystate_SelectedIndexChanged(st.StreetState, new EventArgs());
                //ddlDistrict.SelectedValue = st2.District;
                //ddlDistrict_SelectedIndexChanged(st2.District, new EventArgs());
                //ddltaluka.Text = st2.Taluka;
                //ddltaluka_SelectedIndexChanged(st2.Taluka, new EventArgs());
                //ddlStreetCity.SelectedValue = st.StreetCity;
                //txtdistributorapplicationnumber.Text = st2.ApplicationNo;
                //s txtpvcapacity.Text = st2.PVCapacity;

            }
            catch (Exception ex) { }
            //ddlStreetCity.SelectedValue = st.InstallCity;
            //ddlStreetState.SelectedValue = st.InstallState;

            //   DateRange.Type = ValidationDataType.Date;
            //Response.Write(st2.ElecLicenceExpires);
            //Response.End();
            //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
            //// DateRange.MaximumValue = DateTime.Now;
            if (!string.IsNullOrEmpty(st2.ElecLicenceExpires))//st2.ElecLicenceExpires != string.Empty)
            {
                //     Response.Write("yes");

                //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(st2.ElecLicenceExpires.ToString());
            }
            else
            {
                //     Response.Write("no");
                //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
            }
            // Response.End();

            if (!string.IsNullOrEmpty(st2.ElecLicenceExpires))
            {
                SiteConfiguration.FromSqlDate(Convert.ToDateTime(st2.ElecLicenceExpires));
            }

            if (st.QuickForm == "True")
            {

                chkquickform.Checked = true;
            }

            if ((Roles.IsUserInRole("Sales Manager")))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller"))
            {
                btnUpdateAddress.Visible = true;
            }
            else
            {
                btnUpdateAddress.Visible = false;
            }
            //if (Roles.IsUserInRole("Administrator"))
            //{
            //    // divAddUpdate.Visible = true;
            //}
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }

            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                //PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                //divAddUpdate.Visible = true;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                // PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                //if (proid != string.Empty)
                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            BindDropDown();

            if (st.ProjectStatusID != "9")
            {

                if (st.ProjectTypeID == "7")
                {
                    divActive.Visible = false;
                    PanAddUpdate.Visible = true;
                    //Response.Write(st.InstallBookingDate);
                    //Response.End();
                    if (st.InstallBookingDate != string.Empty)
                    {
                        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                        {
                            btnRemoveInst.Visible = true;
                            btnMove.Visible = true;
                            ddlMoveProject.Visible = true;
                        }
                    }
                    else
                    {
                        btnRemoveInst.Visible = false;
                        btnMove.Visible = false;
                        ddlMoveProject.Visible = false;
                    }
                    if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
                    {
                        PanAddUpdate.Visible = true;
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            PanAddUpdate.Visible = true;
                            DivDocNotVerified.Visible = false;
                        }
                        else
                        {
                            PanAddUpdate.Visible = false;
                            DivDocNotVerified.Visible = true;
                        }
                    }

                    string ProjectID = proid;// Request.QueryString["proid"];
                    if (Convert.ToInt32(st.InstallDays) > 0)
                    {
                        txtInstallDays.Text = st.InstallDays;
                    }
                    chkElecDistOK.Checked = Convert.ToBoolean(st.ElecDistOK);
                    if (st.ElecDistOK == "True")
                    {
                        divElecDistApproved.Visible = true;

                        try
                        {
                            txtElecDistApproved.Text = Convert.ToDateTime(st.ElecDistApproved).ToShortDateString();
                        }
                        catch { }

                        txtInstallBookingDate.Enabled = true;
                        //   ImageInstallBookingDate.Enabled = true;
                        rblAM1.Attributes.Add("onclick", "return true;");
                        rblAM2.Attributes.Add("onclick", "return true;");
                        rblPM1.Attributes.Add("onclick", "return true;");
                        rblPM2.Attributes.Add("onclick", "return true;");
                        //rblAM1.Enabled = true;
                        //rblAM2.Enabled = true;
                        //rblPM1.Enabled = true;
                        //rblPM2.Enabled = true;
                        txtInstallDays.Enabled = true;
                    }
                    else
                    {
                        divElecDistApproved.Visible = false;

                        txtInstallBookingDate.Enabled = true;
                        //  ImageInstallBookingDate.Enabled = false;
                        //rblAM1.Enabled = false;
                        //lblAM1.Attributes.Add("style", "pointer-events: none;");
                        rblAM1.Attributes.Add("onclick", "return false;");
                        rblAM2.Attributes.Add("onclick", "return false;");
                        rblPM1.Attributes.Add("onclick", "return false;");
                        rblPM2.Attributes.Add("onclick", "return false;");
                        //rblAM2.Enabled = false;
                        //rblPM1.Enabled = false;
                        //rblPM2.Enabled = false;
                        txtInstallDays.Enabled = false;
                    }
                    txtInstallerNotes.Text = st.InstallerNotes;
                    try
                    {
                        txtInstallAddressline.Text = st.InstallAddress + ", " + st.InstallCity + " " + st.InstallState + " " + st.InstallPostCode;
                    }
                    catch { }
                    txtInstallAddress.Text = st.InstallAddress;

                    //txtformbayUnitNo.Text = st.unit_number;
                    //try
                    //{
                    //    ddlformbayunittype.SelectedValue = st.unit_type;
                    //}
                    //catch { }
                    txtformbayStreetNo.Text = st.street_number;
                    txtformbaystreetname.Text = st.street_name;
                    //ddlformbaystreettype.SelectedValue = st.street_type;

                    //ddlStreetCity.SelectedValue = st.InstallCity;
                    //ddlStreetState.SelectedValue = st.InstallState;
                    txtStreetPostCode.Text = st.InstallPostCode;

                    hndstreetname.Value = st.street_name;
                    hndstreetno.Value = st.street_number;
                    hndstreetsuffix.Value = st.street_suffix;
                    hndstreettype.Value = st.street_type;
                    hndunitno.Value = st.unit_number;
                    hndunittype.Value = st.unit_type;
                    hndaddress.Value = st.InstallAddress;

                    if (st.IsFormBay == "2")
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return false;");
                        //cbkIsFormBay.Enabled = false;
                    }
                    else
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return true;");
                        //cbkIsFormBay.Enabled = true;
                        if (st.IsFormBay == "1")
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return true;");
                            //cbkIsFormBay.Checked = true;
                        }
                        else
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return false;");
                            //cbkIsFormBay.Checked = false;
                        }
                    }

                    if (st.PanelBrandID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                        hbdSystemDetails.Value = txtSystemDetails.Text.ToString();
                    }

                    try
                    {
                        ddlInstaller.SelectedValue = st.Installer;
                    }
                    catch { }
                    try
                    {
                        ddlDesigner.SelectedValue = st.Designer;
                    }
                    catch { }
                    try
                    {
                        ddlElectrician.SelectedValue = st.Electrician;
                    }
                    catch { }

                    if (st.StockAllocationStore != string.Empty)
                    {
                        try
                        {
                            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        }
                        catch { }
                    }
                    else
                    {
                        if (st.InstallState != string.Empty)
                        {
                            DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(st.InstallState);
                            if (dtState.Rows.Count > 0)
                            {
                                ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                            }
                        }
                    }
                    rblAM1.Checked = Convert.ToBoolean(st.InstallAM1);
                    rblAM2.Checked = Convert.ToBoolean(st.InstallAM2);
                    rblPM1.Checked = Convert.ToBoolean(st.InstallPM1);
                    rblPM2.Checked = Convert.ToBoolean(st.InstallPM2);

                    try
                    {
                        txtInstallBookingDate.Text = Convert.ToDateTime(st.InstallBookingDate).ToShortDateString();
                    }
                    catch { }
                }
                else if (st.ProjectStatusID == "2" || st.ProjectStatusID == "8")
                {
                    divActive.Visible = true;
                    PanAddUpdate.Visible = false;
                }
                else
                {
                    divActive.Visible = false;
                    PanAddUpdate.Visible = true;

                    if (st.InstallBookingDate != string.Empty)
                    {
                        if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Installation Manager")))
                        {
                            btnRemoveInst.Visible = true;
                            btnMove.Visible = true;
                            ddlMoveProject.Visible = true;
                        }
                    }
                    else
                    {
                        btnRemoveInst.Visible = false;
                        btnMove.Visible = false;
                        ddlMoveProject.Visible = false;
                    }
                    if (st.FinanceWithID == string.Empty || st.FinanceWithID == "1")
                    {
                        PanAddUpdate.Visible = true;
                        DivDocNotVerified.Visible = false;
                    }
                    else
                    {
                        if (st.DocumentVerified == "True")
                        {
                            PanAddUpdate.Visible = true;
                            DivDocNotVerified.Visible = false;
                        }
                        else
                        {
                            PanAddUpdate.Visible = false;
                            DivDocNotVerified.Visible = true;
                        }
                    }

                    string ProjectID = proid;// Request.QueryString["proid"];
                    if (Convert.ToInt32(st.InstallDays) > 0)
                    {
                        txtInstallDays.Text = st.InstallDays;
                    }
                    chkElecDistOK.Checked = Convert.ToBoolean(st.ElecDistOK);
                    if (st.ElecDistOK == "True")
                    {
                        divElecDistApproved.Visible = true;

                        try
                        {
                            txtElecDistApproved.Text = Convert.ToDateTime(st.ElecDistApproved).ToShortDateString();
                        }
                        catch { }

                        txtInstallBookingDate.Enabled = true;
                        //   ImageInstallBookingDate.Enabled = true;
                        rblAM1.Attributes.Add("onclick", "return true;");
                        rblAM2.Attributes.Add("onclick", "return true;");
                        rblPM1.Attributes.Add("onclick", "return true;");
                        rblPM2.Attributes.Add("onclick", "return true;");
                        //rblAM1.Enabled = true;
                        //rblAM2.Enabled = true;
                        //rblPM1.Enabled = true;
                        //rblPM2.Enabled = true;
                        txtInstallDays.Enabled = true;
                    }
                    else
                    {
                        divElecDistApproved.Visible = false;

                        txtInstallBookingDate.Enabled = true;
                        //      ImageInstallBookingDate.Enabled = false;
                        //rblAM1.Enabled = false;
                        //lblAM1.Attributes.Add("style", "pointer-events: none;");
                        rblAM1.Attributes.Add("onclick", "return false;");
                        rblAM2.Attributes.Add("onclick", "return false;");
                        rblPM1.Attributes.Add("onclick", "return false;");
                        rblPM2.Attributes.Add("onclick", "return false;");
                        //rblAM2.Enabled = false;
                        //rblPM1.Enabled = false;
                        //rblPM2.Enabled = false;
                        txtInstallDays.Enabled = false;
                    }
                    txtInstallerNotes.Text = st.InstallerNotes;
                    try
                    {
                        txtInstallAddressline.Text = st.InstallAddress + ", " + st.InstallCity + " " + st.InstallState + " " + st.InstallPostCode;
                    }
                    catch { }
                    txtInstallAddress.Text = st.InstallAddress;
                    // txtformbayUnitNo.Text = st.unit_number;
                    //try
                    //{
                    //    ddlformbayunittype.SelectedValue = st.unit_type;
                    //}
                    //catch { }
                    txtformbayStreetNo.Text = st.street_number;
                    txtformbaystreetname.Text = st.street_name;
                    // ddlformbaystreettype.SelectedValue = st.street_type;
                    //if(st.InstallCity ! =null)
                    //     {

                    // }
                    // ddlStreetCity.SelectedValue = st.InstallCity;
                    //ddlStreetState.SelectedValue = st.InstallState;
                    //txtStreetPostCode.Text = st.InstallPostCode;

                    hndstreetname.Value = st.street_name;
                    hndstreetno.Value = st.street_number;
                    hndstreetsuffix.Value = st.street_suffix;
                    hndstreettype.Value = st.street_type;
                    hndunitno.Value = st.unit_number;
                    hndunittype.Value = st.unit_type;
                    hndaddress.Value = st.InstallAddress;

                    if (st.IsFormBay == "2")
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return false;");
                        //cbkIsFormBay.Enabled = false;
                    }
                    else
                    {
                        cbkIsFormBay.Attributes.Add("onclick", "return true;");
                        //cbkIsFormBay.Enabled = true;
                        if (st.IsFormBay == "1")
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return true;");
                            //cbkIsFormBay.Checked = true;
                        }
                        else
                        {
                            cbkIsFormBay.Attributes.Add("onclick", "return false;");
                            //cbkIsFormBay.Checked = false;
                        }
                    }

                    if (st.PanelBrandID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName;
                        // hdnStockItem.Value = st.NumberPanels.ToString();
                        //hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter.";
                        //  hdnStockItem.Value = st.NumberPanels.ToString();
                        //hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }
                    if (st.PanelBrandID != "" && st.InverterDetailsID != "" && st.SecondInverterDetailsID != "")
                    {
                        txtSystemDetails.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.InverterDetailsName + " Inverter. Plus Second Inverter" + st.SecondInverterDetails;
                        //  hdnStockItem.Value = st.NumberPanels.ToString();
                        // hdnSysDetails.Value = txtSystemDetails.Text.ToString();
                    }

                    try
                    {
                        ddlInstaller.SelectedValue = st.Installer;
                    }
                    catch { }
                    try
                    {
                        ddlDesigner.SelectedValue = st.Designer;
                    }
                    catch { }
                    try
                    {
                        ddlElectrician.SelectedValue = st.Electrician;
                    }
                    catch { }

                    if (st.StockAllocationStore != string.Empty)
                    {
                        try
                        {
                            ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                        }
                        catch { }
                    }
                    else
                    {
                        if (st.InstallState != string.Empty)
                        {
                            DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(st.InstallState);
                            if (dtState.Rows.Count > 0)
                            {
                                ddlStockAllocationStore.SelectedValue = dtState.Rows[0]["CompanyLocationID"].ToString();
                            }
                        }
                    }
                    rblAM1.Checked = Convert.ToBoolean(st.InstallAM1);
                    rblAM2.Checked = Convert.ToBoolean(st.InstallAM2);
                    rblPM1.Checked = Convert.ToBoolean(st.InstallPM1);
                    rblPM2.Checked = Convert.ToBoolean(st.InstallPM2);

                    try
                    {
                        txtInstallBookingDate.Text = Convert.ToDateTime(st.InstallBookingDate).ToShortDateString();
                    }
                    catch { }
                }
            }
            if (st.Installer != string.Empty && st.InstallBookingDate != string.Empty)
            {
                if (st.FormbayId != string.Empty)
                {
                    btnformbay1.Enabled = false;
                }
                else
                {
                    btnformbay1.Enabled = true;
                }
            }
            else
            {
                btnformbay1.Enabled = true;
            }
            DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(proid);
            if (dtPickList.Rows.Count > 0)
            {
                divPickList.Visible = true;
                rptPickList.DataSource = dtPickList;
                rptPickList.DataBind();
            }
            else
            {
                divPickList.Visible = false;
            }
        }
    }
    public void BindDropDown()
    {
        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlStockAllocationStore.Items.Clear();
        ddlStockAllocationStore.Items.Add(item);

        ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlStockAllocationStore.DataValueField = "CompanyLocationID";
        ddlStockAllocationStore.DataTextField = "CompanyLocation";
        ddlStockAllocationStore.DataMember = "CompanyLocation";
        ddlStockAllocationStore.DataBind();



        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlElectrician.Items.Clear();
        ddlElectrician.Items.Add(item3);

        ddlElectrician.DataSource = ClstblContacts.tblContacts_SelectElectrician();
        ddlElectrician.DataValueField = "ContactID";
        ddlElectrician.DataTextField = "Contact";
        ddlElectrician.DataMember = "Contact";
        ddlElectrician.DataBind();

        ListItem items = new ListItem();
        items.Text = "Select State";
        items.Value = "";
        ddlStreetState.Items.Clear();
        ddlStreetState.Items.Add(items);
        DataTable dt = ClstblState.tblStates_Select();
        ddlStreetState.DataSource = dt;
        ddlStreetState.DataMember = "FullName";
        ddlStreetState.DataTextField = "FullName";
        ddlStreetState.DataValueField = "FullName";
        ddlStreetState.DataBind();


        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlDesigner.Items.Clear();
        ddlDesigner.Items.Add(item4);
        ddlDesigner.DataSource = ClstblContacts.tblContacts_SelectDesigner();
        ddlDesigner.DataValueField = "ContactID";
        ddlDesigner.DataTextField = "Contact";
        ddlDesigner.DataMember = "Contact";
        ddlDesigner.DataBind();

        //ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        //ddlformbaystreettype.DataMember = "StreetType";
        //ddlformbaystreettype.DataTextField = "StreetType";
        //ddlformbaystreettype.DataValueField = "StreetCode";
        //ddlformbaystreettype.DataBind();
        //ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        //ddlformbayunittype.DataMember = "UnitType";
        //ddlformbayunittype.DataTextField = "UnitType";
        //ddlformbayunittype.DataValueField = "UnitType";
        //ddlformbayunittype.DataBind();

        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            string date1 = txtInstallBookingDate.Text.Trim();
            string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);
            //   Response.Write(date1+"="+ date2);
            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
        }
        else
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
        }

        ListItem itemM = new ListItem();
        itemM.Text = "Project";
        itemM.Value = "";
        ddlMoveProject.Items.Clear();
        ddlMoveProject.Items.Add(itemM);

        //string proid = HiddenField2.Value;
        string proid = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(proid);
        if (st.PanelBrandID != string.Empty && st.InverterDetailsID != string.Empty)
        {
            ddlMoveProject.DataSource = ClstblProjects.tblProjects_SelectMove(st.PanelBrandID, st.InverterDetailsID);
            ddlMoveProject.DataValueField = "ProjectID";
            ddlMoveProject.DataTextField = "Project";
            ddlMoveProject.DataMember = "Project";
            ddlMoveProject.DataBind();
        }
    }

    protected void btnUpdatePreInst_Click(object sender, EventArgs e)
    {
        //BindGrid(0);
        BindScript();
        string ProjectID = "";
        // installbookingtracker.openModal = "";
        string DiscomDate = TextBox1.Text;
        string Bi_directionalDate = TextBox2.Text;

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            //string ProjectID = Request.QueryString["proid"];
            string InstallBookingDate = txtInstallBookingDate.Text.Trim();
            string InstallDays = txtInstallDays.Text;
            string ElecDistOK = Convert.ToString(chkElecDistOK.Checked);
            string InstallerNotes = txtInstallerNotes.Text;
            string ElecDistApproved = txtElecDistApproved.Text.Trim();

            // string formbayUnitNo = txtformbayUnitNo.Text;
            //string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            //string formbaystreettype = ddlformbaystreettype.SelectedValue;

            //txtInstallAddress.Text = formbayUnitNo + " " + formbayunittype + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
            txtInstallAddress.Text = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = ddlStreetCity.Text;
            string InstallState = ddlStreetState.SelectedValue;
            string InstallPostCode = txtStreetPostCode.Text;
            string Installer = ddlInstaller.SelectedValue;
            string Designer = ddlDesigner.SelectedValue;
            string Electrician = ddlElectrician.SelectedValue;
            string StockAllocationStore = ddlStockAllocationStore.SelectedValue;
            string InstallAM1 = Convert.ToString(rblAM1.Checked);
            string InstallAM2 = Convert.ToString(rblAM2.Checked);
            string InstallPM1 = Convert.ToString(rblPM1.Checked);
            string InstallPM2 = Convert.ToString(rblPM2.Checked);
            string IsFormBay = "";
            string project = InstallCity + "-" + InstallAddress;
            string CustomerID = Request.QueryString["compid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (txtdiscommeterno.Text != null && txtdiscommeterno.Text != "")
            {
                bool s1 = ClstblProjects.tblprojects_update_DiscomMeterno(txtdiscommeterno.Text, ProjectID);
            }
            if (txtSolarMeterNumber.Text != null && txtSolarMeterNumber.Text != "")
            {
                bool scno = ClstblProjects.tblprojects_update_DiscomCerificateMeterno(txtSolarMeterNumber.Text, ProjectID);

            }
            if (FileUpload2.HasFiles)
            {

                string PDFFilename = string.Empty;
                string DocumentId = "20";
                //string ID = hdnItemID.Value;
                //SttblCustomers stemp = ClstblC1ustomers.tblCustomer_SelectByID(userid);
                //var filepath = ProjectID + "_" + Filename;

                FileUpload2.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                //  bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");
                int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, txtdiscommeterno.Text, PDFFilename, UpdatedBy, DateTime.Now);
            }
            else
            {
                string PDFFilename = hdnFileName.Value.ToString();
                if (!string.IsNullOrEmpty(PDFFilename))
                {
                    Session["fileName"] = PDFFilename;
                    string DocumentId = "20";
                    //string ID = hdnItemID.Value;
                    //SttblCustomers stemp = ClstblC1ustomers.tblCustomer_SelectByID(userid);
                    //var filepath = ProjectID + "_" + Filename;

                    FileUpload2.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                    //  bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);

                    if (File.Exists(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename))
                    {
                        SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                        SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");
                        int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, txtdiscommeterno.Text, PDFFilename, UpdatedBy, DateTime.Now);
                    }
                }
            }
            if (fpBidirectionalMeter.HasFiles)
            {
                // string CustomerID = Request.QueryString["compid"];
                string PDFFilename = string.Empty;
                //  string Filename = fpBidirectionalMeter.FileName;
                string DocumentId = "21";
                //string ID = hdnItemID.Value;
                //SttblCustomers stemp = ClstblC1ustomers.tblCustomer_SelectByID(userid);
                PDFFilename = ProjectID + "_" + DocumentId + "_" + fpBidirectionalMeter.FileName;
                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                fpBidirectionalMeter.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                //var filepath = ProjectID + "_" + Filename;
                int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, "", PDFFilename, UpdatedBy, DateTime.Now);

            }


            else
            {
                string PDFFilename = hdnFile2.Value.ToString();
                if (!string.IsNullOrEmpty(PDFFilename))
                {
                    string DocumentId = "20";
                    //string ID = hdnItemID.Value;
                    //SttblCustomers stemp = ClstblC1ustomers.tblCustomer_SelectByID(userid);
                    //var filepath = ProjectID + "_" + Filename;

                    fpBidirectionalMeter.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                    //  bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
                    if (File.Exists(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename))
                    {
                        SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                        SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");
                        int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, "", PDFFilename, UpdatedBy, DateTime.Now);
                    }
                }
            }


            if (cbkIsFormBay.Checked == true)
            {
                IsFormBay = "1";
            }
            else
            {
                IsFormBay = "2";
            }
            string CustId = "";
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                CustId = Request.QueryString["compid"];
            }
            else
            {
                HiddenField hdn_custpopup = this.Parent.FindControl("hdn_custpopup") as HiddenField;
                if (hdn_custpopup.Value != string.Empty)
                {
                    CustId = hdn_custpopup.Value;
                }
            }

            int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustId, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, InstallCity, InstallState, InstallPostCode);

            // ClstblCustomers.tblCustomers_ExitsByID_Address(CustId, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
            if (existcustomer > 0)
            {
                PanAddUpdate.Visible = true;
                ModalPopupExtenderAddress.Show();
                //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustId, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
                //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                // rptaddress.DataSource = dt;
                rptaddress.DataBind();
            }
            else
            {
                string CustID = "";
                if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
                {
                    CustID = Request.QueryString["compid"].ToString();
                }
                else
                {
                    HiddenField hdn_custpopup = this.Parent.FindControl("hdn_custpopup") as HiddenField;
                    if (hdn_custpopup.Value != string.Empty)
                    {
                        CustID = hdn_custpopup.Value;
                    }
                }
                //SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                //bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
                bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);
                //bool succ = ClstblCustomers.tblCustomer_Update_Address(CustID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                bool succ = ClstblCustomers.tblCustomer_Update_Address(CustID, txtformbayStreetNo.Text, txtformbaystreetname.Text, "", hndstreetsuffix.Value);

                ClsProjectSale.tblInstallBookings_Delete(ProjectID);
                int s = ClsProjectSale.tblInstallBookings_Insert(stPro.ProjectNumber, Convert.ToString(txtInstallBookingDate.Text), ProjectID, "AM1", "");

                if (txtElecDistApproved.Text == string.Empty)
                {
                    //RequiredFieldValidatorEDA.Visible = true;
                }
                else
                {
                    //  RequiredFieldValidatorEDA.Visible = false;
                }

                bool sucPreInst = false;


                /* =========================== Mail =========================== */

                StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                string from = st.from;
                SiteURL = st.siteurl;

                if (ddlSendMail.SelectedValue == "3") // Booking Cancel Mail
                {
                    if (txtInstallBookingDate.Text.Trim() == string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(ddlInstaller.SelectedValue);

                        TextWriter txtWriter = new StringWriter() as TextWriter;
                        string mailto = stCont.ContEmail;
                        string subject = "Installation Cancel Updates from Australian Premium Solar";
                        string Address = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode;
                        string MobileNo = "";
                        string EmailAddress = "";

                        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(stPro.CustomerID);
                        if (dtCont.Rows.Count > 0)
                        {
                            MobileNo = dtCont.Rows[0]["ContMobile"].ToString();
                            EmailAddress = dtCont.Rows[0]["ContEmail"].ToString();
                        }
                        else
                        {
                            MobileNo = "-";
                            EmailAddress = "-";
                        }
                        Server.Execute("~/mailtemplate/instcancel.aspx?InstallerName=" + stPro.InstallerName + "&InstallBookingDate=" + stPro.InstallBookingDate + "&ProjectNumber=" + stPro.ProjectNumber + "&Customer=" + stPro.Customer + "&Address=" + Address + "&MobileNo=" + MobileNo + "&EmailAddress=" + EmailAddress + "&SystemDetails=" + stPro.SystemDetails + "&InstallationNote=" + stPro.InstallerNotes, txtWriter);
                        try
                        {
                            Utilities.SendMail(from, mailto, subject, txtWriter.ToString(), "cc:warehouse");
                        }
                        catch
                        {
                        }
                    }
                }
                /* ============================================================ */

                /* =========================== Project Move Mail =========================== */

                if (ddlMoveProject.SelectedValue != string.Empty)
                {
                    SttblProjects stPM = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

                    TextWriter txtWriter = new StringWriter() as TextWriter;
                    string mailto = "";
                    string subject = "Project Move Updates from Australian Premium Solar";

                    string Project = stPro.ProjectNumber + " : " + stPro.Project;
                    string ProjectMove = stPM.ProjectNumber + " : " + stPM.Project;

                    Server.Execute("~/mailtemplate/warehouse.aspx?Project=" + Project + "&ProjectMove=" + ProjectMove, txtWriter);
                    try
                    {
                        Utilities.SendMail(from, mailto, subject, txtWriter.ToString());
                    }
                    catch
                    {
                    }
                }
                /* ========================================================================= */

                if (chkElecDistOK.Checked == true && txtElecDistApproved.Text != "")
                {
                    if ((rblAM1.Checked != false || rblAM2.Checked != false || rblPM1.Checked != false || rblPM2.Checked != false) && txtInstallDays.Text != "")
                    {
                        if (UpdatedBy == null)
                        {
                            UpdatedBy = "";
                        }
                        sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, InstallBookingDate, InstallDays, ElecDistOK, InstallerNotes, ElecDistApproved, InstallAddress, InstallCity, InstallState, InstallPostCode, Installer, Designer, Electrician, "", "False", StockAllocationStore, InstallAM1, InstallAM2, InstallPM1, InstallPM2, UpdatedBy, IsFormBay, DiscomDate, Bi_directionalDate);

                        //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project,"","","");
                        bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, "", "", txtformbayStreetNo.Text, txtformbaystreetname.Text, "", hndstreetsuffix.Value, project);
                        ClsProjectSale.tblProjects_Update_others(ProjectID, txtformbayNearby.Text, ddlStreetCity.Text, ddlStreetState.SelectedValue, txtStreetPostCode.Text, project);
                        //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);

                        bool succ_street1 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);
                        //bool succ_street1 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);

                        string formbayinstalldate = "";
                        if (InstallBookingDate != string.Empty)
                        {
                            formbayinstalldate = InstallBookingDate;
                        }
                        else //if null than get next sunday date
                        {
                            if (chkElecDistOK.Checked == true)
                            {
                                DateTime InstallDate = DateTime.Now;
                                DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                                int start = (int)InstallDate.DayOfWeek;
                                int target = (int)dayOfWeek;
                                if (target <= start)
                                    target += 7;
                                formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                            }
                        }
                        ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);
                    }
                    //else
                    //{
                    //    DivInstalldetails.Visible = true;
                    //}
                }
                else
                {
                    sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, InstallBookingDate, InstallDays, ElecDistOK, InstallerNotes, ElecDistApproved, InstallAddress, InstallCity, InstallState, InstallPostCode, Installer, Designer, Electrician, "", "False", StockAllocationStore, InstallAM1, InstallAM2, InstallPM1, InstallPM2, UpdatedBy, IsFormBay, DiscomDate, Bi_directionalDate);

                    //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project,"","","");
                    bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, "", "", txtformbayStreetNo.Text, txtformbaystreetname.Text, "", hndstreetsuffix.Value, project);
                    ClsProjectSale.tblProjects_Update_others(ProjectID, txtformbayNearby.Text, ddlStreetCity.Text, ddlStreetState.SelectedValue, txtStreetPostCode.Text, project);
                    //bool succ_street2 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
                    bool succ_street2 = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);

                    string formbayinstalldate = "";
                    if (InstallBookingDate != string.Empty)
                    {
                        formbayinstalldate = InstallBookingDate;
                    }
                    else //if null than get next sunday date
                    {
                        if (chkElecDistOK.Checked == true)
                        {
                            DateTime InstallDate = DateTime.Now;
                            DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                            int start = (int)InstallDate.DayOfWeek;
                            int target = (int)dayOfWeek;
                            if (target <= start)
                                target += 7;
                            formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                        }
                    }
                    ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);
                }

                /* -------------------- Job Booked -------------------- */
                if (txtInstallBookingDate.Text.Trim() != string.Empty)
                {
                    if (stPro.ProjectStatusID == "3")
                    {
                        DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "11");
                        if (dtStatus.Rows.Count > 0)
                        {
                        }
                        else
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, UpdatedBy, stPro.NumberPanels);
                        }
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                    }
                }
                else
                {
                    if (Convert.ToInt32(stPro.ProjectStatusID) > 11 || stPro.ProjectStatusID == "10" || stPro.ProjectStatusID == "5")
                    {
                        //RequiredFieldValidatorIBDate.Visible = true;
                    }
                    else
                    {
                        DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "3");
                        if (dtStatus.Rows.Count > 0)
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, stPro.NumberPanels);
                        }
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                /* ---------------------------------------------------- */

                /* =========================== Mail =========================== */
                if (ddlSendMail.SelectedValue == "1")  // Customer Mail
                {
                    if (txtInstallBookingDate.Text.Trim() != string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        //string CustomerID = stPro.CustomerID;
                        string Customer = stPro.Customer;

                        if (CustomerID != string.Empty)
                        {
                            SttblContacts stContC = ClstblContacts.tblContacts_SelectByContactID(Installer);
                            string InstallerName = stContC.ContFirst + ' ' + stContC.ContLast;

                            TextWriter txtWriter = new StringWriter() as TextWriter;
                            string mailtoC = stContC.ContEmail;
                            string subjectC = "Installation Updates from Australian Premium Solar";

                            Server.Execute("~/mailtemplate/custbookmail.aspx?Customer=" + Customer + "&InstallBookingDate=" + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(InstallBookingDate)), txtWriter);
                            try
                            {
                                Utilities.SendMail(from, mailtoC, subjectC, txtWriter.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                if (ddlSendMail.SelectedValue == "2") // Installer Mail
                {
                    if (txtInstallBookingDate.Text.Trim() != string.Empty && ddlInstaller.SelectedValue != string.Empty)
                    {
                        //string CustomerID = stPro.CustomerID;
                        string Customer = stPro.Customer;

                        if (Installer != string.Empty)
                        {
                            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
                            SttblContacts stContI = ClstblContacts.tblContacts_SelectByContactID(Installer);

                            TextWriter txtWriterI = new StringWriter() as TextWriter;
                            string mailtoI = stContI.ContEmail;
                            string subjectI = "Installation Updates from Australian Premium Solar";

                            if (rblAM1.Checked == true)
                            {
                                InstallAM1 = ", AM1";
                            }
                            if (rblPM1.Checked == true)
                            {
                                InstallPM1 = ", PM1";
                            }
                            if (rblAM2.Checked == true)
                            {
                                InstallAM2 = ", AM2";
                            }
                            if (rblPM2.Checked == true)
                            {
                                InstallPM2 = ", PM2";
                            }
                            InstallDays = ", Install Days: " + txtInstallDays.Text;

                            string InstallationDate = InstallBookingDate + InstallAM1 + InstallPM1 + InstallAM2 + InstallPM2 + InstallDays;
                            string Address = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + " - " + stPro.InstallPostCode;

                            string MobileNo = "";
                            string EmailAddress = "";

                            DataTable dtCont = ClstblContacts.tblContacts_SelectTop(CustomerID);
                            if (dtCont.Rows.Count > 0)
                            {
                                MobileNo = dtCont.Rows[0]["ContMobile"].ToString();
                                EmailAddress = dtCont.Rows[0]["ContEmail"].ToString();
                            }
                            else
                            {
                                MobileNo = "-";
                                EmailAddress = "-";
                            }

                            Server.Execute("~/mailtemplate/instbookmail.aspx?InstallerName=" + stPro.InstallerName + "&InstallationDate=" + InstallationDate + "&ProjectNumber=" + stPro.ProjectNumber + "&Customer=" + Customer + "&Address=" + Address + "&MobileNo=" + MobileNo + "&EmailAddress=" + EmailAddress + "&SystemDetails=" + stPro.SystemDetails + "&InstallationNote=" + stPro.InstallerNotes, txtWriterI);
                            try
                            {
                                Utilities.SendMail(from, mailtoI, subjectI, txtWriterI.ToString());
                            }
                            catch
                            {
                            }
                        }
                    }
                }

                //if (stPro.ProjectTypeID == "8")
                //{
                //    if (ddlInstaller.SelectedValue != string.Empty)
                //    {
                //        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "16");//Technician Assign
                //    }
                //    if (txtInstallBookingDate.Text != string.Empty)
                //    {
                //        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "17");//Schedule for MTCE
                //    }
                //}

                /* ============================================================ */
                //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);


                //string formbayinstalldate = "";
                //if (InstallBookingDate != string.Empty)
                //{
                //    formbayinstalldate = InstallBookingDate;
                //}
                //else //if null than get next sunday date
                //{
                //    DateTime InstallDate = Convert.ToDateTime(InstallBookingDate);
                //    DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                //    int start = (int)InstallDate.DayOfWeek;
                //    int target = (int)dayOfWeek;
                //    if (target <= start)
                //        target += 7;
                //    formbayinstalldate = (InstallDate.AddDays(target - start)).ToString();
                //}
                //ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, formbayinstalldate);

                bool s1 = ClstblProjects.tblprojects_updateInstallCompDate(txtinstCompDate.Text, ProjectID);
                BindProjects();
                if (sucPreInst)
                {


                    if (chkquickform.Checked == true)
                    {
                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("True", Request.QueryString["proid"]);

                    }
                    else
                    {
                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("False", Request.QueryString["proid"]);
                        bool ISExport = ClstblProjects.tbl_projects_update_IsExport_zeroByProjectID(Request.QueryString["proid"]);
                    }
                    SetAdd1();
                    //PanSuccess.Visible = true;
                    //DivInstalldetails.Visible = false;
                    try
                    {

                    }
                    catch { }
                    AjaxControlToolkit.ModalPopupExtender m = this.Parent.FindControl("ModalPopupExtender1") as AjaxControlToolkit.ModalPopupExtender;
                    // AjaxControlToolkit hdnclosepopup = this.Page.Parent.FindControl("hdnclosepopup") as AjaxControlToolkit;
                    HiddenField hdnclosepopup = this.Parent.FindControl("hdnclosepopup") as HiddenField;

                    if (hdnclosepopup != null)
                    {
                        hdnclosepopup.Value = "2";
                        m.Hide();
                        //GridView GridView1 = (GridView)this.Parent.FindControl("GridView1");
                        //GridView1.DataBind();
                        //Page.ClientScript.(this.GetType(), "close", "<script language=javascript>window.opener.location.reload(true);self.close();</script>");
                    }
                }
                else
                {
                    PanSuccess.Visible = false;
                    //DivInstalldetails.Visible = true;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                {
                    BindProjectPreInst(Request.QueryString["proid"]);
                    panelPickListBtn.Enabled = true;
                }
                else
                {
                    BindProjectPreInst(HiddenField2.Value);
                }


                if (!string.IsNullOrEmpty(txtInstallBookingDate.Text))
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectApplicationID(ProjectID, "Feasibility Report");
                }
                if (!string.IsNullOrEmpty(txtinstCompDate.Text))
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectApplicationID(ProjectID, "Inspection self certification");
                    bool suc1 = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "28");
                }
                if (!string.IsNullOrEmpty(hdnFile2.Value.ToString()))
                {
                    string fileName = Session["fileName"].ToString();
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectApplicationID(ProjectID, "Meter Installed");
                    }
                }
                BindProjectPreInst(Request.QueryString["proid"]);
                SttblProjects st12 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "<script type='text/javascript'>callSomething('" + st12.ApplicaionStatus + "');</script>", false);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp1", "<script type='text/javascript'>callProjectSTatus('" + st12.ProjectSTatus + "');</script>", false);

            }
        }
    }

    private void BindGrid(int v)
    {
        //throw new NotImplementedException();
    }

    protected void btnCustAddress_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
            txtInstallAddress.Text = st.StreetAddress;
            //txtformbayUnitNo.Text = st.unit_number;
            //ddlformbayunittype.SelectedValue = st.unit_type;
            txtformbayStreetNo.Text = st.street_number;
            txtformbaystreetname.Text = st.street_name;
            // ddlformbaystreettype.SelectedValue = st.street_type;
            ddlStreetCity.Text = st.StreetCity;
            ddlStreetState.SelectedValue = st.StreetState;
            txtStreetPostCode.Text = st.StreetPostCode;
            txtInstallAddressline.Text = st.street_address;
        }
    }
    protected void ddlInstaller_SelectedIndexChanged(object sender, EventArgs e)
    {
        //UserControlButtonClicked(sender);
        BindScript();
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        PanAddUpdate.Visible = true;
        if (ddlInstaller.SelectedValue != string.Empty)
        {
            // Response.Write(ddlInstaller.SelectedValue);
            DataTable dtDes = ClstblContacts.tblContacts_SelectDesignerByContactID(ddlInstaller.SelectedValue);
            if (dtDes.Rows.Count > 0)
            {
                ddlDesigner.SelectedValue = ddlInstaller.SelectedValue;
            }
            else
            {
                ddlDesigner.SelectedValue = "";
            }

            DataTable dtEle = ClstblContacts.tblContacts_SelectElectricianByContactID(ddlInstaller.SelectedValue);
            if (dtEle.Rows.Count > 0)
            {
                ddlElectrician.SelectedValue = ddlInstaller.SelectedValue;
            }
            else
            {
                ddlElectrician.SelectedValue = "";
            }
        }
        else
        {
            ddlDesigner.SelectedValue = "";
            ddlElectrician.SelectedValue = "";
        }
    }

    protected void btnPickList_Click(object sender, EventArgs e)
    {

        //try
        //{
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID(ProjectID);
        if (PickListExist == 0)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtreason.Text, txtnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            if (success > 0)
            {
                Telerik_reports.generate_PickList(ProjectID);
                picklistdiv.Visible = false;
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            hdnPickListLogID.Value = string.Empty;
            txtreason.Text = string.Empty;
            txtnote.Text = string.Empty;
            txtreason.Enabled = true;
            txtnote.Enabled = true;
            picklistdiv.Visible = true;
            btnPickList.Attributes.Add("disabled", "disabled");
            btnDownload.Style.Add("display", "none");
            btnSubmit.Attributes.Remove("disabled");
        }

        //}
        //catch
        //{
        //}

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //try
        //{
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        if (!string.IsNullOrEmpty(txtnote.Text) && !string.IsNullOrEmpty(txtreason.Text))
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtreason.Text, txtnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            //            
            if (success > 0)
            {
                hdnPickListLogID.Value = success.ToString();
                DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();
                }
                else
                {
                    divPickList.Visible = false;
                }
                //ClientScriptManager cs = Page.ClientScript;
                //cs.RegisterStartupScript(this.GetType(), "modalstuff", "$('#<%=btnHidden.ClientID %>').click();", true);
                // Telerik_reports.generate_PickList(ProjectID);
                txtreason.Enabled = false;
                txtnote.Enabled = false;
                btnSubmit.Attributes.Add("disabled", "disabled");
                // btnSubmit.Enabled = false;
                btnDownload.Style.Add("display", "");
                SetAdd1();
            }
            else
            {
                txtreason.Enabled = true;
                txtnote.Enabled = true;
                btnSubmit.Enabled = true;
                btnDownload.Style.Add("display", "none");
                SetError1();
            }
        }

        //}
        //catch
        //{
        //}
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        try
        {
            Telerik_reports.generate_PickList(ProjectID);
        }
        catch
        {
            if (!string.IsNullOrEmpty(hdnPickListLogID.Value))
            {
                SetError1();
                picklistdiv.Visible = false;
                btnPickList.Attributes.Remove("disabled");
                bool suc = ClstblProjects.tbl_PickListLog_DeleteByID(hdnPickListLogID.Value);

                DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();
                }
                else
                {
                    divPickList.Visible = false;
                }

            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        picklistdiv.Visible = false;
        btnPickList.Attributes.Remove("disabled");

        if (!string.IsNullOrEmpty(hdnPickListLogID.Value))
        {
            bool suc = ClstblProjects.tbl_PickListLog_DeleteByID(hdnPickListLogID.Value);

            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }

            DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
            if (dtPickList.Rows.Count > 0)
            {
                divPickList.Visible = true;
                rptPickList.DataSource = dtPickList;
                rptPickList.DataBind();
            }
            else
            {
                divPickList.Visible = false;
            }

        }
    }
    protected void ddlformbaypostalcode_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        //ddlStreetState.Focus();
    }
    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    client.setHeaderHtml(HeaderHtml);
        //    client.setFooterHtml(FooterHtml);
        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    protected void btnRemoveInst_Click(object sender, EventArgs e)
    {
        //ModalPopupStockUpdate.Show();
        ddlstockstatus.SelectedValue = "";
        txtDate.Text = string.Empty;
        txtNotes.Text = string.Empty;
        txtProjNo.Text = string.Empty;
        //txtProjNo.Visible = false;
        modalstockupdate.Show();
    }
    protected void txtInstallBookingDate_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (txtInstallBookingDate.Text.Trim() != string.Empty)
        {
            if (txtInstallDays.Text.Trim() != string.Empty)
            {
                string date1 = txtInstallBookingDate.Text.Trim();
                string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

                ListItem item2 = new ListItem();
                item2.Text = "Installer";
                item2.Value = "";
                ddlInstaller.Items.Clear();
                ddlInstaller.Items.Add(item2);

                ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
                ddlInstaller.DataValueField = "ContactID";
                ddlInstaller.DataTextField = "Contact";
                ddlInstaller.DataMember = "Contact";
                ddlInstaller.DataBind();
            }
        }
    }
    protected void txtInstallDays_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (txtInstallDays.Text != string.Empty)
        {
            if (txtInstallBookingDate.Text != string.Empty)
            {
                string date1 = txtInstallBookingDate.Text.Trim();
                string date2 = Convert.ToString(Convert.ToDateTime(txtInstallBookingDate.Text.Trim()).AddDays(Convert.ToInt32(txtInstallDays.Text.Trim())));

                ListItem item2 = new ListItem();
                item2.Text = "Installer";
                item2.Value = "";
                ddlInstaller.Items.Clear();
                ddlInstaller.Items.Add(item2);

                ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectAvailInstaller(date1, date2);
                ddlInstaller.DataValueField = "ContactID";
                ddlInstaller.DataTextField = "Contact";
                ddlInstaller.DataMember = "Contact";
                ddlInstaller.DataBind();
            }
        }
    }
    protected void txtInstallCity_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(UpdatePanel1,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        PanAddUpdate.Visible = true;
        string[] cityarr = ddlStreetCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlStreetCity.Text = cityarr[0].Trim();
            ddlStreetState.SelectedValue = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
        }
        else
        {
            ddlStreetState.SelectedValue = string.Empty;
            ddlStreetCity.Text = string.Empty;
        }
    }
    protected void btnMove_Click(object sender, EventArgs e)
    {
        chkElecDistOK.Checked = false;
        divElecDistApproved.Visible = false;
        txtElecDistApproved.Text = string.Empty;
        txtInstallBookingDate.Text = string.Empty;
        rblAM1.Checked = false;
        rblAM2.Checked = false;
        rblPM1.Checked = false;
        rblPM2.Checked = false;
        txtInstallDays.Text = "1";
        txtInstallerNotes.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        ddlDesigner.SelectedValue = "";
        ddlElectrician.SelectedValue = "";
    }


    async Task RunAsync()
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
            SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
            SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
            string ProjectTypeName = "New";
            if (stProject.ProjectTypeID == "3")
            {
                ProjectTypeName = "Adding";
            }
            if (stProject.ProjectTypeID == "7")
            {
                ProjectTypeName = "Replacing";
            }
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
            SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
            DataTable dt = ClstblCustomers.tbl_formbayunittypeID_SelectByUnittype(stcustomer.unit_type);
            DataTable dt2 = ClstblCustomers.tbl_formbayStreettypeID_SelectByStreetCode(stcustomer.street_type);
            string installbooking = stProject.InstallBookingDate;
            Decimal stcrate = 0;
            if (!string.IsNullOrEmpty(installbooking))
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
            }
            string Unitid = "";
            String StreetId = "";
            if (dt.Rows.Count > 0)
            {
                Unitid = dt.Rows[0]["Id"].ToString();
            }
            if (dt2.Rows.Count > 0)
            {
                StreetId = dt2.Rows[0]["GreenBoatStreetType"].ToString();
            }

            string SystemMountingType = "Ground mounted or free standing";
            string typeofconnection = "Connected to an electricity grid with battery storage";
            if (stProject.InstallBase == "1")
            {
                SystemMountingType = "Building or structure";
            }
            if (Convert.ToBoolean(stProject.GridConnected))
            {
                typeofconnection = "Connected to an electricity grid without battery storage";
            }
            try
            {
                JObject oJsonObject = new JObject();
                oJsonObject.Add("Username", "arisesolar");
                oJsonObject.Add("Password", "arisesolar1");
                var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
                bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
                if (IsSuccess)
                {
                    var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
                    RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
                    string AccessToken = RootObject.TokenData.access_token;

                    litmessage.Text = AccessToken.ToString();
                    client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                    var panellist = new List<Panel>();
                    panellist.Add(new Panel { Brand = stProject.PanelBrand, Model = stProject.PanelModel, NoOfPanel = stProject.NumberPanels });
                    var inverterlist = new List<Inverter>();
                    inverterlist.Add(new Inverter { Brand = stProject.InverterBrand, Model = stProject.InverterModel, Series = stProject.InverterSeries });
                    //var testlist = new List<JobStcDetailsPanelBrand>();
                    //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                    //testlist.Add(new JobStcDetailsPanelBrand { Brand = stProject.PanelBrand, Model = stProject.PanelModel });
                    try
                    {
                        var content = new CreateJob
                        {

                            VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                            BasicDetails = new BasicDetails
                            {
                                JobType = 1,
                                RefNumber = stProject.ProjectNumber,
                                JobStage = 1,
                                Title = stcustomer.Customer,
                                StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                Priority = 1,
                                Description = stProject.ProjectNotes

                            },
                            JobOwnerDetails = new JobOwnerDetails
                            {

                                OwnerType = "Individual",
                                //CompanyName = "Customer Name",
                                FirstName = stCustContact.ContFirst,
                                LastName = stCustContact.ContLast,
                                Email = stCustContact.ContEmail,
                                Phone = stCustContact.ContMobile,
                                Mobile = stCustContact.ContMobile,
                                AddressID = 1,
                                UnitTypeID = Unitid,
                                UnitNumber = stcustomer.unit_number,
                                StreetName = stcustomer.street_name,
                                StreetNumber = stcustomer.street_number,
                                StreetTypeId = StreetId,
                                Town = stcustomer.StreetCity,
                                State = stcustomer.StreetState,
                                PostCode = stcustomer.StreetPostCode,
                                IsPostalAddress = false
                            },
                            JobInstallationDetails = new JobInstallationDetails
                            {
                                AddressId = 1,
                                UnitTypeID = Unitid,
                                UnitNumber = stcustomer.unit_number,
                                StreetName = stProject.street_name,
                                StreetNumber = stProject.street_number,
                                StreetTypeId = StreetId,
                                Town = stProject.InstallCity,
                                State = stProject.InstallState,
                                PostCode = stProject.InstallPostCode,
                                //isPostalAddress = false,
                                DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                NMI = stProject.NMINumber,
                                MeterNumber = stProject.MeterNumber1,
                                PhaseProperty = stProject.MeterPhase,
                                PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                SingleMultipleStory = sthouse.HouseType,
                                InstallingNewPanel = ProjectTypeName,
                                //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                ExistingSystem = false
                            },
                            JobSystemDetails = new JobSystemDetails
                            {
                                SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                //InstallationType = "Other"
                                //SystemModel = "TestModel",
                                //NoOfPanel = 5
                            },
                            JobStcDetails = new JobStcDetails
                            {
                                TypeOfConnection = typeofconnection,
                                SystemMountingType = SystemMountingType,
                                // DeemingPeriod = "13",
                                DeemingPeriod = stcrate.ToString(),
                                MultipleSguAddress = "No"
                            },
                            Panel = panellist.ToArray(),
                            Inverter = inverterlist.ToArray(),
                            InstallerView = new InstallerView
                            {
                                CECAccreditationNumber = stinst.Accreditation,
                                SeDesignRoleId = 1,
                                //Email = "richa@meghtechnologies.com",
                                FirstName = stinst.ContFirst,
                                LastName = stinst.ContLast,
                                Phone = stinst.ContMobile,
                                Mobile = stinst.ContMobile,
                                //ElectricalContractorsLicenseNumber = "9876543214",
                                AddressID = 1,
                                //UnitTypeID = 1,
                                //UnitNumber = "1",
                                StreetName = stcustomerInst.street_name,
                                StreetNumber = stcustomerInst.street_number,
                                StreetTypeId = 1,
                                Town = stcustomerInst.StreetCity,
                                State = stcustomerInst.StreetState,
                                PostCode = stcustomerInst.StreetPostCode,
                                IsPostalAddress = true,
                            }

                        };
                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                        // lbltesting.Text = temp.ToString();
                        var oTaskPostAsyncJob = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
                        bool IsSuccessJob = oTaskPostAsyncJob.IsSuccessStatusCode;

                        var JsonString = await oTaskPostAsyncJob.Content.ReadAsStringAsync();
                        RootObj RootObjectJob = JsonConvert.DeserializeObject<RootObj>(JsonString);
                        //Response.Write(JsonString + "   " + IsSuccessJob.ToString() + " Message: " + RootObjectJob.Message + " statusCode: " + RootObjectJob.StatusCode + "Status:" + RootObjectJob.Status);
                        //Response.Write(Convert.ToDateTime(stProject.InstallBookingDate));
                        //Response.End();

                        bool GreenBotUpdate = ClstblProjects.tblProjects_update_IsGreenBot("true", ProjectID);

                        litmessage.Text = RootObjectJob.Message;
                        PanError.Visible = true;
                    }
                    catch (Exception e)
                    {
                        litmessage.Text = e.Message;
                        PanError.Visible = true;
                        //Console.WriteLine(e.Message);
                    }
                }


            }
            catch (Exception e)
            {
                litmessage.Text = e.Message;
                //Console.WriteLine(e.Message);
            }
        }
    }
    public void FormbayRunAsync()
    {
        // lbltesting.Text = "hii2";
        try
        {
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }
            if (ProjectID != string.Empty)
            {
                // lbltesting.Text = "hii3";
                SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                // SttblElecDistributor stdist = ClstblElecDistributor.tblElecDistributor_SelectByElecDistributorID(stProject.ElecDistributorID);
                // SttblElecRetailer stelect = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(stProject.ElecRetailerID);
                SttblHouseType sthouse = ClstblHouseType.tblHouseType_SelectByHouseTypeID(stProject.HouseTypeID);
                string ProjectTypeName = "1";
                if (stProject.ProjectTypeID == "2")
                {
                    ProjectTypeName = "1";
                }
                else if (stProject.ProjectTypeID == "3")
                {
                    ProjectTypeName = "2";
                }
                else if (stProject.ProjectTypeID == "7")
                {
                    ProjectTypeName = "3";
                }

                SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stProject.Installer);
                //SttblContacts stelect = ClstblContacts.tblContacts_SelectByContactID(stProject.Electrician);
                //SttblContacts stdesig = ClstblContacts.tblContacts_SelectByContactID(stProject.Designer);
                SttblCustomers stcustomer = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
                SttblContacts stCustContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
                SttblCustomers stcustomerInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stinst.CustomerID);
                Sttbl_formbaystreettype stStreeType = ClstblStreetType.tbl_formbaystreettype_SelectByStreetCode(stProject.street_type);
                string installbooking = stProject.InstallBookingDate;
                Decimal stcrate = 0;
                if (!string.IsNullOrEmpty(installbooking))
                {
                    string date = Convert.ToDateTime(installbooking).ToShortDateString();
                    DateTime dt1 = Convert.ToDateTime(date);
                    String year = dt1.Year.ToString();
                    DataTable dt3 = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                    stcrate = Convert.ToDecimal(dt3.Rows[0]["STCRate"]);
                }
                string SystemMountingType = "Ground mounted or free standing";
                string typeofconnection = "Connected to an electricity grid with battery storage";
                if (stProject.InstallBase == "1")
                {
                    SystemMountingType = "Building or structure";
                }
                if (Convert.ToBoolean(stProject.GridConnected))
                {
                    typeofconnection = "Connected to an electricity grid without battery storage";
                }
                try
                {
                    //  lbltesting.Text = "hii4";
                    Dictionary<string, string> oJsonObject = new Dictionary<string, string>();
                    /// JObject oJsonObject = new JObject();
                    oJsonObject.Add("Username", "arisesolar");
                    oJsonObject.Add("Password", "Arise@123");
                    oJsonObject.Add("redirect_uri", "http://api.quickforms.com.au/");
                    oJsonObject.Add("client_id", "5D95C4B5-EE01-4D87-B1BC-B4C159CBEFDF");
                    // oJsonObject.Add("scope", "JobData");
                    oJsonObject.Add("scope", "CreateJob");
                    oJsonObject.Add("response_type", "code");

                    HttpClient client = new HttpClient();

                    var content1 = new FormUrlEncodedContent(oJsonObject);
                    var result = client.PostAsync("http://api.quickforms.com.au/auth/logon", content1).Result;
                    var tokenJson = result.Content.ReadAsStringAsync();
                    bool IsSuccess = result.IsSuccessStatusCode;
                    if (IsSuccess)
                    {
                        TokenObject RootObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson.Result);
                        string AccessToken = RootObject.access_token;
                        string CustomerID = RootObject.CustomerUserID;
                        //lbltesting.Text = AccessToken;
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        var panellist = new List<Panel>();
                        panellist.Add(new Panel { Brand = stProject.PanelBrand, Model = stProject.PanelModel, NoOfPanel = stProject.NumberPanels, STC = stProject.SystemCapKW, size = stProject.PanelOutput });
                        var inverterlist = new List<Inverter>();
                        inverterlist.Add(new Inverter { Brand = stProject.InverterBrand, Model = stProject.InverterModel, Series = stProject.InverterSeries, size = stProject.InverterOutput, noofinverter = Convert.ToInt32(stProject.inverterqty) });
                        var content = new CreateJob
                        {
                            Arisesolarid = Convert.ToInt32(ProjectID),
                            VendorJobId = Convert.ToInt32(stProject.ProjectNumber),
                            BasicDetails = new BasicDetails
                            {
                                JobType = Convert.ToInt32(stcustomer.ResCom),
                                RefNumber = stProject.ProjectNumber,
                                //JobStage = Convert.ToInt32(stcustomer.ResCom),
                                JobStage = Convert.ToInt32(ProjectTypeName),
                                Jobstatus = 1,
                                Createddate = DateTime.Now,
                                StrInstallationDate = Convert.ToDateTime(stProject.InstallBookingDate),
                                Description = stProject.ProjectNotes,
                            },
                            JobOwnerDetails = new JobOwnerDetails
                            {
                                //  OwnerType = "Individual",
                                CompanyName = stCustContact.ContFirst + " " + stCustContact.ContLast,
                                FirstName = stCustContact.ContFirst,
                                LastName = stCustContact.ContLast,
                                Phone = stCustContact.ContMobile,
                                Email = stCustContact.ContEmail,
                                Mobile = stCustContact.ContMobile,
                                // AddressID = 1,
                                UnitTypeID = stProject.unit_type,
                                UnitNumber = stProject.unit_number,
                                StreetName = stProject.street_name,
                                StreetNumber = stProject.street_number,
                                StreetTypeId = stStreeType.StreetCode,
                                StreetAddress = stProject.street_address,
                                Town = stcustomer.StreetCity,
                                State = stcustomer.StreetState,
                                PostCode = stcustomer.StreetPostCode,
                                IsPostalAddress = true,
                            },
                            JobInstallationDetails = new JobInstallationDetails
                            {
                                // AddressId = 1,
                                Inst_UnitNo = stProject.unit_number,
                                Inst_UnitType = stProject.unit_type,
                                Inst_StreetName = stProject.street_name,
                                Inst_StreetNo = stProject.street_number,
                                Inst_StreetType = stStreeType.StreetCode,
                                Inst_StreetAddress = stProject.street_address,
                                Inst_Suburb = stcustomer.StreetCity,
                                Inst_PostCode = stcustomer.StreetPostCode,
                                Inst_State = stcustomer.StreetState,

                                //isPostalAddress = false,
                                //  DistributorID = Convert.ToInt32(stdist.GreenBoatDistributor),
                                // ElectricityProviderID = Convert.ToInt32(stelect.ElectricityProviderId),
                                NMI = stProject.NMINumber,
                                // MeterNumber = stProject.MeterNumber1,
                                //  PhaseProperty = stProject.MeterPhase,
                                //PropertyType = stcustomer.ResCom == "1" ? "Residential" : "Commercial",
                                SingleMultipleStory = sthouse.HouseType,
                                Rooftype = stProject.RoofType,
                                RegPlanNo = stProject.RegPlanNo,
                                LotNo = stProject.LotNumber,

                                // InstallingNewPanel = ProjectTypeName,
                                //NoOfPanels = Convert.ToInt32(stProject.NumberPanels),
                                //  ExistingSystem = false 
                            },
                            JobSystemDetails = new JobSystemDetails
                            {
                                Systemtype = 1,
                                MountingType = 1,
                                ConnectionType = 1,
                                SystemSize = Convert.ToDecimal(stProject.SystemCapKW),
                                //InstallationType = "Other"
                                //SystemModel = "TestModel",
                                //NoOfPanel = 5
                            },
                            JobStcDetails = new JobStcDetails
                            {
                                TypeOfConnection = typeofconnection,
                                SystemMountingType = SystemMountingType,
                                //DeemingPeriod = "13",
                                DeemingPeriod = stcrate.ToString(),
                                MultipleSguAddress = "No"
                            },
                            Panel = panellist.ToArray(),
                            Inverter = inverterlist.ToArray(),
                            InstallerView = new InstallerView
                            {
                                Installerid = stinst.formbayid,
                                FirstName = stinst.ContFirst,
                                LastName = stinst.ContLast,
                                FullName = stProject.InstallerName,
                                //Customerid = 2,
                                Customerid = Convert.ToInt32(CustomerID),
                                CECAccreditationNumber = stinst.Accreditation,
                                SeDesignRoleId = 1,
                                IsCompleteUnit = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                                IsMoreSGU = stProject.ProjectTypeID == "3" ? "No" : "Yes",
                            },
                            ElectricianView = new ElectricianView
                            {
                                FullName = stProject.ElectricianName
                            },
                            DesignerView = new DesignerView
                            {
                                FullName = stProject.DesignerName
                            },
                            quickformGuid = stProject.quickformGuid
                        };

                        var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                        //lbltesting.Text = temp;
                        if (string.IsNullOrEmpty(stProject.quickformGuid))
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Createjob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.guidvalue))
                                    {
                                        bool succ_apicall = ClstblProjects.tblproject_update_quickformFlag(ProjectID, JObresult.guidvalue);
                                        bool check = ClstblProjects.tblProjects_update_IsQuickForm("True", Request.QueryString["proid"]);
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                        //   lbltesting.Text = JObresult.Message;
                                    }
                                    else
                                    {
                                        //   lbltesting.Text = JObresult.Message.ToString();
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (var client1 = new HttpClient())
                            {
                                client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/Updatejob/jobData");

                                //HTTP POST
                                var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                                postTask.Wait();

                                var resultdata = postTask.Result;
                                // lbltesting.Text = resultdata.ToString();
                                if (resultdata.IsSuccessStatusCode)
                                {
                                    var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                                    RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                                    //   litmessage.Text = jsondata.ToString();
                                    PanError.Visible = true;
                                    litmessage.Text = JObresult.Message;
                                    if (!string.IsNullOrEmpty(JObresult.code))
                                    {
                                        PanError.Visible = true;
                                        litmessage.Text = JObresult.Message;
                                    }
                                }
                            }
                        }
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    lbltesting.Text = e.Message;
                    //Console.WriteLine(e.Message);
                }
            }

        }
        catch (Exception e)
        {
            lbltesting.Text = e.Message;
            //Console.WriteLine(e.Message);
        }

    }
    protected async void GreenBotSave(object sender, EventArgs e)
    {
        await RunAsync();
    }
    protected void btnformbay1_Click(object sender, EventArgs e)
    {
        // FormbayRunAsync();

    }
    protected void btnquickfrom_Click(object sender, EventArgs e)
    {
        //lbltesting.Text = "dfdhfdhd";
        FormbayRunAsync();
    }

    protected void btnquickfromUpdate_Click(object sender, EventArgs e)
    {
        FormbayRunAsync();
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            string quickformguid = stProject.quickformGuid;
        }
    }

    protected void btnUpdateAddress_Click(object sender, EventArgs e)
    {

        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProject.CustomerID);
            SttblEmployees stEmpID = ClstblEmployees.tblEmployees_SelectByEmployeeID(stContact.EmployeeID);

            //if (stProject.IsFormBay == "2")
            {
                ModalPopupExtender1.Show();
                div_popup.Visible = true;

                ddlUnitType.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
                ddlUnitType.DataMember = "UnitType";
                ddlUnitType.DataTextField = "UnitType";
                ddlUnitType.DataValueField = "UnitType";
                ddlUnitType.DataBind();

                ddlstreetType.DataSource = ClstblStreetType.tblStreetType_SelectAsc();
                ddlstreetType.DataMember = "StreetTypeName";
                ddlstreetType.DataTextField = "StreetTypeName";
                ddlstreetType.DataValueField = "StreetTypeCode";
                ddlstreetType.DataBind();
                ddlstreetType.SelectedValue = stProject.street_type.ToUpper().ToString();

                txtUpdateFirstName.Text = stContact.ContFirst;
                txtUpdateLastName.Text = stContact.ContLast;
                txtContMobile.Text = stContact.ContMobile;
                txtCustPhone.Text = stCust.CustPhone;
                txtContEmail.Text = stContact.ContEmail;
                ddlUnitType.SelectedValue = stProject.unit_type.ToString();
                txtUnitNo.Text = stProject.unit_number;
                txtstreetno.Text = stProject.street_number;
                txtStreetName.Text = stProject.street_name;



                txtCity.Text = stProject.InstallCity;
                txtState.Text = stProject.InstallState;
                txtpostcode.Text = stProject.InstallPostCode;


            }
        }
    }
    protected void btnUpdateAddressData_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        if (ProjectID != string.Empty)
        //if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            //string ProjectID = Request.QueryString["proid"];
            SttblProjects stProject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(stProject.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProject.CustomerID);
            SttblEmployees stEmpID = ClstblEmployees.tblEmployees_SelectByEmployeeID(stContact.EmployeeID);
            // string formbayUnitNo = txtformbayUnitNo.Text;
            //string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            //string formbaystreettype = ddlformbaystreettype.SelectedValue;


            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = ddlStreetCity.Text;
            string InstallState = ddlStreetState.SelectedValue;
            string InstallPostCode = txtStreetPostCode.Text;
            txtInstallAddress.Text = formbayStreetNo + " " + formbaystreetname + " " + InstallCity + " " + InstallState + " " + InstallPostCode;
            string project = InstallCity + "-" + InstallAddress;
            //update data
            ClstblCustomers.tblCustomers_InsertContactsDetails(stProject.CustomerID, txtUpdateFirstName.Text, txtUpdateLastName.Text, txtContMobile.Text, txtContEmail.Text);

            //string suffix="";
            //if(stCust.street_suffix !=string.Empty)
            //{
            //    suffix=stCust.street_suffix;
            //}
            //ClstblCustomers.tblCustomer_Update_Address(stProject.CustomerID, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "");
            ClstblCustomers.tblCustomer_Update_Address(stProject.CustomerID, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "");

            bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(stProject.CustomerID, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, "");
            //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);
            //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtStreetAddressLine.Text);chkformbayid.Checked.ToString());
            ClstblCustomers.tblCustomers_UpdateAddressDetails(stProject.CustomerID, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text, txtCity.Text, txtState.Text, txtpostcode.Text, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text, txtCity.Text, txtState.Text, txtpostcode.Text, txtCustPhone.Text);

            bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, "", "", txtformbayStreetNo.Text, txtformbaystreetname.Text, "", hndstreetsuffix.Value, project);
            ClsProjectSale.tblProjects_Update_others(ProjectID, txtformbayNearby.Text, ddlStreetCity.Text, ddlStreetState.SelectedValue, txtStreetPostCode.Text, project);
            bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text);
        }
        BindProjects();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            BindProjectPreInst(Request.QueryString["proid"]);
        }
        else
        {
            BindProjectPreInst(HiddenField2.Value);
        }
        //BindProjectPreInst();
    }
    public void HTMLExportToPDF1(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    //client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/stcheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    //client.setHeaderHtml(HeaderHtml);
        //    //client.setFooterHtml(FooterHtml);

        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }



    //protected void btnCreateSTCForm_Click(object sender, ImageClickEventArgs e)
    protected void btnCreateSTCForm_Click(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        try
        {
            if (st.ProjectTypeID == "3")
            {
                Telerik_reports.generate_stcug(ProjectID);
            }
            else
            {
                Telerik_reports.generate_STCform(ProjectID);

            }


        }
        catch { }

    }

    public void GetPreInstClick()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            HiddenField2.Value = Request.QueryString["proid"];
            BindProjectPreInst(Request.QueryString["proid"]);
        }
    }
    public void GetPreInstClickByProject(string proid)
    {
        //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
        //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());
        HiddenField2.Value = proid;
        BindProjectPreInst(proid);
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }

    protected void ibtnCancel2_Click(object sender, EventArgs e)
    {
        //ModalPopupStockUpdate.Hide();
    }

    protected void rptaddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderAddress.Show();
        rptaddress.PageIndex = e.NewPageIndex;

        //string formbayUnitNo = txtformbayUnitNo.Text;
        //string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        //string formbaystreettype = ddlformbaystreettype.SelectedValue;

        //txtInstallAddress.Text =  formbayStreetNo + " " + formbaystreetname + " " ;
        string InstallAddress = txtInstallAddress.Text;

        string InstallCity = ddlStreetCity.Text;
        string InstallState = ddlStreetState.SelectedValue;
        string InstallPostCode = txtStreetPostCode.Text;
        txtInstallAddress.Text = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
        string CustomerID = Request.QueryString["compid"];
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        //rptaddress.DataSource = dt;
        // rptaddress.DataBind();
    }

    public void streetaddress()
    {
        address = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
        txtInstallAddress.Text = address;
        BindScript();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void ddlstockstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if(ddlstockstatus.SelectedIndex==3)
        //{
        //    txtProjNo.Text = string.Empty;
        //    txtprono.Visible = true;
        //}
        //else
        //{
        //    txtprono.Visible = false;            
        //}
        //modalstockupdate.Show();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string InstallBookingDate = txtInstallBookingDate.Text;
            string InstallerID = ddlInstaller.SelectedValue;
            string designerid = ddlDesigner.SelectedValue;
            string electricianid = ddlElectrician.SelectedValue;
            string DiscomDate = TextBox1.Text;
            string Bi_directionalDate = TextBox2.Text;

            chkElecDistOK.Checked = false;
            divElecDistApproved.Visible = false;
            txtElecDistApproved.Text = string.Empty;
            txtInstallBookingDate.Text = string.Empty;
            rblAM1.Checked = false;
            rblAM2.Checked = false;
            rblPM1.Checked = false;
            rblPM2.Checked = false;
            txtInstallDays.Text = "1";
            //txtInstallerNotes.Text = string.Empty;
            ddlInstaller.SelectedValue = "";
            ddlDesigner.SelectedValue = "";
            ddlElectrician.SelectedValue = "";
            //===========================================================================================
            // string InstallBookingDate = txtInstallBookingDate.Text.Trim();
            // string InstallDays = txtInstallDays.Text;
            // string ElecDistOK = Convert.ToString(chkElecDistOK.Checked) = false.ToString();
            string InstallerNotes = txtInstallerNotes.Text;
            //string ElecDistApproved = txtElecDistApproved.Text.Trim();

            // string formbayUnitNo = txtformbayUnitNo.Text;
            // string formbayunittype = ddlformbayunittype.SelectedValue;
            string formbayStreetNo = txtformbayStreetNo.Text;
            string formbaystreetname = txtformbaystreetname.Text;
            //  string formbaystreettype = ddlformbaystreettype.SelectedValue;


            string InstallAddress = txtInstallAddress.Text;

            string InstallCity = ddlStreetCity.Text;
            string InstallState = ddlStreetState.Text;
            string InstallPostCode = txtStreetPostCode.Text;
            txtInstallAddress.Text = formbayStreetNo + " " + formbaystreetname + " " + InstallCity + " " + InstallState + " " + InstallPostCode;
            //  string Installer = ddlInstaller.SelectedValue;
            // string Designer = ddlDesigner.SelectedValue;
            // string Electrician = ddlElectrician.SelectedValue;
            string StockAllocationStore = ddlStockAllocationStore.SelectedValue;
            //string InstallAM1 = Convert.ToString(rblAM1.Checked);
            //string InstallAM2 = Convert.ToString(rblAM2.Checked);
            //string InstallPM1 = Convert.ToString(rblPM1.Checked);
            //string InstallPM2 = Convert.ToString(rblPM2.Checked);
            string IsFormBay = "";
            String UpdatedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
            if (cbkIsFormBay.Checked == true)
            {
                IsFormBay = "1";
            }
            else
            {
                IsFormBay = "2";
            }
            //=============================================================================
            string ProjectID = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];
            }
            else
            {
                ProjectID = HiddenField2.Value;
            }
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string UpdatedBy = stEmp.EmployeeID;

            if (ProjectID != string.Empty)
            {
                //string ProjectID = Request.QueryString["proid"];
                ClstblProjects.tblProjects_Update_ProjectStatusID(ProjectID, "3");
                ClsProjectSale.tblProjects_UpdateIsFormBay(ProjectID, "2");
                ClstblProjects.tblProjects_UpdateInstallBookingDate(ProjectID, "");
                ClstblProjects.tblProjects_UpdateFormbayInstallBookingDate(ProjectID, "");
            }
            // string UpdatedBy = "";
            bool sucPreInst = false;
            sucPreInst = ClsProjectSale.tblProjects_UpdatePreInst(ProjectID, txtInstallBookingDate.Text, txtInstallDays.Text, chkElecDistOK.Checked.ToString(), InstallerNotes, txtElecDistApproved.Text, InstallAddress, InstallCity, InstallState, InstallPostCode, ddlInstaller.SelectedValue, ddlDesigner.SelectedValue, ddlElectrician.SelectedValue, "", "False", StockAllocationStore, Convert.ToString(rblAM1.Checked), Convert.ToString(rblAM2.Checked), Convert.ToString(rblPM1.Checked), Convert.ToString(rblPM2.Checked), UpdatedBy, IsFormBay, DiscomDate, Bi_directionalDate);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (string.IsNullOrEmpty(st.InstallBookingDate))
            {
                int suc1 = ClstblProjects.tblStockUpdate_Insert(ProjectID, st.ProjectNumber, ddlstockstatus.SelectedValue.ToString(), ddlstockstatus.SelectedItem.Text, txtDate.Text, txtNotes.Text, txtProjNo.Text, InstallBookingDate, InstallerID, designerid, electricianid, UpdatedDate, UpdatedBy);
                SetAdd1();
            }
            BindProjects();
        }
        catch (Exception ex)
        {
            SetError1();
        }
    }


    protected void btnPickupList_Click(object sender, EventArgs e)
    {
    }

    protected void rptattribute_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");



        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hdnStockCategoryId");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hndQty");
        HiddenField hndqty1 = (HiddenField)e.Item.FindControl("hndqty1");
        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;

        if (string.IsNullOrEmpty(hndqty1.Value))
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqty1.Value;
        }




        if (e.Item.ItemIndex == 0)
        {

            /// btnaddnew.Attributes.Remove("disabled");
            //litremove.Attributes.Remove("disabled");
            litremove.Visible = false;

        }
        else
        {

            // chkdelete.Visible = true;
            litremove.Visible = true;


        }


    }

    public void BindAddStockModel()
    {
        ScriptManager.RegisterStartupScript(updatestockpanel, this.GetType(), "MyAction", "doMyAction();", true);
    }
    //protected void btnaddnew_Click(object sender, EventArgs e)
    //{
    //    //AddmoreAttribute();
    //    //ModalPopupExtender4.Show();
    //}
    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("Qty", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;


            rpttable.Rows.Add(dr);

        }
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("Qty", Type.GetType("System.String"));
            // rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            // rpttable.Columns.Add("SysDetails", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "";
            //  dr["StockOrderItemID"] = "";
            dr["type"] = "";
            // dr["SysDetails"] = "";
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }

    protected void ddlStockCategoryID_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptattribute.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        ddlStockItem.Items.Clear();

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();
            // ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

        }

        ModalPopupExtender4.Show();
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;


        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        ModalPopupExtender4.Show();
    }

    protected void btnAddRow_Click(object sender, EventArgs e)
    {



        AddmoreAttribute();

        ModalPopupExtender4.Show();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID(ProjectID);
        if (PickListExist == 0)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtpicklistreason.Text, txtpicklistnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            if (success > 0)
            {
                Telerik_reports.generate_PickList(ProjectID);
                picklistdiv.Visible = false;
                SetAdd1();

                ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                #region insert item data

                ClstblProjects.tbl_PicklistItemsDetail_Delete(success.ToString());

                foreach (RepeaterItem item in rptattribute.Items)
                {
                    HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                    if (hdntype.Value != "1")
                    {

                        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                        // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                        TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                        int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                        //string stockitem = ddlStockItem.SelectedItem.Text;

                    }


                }
                #endregion
                ModalPopupExtender4.Hide();
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            ModalPopupExtender4.Hide();
            hdnPickListLogID.Value = string.Empty;
            txtpicklistreason.Text = string.Empty;
            txtpicklistnote.Text = string.Empty;
            txtpicklistreason.Enabled = true;
            txtpicklistnote.Enabled = true;
            picklistdiv.Visible = true;
            btnPickList.Attributes.Add("disabled", "disabled");
            btnDownload.Style.Add("display", "none");
            btnSubmit.Attributes.Remove("disabled");
            bindpicklistagain();
        }





    }


    // BindAddStockModel();

    //}
    //protected void Button3_Click(object sender, EventArgs e)
    //{

    //}

    protected void Button4_Click(object sender, EventArgs e)
    {

    }

    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int index = GetControlIndex(((DropDownList)sender).ClientID);
        //RepeaterItem item = rptattribute.Items[index];
        //DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        //DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        //if (ddlStockCategoryID.SelectedValue != string.Empty)
        //{
        //    hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();

        //}
    }

    protected void btnaddnew_Click1(object sender, EventArgs e)
    {
        string ProjectID = "";
        //bindrepeateragain();
        //BindAddedAttributeagain();
        //bindrepeater();
        //BindAddedAttribute();
        ProjectID = Request.QueryString["proid"];
        //DataTable dt = new DataTable();
        //dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
        //DataTable dt1 = new DataTable();
        //DataTable dt2 = new DataTable();
        //dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
        //                    new DataColumn("Qty",typeof(string)),
        //                     new DataColumn("StockCategoryID",typeof(int)),
        //                      new DataColumn("Type",typeof(int))});


        //if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
        //    }

        //}
        //if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
        //{

        //    if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
        //    }

        //}
        //if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
        //    }
        //}

        //if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
        //{
        //    if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
        //    {
        //        dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
        //    }

        //}

        //if (dt1.Rows.Count > 0)
        //{
        //    MaxAttribute = dt1.Rows.Count;
        //}
        //else
        //{
        //    MaxAttribute = 1;
        //}
        //// dr["Type"] = hdntype.Value;
        //rptattribute.DataSource = dt1;
        //rptattribute.DataBind();

        ////ModalPopupExtender4.Show();


        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }
        int PickListExist = ClstblProjects.tblPickListLog_ExistsByProjectID(ProjectID);
        if (PickListExist == 0)
        {
            bindrepeater();
            BindAddedAttribute();
            DataTable dt = new DataTable();
            dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int))});


            if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
            {
                if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
                }

            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
            {

                if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
                }

            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
            {
                if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
                }
            }

            if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
            {
                if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
                {
                    dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
                }

            }

            if (dt1.Rows.Count > 0)
            {
                MaxAttribute = dt1.Rows.Count;
            }
            else
            {
                MaxAttribute = 1;
            }
            // dr["Type"] = hdntype.Value;
            rptattribute.DataSource = dt1;
            rptattribute.DataBind();

            //ModalPopupExtender4.Show();
            ModalPopupExtender4.Show();
        }
        else
        {
            ModalPopupExtender4.Hide();
            hdnPickListLogID.Value = string.Empty;
            txtpicklistreason.Text = string.Empty;
            txtpicklistnote.Text = string.Empty;
            txtpicklistreason.Enabled = true;
            txtpicklistnote.Enabled = true;
            picklistdiv.Visible = true;
            btnPickList.Attributes.Add("disabled", "disabled");
            btnDownload.Style.Add("display", "none");
            btnSubmit.Attributes.Remove("disabled");
            bindpicklistagain();
        }

    }

    public void bindpicklistagain()
    {
        bindrepeateragain();
        BindAddedAttributeagain();
        String ProjectID = Request.QueryString["proid"];
        DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjects_ItemDetailsWise(ProjectID);
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        dt1.Columns.AddRange(new DataColumn[4] { new DataColumn("StockItemID", typeof(int)),
                            new DataColumn("Qty",typeof(string)),
                             new DataColumn("StockCategoryID",typeof(int)),
                              new DataColumn("Type",typeof(int))});


        if (!string.IsNullOrEmpty(dt.Rows[0]["PanelBrandID"].ToString()) || (dt.Rows[0]["PanelBrandID"].ToString()) != "0")
        {
            if (Convert.ToInt32(dt.Rows[0]["PanelBrandID"]) != 0)
            {
                dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["PanelBrandID"]), dt.Rows[0]["Panelqty"], dt.Rows[0]["PanelCategoryID"], dt.Rows[0]["Type"]);
            }

        }
        if (!string.IsNullOrEmpty(dt.Rows[0]["InverterDetailsId"].ToString()))
        {

            if (Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]) != 0)
            {
                dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["InverterDetailsId"]), dt.Rows[0]["inverterqty"], dt.Rows[0]["InverterCategoryId"], dt.Rows[0]["Type"]);
            }

        }
        if (!string.IsNullOrEmpty(dt.Rows[0]["SecondInverterDetailsID"].ToString()))
        {
            if (Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]) != 0)
            {
                dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["SecondInverterDetailsID"]), dt.Rows[0]["inverterqty2"], dt.Rows[0]["SecondInverterCategoryID"], dt.Rows[0]["Type"]);
            }
        }

        if (!string.IsNullOrEmpty(dt.Rows[0]["ThirdInverterDetailsID"].ToString()))
        {
            if (Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]) != 0)
            {
                dt1.Rows.Add(Convert.ToInt32(dt.Rows[0]["ThirdInverterDetailsID"]), dt.Rows[0]["inverterqty3"], dt.Rows[0]["ThirdInverterCategoryID"], dt.Rows[0]["Type"]);
            }

        }

        if (dt1.Rows.Count > 0)
        {
            MaxAttribute = dt1.Rows.Count;
        }
        else
        {
            MaxAttribute = 1;
        }
        // dr["Type"] = hdntype.Value;
        Repeater1.DataSource = dt1;
        Repeater1.DataBind();

        ModalPopupExtenderPickListAgain.Show();
    }
    protected void BindAddedAttributeagain()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("Qty", Type.GetType("System.String"));
        rpttable.Columns.Add("type", Type.GetType("System.String"));

        foreach (RepeaterItem item in Repeater1.Items)
        {
            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
            //TextBox txtSysDet = (TextBox)item.FindControl("txtSysDetails");
            HiddenField hdnStockOrderItemID = (HiddenField)item.FindControl("hdnStockOrderItemID");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            DataRow dr = rpttable.NewRow();
            dr["type"] = hdntype.Value;

            dr["StockCategoryID"] = ddlStockCategoryID.SelectedValue;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["Qty"] = txtQuantity.Text;


            rpttable.Rows.Add(dr);

        }
    }
    protected void bindrepeateragain()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("Qty", Type.GetType("System.String"));
            // rpttable.Columns.Add("StockOrderItemID", Type.GetType("System.String"));
            rpttable.Columns.Add("type", Type.GetType("System.String"));
            // rpttable.Columns.Add("SysDetails", Type.GetType("System.String"));
        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockItemID"] = "";
            dr["Qty"] = "";
            //  dr["StockOrderItemID"] = "";
            dr["type"] = "";
            // dr["SysDetails"] = "";
            rpttable.Rows.Add(dr);
        }
        Repeater1.DataSource = rpttable;
        Repeater1.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in Repeater1.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = Repeater1.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in Repeater1.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }
    protected void AddmoreAttributeagain()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttributeagain();
        bindrepeateragain();
    }

    protected void btnAddRowagain_Click(object sender, EventArgs e)
    {
        AddmoreAttributeagain();

        ModalPopupExtenderPickListAgain.Show();
    }

    protected void btnaddagain_Click(object sender, EventArgs e)
    {
        string ProjectID = "";
        String PickListDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField2.Value;
        }

        if (!string.IsNullOrEmpty(txtpicklistnote.Text) && !string.IsNullOrEmpty(txtpicklistreason.Text))
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            int success = ClstblProjects.tblPickListLog_Insert(ProjectID, txtpicklistreason.Text, txtpicklistnote.Text, PickListDate, st.InstallBookingDate, st.Installer, st.InstallerName, st.Designer, st.Electrician, UpdatedBy);
            //            
            if (success > 0)
            {
                hdnPickListLogID.Value = success.ToString();
                DataTable dtPickList = ClstblProjects.tbl_PickListLog_SelectByPId(ProjectID);
                if (dtPickList.Rows.Count > 0)
                {
                    divPickList.Visible = true;
                    rptPickList.DataSource = dtPickList;
                    rptPickList.DataBind();

                    ddlStockAllocationStore.SelectedValue = st.StockAllocationStore;
                    #region insert item data

                    ClstblProjects.tbl_PicklistItemsDetail_Delete(success.ToString());

                    foreach (RepeaterItem item in Repeater1.Items)
                    {
                        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

                        if (hdntype.Value != "1")
                        {

                            DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
                            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                            // DropDownList ddlStockAllocationStore = (DropDownList)item.FindControl("ddlStockAllocationStore");
                            TextBox txtOrderQuantity = (TextBox)item.FindControl("txtQuantity");
                            int suc = ClstblProjects.tbl_PicklistItemDetail_Insert(success.ToString(), ddlStockItem.SelectedValue, ddlStockItem.SelectedItem.ToString(), txtOrderQuantity.Text, ddlStockAllocationStore.SelectedItem.ToString(), ddlStockCategoryID.SelectedValue);

                            //string stockitem = ddlStockItem.SelectedItem.Text;                          
                        }


                    }
                    #endregion
                }
                else
                {
                    divPickList.Visible = false;
                }
                txtpicklistreason.Enabled = false;
                txtpicklistnote.Enabled = false;
                btnaddagain.Attributes.Add("disabled", "disabled");
                SetAdd1();
            }
            else
            {
                txtpicklistreason.Enabled = true;
                txtpicklistnote.Enabled = true;
                btnaddagain.Enabled = true;
                SetError1();
            }
        }

    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        Button litremove = (Button)item.FindControl("litremove");

        DropDownList ddlStockCategoryID = (DropDownList)e.Item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
        TextBox txtOrderQuantity = (TextBox)e.Item.FindControl("txtQuantity");



        DataTable dtStockCategory = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlStockCategoryID.DataSource = dtStockCategory;
        ddlStockCategoryID.DataTextField = "StockCategory";
        ddlStockCategoryID.DataValueField = "StockCategoryID";
        ddlStockCategoryID.DataBind();

        HiddenField hdnStockCategoryId = (HiddenField)e.Item.FindControl("hdnStockCategoryId");
        HiddenField hdnStockItem = (HiddenField)e.Item.FindControl("hdnStockItem");
        HiddenField hdbQty = (HiddenField)e.Item.FindControl("hdbQty");
        HiddenField hndqtynew = (HiddenField)e.Item.FindControl("hndqtynew");

        DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
        ddlStockItem.DataSource = dtStockItem;
        ddlStockItem.DataTextField = "StockItem";
        ddlStockItem.DataValueField = "StockItemID";
        ddlStockItem.DataBind();

        ddlStockCategoryID.SelectedValue = hdnStockCategoryId.Value;
        ddlStockItem.SelectedValue = hdnStockItem.Value;

        if (ddlStockItem.SelectedValue == "")
        {
            txtOrderQuantity.Text = "0";
        }
        else
        {
            txtOrderQuantity.Text = hndqtynew.Value;
        }




        if (e.Item.ItemIndex == 0)
        {

            /// btnaddnew.Attributes.Remove("disabled");
            //litremove.Attributes.Remove("disabled");
            litremove.Visible = false;

        }
        else
        {

            // chkdelete.Visible = true;
            litremove.Visible = true;


        }
    }

    protected void ddlStockCategoryID_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = Repeater1.Items[index];
        DropDownList ddlStockCategoryID = (DropDownList)item.FindControl("ddlStockCategoryID");
        DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
        ddlStockItem.Items.Clear();

        if (ddlStockCategoryID.SelectedValue != string.Empty)
        {
            hdnStockCategoryId.Value = ddlStockCategoryID.SelectedValue.ToString();
            DataTable dtStockItem = ClstblStockItems.tblStockItems_Select_Category(hdnStockCategoryId.Value);
            ddlStockItem.DataSource = dtStockItem;
            ddlStockItem.DataTextField = "StockItem";
            ddlStockItem.DataValueField = "StockItemID";
            ddlStockItem.DataBind();

        }

        ModalPopupExtenderPickListAgain.Show();
    }
    protected void litremove_Click1(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = Repeater1.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
        PanAddUpdate.Visible = true;


        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in Repeater1.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = Repeater1.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in Repeater1.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        ModalPopupExtenderPickListAgain.Show();
    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        txtformbaystreetname.Focus();


    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "address();", true);

        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbaystreetname.Text = txtformbaystreetname.Text;
        //}
        BindScript();

        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
        //ddlformbaystreettype.Focus();
        // Response.End();

    }
    protected void txtformbayNearby_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        txtformbaystreetname.Focus();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbayStreetNo.Text = txtformbayStreetNo.Text;
        //}
        BindScript();

    }
    protected void ddlformbaycity_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        // chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        ddlStreetState.Focus();
    }
    protected void ddlformbaystate_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        ddlStreetState.Focus();
    }
    protected void ddltaluka_SelectedIndexChanged(object sender, EventArgs e)
    {
        string taluka = ddltaluka.Text;
        if (!string.IsNullOrEmpty(taluka))
        {

        }
    }

    protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
    {
        string District = ddlDistrict.SelectedValue;
        if (!string.IsNullOrEmpty(District))
        {

        }
    }

    protected void ddlDownload_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDownload.SelectedValue == "1")
        {
            string filename = ddlDownload.SelectedItem.Text + ".pdf";
            string Filpath = Server.MapPath("~/userfiles/Download/" + filename);
            DownLoad(Filpath);
        }


    }
    public void DownLoad(string FName)
    {
        string path = FName;
        System.IO.FileInfo file = new System.IO.FileInfo(path);
        if (file.Exists)
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "Download; filename=" + file.Name); Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream"; //
        }
    }

    protected void txtinstCompDate_TextChanged(object sender, EventArgs e)
    {
        string instCompDate = txtinstCompDate.Text;
        string InstallBookingDate = txtInstallBookingDate.Text;
        if (!string.IsNullOrEmpty(InstallBookingDate))
        {
            bool result = Convert.ToBoolean((Convert.ToDateTime(instCompDate) >= Convert.ToDateTime(InstallBookingDate)));
            if (!result)
            {
                txtinstCompDate.BorderColor = System.Drawing.Color.Red;
                lnlError.Text = "InstallCompleteDate must be grather then InstallBookingDate";
                lnlError.ForeColor = System.Drawing.Color.Red;
                btnUpdatePreInst.Enabled = false;
            }
            else
            {
                txtinstCompDate.BorderColor = System.Drawing.Color.Black;
                lnlError.Text = "";
                lnlError.ForeColor = System.Drawing.Color.Black;
                btnUpdatePreInst.Enabled = true;
            }
        }

    }
}