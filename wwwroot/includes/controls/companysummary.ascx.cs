﻿using System;
using System.Web;
using System.Web.Security;

public partial class includes_companysummary : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindSummary();
    }
    public void BindSummary()
    {
        string CustomerID = Request.QueryString["compid"];
        if (!string.IsNullOrEmpty(CustomerID))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByCustomerID(CustomerID);
            //lblMrMs.Text = stCont.ContMr;
            lblFirstName.Text = stCont.ContFirst;
            lblLastName.Text = stCont.ContLast;
            lblmiddlename.Text = stCont.ContMiddle;

            lblEmail.Text = stCont.ContEmail;
            //lbltaluka.Text = stCont.Taluka;
            //lbldistrict.Text = stCont.District;
            //lblareatype.Text = stCont.AreaType;
            //lblrooftoparea.Text = stCont.RoofTopArea;
            //lblamuc.Text = stCont.AvgMonUnitCons;
            //lblamb.Text = stCont.AvgMonBill;
            //lblchnpart.Text = stCont.Chapanels;
            //lblpremises.Text = stCont.Premises;
            //lblcmc.Text = stCont.ComMeterConn;

            lblMobile.Text = "-";
            if (stCont.ContMobile != "")
            {
                lblMobile.Text = stCont.ContMobile;
            }
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblCompanyNumber.Text = st.CompanyNumber;
            lblCustomer.Text = st.Customer;
            lblAddress1.Text = st.Address1;
            lblAddress2.Text = st.Address2;
            //lblStreetAddress.Text = st.StreetAddress;
            lblStreetCity.Text = st.StreetCity;
            lblStreetState.Text = st.StreetState;
            lblStreetPostCode.Text = st.StreetPostCode;
            lbltaluka.Text = st.Taluka;
            lbldistrict.Text = st.District;
            lblareatype.Text = st.AreaType;
            lblrooftoparea.Text = st.RoofTopArea;
            lblamuc.Text = st.AvgMonUnitCons;
            

            lblamb.Text = st.AvgMonBill;
            lblchnpart.Text = st.Chapanels;
            lblpremises.Text = st.Premises;
            lblcmc.Text = st.ComMeterConn;

            if (st.ComMeterConn == "True")
            {
                lblcmc.Text = "Yes";
            }
            else if (st.ComMeterConn == "False")
            {
                lblcmc.Text = "No";
            }
            else
            {
                lblcmc.Text = "";
            }
            lblkilo.Text = st.kilowatt;
            lbllat.Text = st.Latitude;
            lbllong.Text = st.Longitude;
            
            lblPostalAddress.Text = "-";
            if (st.PostalAddress != "")
            {
                lblPostalAddress.Text = st.StreetAddress;
            }
            lblPostalCity.Text = "-";
            if (st.PostalCity != "")
            {
                lblPostalCity.Text = st.PostalCity;
            }
            lblPostalState.Text = "-";
            if (st.PostalState != "")
            {
                lblPostalState.Text = st.PostalState;
            }
            lblPostalPostCode.Text = "-";
            if (st.PostalPostCode != "")
            {
                lblPostalPostCode.Text = st.PostalPostCode;
            }
            lblCountry.Text = "-";
            if (st.Country != "")
            {
                lblCountry.Text = st.Country;
            }
            lblWebsite.Text = "-";
            if (st.CustWebSiteLink != "")
            {
                lblWebsite.Text = st.CustWebSiteLink;
            }

            lblPhone.Text = "-";
            if (st.CustPhone != "")
            {
                lblPhone.Text = st.CustPhone;
            }
            lblAltPhone.Text = "-";
            if (st.CustAltPhone != "")
            {
                lblAltPhone.Text = st.CustAltPhone;
            }
            lblType.Text = st.CustType;
            lblSource.Text = st.CustSource;
            if (lblSource.Text == "Door To Door")
            {
                trD2DEmployee.Visible = true;
                trD2DAppDate.Visible = true;
                trD2DAppTime.Visible = true;
            }

            if (st.CustSourceSubID != "0" && st.CustSourceSubID != "")
            {
                trSubSource.Visible = true;
                lblSubSource.Text = st.CustSourceSub;
            }
            else
            {
                trSubSource.Visible = false;
            }
            lblFax.Text = "-";
            if (st.CustFax != "")
            {
                lblFax.Text = st.CustFax;
            }
            
            string notes = "";
            lblNotes.Text = "-";
            if (st.CustNotes != "")
            {
                lblNotes.Text = st.CustNotes;
            }

            if (st.ResCom == "1")
            {
                lblRes.Text = "Res";
            }
            else
            {
                lblRes.Text = "Com";
            }
            lblArea.Text = "-";
            if (st.Area == "1")
            {
                lblArea.Text = "Metro";
            }
            else if (st.Area == "2")
            {
                lblArea.Text = "Regional";
            }

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (stEmp.SalesTeamID != string.Empty)
            {
                //if (stEmp.SalesTeamID == "10")
                if (Roles.IsUserInRole("Lead Manager") || Roles.IsUserInRole("leadgen"))
                {
                    trD2DEmployee.Visible = true;
                    trD2DAppDate.Visible = true;
                    trD2DAppTime.Visible = true;

                    lblD2DEmployee.Text = st.D2DEmpName;
                    try
                    {
                        lblD2DAppDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.D2DAppDate));
                    }
                    catch { }
                    lblD2DAppTime.Text = st.D2DAppTime;
                }
                else
                {
                    trD2DEmployee.Visible = false;
                    trD2DAppDate.Visible = false;
                    trD2DAppTime.Visible = false;
                }
            }

            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Verification"))
            {
                //trD2DEmployee.Visible = true;
                //trD2DAppDate.Visible = true;
                //trD2DAppTime.Visible = true;
                try
                {
                    lblD2DEmployee.Text = st.D2DEmpName;
                }
                catch { }
                try
                {
                    lblD2DAppDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.D2DAppDate));
                }
                catch { }
                try
                {
                    lblD2DAppTime.Text = st.D2DAppTime;
                }
                catch { }
            }
        }
    }
}