<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectTest.ascx.cs" Inherits="includes_controls_projectTest" %>

<script>
    
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedsale);
    function pageLoadedsale() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
        $('#<%=btnUpdateSale.ClientID %>').click(function () {
            formValidate();
        });

        
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();
        $('#<%=btnUpdateSale.ClientID %>').click(function () {
            formValidate();
        });
    });

    function callSomething(value) {
        $("#ctl00_ContentPlaceHolder1_lblRebateStatus").val(value);
        $("#ctl00_ContentPlaceHolder1_lblRebateStatus").text(value);

    }
    function callProjectSTatus(value) {
        $("#ctl00_ContentPlaceHolder1_lblstatus").val(value);
        $("#ctl00_ContentPlaceHolder1_lblstatus").text(value);

    }
    function Uploadfilesales123() {
        var uploadfiles = $('input[type="file"]').get(1);
        var fileUpload = document.getElementById("<%=FUmtcepaperwork.ClientID %>");
         $('#<%=hdnFileName.ClientID %>').val(fileUpload.files[0].name);
         var fromdata = new FormData();
         fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
        var choice = {};
        choice.url = "HandlerSales.ashx";
        choice.type = "POST";
        choice.data = fromdata;
        choice.contentType = false;
        choice.processData = false;
        choice.success = function (result) {
        };
        choice.error = function (err) {
        };
        $.ajax(choice);
    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
    function checkDec(el) {
      //  alert("chkk");
        var ex = /^[0-9]+\.?[0-9]*$/;
        if (ex.test(el.value) == false) {
            el.value = el.value.substring(0, el.value.length - 1);
        }
    }
    function Load() {
        console.log("Test Call");
    }

</script>
<style>
    .span_width100 span{display:block; width:100%;}
</style>
<asp:UpdatePanel ID="UpdatePanel" runat="server">
    <ContentTemplate>
           <asp:HiddenField ID="hdnFileName" runat="server" />
        <section class="row m-b-md" id="panprojectsale" runat="server">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea boxPadding formGrid">

                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate projectsale">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="divSitedetails" runat="server">
                                    <div class="widget flat radius-bordered borderone">
                                       <%-- <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Site Details</span>
                                        </div>--%>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan3">
                                                <div class="row">
                                                    <div class="col-md-3"><div class="form-group selpen" style="display:none">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    House Type
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlHouseType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"><div class="form-group selpen" style="display:none">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Elec Discom
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDist" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlElecDist_SelectedIndexChanged"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Sectioned Load
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtLotNum" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtLotNum"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ></asp:RegularExpressionValidator>--%>
                                                                <%--^\d+$--%>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." Style="color: red;"
                                                                    ValidationGroup="sale" ControlToValidate="txtLotNum" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Consumer Number
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtNMINumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                      
                                                    <div class="col-md-3" id="divSurveyCerti" runat="server" visible="false"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Survey Certificate
                                                                </label>
                                                            </span><span class="radiogruopmain">
                                                                <asp:RadioButtonList ID="rblSurveyCerti" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                   <div class="col-md-3" style="display:none"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Roof Type
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlRoofType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRoofType_SelectedIndexChanged"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Terrif Code
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecRetailer" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    
                                                    <div class="col-md-3" id="divFlatPanels" runat="server" visible="false"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <asp:Label ID="lblFlatPanels" runat="server"></asp:Label>:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtFlatPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtFlatPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" id="divCertiApprove" runat="server" visible="false"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Certificate Approve
                                                                </label>
                                                            </span><span class="radiogruopmain">
                                                                <asp:RadioButtonList ID="rblCertiApprove" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"  style="display:none"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Voltage Line
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtRegPlanNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Phase 1-2-3:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtMeterPhase" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMeterPhase" Style="color: red;"
                                                                    ValidationGroup="sale" MinimumValue="1" MaximumValue="3" Type="Integer" Text="Enter 1, 2 or 3."
                                                                    Display="Dynamic" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" id="divPitchedPanels" runat="server" visible="false"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <asp:Label ID="lblPitchedPanels" runat="server"></asp:Label>:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPitchedPanels" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtPitchedPanels"
                                                                    ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    
                                                    <div class="col-md-3" style="display: none;"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Roof Angle
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlRoofAngle" runat="server" aria-controls="DataTables_Table_0" CssClass="myval form-control"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display: none;"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Upgrade
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlmeterupgrade" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control" Width="200">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>                                                    
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>

                                <div id="divDistAppDetail" runat="server" >
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue" style="display:none">
                                            <span class="widget-caption">Distributor Application Detail</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan4">
                                                <div class="row">
                                                    <div class="col-md-3" style="display:none"><div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label23" runat="server" class="control-label">
                                                Application Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtElecDistApplied" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none"><div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label1" runat="server" class="  control-label">
                                              Application Number</asp:Label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtdistributorapplicationnumber" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </span>
                                                        </div></div>
                                                    <div class="col-md-3"  style="display:none"><div class="form-group selpen" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    GEDA No:
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtgedano" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div></div>
                                                    <div class="col-md-3"  style="display:none"><div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label2" runat="server" class="  control-label">
                                               GEDA Approved Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtElecDistAppDate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"  style="display:none"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Employee:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    
                                                    <div class="col-md-3" style="display: none"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Dist Approved By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDistAppBy" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    
                                                    <div class="col-md-3"  style="display:none"><div class="form-group selpen">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Applied By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlElecDistApplyMethod" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    
                                                    
                                                    <div class="col-md-3"  style="display:none"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Approval Ref:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtApprovalRef" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" runat="server" visible="false"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    From Email/Fax:
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtElecDistApplySentFrom" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div></div>
                                                    
                                                    <!--<div class="col-md-3"><%--<div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Dist Applied:
                                                            </label>
                                                        </span><span>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtElecDistApplied" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="padding-left: 7px;">
                                                                        <asp:ImageButton ID="Image15" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender15" runat="server" PopupButtonID="Image15"
                                                                            TargetControlID="txtElecDistApplied" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtElecDistApplied"
                                                                            ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                        <%--       <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" class="col-sm-2  control-label">
                                                <strong></strong></asp:Label>
                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="" runat="server" class="form-control" placeholder="">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>--%>
                                                        <%--<div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Dist Approved Date:
                                                            </label>
                                                        </span><span>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtElecDistAppDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="padding-left: 7px;">
                                                                        <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="ImageButton1"
                                                                            TargetControlID="txtElecDistAppDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtElecDistAppDate"
                                                                            ID="RegularExpressionValidator7" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>--%>
                                                    </div>-->
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>


                                </div>
                                <div id="divMeterDetails" runat="server" style="display:none">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Meter Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan6">
                                                <div class="row">
                                                    <div class="col-md-3" style="display: none;"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Peak Meter No:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPeakMeterNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display: none"><div class="form-group selpen">
                                                            <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                            </span>--%><span class="checkbox-info checkbox">



                                                                <label for="<%=chkEnoughMeterSpace.ClientID %>">
                                                                    <asp:CheckBox ID="chkEnoughMeterSpace" runat="server" />
                                                                    <span class="text">&nbsp;Enough Meter Space:</span>
                                                                </label>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none;"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Off-Peak Meters:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtOffPeakMeters" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3" style="display:none;"><div class="form-group selpen">
                                                            <%--<span class="name paddtop2px floatleft">
                                                            <label class="control-label">
                                                                
                                                            </label>
                                                        </span>--%><span class="checkbox-info checkbox">

                                                            <label for="<%=chkIsSystemOffPeak.ClientID %>">
                                                                <asp:CheckBox ID="chkIsSystemOffPeak" runat="server" />
                                                                <span class="text">&nbsp;Is System Off-Peak:</span>
                                                            </label>
                                                        </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <%--<div class="col-md-3" style="display:none;"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Phase 1-2-3:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtMeterPhase" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                <asp:RangeValidator ID="valrDate" runat="server" ControlToValidate="txtMeterPhase" Style="color: red;"
                                                                    ValidationGroup="sale" MinimumValue="1" MaximumValue="3" Type="Integer" Text="Enter 1, 2 or 3."
                                                                    Display="Dynamic" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>--%>
                                                    <div class="col-md-3"><div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Estimate:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtMeterEstimate" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"><div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Estimated Value:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlestimatevalue" runat="server" AppendDataBoundItems="true" Width="222px">
                                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"><div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="Label3" runat="server" class="  control-label">
                                               Estimate Payment Due:</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtestimatepaymentdue" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div></div>
                                                    
                                                    <div class="col-md-3"><div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Status For Net Meter:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtstatusfornetmeter" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                    <div class="col-md-3"><div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Estimated Paid:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEstimatedPaid" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div></div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div id="divPanelDetail" runat="server">
                                    <asp:Panel runat="server" ID="Pan1">
                                        <div class="box dropDownwid100"> 
                                            <div class="boxHeading"> 
                                                <h4 class="formHeading">Panel Details</h4>
                                            </div>  
                                            <div class="boxPadding">
                                                <div class="row">
                                                    <div class="col-sm-4 form-group">
                                                        <label class="control-label">
                                                            Select Panel
                                                        </label>
                                                        <asp:Label ID="labl" runat="server"></asp:Label>
                                                        <asp:DropDownList ID="ddlPanel" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPanel_SelectedIndexChanged1" AppendDataBoundItems="true">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-4 form-group">
                                                        <label class="control-label">
                                                            Brand 
                                                        </label>
                                                        <asp:TextBox ID="txtPanelBrand" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-4 form-group">
                                                        <label class="control-label">
                                                            No. of Panels
                                                        </label>
                                                        <asp:TextBox ID="txtNoOfPanels" runat="server" MaxLength="10" CssClass="form-control"
                                                            AutoPostBack="true" OnTextChanged="txtNoOfPanels_TextChanged"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" Style="color: red;"
                                                            ControlToValidate="txtNoOfPanels" Display="Dynamic" ErrorMessage="Number Only"
                                                            ValidationGroup="sale" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                    </div>
                                                    <%--<div class="col-md-3" style="display:none;"><div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Actual Subsidy Amt
                                                            </label>
                                                            <br />
                                                        </span><span class="pricespan">
                                                            <asp:TextBox ID="txtRebate" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRebate"
                                                                ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                                ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                        </div></div>
                                                    --%>
                                            
                                                    <div class="col-sm-4">
                                                        <label class="control-label">
                                                            Watts/Panel 
                                                        </label>
                                                        <asp:TextBox ID="txtWatts" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label">
                                                            Model
                                                        </label>
                                                        <asp:TextBox ID="txtPanelModel" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label">
                                                            System Capacity
                                                        </label>
                                                        <asp:TextBox ID="txtSystemCapacity" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-3" style="display:none;"><div class="form-group selpen">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                subsidy PCR No.
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtsubsidypcrno" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="display: none;">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    STC Mult
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSTCMult" runat="server" Text="1" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3" style="display: none;">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Zone Rt
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtZoneRt" runat="server" MaxLength="200" CssClass="form-control" Text="0"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="divInverterDetail" runat="server">
                                    <asp:Panel runat="server" ID="Pan2">
                                        <div class="box dropDownwid100"> 
                                            <div class="boxHeading"> 
                                                <h4 class="formHeading">Inverter Details</h4>
                                            </div>  
                                            <div class="boxPadding">
                                                <div class="row">
                                                    
                                                    <div class="selpen col-md-3">
                                                        <span class="name disblock ">
                                                            <label class="control-label">
                                                                Select Inverter
                                                            </label>
                                                        </span>
                                                        <span>
                                                            <asp:DropDownList ID="ddlInverter" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                OnSelectedIndexChanged="ddlInverter_SelectedIndexChanged" AutoPostBack="true"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                    </div>
                                                    <div class="selpen col-md-1">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Qty1
                                                            </label>
                                                        </span>
                                                        <span>
                                                            <asp:TextBox ID="txtqty" runat="server" Text="0" MaxLength="50" CssClass="form-control alignright" ></asp:TextBox>
                                                            <%--<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtqty"
                                                                ValidationGroup="sale" MinimumValue="1" Type="Integer" Text="Enter Qty greater than 0"
                                                                Display="Dynamic" />--%>
                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" Style="color: red;"
                                                                ControlToValidate="txtqty" ErrorMessage="Enter Qty &gt; than 0"
                                                                Operator="GreaterThan" Type="Integer" ValidationGroup="sale" Display="Dynamic"
                                                                ValueToCompare="0" />
                                                        </span>
                                                    </div>
                                                        
                                                    <div class="selpen col-md-3 dispnone">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                2nd Inverter
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlInverter2" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                OnSelectedIndexChanged="ddlInverter2_SelectedIndexChanged" AutoPostBack="true"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                    </div>
                                                    <div class="selpen col-md-1 dispnone">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Qty2
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtqty2" Text="0" runat="server" MaxLength="50" CssClass="form-control alignright" ></asp:TextBox>
                                                        </span>
                                                    </div>
                                                        
                                                    <div class="selpen col-md-3 dispnone">
                                                        <span class="name disblock ">
                                                            <label class="control-label">
                                                                3rd Inverter
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlInverter3" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInverter3_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                    </div>
                                                    <div class="selpen col-md-1 dispnone">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Qty3
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtqty3" runat="server" MaxLength="50" CssClass="form-control alignright" ></asp:TextBox>
                                                        </span>
                                                    </div>
                                                        
                                                    <div class="col-md-2 selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Brand
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInverterBrand" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                    </div>
                                                    <div class="col-md-2 selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Series
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSeries" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                     </div>
                                                    <div class="col-md-4 selpen" style="display: none;">
                                                            <div class="row">
                                                                <div class="col-md-3 ">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            STC
                                                                        </label>
                                                                    </span>
                                                                    <span class="spicalspanwith">
                                                                        <div class="first1">
                                                                            <asp:TextBox ID="txtSTCNo" runat="server" Enabled="false" MaxLength="50" CssClass="form-control"
                                                                                ></asp:TextBox>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="second1">
                                                                        <label class="control-label">
                                                                            Value
                                                                        </label>
                                                                    </div>
                                                                    <div class="thrid1">
                                                                        <asp:TextBox ID="txtSTCValue" runat="server" Enabled="false" MaxLength="50" CssClass="form-control alignright floatleft dolarsingn" Width="80" Text="0"></asp:TextBox>

                                                                    </div>
                                                                    <div class="four1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Inverter Output
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInverterOutput" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                </div>

                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                    </div>
                                                    <div class="col-md-3 selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Model
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInverterModel" runat="server" MaxLength="200" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                            </span>
                                                    </div>
                                                    <div class="selpen col-md-1">
                                                        <label class="control-label">Size</label>
                                                        <%--<input type="text" class="form-control">--%>
                                                        <asp:TextBox ID="txtsize" runat="server" AutoPostBack="true"  OnTextChanged="txtsize_TextChanged" CssClass="form-control" disabled="disabled"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4 displaynone768" style="display:none;">
                                                        <div class="">
                                                            <div class="form-group selpen">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                    </label>
                                                                </span><span></span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group selpen">
                                                                <span class="name disblock">
                                                                    <label class="control-label">
                                                                    </label>
                                                                </span><span></span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="form-group selpen">
                                                            <div style="height: 50px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                

                                <div id="div3" runat="server">
                                    <asp:Panel runat="server" ID="Panel3">
                                        <div class="box dropDownwid100"> 
                                            <div class="boxHeading"> 
                                                <h4 class="formHeading">Pricing Details</h4>
                                            </div>  
                                            <div class="boxPadding">
                                                <div class="row">

                                                    <div class="form-group col-sm-3 selpen">

                                                        <span class="name">
                                                        <label class="control-label">
                                                            Actual Cost
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <asp:TextBox ID="txtRebate" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left" onkeyup="checkDec(this);"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtRebate"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>
                                                    <div class="form-group col-sm-3 selpen">

                                                        <span class="name">
                                                        <label class="control-label">
                                                            40% Subsidy
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <asp:TextBox ID="fortysubsidy" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fortysubsidy"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>
 
                                                    <div class="form-group col-sm-3 selpen">

                                                        <span class="name">
                                                        <label class="control-label">
                                                            20% Subsidy
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <asp:TextBox ID="twntysubsidy" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="twntysubsidy"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 selpen">
                                                        <span class="name">
                                                        <label class="control-label">
                                                            Actual Subsidy Amount
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <asp:TextBox ID="txtactualamt" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server" ControlToValidate="txtactualamt"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 selpen">
                                                        <span class="name">
                                                        <label class="control-label">
                                                            Total Payable Amount
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <asp:TextBox ID="txttotalamt" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left" disabled="disabled"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server" ControlToValidate="txttotalamt"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>
                                                     <div class="col-sm-3 selpen">
                                                        <span class="name">
                                                        <label class="control-label">
                                                       Deposite Required
                                                        </label>
                                                        </span>
                                                        <span class="pricespan">
                                                        <%--<asp:TextBox ID="txtDepoReq" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left" OnTextChanged="txtDepoReq_TextChanged" AutoPostBack="true" ></asp:TextBox>--%>
                                                        <asp:TextBox ID="txtDepoReq" runat="server" MaxLength="15" CssClass="form-control  floatleft dolarsingn" Style="width: 100%; text-align: left" AutoPostBack="true" ></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server" ControlToValidate="txttotalamt"
                                                            ValidationGroup="sale" Display="Dynamic" ErrorMessage="Number Only" Style="color: red;"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-3 selpen">
                                                        <span class="name">
                                                        <label class="control-label">
                                                            EleStructureCost
                                                        </label>
                                                        </span>
                                                        <span class="floatleftin">
                                                       <asp:TextBox ID="txtOther"  runat="server" MaxLength="15"  OnTextChanged="txtOther_TextChanged" AutoPostBack="true" CssClass="form-control alignright dolarsingn"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtOther" Display="Dynamic" ErrorMessage="Number Only"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        </span>
                                                    </div>

                                                    <div class="col-sm-3 selpen">
                                                        <span class="name">
                                                        <label class="control-label">
                                                            Net Total
                                                        </label>
                                                        </span>
                                                        <span class="floatleftin">
                                                       <asp:TextBox ID="txtnettotal"  runat="server" CssClass="form-control alignright dolarsingn" disabled="disabled"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>

                                

                                
                                <div id="div1" runat="server" style="display:none">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Select Type</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Panel1">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">
                                                                    <label for="<%=rblonlypanel.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblonlypanel" GroupName="qq" Checked="" />
                                                                        <span class="text">Only Panel&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">
                                                                    <label for="<%=rblonlyinverter.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblonlyinverter" GroupName="qq" Checked="" />
                                                                        <span class="text">Only Inverter&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span>
                                                                <div class="radio radio-info radio-inline">

                                                                    <label for="<%=rblboth.ClientID %>">
                                                                        <asp:RadioButton runat="server" ID="rblboth" GroupName="qq" Checked="" />
                                                                        <span class="text">Both&nbsp; </span>
                                                                    </label>
                                                                </div>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="" id="divSPA" runat="server" visible="false">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">SP Ausnet Payment</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan5">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    SPA Inv No
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSPAInvoiceNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group  dateimgarea selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Date Paid
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:TextBox ID="txtSPAPaid" runat="server" Width="85" CssClass="form-control"></asp:TextBox>
                                                                <asp:ImageButton ID="Image16" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender16" runat="server" PopupButtonID="Image16"
                                                                    TargetControlID="txtSPAPaid" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtSPAPaid" Style="color: red;"
                                                                    ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen ">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Paid By
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSPAPaidBy" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>

                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Amount Paid
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSPAPaidAmount" runat="server" MaxLength="20" CssClass="form-control"
                                                                    Width="200px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtSPAPaidAmount" Style="color: red;"
                                                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group selpen ">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Pay Method
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSPAPaidMethod" runat="server" AppendDataBoundItems="true"
                                                                    Width="200px" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div id="divVicRebate" runat="server" style="display:none">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Vic Govt Rebate Detail</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Panel2">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Rebate App Ref:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtVicAppRefNo" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name disblock">
                                                                <asp:Label ID="lblVicdate" runat="server" class="control-label">
                                                Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtVicdate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Vic Rebate:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlvicrebate" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval form-control">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                    <asp:ListItem Value="1">With Rebate Install</asp:ListItem>
                                                                    <asp:ListItem Value="0">Without Rebate Install</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Note:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtVicNote" CssClass="form-control" runat="server" Width="100%" Height="78px" TextMode="MultiLine"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div id="divMtce" runat="server" visible="false">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Maintenance Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan7">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group dateimgarea">
                                                            <span class="name">
                                                                <label class="control-label">Call Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtOpenDate" runat="server" CssClass="form-control" Width="100px" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Image3" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image3"
                                                                                TargetControlID="txtOpenDate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtOpenDate" ID="RegularExpressionValidator8" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Call Reason1: </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceReasonID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Call Reason2: </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceReasonSubID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group dateimgarea checkboxmain3">
                                                            <%--<span class="name paddtop9none">
                                                            <label class="control-label"></label>
                                                        </span>--%><span>
                                                            <asp:CheckBox ID="chkWarranty" runat="server" />
                                                            <label for="<%=chkWarranty.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                            &nbsp;Warranty
                                                        </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Call Method </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceCallID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sales Rep</label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    Width="200px" AppendDataBoundItems="true" Enabled="false">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Call Status</label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlProjectMtceStatusID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value=""> Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Work Done</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtWorkDone" runat="server" Width="300px" Height="108px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Customer's Description of Work to be Done</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtCustomerInput" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Description of Fault Identified</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtFaultIdentified" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Description of ActionsThis value is required. to Fix</label>
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtActionRequired" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div id="divsupplier" runat="server" visible="false">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Inverter Replacement Details</span>
                                        </div>
                                        <div class="widget-body">
                                            <asp:Panel runat="server" ID="Pan8">
                                                <div class="col-md-4">
                                                    <div id="divthrough" class="form-group" runat="server">
                                                        <span class="name">
                                                            <label class="control-label">Through Type </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlthroughtype" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlthroughtype_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select </asp:ListItem>
                                                                <asp:ListItem Value="1">Supplier Through </asp:ListItem>
                                                                <asp:ListItem Value="2">In House Through </asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div id="divsupplierthrough" runat="server" visible="false">
                                                    <div class="col-md-4" id="divinvioceno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice No. </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtinvoiceno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="oldserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Old Serial No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtoldserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divlogdement" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtlogdement" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="divlogdementdate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtlogdementdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="img2" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="img2"
                                                                                TargetControlID="txtlogdementdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtlogdementdate" ID="RegularExpressionValidator10" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divtrackingno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Tracking no </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txttrackingno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="Divnewserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">New Serail No  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtnewserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="diverrormessage" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Error Message </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txterrormessage" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divdaulty" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Faulty UNit Pickup Date& Time </label>
                                                            </span><span>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtfaultydate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgfaulty" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" PopupButtonID="imgfaulty"
                                                                                TargetControlID="txtfaultydate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtfaultydate" ID="RegularExpressionValidator11" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divpickthrough" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Pickup Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlpickthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divdeliveryrec" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Delievery Received  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtdeliveryreceived" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divsentsupplier" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent to Suppllier Date&time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsentsupplier" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgsent" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" PopupButtonID="imgsent"
                                                                                TargetControlID="txtsentsupplier" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtsentsupplier" ID="RegularExpressionValidator12" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divsentthrough" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlsentthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="divinviocedate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice Sent Date&Time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtinvoicesent" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imginvoice" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" PopupButtonID="imginvoice"
                                                                                TargetControlID="txtinvoicesent" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txtinvoicesent" ID="RegularExpressionValidator13" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divinhousethrough" runat="server" visible="false">

                                                    <div class="col-md-4" id="holdserialno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Old Serial No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="htxtoldserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="hlogdementno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement No </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txthlogdementno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4" id="divhlogdementdate" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Logdement Date </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txthlogdementdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Imghlogdate" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" PopupButtonID="Imghlogdate"
                                                                                TargetControlID="txthlogdementdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txthlogdementdate" ID="RegularExpressionValidator14" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div6" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Installer Pickup </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlinstallerpickup" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="Divhnewserailno" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">New Serail No  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txthnewserialno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="divherrormesage" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Error Message </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtherrormesage" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div9" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Faulty UNit Pickup Date& Time </label>
                                                            </span><span>

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txth_faultyunit" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="Imgfaultyunit" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender8" runat="server" PopupButtonID="Imgfaultyunit"
                                                                                TargetControlID="txth_faultyunit" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txth_faultyunit" ID="RegularExpressionValidator16" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4" id="div11" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Delievery Received  </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtdeliveryrec" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div12" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent to Suppllier Date&time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txt_hsentsupplierdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="imgsentsupplier" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender9" runat="server" PopupButtonID="imgsentsupplier"
                                                                                TargetControlID="txt_hsentsupplierdate" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txt_hsentsupplierdate" ID="RegularExpressionValidator17" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div13" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Sent Through </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlhsentthrough" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0">Select </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="col-md-4" id="div14" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice Sent Date&Time </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txt_incoicesent_date" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>&nbsp;<asp:ImageButton ID="img_invoice" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                            <cc1:CalendarExtender ID="CalendarExtender10" runat="server" PopupButtonID="img_invoice"
                                                                                TargetControlID="txt_incoicesent_date" Format="dd/MM/yyyy">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:RegularExpressionValidator ControlToValidate="txt_incoicesent_date" ID="RegularExpressionValidator18" runat="server" ErrorMessage="Enter valid date" Style="color: red;"
                                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="div2" runat="server">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">Invoice No. </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txt_invoiceno" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row dispnone">
                            <div class="col-sm-12">
                                <div class="dividerLine"></div>
                            </div>
                            <%--<div class="col-md-12 textRight">
                                <asp:Button CssClass="btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnUpdateSale" runat="server" OnClick="btnUpdateSale_Click"
                                    Text="Save" CausesValidation="true" ValidationGroup="sale" />
                            </div>--%>
                        </div>
                    </asp:Panel>

                      <div class="quateapage">
                        <asp:Panel ID="Panel4" runat="server" CssClass="PanAddUpdate">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel5" runat="server">
                                        <div class="box dropDownwid100"> 
                                            <div class="boxHeading"> 
                                                <div class="row">
                                                <div class="col-md-9">
                                                <h4 class="formHeading">Quote Details</h4>
                                                </div>
                                                <div class="col-md-3 textRight">
                                                <%--<asp:ImageButton ID="btnCreateQuote" runat="server" ImageUrl="~/images/btn_create_new_quote.png" OnClick="btnCreateQuote_Click" CausesValidation="false" />--%>
                                                                    <asp:LinkButton ID="btnCreateQuote" OnClick="btnCreateQuote_Click" runat="server" CausesValidation="false" class="btn btn-labeled btn-primary">
                                                                    <i class="btn-label fa fa-file-pdf-o" style="margin-right:5px;"></i>Create Quote
                                                                    </asp:LinkButton>
                                                </div>
                                                </div>

                                            </div>  
                                            <div class="boxPadding">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                                <asp:Panel runat="server"   ID="PanQuoteSent" Enabled="false">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label4" runat="server" class="  control-label">First Quote Sent</asp:Label></span>
                                                                    <div class="input-group date wid100 datetimepicker1 ">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtQuoteSent" runat="server" class="form-control" Width="125px">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <span class="name">
                                                                    <asp:Label ID="Label5" runat="server" class="  control-label">Quote Accepted</asp:Label></span>
                                                                <div class="input-group date wid100 datetimepicker1 ">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtQuoteAcceptedQuote" runat="server" class="form-control" Width="125px">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label class="control-label">Upload Quote</label>
                                                                    <span id="Span1" runat="server">
                                                                    <label class="custom-fileupload">
                                                                        <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        
                                                                        
                                                                            <asp:FileUpload ID="FUmtcepaperwork" runat="server" class="custom-file-input"  onchange="Uploadfilesales123();" />
                                                                            <span class="custom-file-control form-control-file form-control"></span>
                                                                            <span class="btnbox"> Attach file</span>
                                                                        
                                                                    </label>
                                                                    </span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label class="control-label">&nbsp;</label>
                                                                <div>
                                                                    <a href="#" class="btn btn-labeled btn-primary">Request Signature</a>
                                                                    <a href="#" class="btn btn-labeled btn-primary">Send Quote</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        
                                                        <div class="row dispnone">
                                                            <div class="col-md-5 col-sm-5" id="divQuotes2" runat="server" visible="false">
                                                                <h5>Quotes</h5>
                                                            </div>
                                                            <div class="col-md-7 col-sm-7" style="text-align: right;">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="row finalgrid documentGrid quotesTab" id="divQuotes" runat="server" visible="false">
                                                            <div class="col-md-12 overX" runat="server" id="divquo">
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 25%; text-align: center">Quote Date
                                                                            </th>
                                                                            <th style="width: 20%; text-align: center">Doc No.
                                                                            </th>
                                                                            <th style="width: 15%; text-align: center">Signed
                                                                            </th>
                                                                            <th style="width: 20%; text-align: center">Document
                                                                            </th>
                                                                            <th style="width: 20%; text-align: center">Email
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <asp:Repeater ID="rptQuote" runat="server" OnItemDataBound="rptQuote_ItemDataBound" OnItemCommand="rptQuote_ItemCommand">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td style="width: 25%; text-align: center">
                                                                                    <%#Eval("ProjectQuoteDate", "{0:ddd - dd MMM yyyy}")%>
                                                                                </td>
                                                                                <td style="width: 20%; text-align: center">
                                                                                    <%#Eval("ProjectQuoteDoc")%>
                                                                                </td>
                                                                                <td style="width: 15%; text-align: center;">
                                                                                    <%-- <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" >
                                                                                        <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                                                        <span class="text">&nbsp;</span>
                                                                                    </label>--%>
                                                                                    <%--<asp:Label runat="server" ID="lblsignchked" Visible="false"><i class="fa fa-check-square" style="width:20px;height:20px;"></i></asp:Label>
                                                                                    <asp:Label runat="server" ID="lblsignUnChked"><i class="fa fa-square"></i></asp:Label>--%>
                                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/check.png" Visible="false" />
                                                                                </td>
                                                                                <td style="width: 25%; text-align: center;">
                                                                                    <asp:HiddenField ID="hndProjectQuoteID" runat="server" Value='<%#Eval("ProjectQuoteID") %>' />
                                                                                    <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                        <%-- <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/icon_document_downalod.png" /> --%>
                                                                                        <i class="fa fa-file-text-o"></i>
                                                                                    </asp:HyperLink>
                                                                                </td>
                                                                                <td style="width: 20%; text-align: center">
                                                                                    <asp:LinkButton ID="btnSendAMail" runat="server" Visible="false" CommandName="MailAttachedPDf" CommandArgument='<%#Eval("ProjectQuoteID")%>' CssClass="addPreloader" >
                                                                                           <i class="fa fa-envelope-o fa-lg"></i>
                                                                                       <%--<span class="typcn typcn-mail" ></span>--%>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                   
                                                    
                                                    
                                                    <div class="col-md-12" style="padding-left: 0px; display:none">
                                                        <div class="form-group col-md-5">
                                                            <asp:LinkButton ID="lnksendmessage" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success addPreloader" OnClick="lnksendmessage_Click">
                                                            <i class="glyphicon glyphicon-envelope"></i>  Send Message
                                                            </asp:LinkButton>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <asp:LinkButton ID="lnksendbrochures" Style="width: 163px; margin-top: 20px;" runat="server" CausesValidation="false" class="btn btn-labeled btn-success addPreloader" OnClick="lnksendbrochures_Click">
                                                            <i class="glyphicon glyphicon-envelope"></i>  Send Quote
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="form-group dateimgarea fileuploadmain col-md-3 dispnone" runat="server" id="divmtcepaperwork">
                                                        
                                                    </div>
                                                    <div class="form-group dateimgarea fileuploadmain col-md-3 dispnone">
                                                        <span>
                                                            <label class="control-label">&nbsp;</label>
                                                            <span>
                                                                <label class="control-label" style="padding-left:35px;">Signed Quote Stored</label>
                                                                <asp:CheckBox ID="chkSignedQuote" runat="server" AutoPostBack="true" OnCheckedChanged="chkSignedQuote_CheckedChanged" />
                                                                <label for="<%=chkSignedQuote.ClientID %>">
                                                                </label>

                                                            </span>
                                                            <span id="divSQ" runat="server" visible="false">
                                                                <asp:HyperLink ID="lblSQ" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                <span class="dateimg">
                                                                    <span class="file-input btn btn-azure btn-file">

                                                                        <asp:FileUpload ID="fuSQ" runat="server" />
                                                                    </span>
                                                                </span>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorSQ" runat="server" ErrorMessage="This value is required." ValidationGroup="quote" ControlToValidate="fuSQ" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                        </span>
                                                    </div>
                                                        
                                                       
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6" id="ssactive" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <asp:Label ID="Label6" runat="server" class="  control-label">SS Active Date</asp:Label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtssdate" runat="server" class="form-control" Width="125px">
                                                                </asp:TextBox>
                                                            </div>
                                                            <br />
                                                            <asp:Button class="btn btn-primary savewhiteicon" ID="btnSave" runat="server" OnClick="btnSave_Click" CausesValidation="false" Text="Solar Service Active" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                               
                                        <div class="projectquote" runat="server" visible="false">
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">System Detail</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group ">
                                                                <span class="name" style="width: 140px;">
                                                                    <label class="control-label">
                                                                        Panel Details
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtPanelDetails" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="form-group ">
                                                                <span class="name" style="width: 140px;">
                                                                    <label class="control-label">
                                                                        Inverter Details
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtInverterDetails" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    

                                    <div class="box dropDownwid100"> 
                                        <div class="boxHeading"> 
                                        <div class="flexRow">
                                            <h4 class="formHeading flexRow">Invoice Details - &nbsp;<asp:Label ID="lblpaymentstatus" runat="server" class="badge greenBadge"></asp:Label></h4>
                                            <asp:Panel ID="PanelMain" runat="server">
                                                    <span class="dateimg flexRow">
                                                        <span style="margin-right:5px;">
                                                            <%--<asp:Button ID="btnCreateInvoice" runat="server" OnClick="btnCreateInvoice_Click" CssClass="createinvoice martopzero" Text="Create Invoice" CausesValidation="false" />--%>
                                                            <asp:LinkButton ID="btnCreateInvoice" runat="server" OnClick="btnCreateInvoice_Click" CausesValidation="false" class="btn btn-labeled btn-primary"><i class="btn-label fa fa-file-text" style="margin-right:5px;"></i>Create Invoice

                                                            </asp:LinkButton>
                                                        </span>
                                                        
                                                        <asp:Panel ID="Panel6" runat="server">
                                                            <span class="dateimg">
                                                                <uc1:InvoicePayments ID="InvoicePayments1" runat="server" />
                                                            </span>
                                                        </asp:Panel>
                                                        
                                                        <%--    <asp:ImageButton ID="btnOpenInvoice" runat="server" OnClick="btnOpenInvoice_Click"
                                                                ImageUrl="../../images/btn_openinvoice.png" CausesValidation="false"/>
                                                        --%>

                                                            <asp:LinkButton ID="btnOpenInvoice" runat="server" OnClick="btnOpenInvoice_Click" CssClass="btn btn-primary"
                                                               CausesValidation="false" style="margin-left:5px;"><i class="btn-label fa fa-file-text" style="margin-right:5px;"></i>Open Invoice
                                                               </asp:LinkButton>
                                                        

                                                    </span>
                                            </asp:Panel>
                                        </div>
                                        </div>  
                                        <div class="boxPadding">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <asp:Panel ID="Panel31" runat="server" CssClass="padleftzero">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Invoice No
                                                                        </label>
                                                                    </span>
                                                                    <span>
                                                                        <asp:TextBox ID="txtInvoiceNumber" runat="server" Enabled="false" CssClass="form-control" ></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtInvoiceNumber"
                                                                            ValidationGroup="quote" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Panel ID="Panel32" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Invoice Doc
                                                                    </label>
                                                                </span>
                                                                <span>
                                                                    <asp:TextBox ID="txtInvoiceDoc" MaxLength="10" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                                                        ValidationGroup="quote" ControlToValidate="txtInvoiceDoc" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                </span>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Panel runat="server" ID="Panel33">
                                                            <%--InvoiceSent--%>
                                                                <span class="name">
                                                                    <asp:Label ID="Label9" runat="server" class="  control-label">Invoice Sent</asp:Label></span>
                                                                <div class="input-group date wid100 datetimepicker1">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtInvoiceSentquote" runat="server" class="form-control" Width="125px" disabled="disabled">
                                                                    </asp:TextBox>
                                                                </div>
                                                            
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Total Paid Amount:
                                                            </label>
                                                        </span>
                                                        <span class="amtvaluebox" >
                                                                <asp:Label ID="lblTotalPaidAmount" runat="server" class="form-control" disabled="disabled"></asp:Label>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Panel ID="divDepRec" runat="server">
                                                            <span class="name">
                                                                <asp:Label ID="Label7" runat="server" class="  control-label">
                                                                    Deposit Rec</asp:Label>
                                                            </span>

                                                            <div class="input-group date wid100 datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtDepositReceived" runat="server" Enabled="false" class="form-control" Width="125px">
                                                                </asp:TextBox>
                                                            </div>

                                                            <asp:RegularExpressionValidator ValidationGroup="quote" ControlToValidate="txtDepositReceived" ID="RegularExpressionValidator22" runat="server" ErrorMessage="Enter valid date" Display="Dynamic"
                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                        <%--    <asp:CustomValidator ClientValidationFunction="CompareDates" ID="cvTodate" runat="server" ErrorMessage="Invalid End Date" ControlToValidate="txtDepositReceived" Display="Dynamic"></asp:CustomValidator>
                                                         --%>   <asp:HiddenField ID="hiddenExpiryDate" runat="server" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDepRec" runat="server" ErrorMessage="This value is required." CssClass="comperror" Visible="false"
                                                                ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="txtDepositReceived" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Panel ID="PanActiveDate" runat="server">
                                                            <span class="name">
                                                                <asp:Label ID="Label8" runat="server" class="  control-label">Active Date</asp:Label>
                                                            </span>
                                                            <div class="input-group date wid100 datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtActiveDate" runat="server"  Enabled="false" class="form-control" Width="125px">
                                                                </asp:TextBox>
                                                                 <asp:HiddenField ID="hdndeprec" runat="server" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAD" runat="server" ErrorMessage="*" Visible="false"
                                                                    ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="txtActiveDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-3 dispnone">
                                                        <span class="name disblock">
                                                            <label class="control-label" >
                                                                Payment Status:
                                                            </label>
                                                        </span>
                                                        <span class="amtvaluebox">
                                                                
                                                        </span>
                                                    </div>
                                                    
                                                    
                                                    
                                                    <div class="col-md-5 dispnone">
                                                        <%--<div class="row">
                                                            <asp:Panel ID="Panel4" runat="server">
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-bottom: 11px !important;">
                                                                        <asp:ImageButton ID="imgbtnPerforma" runat="server" OnClick="imgbtnPerforma_Click"
                                                                            ImageUrl="~/images/proformainvoice.png" CausesValidation="false" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div style="height: 49px;">
                                                                        <asp:LinkButton ID="btnWelcomeDocs" Style="width: 163px" runat="server" OnClick="btnWelcomeDocs_Click" CausesValidation="false" class="btn btn-labeled btn-primary"><i class="btn-label fa fa-info"></i>Welcome Letter
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>--%>
                                                        
                                                        <div class="clear"></div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                
                                                                <div class="col-md-4" id="divrecept" runat="server" visible="false">
                                                                    <span class="name disblock">
                                                                        <label class="control-label" style="width:76px">
                                                                            Receipt No:
                                                                        </label>
                                                                    </span><span class="amtvaluebox">
                                                                        <b>
                                                                            <asp:Label ID="lblreceipt" runat="server" Style="width: 130px;"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--<div class="form-group">
                                                            <div class="row">
                                                                
                                                                <div class="col-md-3">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Total Paid Amount:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblTotalPaidAmount" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Payment Status:
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblpaymentstatus" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3" id="divrecept" runat="server" visible="false">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Receipt No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        </label>
                                                                    </span><span class="amtvaluebox" style="width: 100px;">
                                                                        <b>
                                                                            <asp:Label ID="lblreceipt" Width="100px" runat="server"></asp:Label></b>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>--%>
                                                    </div>
                                                </div>
                                                <div class="form_group" style="display:none;">
                                                    <div style="width: 100%; display: inline-block; margin-top: 28px" runat="server" enabled="false">
                                                        <div class=" checkbox-info checkbox">
                                                            <span class="fistname">
                                                                <label for="<%=chkactive.ClientID %>" class="control-label">
                                                                    <asp:CheckBox ID="chkactive" runat="server" />
                                                                    <span class="text">Ready to Active   </span>
                                                                </label>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                           

                                    <asp:Panel ID="Panel7" runat="server">
                                        <div style="display:none">
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">Document Upload Detail</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkMeterBoxPhotosSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkMeterBoxPhotosSaved" runat="server" onclick="fileChange()" />
                                                                            <span class="text">Meter Photos Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span runat="server" class="floatleft" id="divMP" style="display: none">
                                                                        <asp:HyperLink ID="lblMP" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuMP" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorMP" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuMP" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkElecBillSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkElecBillSaved" runat="server" onclick="fileChange1()" />
                                                                            <span class="text">Electricity Bill Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divEB" style="display: none">
                                                                        <asp:HyperLink ID="lblEB" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuEB" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEB" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuEB" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkProposedDesignSaved.ClientID %>">
                                                                            <asp:CheckBox ID="chkProposedDesignSaved" runat="server" onclick="fileChange2()" />
                                                                            <span class="text">Prop Design Saved  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divPD" style="display: none">
                                                                        <asp:HyperLink ID="lblPD" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuPD" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPD" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuPD" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkPaymentReceipt.ClientID %>">
                                                                            <asp:CheckBox ID="chkPaymentReceipt" runat="server" onclick="fileChange3()" />
                                                                            <span class="text">Payment Receipt  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="divPR" style="display: none">
                                                                        <asp:HyperLink ID="lblPR" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fuPR" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPR" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fuPR" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chkbeatquote.ClientID %>">
                                                                            <asp:CheckBox ID="chkbeatquote" runat="server" onclick="fileChange4()" />
                                                                            <span class="text">Beat Quote  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="Spanbeat" style="display: none">
                                                                        <asp:HyperLink ID="lblbeat" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="fubeat" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorbeat" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="fubeat" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=chknearmap.ClientID %>">
                                                                            <asp:CheckBox ID="chknearmap" runat="server" onclick="fileChange5()" />
                                                                            <span class="text">NearMap Photo  &nbsp;</span>
                                                                        </label>
                                                                    </span>
                                                                    <span id="Spanmap" style="display: none">
                                                                        <asp:HyperLink ID="lblnearmap" runat="server" Target="_blank"></asp:HyperLink>
                                                                        <span class="file-input btn btn-azure btn-file">
                                                                            <asp:FileUpload ID="funearmap" runat="server" />
                                                                        </span>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatormap" runat="server" ErrorMessage="*"
                                                                            ValidationGroup="quote" SetFocusOnError="true" ControlToValidate="funearmap" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row" id="divAddUpdate" runat="server">
                                            <div class="col-sm-12">
                                                <div class="dividerLine"></div>
                                            </div>
                                            <div class="col-md-12 textRight">
                                                <asp:Button CssClass="btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnUpdateSale" runat="server" OnClick="btnUpdateSale_Click"
                                                Text="Save" CausesValidation="true" ValidationGroup="sale" />
                                            </div>
                                            <%--<div class="col-md-12 textRight">
                                                    <asp:Button class="btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnUpdateQuote" runat="server" OnClick="btnUpdateQuote_Click" ValidationGroup="quote" Text="Save" />
                                            </div>--%>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>

                </div>
            </div>
        </section>

    </ContentTemplate>
    <Triggers>
        
        <asp:PostBackTrigger ControlID="btnOpenInvoice" />
        <asp:PostBackTrigger ControlID="btnCreateQuote" />
        <%--<asp:PostBackTrigger ControlID="ddlpanel" />--%>
    </Triggers>
</asp:UpdatePanel>





