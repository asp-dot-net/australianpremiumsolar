﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using AjaxControlToolkit;
using System.Configuration;
using System.Drawing;

public partial class includes_controls_documents : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        BindGrid(0);
        errorLabel.Text = "";
        BindDocNameDropdown();
    }
    public void BindDocNameDropdown()
    {
        string CustomerID = Request.QueryString["compid"];
        if (!string.IsNullOrEmpty(CustomerID))
        {
            ddlDocumentName.Items.Clear();
            ListItem item = new ListItem();
            item.Value = "";
            item.Text = "Select Document";
            ddlDocumentName.Items.Add(item);

            var data = ClstblDocuments.tblDocuments_SelectActive_Project(CustomerID);
            ddlDocumentName.DataSource = data;
            ddlDocumentName.DataMember = "DocumentName";
            ddlDocumentName.DataTextField = "DocumentName";
            ddlDocumentName.DataValueField = "Id";
            ddlDocumentName.DataBind();
        }

    }
    public void HideTabContainer()
    {
        //divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }

    public void HideContactAction()
    {
        //  GridView1.Columns[7].Visible = false;
        divrightbtn.Visible = false;

        if (Roles.IsUserInRole("Administrator"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            divrightbtn.Visible = false;
        }
    }
    public void ShowContactAction()
    {
        //GridView1.Columns[7].Visible = true;
        //divrightbtn.Visible = true;
    }

    public void BindDocuments()
    {
        HidePanels();
        BindDocNameDropdown();
        //TabAccount.Visible = false;
        //divrightbtn.Visible = true;
        BindGrid(0);
        //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        //ddlSelectRecords.DataBind();

        string CustomerID = Request.QueryString["compid"];
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager"))
        {

            if (stCust.CustTypeID == "3" || stCust.CustTypeID == "4")
            {
                //  TabAccount.Visible = true;
            }
            else
            {
                //TabAccount.Visible = false;
            }
        }
    }
    protected DataTable GetGridData()
    {
        string emptypeValue = string.Empty;
        DataTable dt = new DataTable();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string CustomerID = Request.QueryString["compid"];
        if (!string.IsNullOrEmpty(CustomerID))
        {
             dt = ClstblDocuments.tblCustomerDocumentsDetail_SelectByUserIdCust(userid, CustomerID);

            if (dt.Rows.Count == 0)
            {
                PanSearch.Visible = true;
            }
        }
        return dt;

    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;
        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string CustomerID = Request.QueryString["compid"];
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        string EmployeeID = st.EmployeeID;

        string ContactEntered = DateTime.Now.AddHours(14).ToString();
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        string ContactEnteredBy = st2.EmployeeID;

        string DocumentId = hdnsize.Value.ToString();
        string number = txtNumber.Text;
        string Filename = "";
        int success = 0;
        int exist = ClstblDocuments.tbldocuments_ExistsByCustomerID(CustomerID);
        //if (exist == 0)
        //{

        try
        {
            string filename = hdnFileName.Value.ToString();
            string PDFFilename = hdnFileName.Value.ToString();
            FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
            //  bool sucSQ = ClsProjectSale.tblProjects_UpdateSQ(ProjectID, SQ);
            SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
            SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");
            /* int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, ApplicationNo, PDFFilename);*/
            success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, number, PDFFilename, ContactEnteredBy, DateTime.Now);

            SetAdd1();
            PanAddUpdate.Visible = false;
            divrightbtn.Visible = true;
            BindGrid(0);

        }
        catch (Exception ex)
        {
            //throw ex;
        }

    }
    protected void ibtnUploadPDF_Click(object sender, EventArgs e)
    {
        try
        {
            //System.Threading.Thread.Sleep(100000);
            if (FileUpload1.HasFile)
            {
                //SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                string PDFFilename = string.Empty;
                string Filename = FileUpload1.FileName;
                string ID = hdnItemID.Value;
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                string CustomerID = Request.QueryString["compid"];
                //if (!string.IsNullOrEmpty(ID))
                //{
                PDFFilename = Filename;

                //if (!string.IsNullOrEmpty(st.FileName))
                //{
                //    SiteConfiguration.DeletePDFFile("StockItemPDF", PDFFilename);
                //}

                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                //SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");

                bool update = ClstblDocuments.tblCustomerDocuments_UpdateFileName(CustomerID, Filename);
                SetAdd1();
                BindGrid(0);
                //}
            }
        }
        catch (Exception ex)
        {
            throw ex;
            // SetError1();
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string CustomerID = Request.QueryString["compid"];
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        string EmployeeID = st.EmployeeID;
        string ContactEntered = DateTime.Now.AddHours(14).ToString();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string ContactEnteredBy = st.EmployeeID;

        string ContTitle = "";
        string ContMr = "";
        string ContFirst = "";
        string ContLast = "";
        string ContMobile = "";
        string ContNotes = "";
        string ContEmail = "";
        string ContMr2 = "";
        string ContFirst2 = "";
        string ContLast2 = "";

        SttblContacts stCon = ClstblContacts.tblContacts_SelectByContactID(id1);
        bool success = ClstblContacts.tblContacts_Update(id1, CustomerID, EmployeeID, "", "", "", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", ContactEntered, ContactEnteredBy, ContTitle, ContMr, ContFirst, ContLast, ContMobile, "", "", "", ContEmail, "", "", "False", "", "", "", "", ContNotes, "", "", "", "", "", "False", "False", "False", "False", "", "False", "", "", "False", "False", "", "", "", ContMr2, ContFirst2, ContLast2);

        //--- do not chage this code Start------
        if (success)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
        //--- do not chage this code end------
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    public void ClearContactSelection()
    {
        //divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ClearContactSelection();
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);
    }

    //protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

    //    if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
    //    {
    //        GridView1.AllowPaging = false;
    //        BindGrid(0);
    //    }
    //    else
    //    {
    //        GridView1.AllowPaging = true;
    //        GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
    //        BindGrid(0);
    //    }
    //}

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        PanSearch.Visible = false;

        ClearContactSelection();
        InitAdd();
        divrightbtn.Visible = false;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkAdd.Visible = false;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>loader();</script>");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

        PanGrid.Visible = true;
        PanSearch.Visible = true;

        Reset();
        PanAddUpdate.Visible = false;
        divrightbtn.Visible = true;
    }
    public void SetAdd()
    {
        divrightbtn.Visible = true;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnReset.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = true;
        btnReset.Visible = true;
        // btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = false;

        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;

        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtNumber.Text = string.Empty;
        ddlDocumentName.SelectedValue = "";
    }
    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnStockitemID2 = (HiddenField)e.Row.FindControl("hndContactID");
            string StockItemID = hdnStockitemID2.Value;
            string CustomerID = Request.QueryString["compid"];
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
            if (!string.IsNullOrEmpty(StockItemID))
            {
                SttblDocuments st = ClstblDocuments.tblCustomerDocList_SelectByID(StockItemID);
                HyperLink lnkPDFUploded = (HyperLink)e.Row.FindControl("lnkPDFUploded");
                HyperLink lnkPDFPending = (HyperLink)e.Row.FindControl("lnkPDFPending");
                if (!string.IsNullOrEmpty(st.FileName))
                {
                    lnkPDFUploded.Visible = true;
                    lnkPDFPending.Visible = false;
                    lnkPDFUploded.NavigateUrl = pdfURL + "CustomerDocuments/" + st.FileName;
                    lnkPDFUploded.Target = "_blank";
                }
                else
                {
                    lnkPDFUploded.Visible = false;
                    lnkPDFPending.Visible = true;
                }
            }
        }
    }

    //protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    //{
    //    // ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);
    //    //Response.Write(TabContainer2.ActiveTabIndex);
    //    //Response.End();
    //    if (TabContainer2.ActiveTabIndex == 0)
    //    {
    //        //contactdetail1.BindContactDetail();
    //    }
    //    //else if (TabContainer2.ActiveTabIndex == 1)
    //    //{
    //    //    contactinfo2.BindContactInfo();
    //    //}
    //    //else if (TabContainer2.ActiveTabIndex == 3)
    //    //{
    //    //    contactaccount2.BindCreateAccount();
    //    //}
    //}

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        SttblDocuments st = ClstblDocuments.tblCustomerDocList_SelectByID(id);
        SiteConfiguration.deleteimage(st.FileName, "CustomerDocuments");
        bool sucess1 = ClstblDocuments.tblCustomerdocument_Delete(id);

        if (sucess1)
        {
            divrightbtn.Visible = true;
            SetAdd1();
        }
        else
        {
            SetError();
        }
        Reset();
        PanAddUpdate.Visible = false;
        BindGrid(0);
    }
}