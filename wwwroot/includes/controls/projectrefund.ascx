<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectrefund.ascx.cs" Inherits="includes_controls_projectrefund" %>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
    .texthidden {overflow:hidden;
    }
</style>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedRefund);
    function pageLoadedRefund() {
        $(".myvalrefund").select2({
            //placeholder: "select",
            allowclear: true
        });

        $('.redreq').click(function () {
            formValidate();
        });
    }
</script>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>

<script>
    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

            function (isConfirm) {
                if (isConfirm) {
                    // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    eval(defaultAction);

                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    return true;
                } else {
                    // swal("Cancelled", "Your imaginary file is safe :)", "error");
                    return false;
                }
            });
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        //HighlightControlToValidate();
        $('#<%=btnUpdateRefund.ClientID %>').click(function () {
            formValidate();
        });
    });
    $(window).load(function () {
        $('#<%=btnUpdateRefund.ClientID %>').click(function () {
            formValidate();
        });
    });
    function doMyActionref() {
        $('#<%=btnUpdateRefund.ClientID %>').click(function () {
            formValidate();
        });
    }
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
    // $("[data-toggle=tooltip]").tooltip();
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>

<asp:UpdatePanel ID="UpdateRefund" runat="server">
    <ContentTemplate>

     
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <div class="salepage">
                        <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Refund</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="col-md-4">
                                                    <div class="form-group wdth230">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Option
                                                            </label>
                                                        </span><span style="width: 200px;">
                                                            <asp:DropDownList ID="ddlOption" runat="server" AppendDataBoundItems="true" Width="100px"
                                                                aria-controls="DataTables_Table_0" CssClass="form-control myval">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="" ControlToValidate="ddlOption"
                                                            InitialValue="" Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>--%>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group dateimgarea wdth230">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Amount
                                                            </label>
                                                        </span><span class="dateimg">
                                                              <asp:TextBox ID="TextBox1" runat="server" MaxLength="7" CssClass="form-control"></asp:TextBox>
                                                            <%--<asp:TextBox ID="txtAmount1" runat="server" MaxLength="7" CssClass="form-control"></asp:TextBox>--%>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBox1"
                                                                ValidationGroup="refund" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$">
                                                            </asp:RegularExpressionValidator>
                                                            
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="" ControlToValidate="TextBox1"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>     
                                                    <div class="form-group dateimgarea wdth230">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Bank Name
                                                            </label>
                                              
                                                              <asp:TextBox ID="txtbankname" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <%--<asp:TextBox ID="txtAmount1" runat="server" MaxLength="7" CssClass="form-control"></asp:TextBox>--%>
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtbankname"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group wdth230">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Account Name
                                                            </label>
                                                        </span><span style="width: 200px;">
                                                            <asp:TextBox ID="txtAccName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtAccName"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group wdth230">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                BSBNo                                               
                                                            </label>
                                                        </span><span style="width: 200px;">
                                                            <asp:TextBox ID="txtBSBNo" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtBSBNo"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group wdth230">

                                                         <span class="name disblock">
                                                            <label class="control-label">
                                                                Account No
                                                            </label>
                                                        </span><span style="width: 200px;">
                                                            <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtAccNo"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        </span>


                                                       
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Notes
                                                            </label>
                                                        </span><span style="width: 200px;">
                                                            <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Columns="8" Rows="9"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtNotes"
                                                                Display="Dynamic" ValidationGroup="refund"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row" id="divAdd" runat="server">
                                                    <div class="col-md-12 textcenterbutton center-text">
                                                        <asp:Button class="btn btn-primary savewhiteicon redreq btnsaveicon" ID="btnUpdateRefund" runat="server" OnClick="btnUpdateRefund_Click"
                                                            Text="Save" CausesValidation="true" ValidationGroup="refund" Visible="true" />

                                                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnsaveRefund" runat="server" OnClick="btnsaveRefund_Click"
                                                            Text="Update" Visible="true" CausesValidation="true" ValidationGroup="refund" />
                                                    </div>
                                                    <br />
                                                </div>
                                                <br />
                                                <div class="row" id="grdInvoice" runat="server">
                                                    <div class="col-md-12  tableblack tableminpadd">
                                                        <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">
                                                            <asp:Panel runat="server" ID="PanGrid">
                                                                <asp:GridView ID="GridView1" DataKeyNames="RefundID" runat="server"
                                                                    PagerStyle-CssClass="gridpagination" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                                    OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="false" OnRowCommand="GridView1_RowCommand" OnRowDeleting="GridView1_RowDeleting">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Option" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <%#Eval("OptionName")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <%#Eval("Amount","{0:0.00}")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Account No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <%#Eval("AccNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Account Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <%#Eval("AccName")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="BSBNo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <%#Eval("BSBNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="120px" HeaderStyle-CssClass="brdrgrayleft" 
                       >
                                                                            <ItemTemplate>
                                                                                 <asp:Label ID="lblFollowUpNote" runat="server" CssClass="texthidden" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Notes")%>'
                                                    Width="150px"><%#Eval("Notes")%></asp:Label>
                                                                               
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Previous Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <%#Eval("ProjectStatus")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px" HeaderStyle-CssClass="brdrgrayleft">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton runat="server" ID="lnkprint" CssClass="btn btn-primary btn-xs Print" CommandArgument='<%#Eval("RefundID") %>' CommandName="print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print">
                                                                                <i class="fa fa-file-pdf-o"></i> Refund
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" Visible='<%#Convert.ToString(Eval("CanChange"))=="1"?false:true%>' CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"> <i class="fa fa-edit"></i> Edit </asp:LinkButton>
                                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" Visible='<%#Convert.ToString(Eval("CanChange"))=="1"?false:true%>' CommandName="Delete" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                                                    CommandArgument='<%#Eval("RefundID")%>' data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                                                <i class="fa fa-trash"></i> Delete
                                                                                </asp:LinkButton>
                                                                                <%--<asp:ImageButton ID="gvbtnDelete" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete This Record?  ');"
                                                                        CausesValidation="false" data-original-title="Delete" data-toggle="tooltip" data-placement="top"
                                                                        ImageUrl="~/admin/images/icons/icon_delet.png"></asp:ImageButton>--%>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="print brdnoneright" />
                                                                            <ItemStyle CssClass="print brdnoneright" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </section>
        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <!--End Danger Modal Templates-->
       </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="GridView1" />
        <asp:PostBackTrigger ControlID="btnUpdateRefund" />
    </Triggers>
</asp:UpdatePanel>
<div runat="server" id="divPrint" style="display: none;"></div>
<script type="text/javascript">
    function printContent() {
        var PageHTML = document.getElementById('<%= (divPrint.ClientID) %>').innerHTML;
        var html = '<html><head>' +

            '</head><body style="background:#ffffff;">' +
            PageHTML +
            '</body></html>';
        var WindowObject = window.open("", "PrintWindow",
            "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.writeln(html);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
        document.getElementById('print_link').style.display = 'block';
    }
</script>



