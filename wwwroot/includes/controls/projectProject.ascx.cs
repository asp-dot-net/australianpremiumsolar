﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_controls_projectProject : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void BindProjectSale()
    {

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            divMtce.Visible = false;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);


            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            //if (Roles.IsUserInRole("PreInstaller"))
            //{
            //    PanAddUpdate.Enabled = true;
            //}
            //if (Roles.IsUserInRole("PostInstaller"))
            //{
            //    PanAddUpdate.Enabled = false;
            //}
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlEmployee.Enabled = false;
                ddlElecDistAppBy.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            BindSaleDropDown();
            if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }

            try
            {

                if (st.SalesType == string.Empty)
                {
                    rblboth.Checked = true;
                }
                if (st.SalesType == "1")
                {
                    rblonlypanel.Checked = true;
                }
                if (st.SalesType == "2")
                {
                    rblonlyinverter.Checked = true;
                }
                if (st.SalesType == "3")
                {
                    rblboth.Checked = true;
                }
            }
            catch
            {
            }

            try
            {
                //Response.Write(st.PanelBrandID);
                //Response.End();
                ddlPanel.SelectedValue = st.PanelBrandID;
            }
            catch { }

            txtPanelBrand.Text = st.PanelBrand;
            //Response.Write(st.PanelBrand);
            //Response.End();
            txtWatts.Text = st.PanelOutput;
            try
            {
                txtRebate.Text = SiteConfiguration.ChangeCurrencyVal((st.RECRebate).ToString());
            }
            catch { }
            txtSTCMult.Text = st.STCMultiplier;
            txtPanelModel.Text = st.PanelModel;
            txtNoOfPanels.Text = st.NumberPanels;
            txtSystemCapacity.Text = st.SystemCapKW;
            //try
            //{            
            ddlInverter.SelectedValue = st.InverterDetailsID;
            //}
            //catch
            //{
            //}
            txtInverterBrand.Text = st.InverterBrand;
            txtSeries.Text = st.InverterSeries;
            try
            {
                ddlInverter2.SelectedValue = st.SecondInverterDetailsID;

                if (st.ThirdInverterDetailsID != "")
                {
                    ddlInverter3.SelectedValue = st.ThirdInverterDetailsID;
                }

            }
            catch
            {
            }
            txtSTCNo.Text = st.STCNumber;

            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                //try
                //{
                txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                //}
                //catch { }
            }

            txtInverterModel.Text = st.InverterModel;
            txtqty.Text = st.inverterqty;
            //Response.Write(st.inverterqty2);
            //Response.End();
            txtqty2.Text = st.inverterqty2;
            txtqty3.Text = st.inverterqty3;


            txtInverterOutput.Text = st.InverterOutput;
            //try
            //{
            ddlElecDist.SelectedValue = st.ElecDistributorID;
            //}
            //catch
            //{
            //}
            txtApprovalRef.Text = st.ElecDistApprovelRef;
            //try
            //{
            ddlElecRetailer.SelectedValue = st.ElecRetailerID;
            //}
            //catch
            //{
            //}
            txtRegPlanNo.Text = st.RegPlanNo;
            //try
            //{
            ddlHouseType.SelectedValue = st.HouseTypeID;
            //}
            //catch
            //{
            //}
            //try
            //{
            ddlRoofType.SelectedValue = st.RoofTypeID;
            //}
            //catch
            //{ }
            //try
            //{
            ddlRoofAngle.SelectedValue = st.RoofAngleID;
            //}
            //catch
            //{ }
            txtLotNum.Text = st.LotNumber;
            txtNMINumber.Text = st.NMINumber;
            txtPeakMeterNo.Text = st.MeterNumber1;
            txtOffPeakMeters.Text = st.MeterNumber2;
            txtMeterPhase.Text = st.MeterPhase;
            ddlestimatevalue.SelectedValue = st.estimatevalue_id;
            txtMeterEstimate.Text = st.Meter_Estimate;
            txtEstimatedPaid.Text = st.Estimate_paid;
            txtestimatepaymentdue.Text = st.Payment_due_Date;
            txtstatusfornetmeter.Text = st.Status_For_Net_Meter;

            try
            {
                ddlmeterupgrade.SelectedValue = st.meterupgrade;
            }
            catch
            {
            }


            chkEnoughMeterSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
            chkIsSystemOffPeak.Checked = Convert.ToBoolean(st.OffPeak);

            if (st.InstallPostCode != "")
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {

                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                        //Response.End();
                        txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                    }
                }
            }
            try
            {
                txtElecDistApplied.Text = Convert.ToDateTime(st.ElecDistApplied).ToShortDateString();
            }
            catch { }
            ddlElecDistApplyMethod.SelectedValue = st.ElecDistApplyMethod;
            txtElecDistApplySentFrom.Text = st.ElecDistApplySentFrom;

            txtgedano.Text = st.GEDANumber;
            txtdistributorapplicationnumber.Text = st.DistributerApplicationNumber;
            txtsubsidypcrno.Text = st.SubsidyPCRNumber;

            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(Request.QueryString["proid"]);
            txtSPAInvoiceNumber.Text = st2.SPAInvoiceNumber;
            try
            {
                txtSPAPaid.Text = Convert.ToDateTime(st2.SPAPaid).ToShortDateString();
            }
            catch { }
            ddlSPAPaidBy.SelectedValue = st2.SPAPaidBy;
            try
            {

                txtSPAPaidAmount.Text = SiteConfiguration.ChangeCurrencyVal(st2.SPAPaidAmount);
            }
            catch { }

            ddlSPAPaidMethod.SelectedValue = st2.SPAPaidMethod;
            if (st.ElecDistributor == "14")
            {
                divSPA.Visible = true;
            }
            else
            {
                divSPA.Visible = false;
            }
            if (ddlElecDist.SelectedValue == "19" && st.InstallState == "TAS")
            {
                if (st.SurveyCerti != string.Empty)
                {
                    if (st.SurveyCerti == "1")
                    {
                        rblSurveyCerti.SelectedValue = "1";
                    }
                    if (st.SurveyCerti == "2")
                    {
                        rblSurveyCerti.SelectedValue = "2";
                    }
                    divSurveyCerti.Visible = true;
                }
                else
                {
                    divSurveyCerti.Visible = false;
                }

                if (st.CertiApprove != string.Empty)
                {
                    if (st.CertiApprove == "1")
                    {
                        rblCertiApprove.SelectedValue = "1";
                    }
                    if (st.CertiApprove == "2")
                    {
                        rblCertiApprove.SelectedValue = "2";
                    }
                    divCertiApprove.Visible = true;
                }
                else
                {
                    divCertiApprove.Visible = false;
                }
            }

            if (st.ElecDistAppBy == string.Empty)
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                try
                {
                    ddlElecDistAppBy.SelectedValue = stEmp.EmployeeID;
                }
                catch { }
            }
            else
            {
                try
                {
                    ddlElecDistAppBy.SelectedValue = st.ElecDistAppBy;
                }
                catch { }
            }
            try
            {
                txtElecDistAppDate.Text = Convert.ToDateTime(st.ElecDistAppDate).ToShortDateString();
            }
            catch { }

            txtVicAppRefNo.Text = st.VicAppReference;
            txtVicNote.Text = st.VicRebateNote;
            ddlvicrebate.SelectedValue = st.VicRebate;
            try
            {
                txtVicdate.Text = Convert.ToDateTime(st.VicDate).ToShortDateString();
            }
            catch { }

            if (Roles.IsUserInRole("Installation Manager"))
            {
                if (st.ProjectStatusID == "3")
                {
                    if (st.ElecDistributorID == "12")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                    else if (st.ElecDistributorID == "13")
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                        txtLotNum.Enabled = false;
                        txtRegPlanNo.Enabled = false;
                    }
                    else
                    {
                        txtApprovalRef.Enabled = false;
                        txtNMINumber.Enabled = false;
                    }
                }
            }
            if (st.ProjectTypeID == "8")
            {
                divMtce.Visible = true;
                divPanelDetail.Visible = true;
                divInverterDetail.Visible = true;
                divSitedetails.Visible = false;
                divDistAppDetail.Visible = false;
                divMeterDetails.Visible = false;
            }
            else
            {
                divMtce.Visible = false;
            }
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
            {
                //Panel1.Enabled = false;
                Panel1.Attributes.Add("style", "pointer-events: none;");
            }
            else
            {
                //Panel1.Enabled = true;
                //Panel1.Attributes.Add("", "");
            }
        }
    }
    public void BindSaleDropDown()
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);

        //String Inverter1ID;

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";

        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlPanel.Items.Clear();
        ddlPanel.Items.Add(item1);

        DataTable dtPanel = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            //Response.Write(State);
            //Response.End();
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
        }
        else
        {

            dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
        }

        ddlPanel.DataSource = dtPanel;
        ddlPanel.DataValueField = "StockItemID";
        ddlPanel.DataMember = "StockItem";
        ddlPanel.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string PanelID = stProj.PanelBrandID;
            if (!String.IsNullOrEmpty(PanelID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(PanelID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem P1 = new ListItem(); // Example List
                        P1.Text = dt.Rows[0]["StockItem"].ToString();
                        P1.Value = PanelID;
                        ddlPanel.Items.Add(P1);
                    }
                }
            }
        }
        ddlPanel.DataBind();


        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlInverter.Items.Clear();
        ddlInverter.Items.Add(item2);

        DataTable dtInverter = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
        }
        else
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlInverter.DataSource = dtInverter;
        ddlInverter.DataValueField = "StockItemID";
        ddlInverter.DataMember = "StockItem";
        ddlInverter.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter1ID = stProj.InverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter1ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter1ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter1ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I1 = new ListItem(); // Example List
                        I1.Text = dt.Rows[0]["StockItem"].ToString();
                        I1.Value = Inverter1ID;
                        ddlInverter.Items.Add(I1);
                    }
                }
            }
        }
        ddlInverter.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlInverter2.Items.Clear();
        ddlInverter2.Items.Add(item3);

        ddlInverter2.DataSource = dtInverter;
        ddlInverter2.DataValueField = "StockItemID";
        ddlInverter2.DataMember = "StockItem";
        ddlInverter2.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter2ID = stProj.SecondInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter2ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter2ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter2ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I2 = new ListItem(); // Example List
                        I2.Text = dt.Rows[0]["StockItem"].ToString();
                        I2.Value = Inverter2ID;
                        ddlInverter2.Items.Add(I2);
                    }
                }
            }
        }
        ddlInverter2.DataBind();


        ListItem item9 = new ListItem();
        item9.Text = "Select";
        item9.Value = "";
        ddlInverter3.Items.Clear();
        ddlInverter3.Items.Add(item9);

        ddlInverter3.DataSource = dtInverter;
        ddlInverter3.DataValueField = "StockItemID";
        ddlInverter3.DataMember = "StockItem";
        ddlInverter3.DataTextField = "StockItem";
        if (Roles.IsUserInRole("SalesRep"))
        {
            string Inverter3ID = stProj.ThirdInverterDetailsID;
            if (!String.IsNullOrEmpty(Inverter3ID))
            {
                //SttblStockItems ststockid = ClstblStockItems.tblStockItems_SelectByStockItemID(Inverter3ID);
                DataTable dt = ClstblStockItems.tblStockItems_SelectByStockItemIDWithFalseSalesTag(Inverter3ID, State);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["SalesTag"].ToString() == "False")
                    {
                        ListItem I3 = new ListItem(); // Example List
                        I3.Text = dt.Rows[0]["StockItem"].ToString();
                        I3.Value = Inverter3ID;
                        ddlInverter3.Items.Add(I3);
                    }
                }
            }
        }
        ddlInverter3.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlHouseType.Items.Clear();
        ddlHouseType.Items.Add(item4);

        ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
        ddlHouseType.DataValueField = "HouseTypeID";
        ddlHouseType.DataMember = "HouseType";
        ddlHouseType.DataTextField = "HouseType";
        ddlHouseType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlRoofType.Items.Clear();
        ddlRoofType.Items.Add(item5);

        ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        ddlRoofType.DataValueField = "RoofTypeID";
        ddlRoofType.DataMember = "RoofType";
        ddlRoofType.DataTextField = "RoofType";
        ddlRoofType.DataBind();



        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlRoofAngle.Items.Clear();
        ddlRoofAngle.Items.Add(item6);

        ddlRoofAngle.DataSource = ClstblRoofAngles.tblRoofAngles_SelectASC();
        ddlRoofAngle.DataValueField = "RoofAngleID";
        ddlRoofAngle.DataMember = "RoofAngle";
        ddlRoofAngle.DataTextField = "RoofAngle";
        ddlRoofAngle.DataBind();

        ListItem itemestimatevalue = new ListItem();
        itemestimatevalue.Text = "Select";
        itemestimatevalue.Value = "";
        ddlestimatevalue.Items.Clear();
        ddlestimatevalue.Items.Add(itemestimatevalue);

        ddlestimatevalue.DataSource = ClsProjectSale.tbl_EstimateValue_Select();
        ddlestimatevalue.DataValueField = "id";
        ddlestimatevalue.DataMember = "EstimateValue";
        ddlestimatevalue.DataTextField = "EstimateValue";
        ddlestimatevalue.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        System.Web.UI.WebControls.ListItem itemPaid = new System.Web.UI.WebControls.ListItem();
        itemPaid.Text = "Select";
        itemPaid.Value = "";
        ddlSPAPaidBy.Items.Clear();
        ddlSPAPaidBy.Items.Add(itemPaid);

        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                if (dr["SalesTeamID"].ToString() != string.Empty)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
            }
            if (SalesTeam != string.Empty)
            {
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
        }
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {

            if (SalesTeam != string.Empty)
            {
                ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSPAPaidBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlSPAPaidBy.DataValueField = "EmployeeID";
        ddlSPAPaidBy.DataMember = "fullname";
        ddlSPAPaidBy.DataTextField = "fullname";
        ddlSPAPaidBy.DataBind();

        System.Web.UI.WebControls.ListItem itemMethod = new System.Web.UI.WebControls.ListItem();
        itemMethod.Text = "Select";
        itemMethod.Value = "";
        ddlSPAPaidMethod.Items.Clear();
        ddlSPAPaidMethod.Items.Add(itemMethod);

        ddlSPAPaidMethod.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
        ddlSPAPaidMethod.DataValueField = "FPTransTypeID";
        ddlSPAPaidMethod.DataMember = "FPTransType";
        ddlSPAPaidMethod.DataTextField = "FPTransType";
        ddlSPAPaidMethod.DataBind();

        System.Web.UI.WebControls.ListItem item11 = new System.Web.UI.WebControls.ListItem();
        item11.Text = "Select";
        item11.Value = "";
        ddlElecDistApplyMethod.Items.Clear();
        ddlElecDistApplyMethod.Items.Add(item11);

        ddlElecDistApplyMethod.DataSource = ClsProjectSale.tblElecDistApplyMethod_SelectASC();
        ddlElecDistApplyMethod.DataValueField = "ElecDistApplyMethodID";
        ddlElecDistApplyMethod.DataMember = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataTextField = "ElecDistApplyMethod";
        ddlElecDistApplyMethod.DataBind();


        ddlpickthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlpickthrough.DataValueField = "PickUpID";
        ddlpickthrough.DataMember = "PickUpID";
        ddlpickthrough.DataTextField = "PickUp";
        ddlpickthrough.DataBind();

        ddlsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlsentthrough.DataValueField = "PickUpID";
        ddlsentthrough.DataMember = "PickUpID";
        ddlsentthrough.DataTextField = "PickUp";
        ddlsentthrough.DataBind();

        ddlhsentthrough.DataSource = ClstblPickUp.tblPickUp_SelectAsc();
        ddlhsentthrough.DataValueField = "PickUpID";
        ddlhsentthrough.DataMember = "PickUpID";
        ddlhsentthrough.DataTextField = "PickUp";
        ddlhsentthrough.DataBind();


        System.Web.UI.WebControls.ListItem item12 = new System.Web.UI.WebControls.ListItem();
        item12.Text = "Select";
        item12.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item12);

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
            if (SalesTeam != string.Empty)
            {
                ddlEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataMember = "fullname";
            ddlEmployee.DataTextField = "fullname";
            ddlEmployee.DataBind();


            ddlEmployee.SelectedValue = stEmp.EmployeeID;
        }
        catch { }


        System.Web.UI.WebControls.ListItem item13 = new System.Web.UI.WebControls.ListItem();
        item13.Text = "Select";
        item13.Value = "";
        ddlElecDistAppBy.Items.Clear();
        ddlElecDistAppBy.Items.Add(item13);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlElecDistAppBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        try
        {
            ddlElecDistAppBy.DataValueField = "EmployeeID";
            ddlElecDistAppBy.DataMember = "fullname";
            ddlElecDistAppBy.DataTextField = "fullname";
            ddlElecDistAppBy.DataBind();
        }
        catch
        { }


        string EleDist = stProj.StreetState;
        //if (stProj.InstallState == "NSW")
        //{
        //    EleDist = "select * from tblElecDistributor where NSW=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "SA")
        //{
        //    EleDist = "select * from tblElecDistributor where SA=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "QLD")
        //{
        //    EleDist = "select * from tblElecDistributor where QLD=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "VIC")
        //{
        //    EleDist = "select * from tblElecDistributor where VIC=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "WA")
        //{
        //    EleDist = "select * from tblElecDistributor where WA=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "ACT")
        //{
        //    EleDist = "select * from tblElecDistributor where ACT=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "TAS")
        //{
        //    EleDist = "select * from tblElecDistributor where TAS=1 order by ElecDistributor asc";
        //}
        //if (stProj.InstallState == "NT")
        //{
        //    EleDist = "select * from tblElecDistributor where NT=1 order by ElecDistributor asc";
        //}

        //DataTable dtElecDist = ClsProjectSale(EleDist);
        //if (dtElecDist.Rows.Count > 0)
        //{
        ListItem item7 = new ListItem();
        item7.Text = "Select";
        item7.Value = "";
        ddlElecDist.Items.Clear();
        ddlElecDist.Items.Add(item7);

        //ddlElecDist.DataSource = dtElecDist;
        ddlElecDist.DataSource = ClsProjectSale.ap_form_element_execute(EleDist);
        ddlElecDist.DataValueField = "ElecDistributorID";
        ddlElecDist.DataMember = "ElecDistributor";
        ddlElecDist.DataTextField = "ElecDistributor";
        ddlElecDist.DataBind();
        // }

        string EleRet = stProj.StreetState;
        //if (stProj.InstallState == "NSW")
        //{
        //    EleRet = "select * from tblElecRetailer where NSW=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "SA")
        //{
        //    EleRet = "select * from tblElecRetailer where SA=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "QLD")
        //{
        //    EleRet = "select * from tblElecRetailer where QLD=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "VIC")
        //{
        //    EleRet = "select * from tblElecRetailer where VIC=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "WA")
        //{
        //    EleRet = "select * from tblElecRetailer where WA=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "ACT")
        //{
        //    EleRet = "select * from tblElecRetailer where ACT=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "TAS")
        //{
        //    EleRet = "select * from tblElecRetailer where TAS=1 order by ElecRetailer asc";
        //}
        //if (stProj.InstallState == "NT")
        //{
        //    EleRet = "select * from tblElecRetailer where NT=1 order by ElecRetailer asc";
        //}

        //DataTable dtElecRet = ClsProjectSale.ap_form_element_execute(EleRet);
        //if (dtElecRet.Rows.Count > 0)
        //{
        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlElecRetailer.Items.Clear();
        ddlElecRetailer.Items.Add(item8);

        ddlElecRetailer.DataSource = ClsProjectSale.ap_form_element_executeEleRetailer(EleRet); ;
        ddlElecRetailer.DataValueField = "ElecRetailerID";
        ddlElecRetailer.DataMember = "ElecRetailer";
        ddlElecRetailer.DataTextField = "ElecRetailer";
        ddlElecRetailer.DataBind();
        //}

        ListItem item21 = new ListItem();
        item21.Text = "Select";
        item21.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item21);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();
        try
        {
            ddlProjectMtceReasonID.SelectedValue = "2";
        }
        catch { }


        ListItem item22 = new ListItem();
        item22.Text = "Select";
        item22.Value = "";
        ddlProjectMtceReasonSubID.Items.Clear();
        ddlProjectMtceReasonSubID.Items.Add(item22);

        ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataBind();

        ddlProjectMtceReasonSubID.SelectedValue = "2";

        ListItem item25 = new ListItem();
        item25.Text = "Select";
        item25.Value = "";
        ddlProjectMtceCallID.Items.Clear();
        ddlProjectMtceCallID.Items.Add(item25);

        ddlProjectMtceCallID.DataSource = ClstblProjects.tblProjectMtceCall_SelectASC();
        ddlProjectMtceCallID.DataValueField = "ProjectMtceCallID";
        ddlProjectMtceCallID.DataMember = "ProjectMtceCall";
        ddlProjectMtceCallID.DataTextField = "ProjectMtceCall";
        ddlProjectMtceCallID.DataBind();

        ddlProjectMtceCallID.SelectedValue = "4";

        ListItem item26 = new ListItem();
        item26.Text = "Select";
        item26.Value = "";
        ddlProjectMtceStatusID.Items.Clear();
        ddlProjectMtceStatusID.Items.Add(item26);

        if (stProj.ProjectTypeID != "8")
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectTop4();
        }
        else
        {
            ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectLast4();
        }
        ddlProjectMtceStatusID.DataValueField = "ProjectMtceStatusID";
        ddlProjectMtceStatusID.DataMember = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataTextField = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataBind();

        //ddlProjectMtceStatusID.SelectedValue = "2";
    }

}