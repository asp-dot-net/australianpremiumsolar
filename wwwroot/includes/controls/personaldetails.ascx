﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="personaldetails.ascx.cs"
    Inherits="includes_personaldetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

 <div class="form-horizontal">

                        <div class="form-group ">
                                    <asp:Label ID="Label14" runat="server" class="col-sm-2 control-label">
                                                Emp&nbsp;Type&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmpType" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">Employee</asp:ListItem>
                                            <asp:ListItem Value="2">Dealer</asp:ListItem>
                                            <asp:ListItem Value="3">Contractor </asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmpType" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                First Name</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="txtEmpFirst"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <asp:Label ID="Label34" runat="server" class="col-sm-2 control-label">
                                                Middle Name</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtEmpMiddle" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" ControlToValidate="txtEmpMiddle"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label">
                                                Last Name</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ControlToValidate="txtEmpLast"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label">
                                                Email</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpEmail" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" ControlToValidate="txtEmpEmail"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtEmpEmail"
                                            Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" class="col-sm-2 control-label">
                                                Mobile</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpMobile" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>


                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpMobile"
                                            Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" ControlToValidate="txtEmpMobile"
                                                            Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label">
                                                Phone</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" ControlToValidate="txtEmpPhone"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                            ControlToValidate="txtEmpPhone" Display="Dynamic" ErrorMessage="Number Only"
                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                Date&nbsp;Hired</asp:Label>
                                    <div class="col-sm-6">
                                        <div class="input-group date datetimepicker1 col-sm-6" style="width: 255px">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtHireDate" runat="server" class="form-controlpagel">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <asp:Label ID="lblaadharno" runat="server" class="col-sm-2 control-label">
                                                Aadhar Card No</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtaadharno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" ControlToValidate="txtaadharno"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" class="col-sm-2 control-label">
                                                Pan No</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtpanno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" ControlToValidate="txtpanno"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                

                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" class="col-sm-2 control-label">
                                                Gst No</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtgstno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" ControlToValidate="txtgstno"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                Source By</asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsourceby" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" ControlToValidate="txtsourceby"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                 <div class="row">
                                                <div class="marginbtm15 col-md-4"  id="divStreetno" runat="server">
                                                    <asp:Label ID="Label12" runat="server">Address 1</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox"  ></asp:TextBox>
                                                         <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 spicaldivin col-md-4 streatfield" id="divstname" runat="server">
                                                    <asp:Label ID="Label2" CssClass="marbtmzero" runat="server">Address 2</asp:Label>

                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control"  ></asp:TextBox>

                                                      
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />



                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                            ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                            ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <div id="Divvalidstreetname" style="display: none">
                                                            <i class="fa fa-check"></i>Address is valid.
                                                        </div>
                                                        <div id="DivInvalidstreetname" style="display: none">
                                                            <i class="fa fa-close"></i>Address is invalid.
                                                            </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                      <div class="marginbtm15 col-md-4" id="Div1" runat="server">
                                                    <asp:Label ID="Label4" runat="server">Taluka</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txttaluka" runat="server" MaxLength="50" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <%--<div class="marginbtm15 col-md-4" visible="false" id="divsttype" runat="server">
                                                    <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                    <div id="Div8" class="drpValidate" runat="server">
                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                                    </div>
                                                </div>--%>
                                            </div>
                              <div class="row">
                                             <div class="marginbtm15 col-md-4" id="Div2" runat="server">
                                                    <asp:Label ID="lblDistrict" runat="server">District</asp:Label>
                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="txtDistrict" runat="server" MaxLength="50" 
                                                            class="form-control modaltextbox"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="marginbtm15 col-md-4" id="seq" runat="server">
                                                    <asp:Label ID="lblMobile" runat="server">City</asp:Label>
                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="ddlStreetCity" runat="server" MaxLength="50" 
                                                            class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                                        <cc1:AutoCompleteExtender ID="AutoCompletestreet" CompletionListCssClass="autocompletedrop" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div9" runat="server">
                                                    <asp:Label ID="Label15" runat="server">State</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetState" runat="server" MaxLength="50" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div10" runat="server">
                                                    <asp:Label ID="Label16" runat="server">Pin Code</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtpincode" runat="server" name="address" class="form-control modaltextbox"  ></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <asp:Button CssClass="btn redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Next" />
                                      
                                    </div>
                                </div>
                            </div>
