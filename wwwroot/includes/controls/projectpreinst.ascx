<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectpreinst.ascx.cs"
    Inherits="includes_controls_projectpreinst" %>
<script runat="server">

    protected void btnAddRowAgain_Click(object sender, EventArgs e)
    {

    }
</script>

<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }
    /*.selected_row {
            background-color: #A1DCF2 !important;
        }

        .modal-body .select2-container {
            z-index: 999999999999999 !important;
        }*/
</style>

<asp:UpdatePanel ID="updatestockpanel" runat="server">
    <ContentTemplate>
        <script>
            function openModal() {
                $('[id*=modal_danger]').modal('show');
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
<script src="../../assets/js/jquery.min.js"></script>
<script>

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedpreinst);

    function Uploadfile12() {
        debugger;
        var uploadfiles = $('input[type="file"]').get(1);
        var fileUpload = document.getElementById("<%=FileUpload2.ClientID %>");
        $('#<%=hdnFileName.ClientID %>').val(fileUpload.files[0].name);
        var fromdata = new FormData();
        fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
        var choice = {};
        choice.url = "Handler.ashx";
        choice.type = "POST";
        choice.data = fromdata;
        choice.contentType = false;
        choice.processData = false;
        choice.success = function (result) {
        };
        choice.error = function (err) {
        };
        $.ajax(choice);
    }

    function pageLoadedpreinst() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
        $(".myval1").select2({
            minimumResultsForSearch: -1
        });

        $('#<%=btnUpdatePreInst.ClientID %>').click(function (e) {
            formValidate();
        });
        $('#<%=Button1.ClientID %>').click(function (e) {
            // alert("1");
            formValidate();
        });

        $('#<%=btnSubmit.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
            <%--    $('#<%=picklistdiv.ClientID %>').hide();
                //$('.loading-container').css('display', 'block');
                if (count == 1) {
                    MyfunSuccess();
                    count++;
                }
                //alert("h1");--%>
            }
            else {
                formValidate();
                //alert("h2");
            }

        });

        {
            // NOTE: this block is to get scroll dynamically for more than 3 record as row's height won't be
            //be of fixed value.
            var row_count1 = $("#tblPL").find('tr').length;
            var table_height = 0;
            //alert(table_height);
            for (var i = 0; i <= 3; i++) {
                table_height = table_height + $("#tblPL tr").eq(i).height();
            }
            if (row_count1 > 3) {
                $('#<%=divpl.ClientID %>').attr("style", "overflow-y: scroll; height: " + table_height + "px !important;");
            }
            else {
                $('#<%=divpl.ClientID %>').attr("style", "");
            }
        }

        $('#<%=btnDownload.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
                $('#<%=picklistdiv.ClientID %>').hide();
                $('#<%=btnPickList.ClientID%>').removeAttr("disabled");
                //alert("h1");
            }
            else {
                formValidate();
                //alert("h2");
            }
        });

    }
    function getParsedAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'address',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data != null) {
                    document.getElementById("<%= txtInstallAddress.ClientID %>").value = data.StreetLine;
                    document.getElementById("<%= txtStreetPostCode.ClientID %>").value = data.Postcode;
                    document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                    document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                    document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                    document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                    document.getElementById("<%= ddlStreetCity.ClientID %>").value = data.Suburb;
                    document.getElementById("<%= ddlStreetState.ClientID %>").value = data.State;
                    document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                    document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                }
                validateProjectAddress();
            }
        });
    }
    function validateProjectAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "json",
            data: {
                s: 'validate',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data == true) {
                    document.getElementById("validaddressid").style.display = "block";
                    document.getElementById("invalidaddressid").style.display = "none";

                    document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                }
                else {
                    document.getElementById("validaddressid").style.display = "none";
                    document.getElementById("invalidaddressid").style.display = "block";
                    document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                }
            }
        });
    }
    function ChkFun1(source, args) {
        validateProjectAddress();
        var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

        if (elem == '1') {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }

    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }

</script>



<script type="text/javascript">
    var count = 1;

    $(document).ready(function () {
        //HighlightControlToValidate();
        //$('.redreq1').click(function () {
        //    form_Validate();
        //});
        // $('.loading-container').css('display', 'none');
    });


    function doMyAction() {

        $('#<%=btnUpdatePreInst.ClientID %>').click(function (e) {
            formValidate();
        });

        $('#<%=Button1.ClientID %>').click(function (e) {
            // alert("2");
            formValidate();
        });

        $('#<%=btnSubmit.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
              <%--  $('#<%=picklistdiv.ClientID %>').hide();
                //$('.loading-container').css('display', 'block');
                if (count == 1) {
                    MyfunSuccess();
                    count++;
                }
                //alert("h1");--%>
            }
            else {
                formValidate();
                //alert("h2");
            }

        });

        $('#<%=btnDownload.ClientID %>').click(function (e) {
            var reason = document.getElementById("<%=txtreason.ClientID %>").value;
            var note = document.getElementById("<%=txtnote.ClientID %>").value;
            if (reason != "" && note != "") {
                $('#<%=picklistdiv.ClientID %>').hide();
                $('#<%=btnPickList.ClientID%>').removeAttr("disabled");
                //alert("h1");
            }
            else {
                formValidate();
                //alert("h2");
            }
        });

    }


    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#FF5F5F");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }

</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea stcpage popuoformbox">
                <div class="contactsarea boxPadding formGrid">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litmessage" runat="server" Text="Transaction Failed."></asp:Literal></strong>
                        </div>
                        <%--<div class="alert alert-danger" id="DivInstalldetails" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please enter installation plan data.</strong>
                        </div>--%>
                        <div class="alert alert-danger" id="DivDocNotVerified" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your documents are not verified.</strong>
                        </div>
                        <div class="alert alert-danger" id="divActive" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong>
                        </div>
                        <div class="alert alert-danger" id="divVicAppRefRebate" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please enter Vic Rebate App Ref.</strong>
                        </div>
                        <div class="alert alert-success" id="divformbaymsg" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;<asp:Label runat="server" ID="lblformbaymsg"></asp:Label></strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="boxPadding">
                            <div class="box dispnone">
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label class="control-label">&nbsp;</label>
                                            <div class="checkbox-info checkBox checkBoxList" style="margin-top: 10px;">
                                                <asp:Label ID="lbltesting" runat="server">Elec Dist OK</asp:Label>
                                                <asp:CheckBox ID="chkElecDistOK" runat="server" AutoPostBack="true" />
                                                <%--<asp:CheckBox ID="chkElecDistOK" runat="server" OnCheckedChanged="chkElecDistOK_CheckedChanged" AutoPostBack="true" />--%>
                                                <label for="<%=chkElecDistOK.ClientID %>" class="control-label"></label>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <label class="control-label">&nbsp;</label>
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="" style="margin-top: 10px;">
                                                <tbody>
                                                    <tr>
                                                        <td width="5" class="control-label">AM </td>
                                                        <td width="10">
                                                            <div class="checkbox-info checkBox checkBoxList" style="margin-top: -12px;">
                                                                <asp:CheckBox ID="rblAM1" runat="server" />
                                                                <label for="<%=rblAM1.ClientID %>">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td width="50">
                                                            <div class="checkbox-info checkBox " style="margin-top: -12px;">
                                                                <asp:CheckBox ID="rblAM2" runat="server" />
                                                                <label for="<%=rblAM2.ClientID %>">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td width="5" class="control-label">PM </td>
                                                        <td width="10">
                                                            <div class="checkbox-info checkBox" style="margin-top: -12px;">
                                                                <asp:CheckBox ID="rblPM1" runat="server" />
                                                                <label for="<%=rblPM1.ClientID %>">
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td width="10">
                                                            <div class="checkbox-info checkBox" style="margin-top: -12px;">
                                                                <asp:CheckBox ID="rblPM2" runat="server" />
                                                                <label for="<%=rblPM2.ClientID %>">
                                                                </label>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <%--      <span class="col-md-12">PM
                                                <asp:CheckBox ID="rblPM1" runat="server" />
                                                <label for="<%=rblPM1.ClientID %>">
                                                    <span></span>
                                                </label>
                                                <asp:CheckBox ID="rblPM2" runat="server" />
                                                <label for="<%=rblPM2.ClientID %>">
                                                    <span></span>
                                                </label>
                                            </span>--%>
                                        </div>

                                        <div class="col-sm-3">

                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 20%;">
                                                            <label class="control-label">Days</label>
                                                            <span>
                                                                <asp:TextBox ID="txtInstallDays" runat="server" Width="50px" Text="1" MaxLength="10" CssClass="form-control wd50" AutoPostBack="true" OnTextChanged="txtInstallDays_TextChanged"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtInstallDays"
                                                                    ValidationGroup="preinst" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                <%--   <asp:RangeValidator ID="rangevalEditQty" runat="server" ErrorMessage="Please enter valid day"
                                                                ValidationGroup="preinst" ControlToValidate="txtInstallDays" Type="Integer" MinimumValue="1"
                                                                MaximumValue="356" Display="Dynamic"></asp:RangeValidator>--%>
                                                            </span>
                                                        </td>
                                                        <td runat="server">
                                                            <div class="checkbox-info checkBox">
                                                                <label class="control-label">Quick Form</label>
                                                                <asp:CheckBox ID="chkquickform" runat="server" />
                                                                <label for="<%=chkquickform.ClientID %>"></label>
                                                            </div>
                                                        </td>

                                                        <td runat="server" visible="false">
                                                            <div style="width: 100%; display: inline-block; margin-top: 28px">
                                                                <div class=" checkbox-info checkbox">
                                                                    <span class="fistname">
                                                                        <label for="<%=cbkIsFormBay.ClientID %>" class="control-label">
                                                                            <asp:CheckBox ID="cbkIsFormBay" runat="server" />
                                                                            <span class="text">IsFormBay   </span>
                                                                        </label>
                                                                    </span>
                                                                </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-1">
                                            <label class="control-label">&nbsp;</label>
                                            <asp:LinkButton ID="btnRemoveInst" Style="width: 160px" runat="server" OnClick="btnRemoveInst_Click" OnClientClick="yes" CausesValidation="false" class="btn btn-labeled btn-danger">
                                            <i class="btn-label fa fa-trash"></i>Remove Install</asp:LinkButton>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label">&nbsp;</label>
                                            <div class="form-group" id="divElecDistApproved" runat="server" visible="false">
                                                <span class="name">
                                                    <asp:Label ID="Label23" runat="server" class="control-label">
                                                    Elec Dist Approved</asp:Label></span>
                                                <div class="input-group date datetimepicker1">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtElecDistApproved" runat="server" class="form-control" Width="125px">
                                                    </asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage=""
                                                    Visible="false" ValidationGroup="preinst" ControlToValidate="txtElecDistApproved"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <%--   <asp:Button ID="btnRemoveInst" runat="server" OnClick="btnRemoveInst_Click" Text="Remove Install" CssClass="btn btn-info" Visible="false" />--%>

                                            <div class="form-group ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="boxHeading">
                                    <h4 class="formHeading">Installation Address</h4>
                                </div>
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="control-label">
                                                Install Site
                                            </label>
                                            <div class="" runat="server" visible="false">
                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="200" CssClass="InstallAddress  form-control" onblur="getParsedAddress();"></asp:TextBox>
                                                <asp:CustomValidator ID="custompreadd" runat="server"
                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="preinst" Enabled="false"
                                                    ClientValidationFunction="ChkFun1"></asp:CustomValidator>
                                            </div>
                                            <div class="">
                                                <asp:HiddenField ID="hndaddress" runat="server" />
                                                <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" CssClass="InstallAddress  form-control"></asp:TextBox>
                                            </div>
                                            <asp:HiddenField ID="hndstreetno" runat="server" />
                                            <asp:HiddenField ID="hndstreetname" runat="server" />
                                            <asp:HiddenField ID="hndstreettype" runat="server" />
                                            <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                            <asp:HiddenField ID="hndunittype" runat="server" />
                                            <asp:HiddenField ID="hndunitno" runat="server" />
                                            <div id="validaddressid" style="display: none">
                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                            </div>
                                            <div id="invalidaddressid" style="display: none">
                                                <img src="../../../images/x.png" alt="cross">
                                                Address is invalid.
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="" id="divStreetno" runat="server">
                                                <asp:Label ID="Label12" runat="server" class="control-label">Address 1</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtAddress1" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true"
                                                        OnTextChanged="txtformbayStreetNo_TextChanged" Visible="false"></asp:TextBox>
                                                    <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="spicaldivin streatfield" id="divstname" runat="server" style="display: none">
                                                <asp:Label ID="Label3" runat="server" class="control-label">Street Name</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                    <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                        ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                        ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="spicaldivin streatfield" id="div7" runat="server">
                                                <asp:Label ID="Label6" runat="server" class="control-label">Address 2</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:TextBox ID="txtAddress2" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>

                                                    <%-- <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                    ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                    ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 dispnone">
                                            <%--<asp:TextBox ID="txtformbayUnitNo" runat="server" placeholder="Unit No" CssClass="form-control" OnTextChanged="txtformbayUnitNo_TextChanged" AutoPostBack="true"></asp:TextBox>--%>
                                        </div>
                                        <div class="col-md-3" id="divNearby" runat="server" style="display: none">
                                            <asp:Label ID="Label10" runat="server" class="control-label">Street State</asp:Label>
                                            <div>
                                                <asp:TextBox ID="txtformbayNearby" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" Visible="false" OnTextChanged="txtformbayNearby_TextChanged"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group col-md-3" style="display: none">
                                            <div class="" id="div5" runat="server">
                                                <asp:Label ID="Label4" runat="server" class="control-label">Street City</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtStreetCity" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                    <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <div class="" id="div6" runat="server">
                                                <asp:Label ID="Label5" runat="server" class="control-label">Street State</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtState1" runat="server" name="address" class="form-control modaltextbox" Visible="false"></asp:TextBox>
                                                    <asp:DropDownList ID="ddlStreetState" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystate_SelectedIndexChanged">
                                                        <asp:ListItem Value="">Street State</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 form-group" id="Div24" runat="server">
                                            <asp:Label ID="Label35" runat="server">District</asp:Label>
                                            <div class="autocompletedropdown">
                                                <asp:DropDownList ID="ddlDistrict" runat="server" AppendDataBoundItems="true" CssClass="myvalproject" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Select District</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-sm-3  form-group" id="Div25" runat="server">
                                                <asp:Label ID="Label36" runat="server">Taluka</asp:Label>
                                                      <div>
                                                            <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox" ></asp:TextBox>
                                                        </div>
                                                 <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    
                                            </div>
                                            <div class="col-sm-3 form-group" id="Div11" runat="server">
                                                <asp:Label ID="Label7" runat="server">Street City</asp:Label>
                                                    <div>
                                                        <asp:TextBox runat="server" ID="ddlStreetCity" Enabled="true" class="form-control modaltextbox" ></asp:TextBox>
                                                    </div>
                                                   <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllCity"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                

                                            </div>
                                        <div class="col-md-6" visible="false" id="divsttype" runat="server" style="display: none">
                                            <div class="marginbtm15 col-md-4">
                                                <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                <div id="Div8" class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp">

                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div id="Divvalidstreetname1" style="display: none">
                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                            </div>
                                            <div id="DivInvalidstreetname1" style="display: none">
                                                <img src="../../../images/x.png" alt="cross">
                                                Address is invalid.
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="seq" runat="server" style="display: none">
                                            <asp:Label ID="lblMobile" runat="server" class="control-label">Street City</asp:Label>
                                            <div class="autocompletedropdown">
                                                <asp:DropDownList ID="ddlStreetCity1" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnTextChanged="ddlformbaycity_SelectedIndexChanged">
                                                    <asp:ListItem Value=""> City</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlStreetCity1" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="Div9" runat="server" style="display: none">
                                            <asp:Label ID="Label15" runat="server" class="control-label">Street State</asp:Label>
                                            <div>
                                                <asp:DropDownList ID="ddlStreetState1" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystate_SelectedIndexChanged">
                                                    <asp:ListItem Value=""> State</asp:ListItem>
                                                </asp:DropDownList>
                                                <%-- <asp:TextBox ID="txtStreetState" runat="server" MaxLength="50" Enabled="true" class="form-control modaltextbox"></asp:TextBox>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlStreetState1" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="drpValidate">
                                                <div id="Div10" runat="server">
                                                    <asp:Label ID="Label16" runat="server" class="control-label"> Post Code</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="true" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="ddlformbaypostalcode_SelectedIndexChanged"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtStreetPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 textRight dispnone">
                                            <%--<asp:ImageButton ID="btnCustAddress" runat="server"
                                                OnClick="btnCustAddress_Click" ImageUrl="~/images/user_cust_address_btn1.png" OnClientClick="yes"
                                                CausesValidation="false" />--%>

                                            <asp:LinkButton ID="btnCustAddress" runat="server" OnClientClick="yes" CausesValidation="false" class="btn btn-labeled btn-primary">
                                            <i class="btn-label fa fa-street-view" style="margin-right:5px;"></i>Use Cust. Add</asp:LinkButton>

                                            <%--<asp:ImageButton ID="btnUpdateAddress" runat="server"
                                            OnClick="btnUpdateAddress_Click" OnClientClick="yes" ImageUrl="~/images/icon_updateaddress.png" />--%>

                                            <asp:LinkButton ID="btnUpdateAddress" runat="server" OnClientClick="yes" OnClick="btnUpdateAddress_Click" CausesValidation="false" class="btn btn-labeled btn-primary"><i class="btn-label fa fa-map-marker" style="margin-right:5px;"></i>Update Address
                                            </asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="box" id="InstallDETAILS">
                                <div class="boxHeading">
                                    <div class="flexRow">
                                        <h4 class="formHeading">Installation Start Detail</h4>
                                        <table cellpadding="5" cellspacing="0" border="0" class="formtable" summary="">
                                            <tbody>
                                                <tr>
                                                    <td valign="top">
                                                        <span class="dateimg" style="display: flex; justify-content: center; align-items: center; margin-right: 5px;">

                                                            <asp:DropDownList ID="ddlDownload" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDownload_SelectedIndexChanged">
                                                                <asp:ListItem Value="">Select File</asp:ListItem>
                                                                <asp:ListItem Value="1">Letter_and_Certificate-3</asp:ListItem>
                                                                <asp:ListItem Value="2">Self Declaration-Certificate</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:ImageButton ID="btnCreateSTCForm" runat="server" ImageUrl="../../admin/images/btn_create_stc_form.png"
                                                                CausesValidation="false" OnClick="btnCreateSTCForm_Click" Visible="false" />
                                                        </span>

                                                    </td>
                                                    <td>
                                                        <asp:Panel runat="server" ID="panelPickListBtn">
                                                            <span class="dateimg">

                                                                <asp:Button ID="btnaddnew" runat="server" CausesValidation="false" CssClass="btn btn-info btngray redreq" OnClick="btnaddnew_Click1" Visible="false" Text="Add" />
                                                                <asp:ImageButton ID="btnPickList" Style="display: none" runat="server" OnClick="btnPickList_Click"
                                                                    Text="PickList" CausesValidation="false"
                                                                    ImageUrl="../../admin/images/btn_picklist.png" />
                                                            </span>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="form-group col-sm-3">
                                            <span class="name">
                                                <asp:Label ID="Label2" runat="server" class="control-label">
                                               Install Start Date</asp:Label></span>
                                            <div class="input-group datevalidation date wid100 datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtInstallBookingDate" runat="server" class="form-control" Width="125px">
                                                </asp:TextBox>
                                                <%--   <asp:RangeValidator ID="DateRange" ValidationGroup="preinst" class="datecmp"
                                                runat="server" ControlToValidate="txtInstallBookingDate"
                                                Type="Date" ErrorMessage="Install Date Is not Greater Than Lic Expire date."></asp:RangeValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group dateimgarea col-md-3">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Stock Allocation Store
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlStockAllocationStore" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                                    ControlToValidate="ddlStockAllocationStore" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                            </div>

                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <span class="name">
                                                <label class="control-label">System Details</label>
                                            </span>
                                            <asp:TextBox ID="txtsysdetails1" runat="server"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <%--    <asp:UpdatePanel runat="server" ID="upddl" UpdateMode="Conditional">
                                        <ContentTemplate>--%>

                                        <div class="form-group col-md-3">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Installer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInstaller_SelectedIndexChanged"
                                                    AutoPostBack="true" aria-controls="DataTables_Table_0" class="myval">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="" CssClass=""
                                                    ControlToValidate="ddlInstaller" Display="Dynamic" ValidationGroup="preinst" InitialValue="">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="btnCustNotes" runat="server" class="btn btn-primary" Text="Cust Notes"
                                                    Visible="false" Style="float: left" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <%--    </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger controlid="ddlInstaller" />
                                        </Triggers>
                                        </asp:UpdatePanel>--%>
                                        <div class="form-group col-md-3" style="display:none">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Designer
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlDesigner" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <span style="float: left; width: 100px; padding-left: 10px;">
                                                <asp:Button ID="btnComplianceCert" runat="server" class="btn btn-primary" Text="Compliance Cert"
                                                    Visible="false" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3" style="display:none">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Electrician
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlElectrician" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>

                                          <div class="form-group col-md-4">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Picklist
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:Button ID="btnPickupList" runat="server" OnClick="btnPickupList_Click" CssClass="btn btn-primary" Text="Picklist" />
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="name">
                                                <label class="control-label">
                                                    Installer Notes
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInstallerNotes" runat="server" TextMode="MultiLine" Height="70px"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--//INSTALL DETAIL--%>

                            <div class="box" id="InstallCompDETAILS">
                                <div class="boxHeading">
                                    <div class="flexRow">
                                        <h4 class="formHeading">Installation Complete Detail</h4>
                                        <table cellpadding="5" cellspacing="0" border="0" class="formtable" summary="">
                                            <tbody>
                                                <tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <span class="name">
                                                <asp:Label ID="Label8" runat="server" class="control-label">
                                               Install Complete Date</asp:Label></span>
                                            <div class="input-group datevalidation date wid100 datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtinstCompDate" runat="server" class="form-control" OnTextChanged="txtinstCompDate_TextChanged" AutoPostBack="true">
                                                </asp:TextBox>
                                                 <asp:Label runat="server" ID="lnlError"></asp:Label>
                                                <%--   <asp:RangeValidator ID="DateRange" ValidationGroup="preinst" class="datecmp"
                                                runat="server" ControlToValidate="txtInstallBookingDate"
                                                Type="Date" ErrorMessage="Install Date Is not Greater Than Lic Expire date."></asp:RangeValidator>--%>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="boxHeading">
                                    <div class="flexRow">
                                        <h4 class="formHeading">DisCom Meter Installation Detail</h4>
                                        <table cellpadding="5" cellspacing="0" border="0" class="formtable" summary="">
                                            <tbody>
                                                <tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="form-group col-sm-3">
                                            <span class="name">
                                                <asp:Label ID="Label9" runat="server" class="control-label">
                                               DisCom Complete Date</asp:Label></span>
                                            <div class="input-group datevalidation date wid100 datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="TextBox1" runat="server" class="form-control" Width="125px">
                                                </asp:TextBox>
                                                <%--   <asp:RangeValidator ID="DateRange" ValidationGroup="preinst" class="datecmp"
                                                runat="server" ControlToValidate="txtInstallBookingDate"
                                                Type="Date" ErrorMessage="Install Date Is not Greater Than Lic Expire date."></asp:RangeValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <span class="name">
                                                <label class="control-label">
                                                    DisCom Meter No
                                                </label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="txtdiscommeterno" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                        </div>

                                         <div class="form-group col-sm-3">
                                            <span class="name">
                                                <label class="control-label">
                                                   Solar meter number
                                                </label>
                                            </span>
                                            <span>
                                                <asp:TextBox ID="txtSolarMeterNumber" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                            </span>
                                        </div>

                                        <div class="form-group col-sm-3">
                                            <div class="topfileuploadbox">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <div style="word-wrap: break-word;">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                DisCom Bidirection Meter Installation <span class="symbol required"></span>
                                                            </label>
                                                            <%-- <span id="errormsg"></span>--%>

                                                            <label class="custom-fileupload">


                                                                <asp:FileUpload ID="FileUpload2" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfile12();" />

                                                                <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                <span class="btnbox">Attach file</span>
                                                                <%--<div id="errormsg" style="display:none">test</div>--%>
                                                                <div class="clear">
                                                                </div>
                                                                <div class="clear">
                                                                </div>

                                                            </label>
                                                        </span>
                                                        <%--<span id="Filesizeerror" style="display:none"></span>--%>

                                                        <div class="clear">
                                                            <asp:Label ID="fileDiscom" runat="server"></asp:Label>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="boxHeading">
                                    <div class="flexRow">
                                        <h4 class="formHeading">Certificate of Bi-directional Meter</h4>
                                        <table cellpadding="5" cellspacing="0" border="0" class="formtable" summary="">
                                            <tbody>
                                                <tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="form-group col-sm-3">
                                            <span class="name">
                                                <asp:Label ID="Label11" runat="server" class="control-label">
                                               Bi-directional Complete Date</asp:Label></span>
                                            <div class="input-group datevalidation date wid100 datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="TextBox2" runat="server" class="form-control" Width="125px">
                                                </asp:TextBox>
                                                <%--   <asp:RangeValidator ID="DateRange" ValidationGroup="preinst" class="datecmp"
                                                runat="server" ControlToValidate="txtInstallBookingDate"
                                                Type="Date" ErrorMessage="Install Date Is not Greater Than Lic Expire date."></asp:RangeValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <div class="topfileuploadbox">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <div style="word-wrap: break-word;">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Certificate of Bi-directional Meter <span class="symbol required"></span>
                                                            </label>
                                                            <%-- <span id="errormsg"></span>--%>

                                                            <label class="custom-fileupload">


                                                                <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="FileUpload1" ClientValidationFunction="getsubcat();" />--%>
                                                                <asp:FileUpload ID="fpBidirectionalMeter" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfile23();" />
                                                                <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                <span class="btnbox">Attach file</span>
                                                                <%--<div id="errormsg" style="display:none">test</div>--%>
                                                                <div class="clear">
                                                                </div>
                                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload2"
                                                                            ValidationGroup="uploadcompanypdf" ValidationExpression="^.+(.pdf|.jpeg|.jpg|.PDF|.JPG|.JPEG)$" Style="color: red;"
                                                                            Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                            ControlToValidate="FileUpload2" Display="Dynamic" ValidationGroup="uploadcompanypdf"></asp:RequiredFieldValidator>--%>

                                                                <div class="clear">
                                                                </div>

                                                            </label>
                                                        </span>
                                                        <%--<span id="Filesizeerror" style="display:none"></span>--%>

                                                        <div class="clear">
                                                            <asp:Label ID="fileBidirectional" runat="server"></asp:Label>

                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="box dispnone">
                                <div class="boxPadding">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="name">
                                                <label class="control-label">System Details</label>
                                            </span><span>
                                                <asp:HiddenField ID="hbdSystemDetails" runat="server" />
                                                <asp:TextBox ID="txtSystemDetails" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="name ">
                                                <label class="control-label">
                                                    Panel
                                                </label>
                                            </span>

                                            <span>
                                                <asp:TextBox ID="txtnoofpanel" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="name">
                                                <label class="control-label">
                                                    Inverter
                                                </label>
                                            </span>

                                            <span>
                                                <asp:TextBox ID="txtnoofinverter" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row dispnone">
                            <div class="col-md-4">
                                <div class="form-group " style="display: none;">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Cust/Inst Notify
                                        </label>
                                    </span><span>
                                        <asp:DropDownList ID="ddlSendMail" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">Customer-Install Confirm</asp:ListItem>
                                            <asp:ListItem Value="2">Installer-Install Confirm</asp:ListItem>
                                            <asp:ListItem Value="3">Installer-Cancel Install</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <span class="name">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <asp:Button ID="btnMove" runat="server" OnClick="btnMove_Click" CssClass="btn btn-primary" Text="Move" Visible="false" />
                                            </div>
                                            <div class="col-md-10">
                                                <asp:DropDownList ID="ddlMoveProject" runat="server" Visible="false" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="">Project</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divPickList" runat="server" visible="false">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5">
                                            <h5>Pick List Log</h5>
                                        </div>
                                    </div>
                                    <div class="row finalgrid">
                                        <div class="col-md-12" runat="server" id="divpl">
                                            <table width="100%" cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%; text-align: center;">PickList Generated Date
                                                        </th>
                                                        <th style="width: 10%; text-align: center">InstallationBooked Date
                                                        </th>
                                                        <th style="width: 10%; text-align: center">Installer Name  
                                                        </th>
                                                        <th style="width: 30%; text-align: center">Reason
                                                        </th>
                                                        <th style="width: 30%; text-align: center">Note
                                                        </th>
                                                        <th style="width: 10%; text-align: center">CreatedBy
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptPickList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 10%;">
                                                                <%#Eval("PickListDateTime", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <td style="width: 10%;">
                                                                <%#Eval("InstallBookedDate", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            <td style="width: 10%; word-break: break-word;">
                                                                <%#Eval("InstallerName")%>
                                                            </td>
                                                            <td style="width: 30%;">
                                                                <%#Eval("Reason")%>
                                                            </td>
                                                            <td style="width: 30%;">
                                                                <%#Eval("Note")%>
                                                            </td>
                                                            <td style="width: 10%;">
                                                                <%#Eval("EmpNicName")%>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="picklistdiv" runat="server" visible="false" style="display: none">
                            <div class="col-md-4">
                                <div class="form-group wdth200">
                                    <span class="name">
                                        <label class="control-label">
                                            Reason for generating PickList again:
                                        </label>
                                    </span><span>
                                        <asp:TextBox ID="txtreason" runat="server" TextMode="MultiLine" Height="70px"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtreason"
                                            Display="Dynamic" ValidationGroup="picklist"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group wdth200">
                                    <span class="name">
                                        <label class="control-label">
                                            Note for generating PickList again:
                                        </label>
                                    </span><span>
                                        <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Height="70px"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                            ValidationGroup="picklist" ControlToValidate="txtnote"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="padding-top: 40px;">
                                    <asp:Button class="btn btn-primary" ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"
                                        CausesValidation="true" ValidationGroup="picklist" Text="Submit" />
                                    <%--<asp:HyperLink ID="hypDownload" Visible="false"  runat="server" Text="Download PickList">            
                                    </asp:HyperLink>--%>
                                    <a class="btn btn-info" runat="server" id="btnDownload" onserverclick="btnDownload_Click" style="display: none;"><i class="fa fa-download" style="font-size: 12px;"></i>Download PickList</a>
                                    <asp:HiddenField runat="server" ID="hdnPickListLogID" />
                                    <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                        CausesValidation="false" Text="Cancel" />
                                </div>
                            </div>
                        </div>

                        <div class="row" id="divAddUpdate" runat="server">
                            <div class="col-sm-12">
                                <div class="dividerLine"></div>
                            </div>
                            <div class="col-md-12 textRight">
                                <asp:Button class="btn largeButton greenBtn savewhiteicon btnsaveicon redreq redreq1" ID="btnUpdatePreInst" runat="server" OnClick="btnUpdatePreInst_Click"
                                    CausesValidation="true" Text="Save" />
                                <asp:Button class="dispnone btn largeButton greenBtn savewhiteicon btnsaveicon redreq redreq1" ID="btngreenbot" runat="server" OnClick="GreenBotSave"
                                    CausesValidation="true" ValidationGroup="preinst" Text="Greenbot Save" />
                                <asp:Button class="dispnone btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnformbay1" Visible="false" CausesValidation="true" ValidationGroup="preinst" runat="server" OnClick="btnformbay1_Click" Text="Formbay" />
                                <asp:Button class="dispnone btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnquickfrom" runat="server" OnClick="btnquickfrom_Click" Text="QuickForm" />
                                <asp:Button class="dispnone btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnquickfromUpdate" runat="server" OnClick="btnquickfromUpdate_Click" Text="QuickForm Update" />
                            </div>

                        </div>
                        </span>
                    </asp:Panel>
                </div>
            </div>
        </section>


        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div_popup" class="modal_popup" align="" style="width: 500px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="lbltitle"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H1">Update Address</h3>
                                        </div>

                                        <%--   <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-md-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="">
                                                            First Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUpdateFirstName" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Last Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUpdateLastName"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-sm-12">
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Mobile 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtContMobile"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>

                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Phone 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtCustPhone"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                            ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>

                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 form-group" id="divEmail" runat="server">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Email 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                            ValidationGroup="updateaddress" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-sm-12">
                                                <div class=" col-sm-6 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Unit No 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtUnitNo"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Unit Type 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlUnitType" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-3 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Street No 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtstreetno"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Street Name 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtStreetName"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Street Type 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlstreetType" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            City 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtCity"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            State 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtState"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class=" col-sm-4 form-group">
                                                    <span class="name left-text">
                                                        <label class="">
                                                            Post Code 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:TextBox ID="txtpostcode"
                                                            runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>


                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <%-- <div class="col-sm-12">--%>

                                            <div class=" col-sm-12 center-text form-group">
                                                <span class="">
                                                    <label class="">
                                                        &nbsp;
                                                    </label>
                                                </span><span class="">
                                                    <asp:Button class="btn btn-primary btnsaveicon" ID="btnUpdateAddressData" runat="server" OnClick="btnUpdateAddressData_Click"
                                                        CausesValidation="true" Text="Save" />

                                                </span>

                                                <div class="clear">
                                                </div>
                                            </div>
                                            <%-- </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="Button2" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
            OkControlID="btnOKAddress" TargetControlID="Button2">
        </cc1:ModalPopupExtender>
        <div id="divAddressCheck" runat="server" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                        </div>
                        <h4 class="modal-title" id="H4">Duplicate Address</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                <tbody>
                                    <tr align="center">
                                        <td>
                                            <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="tablescrolldiv">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="rptaddress" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptaddress_PageIndexChanging"
                                        PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Customers">
                                                <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate><%# Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Button ID="btnNULLStock" Style="display: none;" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
        <asp:HiddenField ID="hdnclosepopup" runat="server" />
        <!-- Region For Stock New Changes Pick List log--->
        <asp:Button ID="Button5" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divStock" TargetControlID="Button5"
            CancelControlID="btnCancelStock">
        </cc1:ModalPopupExtender>
        <div id="divStock" class="modal_popup" runat="server" style="display: none">
            <div class="modal-dialog" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="btnCancelStock" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="H2">Add PickList Stock Item</h4>
                    </div>
                    <div class="finaladdupdate printorder">
                        <div id="Div1" runat="server" visible="true">
                            <div class="graybgarea" style="padding: 10px 0px;">
                                <div class="row">
                                    <div class="col-md-8">
                                        <asp:Repeater ID="rptattribute" runat="server" OnItemDataBound="rptattribute_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Category
                                                                </label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockCategoryId" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                        ValidationGroup="Add" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Item</label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <%--      <div class="form-group col-md-2">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    System Details</label>
                                                            </span><span style="width: 90%">
                                                                <asp:HiddenField ID="hdnSysDetails" runat="server" Value='<%# Eval("SysDetails") %>' />
                                                                <asp:TextBox ID="txtSysDetails" runat="server" ReadOnly="true"  CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>--%>
                                                    <div class="form-group col-md-2">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Quantity</label>
                                                            </span><span style="width: 60%">
                                                                <asp:HiddenField ID="hndQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                                <asp:HiddenField ID="hndqty1" runat="server" Value='<%# Eval("Qty") %>' />
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group ">
                                                            <div class="padtop15">
                                                                <span class="name">
                                                                    <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-top: 23px;">
                                            <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-left: 15px; padding-top: 3px;">
                                            <asp:Button ID="btnAdd" runat="server" Text="Save" OnClick="btnAdd_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Region For Stock New Changes--->
        <!-- Region For Stock New Changes Pick List log--->
        <asp:Button ID="Button6" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderPickListAgain" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="div3" TargetControlID="Button6"
            CancelControlID="Button7">
        </cc1:ModalPopupExtender>
        <div id="div3" class="modal_popup" runat="server" style="display: none">
            <div class="modal-dialog" style="width: 1000px">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="Button7" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="H3">Add PickList Stock Item Again</h4>
                    </div>
                    <div class="finaladdupdate printorder">
                        <div id="Div4" runat="server" visible="true">
                            <div class="graybgarea" style="padding: 10px 0px;">
                                <div class="row">
                                    <div class="col-md-8">
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Category
                                                                </label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockCategoryId" runat="server" Value='<%# Eval("StockCategoryID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockCategoryID" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlStockCategoryID_SelectedIndexChanged1"
                                                                        AutoPostBack="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                                        ValidationGroup="Add" ControlToValidate="ddlStockCategoryID" Display="Dynamic" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Stock Item</label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hdnStockItem" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                                <div class="drpValidate">
                                                                    <asp:DropDownList ID="ddlStockItem" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                                        AppendDataBoundItems="true">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="comperror"
                                                                        ControlToValidate="ddlStockItem" Display="Dynamic" ValidationGroup="Add"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <div class="col-md-12">
                                                            <span class="name disblock">
                                                                <label>
                                                                    Quantity</label>
                                                            </span><span style="width: 60%">
                                                                <asp:HiddenField ID="hdbQty" runat="server" Value='<%# Eval("Qty") %>' />
                                                                <asp:HiddenField ID="hndqtynew" runat="server" Value='<%# Eval("Qty") %>' />
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group ">
                                                            <div class="padtop15">
                                                                <span class="name">
                                                                    <%--<asp:HiddenField ID="hdnStockOrderItemID" runat="server" Value='<%#Eval("StockOrderItemID") %>' />--%>
                                                                    <asp:CheckBox ID="chkdelete" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkdelete").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <br />
                                                                    <asp:HiddenField ID="hdntype" runat="server" Value='<%#Eval("type") %>' />
                                                                    <asp:Button ID="litremove" runat="server" OnClick="litremove_Click1" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" /><br />

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-top: 23px;">
                                            <asp:Button ID="btnAddRowagain" runat="server" Text="Add" OnClick="btnAddRowagain_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="col-md-12 form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Reason for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtpicklistreason" runat="server" TextMode="MultiLine" Width="290px" Height="80px"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass="" ControlToValidate="txtpicklistreason"
                                                    Display="Dynamic" ValidationGroup="picklistagain"></asp:RequiredFieldValidator>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12 form-group">
                                            <span class="name disblock">
                                                <label>
                                                    Note for generating PickList again:</label>
                                            </span><span style="width: 60%">
                                                <asp:TextBox ID="txtpicklistnote" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtpicklistnote"
                                                    ErrorMessage="" CssClass="reqerror" ValidationGroup="picklistagain" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: left; padding-left: 15px; padding-top: 3px;">
                                            <asp:Button ID="btnaddagain" runat="server" Text="Save" OnClick="btnaddagain_Click" CssClass="btn btn-info redreq"
                                                CausesValidation="true" ValidationGroup="picklistagain" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  End Region For Stock New Changes--->


        <asp:Button ID="Button3" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="modalstockupdate" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="LinkButton1" DropShadow="false" PopupControlID="div2" TargetControlID="Button3">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div2" class="modal_popup" align="" style="width: 350px;">
            <div class="popupdiv">
                <div class="">
                    <div class="color-line"></div>
                    <div class="">

                        <div class="panel panel-default">
                            <div class="">
                                <asp:Label runat="server" ID="Label1"></asp:Label>
                                <div class="">
                                    <div class="modal-header">

                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton1" CausesValidation="false"
                                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                                 Close
                                            </asp:LinkButton>
                                        </div>

                                        <div class="">
                                            <h3 class="modal-title" id="H145">Stock Update</h3>
                                        </div>

                                        <%--   <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important;">
                                <div class="row">
                                    <div class="">

                                        <div class="graybgarea">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12 form-group">
                                                    <span class="name  left-text">
                                                        <label class="control-label">
                                                            Stock Status 
                                                        </label>
                                                    </span><span class="">
                                                        <asp:DropDownList ID="ddlstockstatus" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select myval" AutoPostBack="true" OnSelectedIndexChanged="ddlstockstatus_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Return Stock</asp:ListItem>
                                                            <asp:ListItem Value="2">Job Cancelled</asp:ListItem>
                                                            <asp:ListItem Value="3">Stock Swapped</asp:ListItem>
                                                            <asp:ListItem Value="4">Stock with Int.</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlstockstatus" Display="Dynamic" ValidationGroup="stockupdate" InitialValue=""> </asp:RequiredFieldValidator>
                                                    </span>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Date 
                                                            </label>
                                                        </span><span class="">
                                                            <div class="input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtDate" runat="server" class="form-control">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                                                                    ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Notes
                                                            </label>
                                                        </span><span class="">
                                                            <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" Width="290px" Height="80px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtNotes"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div runat="server" class="col-sm-12" visible="false" id="txtprono">
                                                    <div class="form-group">
                                                        <span class="name  left-text">
                                                            <label class="">
                                                                Project Number
                                                            </label>
                                                        </span><span class="">
                                                            <asp:TextBox ID="txtProjNo" runat="server" class="form-control"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                                                Enabled="True" FilterType="Numbers" TargetControlID="txtProjNo">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProjNo"
                                                                ErrorMessage="" CssClass="reqerror" ValidationGroup="stockupdate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>

                                                        <div class="clear">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class=" col-sm-12 center-text form-group">
                                                    <span class="">
                                                        <label class="">
                                                            &nbsp;
                                                        </label>
                                                    </span><span class="">
                                                        <asp:Button CausesValidation="true" ValidationGroup="stockupdate" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="Button1" runat="server" OnClick="Button1_Click"
                                                            Text="Save" />

                                                    </span>
                                                    <asp:Button CausesValidation="true" ValidationGroup="stockupdate" class="btn btn-primary savewhiteicon btnsaveicon redreq redreq1" ID="Button4" runat="server" OnClick="Button4_Click"
                                                        Text="Save" />
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- </div>
        </div>--%>
        <style>
            .modalbackground {
                background-color: Gray;
                opacity: 0.5;
                filter: Alpha(opacity=50);
            }

            .modalpopup {
                background-color: white;
                padding: 6px 6px 6px 6px;
            }

            table.formtable h3 {
                color: #800000;
                font-size: 16px;
            }
        </style>
        <style type="text/css">
            .modalPopup {
                background-color: #696969;
                filter: alp;
                opacity: 0.7;
                z-index: -1;
            }
        </style>
        <script>

            function Uploadfile23() {
                debugger;
                var uploadfiles = $('input[type="file"]').get(1);
                var fileUpload = document.getElementById("<%=fpBidirectionalMeter.ClientID %>");
                $('#<%=hdnFile2.ClientID %>').val(fileUpload.files[0].name);
                var fromdata = new FormData();
                fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
                var choice = {};
                choice.url = "Handler.ashx";
                choice.type = "POST";
                choice.data = fromdata;
                choice.contentType = false;
                choice.processData = false;
                choice.success = function (result) {
                };
                choice.error = function (err) {
                };
                $.ajax(choice);
            }

            function validateformbaystreetAddress(source, args) {
                var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
                $.ajax({
                    type: "POST",
                    //action:"continue.aspx",
                    url: "company.aspx/Getstreetname",
                    data: "{'streetname':'" + streetname + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == true) {
                            document.getElementById("Divvalidstreetname1").style.display = "block";
                            document.getElementById("DivInvalidstreetname1").style.display = "none";
                        }
                        else {
                            document.getElementById("Divvalidstreetname1").style.display = "none";
                            document.getElementById("DivInvalidstreetname1").style.display = "block";
                        }
                    }
                });
            }

        <%--    function btnSumbit_ClientClick() {
                //alert("fstst");
                formValidate();
                //$('#<%=picklistdiv.ClientID %>').hide();
            }--%>

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpreinst);
            function pageLoadedpreinst() {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
            }

        </script>
        <asp:HiddenField ID="hdnFileName" runat="server" />
        <asp:HiddenField ID="hdnFile2" runat="server" />
    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnUpdatePreInst" />--%>
        <asp:PostBackTrigger ControlID="btnPickList" />
        <asp:PostBackTrigger ControlID="btnDownload" />
        <asp:PostBackTrigger ControlID="btnRemoveInst" />
        <asp:PostBackTrigger ControlID="btnMove" />
        <asp:PostBackTrigger ControlID="btnCreateSTCForm" />
        <asp:PostBackTrigger ControlID="btnformbay1" />
        <asp:PostBackTrigger ControlID="btngreenbot" />
        <%-- <asp:PostBackTrigger ControlID="Button1" />--%>
        <asp:PostBackTrigger ControlID="btnUpdateAddressData" />
        <asp:PostBackTrigger ControlID="btnaddnew" />
        <%--<asp:PostBackTrigger ControlID="btnAdd_Click" />--%>
        <%--  <asp:AsyncPostBackTrigger ControlID="updatepanelgrid" />--%>
    </Triggers>
</asp:UpdatePanel>

<script>

</script>
