﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class includes_controls_promo : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        HidePanels();
    }

    public void BindPromo()
    {
      
        BindGrid(0);
        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();
        //Response.Write("dsf");
        //Response.End();
        if ((Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
        {
            
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            string CEmpType = stEmpC.EmpType;
            //string CSalesTeamID = stEmpC.SalesTeamID;
            string CSalesTeamID = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                }
                CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
            }

            if (Request.QueryString["proid"] != string.Empty)
            {
                SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stCont.EmployeeID);
                string EmpType = stEmp.EmpType;
                //string SalesTeamID = stEmp.SalesTeamID;
                string SalesTeam = "";
                DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stCont.EmployeeID);
                if (dt_empsale1.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale1.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }

                if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                {
                    PanGrid.Enabled = true;
                }
                else
                {
                    PanGrid.Enabled = false;
                }
            }
        }
        if ((Roles.IsUserInRole("Accountant")) || (Roles.IsUserInRole("BookInstallation")) || (Roles.IsUserInRole("Installer")) || (Roles.IsUserInRole("Maintenance")) || (Roles.IsUserInRole("PostInstaller")) || (Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Verification")))
        {
            PanGrid.Enabled = false;
        }
    }

    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblPromo.tblPromo_Select_ByContactID(Request.QueryString["contid"]);

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
      
        DataTable dt = GetGridData();
      
        if (dt.Rows.Count == 0)
        {

            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
      
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        bool sucess1 = ClstblContacts.tblContacts_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetAdd1();
            PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(pancontactpromo,
                                           this.GetType(),
                                           "MyAction",
                                           "doMyAction();",
                                           true);
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void chktag_OnCheckedChanged(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        bool success = true;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");
            CheckBox chktag = (CheckBox)gridRow.FindControl("chktag");

            success = ClstblPromo.tblPromo_update_Interested(hndid.Value, chktag.Checked.ToString());
        }
        BindPromo();
    }

    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}