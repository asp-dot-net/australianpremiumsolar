<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectdetail.ascx.cs"
    Inherits="includes_controls_projectdetail" %>


<input type="hidden" id="chkaddvalpro" value="0" />
<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }
</style>
<%--<script src="../../assets/js/jquery.min.js"></script>--%>

<script type="text/javascript">

    function fnHideLoader() {
        $('.loading-container').hide();
    }

     function callSomething(value) {
        $("#ctl00_ContentPlaceHolder1_lblRebateStatus").val(value);
        $("#ctl00_ContentPlaceHolder1_lblRebateStatus").text(value);

    }
     function callProjectSTatus(value) {
        $("#ctl00_ContentPlaceHolder1_lblstatus").val(value);
        $("#ctl00_ContentPlaceHolder1_lblstatus").text(value);

    }

    function Uploadfiledetai() {
        var uploadfiles = $('input[type="file"]').get(1);

        var fileUpload = document.getElementById("<%=FileUpload2.ClientID %>");
        $('#<%=hdnFileName.ClientID %>').val(fileUpload.files[0].name);
        var fromdata = new FormData();
        fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
        var choice = {};
        choice.url = "Handler.ashx";
        choice.type = "POST";
        choice.data = fromdata;
        choice.contentType = false;
        choice.processData = false;
        choice.success = function (result) {
        };
        choice.error = function (err) {
        };
        $.ajax(choice);
    }

    function Uploadfiledetaipayment() {
        var uploadfiles = $('input[type="file"]').get(1);

        var fileUpload = document.getElementById("<%=FileUpload1.ClientID %>");
        $('#<%=hdnFileNamepayment.ClientID %>').val(fileUpload.files[0].name);
        var fromdata = new FormData();
        fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
        var choice = {};
        choice.url = "HandlerDetailPayment.ashx";
        choice.type = "POST";
        choice.data = fromdata;
        choice.contentType = false;
        choice.processData = false;
        choice.success = function (result) {
        };
        choice.error = function (err) {
        };
        $.ajax(choice);
    }


    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<script type="text/javascript">
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>

<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>

        <section class="row m-b-md">
            <div class="col-sm-12">
                <div class="contactsarea boxPadding formGrid">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-danger" id="PanCompanyError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Please First Update Information in Company.</strong>
                        </div>
                    </div>

                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-info" id="lblerror" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                </div>

                                <div class="box dropDownwid100">
                                    <div class="boxHeading">
                                        <h4 class="formHeading">Project Details</h4>
                                    </div>
                                    <div class="boxPadding">
                                        <div class="row">
                                            <div class="col-sm-2 form-group">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        Project Type<span class="symbol required"></span>
                                                    </label>
                                                </span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlProjectTypeID" runat="server" AutoPostBack="true" Width="200px" OnSelectedIndexChanged="ddlProjectTypeID_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                        ControlToValidate="ddlProjectTypeID" Display="Dynamic" ValidationGroup="projectdetail"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group" id="divOnHold" runat="server" visible="false">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        OnHold Reason<span class="symbol required"></span>
                                                    </label>
                                                </span><span>
                                                    <asp:DropDownList ID="ddlProjectOnHoldID" runat="server" Width="200px" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                        ValidationGroup="detail1" ControlToValidate="ddlProjectOnHoldID" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group" id="divCancelReason" runat="server" visible="false">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        Cancel Reason<span class="symbol required"></span>
                                                    </label>
                                                </span><span>
                                                    <asp:DropDownList ID="ddlProjectCancelID" runat="server" Width="200px" aria-controls="DataTables_Table_0" CssClass="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required."
                                                        ValidationGroup="detail1" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group" id="divSalesRep" runat="server">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        Sales Rep<span class="symbol required"></span>
                                                    </label>
                                                </span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                        Width="200px" AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage=""
                                                        ValidationGroup="projectdetail" ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>

                                            <div class="col-sm-2 form-group">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Old Project No.
                                                    </label>
                                                </span><span class="withpersonal">
                                                    <asp:TextBox ID="txtOldProjectNumber" runat="server" MaxLength="200" class="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Manual Quote No.
                                                    </label>
                                                </span><span class="withpersonal">
                                                    <asp:TextBox ID="txtManualQuoteNumber" runat="server" MaxLength="200" class="form-control"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        Contact<span class="symbol required"></span>
                                                    </label>
                                                </span>
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlContact" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                        Width="200px" AppendDataBoundItems="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                        ValidationGroup="projectdetail" ControlToValidate="ddlContact" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 form-group">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                        Project Opened<span class="symbol required"></span>
                                                    </label>
                                                </span><span>

                                                    <asp:TextBox ID="txtProjectOpened" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                                    <%--
                                                        <asp:ImageButton ID="Image15" Enabled="false" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                        <cc1:CalendarExtender ID="CalendarExtender15" runat="server" PopupButtonID="Image15"
                                                            TargetControlID="txtProjectOpened" Format="dd/MM/yyyy">
                                                        </cc1:CalendarExtender>
                                                        <asp:RegularExpressionValidator ValidationGroup="sale" ControlToValidate="txtProjectOpened"
                                                            ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                    --%>

                                                </span>


                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label class="control-label">Install Site</label>
                                                <div id="Div1" class="" visible="false" runat="server">
                                                    <asp:HiddenField ID="hndaddress" runat="server" />
                                                    <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="200" class="form-control"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                        ControlToValidate="txtInstallAddressline" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                    <%-- <asp:CustomValidator ID="Customdetail" runat="server"
                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="projectdetail" Enabled="false"
                                                            ClientValidationFunction="ChkFundetail"></asp:CustomValidator>
                                                    --%>
                                                </div>


                                                <asp:Panel runat="server" ID="PanInstallAddress">
                                                    <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                    ControlToValidate="txtInstallAddress" Display="Dynamic" ValidationGroup="detail1"></asp:RequiredFieldValidator>--%>
                                                </asp:Panel>

                                                <asp:HiddenField ID="hndstreetno" runat="server" />
                                                <asp:HiddenField ID="hndstreetname" runat="server" />
                                                <asp:HiddenField ID="hndstreettype" runat="server" />
                                                <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                <asp:HiddenField ID="hndunittype" runat="server" />
                                                <asp:HiddenField ID="hndunitno" runat="server" />
                                                <div id="validaddressid" style="display: none">
                                                    <i class='fa fa-check'></i>
                                                    <%--        <img src="../../../images/check.png" alt="check">--%>
                                                    Address is valid.
                                                </div>
                                                <div id="invalidaddressid" style="display: none">
                                                    <i class='fa fa-close'></i>
                                                    Address is invalid.
                                                </div>
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <div class="" id="divStreetno" runat="server">
                                                    <asp:Label ID="Label12" runat="server">Address 1</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 form-group spicaldivin streatfield" id="divstname" runat="server">
                                                <asp:Label ID="Label1" runat="server">Address 2</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="col-sm-3 form-group" id="divNearby" runat="server" style="display: none">
                                                <asp:Label ID="Label10" runat="server">Near by</asp:Label>
                                                <div>
                                                    <asp:TextBox ID="txtformbayNearby" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayNearby_TextChanged"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtformbayNearby" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 form-group" visible="false" id="divsttype" runat="server" style="display: none">
                                                <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                <div id="Div8" class="drpValidate" runat="server">
                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnTextChanged="ddlformbaystreettype_SelectedIndexChanged">

                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 form-group" id="Div9" runat="server">
                                                <asp:Label ID="Label15" runat="server">Street State</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:DropDownList ID="ddlStreetState" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                        <asp:ListItem Value="">Street State</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-4  form-group" id="Div24" runat="server">
                                                <asp:Label ID="Label35" runat="server">District</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:DropDownList ID="ddlDistrict" runat="server" AppendDataBoundItems="true" CssClass="myvalproject">
                                                        <asp:ListItem Value="0">Select District</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-4  form-group" id="Div25" runat="server">
                                                <asp:Label ID="Label36" runat="server">Taluka</asp:Label>
                                                      <div>
                                                            <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox" ></asp:TextBox>
                                                        </div>
                                                 <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    
                                            </div>
                                            <div class="col-sm-6 form-group" id="seq" runat="server">
                                                <asp:Label ID="lblMobile" runat="server">City</asp:Label>
                                                    <div>
                                                        <asp:TextBox runat="server" ID="ddlStreetCity" Enabled="true" class="form-control modaltextbox" ></asp:TextBox>
                                                    </div>
                                                   <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllCity"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                

                                            </div>
                                            <div class="col-sm-4 form-group" id="Div3" runat="server" style="display:none">
                                                <asp:Label ID="Label6" runat="server">Area</asp:Label>
                                                <div class="autocompletedropdown">
                                                    <asp:DropDownList ID="ddlArea" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomp"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select Area</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlArea" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <div class="drpValidate">
                                                    <div class="" id="Div10" runat="server">
                                                        <asp:Label ID="Label16" runat="server">Post Code</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="true" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="ddlformbaypostalcode_SelectedIndexChanged"></asp:TextBox>
                                                                 </div>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Project Notes
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtProjectNotes" CssClass="form-control" runat="server" Width="100%" Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Installer Notes
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtInstallerNotes" runat="server" Width="100%" Height="118px"
                                                        TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                    <asp:Button class="savewhiteicon btnviewnotes savewhiteicon martop5 dispnone"
                                                        ID="btnViewNotes" runat="server" OnClick="btnViewNotes_Click"
                                                        Text="View Notes" CausesValidation="true" />

                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Notes for Installation Department
                                                    </label>
                                                </span><span class="with44">
                                                    <asp:TextBox ID="txtMeterInstallerNotes" CssClass="form-control" runat="server" Width="100%"
                                                        Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box dropDownwid100">
                                    <div class="boxHeading">
                                        <h4 class="formHeading">DISCOM Details</h4>
                                    </div>
                                    <div class="boxPadding">
                                        <div class="row">
                                            <div class="form-group col-sm-3 selpen">
                                                <label class="control-label">
                                                    Select Discom
                                                </label>
                                                <asp:DropDownList ID="ddlElecDist" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddlElecDist_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group col-sm-3 selpen">
                                                <label class="control-label">
                                                    Select Division
                                                </label>
                                                <asp:DropDownList ID="ddldivision" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" OnSelectedIndexChanged="ddldivision_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                                  <div class="form-group col-sm-3 selpen">
                                                <label class="control-label">
                                                    Select Sub Division
                                                </label>
                                                <asp:DropDownList ID="ddlSubDivision" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true" 
                                                   >
                                                </asp:DropDownList>
                                            </div>
                                             <div class="form-group col-sm-3 selpen">
                                                <label class="control-label">
                                                    Circle
                                                </label>
                                                <asp:TextBox ID="txtCircle" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group col-sm-4 selpen">
                                                <label class="control-label">
                                                    Consumer Number
                                                </label>
                                                <asp:TextBox ID="txtNMINumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group col-sm-4 selpen">
                                                <label class="control-label">
                                                    Sanctioned /Contract Load (in kW)
                                                </label>
                                                <asp:TextBox ID="txtsanction" runat="server" MaxLength="200" CssClass="form-control" OnTextChanged="txtsanction_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                <asp:Label runat="server" ID="lnlError"></asp:Label>
                                            </div>
                                            <div class="form-group col-sm-4 selpen">
                                                <label class="control-label">
                                                    Who will provide the Net-Meter?
                                                </label>
                                                <asp:DropDownList ID="ddlnetmeter" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                    <asp:ListItem Value="0">Select Net-Meter</asp:ListItem>
                                                    <asp:ListItem Value="1">Discom </asp:ListItem>
                                                    <asp:ListItem Value="2">Installer/EA</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group col-sm-3 selpen">
                                                <label class="control-label">
                                                    Phase of proposed Solar Inverter
                                                </label>
                                                <asp:DropDownList ID="ddlsolarinverter" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                    <asp:ListItem Value="0">Select Phase</asp:ListItem>
                                                    <asp:ListItem Value="1">Single Phase </asp:ListItem>
                                                    <asp:ListItem Value="2">Three phase</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 selpen">
                                                <label class="control-label">
                                                    PV Capacity (DC) to be installed (in kW)
                                                </label>
                                                <asp:TextBox ID="txtpvcapacity" runat="server" MaxLength="200" CssClass="form-control" OnTextChanged="txtpvcapacity_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3 selpen">
                                                <label class="control-label">
                                                    House Type
                                                </label>
                                                <asp:DropDownList ID="ddlHouseType" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 selpen">
                                                <label class="control-label">
                                                    Roof Type
                                                </label>
                                                <asp:DropDownList ID="ddlRoofType" runat="server" aria-controls="DataTables_Table_0" CssClass=""
                                                    AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box dropDownwid100">
                                    <div class="boxHeading">
                                        <h4 class="formHeading">Application Details</h4>
                                    </div>
                                    <div class="boxPadding">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <asp:Label ID="Label2" runat="server" class="  control-label">Application Number</asp:Label>
                                                <asp:TextBox ID="txtdistributorapplicationnumber" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                             <div class="col-sm-3">
                                                <asp:Label ID="Label7" runat="server" class="  control-label">GUVNL Registeration No</asp:Label>
                                                <asp:TextBox ID="txtGUVNLregi" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:Label ID="Label3" runat="server" class="control-label">Application Date</asp:Label>
                                                <div class="input-group date wid100 datetimepicker1 ">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtElecDistApplied" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="formainline">
                                                    <asp:HiddenField ID="hdnFileName" runat="server" />
                                                    <asp:HiddenField ID="hdnFileNamepayment" runat="server" />

                                                    <div class="topfileuploadbox">
                                                        <div class="form-group" style="margin-bottom: 0px;">
                                                            <div style="word-wrap: break-word;">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Upload File <span class="symbol required"></span>
                                                                    </label>
                                                                    <%-- <span id="errormsg"></span>--%>

                                                                    <label class="custom-fileupload">

                                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                        <asp:FileUpload ID="FileUpload2" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfiledetai();" />
                                                                        <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                        <span class="btnbox">Attach file</span>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-primary" Target="_blank"
                                                    Text="View Note">
                                                </asp:HyperLink>
                                                                    </label>
                                                                </span>
                                                                <%--<span id="Filesizeerror" style="display:none"></span>--%>

                                                                <div class="clear">
                                                                    <asp:Label ID="lblfileName" runat="server"></asp:Label>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-1 padd01366">
                                                <label class="control-label">&nbsp;</label>
                                                
                                                </label>
                                            </div>



                                            <%-- <asp:Button CssClass="btn largeButton greenBtn btnaddicon redreqdocument" ID="btnAdd" runat="server" OnClick="btnattachfile_Click"
                                             Text="Add"  />--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="box dropDownwid100">
                                    <div class="boxHeading flexRow">
                                        <h4 class="formHeading">Quote Details</h4>
                                        <asp:Button class="btn btn-primary btnPayment savewhiteicon" ID="Button3" runat="server" OnClick="uploadpayment_Click" Text="Upload Payments" />
                                    </div>
                                    <div class="boxPadding">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label class="control-label">
                                                   Deposit Quote No:
                                                </label>
                                                <asp:TextBox ID="txtMeterEstimate" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">
                                                    Deposit Quotation Amount (in Rs):
                                                </label>
                                                <asp:TextBox ID="txtEstimatedPaid" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:Label ID="Label4" runat="server" class="  control-label">Deposit Due Date</asp:Label>
                                                <div class="input-group date wid100 datetimepicker1 ">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtestimatepaymentdue" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label">
                                                    Paid Status:
                                                </label>
                                                <asp:DropDownList ID="ddlestimatevalue" runat="server" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 dispnone">
                                                <%--<a href="#" class="btn largeButton blueBtn" OnClick="uploadpayment_Click">Upload Payments</a>
                                                --%>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row dispnone">
                                    <div class="col-md-4">

                                        <div class="form-group row">
                                            <div class="form-group col-md-6" id="divFollowupDate" runat="server" visible="false">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Follow Up Date
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtFollowupDate" runat="server" Width="120px" CssClass="form-control"
                                                        Enabled="false"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6" id="divNextFollowUpDate" runat="server" visible="false">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Next Follow Up
                                                    </label>
                                                </span><span class="dateimg">
                                                    <asp:TextBox ID="txtNextFollowUpDate" runat="server" Width="120px" CssClass="form-control"
                                                        Enabled="false"></asp:TextBox>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    House statys<span class="symbol required"></span>
                                                </label>
                                            </span>
                                            <div class="drpValidate">
                                                <asp:DropDownList ID="ddlHousestatys" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                    Width="200px" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: none">
                                            <span class="name disblock">
                                                <label class="control-label">
                                                    Completion Date<span class="symbol required"></span>
                                                </label>
                                            </span>
                                            <div class="input-group date datetimepicker1 ">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtStartDate" runat="server" class="form-control" Width="125px">
                                                </asp:TextBox>
                                                <%--<cc1:MaskedEditExtender ID="mskeditCheckIn" runat="server" Mask="99/99/9999" MessageValidatorTip="true"
                                                                OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="date"
                                                                TargetControlID="txtStartDate" CultureName="en-GB">
                                                            </cc1:MaskedEditExtender>--%>
                                            </div>
                                        </div>

                                        <div class="form-group" style="display: none">
                                            <%-- <span class="name disblock">
                                                    <label class="control-label">
                                                        SM Ready<span class="symbol required"></span>
                                                    </label>
                                                </span>--%>
                                            <div class=" checkbox-info checkbox">
                                                <span class="fistname">
                                                    <label for="<%=chkclickSMready.ClientID %>">
                                                        <asp:CheckBox ID="chkclickSMready" runat="server" />
                                                        <span class="text">SM Ready  &nbsp;</span>
                                                    </label>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="display: none">
                                            <div class="form-group col-md-6" id="divinstaller" runat="server" visible="false" style="padding-top: 12px">
                                                <asp:Button ID="btnInstDetail" runat="server" Text="Installation Detail" OnClick="btnInstDetail_Click" CssClass="btninstallation" />
                                            </div>
                                            <div class="form-group col-md-6 " runat="server" id="divclickcustomer" visible="false">
                                                <div class=" checkbox-info checkbox">
                                                    <span class="fistname">
                                                        <label for="<%=chkclickcustomer.ClientID %>">
                                                            <asp:CheckBox ID="chkclickcustomer" runat="server" />
                                                            <span class="text">Click Customer  &nbsp;</span>
                                                        </label>
                                                    </span>
                                                    <asp:Button class="btn btn-primary savewhiteicon" ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />

                                                </div>
                                                <%-- <label>Click Customer</label>
                                                <asp:CheckBox ID="chkclickcustomer" runat="server" />
                                                <label for="<%=chkclickcustomer.ClientID %>">
                                                    <span></span>
                                                </label>--%>
                                            </div>
                                        </div>

                                        <%--<div class="form-group checkareanew" runat="server" id="divcancelproject" visible="false">
                                                <label>Project Cancel</label>
                                                <asp:CheckBox ID="chkprojectcancel" runat="server" />
                                                <label for="<%=chkprojectcancel.ClientID %>">
                                                    <span></span>
                                                </label>
                                                <div class="clear"></div>
                                            </div>--%>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group" id="divFollowUpNote" runat="server" visible="false" style="margin-top: 0px;">
                                            <span class="name">
                                                <label class="control-label">
                                                    Follow Up
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtFollowUpNote" runat="server" CssClass="form-control textareabox" Height="110px"
                                                    TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>

                                        <asp:Panel runat="server" ID="oldsystemdetaildiv" Visible="false">
                                            <div class="form-group">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Old System Details
                                                    </label>
                                                    <br />
                                                </span><span class="with44">
                                                    <asp:TextBox ID="txtoldsystemdetails" CssClass="form-control" runat="server" Width="100%"
                                                        Height="118px" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                        ValidationGroup="projectdetail" ControlToValidate="txtoldsystemdetails" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <asp:ImageButton ID="btnCustAddress" Style="margin-top: 5px; display: none" runat="server" OnClick="btnCustAddress_Click"
                                    ImageUrl="~/images/user_cust_address_btn1.png" CausesValidation="false" />

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dividerLine"></div>
                            </div>
                            <div class="col-md-12 textRight">
                                <asp:Button class="btn largeButton greenBtn savewhiteicon savewhiteicon btnsaveicon" ID="btnUpdateDetail" runat="server" OnClick="btnUpdateDetail_Click"
                                    Text="Save" CausesValidation="true" ValidationGroup="projectdetail" />
                            </div>

                        </div>
                        </span>
                    </asp:Panel>


                </div>
            </div>

        </section>
        <%---------Upload quote patyment--------%>
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDetails" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
            CancelControlID="lnkcancel">
        </cc1:ModalPopupExtender>
        <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line printorder "></div>
                    <div class="modal-header printorder">
                        <div class="modalHead">
                            <h4 class="modal-title" id="myModalLabel"><i class="pe-7s-news-paper popupIcon"></i>Add Document</h4>
                            <div>
                                <asp:LinkButton ID="lnkcancel" runat="server" type="button" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal">Close
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="modal-body paddnone" runat="server" id="divdetail">
                        <div class="formainline formGrid">
                            <div class="">
                                <span class="name disblock">
                                    <label class="control-label">
                                    </label>
                                </span>
                                <div class="">


                                    <div class="spicaldivin " id="div2" runat="server">
                                        <div class="">
                                            <div class="row marginbtm15">

                                                <div class="col-md-6">
                                                    <asp:Label ID="Label40" runat="server" class="control-label">Payment Amount</asp:Label>
                                                    <asp:TextBox ID="txtpayamt" runat="server" MaxLength="30" CssClass="form-control"
                                                        placeholder="Payment Amount"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtpayamt" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblLastName" class="control-label" runat="server">Payment Method</asp:Label>
                                                        <div class="marginbtm15">
                                                            <asp:DropDownList ID="ddlpaymentmethod" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Bank Transfer</asp:ListItem>
                                                                <asp:ListItem Value="2">Credit/Debit Card</asp:ListItem>
                                                                <asp:ListItem Value="3">Cash</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <br />
                                                            <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="ddlpaymentmethod" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>
                                                            --%>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <asp:Label ID="Label5" runat="server" class="control-label">Payment Receipt</asp:Label>
                                                    <asp:TextBox ID="txtpayamtreceipt" runat="server" MaxLength="30" CssClass="form-control"
                                                        placeholder="Payment Receipt"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="txtpayamtreceipt" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="formainline">

                                                        <div class="topfileuploadbox marbtm15">

                                                            <div>
                                                                <div style="word-wrap: break-word;">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Upload Payment Doc <span class="symbol required"></span>
                                                                        </label>
                                                                        <label class="custom-fileupload">

                                                                            <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                            <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="FileUpload1" ClientValidationFunction="getsubcat();" />--%>
                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfiledetaipayment();" />
                                                                            <span class="custom-file-control form-control-file form-control"></span>
                                                                            <span class="btnbox">Upload file</span>

                                                                            <div class="clear">
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" CssClass=""
                                                                                ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf"></asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf|.jpeg|.jpg|.PDF|.JPG|.JPEG)$" Style="color: red;"
                                                                                Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                            <div class="clear">
                                                                            </div>

                                                                        </label>
                                                                        <div class="clear">
                                                                        </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <%--<div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>--%>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="">
                                    <div class="dividerLine"></div>
                                </div>
                                <div class="textRight">

                                    <asp:Button CssClass="btn largebutton greenBtn redreq btnaddicon" ID="button1" runat="server" OnClick="button1_Click"
                                        Text="Add" ValidationGroup="uploadpdf" />
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
        <!------------------------------------ View Installer Notes ------------------------------------>
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelNotes" DropShadow="false" PopupControlID="divAddNotes" TargetControlID="btnNULLNotes">
        </cc1:ModalPopupExtender>
        <div id="divAddNotes" runat="server" style="display: none" class="modal_popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="ibtnCancelNotes" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h3 class="modal-title" id="myModalLabel">
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            Installer Notes</h3>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div id="divInstallgrid" runat="server">
                                <div class="table-responsive tableblack tableminpadd finalgrid">
                                    <%--<asp:GridView ID="GridView1" DataKeyNames="ProjectID"
                                        runat="server" PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                <ItemTemplate>
                                                    <%#Eval("Notes")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                <ItemTemplate>
                                                    <%#Eval("employeename")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130px" HeaderStyle-CssClass="brdrgrayright">
                                                <ItemTemplate>
                                                    <%#Eval("Date","{0:dd MMM yyyy}")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>--%>

                                    <asp:GridView ID="GridView"
                                        runat="server" PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" AutoGenerateColumns="false">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                <ItemTemplate>
                                                    <%#Eval("employeename")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULLNotes" Style="display: none;" runat="server" />
        <!---------------------------------------------------------------------------------------------->

        <!----------------------------------- Update Installer Detail ---------------------------------->
        <cc1:ModalPopupExtender ID="ModalPopupExtenderInst" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelInstDetail" DropShadow="false" PopupControlID="divInstDetail"
            TargetControlID="btnNULLInstDetail">
        </cc1:ModalPopupExtender>


        <div id="divInstDetail" runat="server" class="modal_popup" align="center" style="width: auto;">

            <div class="modal-dialog " style="width: 400px;">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <div style="float: right">
                            <asp:LinkButton ID="ibtnCancelInstDetail" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                            </asp:LinkButton>
                        </div>
                        <h3 class="modal-title" id="H2">Update Installation Detail</h3>
                        <%--  <h4 class="modal-title" id="H1">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                      </h4>--%>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div style="width: 215px; margin: 0px auto; border: 0px solid #ccc;">

                                <div class="form-group checkareanew">

                                    <div class="name left-text form-group">
                                        <span></span>
                                        <div class=" checkbox-info checkbox">
                                            <span class="fistname">
                                                <label for="<%=chkElecDistOK.ClientID %>" class="control-label">
                                                    <asp:CheckBox ID="chkElecDistOK" runat="server" />
                                                    <span class="text">Elec Dist OK  &nbsp;</span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                    <div class="form-group" id="divElecDistApproved" runat="server">
                                        <span class="name">
                                            <asp:Label ID="Label23" runat="server" class="control-label">
                                            Elec Dist Approved</asp:Label></span>
                                        <div class="input-group date datetimepicker1">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtElecDistApproved" runat="server" class="form-control" Width="175px">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage=""
                                                Visible="false" ValidationGroup="instdetail" ControlToValidate="txtElecDistApproved"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <span class="name left-text">
                                            <label class="control-label">
                                                Installer
                                            </label>
                                        </span>
                                        <div style="text-align: left;">
                                            <div style="width: 210px;">
                                                <div class="drpValidate">
                                                    <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" Width="200px" CssClass="myval">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ValidationGroup="instdetail"
                                                        ControlToValidate="ddlInstaller" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <br />
                                        <div class="form-group center-text">
                                            <span class="name"></span><span class="dateimg">
                                                <asp:Button ID="ibtnUpdateDetail" runat="server" Text="Update" OnClick="ibtnUpdateDetail_Click"
                                                    CssClass="btn btn-primary btnsaveicon" ValidationGroup="instdetail" />
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <asp:Button ID="btnNULLInstDetail" Style="display: none;" runat="server" />
        <!---------------------------------------------------------------------------------------------->


        <asp:Button ID="Button2" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
            OkControlID="btnOKAddress" TargetControlID="Button2">
        </cc1:ModalPopupExtender>
        <div id="divAddressCheck" runat="server" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                        </div>
                        <h4 class="modal-title" id="H4">Duplicate Address</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                <tbody>
                                    <tr align="center">
                                        <td>
                                            <h4 class="noline" style="color: black"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                                This looks like a Duplicate Entry.</b></h4>
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <td>
                                            <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="tablescrolldiv">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server"
                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Customers">
                                                <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="loaderPopUP">
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadeddetail);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    //if (args.get_error() != undefined) {
                    //    args.set_errorhandled(true);
                    //}

                }
                function pageLoadeddetail() {


                    //gridviewScroll();
                    $('#<%=btnUpdateDetail.ClientID %>').click(function () {
                        formValidate();

                    });
                    $('#<%=ibtnUpdateDetail.ClientID %>').click(function () {
                        formValidate();
                    });
                    ////alert($(".search-select").attr("class"));
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    //$("[data-toggle=tooltip]").tooltip();
                    //$("searchbar").attr("imgbtn");
                    //gridviewScroll();
                    //=============Street 

                    $('.custom-file-input').on('change', function (e) {
                        var fileName = e.target.files[0].name;
                        //var fileName = document.getElementsByClassName("fileupload").files[0].name;
                        //alert(fileName);
                        $(this).next('.form-control-file').addClass("selected").html(fileName);
                    });
                }
                function address() {

                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddvalpro").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddvalpro").val("0");
                            }
                        }
                    });
                }
                //=================Street



                function validatestreetAddress(source, args) {
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
                    $.ajax({
                        type: "POST",
                        //action:"continue.aspx",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                //document.getElementById("Divvalidstreetname").style.display = "block";
                                //document.getElementById("DivInvalidstreetname").style.display = "none";
                                $("#chkaddvalpro").val("1");
                            }
                            else {
                                //document.getElementById("Divvalidstreetname").style.display = "none";
                                //document.getElementById("DivInvalidstreetname").style.display = "block";
                                $("#chkaddvalpro").val("0");
                            }
                        }
                    });
                    var mydataval = $("#chkaddvalpro").val();
                    if (mydataval == "1") {
                        // args.IsValid = true;
                    }
                    else {
                        //args.IsValid = false;
                    }
                }
            </script>
        </div>
    </ContentTemplate>
    <Triggers>

        <%--<asp:PostBackTrigger ControlID="button1" />--%>
        <asp:PostBackTrigger ControlID="ddlRoofType" />
        <%--<asp:PostBackTrigger ControlID="btnUpdateDetail" />--%>
    </Triggers>
</asp:UpdatePanel>






