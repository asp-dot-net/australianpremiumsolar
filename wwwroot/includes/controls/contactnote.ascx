<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contactnote.ascx.cs" Inherits="includes_controls_contactnote" %>


<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        HighlightControlToValidate();
        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidate(); 
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
         
    });
    $(window).load(function () {
        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidate();
        });
    });
    function doMyAction() {
        loader();
        $(".myval").select2({
           // placeholder: "select",
            allowclear: true
        });
        HighlightControlToValidate();
        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidate();
        });
    }
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#b94a48");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<section class="row m-b-md" id="pancontactnote" runat="server">
    <div class="col-sm-12">
        <div class="contactsarea">
            <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Contact Note
                            </div>
                            <div class="lightgraybgarea">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Note 
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="This value is required."  CssClass="comperror"
                                                        ControlToValidate="txtnote" Display="Dynamic" ValidationGroup="contactnote"></asp:RequiredFieldValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <span class="name">&nbsp;</span><span>
                                                    <asp:Button class="btn btn-primary addwhiteicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                        Text="Add" ValidationGroup="contactnote" />
                                                    <asp:Button class="btn btn-purple resetbutton" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                        CausesValidation="false" Text="Reset" />
                                                    <asp:Button class="btn btn-dark-grey calcelwhiteicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                        CausesValidation="false" Text="Cancel" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="contacttoparea paddleftright10 row" id="PanSearch" runat="server">
                <div class="leftarea1">
                    <div class="showdata ">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>Show</td>
                                <td>
                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                        aria-controls="DataTables_Table_0" class="myval1">
                                    </asp:DropDownList></td>
                                <td>entries</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <div id="divrightbtn" runat="server" style="text-align: right;">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" class="btn btn-default btn-rounded addcontact"
                            OnClick="lnkAdd_Click"><i></i>Add</asp:LinkButton>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                </div>
                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in This view</strong>
                </div>
            </div>
            <div class="contactbottomarea">
                <div class="tableblack">
                    <div class="table-responsive" id="PanGrid" runat="server">
                        <asp:GridView ID="GridView1" DataKeyNames="ContNoteID" runat="server" AllowPaging="true"
                            PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" PageSize="10"
                            OnRowDeleting="GridView1_RowDeleting" OnPageIndexChanging="GridView1_PageIndexChanging"
                            AllowSorting="true" class="table table-bordered table-hover" OnSorting="GridView1_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" SortExpression="NoteSetname">
                                    <ItemTemplate>
                                        <%#Eval("ContNoteDate", "{0:dd MMM, yyyy}")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px" SortExpression="NoteSetname">
                                    <ItemTemplate>
                                        <%#Eval("NoteSetname")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="For" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="300px" SortExpression="employeename">
                                    <ItemTemplate>
                                        <%#Eval("employeename")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Note" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#Eval("ContNote")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="gvbtnDelete" runat="server" 
                                            CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("ContNoteID")%>'
                                            Visible='<%# Eval("usercheck").ToString() == "1" || Eval("ContNoteDone").ToString() == "True" ? false : true %>'>
                                            <img src="<%=SiteURL%>/admin/images/icons/icon_delet.png"/></asp:LinkButton>
                                         
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" HorizontalAlign="Center" CssClass="verticaaline" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            var popup = $find('<%= newmodalpopup.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            var popup = $find('<%= newmodalpopup.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
           

        }
        function pageLoaded() {
            //gridviewScroll();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            ////alert($(".search-select").attr("class"));
            //$(".myval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$("[data-toggle=tooltip]").tooltip();
            //$("searchbar").attr("imgbtn");
            //gridviewScroll();
        }
    </script>
   
    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
        PopupControlID="updateprogress1"   />
</div>

 <!--Danger Modal Templates-->
<asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
            <!--End Danger Modal Templates-->