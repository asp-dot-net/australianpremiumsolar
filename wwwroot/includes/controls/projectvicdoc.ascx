<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectvicdoc.ascx.cs" Inherits="includes_controls_projectvicdoc" %>
<%@ Register Src="../InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script>
    //var prm = Sys.WebForms.PageRequestManager.getInstance();
    //prm.add_endRequest(endrequesthandler);
    //function endrequesthandler(sender, args) {
    //    $('input[type=file]').bootstrapFileInput();
    //}
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="panelvicdoc" runat="server">
            <section class="row m-b-md">
                <div class="col-sm-12 minhegihtarea">
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="contactsarea">
                            <div class="salepage">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">Download Documents</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        STC From
                                                                    </label>
                                                                </span><span class="dateimg">

                                                                    <asp:ImageButton ID="btnCreateSTCForm" runat="server" ImageUrl="../../admin/images/btn_create_stc_form.png"
                                                                        CausesValidation="false" OnClick="btnCreateSTCForm_Click" />
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Solar Statement
                                                                    </label>
                                                                </span><span class="dateimg">

                                                                    <asp:ImageButton ID="btnsolarsystem" runat="server" ImageUrl="../../admin/images/btn_downloadred.jpg"
                                                                        CausesValidation="false" OnClick="btnsolarsystem_Click" />
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Welcome Letter
                                                                    </label>
                                                                </span>
                                                                <asp:LinkButton ID="btnWelcomeDocs" Style="width: 163px" runat="server" OnClick="btnWelcomeDocs_Click" CausesValidation="false" class="btn btn-labeled btn-primary">
                                        <i class="btn-label fa fa-info"></i>Welcome Letter 
                                                                </asp:LinkButton>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Comp. Certificate
                                                                    </label>
                                                                </span><span class="dateimg">
                                                                    <a id="ewrdowload" runat="server" download>
                                                                        <image src="/admin/images/btn_downloadgreen.jpg"></image>
                                                                    </a>
                                                                    <%--       <asp:HyperLink ID="lblST" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                    <asp:ImageButton ID="btnewr" runat="server" CausesValidation="false" OnClick="EWR_Click" 
                                                                        ImageUrl="../../admin/images/btn_downloadgreen.jpg" />--%>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Payment Receipt
                                                                    </label>
                                                                </span><span class="dateimg">

                                                                    <asp:ImageButton ID="btnpaymentreceipt" runat="server" ImageUrl="../../admin/images/btn_download.png"
                                                                        CausesValidation="false" OnClick="btnPrintReceipt_Click" />
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        EWR
                                                                    </label>
                                                                </span><span class="dateimg">
                                                  <%--                  <a id="A1" runat="server" download>
                                                                        <image src="/admin/images/btn_downloadgreen.jpg"></image>
                                                                    </a>--%>
                                                                           <%--<asp:HyperLink ID="lblST" runat="server" Target="_blank" Visible="false"></asp:HyperLink>--%>
                                                                    <asp:ImageButton ID="btncerti" runat="server" CausesValidation="false" 
                                                                        ImageUrl="../../admin/images/btn_download.png" Onclick="btncerti_Click" />
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                        <div>
                                            <div class="widget flat radius-bordered borderone">
                                                <div class="widget-header bordered-bottom bordered-blue">
                                                    <span class="widget-caption">Upload Documents</span>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <%-- <input type="file" title="Search for a file to add" class="btn-primary btn">   --%>
                                                            <div class="form-group dateimgarea fileuploadmain">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Upload EWR
                                                                    </label>
                                                                </span>
                                                                <span class="file-input btn btn-azure btn-file">
                                                                    <asp:FileUpload ID="fuST" runat="server" />
                                                                </span>
                                                                </span>
                                                                <asp:RequiredFieldValidator runat="server" ID="ewrdocreq" ControlToValidate="fuST" ForeColor="Red" ErrorMessage="Please  upload file."
                                                                    Display="Dynamic" ValidationGroup="a1">

                                                                </asp:RequiredFieldValidator>
                                                                <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnAdd" runat="server"
                                                                    Text="Add" ValidationGroup="a1" OnClick="btnAdd_Click" />
                                                                <%-- <a ID="hypewr" runat="server" onclick="hypewr_click"></a>--%>
                                                                <asp:LinkButton runat="server" ID="hypewr" OnClick="hypewr_Click"></asp:LinkButton>

                                                                <div class="clear">
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </section>
        </asp:Panel>

        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>


    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCreateSTCForm" />
        <asp:PostBackTrigger ControlID="btnWelcomeDocs" />
        <asp:PostBackTrigger ControlID="btnpaymentreceipt" />
        <asp:PostBackTrigger ControlID="btnsolarsystem" />
        <asp:PostBackTrigger ControlID="btnAdd" />
        <asp:PostBackTrigger ControlID="btncerti" />
        <asp:PostBackTrigger ControlID="hypewr" />
    </Triggers>
</asp:UpdatePanel>
