﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="companysummary.ascx.cs"
    Inherits="includes_companysummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    

<div class="panel-body paddnoneall formGrid customerDetail customerPage">
    <div class="row">
        <div class="col-md-6 leftSidePage">
            <div class="companyFormBody boxPadding">
            <div class="row">
                

                <div class="col-md-4 form-group">
                    <label class="control-label"> First Name</label>
                        <asp:Label ID="lblFirstName" runat="server" class="form-control"></asp:Label>
                       
                </div>

                <div class="col-md-4 form-group">
                    <label class="control-label"> Middle Name</label>
                        <asp:Label ID="lblmiddlename" runat="server" class="form-control"></asp:Label>
                       
                </div>

                <div class="col-md-4 form-group">
                    <label class="control-label"> Last Name</label>
                         <asp:Label ID="lblLastName" runat="server" class="form-control"></asp:Label></span>
                </div>

                <div class="col-md-12 form-group">
                    <label class="control-label">Customer</label>
                    <asp:Label ID="lblCustomer" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Mobile</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>
                        <asp:Label ID="lblMobile" runat="server" class="form-control"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Phone</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                        <asp:Label ID="lblPhone" runat="server" class="form-control"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Email</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                        <asp:Label ID="lblEmail" runat="server" class="form-control"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Alt Phone</label>
                    <div class="input-group">
                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                        <asp:Label ID="lblAltPhone" runat="server" class="form-control"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Type</label>
                    <span class="form-control"><asp:Label ID="lblType" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;(<asp:Label ID="lblRes" runat="server"></asp:Label>)</span>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Source</label>
                    <asp:Label ID="lblSource" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-12 form-group">
                    <label class="control-label">Notes</label>
                    <asp:Label ID="lblNotes" runat="server" class="form-control" style="height:12.45rem;"></asp:Label>
                </div>
            </div>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-striped table-hover table table-bordered">
                <tr id="trD2DEmployee" runat="server" visible="false">
                    <td width="200px" valign="top">Out Door Employee
                    </td>
                    <td>
                        <asp:Label ID="lblD2DEmployee" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trD2DAppDate" runat="server" visible="false">
                    <td width="200px" valign="top">Appointment Date
                    </td>
                    <td>
                        <asp:Label ID="lblD2DAppDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trD2DAppTime" runat="server" visible="false">
                    <td width="200px" valign="top">Appointment Time
                    </td>
                    <td>
                        <asp:Label ID="lblD2DAppTime" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none;">
                    <td width="200px" valign="top">CompanyNumber
                    </td>
                    <td>
                        <asp:Label ID="lblCompanyNumber" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trSubSource" runat="server" visible="false">
                    <td width="200px" valign="top">SubSource
                    </td>
                    <td>
                        <asp:Label ID="lblSubSource" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 rightSidePage">
            <div class="companyFormBody boxPadding">
            <div class="row">
                <%--<div class="col-md-12 form-group">
                    <label class="control-label">Address</label>
                    <asp:Label ID="lblStreetAddress" runat="server" class="form-control"></asp:Label>
                </div>--%>

                <div class="col-md-6 form-group">
                    <label class="control-label">Address 1</label>
                    <asp:Label ID="lblAddress1" runat="server" class="form-control"></asp:Label>
                </div>

                <div class="col-md-6 form-group">
                    <label class="control-label">Address 2</label>
                    <asp:Label ID="lblAddress2" runat="server" class="form-control"></asp:Label>
                </div>

                <div class="col-md-6 form-group">
                    <label class="control-label">State</label>
                    <asp:Label ID="lblStreetState" runat="server" class="form-control"></asp:Label>
                </div>

                <div class="col-md-6 form-group" >
                    <label class="control-label">District</label>
                    <asp:Label ID="lbldistrict" runat="server" class="form-control"></asp:Label>
                </div>

                <div class="col-md-4 form-group">
                    <label class="control-label">Taluka</label>
                    <asp:Label ID="lbltaluka" runat="server" class="form-control"></asp:Label>
                </div>
                
                <div class="col-md-4 form-group">
                    <label class="control-label">City</label>
                    <asp:Label ID="lblStreetCity" runat="server" class="form-control"></asp:Label>
                </div>
                 <div class="col-md-4 form-group" style="display: none">
                    <label class="control-label">Area</label>
                    <asp:Label ID="lblStreetArea" runat="server" class="form-control"></asp:Label>
                </div>
                
                <div class="col-md-4 form-group">
                    <label class="control-label">PinCode</label>
                    <asp:Label ID="lblStreetPostCode" runat="server" class="form-control"></asp:Label>
                </div>

                <div class="col-md-4 form-group">
                    <label class="control-label">Kilo Watt</label>
                    <asp:Label ID="lblkilo" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-4 form-group">
                    <label class="control-label">Latitude</label>
                    <asp:Label ID="lbllat" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-4 form-group">
                    <label class="control-label">Longitude</label>
                    <asp:Label ID="lbllong" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Average Monthly Unit Consumed</label>
                    <asp:Label ID="lblamuc" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Average Monthly Bill</label>
                    <asp:Label ID="lblamb" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-4 form-group">
                    <label class="control-label">Area Type</label>
                    <asp:Label ID="lblareatype" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-4 form-group">
                    <label class="control-label">Heigth of Structure</label>
                    <asp:Label ID="lblrooftoparea" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-4 form-group">
                    <label class="control-label">Premises</label>
                    <asp:Label ID="lblpremises" runat="server" class="form-control"></asp:Label>
                </div>

               
                <div class="col-md-6 form-group">
                    <label class="control-label">Common Meter Connection</label>
                    <asp:Label ID="lblcmc" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label">Channels Partners </label>
                    <asp:Label ID="lblchnpart" runat="server" class="form-control"></asp:Label>
                </div>
                 <div class="col-md-6 form-group dispnone">
                    <label class="control-label">Area</label>
                    <asp:Label ID="lblArea" runat="server" class="form-control"></asp:Label>
                </div>
                <div class="col-md-12 form-group">
                    <label class="control-label">Country</label>
                    <asp:Label ID="lblCountry" runat="server" class="form-control"></asp:Label>
                </div>
            </div>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-striped table-hover table table-bordered">

                <tr style="display:none">
                    <td width="200px" valign="top">Postal Address
                    </td>
                    <td>
                        <asp:Label ID="lblPostalAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none">
                    <td width="200px" valign="top">Postal City
                    </td>
                    <td>
                        <asp:Label ID="lblPostalCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none">
                    <td width="200px" valign="top">Postal State
                    </td>
                    <td>
                        <asp:Label ID="lblPostalState" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none">
                    <td width="200px" valign="top">Postal PostCode
                    </td>
                    <td>
                        <asp:Label ID="lblPostalPostCode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td width="200px" valign="top">Website
                    </td>
                    <td>
                        <asp:Label ID="lblWebsite" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td width="200px" valign="top">Fax
                    </td>
                    <td>
                        <asp:Label ID="lblFax" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
</ContentTemplate>
</asp:UpdatePanel>


 <div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
          
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
           
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            
        }
    </script>
   <%-- <asp:UpdateProgress ID="updateprogress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
        <div class="loading-container">
        <div class="loader"></div>
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress2"
        PopupControlID="updateprogress2" BackgroundCssClass="modalPopup" />--%>
</div>



<%--<script type="text/javascript">
    $(document).ready(function () {
        loader();
    });
    function loader() {
        $(window).load(function () {
        });
    }
</script>--%>
