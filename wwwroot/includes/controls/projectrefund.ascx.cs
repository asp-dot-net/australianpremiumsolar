﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_controls_projectrefund : System.Web.UI.UserControl
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            BindGrid(0);
           // BindProjectRefund();
        }
    }

    public void BindProjectRefund()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            if ((Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("Accountant")))
            {
                SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                if (st2.ProjectStatusID == "3")
                {
                    //btnUpdateRefund.Visible = false;
                }
            }

            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
              //  btnUpdateRefund.Visible = false;
            }


            SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByProjectID(ProjectID);
            if (st.Amount != string.Empty && st.OptionID != string.Empty)
            {
              
                // TextBox1.Text = SiteConfiguration.ChangeCurrencyVal(st.Amount);
             
              //  ddlOption.SelectedValue = st.OptionID;
                //txtNotes.Text = st.Notes;
                //txtAccName.Text = st.AccName;
                //txtBSBNo.Text = st.BSBNo;
                //txtAccNo.Text = st.AccNo;
            }
            else
            {
                TextBox1.Enabled = true;
                ddlOption.Enabled = true;
            }

            DataTable dt = ClstblProjectRefund.tblProjectRefund_SelectByPID(ProjectID);
            //Response.Write(dt.Rows.Count);
            //Response.End();
            if (dt.Rows.Count > 0)
            {
                // divAdd.Visible = true;
                btnUpdateRefund.Visible = true;
            }
            else
            {
                //divAdd.Visible = false;
                btnUpdateRefund.Visible = false;
            }

            BindDropDown();
            //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            //ddlSelectRecords.DataBind();


            BindGrid(0);
            //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            //ddlSelectRecords.DataBind();
        }
        BindScript();
    }
    public void BindDropDown()

    {
        if(ddlOption.SelectedValue == string.Empty)
        {
            ListItem item2 = new ListItem();
            item2.Text = "Select";
            item2.Value = "";
            ddlOption.Items.Clear();
            ddlOption.Items.Add(item2);

            ddlOption.DataSource = ClstblRefundOptions.tblRefundOptions_SelectActice();
            ddlOption.DataValueField = "OptionID";
            ddlOption.DataMember = "OptionName";
            ddlOption.DataTextField = "OptionName";
            ddlOption.DataBind();
        }
       
    }
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            //Response.Write(Request.QueryString["proid"]);
            // Response.End();
            dt = ClstblProjectRefund.tblProjectRefund_SelectByPID(ProjectID);
        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        btnsaveRefund.Visible = false;
        btnUpdateRefund.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        // Response.Write(dt.Rows.Count);
        // Response.End();
        if (dt.Rows.Count == 0)
        {
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            btnUpdateRefund.Visible = true;
            PanGrid.Visible = true;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //PanNoRecord.Visible = false;

            //if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            //{
            //    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            //    {
            //        //========label Hide
            //        divnopage.Visible = false;
            //    }
            //    else
            //    {
            //        divnopage.Visible = true;
            //        int iTotalRecords = dv.ToTable().Rows.Count;
            //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            //        if (iEndRecord > iTotalRecords)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        if (iStartsRecods == 0)
            //        {
            //            iStartsRecods = 1;
            //        }
            //        if (iEndRecord == 0)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            //    }
            //}
        }
    }
    //public void BindGrid()
    //{
    //    DataTable dt = ClstblProjectRefund.tblProjectRefund_SelectByPID(Request.QueryString["proid"]);
    //    if (dt.Rows.Count > 0)
    //    {
    //        GridView1.DataSource = dt;
    //        GridView1.DataBind();

    //        if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
    //        {
    //            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
    //            {
    //                //========label Hide
    //                divnopage.Visible = false;
    //                PanGrid.Attributes.Add("class", "table-responsive");
    //            }
    //            else
    //            {
    //                PanGrid.Attributes.Add("class", "table-responsive datahidden");
    //                divnopage.Visible = true;
    //                int iTotalRecords = dv.ToTable().Rows.Count;
    //                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //                if (iEndRecord > iTotalRecords)
    //                {
    //                    iEndRecord = iTotalRecords;
    //                }
    //                if (iStartsRecods == 0)
    //                {
    //                    iStartsRecods = 1;
    //                }
    //                if (iEndRecord == 0)
    //                {
    //                    iEndRecord = iTotalRecords;
    //                }
    //                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //            }
    //        }
    //    }

    //    //dv = new DataView(dt);
    //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>alert(" + dt.Rows.Count + ");</script>");
    //    //if (dt.Rows.Count == 0)
    //    //{
    //    //    PanGrid.Visible = false;
    //    //    divnopage.Visible = false;
    //    //}
    //    //else
    //    //{
    //    //    GridView1.DataSource = dt;
    //    //    GridView1.DataBind();
    //    //}
    //}
    protected void btnUpdateRefund_Click(object sender, EventArgs e)
    {
        //Response.Write(ddlOption.SelectedValue);
        //Response.End();
        //Response.Write(TextBox1.Text +"==="+ TextBox1.Text);
        //Response.End();
        string ProjectID = Request.QueryString["proid"];
        string UserID = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string OptionID = ddlOption.SelectedValue;

        if (TextBox1.Text != string.Empty || ddlOption.SelectedValue != "")
        {
            int success = ClstblProjectRefund.tblProjectRefund_Insert(ProjectID, ddlOption.SelectedValue, TextBox1.Text.Trim(), UserID, txtNotes.Text, txtAccName.Text, txtBSBNo.Text, txtAccNo.Text, txtbankname.Text);
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            ClstblProjectRefund.tblProjectRefund_UpdateProjectStatusID(success.ToString(), st2.ProjectStatusID);
            if (Convert.ToString(success) != string.Empty)
            {
                SetAdd1();
                //PanSuccess.Visible = true;
                BindProjectRefund();

            }
            else
            {
                SetExist();
                //PanAlreadExists.Visible = true;

            }
        }
        //BindGrid(0);
        
        TextBox1.Text = string.Empty;
        txtAccName.Text = string.Empty;
        txtBSBNo.Text = string.Empty;
        txtAccNo.Text = string.Empty;
        txtNotes.Text = string.Empty;
        txtbankname.Text = string.Empty;
        ddlOption.SelectedValue = "";
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    //protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
    //    {
    //        GridView1.AllowPaging = false;
    //    }
    //    else
    //    {
    //        GridView1.AllowPaging = true;
    //        GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
    //    }
    //    BindGrid(0);
    //}

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdateRefund, this.GetType(), "MyAction", "doMyActionref();", true);

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "print")
        {

            string RefundId = e.CommandArgument.ToString();
            SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundId);
            Telerik_reports.generate_refundform(st.ProjectID, RefundId);
            //Response.Write(id);
            //Response.End();
            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/refundprint.aspx?id=" + id, txtWriter);
            //divPrint.InnerHtml = txtWriter.ToString();
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>printContent();</script>");
        }
        //if (e.CommandName == "Delete")
        //{
        //    string id = e.CommandArgument.ToString();
        //ClstblProjectRefund.tblProjectRefund_Delete(id);
        //    BindGrid(0);
        //}
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }
    public void SetDelete()
    {
        Reset();
        //HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetError()
    {
        Reset();
        //HidePanels();
        //SetExist();
        SetError1();
        //PanAlreadExists.Visible = true;

    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        ddlOption.SelectedValue = "";
        txtAccName.Text = string.Empty;
        TextBox1.Text = string.Empty;
        txtBSBNo.Text = string.Empty;
        txtAccNo.Text = string.Empty;
        txtNotes.Text = string.Empty;
        txtbankname.Text = string.Empty;
        //btnUpdateRefund.Visible = true;
    }
    public void SetUpdate()
    {
        Reset();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(id);

        ddlOption.SelectedValue = st.OptionID;
        txtAccName.Text = st.AccName;
        TextBox1.Text = st.Amount;
        txtBSBNo.Text = st.BSBNo;
        txtAccNo.Text = st.AccNo;
        txtNotes.Text = st.Notes;
        txtbankname.Text = st.BankName;
        divAdd.Visible = true;
        btnsaveRefund.Visible = true;
        btnUpdateRefund.Visible = false;
    }
    protected void btnsaveRefund_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string Optionid = ddlOption.SelectedValue;
        string AccName = txtAccName.Text;
        string Amount = TextBox1.Text;
        string BSBNo = txtBSBNo.Text;

        string AccNo = txtAccNo.Text;
        string Notes = txtNotes.Text;
        string BankName = txtbankname.Text;

        bool success = ClstblProjectRefund.tblProjectRefund_UpdateBy_RefundId(id1, Optionid, AccName, Amount, BSBNo, AccNo, Notes,BankName);
        if (success)
        {
            SetUpdate();
            btnsaveRefund.Visible = false;
            btnUpdateRefund.Visible = true;
        }
        else
        {
            SetError();
        }
        BindGrid(0);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblProjectRefund.tblProjectRefund_Delete(id);

        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        BindGrid(0);

    }
}