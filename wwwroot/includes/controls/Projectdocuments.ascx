 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Projectdocuments.ascx.cs" Inherits="includes_controls_documents" %>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<style>
    .contactsarea {
        position: relative;
    }
</style>

<script>
    function Uploadfile() {
        var isresult = true;
        var uploadfiles = document.querySelector('#<%=FileUpload1.ClientID %>').files[0];
        var file = document.querySelector('#<%=FileUpload1.ClientID %>').files[0];
        var docname = $('#<%=ddlDocumentName.ClientID%>').val();
        $('#<%=hdnsize.ClientID %>').val($('#<%=ddlDocumentName.ClientID%>').val());
        var size = parseFloat(file.size / 1024);
        if (file.type == "image/jpeg" && docname == "3") {
            if (size > 200) {
                $('#Filesizeerror').show();
                $('#<%=errorLabel.ClientID %>').show();
                $('#<%=errorLabel.ClientID %>').text("File size should be less than 195 Kb.");

                $("#Filesizeerror").css('visibility', 'visible');
                $('#Filesizeerror').text = "File size should be less than 200 KB."
                alert("File size should be less than 200 KB.");
                isresult = false;
                $("#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_btnAdd").prop('disabled', true);

                return false;
            }
        }
        else if (file.type == "application/pdf" && docname == "2") {
            if (size > 195) {
                $('#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_errorLabel').show();
                $('#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_errorLabel').text = "File size should be less than 195 Kb."
                $('#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_errorLabel').val = "File size should be less than 195 Kb."

                alert("File size should be less than 195 Kb.");
                isresult = false;
                $("#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_btnAdd").prop('disabled', true);
                return false;
            }
        }

        else {
            if (size > 995) {
                //alert(docname);
                $('#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_errorLabel').show();
                $('#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_errorLabel').text = "File size should be less than 995 Kb."
                alert("File size should be less than 995 Kb.");
                isresult = false;
                $("#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_btnAdd").prop('disabled', true);

                return false;
            }
        }
        if (isresult) {
            debugger;
            $("#ctl00_ContentPlaceHolder1_TabContainer1_TabDocuments_documents1_btnAdd").prop('disabled', false);

            var uploadedfiles = uploadfiles;
            var fromdata = new FormData();
            for (var i = 0; i < uploadedfiles.length; i++) {
                fromdata.append(uploadedfiles.name, uploadedfiles);
            }
            fromdata.append(uploadedfiles.name, uploadedfiles);
            $('#<%=hdnFileName.ClientID %>').val(uploadedfiles.name);
            alert(fromdata);
            var choice = {};
            choice.url = "Handler.ashx";
            choice.type = "POST";
            choice.data = fromdata;
            choice.contentType = false;
            choice.processData = false;
            choice.success = function (result) {
            };
            choice.error = function (err) {
            };
            $.ajax(choice);
        }
    }
    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

            function (isConfirm) {
                if (isConfirm) {
                    // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    eval(defaultAction);

                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    return true;
                } else {
                    // swal("Cancelled", "Your imaginary file is safe :)", "error");
                    return false;
                }
            });


    }
</script>
<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>

        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedcontacttab);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);
            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').removeClass('loading-inactive');
            }
            function endrequesthandler(sender, args) {
                $('.loading-container').addClass('loading-inactive');
                $(".myvalcont").select2({
                    //placeholder: "select",
                    allowclear: true
                });

            }

            function funFileValidtion() {
                alert(1);
                console.log($("#ddlDocumentName :selected").val());
            }
            function pageLoadedcontacttab() {
                $('.custom-file-input').on('change', function (e) {
                    var fileName = e.target.files[0].name;
                    //var fileName = document.getElementsByClassName("fileupload").files[0].name;
                    //alert(fileName);
                    $(this).next('.form-control-file').addClass("selected").html(fileName);
                });

                $('.redreqdocument').click(function () {
                    console.log("formValidate document CAlled");
                    formValidate();
                });
            }
        </script>

        <section class="row m-b-md" id="pancontacts" runat="server">

            <div class="col-sm-12">
                <asp:HiddenField ID="hdnFileName" runat="server" />
                <asp:HiddenField ID="hdnsize" runat="server" />

                <div class="contactsarea boxPadding">

                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">

                        <div class="widget flat radius-bordered borderone">
                            <div class="widget-header bordered-bottom bordered-blue">
                                <h4 class="formHeading widget-caption">
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Documents</h4>
                            </div>
                            <div class="widget-body">
                                <div class="lightgraybgarea formGrid">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row spicaldivin " id="div1" runat="server">
                                                    <div class="col-sm-12">
                                                        <div class="row marginbtm15">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:Label ID="lblLastName" runat="server" class="control-label">Document Name</asp:Label>
                                                                    <div class="marginbtm15">
                                                                        <asp:DropDownList ID="ddlDocumentName" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <br />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                                                            ControlToValidate="ddlDocumentName" Display="Dynamic" ValidationGroup="uploadcompanypdf" InitialValue=""></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:Label ID="Label1" runat="server" class="control-label">Document Number</asp:Label>
                                                                <asp:TextBox ID="txtNumber" runat="server" MaxLength="30" CssClass="form-control"
                                                                    placeholder="Number"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtNumber" Display="Dynamic" ValidationGroup="uploadcompanypdf" InitialValue=""></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="formainline">

                                                                    <div class="topfileuploadbox">
                                                                        <div class="form-group" style="margin-bottom: 0px;">
                                                                            <div style="word-wrap: break-word;">
                                                                                <span class="name">
                                                                                    <label class="control-label">
                                                                                        Upload File <span class="symbol required"></span>
                                                                                    </label>
                                                                                    <%-- <span id="errormsg"></span>--%>

                                                                                    <label class="custom-fileupload">

                                                                                        <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                                        <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="FileUpload1" ClientValidationFunction="getsubcat();" />--%>
                                                                                        <%--<asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="return Uploadfile();" />--%>
                                                                                        <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfile();" />

                                                                                        <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                                        <span class="btnbox">Attach file</span>
                                                                                        <%--onchange="return getsubcat();--%>
                                                                                        <div class="clear">
                                                                                            <asp:Label ID="errorLabel" runat="server"></asp:Label>
                                                                                        </div>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                            ValidationGroup="uploadcompanypdf" ValidationExpression="^.+(.pdf|.jpeg|.jpg|.PDF|.JPG|.JPEG)$" Style="color: red;"
                                                                                            Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass=""
                                                                                            ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadcompanypdf"></asp:RequiredFieldValidator>

                                                                                        <div class="clear">
                                                                                        </div>

                                                                                    </label>
                                                                                </span>

                                                                                <div class="clear">
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <%--<div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>--%>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dividerLine"></div>
                                            </div>
                                            <div class="form-group col-md-12 textRight">

                                                <asp:Button CssClass="btn largeButton greenBtn btnaddicon redreqdocument" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" ValidationGroup="uploadcompanypdf" />
                                                <asp:Button CssClass="btn largeButton greenBtn savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" />
                                                <asp:Button CssClass="btn largeButton whiteBtn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn largeButton blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>

                    <div class="" id="PanSearch" runat="server">
                        <%--<div class="leftarea1">
                        <div class="showenteries">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Show</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" CssClass="myval1">
                                        </asp:DropDownList></td>
                                    <td>entries</td>
                                </tr>
                            </table>
                        </div>
                    </div>--%>
                        <div class="contacttoparea">
                            <div id="divrightbtn" runat="server" style="text-align: right;">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn largeButton blueBtn"
                                    OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="messesgarea">
                        <div class="alert alert-success" style="margin-bottom: 20px;" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                    </div>
                    <div class="contactbottomarea finalgrid documentGrid">
                        <div class="tablescrolldiv">
                            <div class="tableblack tableminpadd">
                                <div class="table-responsive" id="PanGrid" runat="server">
                                    <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" AllowSorting="true" OnSorting="GridView1_Sorting"
                                        OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="false"
                                        CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Document Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("Id") %>' />
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerId") %>' />
                                                    <%#Eval("DocumentName")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                                <ItemTemplate>
                                                    <%#Eval("Number")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Uploaded By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                                <ItemTemplate>
                                                    <%#Eval("empFullName")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Uploaded DateTime" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                                <ItemTemplate>
                                                    <%#Eval("UploadedDateTime")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document Attached" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="documentLabel">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkPDFUploded" runat="server" CommandName="PDFUploded" CommandArgument='<%#Eval("CustomerId") %>' CausesValidation="false" CssClass="btn btn-palegreen docFile"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFUploded" Visible="false">
                                                            <i class="pe-7s-copy-file"></i> Doc
                                                    </asp:HyperLink>
                                                    <asp:HyperLink ID="lnkPDFPending" runat="server" CommandName="PDFPending" CommandArgument='<%#Eval("CustomerId") %>' CausesValidation="false" CssClass="btn btn-darkorange"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFPending" Visible="false" Style="font-size: 13px!important; padding: 1px 6px;">
                                                            <i class="btn-label glyphicon glyphicon-remove" style="font-size:13px;"></i> Doc
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Actions" HeaderStyle-CssClass="documentLabel center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btngray" CausesValidation="false"
                                                        CommandName="Delete" CommandArgument='<%#Eval("Id")%>'>
                        <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>
        </section>


        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message">

            <div class="modal-dialog ">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <div class="modalHead">
                            <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-trash popupIcon"></i>Delete</h4>
                        </div>
                    </div>

                    <div class="modal-body ">
                        <div class="formainline formGrid">
                            <div class="">
                                <span class="name disblock">
                                    <label class="control-label">
                                        Are You Sure You want to Delete This File?
                                    </label>
                                </span>
                                <div class="">
                                    <div class="form-group spicaldivin " runat="server">
                                        <div class="col-sm-12">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 textRight">
                                    <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandName="deleteRow" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                    <asp:LinkButton ID="lnkcancel" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <!--End Danger Modal Templates-->
        <script type="text/javascript">

            function getsubcat() {

                var docname = $('#<%=ddlDocumentName.ClientID%>').val();
                var file = document.querySelector('#<%=FileUpload1.ClientID %>').files[0];
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function (_file) {
                    var size = parseFloat(file.size / 1024);
                    if (file.type == "image/jpeg" && docname == "3") {
                        //alert(docname);
                        if (size > 200) {
                            $('#Filesizeerror').show();
                            $('#Filesizeerror').text = "File size should be less than 200 KB."
                            //alert("File size should be less than 200 KB.");
                            $('#<%=FileUpload1.ClientID%>').val("");
                            $(".file-input-name").empty();
                            return false;
                        }
                    }
                    else if (file.type == "application/pdf" && docname == "2") {
                        if (size > 195) {
                            $('#Filesizeerror').show();
                            $('#Filesizeerror').text = "File size should be less than 195 Kb."

                            alert("File size should be less than 195 Kb.");
                            $('#<%=FileUpload1.ClientID%>').val("");
                            $(".file-input-name").empty();
                            return false;
                        }
                    }

                    else {
                        if (size > 995) {
                            $('#Filesizeerror').show();
                            $('#Filesizeerror').text = "File size should be less than 995 Kb."
                            alert("File size should be less than 995 Kb.");
                            $('#<%=FileUpload1.ClientID%>').val("");
                            $(".file-input-name").empty();
                            return false;
                        }
                    }
                };
            }
        </script>
    </ContentTemplate>
    <Triggers>
        <%--<asp:PostBackTrigger ControlID="btnAdd" />--%>
    </Triggers>
</asp:UpdatePanel>

