﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class includes_controls_projectdetail : System.Web.UI.UserControl
{
    protected static string Siteurl;
    protected static string address;

    public class GovStatus
    {
        public int id { get; set; }
        public string StatusName { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        UserControl tuc1 = (UserControl)Page.LoadControl("~/includes/controls/Projectdetail.ascx");
        lnlError.Text = string.Empty;
        if (ddlProjectTypeID.SelectedValue == "7")
        {
            Session["openMaintainace"] = "open";
            RequiredFieldValidator5.Enabled = false;
        }
        PanSuccess.Visible = false;
        PanError.Visible = false;
        string ProjectID = string.Empty;
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                string companyid = Request.QueryString["compid"];
                SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(companyid);
                string formbayadd = st.isformbayadd;


                if (st.isformbayadd != string.Empty)
                {
                    if (Convert.ToBoolean(st.isformbayadd) == true)
                    {
                        txtInstallAddressline.Enabled = true;

                        PanInstallAddress.Enabled = false;
                    }
                    else if (Convert.ToBoolean(st.isformbayadd) == false)
                    {
                        txtInstallAddressline.ReadOnly = true;
                        txtInstallAddress.Enabled = true;
                        txtformbaystreetname.Enabled = true;
                    }
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                ProjectID = Request.QueryString["proid"];

                SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                ddlRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
                ddlRoofType.DataValueField = "RoofTypeID";
                ddlRoofType.DataMember = "RoofType";
                ddlRoofType.DataTextField = "RoofType";
                ddlRoofType.DataBind();


                txtCircle.Text = st2.Circle;
                if (!string.IsNullOrEmpty(st2.SubDivisionId))
                    ddlSubDivision.SelectedValue = st2.SubDivisionId;
                txtGUVNLregi.Text = st2.GUVNL_Reg_No;


                ddlElecDist.DataSource = ClstblCustSource.tblElecDistributor_Select();
                ddlElecDist.DataValueField = "ElecDistributorID";
                ddlElecDist.DataTextField = "ElecDistributor";
                ddlElecDist.DataBind();

                ddlHouseType.DataSource = ClstblHouseType.tblHouseType_SelectASC();
                ddlHouseType.DataValueField = "HouseTypeID";
                ddlHouseType.DataMember = "HouseType";
                ddlHouseType.DataTextField = "HouseType";
                ddlHouseType.DataBind();

                ddlestimatevalue.DataSource = ClsProjectSale.tbl_EstimateValue_Select();
                ddlestimatevalue.DataValueField = "id";
                ddlestimatevalue.DataMember = "EstimateValue";
                ddlestimatevalue.DataTextField = "EstimateValue";
                ddlestimatevalue.DataBind();

                StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                Siteurl = st1.siteurl;

                if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
                {

                    if ((Roles.IsUserInRole("Sales Manager")))
                    {
                        if (st2.ProjectStatusID == "3")
                        {
                            btnUpdateDetail.Visible = false;
                            btnSave.Visible = false;
                        }
                    }
                    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Administrator"))
                    {
                        divclickcustomer.Visible = true;
                    }


                    BindDropDown();

                    ModalPopupExtenderInst.Hide();
                }
                if (Roles.IsUserInRole("Administrator"))
                {
                    ddlSalesRep.Enabled = true;
                    PanAddUpdate.Enabled = true;
                    divinstaller.Visible = true;
                }

                if (Roles.IsUserInRole("Finance"))
                {
                    PanAddUpdate.Enabled = false;
                }
                if (Roles.IsUserInRole("Accountant"))
                {
                    PanAddUpdate.Enabled = true;
                }
                if (Roles.IsUserInRole("BookInstallation"))
                {
                    PanAddUpdate.Enabled = false;
                }
                if (Roles.IsUserInRole("Customer"))
                {
                    PanAddUpdate.Enabled = false;
                }
                if (Roles.IsUserInRole("Maintenance"))
                {
                    PanAddUpdate.Enabled = true;
                }
                if (Roles.IsUserInRole("PostInstaller"))
                {
                    PanAddUpdate.Enabled = true;
                }
                if (Roles.IsUserInRole("PreInstaller"))
                {
                    PanAddUpdate.Enabled = true;
                    divinstaller.Visible = true;
                }
                if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
                {
                    ddlSalesRep.Enabled = true;
                }
                if (Roles.IsUserInRole("STC"))
                {
                    PanAddUpdate.Enabled = false;
                }
                if (Roles.IsUserInRole("Installer"))
                {
                    PanAddUpdate.Enabled = false;
                }
                if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
                {
                    if (st2.ProjectStatusID == "5")
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
                chkclickcustomer.Checked = Convert.ToBoolean(st2.IsClickCustomer);

                if (Roles.IsUserInRole("Installation Manager"))
                {
                    divinstaller.Visible = true;
                    ddlSalesRep.CssClass = "myval";
                    //CssClass is used because if we enable the dropdown false,then the class file is not applied.So,it is added from the backcode.
                }
                if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                {
                    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                    SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                    string CEmpType = stEmpC.EmpType;
                    string CSalesTeamID = "";
                    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                    if (dt_empsale.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale.Rows)
                        {
                            CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                        }
                        CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                    }

                    if (Request.QueryString["proid"] != string.Empty)
                    {
                        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
                        string EmpType = stEmp.EmpType;
                        string SalesTeam = "";
                        DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st2.EmployeeID);
                        if (dt_empsale1.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt_empsale1.Rows)
                            {
                                SalesTeam += dr["SalesTeamID"].ToString() + ",";
                            }
                            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                        }

                        if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                        {
                            PanAddUpdate.Enabled = true;
                        }
                        else
                        {
                            PanAddUpdate.Enabled = false;
                        }
                    }
                }


                if (Roles.IsUserInRole("SalesRep") && (st2.ProjectStatusID == "3" || st2.ProjectStatusID == "5"))
                {
                    PanAddUpdate.Enabled = false;
                }
                try
                {
                    ddlSalesRep.SelectedValue = st2.SalesRep;
                }
                catch { }
                try
                {
                    ddlProjectTypeID.SelectedValue = st2.ProjectTypeID;

                }
                catch { }

                try
                {
                    ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(st2.CustomerID);
                    ddlContact.DataValueField = "ContactId";
                    ddlContact.DataTextField = "Contact";
                    ddlContact.DataMember = "Contact";
                    ddlContact.DataBind();
                    ddlContact.SelectedValue = st2.CustomerID;

                }
                catch { }

                try
                {
                    txtProjectOpened.Text = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                }
                catch { }

                try
                {
                    txtOldProjectNumber.Text = st2.OldProjectNumber;
                }
                catch { }
                try
                {
                    txtManualQuoteNumber.Text = st2.ManualQuoteNumber;
                }
                catch { }
                try
                {
                    txtInstallAddressline.Text = st2.InstallAddress + ", " + st2.InstallCity + " " + st2.InstallState + " " + st2.InstallPostCode;
                }
                catch { }

                try
                {
                    txtformbayStreetNo.Text = st2.street_number;
                }
                catch { }

                try
                {
                    txtformbayNearby.Text = st2.NearBy;
                }
                catch { }
                try
                {


                }
                catch { }
                try
                {
                    txtformbaystreetname.Text = st2.street_name;
                }
                catch { }

                try
                {
                }
                catch { }
                try
                {
                    txtStreetPostCode.Text = st2.InstallPostCode;
                }
                catch { }
                try
                {
                    txtInstallAddress.Text = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
                }
                catch { }
                try
                {
                    txtProjectNotes.Text = st2.ProjectNotes;
                }
                catch { }
                try
                {
                    ddlInstaller.SelectedValue = st2.Installer;
                }
                catch { }
                try
                {
                    txtElecDistApproved.Text = Convert.ToDateTime(st2.ElecDistApproved).ToShortDateString();
                }
                catch { }
                try
                {
                    ddlHousestatys.SelectedValue = st2.HouseStayID;
                }
                catch { }
                try
                {
                    txtStartDate.Text = st2.HouseStayDate;
                }
                catch { }

                try
                {
                    txtdistributorapplicationnumber.Text = st2.ApplicationNo;
                    txtElecDistApplied.Text = st2.ApplicationDate;
                    HyperLink1.NavigateUrl = string.Format("http://b2848392e002ee33fd61-5c566850018ebfb871276c1331018ded.r57.cf2.rackcdn.com/UserfilesTesting/CustomerDocuments/" + st2.DocumentURL);
                    lblfileName.Text = st2.DocumentURL;
                }
                catch
                {

                }
                try
                {
                    if (st2.ElecDistributorID != null)
                    {
                        ddlElecDist.SelectedValue = st2.ElecDistributorID;
                        ddldivision.DataSource = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
                        ddldivision.DataValueField = "Id";
                        ddldivision.DataMember = "DivisionName";
                        ddldivision.DataTextField = "DivisionName";
                        ddldivision.DataBind();
                        ddldivision.SelectedValue = st2.DivisionId;
                    }


                    txtNMINumber.Text = st2.NMINumber;
                    txtsanction.Text = st2.LotNumber;
                    ddlnetmeter.SelectedValue = st2.NetMeterId;
                    ddlsolarinverter.SelectedValue = st2.MeterPhase;
                    txtpvcapacity.Text = st2.PVCapacity;
                    ddlHouseType.SelectedValue = st2.HouseTypeID;
                    ddlRoofType.SelectedValue = st2.RoofTypeID;
                    txtMeterEstimate.Text = st2.Meter_Estimate;
                    txtEstimatedPaid.Text = st2.Estimate_paid;
                    txtestimatepaymentdue.Text = st2.Payment_due_Date;
                    ddlestimatevalue.SelectedValue = st2.estimatevalue_id;
                    txtProjectNotes.Text = st2.ProjectNotes;
                    txtInstallerNotes.Text = st2.InstallerNotes;
                    txtMeterInstallerNotes.Text = st2.InstallerNotes;

                }
                catch
                {

                }
                if (st2.IsSMReady == "True")
                {
                    chkclickSMready.Checked = true;
                }
                DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID(st2.CustomerID);
                if (dtFollwUp.Rows.Count > 0)
                {

                    try
                    {
                        txtFollowupDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["FollowupDate"].ToString()));
                    }
                    catch { }
                    try
                    {
                        txtNextFollowUpDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));
                    }
                    catch { }
                    txtFollowUpNote.Text = dtFollwUp.Rows[0]["Description"].ToString();
                }
                else
                {

                }

                try
                {
                    txtInstallerNotes.Text = st2.InstallerNotes;
                }
                catch { }
                try
                {
                    txtMeterInstallerNotes.Text = st2.MeterInstallerNotes;
                }
                catch { }
                try
                {
                    txtoldsystemdetails.Text = st2.OldSystemDetails;
                }
                catch { }

                DataTable dtInstNotes = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
                if (dtInstNotes.Rows.Count > 0)
                {
                    btnViewNotes.Visible = true;
                }
                else
                {
                    btnViewNotes.Visible = false;
                }

                if (ddlProjectTypeID.SelectedItem.Text == "Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
                {
                    oldsystemdetaildiv.Visible = true;
                }
                else
                {
                    oldsystemdetaildiv.Visible = false;
                }
            }
        }
    }

    public void BindProjectDetail()
    {

    }
    public void binddetail()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                string companyid = Request.QueryString["compid"];
                SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(companyid);
                string formbayadd = st.isformbayadd;

                ddlStreetCity.Text = st.StreetCity;
                if (st.isformbayadd != string.Empty)
                {
                    if (Convert.ToBoolean(st.isformbayadd) == true)
                    {
                        txtInstallAddressline.Enabled = true;
                        PanInstallAddress.Enabled = false;
                    }
                    else if (Convert.ToBoolean(st.isformbayadd) == false)
                    {
                        txtInstallAddressline.ReadOnly = true;
                        txtInstallAddress.Enabled = true;
                        txtformbaystreetname.Enabled = true;
                    }
                }
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            txtCircle.Text = st2.Circle;
            if (!string.IsNullOrEmpty(st2.SubDivisionId))
                ddlSubDivision.SelectedValue = st2.SubDivisionId;
            txtGUVNLregi.Text = st2.GUVNL_Reg_No;

            if (Roles.IsUserInRole("Administrator"))
            {
                ddlSalesRep.Enabled = true;
                PanAddUpdate.Enabled = true;
                divinstaller.Visible = true;
            }

            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
                divinstaller.Visible = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                ddlSalesRep.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st2.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }



            chkclickcustomer.Checked = Convert.ToBoolean(st2.IsClickCustomer);

            if (Roles.IsUserInRole("Installation Manager"))
            {
                divinstaller.Visible = true;
                ddlSalesRep.CssClass = "myval";
                //CssClass is used because if we enable the dropdown false,then the class file is not applied.So,it is added from the backcode.
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st2.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }

            //BindDropDown();

            if (Roles.IsUserInRole("SalesRep") && (st2.ProjectStatusID == "3" || st2.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }
            try
            {
                ddlSalesRep.SelectedValue = st2.SalesRep;
            }
            catch { }
            try
            {
                ddlProjectTypeID.SelectedValue = st2.ProjectTypeID;
                ddlSalesRep.SelectedValue = st2.SalesRep;
                // if project type is Maintainanace

                HideAndShowPanel(st2.ProjectTypeID);


            }
            catch { }

            try
            {

            }
            catch { }

            try
            {
                txtProjectOpened.Text = Convert.ToDateTime(st2.ProjectOpened).ToShortDateString();
            }
            catch { }

            try
            {
                txtOldProjectNumber.Text = st2.OldProjectNumber;
            }
            catch { }
            try
            {
                txtManualQuoteNumber.Text = st2.ManualQuoteNumber;
            }
            catch { }
            try
            {
                txtInstallAddressline.Text = st2.InstallAddress + ", " + st2.InstallCity + " " + st2.InstallState + " " + st2.InstallPostCode;
            }
            catch { }

            try
            {
                txtformbayStreetNo.Text = st2.street_number;
            }
            catch { }
            try
            {

                ddlformbaystreettype.SelectedValue = st2.street_type;
            }
            catch { }
            try
            {
                txtformbayNearby.Text = st2.NearBy;
            }
            catch { }
            try
            {

            }
            catch { }
            try
            {
                txtformbaystreetname.Text = st2.street_name;
            }
            catch { }

            ddlArea.SelectedValue = st2.StreetArea;
            try
            {
                ddlStreetState.SelectedValue = st2.StreetState;
                ddlformbaystate_SelectedIndexChanged(st2.StreetState, new EventArgs());
                ddlDistrict.SelectedValue = st2.District;
                ddltaluka.Text = st2.Taluka;
                ddltaluka_SelectedIndexChanged(st2.Taluka, new EventArgs());
                ddlStreetCity.Text = st2.StreetCity;


                txtpvcapacity.Text = st2.PVCapacity;

            }
            catch { }
            try
            {
                txtStreetPostCode.Text = st2.InstallPostCode;
            }
            catch { }
            try
            {
                txtInstallAddress.Text = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
            }
            catch { }
            try
            {
                txtProjectNotes.Text = st2.ProjectNotes;
            }
            catch { }
            try
            {
                ddlInstaller.SelectedValue = st2.Installer;
            }
            catch { }
            try
            {
                txtElecDistApproved.Text = Convert.ToDateTime(st2.ElecDistApproved).ToShortDateString();
            }
            catch { }
            try
            {
                ddlHousestatys.SelectedValue = st2.HouseStayID;
            }
            catch { }
            try
            {
                txtStartDate.Text = st2.HouseStayDate;
            }
            catch { }


            if (st2.IsSMReady == "True")
            {
                chkclickSMready.Checked = true;
            }
            DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID(st2.CustomerID);
            if (dtFollwUp.Rows.Count > 0)
            {

                try
                {
                    txtFollowupDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["FollowupDate"].ToString()));
                }
                catch { }
                try
                {
                    txtNextFollowUpDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));
                }
                catch { }
                txtFollowUpNote.Text = dtFollwUp.Rows[0]["Description"].ToString();
            }
            else
            {

            }

            try
            {
                txtInstallerNotes.Text = st2.InstallerNotes;
            }
            catch { }
            try
            {
                txtMeterInstallerNotes.Text = st2.MeterInstallerNotes;
            }
            catch { }
            try
            {
                txtoldsystemdetails.Text = st2.OldSystemDetails;
            }
            catch { }

            DataTable dtInstNotes = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
            if (dtInstNotes.Rows.Count > 0)
            {
                btnViewNotes.Visible = true;
            }
            else
            {
                btnViewNotes.Visible = false;
            }


            if (ddlProjectTypeID.SelectedItem.Text == "Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
            {
                oldsystemdetaildiv.Visible = true;
            }
            else
            {
                oldsystemdetaildiv.Visible = false;
            }
        }
    }
    protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void ddltaluka_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    public void BindDropDown()

    {
        if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
        {
            string companyid = Request.QueryString["compid"];
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(companyid);

            ListItem item2 = new ListItem();
            item2.Text = "Select";
            item2.Value = "";
            ddlProjectTypeID.Items.Clear();
            ddlProjectTypeID.Items.Add(item2);

            ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
            ddlProjectTypeID.DataValueField = "ProjectTypeID";
            ddlProjectTypeID.DataTextField = "ProjectType";
            ddlProjectTypeID.DataMember = "ProjectType";
            ddlProjectTypeID.DataBind();

            ListItem item3 = new ListItem();
            item3.Text = "Installer";
            item3.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item3);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();

            ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
            ddlformbaystreettype.DataMember = "StreetType";
            ddlformbaystreettype.DataTextField = "StreetType";
            ddlformbaystreettype.DataValueField = "StreetCode";
            ddlformbaystreettype.DataBind();

            ddlStreetState.DataSource = ClstblState.tblStates_Select();
            ddlStreetState.DataMember = "FullName";
            ddlStreetState.DataTextField = "FullName";
            ddlStreetState.DataValueField = "FullName";
            ddlStreetState.DataBind();

            ddlDistrict.DataSource = ClstblState.tbl_DistrictName();
            ddlDistrict.DataMember = "DistrictName";
            ddlDistrict.DataTextField = "DistrictName";
            ddlDistrict.DataValueField = "DistrictName";
            ddlDistrict.DataBind();

            ddlArea.DataSource = ClstblState.tbl_GetAllArea();
            ddlArea.DataMember = "AreaName";
            ddlArea.DataTextField = "AreaName";
            ddlArea.DataValueField = "AreaName";
            ddlArea.DataBind();

            ddlSubDivision.DataSource = ClstblProjects.tbl_GetSubdivision();
            ddlSubDivision.DataMember = "SubDivisionName";
            ddlSubDivision.DataTextField = "SubDivisionName";
            ddlSubDivision.DataValueField = "ID";
            ddlSubDivision.DataBind();

            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (!string.IsNullOrEmpty(st2.StreetCity))
                ddlStreetCity.Text = st2.StreetCity;
            else
                ddlStreetCity.Text = st.StreetCity;

            ddlStreetState.SelectedValue = st2.StreetState;

            if (!string.IsNullOrEmpty(st2.Taluka))
                ddltaluka.Text = st2.Taluka;
            else
                ddltaluka.Text = st.Taluka;

            if (!string.IsNullOrEmpty(st2.District))
                ddlDistrict.SelectedValue = st2.District;
            else
                ddlDistrict.SelectedValue = st.District;




            ddlHousestatys.DataSource = ClstblProjects.tbl_HouseStatys_Select();
            ddlHousestatys.DataValueField = "Id";
            ddlHousestatys.DataTextField = "Name";
            ddlHousestatys.DataMember = "Name";
            ddlHousestatys.DataBind();

            ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            ddlSalesRep.DataValueField = "EmployeeID";
            ddlSalesRep.DataTextField = "fullname";
            ddlSalesRep.DataMember = "fullname";
            ddlSalesRep.DataBind();

            ddlSalesRep.SelectedValue = "159";

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }

            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                if (SalesTeam != string.Empty)
                {
                    ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                }
            }
            else
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            }
            //ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectByUserASC(userid);

            string ProjectId = Request.QueryString["proid"];
            if (ProjectId != null)
            {
                ListItem item7 = new ListItem();
                item7.Text = "Select";
                item7.Value = "";
                ddlProjectOnHoldID.Items.Clear();
                ddlProjectOnHoldID.Items.Add(item7);

                ddlProjectOnHoldID.DataSource = ClstblProjectOnHold.tblProjectOnHold_SelectASC();
                ddlProjectOnHoldID.DataValueField = "ProjectOnHoldID";
                ddlProjectOnHoldID.DataTextField = "ProjectOnHold";
                ddlProjectOnHoldID.DataMember = "ProjectOnHold";
                ddlProjectOnHoldID.DataBind();

                ListItem item8 = new ListItem();
                item8.Text = "Select";
                item8.Value = "";
                ddlProjectCancelID.Items.Clear();
                ddlProjectCancelID.Items.Add(item8);

                ddlProjectCancelID.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
                ddlProjectCancelID.DataValueField = "ProjectCancelID";
                ddlProjectCancelID.DataTextField = "ProjectCancel";
                ddlProjectCancelID.DataMember = "ProjectCancel";
                ddlProjectCancelID.DataBind();
            }
        }
    }

    public void bindsalesrep()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        try
        {
            ddlSalesRep.SelectedValue = st2.SalesRep;
        }
        catch
        {

        }
    }
    public void bindprojecttype()
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        try
        {
            ddlProjectTypeID.SelectedValue = st2.ProjectTypeID;
        }
        catch
        {

        }
    }
    protected void btnUpdateDetail_Click(object sender, EventArgs e)
    {
        //BindScript();
        //Response.Write("heloo");
        //Response.End();
        // BindDropDown();
        string paidString = string.Empty;

        DataTable dtGovStatus = ClstblProjects.tbl_GetAllGovStatus();
        List<GovStatus> GovList = new List<GovStatus>();
        for (int i = 0; i < dtGovStatus.Rows.Count; i++)
        {
            GovStatus student = new GovStatus();
            student.id = Convert.ToInt32(dtGovStatus.Rows[i]["Id"]);
            student.StatusName = dtGovStatus.Rows[i]["StatusName"].ToString();
            GovList.Add(student);
        }

        string ProjectID = Request.QueryString["proid"];
        string CustomerID = Request.QueryString["compid"];
        string ContactID = ddlContact.SelectedValue;
        string EmployeeID = ddlSalesRep.SelectedValue;
        string SalesRep = ddlSalesRep.SelectedValue;
        string ProjectTypeID = ddlProjectTypeID.SelectedValue;
        //string ProjectStatusID = ddlProjectStatusID.SelectedValue;
        string ProjectOpened = txtProjectOpened.Text;
        string OldProjectNumber = txtOldProjectNumber.Text;
        string ManualQuoteNumber = txtManualQuoteNumber.Text;
        string Project = txtformbaystreetname.Text + "-" + txtInstallAddress.Text;

        //string formbayUnitNo = txtformbayUnitNo.Text;
        //string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;

        string InstallCity = ddlStreetCity.Text;
        //string InstallState = txtInstallState.Text;
        //string InstallPostCode = txtInstallPostCode.Text;
        string PVCapacity = txtpvcapacity.Text;
        string NetMeter = ddlnetmeter.SelectedValue;
        string Discom = ddlElecDist.SelectedValue;
        string Subdivison = ddlSubDivision.SelectedValue;
        string Division = ddldivision.SelectedValue;
        string ConsumerNo = txtNMINumber.Text;
        string HouseTypeID = ddlHouseType.SelectedValue;
        string RoofTypeID = ddlRoofType.SelectedValue;
        string sanction = txtsanction.Text;
        string Phase = ddlsolarinverter.SelectedValue;
        string ApplicationNo = txtdistributorapplicationnumber.Text;
        string GUVNL_Registeration_No = txtGUVNLregi.Text;
        string Circle = txtCircle.Text;


        if (!string.IsNullOrEmpty(ApplicationNo))
        {
            bool res = Attachfile();
            if (res)
            {
                paidString = GovList.Where(x => x.id == 1).Select(x => x.StatusName).FirstOrDefault();
            }
        }

        string ApplicationDate = txtElecDistApplied.Text;
        string QuoteNo = txtMeterEstimate.Text;
        string EstimatedValue = txtEstimatedPaid.Text;
        string duedate = txtestimatepaymentdue.Text;
        string PaidStatus = ddlestimatevalue.Text;
        if (PaidStatus == "1")
        {
            paidString = GovList.Where(x => x.id == 3).Select(x => x.StatusName).FirstOrDefault(); ;

        }
        else if (PaidStatus == "2")
        {
            paidString = GovList.Where(x => x.id == 2).Select(x => x.StatusName).FirstOrDefault(); ;

        }
        string ProjectNotes = txtProjectNotes.Text;
        string FollowUp = txtFollowupDate.Text;
        string FollowUpNote = txtFollowUpNote.Text;
        string InstallerNotes = txtInstallerNotes.Text;
        string MeterInstallerNotes = txtMeterInstallerNotes.Text;
        string oldsystemdetail = txtoldsystemdetails.Text;
        string StreetCity = ddlStreetCity.Text;
        string StreetState = ddlStreetState.SelectedValue;
        string StreetPostCode = txtStreetPostCode.Text;
        //string projectcancel = chkprojectcancel.Checked.ToString();
        string StreetArea = ddlArea.SelectedValue;
        txtInstallAddress.Text = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
        string InstallAddress = txtInstallAddress.Text;
        string project = InstallCity + '-' + InstallAddress;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string Area = ddlArea.SelectedValue;
        string District = ddlDistrict.SelectedValue;
        string Taluka = ddltaluka.Text;
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        SttblProjects stproject = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //Response.Write("'" + CustomerID + "','" + formbayunittype + "','" + formbayUnitNo + "','" + formbayStreetNo + "','" + formbaystreetname + "','" + formbaystreettype + "','" + InstallCity + "','" + InstallState + "','" + InstallPostCode + "'");
        //int existaddress = ClstblCustomers.tblCustomers_ExitsByCustID_Address(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        //if (Roles.IsUserInRole("PreInstaller"))
        //{
        //    existaddress = 1;
        //}
        //if (existaddress > 0)
        //{


        var customerdetails = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);

        bool resultArea = ClstblCustomers.tblCustomers_UpdateAddress(CustomerID, InstallAddress, InstallCity, StreetState, StreetPostCode, customerdetails.Customer, StreetArea);
        //int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustomerID, "", "", formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
        int existcustomer = ClstblCustomers.tblCustomers_ExitsByID_Address(CustomerID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dtaddress = ClstblCustomers.tblCustomers_tblProjects_Exits_Address_new(CustomerID, InstallAddress, InstallCity, StreetState, StreetPostCode, StreetArea);
        if (dtaddress.Rows.Count > 0)
        {
            PanAddUpdate.Visible = true;
            //lblerror.Visible = true;
            ModalPopupExtenderAddress.Show();
            // DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
            //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dtaddress;
            rptaddress.DataBind();
        }
        else
        {
            //    lblerror.Visible = false;
            if (ddlSalesRep.SelectedValue.ToString() != stproject.EmployeeID)
            {
                ClstblCustomers.tblCustomers_Update_Assignemployee(CustomerID, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                ClstblProjects.tblProjects_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
                ClstblContacts.tblContacts_UpdateEmployee(CustomerID, ddlSalesRep.SelectedValue.ToString());
            }
            //Response.Write(InstallCity);
            //Response.End();
            bool sucDetail = ClsProjectSale.tblProjects_UpdateDetails(ProjectID, ContactID, EmployeeID, SalesRep, ProjectTypeID, ProjectOpened, OldProjectNumber, ManualQuoteNumber, ProjectNotes, FollowUp, FollowUpNote, InstallerNotes, MeterInstallerNotes, InstallAddress, StreetCity, StreetState, StreetPostCode, UpdatedBy, Project);
            //bool nearby = ClstblCustomers.tblCustomer_Update_NearBy(Convert.ToString(success), NearBy, PostalNearBy, District, PostalDistrict);
            bool sucDetail2 = ClsProjectSale.OldSystemDetails(ProjectID, oldsystemdetail);
            bool result = ClstblProjects.tblprojects_Update_ApplicationStatus(paidString, ProjectID);
            bool sucDetail3 = ClsProjectSale.insertprojectdetails(ProjectID, Discom, Division, ConsumerNo, sanction, NetMeter, Phase, PVCapacity, HouseTypeID, RoofTypeID, ApplicationNo, ApplicationDate, QuoteNo);
            bool sucDetail4 = ClsProjectSale.updateestimatevalues(ProjectID, EstimatedValue, duedate, PaidStatus);
            bool sucDetail5 = ClsProjectSale.updatProjectDIsDetails(ProjectID, Circle, Subdivison, GUVNL_Registeration_No);

            //Response.Write(sucDetail);
            //Response.End();

            //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
            bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(ProjectID, "", "", txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value, project);
            ClsProjectSale.tblProjects_Update_others(ProjectID, txtformbayNearby.Text, ddlStreetCity.Text, ddlStreetState.SelectedValue, txtStreetPostCode.Text, project);
            //bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, txtInstallAddressline.Text);
            bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(ProjectID, InstallAddress);
            ClsProjectSale.tblProjects_UpdateDistrictTaluka(ProjectID, ddlDistrict.SelectedValue, ddltaluka.Text);
            bool success_House = ClstblProjects.tblProjects_UpdateHouseStays(ProjectID, ddlHousestatys.SelectedValue, txtStartDate.Text.Trim());

            //bool succ = ClstblCustomers.tblCustomer_Update_Address(CustomerID, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
            bool succ = ClstblCustomers.tblCustomer_Update_Address(CustomerID, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
            //bool succ_updateprojectcancel = ClstblProjects.tblProjects_projectcancel_update(ProjectID, projectcancel);

            if (txtFollowUpNote.Text != string.Empty && txtFollowupDate.Text != string.Empty)
            {

                int exist = ClsProjectSale.tblCustInfo_Exists(CustomerID, ContactID, FollowUp);
                if (exist == 0)
                {
                    int successInfo = ClstblCustInfo.tblCustInfo_Insert(CustomerID, txtFollowUpNote.Text, txtFollowupDate.Text, ContactID, UpdatedBy, "1");
                }
            }
            if (txtInstallerNotes.Text != string.Empty)
            {
                int exist2 = ClsProjectSale.tblInstallerNotes_Exists(ProjectID, EmployeeID, txtInstallerNotes.Text);
                if (exist2 == 0)
                {
                    int success3 = ClstblInstallerNotes.tblInstallerNotes_Insert(ProjectID, EmployeeID, txtInstallerNotes.Text);
                    BindProjectDetail();
                }
            }
            if (ddlProjectTypeID.SelectedValue == "2")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateFormsSolar(ProjectID);
            }
            if (ddlProjectTypeID.SelectedValue == "3")
            {
                bool suc = ClsProjectSale.tblProjects_UpdateFormsSolarUG(ProjectID);
            }
            if (ddlInstaller.SelectedValue != string.Empty)
            {
                string installer = ddlInstaller.SelectedValue;
                string Designer = installer;
                string Electrician = installer;
                bool suc = ClsProjectSale.tblProjects_UpdatePreInstaller(ProjectID, installer, Designer, Electrician);

                int suc1 = ClsProjectSale.tblInstallBookings_InsertprojectDetail(stproject.ProjectNumber, ProjectID);
            }

            /* --------------------- SM Ready. --------------------- */
            string SMready = "0";
            if (string.IsNullOrEmpty(stproject.IsSMReady) || stproject.IsSMReady == "False")
            {
                if (chkclickSMready.Checked == true)
                {
                    SMready = "1";
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "24");
                    bool suc1 = ClsProjectSale.tblProjects_UpdateIsSMReady(ProjectID, SMready);
                }
            }

            /* --------------------- Update STC No. --------------------- */

            decimal stcno;
            //if (stproject.InstallPostCode != string.Empty)
            //{
            //    DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
            //    if (dtZoneCode.Rows.Count > 0)
            //    {
            //        string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            //        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            //        if (dtZoneRt.Rows.Count > 0)
            //        {
            //            string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

            //            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * 15);
            //            int myInt = (int)Math.Round(stcno);
            //            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
            //        }
            //    }
            //}

            if (stproject.InstallPostCode != string.Empty)
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stproject.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {
                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        string ZoneRt = dtZoneRt.Rows[0]["STCRating"].ToString();

                        string installbooking = stproject.InstallBookingDate;
                        if (installbooking == null || installbooking == "")
                        {
                            DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                            String year = currentdate.Year.ToString();
                            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                            int myInt = (int)Math.Round(stcno);
                            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));

                        }
                        else
                        {
                            string date = Convert.ToDateTime(installbooking).ToShortDateString();
                            DateTime dt1 = Convert.ToDateTime(date);
                            String year = dt1.Year.ToString();
                            DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                            Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                            stcno = (Convert.ToDecimal(stproject.SystemCapKW) * Convert.ToDecimal(ZoneRt) * stcrate);
                            int myInt = (int)Math.Round(stcno);
                            ClsProjectSale.tblProjects_UpdateSTCNo(ProjectID, Convert.ToString(myInt));
                        }

                    }
                }
            }

            /* ---------------------------------------------------------- */


            //--- do not chage this code Start------
            if (sucDetail)
            {
                BindProjects();
                bindsalesrep();
                bindprojecttype();
                SetAdd1();
                //PanSuccess.Visible = true;
            }
            else
            {
                SetError1();
                //PanError.Visible = true;
            }
            ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "InActiveLoader()", true);
            //Response.Redirect(Request.RawUrl);
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "<script type='text/javascript'>callSomething('" + st.ApplicaionStatus + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "<script type='text/javascript'>callProjectSTatus('" + st.ProjectSTatus + "');</script>", false);

        }

    }
    protected void btnCustAddress_Click(object sender, EventArgs e)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        txtInstallAddress.Text = st.StreetAddress;
        //txtformbayUnitNo.Text = st.unit_number;
        //ddlformbayunittype.SelectedValue = st.unit_type;
        txtformbayStreetNo.Text = st.street_number;
        txtformbaystreetname.Text = st.street_name;
        ddlformbaystreettype.SelectedValue = st.street_type;
        ddlStreetCity.Text = st.StreetCity;
        ddlStreetState.SelectedValue = st.StreetState;
        txtStreetPostCode.Text = st.StreetPostCode;
        txtformbayNearby.Text = st.Nearby;
    }
    protected void btnViewNotes_Click(object sender, EventArgs e)
    {
        DataTable dt = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
        if (dt.Rows.Count > 0)
        {
            ModalPopupExtender2.Show();
            divInstallgrid.Visible = true;
            GridView.DataSource = ClstblInstallerNotes.tblInstallerNotes_Select(Request.QueryString["proid"]);
            GridView.DataBind();
        }
        else
        {
            divInstallgrid.Visible = false;
        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        Literal ltProject = (Literal)this.Parent.Parent.FindControl("ltproject");
        string Project = txtformbaystreetname.Text + "-" + txtInstallAddress.Text;
        ltProject.Text = Project;
        //string formbayUnitNo = txtformbayUnitNo.Text;
        //string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;
        //string ApplicationNo = 
        string InstallCity = ddlStreetCity.Text;
        string InstallState = ddlStreetState.SelectedValue;
        string InstallPostCode = txtStreetPostCode.Text;
        string nearby = txtformbayNearby.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        if (dt.Rows.Count > 0)
        {
            GridView.DataSource = dt;
            GridView.DataBind();
        }


    }
    protected void ddlFind_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PanAddUpdate.Visible = true;

        //if (ddlFind.SelectedValue != string.Empty)
        //{
        //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ddlFind.SelectedValue);
        //    SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        //    lblcompanyname.Text = stCust.Customer;

        //    lblcity.Text = st.InstallCity;
        //    lblstreet.Text = st.InstallAddress;
        //    lbladdress.Text = st.InstallAddress + "," + st.InstallCity + "," + st.InstallState;

        //    txtInstallAddress.Text = st.InstallAddress;
        //    txtInstallCity.Text = st.InstallCity;
        //    txtInstallState.Text = st.InstallState;
        //    txtInstallPostCode.Text = st.InstallPostCode;
        //}
    }
    protected void txtInstallCity_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        string[] cityarr = txtformbaystreetname.Text.Split('|');

        if (cityarr.Length > 1)
        {
            txtformbaystreetname.Text = cityarr[0].Trim();
            ddlStreetState.SelectedValue = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
            //txtInstallState.Text = cityarr[1].Trim();
            //txtInstallPostCode.Text = cityarr[2].Trim();

        }
        else
        {
            ddlStreetState.SelectedValue = string.Empty;
            txtStreetPostCode.Text = string.Empty;
            //txtInstallState.Text = string.Empty;
            //txtInstallPostCode.Text = string.Empty;
        }
    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);

        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbaystreetname.Text = txtformbaystreetname.Text;
        //}
        BindScript();

        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
        ddlformbaystreettype.Focus();
        // Response.End();

    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        ddlStreetCity.Focus();
    }
    protected void ddlformbaycity_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        // chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        ddlStreetState.Focus();
    }

    protected void ddlformbaystate_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}

        ddlStreetState.Focus();
    }
    protected void ddlformbaypostalcode_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        BindScript();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    ddlPostalformbaystreettype.SelectedValue = ddlformbaystreettype.SelectedValue;
        //}
        //ddlStreetState.Focus();
    }
    public void BindScript()
    {

        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);

    }


    protected void btnInstDetail_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        chkElecDistOK.Checked = Convert.ToBoolean(st2.ElecDistOK);
        txtElecDistApproved.Text = st2.ElecDistApproved;
        ddlInstaller.SelectedValue = st2.Installer;
        ModalPopupExtenderInst.Show();

        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void ibtnUpdateDetail_Click(object sender, EventArgs e)
    {

        if (ddlInstaller.SelectedValue != string.Empty && txtElecDistApproved.Text.Trim() != string.Empty)
        {

            bool succ = ClsProjectSale.tblProjects_UpdateInstDetail(Convert.ToString(Request.QueryString["proid"]), Convert.ToString(chkElecDistOK.Checked), txtElecDistApproved.Text.Trim(), ddlInstaller.SelectedValue);

            //Response.Write(Convert.ToString(succ) + "==" + projectcancel);
            //Response.End();


            if (succ)
            {
                ModalPopupExtenderInst.Hide();
            }
        }
    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }
    //protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    //{
    //    ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "address();", true);
    //    PanAddUpdate.Visible = true;
    //    streetaddress();
    //}
    public void streetaddress()
    {
        address = txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + txtformbayNearby.Text + " " + ddlStreetCity.Text + " " + ddlStreetState.SelectedValue + " " + "" + txtStreetPostCode.Text;
        txtInstallAddress.Text = address;
        BindScript();
    }
    protected void rptaddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderAddress.Show();
        rptaddress.PageIndex = e.NewPageIndex;

        string CustomerID = Request.QueryString["compid"];
        //string formbayUnitNo = txtformbayUnitNo.Text;
        //string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;
        string streetcity = ddlStreetCity.Text;
        txtInstallAddress.Text = formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype + streetcity;
        string InstallAddress = txtInstallAddress.Text;

        //string InstallCity = txtInstallCity.Text;
        string InstallState = ddlStreetState.SelectedValue;
        string InstallPostCode = txtStreetPostCode.Text;

        DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, "", "", formbayStreetNo, formbaystreetname, formbaystreettype, streetcity, InstallState, InstallPostCode);
        //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        rptaddress.DataSource = dt;
        rptaddress.DataBind();
    }

    protected void ddltaluka_TextChanged(object sender, EventArgs e)
    {
        string taluka = ddltaluka.Text;
        if (!string.IsNullOrEmpty(taluka))
        {
            ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "InActiveLoader()", true);

        }
    }



    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    protected void ddlElecDist_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        PanAddUpdate.Visible = true;
        if (ddlElecDist.SelectedValue != string.Empty)
        {
            DataTable dt = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
            ddldivision.Items.Clear();
            ddldivision.DataSource = dt;
            ddldivision.DataValueField = "Id";
            ddldivision.DataMember = "DivisionName";
            ddldivision.DataTextField = "DivisionName";
            ddldivision.DataBind();

            if (ddlElecDist.SelectedValue == "14")
            {
                //divSPA.Visible = true;
            }
            else
            {
                //divSPA.Visible = false;
            }

            string ProjectID = Request.QueryString["proid"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlElecDist.SelectedValue == "19" && stPro.InstallState == "TAS")
            {
                //divSurveyCerti.Visible = true;
                //divCertiApprove.Visible = true;
            }
            else
            {
                //divSurveyCerti.Visible = false;
                //divCertiApprove.Visible = false;
            }



            //DataTable dtEnergex = ClstblElecDistributor.tblElecDistributor_SelectEnergex();
            //if (dtEnergex.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtEnergex.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorNMINumber.Visible = true;
            //    }
            //    else
            //    {
            //        RequiredFieldValidatorNMINumber.Visible = false;
            //    }
            //}

            //DataTable dtErgon = ClstblElecDistributor.tblElecDistributor_SelectErgon();
            //if (dtErgon.Rows.Count > 0)
            //{
            //    if (ddlElecDist.SelectedValue == dtErgon.Rows[0]["ElecDistributorID"].ToString())
            //    {
            //        RequiredFieldValidatorApprovalRef.Visible = true;
            //        RequiredFieldValidatorElecRetailer.Visible = true;
            //        RequiredFieldValidatorRegPlanNo.Visible = true;
            //    }
            //}
        }
        else
        {
            //divSurveyCerti.Visible = false;
            //divCertiApprove.Visible = false;
        }
    }

    protected void ddldivision_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlRoofType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        if (ddlRoofType.SelectedValue != string.Empty)
        {
            if (ddlRoofType.SelectedValue == "8") // Tin Flat
            {
                //ddlRoofAngle.Enabled = false;
                //ddlRoofAngle.SelectedValue = "";
            }
            else
            {
                //ddlRoofAngle.Enabled = true;
            }
            if (ddlRoofType.SelectedValue == "7") // Tin + Tile  
            {
                //divFlatPanels.Visible = true;
                //divPitchedPanels.Visible = true;
                //lblFlatPanels.Text = "Panels on Tin";
                //lblPitchedPanels.Text = "Panels on Tile";
            }
            else if (ddlRoofType.SelectedValue == "9") // Tin Pitched + Flat
            {
                //divFlatPanels.Visible = true;
                //divPitchedPanels.Visible = true;
                //lblFlatPanels.Text = "Panels on Flat";
                //lblPitchedPanels.Text = "Panels on Pitched";
            }
            else
            {
                //divFlatPanels.Visible = false;
                //divPitchedPanels.Visible = false;
                //lblFlatPanels.Text = "";
                //lblPitchedPanels.Text = "";
            }
        }
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        //ddlformbayunittype.Focus();

    }

    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        txtformbayStreetNo.Focus();

    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        BindScript();
        txtformbaystreetname.Focus();


    }

    protected void ddlProjectTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {

        HideAndShowPanel(ddlProjectTypeID.SelectedValue);
        if (ddlProjectTypeID.SelectedItem.Text == "Replacing" || ddlProjectTypeID.SelectedItem.Text == "Solar UG")
        {
            oldsystemdetaildiv.Visible = true;
        }
        else
        {
            oldsystemdetaildiv.Visible = false;
        }

    }
    protected void txtformbayNearby_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        streetaddress();
        //checkExistsAddress();
        txtformbaystreetname.Focus();
        //chkSameasAbove.Checked = false;
        //if (chkSameasAbove.Checked)
        //{
        //    txtPostalformbayStreetNo.Text = txtformbayStreetNo.Text;
        //}
        BindScript();

    }
    public void HideAndShowPanel(string flag)
    {
        ////7 : Maintainance
        //Panel myPanel = (Panel)ddlProjectTypeID.NamingContainer.NamingContainer.FindControl("Panel2");

        //AjaxControlToolkit.TabContainer TabContainer1 = myPanel.FindControl("TabContainer1") as AjaxControlToolkit.TabContainer;
        ////  AjaxControlToolkit.TabPanel TabSale = TabContainer1.FindControl("TabSale") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabPrice = TabContainer1.FindControl("TabPrice") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabQuote = TabContainer1.FindControl("TabQuote") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabFinance = TabContainer1.FindControl("TabFinance") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabForms = TabContainer1.FindControl("TabForms") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabSTC = TabContainer1.FindControl("TabSTC") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabPreInst = TabContainer1.FindControl("TabPreInst") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabPostInst = TabContainer1.FindControl("TabPostInst") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabElecInv = TabContainer1.FindControl("TabElecInv") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabMtce = TabContainer1.FindControl("TabMtce") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabDocs = TabContainer1.FindControl("TabDocs") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabRefund = TabContainer1.FindControl("TabRefund") as AjaxControlToolkit.TabPanel;
        //AjaxControlToolkit.TabPanel TabMaintenance = TabContainer1.FindControl("TabMaintenance") as AjaxControlToolkit.TabPanel;
        //string openMaintainace;

        //if (flag == "7")
        //{
        //    openMaintainace = "open";

        //    //   TabSale.Visible = false;
        //    TabPrice.Visible = true;
        //    TabQuote.Visible = true;
        //    TabFinance.Visible = false;
        //    TabForms.Visible = false;
        //    TabSTC.Visible = true;
        //    TabPreInst.Visible = true;
        //    TabPostInst.Visible = true;
        //TabElecInv.Visible = true;
        //    TabMtce.Visible = true;
        //    TabDocs.Visible = false;
        //    TabRefund.Visible = false;
        //    //TabMaintenance.Visible = true;
        //    //TabMaintenance.HeaderText = "Maintenance";

        //}

        //else
        //{
        //    openMaintainace = "close";

        //    //   TabSale.Visible = true;
        //    TabPrice.Visible = true;
        //    TabQuote.Visible = true;
        //    TabFinance.Visible = true;
        //    TabForms.Visible = true;
        //    TabSTC.Visible = true;
        //    TabPreInst.Visible = true;
        //    TabPostInst.Visible = true;
        //    TabElecInv.Visible = true;
        //    TabMtce.Visible = true;
        //    TabDocs.Visible = true;
        //    TabRefund.Visible = true;
        //    //TabMaintenance.Visible = true;

        //    //TabMaintenance.HeaderText = "Sale";
        //}
        //Session["openMaintainace"] = openMaintainace;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool success = ClsProjectSale.tblProjects_IsClickCustomer(Request.QueryString["proid"], chkclickcustomer.Checked.ToString());
        if (success)
        {
            SetAdd1();
        }
        else
        {
            SetError1();
        }

    }


    protected void uploadpayment_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderDetails.Show();
    }

    protected void btnuploadoc_Click(object sender, EventArgs e)
    {

    }

    public bool Attachfile()
    {
        bool result = false;
        string ProjectID = Request.QueryString["proid"];
        string CustomerID = Request.QueryString["compid"];
        string ApplicationNo = txtdistributorapplicationnumber.Text;
        string ApplicationDate = txtElecDistApplied.Text;
        string DocumentId = "19";
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string Filename = "";
        string PDFFilename = "";
        try
        {
            if (FileUpload2.HasFile)
            {
                PDFFilename = string.Empty;
                Filename = FileUpload2.FileName;
                string ID = hdnItemID.Value;
                //SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                PDFFilename = ProjectID + "_" + Filename;
                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                FileUpload2.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                result = true;

            }
            else
            {
                PDFFilename = hdnFileName.Value.ToString();
                string ID = hdnItemID.Value;
                //SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                FileUpload2.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                result = true;
            }
        }
        catch (Exception ex)
        {
            //throw ex;
        }
        var filepath = ProjectID + "_" + Filename;
        
            int success = ClstblDocuments.tblCustomerDocuments_Insert(CustomerID, DocumentId, ApplicationNo, PDFFilename, UpdatedBy, DateTime.Now);
        return result;
    }

    protected void button1_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        String ProjectNumber = st2.ProjectNumber;
        String PaymentAmount = txtpayamt.Text;
        String PaymentMethod = ddlpaymentmethod.SelectedValue;
        String PaymentReceipt = txtpayamtreceipt.Text;
        string Filename = "";
        int success = 0;
        string PDFFilename = string.Empty;
        if (FileUpload1.HasFile)
        {

            Filename = FileUpload1.FileName;
            //string ID = hdnItemID.Value;
            //SttblCustomers stemp = ClstblCustomers.tblEmp_SelectByID(userid);
            PDFFilename = ProjectID + "_" + Filename;
            SiteConfiguration.DeletePDFFile("EmployeeDocuments", PDFFilename);
            FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/EmployeeDocuments/") + PDFFilename);
            SiteConfiguration.UploadPDFFile("EmployeeDocuments", PDFFilename);
            success = ClstblDocuments.InserQuotePayment(ProjectID, ProjectNumber, userid, PaymentAmount, PaymentMethod, PaymentReceipt, PDFFilename, DateTime.Now.ToString());
            //success = ClstblDocuments.tblEmployeeDocuments_Insert(EmpId, DocumentId, number, PDFFilename);

        }
        else
        {
            string ID = hdnItemID.Value;
            //SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
            PDFFilename = hdnFileNamepayment.Value.ToString();
            SiteConfiguration.DeletePDFFile("EmployeeDocuments", PDFFilename);
            FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/EmployeeDocuments/") + PDFFilename);
            SiteConfiguration.UploadPDFFile("EmployeeDocuments", PDFFilename);
            success = ClstblDocuments.InserQuotePayment(ProjectID, ProjectNumber, userid, PaymentAmount, PaymentMethod, PaymentReceipt, PDFFilename, DateTime.Now.ToString());

        }
        try
        {

        }
        catch (Exception ex)
        {
            throw ex;
        }
        if (txtEstimatedPaid.Text == txtpayamt.Text)
        {
            ddlestimatevalue.SelectedValue = "1";
        }
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "InActiveLoader()", true);

        SetAdd1();
    }

    protected void txtpvcapacity_TextChanged(object sender, EventArgs e)
    {
        string SanctionedValue = txtsanction.Text;
        string PVCapacity = txtpvcapacity.Text;
        bool result = Convert.ToBoolean((Convert.ToDecimal(SanctionedValue) >= Convert.ToDecimal(PVCapacity)));
        if(!result)
        {
            txtsanction.BorderColor= System.Drawing.Color.Red;
            lnlError.Text = "SanctionedValue must be grather then PVCapacity";
            lnlError.ForeColor= System.Drawing.Color.Red;
            btnUpdateDetail.Enabled = false;
        }

    }

    protected void txtsanction_TextChanged(object sender, EventArgs e)
    {
        string SanctionedValue = txtsanction.Text;
        string PVCapacity = txtpvcapacity.Text;
        if (!string.IsNullOrEmpty(PVCapacity))
        {
            bool result = Convert.ToBoolean((Convert.ToDecimal(SanctionedValue) >= Convert.ToDecimal(PVCapacity)));
            if (result)
            {
                txtsanction.BorderColor = System.Drawing.Color.Black;
                lnlError.Text = "";
                lnlError.ForeColor = System.Drawing.Color.Black;
                btnUpdateDetail.Enabled = true;

            }
        }
    }
}