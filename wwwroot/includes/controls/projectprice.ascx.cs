﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class includes_controls_projectprice : System.Web.UI.UserControl
{
    decimal travelcost = 0;
    decimal CalBalcost = 0;
    decimal totalcostnew = 0;
    decimal depositreqcost = 0;
    decimal balcost = 0;
    decimal depositcost = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            
            if ((Roles.IsUserInRole("Sales Manager")))
            {

                if (st2.ProjectStatusID == "3")
                {
                    btnUpdatePrice.Visible = false;
                }
            }
           

        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    public void BindProjectPrice()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
            //Response.Write("HotWaterMeter:"+st.HotWaterMeter+"<br/>");
            //Response.Write("SmartMeter:" + st.SmartMeter);
            //Response.End();

            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }


            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }
            BindPriceDropDown();

            if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }

            txtHouseTypePrice.Text = st.HouseType;
            txtRoofTypePrice.Text = st.RoofType;
            txtRoofAnglePrice.Text = st.RoofAngle;

            try
            {
                double cap = Convert.ToDouble(st.SystemCapKW);
                //if (cap < 1)
                //{
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;

                //    subcap1 = cap * 46.827 * 0.40;
                //    txtBasicSystemCost.Text = Convert.ToString(cap * 46.827);
                //    var finalcost= cap * 46.827;
                //    txtTotalCost1.Text = Convert.ToString(finalcost - (subcap1 + subcap2));
                //}
                //if (cap >= 1 && cap <= 1.50)
                //{
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;

                //    subcap1 = cap * 46.827 * 0.40;
                //    var finalcost = cap * 46.827;
                //    txtBasicSystemCost.Text = Convert.ToString(cap * 46.827);
                //    txtTotalCost1.Text = Convert.ToString(finalcost - (subcap1 + subcap2));
                //    //txttotalamt.Text = Convert.ToString(actCost - (subcap1 + subcap2));
                //}
                //if (cap >= 1.51 && cap <= 2.50)
                //{
                //    cap = 2;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 2.51 && cap <= 3.50)
                //{
                //    cap = 3;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 3.51 && cap <= 4.50)
                //{
                //    cap = 4;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 4.51 && cap <= 5.50)
                //{
                //    cap = 5;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 5.51 && cap <= 6.00)
                //{
                //    cap = 6;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 6.01 && cap <= 6.50)
                //{
                //    cap = 8;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 6.51 && cap <= 7.50)
                //{
                //    cap = 8;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 7.51 && cap <= 8.50)
                //{
                //    cap = 8;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 8.51 && cap <= 9.50)
                //{
                //    cap = 8;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 9.51 && cap <= 9.99)
                //{
                //    cap = 8;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap == 10.00)
                //{
                //    cap = 10;
                //    SttblAPSPricemaster stprice = ClstblProjects.tblAPSPriceMaster_SelectBySystemSize(Convert.ToString(cap));
                //    txtBasicSystemCost.Text = stprice.TotalPrice;
                //    txtTotalCost1.Text = stprice.TotalPayableprice;
                //}
                //if (cap >= 10.00 && cap <= 25.00)
                //{
                //    var cap1 = cap - 3;
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;
                //    var subcap3 = 0.00;

                //    subcap1 = 3 * 38006 * 0.40;
                //    subcap2 = 7 * 38006 * 0.20;

                //    var cap2 = cap1 - 7;

                //    if (cap2 > 0)
                //    {
                //        subcap3 = cap2 * 38006;
                //    }

                //    cap = cap * 38006;
                //    var totalprice = subcap1 + subcap2 + subcap3;
                //    txtBasicSystemCost.Text = Convert.ToString(cap);
                //    txtTotalCost1.Text = Convert.ToString(cap - (subcap1 + subcap2));
                //}
                //if (cap >= 25.00 && cap <= 50.00)
                //{
                //    var cap1 = cap - 3;
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;
                //    var subcap3 = 0.00;

                //    subcap1 = 3 * 37505 * 0.40;
                //    subcap2 = 7 * 37505 * 0.20;

                //    var cap2 = cap1 - 7;

                //    if (cap2 > 0)
                //    {
                //        subcap3 = cap2 * 37505;
                //    }

                //    cap = cap * 37505;
                //    var totalprice = subcap1 + subcap2 + subcap3;
                //    txtBasicSystemCost.Text = Convert.ToString(cap);
                //    txtTotalCost1.Text = Convert.ToString(cap - (subcap1 + subcap2));
                //}
                //if (cap >= 50.00 && cap <= 100.00)
                //{
                //    var cap1 = cap - 3;
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;
                //    var subcap3 = 0.00;

                //    subcap1 = 3 * 36002 * 0.40;
                //    subcap2 = 7 * 36002 * 0.20;

                //    var cap2 = cap1 - 7;

                //    if (cap2 > 0)
                //    {
                //        subcap3 = cap2 * 36002;
                //    }

                //    cap = cap * 36002;
                //    var totalprice = subcap1 + subcap2 + subcap3;
                //    txtBasicSystemCost.Text = Convert.ToString(cap);
                //    txtTotalCost1.Text = Convert.ToString(cap - (subcap1 + subcap2));
                //}
                //if (cap >= 100.00)
                //{
                //    var cap1 = cap - 3;
                //    var subcap1 = 0.00;
                //    var subcap2 = 0.00;
                //    var subcap3 = 0.00;

                //    subcap1 = 3 * 33999 * 0.40;
                //    subcap2 = 7 * 33999 * 0.20;

                //    var cap2 = cap1 - 7;

                //    if (cap2 > 0)
                //    {
                //        subcap3 = cap2 * 33999;
                //    }

                //    cap = cap * 33999;
                //    var totalprice = subcap1 + subcap2 + subcap3;
                //    txtBasicSystemCost.Text = Convert.ToString(cap);
                //    txtTotalCost1.Text = Convert.ToString(cap - (subcap1 + subcap2));
                //}
                SttblAPSPricemaster stpricenew = ClstblProjects.tblpricemaster_selectbycap(Convert.ToString(cap));
                if (cap < 3)
                {
                    var subcap1 = cap * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    var subcap2 = 0.00;
                    txtBasicSystemCost.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    var elestructcost = Convert.ToDecimal(st.VarOther);
                    var totalsusidy = subcap1 + subcap2;
                    var totalactcost = Convert.ToDecimal(actCost - totalsusidy);
                    txtTotalCost1.Text = Convert.ToString((totalactcost + elestructcost));
                }
                else
                {
                    var cap1 = cap - 3;
                    var subcap1 = 0.00;
                    var subcap2 = cap1;
                    var subcap3 = 0.00;

                    subcap1 = 3 * Convert.ToDouble(stpricenew.PricePerKW) * 0.40;
                    if (subcap2 <= 10)
                    {
                        subcap2 = subcap2 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;
                    }
                    else
                    {
                        subcap2 = 7 * Convert.ToDouble(stpricenew.PricePerKW) * 0.20;

                        var cap2 = cap - 10;

                        if (cap2 > 0)
                        {
                            subcap3 = cap2 * Convert.ToDouble(stpricenew.PricePerKW);
                        }
                    }
                    txtBasicSystemCost.Text = Convert.ToString(cap * Convert.ToDouble(stpricenew.PricePerKW));
                    var actCost = cap * Convert.ToDouble(stpricenew.PricePerKW);
                    var elestructcost = Convert.ToDecimal(st.VarOther);
                    var totalsusidy = subcap1 + subcap2 + subcap3;
                    var totalactcost = Convert.ToDecimal(actCost - totalsusidy);
                    txtTotalCost1.Text = Convert.ToString((totalactcost + elestructcost));
                }
                totalcostnew = Convert.ToDecimal(txtTotalCost1.Text);
            }
            catch(Exception ex)
            { throw ex; }
            try
            {
                txtHouseType.Text = SiteConfiguration.ChangeCurrency_Val(st.VarHouseType);
            }
            catch
            { }
            try
            {
                txtRoofType.Text = SiteConfiguration.ChangeCurrency_Val(st.VarRoofType);
            }
            catch
            { }
            try
            {
                txtRoofAngle.Text = SiteConfiguration.ChangeCurrency_Val(st.VarRoofAngle);
            }
            catch
            { }
            try
            {
                txtMeterInstallation.Text = SiteConfiguration.ChangeCurrency_Val(st.VarMeterInstallation);
            }
            catch
            { }
            try
            {
                txtMeterUpgrade.Text = SiteConfiguration.ChangeCurrency_Val(st.VarMeterUG);
            }
            catch
            { }
            try
            {
                if (!string.IsNullOrEmpty(st.collectioncharge))
                {
                    txtcharge.Text = SiteConfiguration.ChangeCurrency_Val(st.collectioncharge);

                }
                else
                {
                    txtcharge.Text = SiteConfiguration.ChangeCurrency_Val(txtcharge.Text = "0");
                }
            }
            catch { }
            try
            {
                if(!string.IsNullOrEmpty(st.HotWaterMeter))
                {
                    txthotmeter.Text = SiteConfiguration.ChangeCurrency_Val(st.HotWaterMeter);
                }
                else
                {
                    txthotmeter.Text = SiteConfiguration.ChangeCurrency_Val(txthotmeter.Text = "0");
                }
                
            }
            catch
            { }
            try
            {
                if (!string.IsNullOrEmpty(st.SmartMeter))
                {
                    txtsmartmeter.Text = SiteConfiguration.ChangeCurrency_Val(st.SmartMeter);
                }
                else
                {
                    txtsmartmeter.Text=SiteConfiguration.ChangeCurrency_Val(txtsmartmeter.Text="0");
                }
            }
            catch
            {

            }
            try
            {
                txtAsbestos.Text = SiteConfiguration.ChangeCurrency_Val(st.VarAsbestos);
            }
            catch
            { }
            try
            {
                txtSplitSystem.Text = SiteConfiguration.ChangeCurrency_Val(st.VarSplitSystem);
            }
            catch
            { }
            try
            {
                txtCherryPicker.Text = SiteConfiguration.ChangeCurrency_Val(st.VarCherryPicker);
            }
            catch
            { }
            try
            {
                txtTravelCost.Text = SiteConfiguration.ChangeCurrency_Val(st.VarTravelTime);
            }
            catch
            { }
            try
            {
                txtOther.Text = SiteConfiguration.ChangeCurrency_Val(st.VarOther);
            }
            catch
            { }
            try
            {
                txtSpecialDiscount.Text = SiteConfiguration.ChangeCurrency_Val(st.SpecialDiscount);
            }
            catch
            { }

            if (Roles.IsUserInRole("Administrator")|| Roles.IsUserInRole("SubAdministrator")||Roles.IsUserInRole("Sales Manager"))
            {
                divusername.Visible=true;
                lblusername.Text = st_emp.EmpFirst + ' ' + st_emp.EmpLast;
            }

            //try
            //{
            //    //Response.Write(SiteConfiguration.ChangeCurrencyVal(st.TotalQuotePrice)+"<=");
            //    //Response.End();
            //    //txtTotalCost1.Text = SiteConfiguration.ChangeCurrency_Val(st.TotalQuotePrice);
            //}
            //catch { }
            try
            {
                txtDepositRequired.Text = SiteConfiguration.ChangeCurrency_Val(st.DepositRequired);
            }
            catch
            { }

            chkMeterEnoughSpace.Checked = Convert.ToBoolean(st.MeterEnoughSpace);
            chkMeterUG.Checked = Convert.ToBoolean(st.MeterUG);
            chkAsbestoss.Checked = Convert.ToBoolean(st.Asbestoss);
            chkSplitSystem.Checked = Convert.ToBoolean(st.SplitSystem);
            chkCherryPicker.Checked = Convert.ToBoolean(st.CherryPicker);
            txtNW.Text = st.PanelConfigNW;
            txtOTH.Text = st.PanelConfigOTH;
            txtTravelTime.Text = st.TravelTime;
            txtVariationOther.Text = st.VariationOther;

            if (st.PanelBrandID != "")
            {
                txtVariations.Text = st.NumberPanels + "X" + st.PanelBrandName;
            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != "")
            {
                txtVariations.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.inverterqty + "X" + st.InverterDetailsName + " Inverter.";
            }
            if (st.PanelBrandID != "" && st.InverterDetailsID != ""  )
            {
                if(st.SecondInverterDetailsID != "0")
                {
                    txtVariations.Text = st.NumberPanels + "X" + st.PanelBrandName + " Panels with " + st.inverterqty + "X" + st.InverterDetailsName + " Inverter. Plus Second Inverter " + st.inverterqty2 + "X" + st.SecondInverterDetails;
                }
                
            }

            txtPromoText.Text = st2.PromoText;
            ddlPromo1ID.SelectedValue = st2.Promo1ID;
            ddlPromo2ID.SelectedValue = st2.Promo2ID;
            chkPromo3.Checked = Convert.ToBoolean(st2.Promo3);

            if (Convert.ToDecimal(txtHouseType.Text) > 0 && txtHouseTypePrice.Text != string.Empty)
            {
                try
                {
                    txtHouseType.Text = SiteConfiguration.ChangeCurrency_Val(st.VarHouseType);
                }
                catch { }
            }
            else
            {
                if (txtHouseTypePrice.Text != string.Empty)
                {
                    DataTable dtHouseType = ClstblHouseType.tblHouseType_SelectByHouseType(txtHouseTypePrice.Text);
                    {
                        try
                        {
                            txtHouseType.Text = SiteConfiguration.ChangeCurrencyVal(dtHouseType.Rows[0]["Variation"].ToString());
                        }
                        catch { }
                    }
                }
            }
            if (Convert.ToDecimal(txtRoofType.Text) > 0 && txtRoofTypePrice.Text != string.Empty)
            {
                try
                {
                    txtRoofType.Text = SiteConfiguration.ChangeCurrency_Val(st.VarRoofType);
                }
                catch { }
            }
            else
            {
                if (txtRoofTypePrice.Text != string.Empty)
                {
                    DataTable dtRoofType = ClstblRoofTypes.tblRoofTypes_SelectByRoofType(txtRoofTypePrice.Text);
                    {
                        try
                        {
                            txtRoofType.Text = SiteConfiguration.ChangeCurrency_Val(dtRoofType.Rows[0]["Variation"].ToString());
                        }
                        catch { }
                    }
                }
            }

            if (Convert.ToDecimal(txtRoofAngle.Text) > 0)
            {
                try
                {
                    txtRoofAngle.Text = SiteConfiguration.ChangeCurrencyVal(st.VarRoofAngle);
                }
                catch { }
            }
            else
            {
                if (txtRoofAnglePrice.Text != string.Empty)
                {
                    DataTable dtRoofAngle = ClstblRoofAngles.tblRoofAngles_SelectByRoofAngle(txtRoofAnglePrice.Text);
                    {
                        try
                        {
                            txtRoofAngle.Text = SiteConfiguration.ChangeCurrency_Val(dtRoofAngle.Rows[0]["Variation"].ToString());
                        }
                        catch { }
                    }
                }
            }
            //Response.Write("Hotmeter:"+txthotmeter.Text+"<br />");
            //Response.Write("Smartmeter:"+txtsmartmeter.Text + "<br />");
            //Response.Write("SyatemCost:" + txtBasicSystemCost.Text + "<br />");
            //Response.Write("HouseType:" + txtHouseType.Text + "<br />");
            //Response.Write("txtRoofType:" + txtRoofType.Text + "<br />");
            //Response.Write("txtRoofAngle:" + txtRoofAngle.Text + "<br />");
            //Response.Write("txtMeterInstallation:" + txtMeterInstallation.Text + "<br />");
            //Response.Write(txtMeterUpgrade.Text + "<br />");
            //Response.Write(txtAsbestos.Text + "<br />");
            //Response.Write(txtSplitSystem.Text + "<br />");
            //Response.Write(txtCherryPicker.Text + "<br />");
            //Response.Write(txtTravelCost.Text + "<br />");
            //Response.Write(txtOther.Text + "<br />");Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) +

            
                
          
           
            try
            {
                txtTotalCost1.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(totalcostnew));
            }
            catch { }

            balcost = totalcostnew - Convert.ToDecimal(txtDepositRequired.Text);
            try
            {
                txtBaltoPay.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balcost));
            }
            catch { }

            depositcost = Convert.ToDecimal(txtTotalCost1.Text) / 10;
            int myInt = (int)Math.Round(depositcost);
            //if (st.DepositAmount != string.Empty)
            //{
            //    txtDeposit.Text = SiteConfiguration.ChangeCurrencyVal(st.DepositAmount);
            //}
            //else
            //{
            try
            {
                txtDeposit.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(myInt));
            }
            catch { }
            //}

            ddlPaymentOption.SelectedValue = st.FinanceWithID;
            //if (st.FinanceWithID == "" || st.FinanceWithID == "1")
            if (ddlPaymentOption.SelectedValue == "" || ddlPaymentOption.SelectedValue == "1")
            {
                divDepositOption.Visible = false;
                divPaymentType.Visible = false;
            }
            else
            {
                divDepositOption.Visible = true;
                ddlDepositOption.SelectedValue = st2.FinanceWithDepositID;
                divPaymentType.Visible = true;
                ddlPaymentType.SelectedValue = st.PaymentTypeID;
            }
        }
    }

    public void BindPriceDropDown()
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlPromo1ID.Items.Clear();
        ddlPromo1ID.Items.Add(item1);

        ddlPromo1ID.DataSource = ClstblPromoType.tblPromoOffer_SelectASC();
        ddlPromo1ID.DataValueField = "PromoOfferID";
        ddlPromo1ID.DataMember = "PromoOffer";
        ddlPromo1ID.DataTextField = "PromoOffer";
        ddlPromo1ID.DataBind();

        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlPromo2ID.Items.Clear();
        ddlPromo2ID.Items.Add(item2);

        ddlPromo2ID.DataSource = ClstblPromoType.tbl_pstatus_SelectASC();
        ddlPromo2ID.DataValueField = "statusid";
        ddlPromo2ID.DataMember = "pstatus";
        ddlPromo2ID.DataTextField = "pstatus";
        ddlPromo2ID.DataBind();

        //ListItem item3 = new ListItem();
        //item3.Text = "Select";
        //item3.Value = "";
        ddlPaymentOption.Items.Clear();
        //  ddlPaymentOption.Items.Add(item3);

        ddlPaymentOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlPaymentOption.DataValueField = "FinanceWithID";
        ddlPaymentOption.DataMember = "FinanceWith";
        ddlPaymentOption.DataTextField = "FinanceWith";
        ddlPaymentOption.DataBind();

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlPaymentType.Items.Clear();
        ddlPaymentType.Items.Add(item4);

        ddlPaymentType.DataSource = ClstblPaymentType.tblPaymentType_SelectActive();
        ddlPaymentType.DataValueField = "PaymentTypeID";
        ddlPaymentType.DataMember = "PaymentType";
        ddlPaymentType.DataTextField = "PaymentType";
        ddlPaymentType.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlDepositOption.Items.Clear();
        ddlDepositOption.Items.Add(item5);

        ddlDepositOption.DataSource = ClstblFinanceWith.tblFinanceWithDeposit_SelectActive();
        ddlDepositOption.DataValueField = "FinanceWithDepositID";
        ddlDepositOption.DataMember = "FinanceWithDeposit";
        ddlDepositOption.DataTextField = "FinanceWithDeposit";
        ddlDepositOption.DataBind();
    }
    protected void btnUpdatePrice_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                 this.GetType(),
        //                                 "MyAction",
        //                                 "doMyAction();",
        //                                 true);

        //CompareValidatorDepositRequired.ValueToCompare = Convert.ToString(txtTotalCost1.Text);

        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string ServiceValue = txtBasicSystemCost.Text;
        string VarHouseType = txtHouseType.Text;
        string VarRoofType = txtRoofType.Text;
        string VarRoofAngle = txtRoofAngle.Text;
        string VarMeterInstallation = txtMeterInstallation.Text;
        string VarMeterUG = txtMeterUpgrade.Text;
        string VarAsbestos = txtAsbestos.Text;
        string VarSplitSystem = txtSplitSystem.Text;
        string VarCherryPicker = txtCherryPicker.Text;
        string VarTravelTime = txtTravelCost.Text;
        string VarOther = txtOther.Text;
        string SpecialDiscount = txtSpecialDiscount.Text;
        string TotalQuotePrice = txtTotalCost1.Text;
        string DepositRequired = txtDepositRequired.Text;
        string FinanceWithID = ddlPaymentOption.SelectedValue;
        string MeterEnoughSpace = Convert.ToString(chkMeterEnoughSpace.Checked);
        string MeterUG = Convert.ToString(chkMeterUG.Checked);
        string Asbestoss = Convert.ToString(chkAsbestoss.Checked);
        string SplitSystem = Convert.ToString(chkSplitSystem.Checked);
        string CherryPicker = Convert.ToString(chkCherryPicker.Checked);
        string PanelConfigNW = txtNW.Text;
        string PanelConfigOTH = txtOTH.Text;
        string TravelTime = txtTravelTime.Text;
        string VariationOther = txtVariationOther.Text;

        string PromoText = txtPromoText.Text;
        string Promo1ID = ddlPromo1ID.SelectedValue;
        string Promo2ID = ddlPromo2ID.SelectedValue;
        string Promo3 = Convert.ToString(chkPromo3.Checked);

        string InvoiceGST = "0";
        string InvoiceExGST = "0";
        if (Convert.ToDecimal(txtTotalCost1.Text) > 0)
        {
            //InvoiceGST = InvoiceTotal / 11
            //InvoiceExGST = InvoiceTotal * 10 / 11

            decimal gst = Convert.ToDecimal(txtTotalCost1.Text) / 11;
            InvoiceGST = Convert.ToString(gst);
            decimal exgst = (Convert.ToDecimal(txtTotalCost1.Text) * 10) / 11;
            InvoiceExGST = Convert.ToString(exgst);
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        string DepositAmount = txtDeposit.Text;
        string PaymentTypeID = ddlPaymentType.SelectedValue;
        bool successmeter = ClsProjectSale.tblProjects_UpdateHotSmartMeter(ProjectID, txthotmeter.Text, txtsmartmeter.Text);
        bool succollectioncharge = ClsProjectSale.tblProjects_Updatecollectioncharge(ProjectID, txtcharge.Text);
        bool sucPrice = ClsProjectSale.tblProjects_UpdatePrice(ProjectID, ServiceValue, VarHouseType, VarRoofType, VarRoofAngle, VarMeterInstallation, VarMeterUG, VarAsbestos, VarSplitSystem, VarCherryPicker, VarTravelTime, VarOther, SpecialDiscount, TotalQuotePrice, DepositRequired, FinanceWithID, MeterUG, Asbestoss, SplitSystem, CherryPicker, PanelConfigNW, PanelConfigOTH, TravelTime, VariationOther, UpdatedBy, DepositAmount, PaymentTypeID);
        bool sucPrice2 = ClsProjectSale.tblProjects2_UpdatePrice(ProjectID, PromoText, Promo1ID, Promo2ID, Promo3);
        bool sucEx = ClsProjectSale.tblProjects_UpdateEx(ProjectID, InvoiceGST, InvoiceExGST);

        if (ddlPaymentOption.SelectedValue == "4" && ddlDepositOption.SelectedValue == "1")
        {
            if(stPro.ProjectStatusID != "2")
            {
                ClstblProjects.tblProjects_UpdateInvoiceDoc(ProjectID, stPro.ProjectNumber);
                int success = ClstblInvoicePayments.tblInvoicePayments_Insert(ProjectID, "0", "0", "0", Convert.ToString(DateTime.Now.AddHours(14)), "11", "0", stEmp.EmployeeID, "", "", "", "False", "0", "", "", "", "");
            }
         
        }
        if (ddlDepositOption.SelectedValue != "")
        {

            ClstblProjects.tblProjects2_UpdateFinanceWithDeposit(ProjectID, ddlDepositOption.SelectedValue);
        }
        if (stPro.ProjectStatusID == "3")
        {
            ClsProjectSale.tblProjects_UpdatePreviousTotalQuotePrice(ProjectID, TotalQuotePrice);
        }

        if (sucPrice)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
        BindProjectPrice();

    }

    protected void txtBasicSystemCost_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text)+Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text)+Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtHouseType.Focus();
    }
    protected void txtHouseType_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                     this.GetType(),
        //                                     "MyAction",
        //                                     "doMyAction();",
        //                                     true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtRoofType.Focus();
    }
    protected void txtRoofType_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtRoofAngle.Focus();
    }
    protected void txtRoofAngle_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtMeterInstallation.Focus();
    }
    protected void txtMeterInstallation_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtMeterInstallation.Text) > 0)
        {
            chkMeterEnoughSpace.Checked = true;
        }
        else
        {
            chkMeterEnoughSpace.Checked = false;
        }
        txtMeterUpgrade.Focus();
    }
    protected void chkMeterEnoughSpace_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        if (chkMeterEnoughSpace.Checked == true)
        {
            DataTable dt = ClstblProjects.tblProjectVariations_Select("8");
            txtMeterInstallation.Text = SiteConfiguration.ChangeCurrencyVal(dt.Rows[0]["ProjectVariationValue"].ToString());

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }
        else
        {
            txtMeterInstallation.Text = "0";
            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtMeterInstallation.Text) > 0)
        {
            chkMeterEnoughSpace.Checked = true;
        }
    }
    protected void txtMeterUpgrade_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtMeterUpgrade.Text) > 0)
        {
            chkMeterUG.Checked = true;
        }
        else
        {
            chkMeterUG.Checked = false;
        }
        txtAsbestos.Focus();
    }
    protected void chkMeterUG_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        if (chkMeterUG.Checked == true)
        {
            DataTable dt = ClstblProjects.tblProjectVariations_Select("3");
            txtMeterUpgrade.Text = SiteConfiguration.ChangeCurrencyVal(dt.Rows[0]["ProjectVariationValue"].ToString());

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }
        if (chkMeterUG.Checked == false)
        {
            txtMeterUpgrade.Text = "0";
            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtMeterUpgrade.Text) > 0)
        {
            chkMeterUG.Checked = true;
        }
    }
    protected void txtAsbestos_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtAsbestos.Text) > 0)
        {
            chkAsbestoss.Checked = true;
        }
        else
        {
            chkAsbestoss.Checked = false;
        }
        txtSplitSystem.Focus();
    }
    protected void chkAsbestoss_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        if (chkAsbestoss.Checked == true)
        {
            DataTable dt = ClstblProjects.tblProjectVariations_Select("2");
            txtAsbestos.Text = SiteConfiguration.ChangeCurrencyVal(dt.Rows[0]["ProjectVariationValue"].ToString());

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }
        else
        {
            txtAsbestos.Text = "0";
            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtAsbestos.Text) > 0)
        {
            chkAsbestoss.Checked = true;
        }
    }
    protected void txtSplitSystem_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtSplitSystem.Text) > 0)
        {
            //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            //txtNW.Text = st.NumberPanels;
            //txtOTH.Text = "0";
            chkSplitSystem.Checked = true;
        }
        else
        {
            chkSplitSystem.Checked = false;
            txtSplitSystem.Text = "0";
            //txtNW.Text = "0";
            //txtOTH.Text = "0";
        }
        txtCherryPicker.Focus();
    }
    protected void chkSplitSystem_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        if (chkSplitSystem.Checked == true)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            txtNW.Text = st.NumberPanels;
            txtOTH.Text = "0";
            DataTable dt = ClstblProjects.tblProjectVariations_Select("5");
            txtSplitSystem.Text = SiteConfiguration.ChangeCurrencyVal(dt.Rows[0]["ProjectVariationValue"].ToString());

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

            txtNW.Text = st.NumberPanels;
            txtOTH.Text = "0";
        }
        else
        {
            txtSplitSystem.Text = "0";
            txtNW.Text = "0";
            txtOTH.Text = "0";

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtSplitSystem.Text) > 0)
        {
            chkSplitSystem.Checked = true;
        }
    }
    protected void txtCherryPicker_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtCherryPicker.Text) > 0)
        {
            chkCherryPicker.Checked = true;
        }
        else
        {
            chkCherryPicker.Checked = false;
        }
        txtTravelTime.Focus();
    }
    protected void chkCherryPicker_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        if (chkCherryPicker.Checked == true)
        {
            DataTable dt = ClstblProjects.tblProjectVariations_Select("6");
            txtCherryPicker.Text = SiteConfiguration.ChangeCurrencyVal(dt.Rows[0]["ProjectVariationValue"].ToString());

            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }
        else
        {
            txtCherryPicker.Text = "0";
            totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
            balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        }

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);

        if (Convert.ToDecimal(txtCherryPicker.Text) > 0)
        {
            chkCherryPicker.Checked = true;
        }
    }

    protected void txtTravelTime_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        travelcost = 2 * Convert.ToDecimal(txtTravelTime.Text);
        txtTravelCost.Text = Convert.ToString(travelcost);

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtOther.Focus();
    }
    protected void txtOther_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtSpecialDiscount.Focus();
        var basiccost = Convert.ToDecimal(txtBasicSystemCost.Text);
        var other = Convert.ToDecimal(txtOther.Text);
        txtTotalCost1.Text = Convert.ToString(basiccost + other);
    }
    protected void txtSpecialDiscount_TextChanged(object sender, EventArgs e)
    {
        //scriptmanager.registerstartupscript(panprojectprice,
        //                                  this.gettype(),
        //                                  "myaction",
        //                                  "domyaction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtDepositRequired.Focus();
    }
    protected void txtDepositRequired_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        //CompareValidatorDepositRequired.ValueToCompare = Convert.ToString(txtTotalCost1.Text);
    }
    protected void txtCalcBalance_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;

        txtBaltoPay.Text = txtCalcBalance.Text;
        depositreqcost = Convert.ToDecimal(txtTotalCost1.Text) - Convert.ToDecimal(txtBaltoPay.Text);

        txtDepositRequired.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(depositreqcost));
        CompareValidatorCalcBalance.ValueToCompare = Convert.ToString(txtTotalCost1.Text);

        txtCalcBalance.Text = string.Empty;
    }
    protected void txtNW_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        decimal OTH = 0;
        if (txtNW.Text == string.Empty)
        {
            txtNW.Text = "0";
            OTH = Convert.ToDecimal(st.NumberPanels) - 0;
        }
        else
        {
            OTH = Convert.ToDecimal(st.NumberPanels) - Convert.ToDecimal(txtNW.Text);
        }
        txtOTH.Text = Convert.ToString(OTH);
    }
    protected void txtOTH_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                  this.GetType(),
        //                                  "MyAction",
        //                                  "doMyAction();",
        //                                  true);
        PanAddUpdate.Visible = true;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        decimal NW = 0;
        if (txtOTH.Text == string.Empty)
        {
            txtOTH.Text = "0";
            NW = Convert.ToDecimal(st.NumberPanels) - 0;
        }
        else
        {
            NW = Convert.ToDecimal(st.NumberPanels) - Convert.ToDecimal(txtOTH.Text);
        }
        txtNW.Text = Convert.ToString(NW);
    }

    protected void btnDeposit_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        PanAddUpdate.Visible = true;
        txtDepositRequired.Text = SiteConfiguration.ChangeCurrencyVal((txtDeposit.Text).ToString());
        balcost = ((Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text)) - (Convert.ToDecimal(txtDepositRequired.Text)));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));
    }
    protected void ddlPaymentOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (ddlPaymentOption.SelectedValue == "1" || ddlPaymentOption.SelectedValue == "")
        {
            divDepositOption.Visible = false;
            divPaymentType.Visible = false;
        }
        else
        {
            divDepositOption.Visible = true;
            divPaymentType.Visible = true;
        }
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void txthotmeter_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                 this.GetType(),
        //                                 "MyAction",
        //                                 "doMyAction();",
        //                                 true);
        PanAddUpdate.Visible = true;

        travelcost = 2 * Convert.ToDecimal(txtTravelTime.Text);
        txtTravelCost.Text = Convert.ToString(travelcost);

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtsmartmeter.Focus();
    }

    protected void txtsmartmeter_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectprice,
        //                                 this.GetType(),
        //                                 "MyAction",
        //                                 "doMyAction();",
        //                                 true);
        PanAddUpdate.Visible = true;

        travelcost = 2 * Convert.ToDecimal(txtTravelTime.Text);
        txtTravelCost.Text = Convert.ToString(travelcost);

        totalcostnew = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text));
        balcost = (Convert.ToDecimal(txthotmeter.Text) + Convert.ToDecimal(txtsmartmeter.Text) + Convert.ToDecimal(txtBasicSystemCost.Text) + Convert.ToDecimal(txtHouseType.Text) + Convert.ToDecimal(txtRoofType.Text) + Convert.ToDecimal(txtRoofAngle.Text) + Convert.ToDecimal(txtMeterInstallation.Text) + Convert.ToDecimal(txtMeterUpgrade.Text) + Convert.ToDecimal(txtAsbestos.Text) + Convert.ToDecimal(txtSplitSystem.Text) + Convert.ToDecimal(txtCherryPicker.Text) + Convert.ToDecimal(txtTravelCost.Text) + Convert.ToDecimal(txtOther.Text)) - (Convert.ToDecimal(txtSpecialDiscount.Text) + Convert.ToDecimal(txtDepositRequired.Text));

        txtTotalCost1.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
        txtBaltoPay.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balcost));

        depositcost = totalcostnew / 10;
        int myInt = (int)Math.Round(depositcost);
        txtDeposit.Text = Convert.ToString(myInt);
        txtOther.Focus();
    }
}