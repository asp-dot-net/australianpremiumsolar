﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="role.ascx.cs"
    Inherits="includes_role" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                Role</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="lstrole" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" ControlToValidate="lstrole"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" class="col-sm-2 control-label">
                                                SalesTeam</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="ddlSalesTeamID" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;OutDoor</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkLTeamOutDoor.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamOutDoor" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;Closer</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkLTeamCloser.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamCloser" runat="server"></asp:CheckBox>
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                

                                <div class="form-group ">
                                    <asp:Label ID="Label15" runat="server" class="col-sm-2 control-label">
                                                
                                        Emp Status</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmployeeStatusID" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmployeeStatusID" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                UserName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtuname" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtuname"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="password" runat="server" >
                                    <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                Password</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Password has to be 5 characters !"
                                            ControlToValidate="txtpassword" ValidationExpression="^.{5,}$" Display="Dynamic" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" ControlToValidate="txtpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="confpassword" runat="server">
                                    <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                Confirm&nbsp;Password&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtcpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword"
                                            ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                            Display="Dynamic"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" ControlToValidate="txtcpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                Location</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                Start&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <%--  <input type="text" runat="server" id="txtStartTime" data-mask="99:99:99" class="form-control" placeholder="00:00:00">--%>
                                        <asp:TextBox ID="txtStartTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtStartTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtStartTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label21" runat="server" class="col-sm-2 control-label">
                                                End&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEndTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>

                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderET" runat="server" TargetControlID="txtEndTime" Mask="99:99:99" MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtEndTime" ControlExtender="MaskedEditExtenderET"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label22" runat="server" class="col-sm-2 control-label">
                                                Break&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtBreakTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtBreakTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlToValidate="txtBreakTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <asp:Label ID="Label24" runat="server" class="col-sm-2 control-label">
                                                Include&nbsp;in&nbsp;Lists</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkInclude.ClientID %>">
                                            <asp:CheckBox ID="chkInclude" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label25" runat="server" class="col-sm-2 control-label">
                                                Active&nbsp;Employee</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkActiveEmp.ClientID %>">
                                            <asp:CheckBox ID="chkActiveEmp" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" class="col-sm-2 control-label">
                                                Show Excel</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkshowexcel.ClientID %>">
                                            <asp:CheckBox ID="chkshowexcel" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" class="col-sm-2 control-label">
                                                Info</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpInfo" runat="server" TextMode="MultiLine" Rows="2" Width="500px" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divtaxfile" runat="server" visible="false">
                                    <asp:Label ID="Label28" runat="server" class="col-sm-2 control-label">
                                                TaxFileNumber</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTaxFileNumber" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divempabn" runat="server" visible="false">
                                    <asp:Label ID="Label29" runat="server" class="col-sm-2 control-label">
                                                EmpABN</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpABN" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divempaccontant" runat="server" visible="false">
                                    <asp:Label ID="Label30" runat="server" class="col-sm-2 control-label">
                                                EmpAccountName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpAccountName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divroster" runat="server" visible="false">
                                    <asp:Label ID="Label31" runat="server" class="col-sm-2 control-label">
                                                On&nbsp;Roster&nbsp;List</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkOnRoster" runat="server" />
                                        <label for="<%=chkOnRoster.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="divpayown" runat="server" visible="false">
                                    <asp:Label ID="Label32" runat="server" class="col-sm-2 control-label">
                                                Company&nbsp;Pays&nbsp;Super</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkPaysOwnSuper" runat="server" />
                                        <label for="<%=chkPaysOwnSuper.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group" id="divgstyown" runat="server" visible="false">
                                    <asp:Label ID="Label33" runat="server" class="col-sm-2 control-label">
                                                GST&nbsp;Payment</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkGSTPayment" runat="server"></asp:CheckBox>
                                        <label for="<%=chkGSTPayment.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <asp:Button CssClass="btn redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" />
                                    </div>
                                </div>
    </div>