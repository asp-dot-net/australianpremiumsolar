﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_imdeprec : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                DataTable dt = ClsDashboard.tblProjects_SelectFirstDep(st1.EmployeeID);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    //divdeprec.Attributes.Add("style", "overflow-y:scroll; height:350px;");
                    PanNoRecord.Visible = false;
                }
                else
                {
                    GridView1.Visible = false;
                PanNoRecord.Visible = true;
                    //SetNoRecords();
                    //PanNoRecord.Visible = true;
                    //divdeprec.Attributes.Add("style", "height:350px;");
                }
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dt = ClsDashboard.tblProjects_SelectFirstDep(st1.EmployeeID);
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "deprec")
        {
            hndProID.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();
        }
    }
    protected void ibtnAddComment_Click(object sender, EventArgs e)
    {
        if (txtDepRec.Text.Trim() != string.Empty)
        {
            string ProjectID = hndProID.Value;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            
            DataTable dtStatusDR = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "8");
            if (dtStatusDR.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("8", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateDepositReceived(ProjectID, txtDepRec.Text.Trim());

                bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
           
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}