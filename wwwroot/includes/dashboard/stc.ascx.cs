﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class includes_dashboard_stc : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("STC"))
            {
                rptPaperRem.DataSource = ClsDashboard.tblProjects_PaperRem_STC();
                rptPaperRem.DataBind();

                rptStCRem.DataSource = ClsDashboard.tblProjects_Applied_STC();
                rptStCRem.DataBind();
            }
        }
    }
}