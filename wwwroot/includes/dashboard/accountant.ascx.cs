﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_accountant : System.Web.UI.UserControl
{
    protected string jscharts1;
    protected string SiteURL;
    protected string jschartticks;
    static DataView dv;
    static DataView dv3;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("Accountant"))
            {
                BindGrid(0);
                BindPanelChart();
            }
        }
    }
    protected DataTable GetGridDataRefund()
    {
        DataTable dt1 = new DataTable();
        dt1 = ClsDashboard.tblProjectRefund_Acct();
      
        int iTotalRecords = dt1.Rows.Count;
        int iEndRecord = GridViewRefund.PageSize * (GridViewRefund.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewRefund.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt1;
    }

    protected DataTable GetGridDataElecInv()
    {
        DataTable dt2 = new DataTable();
        dt2 = ClsDashboard.tblProjectElecInv_Acct();

        //Response.Write(dt2.Rows.Count);
        //Response.End();

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridViewElecInv.PageSize * (GridViewElecInv.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewElecInv.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridDataRefund();
        dv = new DataView(dt1);


        if (dt1.Rows.Count > 0)
        {
            GridViewRefund.DataSource = dt1;
            GridViewRefund.DataBind();
        }


        DataTable dt2 = new DataTable();
        dt2 = GetGridDataElecInv();

        dv3 = new DataView(dt2);
        //Response.Write(dt2.Rows.Count);
        //Response.End();
        if (dt2.Rows.Count > 0)
        {
            GridViewElecInv.DataSource = dt2;
            GridViewElecInv.DataBind();
        }

    }

    protected void GridViewRefund_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewRefund.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridViewRefund.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }

    protected void GridViewRefund_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "refund")
        {
            string RefundID = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            ListItem item6 = new ListItem();
            item6.Text = "Select";
            item6.Value = "";
            ddlPayMethod.Items.Clear();
            ddlPayMethod.Items.Add(item6);

            ddlPayMethod.DataSource = ClsProjectSale.tblInvoicePayMethod_Select();
            ddlPayMethod.DataValueField = "InvoicePayMethodID";
            ddlPayMethod.DataTextField = "InvoicePayMethodABB";
            ddlPayMethod.DataMember = "InvoicePayMethodABB";
            ddlPayMethod.DataBind();

            hndRefundID.Value = RefundID;
        }
    }
    protected void ibtnAddComment_Onclick(object sender, EventArgs e)
    {
        string RefundID = hndRefundID.Value;
        string AccUserID = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string PaymentMode = ddlPayMethod.SelectedValue;
        string PaidDate = txtPaidDate.Text;
        string Remarks = txtRemarks.Text;

        bool suc = ClstblProjectRefund.tblProjectRefund_Update(RefundID, PaymentMode, PaidDate, AccUserID, Remarks);
        if (suc)
        {
            ModalPopupExtender2.Hide();
            ddlPayMethod.SelectedValue = "";
            txtPaidDate.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }
    }
    protected void ibtnCancelComment_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
    }
    protected void GridViewElecInv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewElecInv.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    void lbElecInv_Command(object sender, CommandEventArgs e)
    {
        GridViewElecInv.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }

    protected void GridViewRefund_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridViewElecInv_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lbElecInv_Command);
        }
    }

    protected void GridViewElecInv_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewElecInv.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewElecInv.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewElecInv.PageIndex - 2;
        page[1] = GridViewElecInv.PageIndex - 1;
        page[2] = GridViewElecInv.PageIndex;
        page[3] = GridViewElecInv.PageIndex + 1;
        page[4] = GridViewElecInv.PageIndex + 2;
        page[5] = GridViewElecInv.PageIndex + 3;
        page[6] = GridViewElecInv.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewElecInv.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewElecInv.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewElecInv.PageIndex == GridViewElecInv.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridViewElecInv.PageSize * (GridViewElecInv.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewElecInv.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    public void BindPanelChart()
    {
        DataTable dt = ClsDashboard.tblProjectsGraph_Acct();
        foreach (DataRow row in dt.Rows)
        {
            jscharts1 += "," + row["total"].ToString() + "";
            jschartticks += ",'" + row["InvCommonType"].ToString() + "'";
        }
        if (!string.IsNullOrEmpty(jschartticks))
        {
            jschartticks = jschartticks.Substring(1);
        }
        if (!string.IsNullOrEmpty(jscharts1))
        {
            jscharts1 = jscharts1.Substring(1);
        }
    }
    
    protected void GridViewRefund_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewRefund.BottomPagerRow;
        //GridViewRow gvrow = GridViewRefund.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewRefund.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewRefund.PageIndex - 2;
        page[1] = GridViewRefund.PageIndex - 1;
        page[2] = GridViewRefund.PageIndex;
        page[3] = GridViewRefund.PageIndex + 1;
        page[4] = GridViewRefund.PageIndex + 2;
        page[5] = GridViewRefund.PageIndex + 3;
        page[6] = GridViewRefund.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewRefund.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewRefund.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewRefund.PageIndex == GridViewRefund.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridViewElecInv.PageSize * (GridViewElecInv.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewElecInv.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    
  
}