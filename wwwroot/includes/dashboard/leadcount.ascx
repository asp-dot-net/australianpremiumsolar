﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="leadcount.ascx.cs" Inherits="includes_dashboard_leadcount" %>
<style>
    h4{
          font-weight: 600 !important;
    }  
</style>

<asp:UpdatePanel runat="server" ID="UpdateLead">
    <ContentTemplate>

        <div class="col-md-6">
            <div class="databox" style="margin-bottom: unset; height: unset;">
                <div class="databox-row bg-orange no-padding" style="border-radius: 10px 10px 0 0;">
                    <div>
                        <div runat="server" id="divemployee2">
                            <div class="databox-cell cell-3 text-align-right padding-20">
                                <div class="pull-right">
                                    <div class="btn-group"></div>
                                </div>
                            </div>
                        </div>
                        <div id="divemployee" runat="server">
                            <div class="databox-cell cell-3 text-align-right padding-10">
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <asp:DropDownList ID="ddlFollowUpEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                            <asp:ListItem Value="">Employee</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <%--<span class="databox-text white">13 DECEMBER</span>--%>
                            </div>
                            <asp:Button runat="server" ID="btnFollowupgo" OnClick="btnFollowupgo_Click" CssClass="btn btn-primary" Text="Go" />
                        </div>
                        <%--<div class="clear"></div>--%>
                    </div>
                </div>
                <div class="col-md-12" style="padding: 0px;">
                    <div class="table-responsive well" style="border-radius: 0 0 10px 10px;box-shadow: 0 0 10px rgba(0,0,0,.3);">
                        <div class="panel-body paddnone leadacntmain" id="divMain" runat="server">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-danger">
                                                    <asp:Literal ID="litUnhandledLead" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Unhandled</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-success">
                                                    <asp:Literal ID="litDepRec" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Dep Rec</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-success">
                                                    <asp:Literal ID="litActive" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Active</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-warning">
                                                    <asp:Literal ID="litProspect" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Prospect</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litCustomer" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Customer</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div >
                                            <div class="center-text">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="liTotal" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Total</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body paddnone" id="divPreInst" runat="server" visible="false">
                            <div class="row m-n">
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="m-t-xl">
                                                <h1 class="text-info"></h1>
                                                <div class="stats-title">
                                                    <h4>Dep Rec</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="m-t-xl">
                                                <h1 class="text-info"></h1>
                                                <div class="stats-title">
                                                    <h4>Active</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litJobBooked" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Job Booked</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litOnHold" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>On Hold</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body paddnone" id="divPostInst" runat="server" visible="false">
                            <div class="row m-n">
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text minheight40">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litJobBookedPost" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Job Booked</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text minheight40">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litInstComplete" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Inst Complete</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text minheight40">
                                                <h1 class="text-info">
                                                    <asp:Literal ID="litPaperRec" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Paper Rec Remaining</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="hpanel">
                                        <div class="panel-body">
                                            <div class="center-text minheight40">
                                                <h1 class="text-info ">
                                                    <asp:Literal ID="litDueAmount" runat="server"></asp:Literal></h1>
                                                <div class="stats-title">
                                                    <h4>Due Amount</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </ContentTemplate>
    <%--<Triggers>
        <asp:PostBackTrigger ControlID="btnFollowupgo" />
    </Triggers>--%>
</asp:UpdatePanel>
<script type="text/javascript">
    function doMyAction() {
        if ($.fn.select2 !== 'undefined') {
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
        }
    }
</script>
