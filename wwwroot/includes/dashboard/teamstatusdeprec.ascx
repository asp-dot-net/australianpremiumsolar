﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="teamstatusdeprec.ascx.cs" Inherits="includes_dashboard_teamstatusdeprec" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel runat="server" ID="UpdateTeamStatus">
    <ContentTemplate>
        <style type="text/css">
            .selected_row {
                background-color: #A1DCF2 !important;
            }
        </style>
        <script type="text/javascript">
            //$(function () {
            //    $("[id*=GridView1] td").bind("click", function () {
            //        var row = $(this).parent();
            //        $("[id*=GridView1] tr").each(function () {
            //            if ($(this)[0] != row[0]) {
            //                $("td", this).removeClass("selected_row");
            //            }
            //        });
            //        $("td", row).each(function () {
            //            if (!$(this).hasClass("selected_row")) {
            //                $(this).addClass("selected_row");
            //            } else {
            //                $(this).removeClass("selected_row");
            //            }
            //        });
            //    });
            //});
            function BindTeam() {
                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
            }
        </script>
        <%--<div class="col-xs-12 col-md-6">
            <div>
                <section class="panel">--%>

        <div class="panel-heading header bg-blue" style="display: none;">
            <div class="header bg-blue">
                <div class="pull-right">
                    <div class="btn-group">
                        <%--<button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-select">
                            <li><a href="#">
                                <input type="radio" name="b">
                                Month</a></li>
                            <li><a href="#">
                                <input type="radio" name="b">
                                Week</a></li>
                            <li><a href="#">
                                <input type="radio" name="b">
                                Day</a></li>
                        </ul>--%>
                        <div class="pull-right">
                            <div class="btn-group">
                                <asp:DropDownList ID="ddldays" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval btn-xs">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Day</asp:ListItem>
                                    <asp:ListItem Value="2">Week</asp:ListItem>
                                    <asp:ListItem Value="3">Month</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnGo" OnClick="btnGo_Click" CssClass="btn btn-primary btn-xs" Text="Go" />
                </div>
                <%--<div class="pull-left" style="color:#fff">
                            Team Status (1st Deposit Recieved)
                        </div>--%>
            </div>
        </div>
       
            <div class="col-md-6">
                <div class="databox" style="margin-bottom:unset;height:unset;">
                    <div class="databox-row bg-orange no-padding">
                        <%--<div class="databox-cell cell-1 text-align-center no-padding padding-top-5">
                            <span class="databox-number white"><i class="fa fa-bar-chart-o no-margin"></i></span>
                        </div>--%>
                        <div class="databox-cell cell-8 padding-top-5 padding-left-5 text-align-left">
                            <span class="databox-number white" style="font-size:17px;">Team Status (1st Deposit Recieved)</span>
                        </div>
                        <div class="databox-cell cell-3 text-align-right padding-10">
                            <div class="pull-right">
                                <div class="btn-group">
                                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval btn-xs">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="1" Selected="True">Day</asp:ListItem>
                                        <asp:ListItem Value="2">Week</asp:ListItem>
                                        <asp:ListItem Value="3">Month</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--<span class="databox-text white">13 DECEMBER</span>--%>
                        </div>
                        <asp:Button runat="server" ID="Button2" OnClick="btnGo_Click" CssClass="btn btn-primary btn-xs" Text="Go" />
                    </div>
                      </div>
                 <div class="col-md-12" style="padding: 0px;">
                        <div class="table-responsive well">
                            <table class="table table-hover">
                                <thead class="bordered-darkorange">
                                    <tr>
                                        <th data-toggle="class" class="th-sortable" width="30%" style="font-size:16px;">Team</th>
                                        <th style="font-size:16px;">No of Panel</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptTeam" runat="server" OnItemDataBound="rptTeam_ItemDataBound">
                                        <ItemTemplate>
                                            <tr style="font-size:15px;">
                                                <td>
                                                    <asp:HiddenField ID="hndSalesTeamID" runat="server" Value='<%# Eval("SalesTeamID") %>' />
                                                    <%# Eval("SalesTeam") %>
                                                </td>
                                                <asp:Repeater ID="rptTeamCount" runat="server" OnItemCommand="rptTeamCount_ItemCommand">
                                                    <ItemTemplate>
                                                        <td class="font-bold">
                                                            <asp:LinkButton ID="lnkTeam" runat="server" CommandName="detail" CommandArgument='<%# Eval("SalesTeamID") %>'><%#Eval("Count")%></asp:LinkButton>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
                   
              
      
        <%--     </section>
            </div>
        </div>--%>

        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
            CancelControlID="Button1">
        </cc1:ModalPopupExtender>
        <div id="myModal" runat="server" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div style="float: right">
                            <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        </div>
                        <h4 class="modal-title" id="myModalLabel">
                            <asp:Label ID="lblDetail" runat="server"></asp:Label>&nbsp;
                    Detail</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="tablescrolldiv">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="GridView1" DataKeyNames="EmployeeID" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="GridView1_PageIndexChanging"
                                        PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Employee">
                                                <ItemTemplate>
                                                    <%# Eval("Employee")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Number of Panels" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%# Eval("Count")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                        <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                        <EmptyDataRowStyle Font-Bold="True" />
                                        <RowStyle CssClass="GridviewScrollItem" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
        <asp:HiddenField ID="hndID" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
