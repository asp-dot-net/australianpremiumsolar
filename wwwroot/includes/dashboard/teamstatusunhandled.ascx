﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="teamstatusunhandled.ascx.cs" Inherits="includes_dashboard_teamstatusunhandled" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
<div class="col-md-6">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-rounded btn-default dropdown-toggle" data-toggle="dropdown"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-select">
                            <li><a href="#">
                                <input type="radio" name="b">
                                Month</a></li>
                            <li><a href="#">
                                <input type="radio" name="b">
                                Week</a></li>
                            <li><a href="#">
                                <input type="radio" name="b">
                                Day</a></li>
                        </ul>
                    </div>
                    <a class="btn btn-default btn-icon btn-rounded btn-sm marginleft3" href="#">Go</a> <%--<span class="badge bg-warning marginleft3">10</span>--%>
                </div>
                <a href="#">Team Status (Unhandled  Project)</a>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th data-toggle="class" class="th-sortable" width="30%">Team</th>
                            <th>Project</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptTeam" runat="server" OnItemDataBound="rptTeam_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hndSalesTeamID" runat="server" Value='<%# Eval("SalesTeamID") %>' />
                                        <%# Eval("SalesTeam") %>
                                    </td>
                                    <asp:Repeater ID="rptTeamCount" runat="server" OnItemCommand="rptTeamCount_ItemCommand">
                                        <ItemTemplate>
                                            <td class="font-bold">
                                                <asp:LinkButton ID="lnkTeam" runat="server" CommandName="detail" CommandArgument='<%# Eval("SalesTeamID") %>'><%#Eval("Count")%></asp:LinkButton>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>


<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
    CancelControlID="Button1">
</cc1:ModalPopupExtender>
<div id="myModal" runat="server" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <div style="float: right">
                <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                     </div>
                <h4 class="modal-title" id="myModalLabel">
                    <asp:Label ID="lblDetail" runat="server"></asp:Label>&nbsp;
                    Detail</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div class="tablescrolldiv">
                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                            <asp:GridView ID="GridView1" DataKeyNames="EmployeeID" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="GridView1_PageIndexChanging"
                                PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                <Columns>
                                    <asp:TemplateField HeaderText="Employee">
                                        <ItemTemplate>
                                            <%# Eval("Employee")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# Eval("Count")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                <EmptyDataRowStyle Font-Bold="True" />
                                <RowStyle CssClass="GridviewScrollItem" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULL" Style="display: none;" runat="server" />
<asp:HiddenField ID="hndID" runat="server" />
