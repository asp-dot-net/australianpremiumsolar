﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
 
public partial class includes_controls_contactaccount : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
        //BindCreateAccount();
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
    }
    public void BindCreateAccount()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stCont.CustomerID);

            string userid = "";
            if (!string.IsNullOrEmpty(stCont.userid))
            {
                userid = stCont.userid;
            }

            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager"))
            {
                if (stCust.CustTypeID == "3" || stCust.CustTypeID == "4")
                {
                    PanAddUpdate.Enabled = true;
                }
                else
                {
                    PanAddUpdate.Enabled = false;
                }
            }
            else
            {
                PanAddUpdate.Enabled = false;
            }

            if (stCont.userid != string.Empty)
            {
                divAdd.Visible = false;
                txtuname.Enabled = false;
                txtpassword.Enabled = false;
                txtcpassword.Enabled = false;
                txtuname.Text = stCont.UserName;
                hidepass.Visible = false;
                hidecpass.Visible = false;
            }
            else
            {
                txtuname.Enabled = true;
                txtpassword.Enabled = true;
                txtcpassword.Enabled = true;
                divAdd.Visible = true;
                hidepass.Visible = true;
                hidecpass.Visible = true;
                //txtuname.Text = "";
            }
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", " <script>alert('');</script>");
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string username = txtuname.Text.Trim();
        string password = txtcpassword.Text.Trim();

        int exist = ClstblEmployees.Spaspnet_UsersAddDataExist(username);
        if (exist == 0)
        {
            try
            {
                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
                SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);

                Membership.CreateUser(username, password, stCont.ContEmail);
                string userid = Membership.GetUser(username).ProviderUserKey.ToString();
                bool success2 = ClstblContacts.tblContacts_Update_UserId(Request.QueryString["contid"], Membership.GetUser(username).ProviderUserKey.ToString());

                if (stCust.CustTypeID == "3")
                {
                    Roles.AddUserToRole(username, "Customer");
                }
                if (stCust.CustTypeID == "4")
                {
                    Roles.AddUserToRole(username, "Installer");
                }

                if (success2)
                {
                    divAdd.Visible = false;
                    SetAdd1();
                    //PanSuccess.Visible = true;
                    txtuname.Enabled = false;
                    txtpassword.Enabled = false;
                    txtcpassword.Enabled = false;
                    PanError.Visible = false;
                }
                else
                {
                    btnAdd.Visible = true;
                    SetError1();
                    //PanError.Visible = true;
                }
            }
            catch (MembershipCreateUserException err)
            {
                SetError1();
                //PanError.Visible = true;
                lblError.Visible = true;
                lblError.Text = GetErrorMessage(err.StatusCode);
            }
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
            lblError.Text = "User with this name already exists. Please enter a different User Name.";
        }
    }

    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "User with this name already exists. Please enter a different User Name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A E-mail with address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}