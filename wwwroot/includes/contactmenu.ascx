﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contactmenu.ascx.cs" Inherits="includes_contactmenu" %>
<div class="paddnone">
    <div class="spicaltab">
        <div class="projecttabnavi">
            <ul>
                <li id="li_updateDetail" runat="server" visible="true">
                    <asp:HyperLink ID="hypDetail" runat="server">Detail</asp:HyperLink>
                </li>
                <li id="li_updateNotes" runat="server" visible="true">
                    <asp:HyperLink ID="hypNotes" runat="server">Notes</asp:HyperLink>
                </li>
                <li id="li_updatePromo" runat="server" visible="true">
                    <asp:HyperLink ID="hypPromo" runat="server">Promo</asp:HyperLink>
                </li>
                <li id="li_updateInfo" runat="server" visible="true">
                    <asp:HyperLink ID="hypInfo" runat="server">Info</asp:HyperLink>
                </li>
                <li id="li_createaccount" runat="server" visible="true">
                    <asp:HyperLink ID="HypCreateAccount" runat="server">Create Account</asp:HyperLink>
                </li>
            </ul>
            <div class="clear">
            </div>

        </div>
    </div>
</div>
