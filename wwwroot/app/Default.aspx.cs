﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class app_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      //  Response.Write(Request.Form["firstName"]);
    }
    [WebMethod]
    public static string CustomersInsert(string userid, string Salutation, string FirstName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area)
    {

        string data = "";
        string ResCom = "1";
        string CustTypeID = "3";
        string CustSourceID = "21";
        string CustSourceSubID = "";
        string StreetCity = "";
        string StreetState = "";
        string StreetPostCode = "";
        string PostalAddress = "";
        string PostalCity = "";
        string PostalState = "";
        string PostalPostCode = "";


        string CustFax = "";
        string CustNotes = "";
        string CustWebSite = "";
        string CustWebSiteLink = "";
        string Customer = FirstName + " " + LastName;
        string Country = "Australia";
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;
        string CustEnteredBy = st.EmployeeID;
        string BranchLocation = "";
        //string ResCom = false.ToString();
        //if (rblResCom1.Checked == true)
        //{
        //    ResCom = "1";
        //}
        //if (rblResCom2.Checked == true)
        //{
        //    ResCom = "2";
        //}


        //if (rblArea1.Checked == true)
        //{
        //    Area = "1";
        //}
        //if (rblArea2.Checked == true)
        //{
        //    Area = "2";
        //}
        if (Salutation == "0")
        {
            Salutation = "";
        }
        if (FirstName == "0")
        {
            FirstName = "";
        }

        if (LastName == "0")
        {
            LastName = "";
        }
        if (Mobile == "0")
        {
            Mobile = "";
        }
        if (Email == "0")
        {
            Email = "";
        }

        if (StreetAddress == "0")
        {
            StreetAddress = "";
        }
        if (Phone == "0")
        {
            Phone = "";
        }
        if (Area == "0")
        {
            Area = "";
        }

        string CustAltPhone = Mobile;


        int success = 0;
        success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, Phone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);
        int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), Salutation.Trim(), FirstName.Trim(), LastName.Trim(), Mobile, Email, EmployeeID, CustEnteredBy);
        int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");
        //ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);
        //bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
        bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), StreetAddress);

        //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);

        bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), StreetAddress);

        //bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), chkformbayid.Checked.ToString());

        ////----tblcontact contphone update - jigar chokshi
        bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), Phone);
     
        //if (txtD2DAppDate.Text != string.Empty)
        //{
        //    bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
        //    bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
        //}

        int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
        string LogReport = "";

        if (Convert.ToString(suc) != "")
        {
            int exist = ClstblCustomers.tblContacts_ExistName(FirstName + ' ' + LastName);
            if (exist == 1)
            {
                LogReport = "Update tblCustLogReport set Name='" + FirstName + ' ' + LastName + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
            }

            int exist2 = ClstblCustomers.tblContacts_ExistMobile(Mobile);
            if (exist2 == 1)
            {
                LogReport = "Update tblCustLogReport set Mobile='" + Mobile + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
            }

            int exist3 = ClstblCustomers.tblContacts_ExistEmail(Email);
            if (exist3 == 1)
            {
                LogReport = "Update tblCustLogReport set Email='" + Email + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
            }

            int exist4 = ClstblCustomers.tblCustomers_ExistPhone(Phone);
            if (exist4 == 1)
            {
                LogReport = "Update tblCustLogReport set LocalNo='" + Phone + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
            }
            ClsProjectSale.ap_form_element_execute(LogReport);
        }
        data = success.ToString();

        return data;
    }

    [WebMethod]
    public static string CustomersUpdate(string CustomerID, string userid, string Salutation, string FirstName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area)
    {
        string data = "";
        string ResCom = "1";
        string CustTypeID = "3";
        string CustSourceID = "21";
        string CustSourceSubID = "";
        string StreetCity = "";
        string StreetState = "";
        string StreetPostCode = "";
        string PostalAddress = "";
        string PostalCity = "";
        string PostalState = "";
        string PostalPostCode = "";


        string CustFax = "";
        string CustNotes = "";
        string CustWebSite = "";
        string CustWebSiteLink = "";
        string Customer = FirstName + " " + LastName;
        string Country = "Australia";
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;
        string CustEnteredBy = st.EmployeeID;
        string BranchLocation = "";


        if (Salutation == "0")
        {
            Salutation = "";
        }
        if (FirstName == "0")
        {
            FirstName = "";
        }

        if (LastName == "0")
        {
            LastName = "";
        }
        if (Mobile == "0")
        {
            Mobile = "";
        }
        if (Email == "0")
        {
            Email = "";
        }

        if (StreetAddress == "0")
        {
            StreetAddress = "";
        }
        if (Phone == "0")
        {
            Phone = "";
        }
        if (Area == "0")
        {
            Area = "";
        }

        bool success = false;
        string CustAltPhone = Mobile;

        bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(CustomerID, StreetAddress);
        success = ClstblCustomers.tblCustomers_Update(CustomerID, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, Phone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, Customer);
        // bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(CustomerID, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
        bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(CustomerID, StreetAddress);
        // bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(CustomerID, chkformbayid.Checked.ToString());
        //----tblcontact contphone update - jigar chokshi
        bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(CustomerID, Phone);
        bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(CustomerID, Mobile);

        string LogReport = "";
        int exist = ClstblCustomers.tblContacts_ExistName(FirstName + ' ' + LastName);
        if (exist == 1)
        {
            LogReport = "Update tblCustLogReport set Name='" + FirstName + ' ' + LastName + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist2 = ClstblCustomers.tblContacts_ExistMobile(Mobile);
        if (exist2 == 1)
        {
            LogReport = "Update tblCustLogReport set Mobile='" + Mobile + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist3 = ClstblCustomers.tblContacts_ExistEmail(Email);
        if (exist3 == 1)
        {
            LogReport = "Update tblCustLogReport set Email='" + Email + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(Phone);
        if (exist4 == 1)
        {
            LogReport = "Update tblCustLogReport set LocalNo='" + Phone + "' WHERE CustomerID='" + CustomerID + "'";
        }
        ClsProjectSale.ap_form_element_execute(LogReport);
        data += success.ToString();

        return data;
    }
}