<%@ Page Language="C#" MasterPageFile="~/admin/templates/loginmaster.master" AutoEventWireup="true"
    CodeFile="default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <asp:Login ID="login1" runat="server" DestinationPageUrl="~/admin/adminfiles/dashboard.aspx" OnLoggedIn="Login1_LoggedIn" DisplayRememberMe="true" Width="100%">
            <LayoutTemplate>
                <div class="app-container app-theme-white body-tabs-shadow loginPage">
                    <div class="app-container">
                        <div class="h-100">
                            <div class="h-100 no-gutters row">
                                <div class="d-none d-lg-block col-lg-4">
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center" tabindex="-1">
                                        <%-- <div class="leftSectionImage"></div>
                                        <div class="app-logo"></div>
                                        <div class="leftSectionContent">
                                            <h1>Welcome to Australian Premium Solar</h1>
                                            <p>The ultimate solar company for solar installation and wholesale</p>
                                        </div> --%>

                                       
                                    </div>
                                </div>
                                <div class="h-100 position-relative d-flex justify-content-center align-items-center col-md-12 col-lg-8">
                                    <h6 class="signUpLink">Don't have an account yet? <a href="javascript:void(0);" class="text-primary">Sign up!</a></h6>
                                    <div class="mx-auto app-login-box col-sm-12 col-md-6 col-lg-5">

                                        <h4 class="text-center signIn">Sign In</h4>

                                        <div>
                                            <form class="">
                                                <div class="form-row">
                                                    <div class="col-md-12 form-group">
                                                        <div class="username">
                                                            <asp:TextBox ID="UserName" runat="server" placeholder="Username" autofocus class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator
                                                                ID="UserNameRequired" runat="server" Display="dynamic" ControlToValidate="UserName" ErrorMessage=""
                                                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 form-group">
                                                        <div class="password">
                                                            <asp:TextBox ID="Password" runat="server" Display="dynamic" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator
                                                                ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage=""
                                                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="position-relative form-check" style="display: none;">
                                                    <asp:CheckBox ID="RememberMe" runat="server" class="form-check-input" />
                                                    <label class="form-check-label">Keep me logged in</label>
                                                </div>

                                                <div class="d-flex justify-content-between">
                                                    <asp:HyperLink ID="HyperLink1" class="btn-lg btn btn-link forgotpassword" runat="server" NavigateUrl="~/admin/forgotpassword.aspx">Forgot Password?</asp:HyperLink>
                                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" class="btn btn-primary btn-lg"
                                                        Text="Sign In" ValidationGroup="Login1" />
                                                </div>
                                            </form>
                                        </div>
                                        <div class="divider1"><span>OR</span></div>
                                        <div class="socialMediaSection d-flex justify-content-between">
                                            <a class="mb-2 mr-2 btn-icon btn btn-primary facebook">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
                                            <a class="mb-2 mr-2 btn-icon btn btn-primary twitter"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a>
                                            <a class="mb-2 mr-2 btn-icon btn btn-primary google"><i class="fa fa-google" aria-hidden="true"></i>Google</a>
                                        </div>
                                    </div>
                                     <ul class="copyRightFooter">
                                            <li>&copy; Copyright APS</li>
                                            <li><a href="#">Privacy</a><a href="#">Legal</a><a href="#">Contact</a></li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <div class="center-div">
                    <div class="login-form">
                        <div class="login-logo">
                            <img src="images/logo_Login.png">
                        </div>
                        <h2>Portal login</h2>
                        <div class="in">
                            <asp:TextBox ID="UserName" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="UserNameRequired" runat="server" Display="dynamic" ControlToValidate="UserName" ErrorMessage=""
                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="in">
                            <asp:TextBox ID="Password" runat="server" Display="dynamic" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage=""
                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="loginbox-forgot">
                            <div class="checkbox">
                                <label>
                                    <asp:CheckBox ID="RememberMe" runat="server" />
                                    <span class="text">Remember me next time.</span>
                                </label>
                            </div>
                        </div>
                        <p class="center">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/admin/forgotpassword.aspx">Forgot Password?</asp:HyperLink>
                        </p>
                        <div class="login-btn">
                            <asp:Button ID="LoginButton" runat="server" CommandName="Login" class="btn-col"
                                Text="Submit" ValidationGroup="Login1" />
                        </div>
                    </div>
                </div> --%>
            </LayoutTemplate>
        </asp:Login>
    </div>
    <%-- <div class="text-center copytext">2017 Copyright <%=SiteName %> </div> --%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=login1.FindControl("LoginButton").ClientID %>').click(function () {
                alert()
                formValidate();
            });
        });
    </script>


</asp:Content>
