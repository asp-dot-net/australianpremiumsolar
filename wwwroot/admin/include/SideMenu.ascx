﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideMenu.ascx.cs" Inherits="admin_include_SideMenu" %>




<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <%--<ul class="vertical-nav-menu">
                    <li class="app-sidebar__heading">Menu</li>
                    <li>
                        <a href="#">
                            <i class="menu-icon pe-7s-home metismenu-icon"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="#">
                           <i class="menu-icon pe-7s-global metismenu-icon"></i>
                            Master
                            <i class="fa fa-angle-down metismenu-state-icon" aria-hidden="true"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Locations
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Source
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Source Sub
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="app-sidebar__heading">Menu</li>
                    <li>
                        <a href="#">
                            <i class="menu-icon pe-7s-home metismenu-icon"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="#">
                           <i class="menu-icon pe-7s-global metismenu-icon"></i>
                            Master
                            <i class="fa fa-angle-down metismenu-state-icon" aria-hidden="true"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Locations
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Source
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon">
                                    </i>Company Source Sub
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>--%>

            <ul class="vertical-nav-menu">
                <asp:Repeater runat="server" ID="menu1" OnItemDataBound="menu1_onitemdatabound" OnItemCommand="menu1_onitemcommand">
                    <ItemTemplate>
                        <li class="app-sidebar__heading" id="lititle" runat="server"><%# Eval("title")%></li>
                        <li id="limain" runat="server">
                            <asp:HiddenField ID="hdndesc1" Value='<%# Eval("description").ToString()%>' runat="server" />
                            <asp:HiddenField ID="hdnmain" runat="server" />
                            <asp:LinkButton ID="hdnheadermenu" CssClass="menu-dropdown" runat="server" CommandArgument='<%# Eval("url") %>' Target="_blank" href='<%# Eval("url") %>'
                                CausesValidation="false">
                                <i class="menu-icon <%# Eval("description").ToString().Split(',').Count()>1?"menu-icon "+Eval("description").ToString().Split(',')[1]:""%> metismenu-icon"></i>
                                <%# Eval("title")%>
                                <i id="iclass" runat="server" class="fa fa-angle-down metismenu-state-icon" visible="false" aria-hidden="true" />
                            </asp:LinkButton>

                            <ul id="UL1" runat="server">
                                <asp:Repeater ID="repsubmenu" runat="server" OnItemDataBound="repsubmenu_OnItemDataBound">
                                    <ItemTemplate>
                                        <li id="lisub" runat="server">
                                            <asp:HiddenField ID="hdndesc" Value='<%# Eval("description").ToString()%>' runat="server" />
                                            <asp:HyperLink ID="hypsubmenu" runat="server" NavigateUrl='<%# Eval("Url") %>' Target="_blank">
                                                <i class="metismenu-icon"></i><%# Eval("title")%>
                                            </asp:HyperLink>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>

            <%--<ul class="vertical-nav-menu">
                <asp:Repeater runat="server" ID="menu1" OnItemDataBound="menu1_onitemdatabound" OnItemCommand="menu1_onitemcommand">
                    <ItemTemplate>
                        <li id="limain" runat="server">

                            <asp:HiddenField ID="hdndesc1" Value='<%# Eval("description").ToString()%>' runat="server" />

                            <asp:HiddenField ID="hdnmain" runat="server" />
                            <asp:LinkButton ID="hdnheadermenu" CssClass="menu-dropdown" runat="server" CommandArgument='<%# Eval("url") %>' Target="_blank" href='<%# Eval("url") %>'
                                CausesValidation="false">
                                <i class='<%# Eval("description").ToString().Split(',').Count()>1?"menu-icon fa "+Eval("description").ToString().Split(',')[1]:""%>'></i><span class="menu-text"><%# Eval("title")%></span>
                                <asp:Label CssClass="fa arrow" ID="lblarraow" runat="server" Visible="false"></asp:Label>
                            </asp:LinkButton>

                            </a>
                                    <ul id="UL1" runat="server" class="submenu mm-collapse">
                                        <asp:Repeater ID="repsubmenu" runat="server" OnItemDataBound="repsubmenu_OnItemDataBound">
                                            <ItemTemplate>
                                                <li id="lisub" runat="server">
                                                    <asp:HyperLink ID="hypsubmenu" runat="server" NavigateUrl='<%# Eval("Url") %>' Target="_blank" Style="text-transform: capitalize;">
                                                   <span class="menu-text"><%# Eval("title")%></span>
                                                    </asp:HyperLink>

                                                    <asp:HiddenField ID="hdndesc" Value='<%# Eval("description").ToString()%>' runat="server" />
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>--%>
        </div>
    </div>
</div>



