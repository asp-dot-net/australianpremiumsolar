﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="admin_include_Header" %>
<!-- Navbar -->
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
<%-- <script src="<%=Siteurl %>admin/vendor/flot2/examples/shared/jquery-ui/jquery-ui.min.js"></script>--%>

<style>
    .navbar .navbar-brand small img {
        height: 30px !important;
        width: 150px !important;
        padding-top: 6px !important;
    }
</style>

<script>
    function getimage(input) { 

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#<%=Image1.ClientID %>')
                    .attr('src', e.target.result)
                    .width(128)
                    .height(128)
                    .title('');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="app-header__content">
        <div class="app-header-left">
            <div class="search-wrapper">
                <div class="input-holder">
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="search-input" placeholder="Type to search"></asp:TextBox>
                    <asp:LinkButton class="search-icon" ID="btnSearch" runat="server" OnClick="btnSearch_Click"><span></span></asp:LinkButton>
                    <%-- <input type="text" class="search-input" placeholder="Type to search">
                    <a class="search-icon"><span></span></a>--%>
                </div>
                <a class="close"></a>
            </div>
        </div>
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <asp:Image Width="42" class="rounded-circle" ID="panelImg" runat="server" />
                                    <%--<img width="42" class="rounded-circle" src="/assets/images/avatars/2.jpg" alt="">--%>
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header dispnone">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            <div class="menu-header-image opacity-2" style="background-image: url('/assets/images/dropdown-header/city3.jpg');"></div>
                                            <div class="menu-header-content text-left">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper flexColumn">
                                                        <div class="widget-content-left mr-3">
                                                            <asp:Image runat="server" ID="Image2" class="rounded-circle" />
                                                        </div>
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">
                                                                <asp:Literal ID="lblusername1" runat="server"></asp:Literal>
                                                            </div>
                                                            <%-- <div class="widget-subheading opacity-8">
                                                                A short profile description
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="scroll-area-xs" style="height: auto;">
                                        <div class="scrollbar-container ps">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <asp:LinkButton ID="panelImgLinkBtn" runat="server" CommandArgument="Panel" OnClick="panelImgLinkBtn_Click">
                                         Aavtar
                                            </asp:LinkButton>
                                        </li>
                                        <li class="nav-item">
                                            <asp:LinkButton ID="btnprofile" runat="server" CommandArgument="Panel" OnClick="btnprofile_Click">
                                         Profile
                                            </asp:LinkButton>

                                            <%--<a href="" class="nav-link">Profile
                                            </a>--%>
                                        </li>
                                        <li class="nav-item">
                                            <a href="javascript:void(0);" class="nav-link">Notifications
                                                            <div class="ml-auto badge badge-warning">
                                                                512
                                                            </div>
                                            </a>
                                        </li>

                                    </ul>
                                    </div>
                                    </div>

                                    <ul class="nav flex-column">
                                        <li class="nav-item-divider
                                            nav-item"></li>
                                        <li class="nav-item">
                                            <asp:LinkButton ID="singout" runat="server" CssClass="nav-link" OnClick="singout_Click">Logout</asp:LinkButton>
                                            <%--  <a href="#" class="nav-link">Logout</a>--%>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                <asp:Literal ID="lblusername" runat="server"></asp:Literal>
                            </div>
                            <div class="widget-subheading">
                                <asp:Literal ID="lblrolename" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="widget-content-right header-user-info ml-3">
                            <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example">
                                <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--<div class="navbar " id="header" style="display: none;">
    <div class="navbar-inner">
        <div class="navbar-container">
            <div class="navbar-header pull-left" id="logo">
                <a href="#" class="navbar-brand"></a>
            </div>

            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="collapse-icon fa fa-bars"></i>
            </div>

            <div class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search" style="padding: 0px 80px;">
                <div class="form-group">
                    <asp:Panel runat="server" ID="PanSearch" DefaultButton="btnSearch">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <asp:LinkButton runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-sm bg-white b-white btn-icon"><i class="fa fa-search"></i></asp:LinkButton>

                            </span>
                            <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control input-sm no-border" placeholder="Search apps, projects..."></asp:TextBox>

                        </div>
                    </asp:Panel>
                </div>
            </div>

            <div class="navbar-header pull-right">

                <div class="navbar-account">
                    <ul class="account-area">
                        <li>

                            <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                <div class="avatar" title="View your public profile" style="margin-right: 40px">
                                    <asp:Image runat="server" ID="Image2" class="avatar" Height="29px" Width="29px" />
                                </div>
                                <section>
                                    <h2><span class="profile"><span>
                                        <asp:Label ID="lblusername" runat="server" Text=""></asp:Label></span></span></h2>
                                </section>
                            </a>
                            <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                <li class="email" style="height: 30px; text-align: center; padding-top: 5px; color: black">
                                    <asp:Label ID="lbluseremail" runat="server" Text=""></asp:Label></li>
                                <li>
                                    <div class="avatar-area">
                                        <asp:LinkButton ID="panelImgLinkBtn" runat="server" CommandArgument="Panel" OnClick="panelImgLinkBtn_Click">
                                            <asp:Image runat="server" ID="panelImg" class="avatar" />
                                            <span class="caption" style="margin-left: 2px;">Change Profile</span>
                                        </asp:LinkButton>
                                    </div>
                                </li>
                                <li class="edit">
                                    <a href="<%=Siteurl %>admin/Profile/Profile.aspx" class="pull-left">Profile</a>

                                </li>
                                <li class="dropdown-footer">
                                    <asp:LinkButton ID="singout" runat="server" OnClick="singout_Click"> Sign out</asp:LinkButton>

                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>--%>
<!-- /Navbar -->


<cc1:ModalPopupExtender ID="ModalPopupExtenderDetail" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
    CancelControlID="lnkcancel">
</cc1:ModalPopupExtender>
<div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup">
    <div class="modal-dialog" style="width: 600px">
        <div class="modal-content">
            <div class="color-line printorder "></div>
            <div class="modal-header printorder">
            <div class="modalHead">
            <h4 class="modal-title" id="myModalLabel"><i class="pe-7s-user popupIcon"></i>Change Profile</h4>
                <div>
                    <asp:LinkButton ID="lnkcancel" runat="server" type="button" class="btn btn-danger btngray" data-dismiss="modal">
                        <i class="fa fa-times"></i>Close
                    </asp:LinkButton>
                </div>
            </div>
            </div>

            <div class="modal-body avtarPopup" runat="server" id="divdetail">
                <div class="">
                    <div class="formainline formGrid">
                        <div class="">
                            <div class="" style="background: none!important;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span class="name disblock dispnone">
                                                <label class="control-label">
                                                </label>
                                            </span>
                                            <span>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table  quotetable">


                                                    <tr>
                                                        <td style="width: 100%; border-top: none" class="dispnone"><label class="control-label">Change Profile</label></td>
                                                        <td style="width: 100%; border-top: none;    padding: 0 !important;">

                                                            <div class="dateimgarea fileuploadmain">
                                                                <span class="dateimg">
                                                                    <span class="file-input btn btn-azure btn-file">
                                                                        <asp:Image runat="server" ID="Image1" class="avatar" Style="width: 128px; height: 128px;" />
                                                                        <span class="caption">
                                                                        <label class="custom-fileupload">
                                                                            
                                                                           <asp:FileUpload ID="fuProfile" onchange="getimage(this)" runat="server" class="custom-file-input"/>
                                                                            <span class="custom-file-control form-control-file form-control"></span>
                                                                            <span class="btnbox">Upload file</span>
                                                                        </label>
                                                                        </span>
                                                                    </span>
                                                                </span>

                                                            </div>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="fileis"
                                                                ControlToValidate="fuProfile" Display="Dynamic" ForeColor="Red" ErrorMessage="Upload a valid file"
                                                                ValidationExpression="^.+(.jpg|.JPG|.jpeg|.JPEG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
                                                            <span>&nbsp;&nbsp;<small><i> [Note:only jpg, jpeg, gif, png, bmp, ico]</i></small></span>
                                                        </td>

                                                    </tr>

                                                </table>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="textRight col-md-12">
                                        <asp:Button class="btn largeButton greenBtn btnsaveicon" ID="btnSave" ValidationGroup="fileis" CausesValidation="true" runat="server" OnClick="btnSave_Click" Text="Update" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:Button ID="btnNULL" Style="display: none;" runat="server" />