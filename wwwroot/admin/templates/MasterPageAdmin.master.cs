using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class BossAdmin_MasterPageAdmin : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //   StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery);
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        if (!IsPostBack)
        {
            SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            System.IO.FileInfo image = new System.IO.FileInfo(Request.PhysicalApplicationPath + "/userfiles/sitelogo/" + st.sitelogoupload);
            bool isfileUploaded = image.Exists;
        }

        if (!Request.IsAuthenticated)
        {
            Response.End();
            Response.Redirect("~/admin/Default.aspx");
        }

        switch (pagename.ToLower())
        {
            //case "dashboard":
            //    dash.Visible = true;
            //    Dashboard.Attributes.Add("class", "activePriNav");
            //    break;

            //case "adminutilities":
            //    divutility.Visible = true;
            //    hyputilities.CssClass = "menuOn";
            //    break;

            //case "adminsiteconfiguration":
            //     divutility.Visible = true;
            //     hypsiteconfig.CssClass = "menuOn";
            //    break;

            //case "announcement":
            //    divannouncement.Visible = true;
            //    hypannounctment.CssClass = "menuOn";
            //    liannouncement.Attributes.Add("class", "activePriNav");
            //    break;

            //case "news":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "newsdetail":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "demo":
            //    divdemo.Visible = true;
            //    liDemo.Attributes.Add("class", "activePriNav");
            //    hypdemo.CssClass = "menuOn";
            //    break;
        }


        if (pagename != "profile")
        {
            // secpage.Attributes.Add("class", "scrollable padder shdoimg");
        }
        else
        {
            //   secpage.Attributes.Add("class", "scrollable shdoimg");
        }


        //////title

        if (pagename == "profile")
        {
            pagetitle.Text = "APS | Profile";
        }
        if (pagename == "dashboard")
        {
            pagetitle.Text = "APS | Dashboard";
        }
        if (pagename == "setting")
        {
            pagetitle.Text = "APS | Setting";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "APS | Profile";
        }
        if (pagename == "help")
        {
            pagetitle.Text = "APS | Help";
        }
        if (pagename == "project")
        {
            pagetitle.Text = "APS | Project";
        }
        if (pagename == "installations")
        {
            pagetitle.Text = "APS | Installation";
        }
        if (pagename == "invoicesissued")
        {
            pagetitle.Text = "APS | Invoices Issued";
        }
        if (pagename == "invoicespaid")
        {
            pagetitle.Text = "APS | Invoices Paid";
        }
        if (pagename == "lead")
        {
            pagetitle.Text = "APS | Lead";
        }
        if (pagename == "lteamcalendar")
        {
            pagetitle.Text = "APS | Team Calender";
        }
        if (pagename == "installercalendar")
        {
            pagetitle.Text = "APS | Installer Calender";
        }


        #region master
        if (pagename == "companylocations")
        {
            pagetitle.Text = "APS | Company Locations";
        }
        if (pagename == "custsource")
        {
            pagetitle.Text = "APS | Company Source";
        }
        if (pagename == "custsourcesub")
        {
            pagetitle.Text = "APS | Company Source Sub";
        }
        if (pagename == "CustType")
        {
            pagetitle.Text = "APS | Company Type";
        }
        if (pagename == "elecdistributor")
        {
            pagetitle.Text = "APS | Elec Distributor";
        }
        if (pagename == "elecretailer")
        {
            pagetitle.Text = "APS | Elec Retailer";
        }
        if (pagename == "employee")
        {
            pagetitle.Text = "APS | Employee";
        }
        if (pagename == "FinanceWith")
        {
            pagetitle.Text = "APS | Finance With";
        }
        if (pagename == "housetype")
        {
            pagetitle.Text = "APS | House Type";
        }
        if (pagename == "leadcancelreason")
        {
            pagetitle.Text = "APS | Lead Cancel Reason";
        }
        if (pagename == "mtcereason")
        {
            pagetitle.Text = "APS | Mtce Reasons";
        }
        if (pagename == "mtcereasonsub")
        {
            pagetitle.Text = "APS | Mtce Reasons Sub";
        }
        if (pagename == "projectcancel")
        {
            pagetitle.Text = "APS | Project Cancel";
        }
        if (pagename == "projectonhold")
        {
            pagetitle.Text = "APS | Project On-Hold";
        }
        if (pagename == "projectstatus")
        {
            pagetitle.Text = "APS | Project Status";
        }
        if (pagename == "projecttrackers")
        {
            pagetitle.Text = "APS | Project Trackers";
        }
        if (pagename == "projecttypes")
        {
            pagetitle.Text = "APS | Project Types";
        }
        if (pagename == "projectvariations")
        {
            pagetitle.Text = "APS | Project Variations";
        }
        if (pagename == "promotiontype")
        {
            pagetitle.Text = "APS | Promotion Type";
        }
        if (pagename == "prospectcategory")
        {
            pagetitle.Text = "APS | Prospect Categories";
        }
        if (pagename == "roofangles")
        {
            pagetitle.Text = "APS | Roof Angles";
        }
        if (pagename == "roottye")
        {
            pagetitle.Text = "APS | Root Type";
        }
        if (pagename == "salesteams")
        {
            pagetitle.Text = "APS | Sales Team";
        }
        if (pagename == "StockCategory")
        {
            pagetitle.Text = "APS | Stock Category";
        }
        if (pagename == "transactiontypes")
        {
            pagetitle.Text = "APS | Transaction Types";
        }
        if (pagename == "paymenttype")
        {
            pagetitle.Text = "APS | Finance Payment Type";
        }
        if (pagename == "invcommontype")
        {
            pagetitle.Text = "APS | Invoice Type";
        }
        if (pagename == "postcodes")
        {
            pagetitle.Text = "APS | Post Codes";
        }
        if (pagename == "custinstusers")
        {
            pagetitle.Text = "APS | Cust/Inst Users";
        }
        if (pagename == "newsletter")
        {
            pagetitle.Text = "APS | News Letter";
        }
        if (pagename == "logintracker")
        {
            pagetitle.Text = "APS | Login Tracker";
        }
        if (pagename == "leftempprojects")
        {
            pagetitle.Text = "APS | Left Employee Projects";
        }
        if (pagename == "lteamtime")
        {
            pagetitle.Text = "APS | Manage L-Team App Time";
        }
        if (pagename == "refundoptions")
        {
            pagetitle.Text = "APS | Refund Options";
        }

        #endregion

        if (pagename == "company" || pagename == "Company")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "APS | Company";
        }
        if (pagename == "contacts")
        {
            pagetitle.Text = "APS | Contact";
        }

        if (pagename == "leadtracker")
        {
            pagetitle.Text = "APS | Lead Tracker";
        }
        if (pagename == "gridconnectiontracker")
        {
            pagetitle.Text = "APS | GridConnection Tracker.";
        }

        if (pagename == "lteamtracker")
        {
            pagetitle.Text = "APS | Team Lead Tracker";
        }
        if (pagename == "stctracker")
        {
            pagetitle.Text = "APS | STC Tracker";
        }
        if (pagename == "instinvoice")
        {
            pagetitle.Text = "APS | Inst Invoice Tracker";
        }
        if (pagename == "salesinvoice")
        {
            pagetitle.Text = "APS | Sales Invoice Tracker";
        }
        if (pagename == "installbookingtracker")
        {
            pagetitle.Text = "APS | Install Booking Tracker";
        }
        if (pagename == "ApplicationTracker")
        {
            pagetitle.Text = "APS | Application Tracker";
        }
        if (pagename == "accrefund")
        {
            pagetitle.Text = "APS | Refund";
        }
        if (pagename == "formbay")
        {
            pagetitle.Text = "APS | FormBay";
        }
        if (pagename == "stockitem")
        {
            pagetitle.Text = "APS | Stock Item";
        }
        if (pagename == "stocktransfer")
        {
            pagetitle.Text = "APS | Stock Transfer";
        }
        if (pagename == "stockorder")
        {
            pagetitle.Text = "APS | Stock Order";
        }
        if (pagename == "stockdatasheet")
        {
            pagetitle.Text = "APS | Data Sheet";
        }

        if (pagename == "Wholesale")
        {
            pagetitle.Text = "APS | Wholesale";
        }
        if (pagename == "wholesalededuct")
        {
            pagetitle.Text = "APS | Wholesale Deduct";
        }
        if (pagename == "stockdeduct")
        {
            pagetitle.Text = "APS | Stock Deduct";
        }
        if (pagename == "stockin")
        {
            pagetitle.Text = "APS | Stock In";
        }
        if (pagename == "WholesaleIn")
        {
            pagetitle.Text = "APS | Wholesale In";
        }
        if (pagename == "stockusage")
        {
            pagetitle.Text = "APS | Stock Usage";
        }
        if (pagename == "stockorderreport")
        {
            pagetitle.Text = "APS | Stock Order Report";
        }

        if (pagename == "promosend")
        {
            pagetitle.Text = "APS | Promo Send";
        }

        if (pagename == "lead")
        {
            pagetitle.Text = "APS | Lead";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "casualmtce")
        {
            pagetitle.Text = "APS| Casual Maintenance";
        }
        if (pagename == "noinstalldate")
        {
            pagetitle.Text = "APS | NoInstall Date Report";
        }
        if (pagename == "installdate")
        {
            pagetitle.Text = "APS | Install Date Report";
        }
        if (pagename == "panelscount")
        {
            pagetitle.Text = "APS | Panel Graph";
        }
        if (pagename == "accountreceive")
        {
            pagetitle.Text = "APS | Account Receive Report";
        }
        if (pagename == "paymentstatus")
        {
            pagetitle.Text = "APS | Payment Status Report";
        }
        if (pagename == "formbaydocsreport")
        {
            pagetitle.Text = "APS | Formbay Docs Report";
        }
        if (pagename == "weeklyreport")
        {
            pagetitle.Text = "APS | Weekly Report";
        }
        if (pagename == "leadassignreport")
        {
            pagetitle.Text = "APS | Lead Assign Report";
        }
        if (pagename == "stockreport")
        {
            pagetitle.Text = "APS | Stock Report";
        }
        if (pagename == "custproject")
        {
            pagetitle.Text = "APS | Customer System Detail";
        }
        if (pagename == "customerdetail")
        {
            pagetitle.Text = "APS | Customer System Detail";
        }
        if (pagename == "instavailable")
        {
            pagetitle.Text = "APS | Installer Available";
        }
        if (pagename == "printinstallation")
        {
            pagetitle.Text = "APS | Installations";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "APS | Maintenance";
        }
        if (pagename == "stocktransferreportdetail")
        {
            pagetitle.Text = "APS | Stock Transfer Report";
        }
        if (pagename == "promotracker")
        {
            pagetitle.Text = "APS | Promo Tracker";
        }
        if (pagename == "unittype")
        {
            pagetitle.Text = "APS | Unit Type";
        }
        if (pagename == "streettype")
        {
            pagetitle.Text = "APS | Street Type";
        }
        if (pagename == "streetname")
        {
            pagetitle.Text = "APS | Street Name";
        }
        if (pagename == "promooffer")
        {
            pagetitle.Text = "APS | Promo Offer";
        }
        if (pagename == "updateformbayId")
        {
            pagetitle.Text = "APS | updateformbayId";
        }
        if (pagename == "pickup")
        {
            pagetitle.Text = "APS | Pick Up";
        }
        if (pagename == "paywaytracker")
        {
            pagetitle.Text = "APS | PayWay";
        }
        if (pagename == "quickform")
        {
            pagetitle.Text = "APS | Quick Form";
        }
        if (pagename == "clickcustomer")
        {
            pagetitle.Text = "APS | Click Customer";
        }

        if (pagename == "PickList")
        {
            pagetitle.Text = "APS | Picklist Tracker";
        }

        if (pagename == "JobPhotos")
        {
            pagetitle.Text = "APS | Job Photos";
        }



    }


    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/admin/Default.aspx");
    }


}


