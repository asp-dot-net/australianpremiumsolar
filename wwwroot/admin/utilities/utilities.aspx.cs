using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class admin_adminfiles_utilities_utilities : System.Web.UI.Page
{
    string foldername = "utilities";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitUpdate();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            tbsitename.Text = st.sitename;
            //tbsiteurl.Text = "http://localhost:57341/";
            tbsiteurl.Text = st.siteurl;
            tblogo.Text = st.sitelogo;
        }              
    }

    public static bool ThumbnailCallBack()
    {
        return false;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {   
        //string logo = tblogo.Text;
        //string sitename = tbsitename.Text;
        //string siteurl = tbsiteurl.Text;

        //    bool success1 = ClsAdminUtilities.SpUtilitiesUpdateData("1", sitename, logo, siteurl);
        //    if (success1)
        //    {
        //        SetUpdate();
        //    }
        //    else
        //    {
        //        SetError();
        //    }            
        //    Reset();       
    }


    //================================== Start Common Code ==================================


    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }

        return m_SortDirection;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
     
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
   
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
   
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        btnUpdate.Visible = true;
     //   lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        tbsitename.Text = string.Empty;
        tbsiteurl.Text = string.Empty;
        tblogo.Text = string.Empty;
    }
    //================================== End Common Code =====================================

}
