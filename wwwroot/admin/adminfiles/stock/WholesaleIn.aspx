<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="WholesaleIn.aspx.cs" Inherits="admin_adminfiles_stock_WholesaleIn" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }
    </style>
    <%--<script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
        </Triggers>
    </asp:UpdatePanel>



    <script>

        $(document).ready(function () {
        });

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }

<%--        function formValidate2() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {

                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#div3').css('display', 'block');
                        $('#<%=divMatch.ClientID %>').hide();
                    }
                }
            }
        }--%>

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
            callMultiCheckbox();

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            $('.loading-container').css('display', 'none');

            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

        }
        function pageLoaded() {

           // $('#div3').css("display", "none");
          <%--  $('#<%=btnRevert.ClientID %>').click(function () {
                formValidate2();
            });--%>

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            //  callMultiCheckbox();

        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Wholesale In
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
        </h5>

    </div>



    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <%-- <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div>
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="Row">
                                    <div class="">




                                        <div class="col-md-12" id="divright" runat="server">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true">
                                                <cc1:TabPanel ID="TabDeduct" runat="server" HeaderText="Wholesale In">

                                                    <ContentTemplate>
                                                        <%--<asp:UpdatePanel runat="server" ID="UpdateDeduct">
                                                                    <ContentTemplate>--%>

                                                        <div align="right">
                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        </div>
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>


                                                        <%-- </ol>--%>

                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="searchfinal">
                                                                    <div class="widget-body shadownone brdrgray">
                                                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div class="dataTables_filter">
                                                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                                                                    <div class="dataTables_filter Responsive-search printpage searchfinal">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock martop5">
                                                                                                        <div>
                                                                                                            <div class="input-group martop5" style="width: 115px;">
                                                                                                                <asp:TextBox ID="txtinvoiceno" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtinvoiceno"
                                                                                                                    WatermarkText="InvoiceNumber" />
                                                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtinvoiceno" FilterType="Numbers" />
                                                                                                                <%--    <cc1:AutoCompleteExtender ID="AutoCompleteExtender10" MinimumPrefixLength="2" runat="server"
                                                                                                        UseContextKey="true" TargetControlID="txtinvoiceno" ServicePath="~/Search.asmx"
                                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                                                        <%--        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search3"
                                                                                                                    ControlToValidate="txtinvoiceno" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                    ValidationExpression="^\d+$" Style="color: red;"></asp:RegularExpressionValidator>--%>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1 martop5" id="div4" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group martop5" id="div5" runat="server" style="width: 114px">
                                                                                                                <asp:DropDownList ID="ddlvendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                                                                    <asp:ListItem Value="0">Customer Name</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group martop5" id="div6" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">ExpectedDate</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2" Selected="True">StockDeductDate</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                                            </div>
                                                                                                            <div class="input-group martop5" style="width: 150px;">
                                                                                                                <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                            ControlToValidate="txtEndDate3" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>

                                                                                                                <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search3" Style="color: red;"></asp:CompareValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group martop5">
                                                                                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search"
                                                                                                                    ValidationGroup="search3" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="true" OnClick="btnSearch_Click"></asp:LinkButton>
                                                                                                                <%--<asp:LinkButton ID="btnSearch3" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch3_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-0 martop5">
                                                                                                                <asp:LinkButton runat="server" ID="btnClear"
                                                                                                                    CausesValidation="false" CssClass="btn btn-primary" OnClick="btnClearAll_Click1"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="inlineblock" style="margin-top: 3px;">
                                                                                                        <div>
                                                                                                            <div class="datashowbox">
                                                                                                                <div class="leftarea1">
                                                                                                                    <div class="showenteries showdata">
                                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                                        <asp:ListItem Value="10">Show entries</asp:ListItem>
                                                                                                                                    </asp:DropDownList>

                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="finalgrid">
                                                                    <div class="table-responsive printArea">
                                                                        <div id="PanGrid" runat="server">
                                                                            <div class="table-responsive xscroll ">
                                                                                <asp:GridView ID="GridView1" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                                                    OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="DeductDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label21" runat="server" Width="100px">
                                                                    <%#Eval("StockDeductDate","{0: dd MMM yyyy}")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hndWholesaleID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                                <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                                <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vendor">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label14" runat="server" Width="120px">
                                                                                             <%#Eval("Vendor")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ExpectedDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="ExpectedDelivery">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="N.Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Literal runat="server" ID="ltallocatedpanels"></asp:Literal>
                                                                                                <%# Eval("Npanel")%>
                                                                                                <asp:HiddenField runat="server" ID="hdnStockItem" Value='<%#Eval("StockItemID")%>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Deducted" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>                                                                                                
                                                                                                <asp:Label ID="ltdeductpanels" runat="server" Width="30px" Text='<%# Eval("Npanel")%>'></asp:Label>
                                                                                                
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Revert" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="printpage">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblrevert" runat="server" Width="30px" Text='<%#(Convert.ToInt32(Eval("Npanel"))+Convert.ToInt32(Eval("StockSum")) != 0) ? Convert.ToInt32(Eval("StockSum"))*(-1): Convert.ToInt32(0)%>'  ></asp:Label>
<%--                                                                                                    <asp:Button ID="btnRevertStock" runat="server" Text="Revert" CssClass="btnrevert" data-toggle="tooltip" data-placement="top" title="Revert" CommandName="RevertStock" CommandArgument='<%#Eval("WholesaleOrderID")%>' OnClientClick="return confirm('Are you sure you want to Revert this Record?');" Visible="false" />--%>
                                                                                                
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Remaining" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label runat="server" ID="ltremainingpanels" Text='<%#Convert.ToInt32(Eval("Npanel"))+Convert.ToInt32(Eval("StockSum"))%>'></asp:Label>    
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="500px" SortExpression="WholesaleOrderItem">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbldetails" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                                                                    Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="StoreName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label4" runat="server" Width="80px">
                                                                                                 <%#Eval("CompanyLocation")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext brdrgrayright" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label17" runat="server" Width="80px">
                                                                                                    <asp:LinkButton ID="lbtnDeduct" CommandName="revert" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" runat="server">Revert</asp:LinkButton>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <PagerTemplate>
                                                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        <div class="pagination">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                        </div>
                                                                                    </PagerTemplate>
                                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSearch" />
                                                                         <asp:PostBackTrigger ControlID="GridView1" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                                <cc1:TabPanel ID="TabRevert" runat="server" HeaderText="Revert">
                                                    <ContentTemplate>
                                                        <%-- <asp:UpdatePanel runat="server" ID="updaterevert">
                                                                    <ContentTemplate>--%>
                                                        <div align="right">

                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>


                                                        </div>
                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord4" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>
                                                        <div class="searchfinal">
                                                            <div class="widget-body shadownone brdrgray">
                                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_filter">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnsearch2">
                                                                                    <div class="dataTables_filter Responsive-search ">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                      <div class="inlineblock">
                                                                                                        <div>
                                                                                                            <div class="input-group martop5" style="width: 115px;">
                                                                                                                <asp:TextBox ID="txtinvoiceno2" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtinvoiceno2"
                                                                                                                    WatermarkText="InvoiceNumber" />
                                                                                                                <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender10" MinimumPrefixLength="2" runat="server"
                                                                                                        UseContextKey="true" TargetControlID="txtinvoiceno" ServicePath="~/Search.asmx"
                                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20"/>--%>
                                                                                                             <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search2"
                                                                                                                    ControlToValidate="txtinvoiceno2" Display="Dynamic" ErrorMessage="Please enter numberic value" Style="color: red"
                                                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtinvoiceno2" FilterType="Numbers" />
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group martop5" id="div2" runat="server" style="width: 114px">
                                                                                                                <asp:DropDownList ID="ddlvendor2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                                                                    <asp:ListItem Value="0">Customer Name</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group martop5" id="div7" runat="server" style="width: 120px">
                                                                                                                <asp:DropDownList ID="ddlSearchDate2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="3">RevertDate</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate2" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
<%--                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtStartDate2" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                                            </div>
                                                                                                            <div class="input-group martop5" id="div35" runat="server" style="width: 150px">
                                                                                                                <div class="input-group date datetimepicker1" style="width: 150px;">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtEndDate2" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
<%--                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                        ControlToValidate="txtEndDate4" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                                                </div>
                                                                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date" Style="color: red"
                                                                                                                    ControlToCompare="txtStartDate2" ControlToValidate="txtEndDate2" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search4"></asp:CompareValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1 martop5" id="div9" runat="server">
                                                                                                                <asp:DropDownList ID="ddlRevert" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">Revert</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">Revert All</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group ">
                                                                                                                <asp:Button ID="btnSearch2" runat="server" Text="Search" ValidationGroup="search2" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="true" OnClick="btnSearch2_Click"></asp:Button>
                                                                                                                <%--  <asp:Button ID="btnSearch4" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch4_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="input-group ">
                                                                                                                <asp:LinkButton runat="server" ID="btnClear2"
                                                                                                                    CausesValidation="false" OnClick="btnClearAll2_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="inlineblock" style="margin-top: 3px;">
                                                                                                        <div>

                                                                                                            <div class="datashowbox">
                                                                                                                <div class="leftarea1">
                                                                                                                    <div class="showenteries showdata">
                                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlSelectRecords2" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords2_SelectedIndexChanged"
                                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                                        <asp:ListItem Value="10">Show entries</asp:ListItem>
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>




                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="finalgrid">
                                                                <div class="table-responsive printArea">
                                                                    <div id="Div10" runat="server">
                                                                        <div class="table-responsive xscroll" id="PanGrid2" runat="server">
                                                                            <asp:GridView ID="GridView2" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover printarea"
                                                                                OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging" OnRowCreated="GridView2_RowCreated"
                                                                                OnDataBound="GridView2_DataBound"
                                                                                AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                                                                                 <Columns>
                                                                                    <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hndWholesaleID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                            <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                            <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Vendor">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label14" runat="server" Width="120px">
                                                                                             <%#Eval("Vendor")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ExpectedDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ExpectedDelivery">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="500px" SortExpression="WholesaleOrderItem">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbldetails" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                                                                Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="StoreName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" Width="80px">
                                                                                                 <%#Eval("CompanyLocation")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="RevertDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label19" runat="server" Width="100px">
                                                                                         <%#Eval("RevertDate","{0:dd MMM yyyy}")%>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Reverted QTY" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="Stock">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label119" runat="server" Width="100px">
                                                                                         <%#Eval("Stock")%>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="printorder"
                                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField Value='<%#Eval("WholesaleOrderID") %>' runat="server" ID="hdnWholesaleOrderID" />
                                                                                            <asp:LinkButton CssClass="printorder" ID="lnkReceived" CausesValidation="false" runat="server" OnClick="lnkReceived_Click" data-toggle="tooltip" data-placement="top" data-original-title="Detail">
                                                                                              
                                                                                                <a target="_blank" href="#" data-original-title="Detail" data-placement="top" data-toggle="tooltip" class="btn btn-primary btn-xs"> <i class="fa fa-link"></i> Detail</a>
                                                                                            </asp:LinkButton>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    <div class="pagination">
                                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                    </div>
                                                                                </PagerTemplate>
                                                                                <PagerStyle CssClass="paginationGrid" />
                                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="paginationnew1" runat="server" id="divnopage2" visible="false">
                                                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage2" style="width: 100%; border-collapse: collapse;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnsearch2" />
                                                                        <asp:PostBackTrigger ControlID="GridView2" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>


    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

    <cc1:ModalPopupExtender ID="ModalPopupExtenderDR" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel2" DropShadow="false" PopupControlID="divStockItems2"
        OkControlID="btnOK2" TargetControlID="btnNULL2">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divStockItems2" style="display: none;">

        <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancel2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="myModalLabel">Project Wholesale Stock Revert</h4>
                </div>
                <div class="modal-body paddnone" style="overflow-y: scroll; height: 350px;">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table cellpadding="5" cellspacing="5" border="0" class="table">
                                    <tr>
                                        <td width="150px"><b>Customer</b></td>
                                        <td>
                                            <asp:Label ID="lblCustomer2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>System Details</b></td>
                                        <td>
                                            <asp:Label ID="lblSystemDetails2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install City</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallCity2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install State</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallState2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <%--<div class="clear"></div>
                        <br />--%>
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="50%"><b>Stock Item</b></td>
                                        <td width="20%"><b>Deduct</b></td>
                                        <td width="30%"><b>Qty</b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRev" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="50%">
                                                    <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <asp:HiddenField ID="hndStockCatergoryID" runat="server" Value='<%#Eval("StockCategoryID") %>' />
                                                    <%#Eval("StockItem") %> </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblStockDeduct" runat="server"><%#Eval("Deduct","{0:0}") %></asp:Label>
                                                </td>
                                                <td width="30%">
                                                    <table>
                                                        <tr>
                                                            <td style="padding-right:5px;">
                                                                <asp:TextBox ID="txtDeductRevert" runat="server" MaxLength="4" Width="50px" CssClass="form-control"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDeductRevert" FilterType="Numbers" />
                                                            </td>
                                                            <td >                                                               
                                                                <asp:CompareValidator ID="CompareValidatorDR" runat="server" ControlToValidate="txtDeductRevert" ValueToCompare='<%#Eval("Deduct","{0:0}") %>'
                                                                    Type="Integer" Display="Dynamic" ErrorMessage="Enter valid stock" Operator="LessThanEqual" Style="color: red;" ValidationGroup="abc"></asp:CompareValidator>
                                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="abc"
                                                                    ControlToValidate="txtDeductRevert" Display="Dynamic" ErrorMessage="Qty can't be Zero." 
                                                                    ValidationExpression="^[1-9]\d*$" Style="color: red;"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="10%"><b>Sr No.</b></td>
                                        <td width="40%" runat="server" id="tdhd1"><b>Pallet Number</b></td>
                                        <td width="40%"><b>Serial Number</b></td>
                                        <td width="10%"><b></b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRevSerial" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="10%">
                                                    <asp:HiddenField ID="hdnInventoryHistId" runat="server" Value='<%# Eval("InventoryHistoryId") %>' />
                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                                </td>
                                                <asp:Panel runat="server" ID="tdhd2">
                                                    <td width="40%">
                                                        <asp:Label ID="lblPallet" runat="server" Text='<%# Eval("Pallet") %>'></asp:Label>
                                                    </td>
                                                </asp:Panel>
                                                <td width="40%">
                                                    <asp:Label ID="lblSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Label>
                                                </td>
                                                <td width="10%" style="text-align: center;">
                                                    <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" cssclass="form-control">
                                                        <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12" id="divMatch" runat="server">
                            <div class="form-group" style="text-align: center">
                                <div class="alert alert-danger">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Selected Check boxes and Panel Qty didn't matched.</strong>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                        </div>
                        <%--  <div class="col-md-12" id="div3">
                            <div class="form-group" style="text-align: center">
                                <div class="alert alert-danger">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Qty cannot be greater than deduct quantity.</strong>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                        </div>--%>
                        <div class="col-md-12" align="center">
                            <div class="form-group">
                                <asp:Button ID="btnRevert" runat="server" Text="Revert" OnClick="btnRevert_Click" CssClass="btn btn-purple resetbutton"
                                    CausesValidation="true" ValidationGroup="abc" />
                                <button id="btnRevertAll" type="submit" runat="server" causesvalidation="false" class="btn btn-purple resetbutton" onserverclick="btnRevertAll_Click">
                                    Revert All</button>
                                <%--     <asp:Button runat="server"  OnClick="btnRevertAll_Click" class="btn btn-purple resetbutton"  Text="Revert All" />--%>
                                <asp:Button ID="btnOK2" Style="display: none;" runat="server"
                                    CssClass="btn" Text=" OK" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL2" Style="display: none;" runat="server" />

    <asp:Button ID="btnNULLMove" Style="display: none;" runat="server" />
    <asp:HiddenField ID="hndProjectID" runat="server" />

    <asp:Button ID="Button2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divstockdetail" TargetControlID="Button2">
    </cc1:ModalPopupExtender>
    <div id="divstockdetail" runat="server" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="Button3" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="Button3_Click">
                        Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H3">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        Stock Revert Details</h4>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetail">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                        <thead>
                                                            <tr>
                                                                <td style="width: 70%">Stock Item
                                                                </td>
                                                                <td style="width: 20%">Stock
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptstockdetail" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 70%;">
                                                                        <%#Eval("StockItem")%>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <%#Eval("Stock")%>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetailmsg" visible="false">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>

                                                    <div class="messesgarea">
                                                        <div class="alert alert-info" id="Div12" runat="server">
                                                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnWholesaleOrderID" />


    <asp:Button ID="Button8" Style="display: none;" runat="server" />


    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>


    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {
            var title = "";

            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
