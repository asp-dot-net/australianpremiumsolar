﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="StockOrderDelivery.aspx.cs" Inherits="admin_adminfiles_stock_StockOrderDelivery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
              <script>
                function openModal() {
                    $('[id*=modal_danger]').modal('show');
                }
            </script>

         <%--   <script>
               function printContent() {
                    $('#<%=myModal.ClientID%>').show();
                    $('#myModalLabel1').show();

                    window.print();
                    $('#<%=myModal.ClientID%>').hide();
                }

            </script>--%>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);

                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);

                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {


                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {


                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $("[data-toggle=tooltip]").tooltip();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(document).ready(function () {

                    <%--    $('#<%=ibtnAddVendor.ClientID %>').click(function () {
                            formValidate();
                        });
                        $('#<%=ibtnAddStock.ClientID %>').click(function () {
                            formValidate();
                        });--%>
                    });

                }
            </script>
    <div class="page-body padtopzero printorder">
                             
            
   <div class="page-body headertopbox printorder">
        <div class="card">
                <div class="card-block">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Order Detail
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
          <div id="hbreadcrumb" class="pull-right">
                    <%--<asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                    <asp:LinkButton ID="lnkBack" runat="server"  CausesValidation="false" CssClass="btn btn-maroon" OnClick="lnkBack_Click"><i class="fa fa-backward"></i> Back</asp:LinkButton>
            </div>
                </h5>
                </div>
        </div>
               
            </div>
               <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-info" id="PAnAddress" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;Record with this Vendor Invoice Number already exists.</strong>
                            </div>
                            <div class="alert alert-danger" id="PanSerialNo" runat="server" visible="false">
                                <i class="icon-info-sign"></i><asp:Literal ID="ltrerror" runat="server"></asp:Literal></literal></strong>
                            </div>
                        </div>
                    </div>

                </asp:Panel> 
                            <div class="panel-body" id="detailsid" runat="server">
                                    <div class="card">
                                            <div class="card-block">
                   
                                <div class="formainline" >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                <tr>
                                                    <td width="200px" valign="top">Stock Location
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Vendor
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">BOL Received
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Manual Order No
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Expected Delevery
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200px" valign="top">Notes
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trOrderItem" runat="server">
                                                    <td colspan="2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-bordered">
                                                            <tr class="graybgarea">
                                                                <td colspan="4">
                                                                    <h4>Stock Order Item Detail</h4>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Stock Category</b></td>
                                                                <td><b>Quantity</b></td>
                                                                <td><b>Stock Item</b></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <asp:Repeater ID="rptOrder" runat="server" OnItemDataBound="rptOrder_ItemDataBound" OnItemCommand="rptOrder_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <asp:HiddenField runat="server" ID="hfStockOrderID" Value='<%#Eval("StockOrderID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hfStockCategoryID" Value='<%#Eval("StockCategoryID") %>' />
                                                                         <asp:HiddenField runat="server" ID="hfStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                                        <td><%#Eval("StockOrderItem") %></td>
                                                                        <td><asp:Label runat="server" ID="lblOrderQuantity" Text=<%#Eval("OrderQuantity") %>></asp:Label></td>
                                                                        <td><%#Eval("StockItem") %></td>
                                                                        <%--Visible='<%#Eval("DeliveryOrderStatus").ToString() == "1" ?true : false %>'--%>
                                                                        <td class="text-center">  <asp:LinkButton ID="btnDelivery" runat="server" Text="Delivered" CausesValidation="false" CssClass="btn btn-maroon btn-xs" 
                                                        data-toggle="tooltip" data-placement="top" title="Delivery" CommandName="Delivered" OnClientClick="yes" CommandArgument='<%#Eval("StockOrderID") +","+ Eval("StockOrderItemID")%>'> <i class="fa fa-truck"></i> Delivery</asp:LinkButton>
                                                                            <asp:ImageButton ID="checkimag" Visible="false" runat="server" Enabled="false" Height="20px" Width="25px" ImageUrl="~/images/check.png" /></td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                  </div>
                  </div>
                            </div>
  </div> 
     <cc1:ModalPopupExtender ID="ModalPopupdeliver" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divdeliver" TargetControlID="btnNull1"
                CancelControlID="btnCancel1">
            </cc1:ModalPopupExtender>
            <div id="divdeliver" runat="server" style="display: none" class="modal_popup">
                <asp:HiddenField ID="hndid" runat="server" />
                 <asp:HiddenField ID="hndStockOrderItemID" runat="server" />
                <div class="modal-dialog" style="width: 300px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <button id="btnCancel1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                            </div>
                            <h4 class="modal-title" id="H1">Delivery Date</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">
                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label25" runat="server" class="control-label ">
                                                <strong>Delivery Date</strong></asp:Label>
                                        </span>
                                        <div class="input-group date datetimepicker1 col-sm-12">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtdatereceived" runat="server" class="form-control">                                             
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqfieldtxtdatereceived" runat="server" ForeColor="Red" ControlToValidate="txtdatereceived" ErrorMessage="*" ValidationGroup="adddate" ></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <span class="name  left-text">
                                            <asp:Label ID="Label22" runat="server" class="control-label ">
                                                <strong>Serial No.</strong></asp:Label>
                                        </span>
                                        <span class="">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block; padding: 0; margin-top: 5px; margin-bottom: 5px;" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="adddate" ValidationExpression="^.+(.xls|.XLS)$"
                                                Display="Dynamic" ForeColor="Red" ErrorMessage="Upload .xls file only"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ForeColor="Red" ControlToValidate="FileUpload1" ErrorMessage="*" ValidationGroup="adddate" ></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FileUpload1"
                                                ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>--%>
                                            <%--<div class="col-sm-12" style="padding:0; margin-top:5px;">                            
                                            <asp:TextBox ID="txtserialno" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>--%>
                                        </span>
                                    </div>



                                    <div runat="server" class="form-group marginleft col-sm-12" style="text-align: center; margin-bottom: 0;">
                                        <asp:Button ID="btndeliver" runat="server" Text="Save" OnClick="btndeliver_Click"
                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="adddate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Button ID="btnNull1" Style="display: none;" runat="server" />
        </ContentTemplate>
         <Triggers>
           <%-- <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <asp:PostBackTrigger ControlID="btndeliver" />


        </Triggers>
         </asp:UpdatePanel>
               
</asp:Content>

