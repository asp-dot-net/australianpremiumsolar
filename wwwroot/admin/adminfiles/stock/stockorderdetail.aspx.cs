﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockorderdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string StockOrderID = Request.QueryString["id"];
            SttblStockOrders st = ClstblStockOrders.tblStockOrders_SelectByStockOrderID(StockOrderID);

            if (st.CompanyLocation != string.Empty)
            {
                lblStockLocation.Text = st.CompanyLocation;
            }
            else
            {
                lblStockLocation.Text = "-";
            }
            if (st.Vendor != string.Empty)
            {
                lblVendor.Text = st.Vendor;
            }
            else
            {
                lblVendor.Text = "-";
            }
            if (st.BOLReceived != string.Empty)
            {

                lblBOLReceived.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.BOLReceived));
            }
            else
            {
                lblBOLReceived.Text = "-";
            }
            if (st.ManualOrderNumber != string.Empty)
            {
                lblManualOrderNo.Text = st.ManualOrderNumber;
            }
            else
            {
                lblManualOrderNo.Text = "-";
            }
            if (st.ExpectedDelivery != string.Empty)
            {
                lblExpectedDelevery.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ExpectedDelivery));
            }
            else
            {
                lblExpectedDelevery.Text = "-";
            }

            DataTable dt = ClstblStockOrders.tblStockOrderItems_Select_ByStockOrderID(StockOrderID);
            if (dt.Rows.Count > 0)
            {
                rptOrder.DataSource = dt;
                rptOrder.DataBind();
                trOrderItem.Visible = true;
            }
            else
            {
                trOrderItem.Visible = false;
            }
        }
    }
    protected void lbtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/stockorder.aspx");
    }
}