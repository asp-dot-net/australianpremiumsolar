<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stockitem.aspx.cs" Inherits="admin_adminfiles_stock_stockitem"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .file-input-name {
            margin-top: 2px;
            margin-left: 0px;
        }
    </style>
    
    <script>
        function ShowProgress() {
            setTimeout(function () {
                //alert("check");
                if (Page_IsValid) {
                    //alert("true");
                    $('.modalbackground').css('background-color', 'unset');
                    //this is because when popup is open, and loader is called background color becomes dark.
                    $('.loading-container').css('display', 'block');                   
                }
                else {
                    //alert("false");
                }                
            }, 200);
        }
        $(function () {
           // $('form').on("click",'#<%=ibtnUploadPDF.ClientID %>', function () {
            $('form').on('submit', function () {
                //alert(this.selector);
                ShowProgress();
            });
        });
    </script>

    <script>
        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //alert("1");
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //alert("2");
            //hide the modal popup - the update progress
            if (sender._postBackSettings.panelsToUpdate != null) {
                $(":file").bootstrapFileInput();

            }
        }

        function pageLoadedpro() {
            //alert("3");
            //$(":file").bootstrapFileInput();
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myvalstockitem").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });


            $(".myval1").select2({
                minimumResultsForSearch: -1
            });

            $(".myval").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }


            //$("[data-toggle=tooltip]").tooltip();
            //$('.tooltipwidth').tooltip();
            //$('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });


        }


        $(document).ready(function () {

            $('#<%=btnAdd.ClientID %>').click(function (e) {
                        formValidate();
                    });

                    $('#<%=btnUpdate.ClientID %>').click(function (e) {
                        formValidate();

                    });
                  <%--  $('#<%=btnCancel.ClientID %>').click(function (e) {
                        alert('#<%=ModuleFileUpload.ClientID %>');

                
                        $('#<%=ModuleFileUpload.ClientID %>').wrap('<a class="file-input-wrapper btn btn-default"></a>').parent().prepend($('<span></span>').html("Browse"));

                    });--%>



        });

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");                        
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");              
                    }
                }
            }
        }

    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="pe-7s-graph2 icon"></i>Stock Item<asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-info btngray"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                </div>
            </div>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>


            <div class="finaladdupdate addEmployee formGrid whitebg">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="with-header  addform centerFormTable">
                            <div class="header bordered-blue">
                                <h4 class="formHeading"><asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Stock Item</h4>
                            </div>
                            <div class="row">
                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label15" runat="server" class="control-label">
                                                Stock&nbsp;Category</asp:Label>
                                        
                                            <asp:DropDownList ID="ddlstockcategory" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="myvalstockitem">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="reqerror"
                                                ControlToValidate="ddlstockcategory" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                    

                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label10" runat="server" class="control-label">
                                                Stock&nbsp;Item</asp:Label>
                                            <asp:TextBox ID="txtstockitem" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                ControlToValidate="txtstockitem" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label1" runat="server" class="control-label">
                                                Brand</asp:Label>
                                            <asp:TextBox ID="txtbrand" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage=""
                                                ControlToValidate="txtbrand" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label2" runat="server" class="control-label">
                                                Stock&nbsp;Model</asp:Label>
                                            <asp:TextBox ID="txtmodel" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage=""
                                                ControlToValidate="txtmodel" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label3" runat="server" class="control-label">
                                                Stock&nbsp;Series</asp:Label>
                                            <asp:TextBox ID="txtseries" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                                ControlToValidate="txtseries" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <asp:Label ID="Label4" runat="server" class="control-label">
                                                Stock&nbsp;Size</asp:Label>
                                        
                                            <asp:TextBox ID="txtStockSize" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ControlToValidate="txtStockSize" Display="Dynamic" ErrorMessage="Enter valid digit"
                                                ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                ControlToValidate="txtStockSize" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-3">
                                        <asp:Label ID="Label5" runat="server" class="control-label">
                                                Min.&nbsp;Stock</asp:Label>
                                        
                                            <asp:TextBox ID="txtminstock" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^[0-9]*$" Display="Dynamic"
                                                ErrorMessage="Enter valid digit" ControlToValidate="txtminstock"></asp:RegularExpressionValidator>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                ControlToValidate="txtminstock" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group col-sm-3">
                                        <asp:Label ID="Label16" runat="server" class="control-label">
                                                QuickForm&nbsp;ID</asp:Label>
                                        
                                            <asp:TextBox ID="txtfixstockitemid" runat="server" MaxLength="8" CssClass="form-control modaltextbox"></asp:TextBox>



                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." 
                                                                ControlToValidate="txtfixstockitemid" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        Stock&nbsp;Location
                                                    </label>

                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        Stock&nbsp;Quantity
                                                    </label>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="name form-group">
                                                    <label class="control-label">
                                                        Sales&nbsp;Tag
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptstocklocation" runat="server">
                                            <ItemTemplate>
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <span class="">
                                                                        <asp:HiddenField ID="hndlocationid" runat="server" Value='<%# Eval("CompanyLocationID") %>' />
                                                                        <span class="form-control" style="display:flex;align-items:center;"><asp:Literal ID="ltlocation" runat="server" Text='<%# Eval("State")+": "+Eval("CompanyLocation") %>'></asp:Literal></span>
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <span>
                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" Text="0" MaxLength="8"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ControlToValidate="txtqty" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[0-9]*$"
                                                                            ErrorMessage="Enter valid digit" ControlToValidate="txtqty"></asp:RegularExpressionValidator>

                                                                    </span>
                                                                </div>
                                                                <div class="col-md-3 center checkBox checkBoxList" style="margin-top: 7px;">
                                                                        <asp:CheckBox ID="chksalestagrep" runat="server" />
                                                                        <label for="<%# Container.FindControl("chksalestagrep").ClientID %>">
                                                                        </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <div id="Div4" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Description</label>
                                            </span><span>
                                                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" Height="100px" CssClass="form-control"
                                                    Width="100%" Style="resize: none;">
                                                </asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <%--<div class="form-group ">
                                            <span class="name">
                                                <label class="control-label">
                                                    Is&nbsp;Dashboard?</label>
                                            </span><span>
                                                <asp:CheckBox ID="chkDashboard" runat="server" />

                                                <label for="<%=chkDashboard.ClientID %>">
                                                    <span></span>
                                                </label>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="form-group col-sm-3 checkBox checkBoxList">
                                            <asp:Label ID="Label11" runat="server" class=" control-label">
                                                Is Active?</asp:Label>
                                            <asp:CheckBox ID="chkisactive" runat="server" />
                                            <label for="<%=chkisactive.ClientID %>"></label>
                                        <div class="clear"></div>
                                    </div>


                                    <div class="form-group col-sm-3 checkBox checkBoxList">
                                            <asp:Label ID="Label13" runat="server" class=" control-label">
                                                Is&nbsp;Dashboard?</asp:Label>
                                            <asp:CheckBox ID="chkDashboard" runat="server" />
                                            <label for="<%=chkDashboard.ClientID %>">
                                            </label>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group col-sm-3 checkBox checkBoxList" id="sales" runat="server" visible="false">
                                       
                                            <asp:Label ID="Label12" runat="server" class=" control-label">
                                               Sales&nbsp;Tag</asp:Label>
                                        
                                            <asp:CheckBox ID="chksalestag" runat="server" />
                                            <label for="<%=chksalestag.ClientID %>">
                                              
                                            </label>
                                        <div class="clear"></div>
                                    </div>
                                
                                

                                <div class="col-sm-12">
                                    <div class="dividerLine"></div>
                                </div>
                                <div class="col-sm-12 textRight">
                                        <asp:Button CssClass="btn largeButton greenBtn btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" />
                                        <asp:Button CssClass="btn largeButton greenBtn btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn largeButton whiteBtn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn largeButton blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                </div>
                            </div>


                        </div>




                    </div>
                </div>
            </div>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>

                        <div class="searchfinal searchFilterSection searchBox">

                            <asp:UpdatePanel ID="updatepanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel runat="server" ID="Panel3">
                                        <div class="topfileuploadbox marbtm15">
                                            <div class="widget-body shadownone brdrgray formGrid">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="filediv">
                                                            <label class="control-label"><b>Modules :</b> Upload Excel File <span class="symbol required"></span></label>
                                                            <div class="row">
                                                            <div class="col-md-8">
                                                                    <label class="custom-fileupload">
                                                                        <asp:FileUpload ID="ModuleFileUpload" runat="server" Style="display: inline-block;" class="fileupdate custom-file-input"/>
                                                                        <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                        <span class="btnbox">Upload file</span>
                                                                    </label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="ModuleFileUpload"
                                                                            ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                                            Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ControlToValidate="ModuleFileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>
                                                                    
                                                            </div>
                                                            <div class="col-md-4 pddleft0">
                                                                <asp:Button class="btn largeButton greenBtn addwhiteicon btnaddicon" ID="btnaddmodule" runat="server" OnClick="btnaddmodule_Click" Text="Add" ValidationGroup="success1" />
                                                                    <%--  <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="Button2" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel" />--%>
                                                            </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label"><b>Inverter :</b> Upload Excel File <span class="symbol required"></span></label>
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    
                                                                    <label class="custom-fileupload">
                                                                        <asp:FileUpload ID="InverterFileUpload" runat="server" Style="display: inline-block;" class="custom-file-input"/>
                                                                        <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                        <span class="btnbox">Upload file</span>
                                                                    </label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="InverterFileUpload"
                                                                            ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                                            Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ControlToValidate="InverterFileUpload" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>
                                                                    
                                                                </div>
                                                                <div class="col-md-4 pddleft0">
                                                                <asp:Button class="btn largeButton greenBtn addwhiteicon btnaddicon" ID="btnAddInverter" runat="server" OnClick="btnAddInverter_Click" Text="Add" ValidationGroup="success" />
                                                                    <%--       <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="Button4" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel">--%>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                                <%--<Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" />
                                </Triggers>--%>
                            </asp:UpdatePanel>
                            <div class="widget-body shadownone brdrgray">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="dataTables_filter Responsive-search">
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                            <div class="">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="inlineblock col-sm-12">
                                                                <div class="row">
                                                                    <div class="input-group col-sm-1 ">
                                                                        <asp:TextBox ID="txtSearchStockItem" runat="server" placeholder="Stock Item" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStockItem"
                                                                            WatermarkText="StockItem" />
                                                                    </div>

                                                                    <%--  <div class="input-group col-sm-2 ">
                                                                        <asp:TextBox ID="txtSearchManufacturer" runat="server" placeholder="Stock Manufacturer" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchManufacturer"
                                                                            WatermarkText="StockManufacturer" />
                                                                    </div>--%>

                                                                    <div class="input-group col-sm-2 ">
                                                                        <asp:TextBox ID="txtSearchModel" runat="server" placeholder="Stock Model" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchModel"
                                                                            WatermarkText="Stock Model" />
                                                                    </div>

                                                                    <div class="input-group col-sm-1 ">
                                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2 ">
                                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="false" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 ">
                                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 ">
                                                                        <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                            <asp:ListItem Value="">Stock Active</asp:ListItem>
                                                                            <asp:ListItem Value="True">Only Active</asp:ListItem>
                                                                            <asp:ListItem Value="False">Not Active</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-1 ">
                                                                        <asp:DropDownList ID="ddlSalesTag" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                            <asp:ListItem Value="">Sales Tag</asp:ListItem>
                                                                            <asp:ListItem Value="True">Only Tagged</asp:ListItem>
                                                                            <asp:ListItem Value="False">Not Tagged</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group date datetimepicker1" style="width: 180px;">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtExpiryDate" placeholder="Expiry Date" runat="server" class="form-control"></asp:TextBox>
                                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                    </div>

                                                                    <div class="input-group ">
                                                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                                                    </div>
                                                                    <div class="input-group ">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>





                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="datashowbox dispnone">
                                    <div class="row">
                                        <div class="dataTables_length showdata col-sm-6">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="finalgrid fullWidthTable searchfinal searchbar mainGridTable main-card card">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Timer runat="server" ID="Timer1" Interval="2000" OnTick="Timer1_Tick"></asp:Timer>
                                    <asp:GridView ID="GridView1" DataKeyNames="id" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
                                        OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Stock Item" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockItem">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server">
                                                <%#Eval("StockItem")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Category" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="textLeft" SortExpression="StockCategory">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="90px">
                                                <%#Eval("StockCategory")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Brand" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="textLeft" SortExpression="StockManufacturer">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("StockManufacturer")%>' data-toggle="tooltip"><%#Eval("StockManufacturer")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Model" HeaderStyle-CssClass="textLeft" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockModel">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label7" runat="server" Width="150px" data-placement="top" data-original-title='<%#Eval("StockModel")%>' data-toggle="tooltip">
                                                <%#Eval("StockModel")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Series" HeaderStyle-CssClass="textLeft"  ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockSeries">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label8" runat="server" Width="70px">
                                                <%#Eval("StockSeries")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Size" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockSeries" HeaderStyle-CssClass="textLeft">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelSize" runat="server" Width="50px">
                                                <%#Eval("StockSize")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" HeaderStyle-CssClass="textLeft" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label9" runat="server">
                                                <%#Eval("CompanyLocation")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="StockQuantity">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Width="60px">
                                                <%#Eval("StockQuantity")%></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%--    <asp:TemplateField HeaderText="Active" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="Active">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Width="40px">
                                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Eval("Active")%>' />
                                                                <label for="<%=chk.ClientID %>" runat="server" id="lblchk">
                                                                    <span></span>
                                                                </label>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sales" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="SalesTag">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Width="40px">
                                                                <asp:CheckBox ID="chk1" runat="server" Checked='<%# Eval("SalesTag")%>' />
                                                                <label for='<%=chk1.ClientID %>' runat="server" id="lblchk12">
                                                                    <span></span>
                                                                </label>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                            --%>
                                            <asp:TemplateField Visible="false" HeaderText="Stock Code" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="90px">
                                                <%#Eval("StockCode")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText=" Min. Stock" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="MinLevel">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label10" runat="server" Width="80px">
                                                <%#Eval("MinLevel")%></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Sold Not Del." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="StockSold">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server" Width="100px">
                                                <%#Eval("StockSold")%></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Net Avail" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="StockNetQty">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server" Width="70px">
                                                <%#Eval("StockNetQty")%></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" HeaderText="Alert" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label14" runat="server" Width="40px">
                                                <%# Convert.ToInt32(Eval("MinLevel")) > Convert.ToInt32(Eval("StockQuantity")) ? "<span style='color:Red;'>X&nbsp;X&nbsp;X</span>" : ""%>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" ItemStyle-Width="20px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="gvbtnDelete" runat="server" CommandName="Delete" ImageUrl="../../../images/icon_edit.png" CssClass="tooltips" data-toggle="tooltip"
                                                        data-placement="top" title="Update Stock Qty" data-original-title="Update Stock Quantity" CausesValidation="false" />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="verticaaline" />
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false" ItemStyle-Width="20px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="gvbtnEditItem" runat="server" CommandName="UpdateStock" CommandArgument='<%#Eval("StockItemID") %>' ImageUrl="../../../images/icon_edit.png"
                                                        data-toggle="tooltip" data-placement="top" data-original-title="Stock Item" CausesValidation="false"></asp:ImageButton>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="verticaaline" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="PDF Status" HeaderStyle-CssClass="">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnStockitemID2" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <asp:HyperLink ID="lnkPDFUploded" runat="server" CommandName="PDFUploded" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false" CssClass="btn btn-palegreen"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFUploded" Visible="false">
                                                            <i class="fa fa-file-pdf-o" style="color:green;font-size: 1rem;margin-right: 5px;"></i> PDF
                                                    </asp:HyperLink>
                                                    <asp:HyperLink ID="lnkPDFPending" runat="server" CommandName="PDFPending" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false" CssClass="btn btn-darkorange"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFPending" Visible="false">
                                                            <i class="fa fa-file-pdf-o" style="color:red;    font-size: 1rem;margin-right: 5px;"></i> PDF
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Actions" HeaderStyle-CssClass="center">
                                                <ItemTemplate>
                                                    <div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle noArrow"></button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList">

                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false">
                                                            <i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat ="server" ID="btnserial" CommandName="SerialNo" CommandArgument='<%#Eval("StockItemID") +";"+ Eval("CompanyLocationID")%>'>
                                                         <i class="fa fa-eye"></i> View
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="gvbtnUpload" runat="server" CommandName="Upload" CommandArgument='<%#Eval("StockItemID") %>' CausesValidation="false">
                                                            <i class="fa fa-upload"></i> Upload
                                                    </asp:LinkButton>
                                                   
                                                    <%-- <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" /> <i class="fa fa-edit"></i>
                                                    --%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             
                                            
                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td class="showEntryBtmBox">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Display</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                            </div>

                            <asp:Button ID="btnNULLData" Style="display: none;" Text="test" runat="server" />
                            <cc1:ModalPopupExtender ID="MPEUpdateStock" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="true" PopupControlID="divStockitem" TargetControlID="btnNULLData"
                                CancelControlID="btncanelStock">
                            </cc1:ModalPopupExtender>
                            <div id="divStockitem" runat="server" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div style="float: right">
                                                <asp:LinkButton ID="btncanelStock" CssClass="btn btn-danger btncancelicon" runat="server">Close</asp:LinkButton>
                                            </div>
                                            <h4 class="modal-title" id="H1">Update Stock Quantity</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <div class="formainline">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <label>Category</label>
                                                                <asp:Literal ID="ltcategory" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Stock Item</label>
                                                                <asp:HiddenField ID="hdnid" runat="server" />
                                                                <asp:Literal ID="ltstockitem" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Brand</label>
                                                                <asp:Literal ID="ltbrand" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Model</label>
                                                                <asp:Literal ID="ltmodel" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Series</label>
                                                                <asp:Literal ID="ltseries" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Stock Quantity</label>
                                                                <asp:Literal ID="ltstockquantity" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Location</label>
                                                                <asp:Literal ID="ltlocation" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Select Head</label>
                                                                <asp:DropDownList ID="ddlhead" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlhead_SelectedIndexChanged"
                                                                    AutoPostBack="true" aria-controls="DataTables_Table_0" class="myvalstockitem">
                                                                    <asp:ListItem Value="">select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                    ControlToValidate="ddlhead" Display="Dynamic" ValidationGroup="stockquantity"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Quantity</label>
                                                                <asp:TextBox ID="txtquantity" runat="server" MaxLength="8" CssClass="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                    ControlToValidate="txtquantity" Display="Dynamic" ValidationGroup="stockquantity"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^-?\d+$" Display="Dynamic"
                                                                    ErrorMessage="Enter valid digit" ControlToValidate="txtquantity" ValidationGroup="stockquantity"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^-[0-9]+$"
                                                                    Visible="false" ErrorMessage="Enter valid digit" ControlToValidate="txtquantity" Display="Dynamic"
                                                                    ValidationGroup="stockquantity"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div style="text-align: center;">
                                                                <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" CssClass="btn btn-danger savewhiteicon"
                                                                    ValidationGroup="stockquantity" />
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnsave" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLDataItem" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="true" PopupControlID="divItem" TargetControlID="btnNULLDataItem"
                                CancelControlID="btncanelItem">
                            </cc1:ModalPopupExtender>
                            <div id="divItem" runat="server" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div style="float: right">
                                                <asp:LinkButton ID="btncanelItem" CssClass="btn btn-danger btncancelicon" runat="server">Close</asp:LinkButton>
                                            </div>
                                            <h4 class="modal-title" id="myModalLabel">Stock Item</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <div class="formainline">
                                                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                                        <ContentTemplate>
                                                            <div class="form-group">
                                                                <label>Stock Item</label>
                                                                <asp:TextBox ID="txtStockItemU" runat="server" CssClass="form-control" Width="200px" MaxLength="255"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Brand</label>
                                                                <asp:TextBox ID="txtStockManufacturerU" runat="server" MaxLength="100" Width="200px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Model</label>
                                                                <asp:TextBox ID="txtStockModelU" runat="server" MaxLength="50" Width="200px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Series</label>
                                                                <asp:TextBox ID="txtStockSeriesU" runat="server" MaxLength="50" Width="200px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Active</label>
                                                                <asp:CheckBox ID="chkActiveU" runat="server" />
                                                                <label for="<%=chkActiveU.ClientID %>">
                                                                    <span></span>
                                                                </label>

                                                            </div>
                                                            <div id="Div5" class="form-group" runat="server" visible="false">
                                                                <label>Sales Tag</label>
                                                                <asp:CheckBox ID="chkSalesTagU" runat="server" />
                                                                <label for="<%=chkSalesTagU.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div style="text-align: center;">
                                                                <asp:Button ID="btnSaveItem" runat="server" Text="Save" OnClick="btnSaveItem_Click" CssClass="btn btn-danger savewhiteicon"
                                                                    ValidationGroup="stockitem" />
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnSaveItem" />
                                                        </Triggers>

                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hndStockItemID" runat="server" />
                            </div>


                            <cc1:ModalPopupExtender ID="ModalPopupExtenderUpload" runat="server" BackgroundCssClass="modalbackground"
                                CancelControlID="btnCancelUpload" DropShadow="false" PopupControlID="divUpload" TargetControlID="btnNULLUpload">
                            </cc1:ModalPopupExtender>

                            <div class="modal_popup" id="divUpload" runat="server" style="display: none; width: 100%;">
                                <div class="modal-dialog" style="width: 15%;">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header">
                                            <div style="float: right">
                                                <asp:LinkButton ID="btnCancelUpload" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                                                    Close
                                                </asp:LinkButton>
                                            </div>
                                            <h4 class="modal-title" id="H3">Upload Brochure</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <div class="formainline formnew">
                                                    <div class="clear"></div>
                                                    <div class="formainline">

                                                        <div class="topfileuploadbox marbtm15">

                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <div style="word-wrap: break-word;">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Upload File <span class="symbol required"></span>
                                                                        </label>
                                                                        <span class="">
                                                                            <asp:HiddenField ID="hdnStockItemID" runat="server" />
                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" />
                                                                            <div class="clear">
                                                                            </div>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf)$" Style="color: red;"
                                                                                Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This field is required." CssClass="reqerror"
                                                                                ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf" Style="color: red;"></asp:RequiredFieldValidator>
                                                                        </span>

                                                                        <div class="clear">
                                                                        </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <asp:Button ID="btnNULLUpload" Style="display: none;" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="ibtnUploadPDF" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <asp:PostBackTrigger ControlID="btnaddmodule" />
            <asp:PostBackTrigger ControlID="btnAddInverter" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
             <%--   $(document).ready(function () {
                    //$("p").on("click", function () {
                    //    alert("The paragraph was clicked.");
                    //});
                    alert("success");
                    $('#ModuleFileUpload').wrap('<a class="file-input-wrapper btn btn-default"></a>').parent().prepend($('<span></span>').html("Browse"));
                    $('<%=btnCancel.ClientID %>').click(function (e) {
                        alert("hello");
                        //$('#LangTable').append(' <br>------------<br> <a class="deletelanguage">Now my class is deletelanguage. click me to test it is not working.</a>');
                    });
                });
                $('<%=btnCancel.ClientID %>').click(function (e) {
                    alert("hello");
                    //$('#LangTable').append(' <br>------------<br> <a class="deletelanguage">Now my class is deletelanguage. click me to test it is not working.</a>');
                });--%>
</script>

</asp:Content>


