﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_JobPhotos : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static string SortDir;
    protected static string cdnURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cdnURL = ConfigurationManager.AppSettings["cdnURL"];   
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindInstaller();

            BindGrid(0);
        }
    }
    
    #region Bind GridView Code
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblProjects.tblProjects_GetData_JobPhotos(txtProjectNo.Text, ddlInstaller.SelectedValue, txtCustomer.Text, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlPhoto.SelectedValue, ddlSerialNo.SelectedValue);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        int iTotalRecords = 0;

        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        //PanTotal.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //HidePanels();
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                //     PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            PanTotal.Visible = false;
            divnopage.Visible = false;
            //ltrPage.Text = "";
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {

                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";

                    if (Convert.ToInt32(50) < dt.Rows.Count)
                    {
                        //========label Hide
                        divnopage.Visible = false;
                    }
                    else
                    {
                        divnopage.Visible = true;
                        iTotalRecords = dv.ToTable().Rows.Count;
                        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                        if (iEndRecord > iTotalRecords)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        if (iStartsRecods == 0)
                        {
                            iStartsRecods = 1;
                        }
                        if (iEndRecord == 0)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    }
                }
            }
        }

    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewSerialNo")
        {
            string arg = e.CommandArgument.ToString();

            string [] No = arg.Split(';');

            string ProjectID = No[0];
            hndProjectID.Value = ProjectID;
            hndProjectNumber.Value = No[1];

            DataTable dt = ClstblProjects.tbl_Installation_Project_ScannedSerial_GetDatByProjectID(ProjectID);

            if (dt.Rows.Count > 0)
            {
                grdSerialNo.DataSource = dt;
                grdSerialNo.DataBind();
            }
            ModalPopupExtenderViewSerialNo.Show();
        }

        if (e.CommandName == "ViewPhoto")
        {
            string ProjectID = e.CommandArgument.ToString();
            P_ProjectID.Value = ProjectID;
            DataTable dt = ClstblProjects.tbl_Installation_Photo_GetDateByProjectId(ProjectID);

            if (dt.Rows.Count > 0)
            {
                rptPhoto.DataSource = dt;
                rptPhoto.DataBind();
            }
            ModalPopupExtenderViewPhoto.Show();
        }

        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        DataView sortedView = new DataView(dt);

        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////// Don't Change Start
        ////string SortDir = string.Empty;
        //    if (dir == SortDirection.Ascending)
        //    {
        //        dir = SortDirection.Descending;
        //        SortDir = "Desc";
        //    }
        //    else
        //    {
        //        dir = SortDirection.Ascending;
        //        SortDir = "Asc";
        //    }
        ////////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();


    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Descending;
            }

            else
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        //GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //custompageIndex = GridView1.PageIndex + 1;

        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;

        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;
                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;
        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;
        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            //Response.Write(dv.ToTable().Rows.Count);
            //Response.End();
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        }
        else
        {
            ltrPage.Text = "";
        }
        //BindScript();

    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }

        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtProjectNo.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        txtCustomer.Text = string.Empty;
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlPhoto.SelectedValue = "0";
        ddlSerialNo.SelectedValue = "0";

        BindGrid(0);
    }

    protected void BindInstaller()
    {
        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();
    }
    
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        Response.Clear();
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "JobPhoto_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6 };

            string[] arrHeader = { "Project No", "Customer", "Installer", "System Details", "Booking Date", "Installed Complet" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    protected void ibtnPDF_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;

        if (ProjectID != "")
        {
            ClsTelerik_reports.Generate_SerialNumber(ProjectID);
        }
    }

    protected void ibtnExcel_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        string ProjectNo = hndProjectNumber.Value;

        DataTable dt = ClstblProjects.tbl_Installation_Project_ScannedSerial_GetDatByProjectID(ProjectID);

        Response.Clear();
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "SerialNo_" + ProjectNo + ".xls";

            int[] ColList = { 0, 1, 2 };

            string[] arrHeader = { "#", "Category", "SerialNo" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void btnPhotoDownload_Click(object sender, EventArgs e)
    {
        string ProjectID = P_ProjectID.Value;
        DataTable dt = ClstblProjects.tbl_Installation_Photo_GetDateByProjectId(ProjectID);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string downloadURL = cdnURL + "PanelInverterPhoto/" + dt.Rows[i]["Photo"];
                string destinationfile = HttpContext.Current.Request.PhysicalApplicationPath + "/userfiles/SerialNo/" + dt.Rows[i]["Photo"];

                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(downloadURL, destinationfile);
                }
            }
        }
    }
}