<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="leadtracker.aspx.cs" Inherits="admin_adminfiles_company_leadtracker" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>

    <style>
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }

        input[type=checkbox] {
            opacity: inherit !important;
            position: inherit !important;
    </style>
    <script type="text/javascript">

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {

                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

    </script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            debugger;
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
            }
        }
    </script>
    <script type="text/javascript">

        function doMyAction1() {

            HighlightControlToValidate();
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#b94a48");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <%--<asp:PostBackTrigger ControlID="btnAssidnLead" />--%>
            <%--<asp:PostBackTrigger ControlID="lbtnExport" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-display2 icon"></i>Lead Tracker</h5>
        <div id="fdfs" class="pull-right" runat="server">
            <%--<ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
            </ol>--%>
        </div>
    </div>

    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                </div>
            </div>

            <div class="searchfinal searchBox searchFilterSection" onkeypress="DefaultEnter(event);">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
                                <div class="col-sm-12">
                                    <div class="inlineblock">
                                        <div class="row">
                                            <div class="input-group" id="divCustomer" runat="server">
                                                <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                    <%--<asp:ListItem Value="1">Type</asp:ListItem>--%>
                                                    <asp:ListItem Value="0">Type</asp:ListItem>
                                                    <asp:ListItem Value="1">Lead</asp:ListItem>
                                                    <asp:ListItem Value="2">Prospect</asp:ListItem>
                                                    <asp:ListItem Value="3">Customer</asp:ListItem>
                                                    <asp:ListItem Value="4">Installer</asp:ListItem>
                                                    <asp:ListItem Value="5">Vendor</asp:ListItem>
                                                    <asp:ListItem Value="6">WholeCust</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group" id="div1" runat="server">
                                                <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true" Width="120px"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="0">Status</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-2 " id="div2" runat="server" visible="false">
                                                <asp:DropDownList ID="ddlSearchSource1" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource1_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="1">Source</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group">
                                                <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalproject" OnSelectedIndexChanged="ddlSource_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Select Source</asp:ListItem>
                                                    <asp:ListItem Value="3">Door to Door</asp:ListItem>
                                                    <asp:ListItem Value="2">Lead Source</asp:ListItem>
                                                    <asp:ListItem Value="1">Tele Marketing</asp:ListItem>


                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-2" id="div3" runat="server" visible="false">
                                                <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-1 " id="tdTeam" runat="server">
                                                <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Team</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                            <div class="input-group col-sm-1">
                                                <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtContactSearch"
                                                    WatermarkText="Contact" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                </cc1:AutoCompleteExtender>
                                            </div>


                                            <div class="input-group col-sm-1 ">
                                                <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtMobile"
                                                    WatermarkText="Mobile" />
                                            </div>
                                            <div class="input-group col-sm-1 ">
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtEmail"
                                                    WatermarkText="Email" />
                                            </div>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                    WatermarkText="Project Number" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            </div>

                                            <div class="input-group col-sm-1">
                                                <asp:DropDownList ID="ddlSelectitems" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">CustEntered</asp:ListItem>
                                                    <asp:ListItem Value="2">NextFollowUp</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                            </div>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                            </div>

                                            <div class="input-group spical multiselect col-sm-1" style="display: none">
                                                <dl class="dropdown">
                                                    <dt>
                                                        <a href="#">
                                                            <span class="hida" id="spanselect">Select</span>
                                                            <p class="multiSel"></p>
                                                        </a>
                                                    </dt>
                                                    <dd id="ddproject" runat="server">
                                                        <div class="mutliSelect" id="mutliSelect">
                                                            <ul>
                                                                <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <li>
                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                            <%--  <span class="checkbox-info checkbox">--%>
                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                            <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                <span></span>
                                                                            </label>
                                                                            <%-- </span>--%>
                                                                            <label class="chkval">
                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                            </label>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div class="input-group ">
                                                <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btngray wid100btn btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                            </div>
                                            <div class="input-group ">
                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray wid100btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="input-group col-sm-1">
                                                <asp:DropDownList ID="ddlSalesRepSearch" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                </asp:DropDownList>
                                                </div>
                                                <div class="input-group">
                                                <asp:Button ID="btnAssidnLead" runat="server" Text="Assign lead" CssClass="btn btn-s-md btn-danger btngray btn-rounded"
                                                    OnClick="btnAssidnLead_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                            

                        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                            OkControlID="btnOK" TargetControlID="btnNULL">
                        </cc1:ModalPopupExtender>
                        <div runat="server" id="divFollowUp" style="display: none;" class="modal_popup addDocumentPopup">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header">
                                        <div class="modalHead">


                                            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                                                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup" />

                                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                Add FollowUp</h4>
                                            <div>
                                                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal" causesvalidation="false">
                                                    Close
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body formGrid paddnone" style="max-height: 500px;">
                                        <div class="panel-body">
                                            <div class="formainline row dropDownwid100">
                                                <div class="form-group col-md-4">
                                                    <asp:Label ID="Label3" runat="server" class="control-label">Contact</asp:Label>
                                                    <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Eurosolar</asp:ListItem>
                                                        <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                        ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                </div>


                                                <div class="form-group col-md-4">
                                                    <asp:Label ID="Label11" runat="server" class="control-label">
                                               Select</asp:Label>
                                                    <asp:DropDownList ID="ddlManager" runat="server" OnSelectedIndexChanged="ddlManager_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true"
                                                        CssClass="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Follow Up</asp:ListItem>
                                                        <asp:ListItem Value="2">Manager</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlManager" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                </div>

                                                <div class="form-group col-md-4" id="divNextDate" runat="server">
                                                    <div class="row">
                                                        <div class=" col-sm-12">
                                                            <asp:Label ID="Label23" runat="server" class="control-label">Next Followup Date</asp:Label>
                                                            <div class="input-group date wid100 datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">
                                                                </asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                    ValidationGroup="info" ErrorMessage="" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-8" runat="server" id="divDescription">

                                                    <asp:Label ID="Label22" runat="server" class="control-label">
                                                Description</asp:Label>

                                                    <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                        ValidationGroup="info" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>

                                                <div class="form-group col-md-4">

                                                    <asp:Label ID="Label19" runat="server" class="control-label">Reject</asp:Label>
                                                    <asp:DropDownList ID="ddlreject" runat="server" AppendDataBoundItems="true"
                                                        CssClass="myval">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="" CssClass=""
                                                        ControlToValidate="ddlreject" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>

                                                </div>




                                                <div class="form-group  col-md-12  textRight">

                                                    <span>
                                                        <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                            ValidationGroup="info" CssClass="btn largeButton greenBtn btnaddicon redreq addwhiteicon center-text" />
                                                        <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                            CssClass="btn" Text=" OK " />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seachfinal">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="tablescrolldiv xscroll" style="overflow: auto; margin-bottom: 10px;">
                                                        <%--<div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">--%>
                                                        <div id="divgrdFollowUp" runat="server" class="content padtopzero marbtm50 finalgrid">
                                                            <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                                AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover popupTableGrid1">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#Eval("Contact") %>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Next Followup Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#Eval("Description") %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Reject" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#Eval("Reject").ToString()=="1"?"Yes":Eval("Reject").ToString()=="0"?"No":"" %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                <%-- <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />--%>
                                                                <EmptyDataRowStyle Font-Bold="True" />
                                                                <%--<RowStyle CssClass="GridviewScrollItem" />--%>
                                                            </asp:GridView>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
                        <asp:HiddenField ID="hndCustomerID" runat="server" />

                        <cc1:ModalPopupExtender ID="ModalPopupExtenderStatus" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
                            OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
                        </cc1:ModalPopupExtender>
                        <div id="divUpdateStatus" runat="server" style="display: none;" class="modal_popup">

                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header">
                                        <div style="float: right">
                                            <asp:LinkButton ID="LinkButton5" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                            Close
                                            </asp:LinkButton>
                                        </div>
                                        <h4 class="modal-title" id="H1">
                                            <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                            Project Cancel Reason</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <div class="formainline">

                                                <div class="form-group marbtm5 row" id="trCancel" runat="server">
                                                    <asp:Label ID="Label13" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Reason:</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlProjectCancelID" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ValidationGroup="status" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group row">
                                                    <asp:Label ID="Label15" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Decsription:</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtprojectcanceldesc" TextMode="MultiLine" Columns="4" Rows="4" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                        <br />

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ValidationGroup="status" ControlToValidate="txtprojectcanceldesc" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group marginleft">
                                                    <asp:Label ID="Label16" runat="server" class="col-sm-4 control-label">
                                                    </asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:Button ID="ibtnUpdateStatus" runat="server" Text="Update" OnClick="ibtnUpdateStatus_Onclick"
                                                            CssClass="btn btn-primary savewhiteicon" ValidationGroup="status" />
                                                        <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                                                            CssClass="btn" Text=" OK " />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
                        <asp:HiddenField ID="hndStatusCustomerID" runat="server" />

                        <cc1:ModalPopupExtender ID="ModalPopupCancelLead" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="btnCancelLead" DropShadow="false" PopupControlID="divCancelLead"
                            OkControlID="btnOKLead" TargetControlID="btnNULLLead">
                        </cc1:ModalPopupExtender>
                        <div id="divCancelLead" runat="server" style="display: none;" class="modal_popup">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header">
                                        <div style="float: right">
                                            <asp:LinkButton ID="btnCancelLead" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                                            Close
                                            </asp:LinkButton>
                                        </div>
                                        <h4 class="modal-title" id="H2" style="padding-top: 4px">
                                            <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                                            <b>Lead Cancel Reason</h4>
                                        </b>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <div class="formainline">

                                                <div class="form-group marbtm5 row">
                                                    <asp:Label ID="Label17" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Reason:</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlContLeadCancelReason" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ValidationGroup="cancellead" ControlToValidate="ddlContLeadCancelReason" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group row">
                                                    <asp:Label ID="Label20" runat="server" class="col-sm-4 control-label">
                                                <strong> Cancel Decsription:</strong></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtcanceldesc" TextMode="MultiLine" Columns="4" Rows="4" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ValidationGroup="cancellead" ControlToValidate="txtcanceldesc" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group marginleft">
                                                    <asp:Label ID="Label28" runat="server" class="col-sm-4 control-label">
                                                    </asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:Button ID="ibtnCancelLead" runat="server" Text="Update" OnClick="ibtnCancelLead_Click"
                                                            CssClass="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="cancellead" />
                                                        <asp:Button ID="btnOKLead" Style="display: none; visible: false;" runat="server"
                                                            CssClass="btn" Text=" OK " />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLLead" Style="display: none;" runat="server" />
                        <asp:HiddenField ID="hndLeadCustID" runat="server" />

                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                            <ProgressTemplate>
                                <%--   <div class="loading-container">
                                               <div class="loader"></div>
                                                       </div>--%>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="updateprogress1"
                            PopupControlID="updateprogress1" BackgroundCssClass="modalPopup" />



                        <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLEdit">
                        </cc1:ModalPopupExtender>

                        <div class="modal_popup addDocumentPopup" id="divEdit" runat="server" style="display: none; width: 100%">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header">
                                        <div class="modalHead">
                                            <h4 class="modal-title" id="H3">Update Customer Detail</h4>
                                            <div>
                                                <asp:LinkButton ID="btnCancelEdit" CausesValidation="false" OnClick="btnCancelEdit_Click" runat="server" CssClass="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal">Close
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-body formGrid" style="overflow-y: scroll; height: 600px;">
                                        <div class="">
                                            <div class="formainline formnew">

                                                <div class="alert alert-info" id="lbleror" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this address aleardy Exist</strong>
                                                </div>
                                                <div class="alert alert-info" id="lbleror1" runat="server" visible="false">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;Record with this Email or Mobile aleardy Exist</strong>
                                                </div>

                                                <div class="clear"></div>
                                                <div class="form-group spical">
                                                    <label class="control-label">Name:</label>
                                                    <div class="onelindiv row">
                                                        <span class="mrdiv col-md-4">
                                                            <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                placeholder="Salutation"></asp:TextBox>
                                                        </span>
                                                        <span class="fistname col-md-4">
                                                            <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                placeholder="First Name"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                                ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                        </span>
                                                        <span class="lastname col-md-4">
                                                            <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                placeholder="Last Name"></asp:TextBox>
                                                        </span>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="Div5" class="form-group spicaldivin" runat="server" visible="false">
                                                    <label class="control-label">Street Address<span class="symbol required"></span> </label>

                                                    <asp:HiddenField ID="hndaddress" runat="server" />
                                                    <asp:TextBox ID="txtStreetAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getParsedAddress();"></asp:TextBox>
                                                    <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                        ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company"
                                                        ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="comperror"
                                                        ControlToValidate="txtStreetAddressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>

                                                    <asp:HiddenField ID="hndstreetno" runat="server" />
                                                    <asp:HiddenField ID="hndstreetname" runat="server" />
                                                    <asp:HiddenField ID="hndstreettype" runat="server" />
                                                    <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                    <asp:HiddenField ID="hndunittype" runat="server" />
                                                    <asp:HiddenField ID="hndunitno" runat="server" />
                                                    <div id="validaddressid" style="display: none">
                                                        <img src="../../../images/check.png" alt="check">Address is valid.
                                                    </div>
                                                    <div id="invalidaddressid" style="display: none">
                                                        <img src="../../../images/x.png" alt="cross">
                                                        Address is invalid.
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>
                                                <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                        <ContentTemplate>--%>


                                                <div class="form-group spicaldivin">

                                                    <label class="control-label">Street Address<span class="symbol required"></span> </label>

                                                    <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                        ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                    <div class="clear"></div>
                                                </div>



                                                <div class="form-group spicaldivin row">
                                                    <span class="col-md-6">
                                                        <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox>
                                                    </span>
                                                    <span class="col-md-6">
                                                        <label class="control-label">Unit Type<span class="symbol required"></span> </label>

                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>

                                                    </span>
                                                    <div class="clear"></div>

                                                </div>
                                                <div class="form-group spicaldivin row">
                                                    <span class="col-md-4">
                                                        <label class="control-label" style="width: 150px;">Street No</label>
                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" CssClass="comperror" Style="width: 180px!important;"
                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <span class="col-md-4">
                                                        <div class="autocompletedropdown">
                                                            <label class="control-label" style="width: 150px;">Street Name<span class="symbol required"></span> </label>
                                                            <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>

                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass="comperror"
                                                                ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                            <div id="Divvalidstreetname" style="display: none">
                                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                                            </div>
                                                            <div id="DivInvalidstreetname" style="display: none">
                                                                <img src="../../../images/x.png" alt="cross">
                                                                Address is invalid.
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <span class="col-md-4">
                                                        <div class="marginbtm15" id="divsttype" runat="server">
                                                            <asp:Label ID="Label18" runat="server">Street Type</asp:Label>
                                                            <div id="Div8" class="drpValidate" runat="server">
                                                                <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" AutoPostBack="true" CssClass="myval" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                                    <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                                    ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="editdetail" InitialValue=""> </asp:RequiredFieldValidator>

                                                            </div>
                                                        </div>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>

                                                <%--    </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>

                                                <div class="form-group spicaldivin row">
                                                    <span class="col-md-4">

                                                        <label class="control-label" style="width: 150px;">Street City<span class="symbol required"></span> </label>

                                                        <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control" AutoPostBack="true"
                                                            OnTextChanged="ddlStreetCity_TextChanged" MaxLength="50"></asp:TextBox>


                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                    </span>

                                                    <span class="col-md-4">

                                                        <label class="control-label" style="width: 150px;">Street State<span class="symbol required"></span> </label>

                                                        <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control"
                                                            Enabled="false"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>
                                                    </span>

                                                    <span class="col-md-4">
                                                        <label class="control-label" style="width: 180px;">Street Post Code<span class="symbol required"></span> </label>
                                                        <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                            Enabled="false"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter a number"
                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ValidationGroup="editdetail" ControlToValidate="txtStreetPostCode" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear"></div>
                                                    </span>
                                                </div>
                                                <%-- <div class="form-group spicaldivin">--%>
                                                <div class="form-group spical row">
                                                    <span class="col-md-12">
                                                        <label class="control-label">
                                                            Email:<span class="symbol required"></span>
                                                        </label>
                                                    </span><span class="col-md-12">
                                                        <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="editdetail"></asp:RequiredFieldValidator>

                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group spical row">
                                                    <span class="col-md-6">
                                                        <label class="control-label">
                                                            Mobile:
                                                        </label>
                                                        <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                            ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>



                                                    </span>
                                                    <span class="col-md-6">

                                                        <label class="control-label">Phone</label>

                                                        <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                    ValidationGroup="editdetail" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                    ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>--%>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="form-group spical row radioButon">
                                                    <span class="col-md-6">
                                                        <label class="control-label">Solar Type</label>
                                                        <div class="radio radio-info radio-inline" style="padding-left: 0px;">
                                                            <label for="<%=rblResCom1.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                <span class="text">Res&nbsp;</span>
                                                            </label>

                                                            <label for="<%=rblResCom2.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                <span class="text">Com&nbsp;</span>
                                                            </label>

                                                        </div>
                                                    </span>
                                                    <span class="col-md-6">
                                                        <label class="control-label">Area</label>

                                                        <div class="radio radio-info radio-inline">
                                                            <label for="<%=rblArea1.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                                <span class="text">Metro&nbsp;</span>
                                                            </label>

                                                            <label for="<%=rblArea2.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                                <span class="text">Regional&nbsp;</span>
                                                            </label>
                                                        </div>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="textRight">
                                                    <asp:Button ID="ibtnEditDetail" runat="server" Text="Update" OnClick="ibtnEditDetail_Click"
                                                        CssClass="btn largeButton greenBtn savewhiteicon btnsaveicon" ValidationGroup="editdetail" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:Button ID="btnNULLEdit" Style="display: none;" runat="server" />
                        <asp:HiddenField ID="hndEditCustID" runat="server" />

                        <asp:HiddenField runat="server" ID="hdncountdata" />
        </asp:Panel>

        <div class="content customerTable searchfinal searchbar mainGridTable main-card card" id="divgrid" runat="server">
            <div id="leftgrid" runat="server">
                <asp:HiddenField ID="HfCustId" runat="server" />
                <div id="PanGrid" runat="server" class="finalgrid" visible="false">
                    <div class="table-responsive xscroll  noPagination">
                        <asp:GridView ID="GridView1" DataKeyNames="CustomerId" runat="server" CssClass="tooltip-demo  text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                            <Columns>
                                  <asp:TemplateField HeaderText="Select Data">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server"  AutoPostBack="true"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerId") %>','tr<%# Eval("CustomerId") %>');">
                                            <img id='imgdiv<%# Eval("CustomerId") %>' src="../../../images/arrowright.png" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company Name" SortExpression="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server"><%#Eval("CompanyName")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="Label91" runat="server"><%#Eval("ContMobile")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SubSource" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ContMobile"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustSourceSub")%>'><%#Eval("CustSourceSub")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'><%#Eval("StreetAddress")%> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Taluka" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="CustType"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="">
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" ><%#Eval("Taluka")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="District" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="StreetState">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server"><%#Eval("District")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Next Follow-Up" ItemStyle-VerticalAlign="Top" ItemStyle-CssClass="tdspecialclass spicalheghtnew"
                                    HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" SortExpression="NextFollowupDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNextFollowUpDate" runat="server" CssClass="gridmainspan" Style="display: initial">
                                            <span class="name badge yellowBadge"><%#Eval("Nextfollowupdate","{0:dd MMM yyyy}")%></span>
                                            <div class="contacticonedit">
                                                <asp:LinkButton ID="lbtnFollowUpNote" CommandName="addfollowupnote" CommandArgument='<%#Eval("CustomerID")%>' CssClass="btn btn-info btngray btn-xs"
                                                    CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Add FollowUp">
                                                              <i class="fa fa-calendar"></i></asp:LinkButton>
                                            </div>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top"
                                    HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFollowUpNote" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>'
                                            Width="150px"><%#Eval("Description")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" SortExpression="AssignedTo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label33" runat="server"><%#Eval("AssignTo")%></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                    <ItemTemplate>
                                        <tr id='tr<%# Eval("CustomerId") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                            <td colspan="98%" class="details">
                                                <div id='div<%# Eval("CustomerId") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover subtablecolor">
                                                        <tr class="GridviewScrollItem">
                                                            <td runat="server" id="tdassignto"><b>Source</b></td>
                                                            <td runat="server" id="tdassignto1">
                                                                <asp:Label ID="Label711" runat="server"><%#Eval("CustSource")%> </asp:Label></td>
                                                            <td><b>Email</b></td>
                                                            <td runat="server" id="tdcompnum">
                                                                <asp:Label ID="Label111" runat="server"> <%#Eval("ContEmail")%> </asp:Label></td>
                                                            <td><b>Mobile</b></td>
                                                            <td runat="server" id="td2">
                                                                <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ContMobile")%>'><%#Eval("ContMobile")%></asp:Label>
                                                            </td>


                                                        </tr>
                                                        <tr>
                                                            <td><b>Notes</b></td>
                                                            <td runat="server" id="td1">
                                                                <asp:Label ID="lblFollowUpNote1" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label>
                                                            </td>
                                                            <td><b>Project Number</b></td>
                                                            <td runat="server" id="td3">
                                                                <asp:Label ID="Label4" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ProjectNumber")%>'><%#Eval("ProjectNumber")%></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle />

                            <PagerTemplate>
                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                <div class="pagination">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                </div>
                            </PagerTemplate>
                            <PagerStyle CssClass="paginationGrid" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                        </asp:GridView>
                    </div>
                    <div class="paginationnew1" runat="server" id="divnopage">
                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td class="showEntryBtmBox">
                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                        aria-controls="DataTables_Table_0" class="myval">
                                        <asp:ListItem Value="25">Display</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                </td>
                                <td>
                                    <table id="tblassign" runat="server" border="0" cellpadding="5" cellspacing="0" class="marginLeftAuto">
                                        <tr>
                                            <td align="right" style="padding: 0 !important;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                //$("[data-toggle=tooltip]").tooltip();

            }
        </script>


        <script type="text/javascript">
            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });


        </script>
    </div>




</asp:Content>
