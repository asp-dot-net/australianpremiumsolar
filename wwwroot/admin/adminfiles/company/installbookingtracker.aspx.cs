using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Collections.Generic;
using System.Linq;

public partial class admin_adminfiles_company_installbookingtracker : System.Web.UI.Page
{
    decimal totalpanels = 0;
    static DataView dv;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    public static string openModal = "false";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            //do something 
        }
        //GridView1.Visible = true;
        //BindGrid(0);
        //projectpreinst.UserControlButtonClicked += new includes_controls_projectpreinst.customHandler(MyParentMethod);
        if (openModal == "true")
        {
            //btnUpdatePreInst
            CheckBox chkElecDistOK = projectpreinst.FindControl("chkElecDistOK") as CheckBox;
            DropDownList ddlInstaller = projectpreinst.FindControl("ddlInstaller") as DropDownList;

            TextBox txtInstallCity = projectpreinst.FindControl("txtInstallCity") as TextBox;
            ImageButton btnCreateSTCForm = projectpreinst.FindControl("btnCreateSTCForm") as ImageButton;
            ImageButton btnCustAddress = projectpreinst.FindControl("btnCustAddress") as ImageButton;
            ImageButton btnUpdateAddress = projectpreinst.FindControl("btnUpdateAddress") as ImageButton;
            Button btnMove = projectpreinst.FindControl("btnMove") as Button;
            Button btnUpdatePreInst = projectpreinst.FindControl("btnUpdatePreInst") as Button;
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(btnUpdatePreInst);

            if (btnMove.Text != "" || btnUpdateAddress.ImageUrl != "" || btnCreateSTCForm.ImageUrl != "" || btnCustAddress.ImageUrl != "" || chkElecDistOK.Checked == true || chkElecDistOK.Checked == false || ddlInstaller.SelectedValue.ToString() != "" || ddlInstaller.SelectedValue.ToString() == "" || txtInstallCity.Text != "" || txtInstallCity.Text == "")
            {
                ModalPopupExtender1.Show();

            }

        }
        HiddenField hdnclosepopup1 = projectpreinst.FindControl("hdnclosepopup") as HiddenField;
        if (hdnclosepopup1.Value == "1")
        {

            ModalPopupExtender1.Hide();
            //PanGrid.Visible = false;
            BindGrid(0);
        }
        //Response.Write(hdnclosepopup1.Value);
        //Response.End();
        // BindGrid(0);
        if (!IsPostBack)
        {

            ModalPopupExtender1.Hide();
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;
            HiddenField hdnclosepopup34 = projectpreinst.FindControl("hdnclosepopup") as HiddenField;
            hdnclosepopup34.Value = "0";
            //if (hdnclosepopup.Value == "2")
            //{

            //    ModalPopupExtender1.Hide();
            //    BindGrid(0);
            //}

            BindGrid(0);

            //includes_controls_projectpreinst objclass1 = new includes_controls_projectpreinst();
            //objclass1.datevalidator();
            //projectpreinst.datevalidator1();
            // UserControl projectpreinst = (UserControl)FindControl("projectpreinst");

            //TextBox txtInstallBookingDate = (TextBox)projectpreinst.FindControl("txtInstallBookingDate");
            //RangeValidator DateRange = (RangeValidator)projectpreinst.FindControl("DateRange");
            //DateRange.MinimumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(-1).ToString());
            //DateRange.MaximumValue = SiteConfiguration.ConvertToSystemDateDefault(DateTime.Now.Date.AddYears(200).ToString());

            //if ((Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
            //{
            //    tdExport.Visible = false;
            //}

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);

            //txtStartDate.Text= DateTime.Now.ToString("dd/MM/yyyy");
            //txtEndDate.Text= DateTime.Now.ToString("dd/MM/yyyy");

            txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.ToShortDateString()));


            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            BindSearchDropdown();
            BindGrid(0);
        }
    }


    public void BindSearchDropdown()
    {
        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataBind();

        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlStockAllocationStore.Items.Clear();
        ddlStockAllocationStore.Items.Add(item);

        ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
        ddlStockAllocationStore.DataValueField = "CompanyLocationID";
        ddlStockAllocationStore.DataTextField = "CompanyLocation";
        ddlStockAllocationStore.DataMember = "CompanyLocation";
        ddlStockAllocationStore.DataBind();

        ListItem item2 = new ListItem();
        item2.Text = "Installer";
        item2.Value = "";
        ddlInstaller.Items.Clear();
        ddlInstaller.Items.Add(item2);

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlElecDist.DataSource = ClstblCustSource.tblElecDistributor_Select();
        ddlElecDist.DataValueField = "ElecDistributorID";
        ddlElecDist.DataTextField = "ElecDistributor";
        ddlElecDist.DataBind();


        ddldivision.DataSource = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
        ddldivision.DataValueField = "Id";
        ddldivision.DataMember = "DivisionName";
        ddldivision.DataTextField = "DivisionName";
        ddldivision.DataBind();

        ddlSubDivision.DataSource = ClstblProjects.tbl_GetSubdivision();
        ddlSubDivision.DataMember = "SubDivisionName";
        ddlSubDivision.DataTextField = "SubDivisionName";
        ddlSubDivision.DataValueField = "ID";
        ddlSubDivision.DataBind();


        ddlFinanceOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlFinanceOption.DataValueField = "FinanceWithID";
        ddlFinanceOption.DataTextField = "FinanceWith";
        ddlFinanceOption.DataMember = "FinanceWith";
        ddlFinanceOption.DataBind();

        ddlSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        ddlSearchStatus.DataValueField = "ProjectStatusID";
        ddlSearchStatus.DataTextField = "ProjectStatus";
        ddlSearchStatus.DataMember = "ProjectStatus";
        ddlSearchStatus.DataBind();

        ddlchnlpart.DataSource = ClstblState.tblGetChnlPartner();
        ddlchnlpart.DataMember = "empFullName";
        ddlchnlpart.DataTextField = "empFullName";
        ddlchnlpart.DataValueField = "empFullName";
        ddlchnlpart.DataBind();
    }

    protected DataTable GetGridData()
    {
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //string employeeId = st.EmployeeID;

        //if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance"))
        //{
        //    employeeId = "";
        //}
        ////Response.Write(employeeId + "--" + txtClient.Text + "--" + txtProjectNumber.Text.Trim() + "--" + txtMQNsearch.Text + "--" + txtSearchStreet.Text + "--" + txtSerachCity.Text + "--" + ddlSearchState.SelectedValue + "--" + txtSearchPostCodeFrom.Text.Trim() + "--" + txtSearchPostCodeTo.Text.Trim() + "--" + ddlSearchStatus.SelectedValue + "--" + ddlProjectTypeID.SelectedValue + "--" + txtPModel.Text + "--" + txtIModel.Text + "--" + txtSysCapacityFrom.Text + "--" + txtSysCapacityTo.Text + "--" + ddlFinanceOption.SelectedValue + "--" + ddlElecDistAppDate.SelectedValue+"--"+ ddldatetype.SelectedValue + "--" + txtStartDate.Text.Trim() + "--" + txtEndDate.Text.Trim() + "--" + txtpurchaseNo.Text+"--"+ chkishistoric.Checked.ToString());
        ////Response.End();
        //DataTable dt = ClstblProjects.tblProjects_InstallBookingTracker1(employeeId, txtClient.Text, txtProjectNumber.Text.Trim(), txtMQNsearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCodeFrom.Text.Trim(), txtSearchPostCodeTo.Text.Trim(), ddlSearchStatus.SelectedValue, ddlProjectTypeID.SelectedValue, txtPModel.Text, txtIModel.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlFinanceOption.SelectedValue, ddlElecDistAppDate.SelectedValue, ddldatetype.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpurchaseNo.Text, chkishistoric.Checked.ToString());

        //if (dt.Rows.Count > 0)
        //{
        //    lblTotalPanels.Text = dt.Rows[0]["TotalPanels"].ToString();
        //}
        //else
        //{
        //    lblTotalPanels.Text = "0";
        //}

        //// ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        DataTable dt = new DataTable();
        //dt = ClstblProjects.tblGetAllJobData();
        string discom = string.Empty;
        string taluka = string.Empty;
        string subdivi = string.Empty;
        string divison = string.Empty;
        string cnlPartner = string.Empty;
        string ProjectNumber = string.Empty;


        if (ddlElecDist.SelectedItem.Text != "Select Discom")
        {
            discom = ddlElecDist.SelectedItem.Text;
        }
        if (ddldivision.SelectedValue !="0")
        {
            divison = ddldivision.SelectedItem.Text;
        }
        if (ddlSubDivision.SelectedValue != "0")
        {
            subdivi = ddlSubDivision.SelectedItem.Text;
        }
        if (ddlchnlpart.SelectedValue != "0")
        {
            cnlPartner = ddlchnlpart.SelectedItem.Text;
        }
        if (ddltaluka.Text != "")
        {
            taluka = ddltaluka.Text;
        }
        if (txtProjectNumber1.Text != "")
        {
            ProjectNumber = txtProjectNumber1.Text;
        }
        //dt = ClstblProjects.tblGetAllInstallerData(ddlElecDist.SelectedItem.Text, ddlSubDivision.SelectedItem.Text, ddldivision.SelectedItem.Text, ddltaluka.Text);
        dt = ClstblProjects.tblGetAllInstallerData(discom, taluka, subdivi, divison, cnlPartner, ProjectNumber);
        if (ddlSelectitems.SelectedValue != "0")
        {
            if (!string.IsNullOrEmpty(txtStartDate1.Text) && !string.IsNullOrEmpty(txtEndDate1.Text))
            {
                DateTime startDate = DateTime.ParseExact(txtStartDate1.Text, "dd/MM/yyyy", null);
                DateTime edDate = DateTime.ParseExact(txtEndDate1.Text, "dd/MM/yyyy", null);
                string selectedValue = ddlSelectitems.SelectedValue;
                if (selectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date <= edDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
            if (!string.IsNullOrEmpty(txtStartDate1.Text))
            {
                DateTime startDate = DateTime.ParseExact(txtStartDate1.Text, "dd/MM/yyyy", null);

                string selectedValue = ddlSelectitems.SelectedValue;
                if (selectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
            if (!string.IsNullOrEmpty(txtEndDate1.Text))
            {
                DateTime edDate = Convert.ToDateTime(txtEndDate1.Text);
                string selectedValue = ddlSelectitems.SelectedValue;
                if (selectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
        }

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        //Response.End();

        if (dt.Rows.Count == 0)
        {

            // divTotal.Visible = false;
            SetNoRecords();
            //PanNoRecord.Visible = true;

            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {

                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //=label Hide
                    divnopage.Visible = true;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        // dt = GetGridData();

        ////////////// Don't Change Start

        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;

        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        BindGrid(0);

        //List<FilterData> studentList = new List<FilterData>();

        //DataTable dt = new DataTable();
        //dv = new DataView(dt);
        //dt = ClstblProjects.tblGetAllJobDataBySearch();
        //List<FilterData> mDataList = new List<FilterData>();

        //studentList = (from DataRow dr in dt.Rows
        //               select new FilterData()
        //               {
        //                   ProjectId = dr["ProjectID"].ToString(),
        //                   customername = dr["CustomerName"].ToString(),
        //                   projectnumber = dr["ProjectNumber"].ToString(),
        //                   ConsumerNumber = dr["ConsumerNumber"].ToString(),
        //                   MobileNumber = dr["MobileNumber"].ToString(),
        //                   Discom = dr["Discom"].ToString(),
        //                   Divison = dr["divisionId"].ToString(),
        //                   SubDivision = dr["Subdivision"].ToString(),
        //                   Address = dr["Paddress"].ToString(),
        //                   state = dr["StreetState"].ToString(),
        //                   City = dr["StreetCity"].ToString(),
        //                   Taluka = dr["Taluka"].ToString(),
        //                   FirstQuoteDate = dr["ProjectQuoteDate"].ToString(),
        //                   pannelmodel = dr["panelModel"].ToString(),
        //                   nvertormodel = dr["panelModel"].ToString(),
        //                   Kw = dr["KW"].ToString(),
        //                   Employeename = dr["Employeename"].ToString(),
        //                   ApplicationStatus = dr["ApplicationStatus"].ToString(),
        //                   ProjectStatus = dr["ProjectStatus"].ToString(),
        //                   BookingDate = dr["BookingDate"].ToString(),
        //                   Project = dr["Project"].ToString(),
        //                   inverterDetails = dr["inverterDetails"].ToString(),
        //                   ProjectNotes = dr["ProjectNotes"].ToString(),
        //                   InstallerNotes = dr["InstallerNotes"].ToString(),
        //                   RooftopArea = dr["RooftopArea"].ToString(),
        //                   stocksize = dr["stocksize"].ToString(),
        //                   panelDetail = dr["panelDetail"].ToString(),
        //                   CustomerID = dr["CustomerID"].ToString(),
        //                   QuoteSent = dr["QuoteSent"].ToString(),
        //                   QuoteAccepted = dr["QuoteAccepted"].ToString(),
        //                   DepositReceived = dr["DepositReceived"].ToString(),
        //                   ActiveDate = dr["ActiveDate"].ToString(),
        //                   InstallCompleted = dr["InstallCompleted"].ToString(),
        //                   followupdate = dr["followupdate"].ToString(),
        //                   InstallBookingDate = dr["InstallBookingDate"].ToString(),

        //               }).ToList();


        //if (!string.IsNullOrEmpty(txtClient.Text))
        //{
        //    studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();
        //}
        //if (!string.IsNullOrEmpty(txtProjectNumber.Text))
        //{
        //    studentList = studentList.Where(x => x.projectnumber.Contains(txtProjectNumber.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtMQNsearch.Text))
        //{
        //    studentList = studentList.Where(x => x.MobileNumber.Contains(txtMQNsearch.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlElecDist.SelectedItem.Text))
        //{
        //    if (ddlElecDist.SelectedItem.Text != "Select Discom")
        //        studentList = studentList.Where(x => x.Discom.Contains(ddlElecDist.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddldivision.SelectedItem.Text))
        //{
        //    if (ddldivision.SelectedItem.Text != "Select Division")
        //        studentList = studentList.Where(x => x.Divison.Contains(ddldivision.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlSubDivision.SelectedItem.Text))
        //{
        //    if (ddlSubDivision.SelectedItem.Text != "Sub Division")
        //        studentList = studentList.Where(x => x.SubDivision.Contains(ddlSubDivision.SelectedItem.Text)).ToList();

        //}
        ////if (!string.IsNullOrEmpty(txtKw.Text))
        ////{
        ////    studentList = studentList.Where(x => x.Kw.Contains(txtKw.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlProjectStatus.SelectedItem.Text))
        ////{
        ////    if (ddlProjectStatus.SelectedItem.Text != "Project Status")
        ////        studentList = studentList.Where(x => x.ProjectStatus.Contains(ddlProjectStatus.SelectedItem.Text)).ToList();

        ////}

        ////if (!string.IsNullOrEmpty(ddlTeam.SelectedItem.Text))
        ////{
        ////    if (ddlTeam.SelectedItem.Text != "Select Team")
        ////        studentList = studentList.Where(x => x.team.Contains(ddlTeam.SelectedItem.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlStreetState.SelectedItem.Text))
        ////{
        ////    if (ddlStreetState.SelectedItem.Text != "State")
        ////        studentList = studentList.Where(x => x.state.Contains(ddlStreetState.SelectedItem.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddltaluka.Text))
        ////{
        ////    studentList = studentList.Where(x => x.Taluka.Contains(ddltaluka.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlCity.Text))
        ////{
        ////    studentList = studentList.Where(x => x.City.Contains(ddlCity.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(txtPanelNo.Text))
        ////{
        ////    studentList = studentList.Where(x => x.pannelmodel.Contains(txtPanelNo.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(txtInvertorNumber.Text))
        ////{
        ////    studentList = studentList.Where(x => x.pannelmodel.Contains(txtInvertorNumber.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(txtCap.Text))
        ////{
        ////    studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(txtCapTo.Text))
        ////{
        ////    // studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlSearchEmployee.SelectedItem.Text))
        ////{
        ////    if (ddlSearchEmployee.SelectedItem.Text != "Employee")
        ////        studentList = studentList.Where(x => x.Employeename.Contains(ddlSearchEmployee.SelectedItem.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlApplicationStatus.SelectedItem.Text))
        ////{
        ////    if (ddlApplicationStatus.SelectedItem.Text != "Application Status")
        ////        studentList = studentList.Where(x => x.ApplicationStatus.Contains(ddlApplicationStatus.SelectedItem.Text)).ToList();

        ////}
        ////if (!string.IsNullOrEmpty(ddlSearchDate.SelectedItem.Text) && ddlSearchDate.SelectedItem.Text != "Project Opened")
        ////{
        ////    if (ddlSearchDate.SelectedValue != "1" && (!string.IsNullOrEmpty(txtStartDate.Text)) && (!string.IsNullOrEmpty(txtEndDate.Text)))
        ////    {
        ////        DateTime startDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
        ////        DateTime endDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);

        ////        string selectedValue = ddlSearchDate.SelectedValue;
        ////        if (selectedValue == "3")

        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "4")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteAccepted)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "5")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.DepositReceived)).ToList();
        ////            studentList = studentList.Where(x => Convert.ToDateTime(x.DepositReceived).Date >= startDate.Date && Convert.ToDateTime(x.DepositReceived).Date <= endDate.Date).ToList();
        ////        }
        ////        if (selectedValue == "6")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
        ////        }
        ////        if (selectedValue == "7")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "8")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.followupdate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "9")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallCompleted)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        //if (selectedValue == "11")
        ////        //    studentList = studentList.Where(x => Convert.ToDateTime(x.QuoteSent).Date >= startDate.Date && Convert.ToDateTime(x.QuoteSent).Date < endDate.Date).ToList();
        ////    }
        ////    else if (string.IsNullOrEmpty(txtStartDate.Text))
        ////    {

        ////        //DateTime startDate = Convert.ToDateTime(txtStartDate.Text);
        ////        DateTime endDate = Convert.ToDateTime(txtEndDate.Text);
        ////        string selectedValue = ddlSearchDate.SelectedValue;
        ////        if (selectedValue == "3")

        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "4")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteAccepted)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "5")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.DepositReceived)).ToList();
        ////            studentList = studentList.Where(x => Convert.ToDateTime(x.DepositReceived).Date <= endDate.Date).ToList();
        ////        }
        ////        if (selectedValue == "6")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
        ////        }
        ////        if (selectedValue == "7")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "8")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.followupdate)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }
        ////        if (selectedValue == "9")
        ////        {
        ////            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallCompleted)).ToList();
        ////            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        ////        }

        ////    }
        ////}

        //ddlSelectRecords.Visible = false;
        //GridView1.PagerSettings.Visible = false;
        //if (studentList.Count == 0)
        //{
        //    SetNoRecords();
        //    //PanNoRecord.Visible = true;
        //    PanGrid.Visible = false;
        //    divnopage.Visible = false;
        //}
        //else
        //{
        //    GridView1.DataSource = studentList;
        //    GridView1.DataBind();
        //    PanNoRecord.Visible = false;
        //    divnopage.Visible = true;
        //    if (studentList.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
        //    {
        //        if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < studentList.Count)
        //        {
        //            //=label Hide
        //            divnopage.Visible = false;
        //        }
        //        else
        //        {
        //            divnopage.Visible = true;
        //            int iTotalRecords = studentList.Count;
        //            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //            if (iEndRecord > iTotalRecords)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            if (iStartsRecods == 0)
        //            {
        //                iStartsRecods = 1;
        //            }
        //            if (iEndRecord == 0)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //        }
        //    }
        //    else
        //    {
        //        if (ddlSelectRecords.SelectedValue == "All")
        //        {
        //            divnopage.Visible = true;
        //            ltrPage.Text = "Showing " + studentList.Count + " entries";
        //        }
        //    }
        //    ltrPage.Text = "Showing " + studentList.Count + " entries";
        //}

    }

    //protected void btnUpdatePreInst(object sender, EventArgs e)
    //{
    //    BindGrid(0);

    //}


    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    txtClient.Text = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    txtSearchPostCodeFrom.Text = string.Empty;
    //    txtSearchPostCodeTo.Text = string.Empty;
    //    ddlSearchStatus.SelectedValue = "";
    //    ddlProjectTypeID.SelectedValue = "";
    //    txtPModel.Text = string.Empty;
    //    txtIModel.Text = string.Empty;
    //    txtSysCapacityFrom.Text = string.Empty;
    //    txtSysCapacityTo.Text = string.Empty;
    //    ddlFinanceOption.SelectedValue = string.Empty;
    //    ddlElecDistAppDate.SelectedValue = string.Empty;
    //    ddlInstallBookingDate.SelectedValue = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtpurchaseNo.Text = string.Empty;

    //    BindGrid(0);
    //}

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //Response.Write(userid);
        //Response.End();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance"))
        {
            employeeId = "";
        }

        DataTable dt = ClstblProjects.tblProjects_InstallBookingTracker1(employeeId, txtClient.Text, txtProjectNumber1.Text.Trim(), txtMQNsearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCodeFrom.Text.Trim(), txtSearchPostCodeTo.Text.Trim(), ddlSearchStatus.SelectedValue, ddlProjectTypeID.SelectedValue, txtPModel.Text, txtIModel.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlFinanceOption.SelectedValue, ddlElecDistAppDate.SelectedValue, ddldatetype.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), txtpurchaseNo.Text, chkishistoric.Checked.ToString());
        //Response.Write(dt.Rows.Count);
        //Response.End();

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Projects" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 4, 5, 28, 21, 23, 8, 10, 11, 12, 13, 25, 29, 16, 19, 20, 30, 18, 26, 31, 24, 32, 33, 22, 34, 35, 36, 37 };
            string[] arrHeader = { "Project", "P/CD", "Quote", "Contact", "Mobile", "Project Notes", "Project#", "Man#", "System Cap", "System Details", "House", "Install Book Date", "State", "Status", "Employee Name", "Dep Rec", "Price", "Roof Type", "Panels", "Inverter", "Team", "FinanceWith", "Source", "Sub Source", "First Dep. Date", "NextFollowupDate", "Description" };
            //only change file extension to .xls for excel file
            //Response.Write(ColList[2]);
            //Response.End();
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }



    protected void ddlElecDist_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                      this.GetType(),
        //                                      "MyAction",
        //                                      "doMyAction();",
        //                                      true);
        if (ddlElecDist.SelectedValue != string.Empty)
        {
            ddldivision.Items.Clear();
            DataTable dt = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
            ddldivision.DataSource = dt;
            ddldivision.DataValueField = "Id";
            ddldivision.DataMember = "DivisionName";
            ddldivision.DataTextField = "DivisionName";
            ddldivision.DataBind();
        }
        else
        {

        }
    }


    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ProjectID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "paid")
        {
            hndinvoicepaymentid.Value = ProjectID;
            ModalPopupExtender2.Show();
        }
        BindGrid(0);
    }
    protected void lnkjob_Click(object sender, EventArgs e)
    {
        // BindGrid(0);
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        HiddenField hdnclosepopup = projectpreinst.FindControl("hdnclosepopup") as HiddenField;
        hdn_custpopup.Value = hdncust.Value;
        hdnclosepopup.Value = "1";
        if (hdnProjectID.Value != string.Empty)
        {

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
            hdnInvoiceProjectid.Value = hdnProjectID.Value;

            //hdnclosepopup.Value="0";
            ModalPopupExtender1.Show();
            openModal = "true";
            div_popup.Visible = true;
            //RangeValidator dateVal = (RangeValidator)RequestFormView.FindControl("RangeValidator1");
            //if (dateVal != null)
            //{

            //    dateVal.MinimumValue = Common.SessionDate().ToString(pattern);
            //    dateVal.MaximumValue = Common.SessionDate().AddYears(10).ToString(pattern);

            //}

            projectpreinst.GetPreInstClickByProject(hdnProjectID.Value);


        }
        else
        {
            div_popup.Visible = false;
            ModalPopupExtender1.Hide();
            openModal = "false";
        }
        //BindGrid(0);
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtClient.Text = string.Empty;
        txtProjectNumber1.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        ListItem item = new ListItem();
        item.Value = "0";
        item.Text = "Select";
        ddldivision.Items.Clear();
        ddldivision.Items.Add(item);

        ddlSearchState.SelectedValue = "";
        ddlElecDist.SelectedValue = "0";
        ddlSubDivision.SelectedValue = "0";
        ddlchnlpart.SelectedValue = "0";
        ddlSelectitems.SelectedValue = "0";

        ddltaluka.Text = string.Empty;
        txtStartDate1.Text = string.Empty;
        txtEndDate1.Text = string.Empty;
        ddltaluka.Text = string.Empty;



        txtSearchPostCodeFrom.Text = string.Empty;
        txtSearchPostCodeTo.Text = string.Empty;
        ddlSearchStatus.SelectedValue = "";
        ddlProjectTypeID.SelectedValue = "";
        txtPModel.Text = string.Empty;
        txtIModel.Text = string.Empty;
        txtSysCapacityFrom.Text = string.Empty;
        txtSysCapacityTo.Text = string.Empty;
        ddlFinanceOption.SelectedValue = string.Empty;
        ddlElecDistAppDate.SelectedValue = string.Empty;
        ddldatetype.SelectedValue = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtpurchaseNo.Text = string.Empty;
        openModal = "false";
        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        divPaiddate.Visible = false;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    public class FilterData
    {
        public string CustomerID { get; set; }
        public string ProjectId { get; set; }
        public string customername { get; set; }
        public string projectnumber { get; set; }
        public string ConsumerNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Discom { get; set; }
        public string Divison { get; set; }
        public string SubDivision { get; set; }
        public string CurrentStatus { get; set; }
        public string Kw { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string City { get; set; }
        public string state { get; set; }
        public string pannelmodel { get; set; }
        public string nvertormodel { get; set; }
        public string capacity { get; set; }
        public string Employeename { get; set; }
        public string team { get; set; }
        public string FirstQuoteDate { get; set; }
        public string ApplicationStatus { get; set; }
        public string ProjectStatus { get; set; }
        public string BookingDate { get; set; }
        public string Project { get; set; }
        public string inverterDetails { get; set; }
        public string ProjectNotes { get; set; }
        public string InstallerNotes { get; set; }
        public string RooftopArea { get; set; }
        public string stocksize { get; set; }
        public string panelDetail { get; set; }
        public string QuoteSent { get; set; }
        public string QuoteAccepted { get; set; }
        public string DepositReceived { get; set; }
        public string ActiveDate { get; set; }
        public string InstallCompleted { get; set; }
        public string InstallBookingDate { get; set; }
        public string followupdate { get; set; }

    }


    protected void ibtnUpdateInstallerData_Click(object sender, EventArgs e)
    {
        bool result = ClsProjectSale.tblProjects_UpdatePreInstByInstallerBooingTracker(hndinvoicepaymentid.Value, txtInstallBookingDate.Text, txtInstallerNotes.Text, ddlStockAllocationStore.SelectedValue, ddlInstaller.SelectedValue);
        if (result)
        {
            ModalPopupExtender2.Hide();
        }
        if (!string.IsNullOrEmpty(txtInstallBookingDate.Text))
        {
            bool suc = ClsProjectSale.tblProjects_UpdateProjectApplicationID(hndinvoicepaymentid.Value, "Feasibility Report");
            bool dtStatus = ClsProjectSale.tblProjects_UpdateProjectStatusID(hndinvoicepaymentid.Value, "11");

        }
        BindGrid(0);
    }
}