﻿<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="PickList.aspx.cs" Inherits="admin_adminfiles_company_PickList" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            <%--$('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });--%>
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>
    <style>
        .nowrap {
            white-space: normal!important;
        }

        .tooltip {
            z-index: 999999;
        }
        
    </style>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
            }
        }

        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-add-user icon"></i>Picklist</h5>
        <div id="hbreadcrumb">
            <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClientClick="ActiveLoader()"
                CssClass="btn-shadow btn btn-info btngray" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i> Update Transport</asp:LinkButton>
        </div>
        <%--<div id="hbreadcrumb">
            <a href="#" class="btn btn-info btngray"><i class="fa fa-upload"></i>Upload Leads</a>
            <div id="tdExport" class="pull-right" runat="server">
            </div>
        </div>--%>
    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.custom-file-input').on('change', function (e) {
                var fileName = e.target.files[0].name;
                //  var fileName = document.getElementsByClassName("fileupload").files[0].name;
                //alert(fileName);
                $(this).next('.form-control-file').addClass("selected").html(fileName);
            });

            
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {

        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });


            //$("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $(".myvalemployee").select2({
                allowclear: true,
                //minimumResultsForSearch: -1
            });

        }

    </script>
    <style>
        .SelectionHeight .select2-selection.select2-selection--multiple{height: 100px;}
        .select2-container .select2-selection--multiple .select2-selection__rendered{
            padding-top:7px;
        }
    </style>

    <div class="page-body padtopzero formGrid">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panelmessesgarea padbtmzero">
                <div class="messesgarea">
                    <div class="alert alert-danger" id="PanEmpty" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Lead Source. </strong>
                    </div>
                </div>
            </div>

            <div class="searchfinal searchFilterSection">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                    <tr>
                                        <td class="">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtProjectNo" runat="server" Placeholder="Project No" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtProjectNo"
                                                            WatermarkText="Project No" />
                                                    </div>

                                                    <div class="input-group col-sm-2">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtCustomer" runat="server" Placeholder="Customer" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtCustomer"
                                                            WatermarkText="Customer" />
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtCity" runat="server" Placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtCity"
                                                            WatermarkText="City" />
                                                    </div>
                                                    
                                                    <div class="input-group col-sm-2">
                                                       <asp:DropDownList ID="ddlSerachVehicleType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0"
                                                            CssClass="myval" >
                                                            <asp:ListItem Value="">Vehicle Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Booking Date</asp:ListItem>
                                                            <asp:ListItem Value="2">Delivery Date</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group" style="width: 150px; padding-top: 3px">
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" CssClass="btn btn-success btn-xs Excel" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PanTotal" runat="server" CssClass="hpanel marbtm15" Visible="false">
            <div class="widget-body shadownone brdrgray">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header" style="padding-left: 18px">
                    <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" class="evenRowTable">
                                    <tr>
                                        <%--<asp:Repeater ID="rptTotal" runat="server">
                                            <ItemTemplate>
                                                <td class="paddingleftright10" style="font-size: medium">

                                                    <asp:Literal ID="lblVehicle" runat="server" Text='<%#Eval("VehicleType")%>'></asp:Literal>
                                                    :&nbsp;
                                                            <asp:Literal ID="lblTotalTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Literal>
                                                </td>
                                                <td></td>

                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            Vehicle Type :&nbsp;
                                            <asp:Literal ID="lblVehicle" runat="server"></asp:Literal>
                                            <%--:&nbsp;--%>

                                        </td>
                                        <td></td>
                                        <td class="paddingleftright10" style="font-size: medium">
                                            Total Trip :&nbsp;
                                            <asp:Literal ID="lblTotalTrip" runat="server" ></asp:Literal>
                                            <%--:&nbsp;--%>

                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>

        <div class="fullWidthTable searchfinal customerTable searchbar mainGridTable main-card mb-3 card">
             
            <div class="finalgrid">
                <div class="padtopzero padrightzero">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server" class="wid100">
                            <div class="table-responsive xscroll leadtablebox">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo  text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand" OnDataBound="GridView1_DataBound"
                                    AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowCreated="GridView1_RowCreated">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>

                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                        <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/arrowright.png" />
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="ProjectNumber">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProjectNumber" runat="server">
                                                    
                                                    <%#Eval("ProjectNumber")%></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server"
                                                    data-placement="top" data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip">
                                                    
                                                    <%#Eval("Customer")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallAddress">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallAddress" runat="server"
                                                    data-placement="top" data-original-title='<%#Eval("InstallAddress")%>' data-toggle="tooltip">
                                                    
                                                    <%#Eval("InstallAddress")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallerName" runat="server">
                                                    
                                                    <%#Eval("InstallerName")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Vehicle Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vehicle Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicleType" runat="server">
                                                    
                                                    <%#Eval("VehicleType")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Booking Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallBookingDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallBookingDate" runat="server">
                                                    
                                                    <%#Eval("FormattedInstallBookingDate")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnPicklist" CommandName="GeneratePickList" CommandArgument='<%#Eval("ProjectID")%>' CssClass="btn btn-info btngray btn-xs"
                                                    CausesValidation="false" runat="server" data-original-title="Ganerate New Picklist" data-toggle="tooltip" data-placement="top">
                                                               <i class="fa fa-list"></i> New Picklist</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                                <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr class="GridviewScrollItem">
                                                                        <td><b>Rego No</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label7" runat="server">
                                                                                 <%#Eval("VehicleRegoNo").ToString() == "" ? "-" : Eval("VehicleRegoNo")%>
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <b>Driver</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label8" runat="server">
                                                                                <%#Eval("Driver").ToString() == "" ? "-" : Eval("Driver")%>
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <b>Delivery Date</b>
                                                                        </td>
                                                                        <td>
                                                                           <asp:Label ID="Label1" runat="server">
                                                                                <%#Eval("FormattedDeliveryDate").ToString() == "" ? "-" : Eval("FormattedDeliveryDate")%>
                                                                            </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <b>Notes</b>
                                                                        </td>
                                                                        <td>
                                                                           <asp:Label ID="lblNotes" runat="server">
                                                                                <%#Eval("Notes").ToString() == "" ? "-" : Eval("Notes")%>
                                                                            </asp:Label>
                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                        </td>

                                    </tr>
                                </table>
                            </div>
                            <%-- <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td class="showEntryBtmBox">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Display</asp:ListItem>
                                                </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                        <div class="pagination paginationGrid">
                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>--%>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function pageLoad(sender, args) { if (!args.get_isPartialLoad()) { $addHandler(document, "keydown", onKeyDown); } }
        <%--function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                $find("<%=MPECustomerName.ClientID%>").hide();
                $find("<%=ModalPopupExtender1.ClientID%>").hide();
            }
        }--%>
    </script>
    <%--   <script type="text/javascript">
        $(document).ready(function () {
            gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });
        }
    </script>--%>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=.] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView2] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=DataList1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=DataList1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <cc1:ModalPopupExtender ID="ModalPopupExtenderTransport" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
        CancelControlID="lnkcancel">
    </cc1:ModalPopupExtender>
    <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup companyPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line printorder "></div>
                <div class="modal-header printorder">
                    <div class="modalHead">
                        <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-news-paper popupIcon"></i>Update Transport</h4>
                        <div>
                            <asp:LinkButton ID="LinkButton1" runat="server" type="button" class="btn redButton btncancelIcon"
                                data-dismiss="myModal"><i class="fa fa-times"></i> Close
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="modal-body paddnone" runat="server" id="divdetail">
                    <div class="formainline formGrid">
                        <div class="">
                            <span class="name disblock">
                                <label class="control-label">
                                </label>
                            </span>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row spicaldivin " id="div27" runat="server">
                                        <div class="col-sm-12">
                                            <div class="row marginbtm15">
                                                
                                                <div class="col-md-3">
                                                    <asp:Label ID="Label37" runat="server" class="control-label">Vehicle Type</asp:Label>
                                                    <div class="marginbtm15">
                                                        <asp:DropDownList ID="ddlVehicleType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0"
                                                            CssClass="myval" OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged" AutoPostBack="true" >
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <asp:Label ID="lblRegoNo" runat="server" class="control-label">Rego No</asp:Label>
                                                    <div class="marginbtm15">
                                                        <asp:TextBox ID="txtNumber" runat="server" ReadOnly="true" CssClass="form-control"
                                                            placeholder="Registration Number"></asp:TextBox>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <asp:Label ID="lblDriverName" runat="server" class="control-label">Driver Name</asp:Label>
                                                    <div class="marginbtm15">
                                                        <asp:TextBox ID="txtDriverName" runat="server" CssClass="form-control"
                                                            placeholder="Driver Name"></asp:TextBox>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <asp:Label ID="lblDeliveryDate" runat="server" class="control-label">Delivery Date</asp:Label>
                                                    <div class="marginbtm15">
                                                        <div class="col- input-group date datetimepicker1">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtDeliveryDate" placeholder="Delovery Date" runat="server" class="form-control"></asp:TextBox>
                                                             <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="" ForeColor="></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row" style="padding-top:15px;">
                                <div class="col-md-12">
                                    <div class="row spicaldivin " id="div1" runat="server">
                                        <div class="col-sm-12">
                                            <div class="row marginbtm15">
                                                
                                                <div class="col-md-12">
                                                    <asp:Label ID="lblProjectNo" runat="server" class="control-label" >Project No</asp:Label>
                                                    <div class="marginbtm15">

                                                        <%--<asp:TextBox ID="txtProjectNumber" TextMode="MultiLine" runat="server" Height="100px" CssClass="form-control"
                                                            placeholder="Project Number" ></asp:TextBox>--%>
                                                        
                                                        <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>

                                                        <asp:ListBox ID="lstProjectNo" runat="server" SelectionMode="Multiple" CssClass="myvalemployee "></asp:ListBox>

                                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" ControlToValidate="lstProjectNo"
                                                            Display="Dynamic" ForeColor="Red" ValidationGroup ="Req"></asp:RequiredFieldValidator>--%>
                                                        
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top:15px;">
                                <div class="col-md-12">
                                    <div class="row spicaldivin " id="div2" runat="server">
                                        <div class="col-sm-12">
                                            <div class="row marginbtm15">
                                                <div class="col-md-12">
                                                    <asp:Label ID="lblNotes" runat="server" class="control-label">Notes</asp:Label>
                                                    <div class="marginbtm15">
                                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="form-control"
                                                            placeholder="Notes"></asp:TextBox>

                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="textRight">

                                <asp:Button CssClass="btn largeButton greenBtn redreq btnaddicon" ID="Button3" runat="server"
                                    Text="Update" ValidationGroup="Req" OnClick="Button3_Click" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

</asp:Content>
