﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Async="true"
    MaintainScrollPositionOnPostback="true" Culture="en-GB" UICulture="en-GB"
    CodeFile="company.aspx.cs" Inherits="admin_adminfiles_company_company"
    EnableEventValidation="true" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/controls/companysummary.ascx" TagName="companysummary" TagPrefix="uc1" %>
<%@ Register Src="../../../includes/controls/contacts.ascx" TagName="contacts" TagPrefix="uc2" %>
<%@ Register Src="../../../includes/controls/project.ascx" TagName="project" TagPrefix="uc3" %>
<%@ Register Src="../../../includes/controls/info.ascx" TagName="info" TagPrefix="uc4" %>
<%@ Register Src="../../../includes/controls/conversation.ascx" TagName="conversation" TagPrefix="uc5" %>
<%@ Register Src="../../../includes/controls/documents.ascx" TagName="docc" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <input type="hidden" id="chkaddval" value="0" />
            <input type="hidden" id="chkaddval1" value="0" />

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(endrequesthandler);
                function endrequesthandler(sender, args) {
                    $('input[type=file]').bootstrapFileInput();
                }
                function hideDivfFT(pageid) {
                    $('[id*=divStartDate]').removeClass('input-group');
                    $('[id*=divStartDate]').hide();
                    $('[id*=hdnStartDate]').val("False");
                    $('[id*=hdnEndDate]').val("False");


                }
                function hideDivfTT(pageid) {
                    $('[id*=DivEndDate]').removeClass('input-group');
                    $('[id*=DivEndDate]').hide();
                    $('[id*=hdnSearchSource1]').val("False");

                }
                function hideDivRec(pageid) {
                    $('[id*=ddlSearchRec1]').removeClass('input-group');
                    $('[id*=ddlSearchRec1]').hide();
                    $('[id*=hdnSearchRec1]').val("False");

                }
                function hideDivSearch(pageid) {
                    $('[id*=ddlSearchType1]').removeClass('input-group');
                    $('[id*=ddlSearchType1]').hide();
                    $('[id*=hdnSearchType1]').val("False");

                }

                function hideDivSource(pageid) {
                    $('[id*=ddlSearchSource1]').removeClass('input-group');
                    $('[id*=ddlSearchSource1]').hide();
                    $('[id*=hdnSearchSource1]').val("False");
                }

                function hideDivSubSource(pageid) {
                    $('[id*=ddlSearchSubSource1]').removeClass('input-group');
                    $('[id*=ddlSearchSubSource1]').hide();
                    $('[id*=hdnSearchSubSource1]').val("False");

                }
                function hideSearchStreet(pageid) {
                    $('[id*=txtSearchStreet1]').removeClass('input-group');
                    $('[id*=txtSearchStreet1]').hide();
                    $('[id*=hdnSearchStreet1]').val("False");

                }
                function hideSearchState(pageid) {
                    $('[id*=ddlSearchState1]').removeClass('input-group');
                    $('[id*=ddlSearchState1]').hide();
                    $('[id*=hdnSearchState1]').val("False");

                }
                function hideSerachCity(pageid) {
                    $('[id*=txtSerachCity1]').removeClass('input-group');
                    $('[id*=txtSerachCity1]').hide();
                    $('[id*=hdnSerachCity1]').val("False");

                }
                function hideSearchPostCode(pageid) {
                    $('[id*=txtSearchPostCode]').removeClass('input-group');
                    $('[id*=txtSearchPostCode]').hide();
                    $('[id*=hdnSearchPostCode1]').val("False");

                }

                function Uploadfilecustomer() {
                    var uploadfiles = $('input[type="file"]').get(1);

                    var fileUpload = document.getElementById("<%=FileUpload1.ClientID %>");
                    var fileSize = fileUpload.files[0].size;
                    $('#<%=hdnFileName.ClientID %>').val(fileUpload.files[0].name);
                    $('#<%=hdnFilesize.ClientID %>').val(fileSize);
                    var fromdata = new FormData();
                    fromdata.append(fileUpload.files[0].name, fileUpload.files[0]);
                    var choice = {};
                    choice.url = "HandlerDoc.ashx";
                    choice.type = "POST";
                    choice.data = fromdata;
                    choice.contentType = false;
                    choice.processData = false;
                    choice.success = function (result) {
                    };
                    choice.error = function (err) {
                    };
                    $.ajax(choice);
                }

            </script>
            <script type="text/javascript" language="javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                function EndRequestHandler(sender, args) {
                    if (args.get_error() != undefined) {
                        args.set_errorHandled(true);
                    }
                }
            </script>
            <style>
                input[type=radio]:checked, input[type=radio]:focus {
                    outline: none !important;
                }

                .showhide {
                    display: none !important;
                }

                input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
                    display: inline-block;
                    content: '\f00c';
                    background-color: #f5f8fc;
                    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                    -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
                    border-color: #1d3c7c;
                }

                input[type=radio]:hover ~ .text :before, input[type=radio]:hover ~ label:before {
                    border-color: #1d3c7c;
                }

                input[type=radio]:active ~ .text :before, input[type=radio]:active ~ label :before {
                    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                    -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                }

                input[type=radio] ~ .text, input[type=radio] ~ label {
                    position: relative;
                    z-index: 11;
                    display: inline-block;
                    margin: 0;
                    line-height: 20px;
                    min-height: 18px;
                    min-width: 18px;
                    font-weight: normal;
                }

                    input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                        font-family: fontAwesome;
                        font-weight: bold;
                        font-size: 13px;
                        color: #1d3c7c;
                        content: "\a0";
                        background-color: #fafafa;
                        border: 1px solid #c8c8c8;
                        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
                        border-radius: 0;
                        display: inline-block;
                        text-align: center;
                        vertical-align: middle;
                        height: 18px;
                        line-height: 16px;
                        min-width: 18px;
                        margin-right: 5px;
                        margin-bottom: 1px;
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -o-transition: all 0.3s ease;
                        transition: all 0.3s ease;
                    }

                    input[type=radio] ~ .text:hover:before, input[type=radio] ~ label:hover:before {
                        border-color: #1d3c7c;
                    }

                    input[type=radio] ~ .text:active:before, input[type=radio] ~ label:active:before {
                        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0 1px 3px rgba(0, 0, 0, 0.1);
                    }

                    input[type=radio] ~ .text:before, input[type=radio] ~ label:before {
                        border-radius: 100%;
                        font-size: 10px;
                        font-family: FontAwesome;
                        line-height: 17px;
                        height: 19px;
                        min-width: 19px;
                    }

                input[type=radio]:checked ~ .text:before, input[type=radio]:checked ~ label:before {
                    content: "\f111";
                }
            </style>

            <script>
                $(".verticalMenu .dropDownList .showdate").click(function () {
                    alert(1);

                });
                function printContent() {
                    var PageHTML = document.getElementById('<%= (printimage.ClientID) %>').innerHTML;
                    var html = '<html><head>' +
                        '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                        '</head><body style="background:#ffffff;">' +
                        PageHTML +
                        '</body></html>';
                    var WindowObject = window.open("", "PrintWindow",
                        "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
                    WindowObject.document.writeln(html);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                    document.getElementById('print_link').style.display = 'block';
                }

            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                    $("[id*=rptName] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptName] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptMobile] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptMobile] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptPhone] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptPhone] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptEmail] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptEmail] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptaddress] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptaddress] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                    //gridviewScroll();
                });

                function divexpandcollapse(divname, trname) {
                    alert(divname);
                    debugger;
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/arrowbottom.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/arrowright.png";
                    }
                }
                function doMyAction() {
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptName] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptName] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptMobile] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptMobile] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptPhone] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptPhone] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptEmail] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptEmail] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptaddress] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptaddress] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                }
                function InActiveLoader() {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    console.log("Function Called After");
                }

                function ActiveLoader() {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                    console.log("Function Called Before");
                }
            </script>


            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                function BeginRequestHandler(sender, args) {

                    var fe = document.activeElement;
                    if (fe != null) {
                        focusedElementId = fe.id;
                    } else {
                        focusedElementId = "";
                    }
                }

                prm.add_pageLoaded(pageLoaded);
                prm.add_beginRequest(BeginRequestHandler);

                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.datetimepicker2').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.datetimepicker3').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.redreq').click(function () {
                        console.log("formValidate CAlled");
                        formValidate();
                    });
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptName] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptName] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptMobile] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptMobile] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptPhone] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptPhone] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptEmail] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptEmail] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptaddress] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptaddress] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $('.custom-file-input').on('change', function (e) {
                        var fileName = e.target.files[0].name;
                        //var fileName = document.getElementsByClassName("fileupload").files[0].name;
                        //alert(fileName);
                        $(this).next('.form-control-file').addClass("selected").html(fileName);
                    });

                }
            </script>
            <script type="text/javascript">
                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/arrowbottom.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/arrowright.png";
                        //  divexpandcollapse("", "");
                    }
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalcomp").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                }
            </script>
            <script>
                function formValidate() {
                    var allvalid = true;
                    console.log(Page_Validators);
                    if (typeof (Page_Validators) != "undefined") {
                        console.log("Page_Validators");
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                                allvalid = false;
                                console.log("false");
                                console.log(Page_Validators[i].controltovalidate);
                            }
                            else {
                                console.log("true");
                                console.log(Page_Validators[i].controltovalidate);
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                        console.log(allvalid);
                        if (allvalid) {
                            $('.loading-container').removeClass('loading-inactive');
                            console.log("Function Called Before");
                        }
                    }
                }
                function address() {

                    //=============Street 
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
            //alert($("#<%=txtformbaystreetname.ClientID %>").val())
                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                //alert("Hiiiii")
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                            }
                        }
                    });
                    var mydataval = $("#chkaddval").val();
                    if (mydataval == "1") {
                        return true;
                    }
                    else {
                        return false;
                    }

                }

                function validatestreetAddress(source, args) {
                    var mydataval;
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            if (data.d == true) {
                       <%-- alert($("#<%=txtformbaystreetname.ClientID %>").val())--%>
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                                mydataval = $("#chkaddval").val("1");
                                strretnamevelid(mydataval);
                            }
                            else {
                                //  alert("ELSE");
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                                mydataval = $("#chkaddval").val("0");
                                strretnamevelid(mydataval);
                            }
                        }
                    });


           // alert(mydataval);

            <%--$.each(data, function (index, itemdata) {
                //itemdata.streetname;
                if (<%=streetname%>==itemdata.streetname) {
                    mydataval = "1";
                }
            });--%>

                    if ($("#<%=chkSameasAbove.ClientID %>").val() == "on") {
                        $("#chkaddval").val(mydataval);
                    }
                    function strretnamevelid(mydataval) {
                        if (mydataval == "1") {
                            alert('efd');
                            args.IsValid = true;
                        }
                        else {
                            args.IsValid = false;
                        }
                    }
                }
                function postaladdress() {
                    //=================Street

                    var streetname = $("#<%=txtPostalformbaystreetname.ClientID %>").val();
                    $.ajax({
                        type: "POST",
                        //action:"continue.aspx",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {

                                $("#<%=txtPostalformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');

                                $("#chkaddval1").val("1");
                            }
                            else {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").addClass('errormassage');

                                $("#chkaddval1").val("0");
                            }
                        }
                    });
                    var mydataval1 = $("#chkaddval1").val();

                    if (mydataval1 == "1") {
                        args.IsValid = true;
                    }
                    else {
                        args.IsValid = false;
                    }
                }
                function postalvalidatestreetAddress(source, args) {


                    var streetname = $("#<%=txtPostalformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval1").val("1");
                            }
                            else {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval1").val("0");
                            }
                        }
                    });
                    var mydataval1 = $("#chkaddval1").val();
                    if ('<%=streetname%>') {
                        mydataval1 = 1;
                    }
                    if (mydataval1 == "1") {
                        args.IsValid = true;
                    }
                    else {
                        args.IsValid = false;
                    }
                }
            </script>


            <div class="page-body headertopbox">
                <asp:HiddenField ID="hdnFileName" runat="server" />
                <asp:HiddenField ID="hdnFilesize" runat="server" />

                <asp:HiddenField ID="hdnSearchRec1" runat="server" />
                <asp:HiddenField ID="hdnSearchSource1" runat="server" />
                <asp:HiddenField ID="hdnSearchSubSource1" runat="server" />
                <asp:HiddenField ID="hdnSearchType1" runat="server" />
                <asp:HiddenField ID="hdnSerachCity1" runat="server" />
                <asp:HiddenField ID="hdnSearchStreet1" runat="server" />
                <asp:HiddenField ID="hdnSearchState1" runat="server" />
                <asp:HiddenField ID="hdnSearchPostCode1" runat="server" />
                <asp:HiddenField ID="hdnStartDate" runat="server" />
                <asp:HiddenField ID="hdnEndDate" runat="server" />
                <asp:HiddenField ID="hdnaddressverification1" runat="server" />



                <h5 class="row-title"><i class="pe-7s-users icon"></i>Manage Customer &nbsp;
               
                    <b>
                        <asp:Literal runat="server" ID="ltcompname"></asp:Literal></b>
                </h5>
                <div id="hbreadcrumb">
                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClientClick="ActiveLoader()" OnClick="lnkAdd_Click" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" OnClientClick="ActiveLoader()" CausesValidation="false" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    <asp:LinkButton ID="lnkclose" runat="server" CssClass="btn-shadow btn btn-info btngray" CausesValidation="false" Visible="false"
                        OnClick="lnkclose_Click"><i class="fa fa-backward"></i> Close</asp:LinkButton>
                </div>
            </div>
            <div class="page-body padtopzero customerPage">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="false">
                        <div class="animate-panel">
                            <div class="row">

                                <%--  <div class="col-md-12">
                                                 
                                                </div>--%>
                                <div class="col-md-6 leftSidePage">
                                    <div class="animate-panel padtopzero">
                                        <div class="header bordered-blue">
                                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                            <h4 id="Haddcomp" runat="server">
                                                <asp:Label runat="server" ID="lbladdcompany" Text="Add New Customer"></asp:Label>
                                            </h4>
                                        </div>
                                        <div class="with-header companyddform addform">


                                            <div class="formGrid companyFormBody">
                                                <div class="row">
                                                    <div class="form-group col-sm-12" id="divD2DEmployee" visible="false" runat="server">
                                                        <asp:Label ID="Label33" runat="server">Customer</asp:Label>
                                                        <div class="drpValidate">
                                                            <asp:DropDownList ID="ddlD2DEmployee" runat="server" AppendDataBoundItems="true" CssClass="myvalcomp"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlD2DEmployee_SelectedIndexChanged" aria-controls="DataTables_Table_0">
                                                                <asp:ListItem Value="">Employee</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="ddlD2DEmployee" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="row dispnone">
                                                        <div class="col-md-6" visible="false" id="divD2DAppDate" runat="server">
                                                            <asp:Label ID="Label23" runat="server" class="control-label">Appointment Date</asp:Label>
                                                            <div>
                                                                <div class="input-group date datetimepicker1">
                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    <asp:TextBox ID="txtD2DAppDate" runat="server" class="form-control" placeholder="Start Date"> </asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class=" col-md-6" id="divD2DAppTime" runat="server">
                                                            <asp:Label ID="Label1" runat="server" class="control-label">Appointment Time</asp:Label>
                                                            <div class="drpValidate">
                                                                <asp:DropDownList ID="ddlAppTime" runat="server" AppendDataBoundItems="true"
                                                                    AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                    <asp:ListItem Value="">Time</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="" CssClass="" InitialValue="Unit Type"
                                                            ControlToValidate="ddlAppTime" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-12 spicaldivin" id="divMrMs" runat="server">

                                                        <div class="row">
                                                            <div class="col-md-4" style="display: none">
                                                                <span class="mrdiv">
                                                                    <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                        placeholder="Salutation"></asp:TextBox>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label class="control-label">First Name</label>
                                                                <span class="fistname">
                                                                    <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                        placeholder="First Name"></asp:TextBox>
                                                                    <%--AutoPostBack="true" OnTextChanged="txtContFirst_TextChanged"--%>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="txtContFirst"
                                                                        Display="Dynamic" SetFocusOnError="True" ValidationGroup="RoleReqFields"></asp:RequiredFieldValidator>

                                                                </span>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label">Middle Name</label>
                                                                <span class="middlename">

                                                                    <asp:TextBox ID="txtContMiddle" runat="server" MaxLength="40" CssClass="form-control"
                                                                        placeholder="Middle Name"></asp:TextBox>
                                                                    <%--OnTextChanged="txtContLast_TextChanged" AutoPostBack="true"--%>
                                                                         
                                                                    
                                                                </span>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <label class="control-label">Last Name</label>
                                                                <span class="lastname">

                                                                    <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                        placeholder="Last Name"></asp:TextBox>
                                                                    <%--OnTextChanged="txtContLast_TextChanged" AutoPostBack="true"--%>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" ControlToValidate="txtContLast"
                                                                        Display="Dynamic" SetFocusOnError="True" ValidationGroup="RoleReqFields"></asp:RequiredFieldValidator>

                                                                </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-12">
                                                        <asp:Label ID="Label22" runat="server" class="control-label">Customer</asp:Label>
                                                        <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <asp:Label ID="Label3" runat="server" class="control-label ">Mobile</asp:Label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>
                                                            <asp:TextBox ID="txtContMobile" runat="server" OnTextChanged="txtContMobile_TextChanged" AutoPostBack="true" MaxLength="10" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile" SetFocusOnError="true" Display="Dynamic"
                                                            ValidationGroup="RoleReqFields" ErrorMessage="Please enter valid mobile number" ForeColor="Red"
                                                            ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" ControlToValidate="txtContMobile"
                                                            Display="Dynamic" SetFocusOnError="True" ValidationGroup="RoleReqFields"></asp:RequiredFieldValidator>

                                                    </div>
                                                    <div class="form-group col-sm-6" id="div1" runat="server">
                                                        <asp:Label ID="Label5" runat="server" class="control-label">Contact No</asp:Label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                            <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                            ValidationGroup="RoleReqFields" Display="Dynamic" ErrorMessage="Please enter valid number "
                                                            ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>

                                                    </div>
                                                    <div class="form-group col-sm-6" id="divEmail" runat="server">
                                                        <asp:Label ID="Label4" runat="server" class="control-label">Email</asp:Label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                                            <asp:TextBox ID="txtContEmail" runat="server" MaxLength="200" OnTextChanged="txtContEmail_TextChanged" AutoPostBack="true" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                            ValidationGroup="RoleReqFields" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ForeColor="Red"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                                    </div>

                                                    <div class="form-group col-sm-6" id="div2" runat="server">
                                                        <asp:Label ID="Label6" runat="server" class="control-label">Alt Contact No</asp:Label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                            <asp:TextBox ID="txtCustAltPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCustAltPhone"
                                                            ValidationGroup="RoleReqFields" Display="Dynamic" ErrorMessage="Please enter valid number "
                                                            ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>

                                                    </div>
                                                    <div class="form-group col-sm-6" id="divCustType" runat="server">
                                                        <asp:Label ID="lblLastName" runat="server" class="control-label">Type</asp:Label>
                                                        <asp:DropDownList ID="ddlCustTypeID" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                            <%--    <asp:ListItem Value="" Text="Select"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="ddlCustTypeID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="form-group col-sm-6" id="div4" runat="server">
                                                        <asp:Label ID="Label7" runat="server" class="control-label">Source</asp:Label>

                                                        <asp:DropDownList ID="ddlCustSourceID" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCustSourceID_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                            <%--  <asp:ListItem Value="" Text="Select"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="ddlCustSourceID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>

                                                        <div class="col-sm-12" style="margin-top: 10px">
                                                            <asp:DropDownList ID="ddlCustSourceSubID" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <asp:ListItem Value="" Text="Select Sub Source"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-6 radioButon spicaldivin" id="div3" runat="server">
                                                        <label class="control-label">Solar Type </label>


                                                        <div class="radio radio-info radio-inline" style="margin-bottom: 7px; padding-left: 0px;">
                                                            <label for="<%=rblResCom1.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                <span class="text">Res&nbsp;</span>
                                                            </label>
                                                            <label for="<%=rblResCom2.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                <span class="text">Com&nbsp;</span>
                                                            </label>
                                                        </div>
                                                        <%--<div class="radio i-checks">
                                                                                                <asp:RadioButtonList ID="rblResCom" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Value="1" Selected="True">Res&nbsp;</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Com</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                    </div>--%>
                                                        <div class="clear"></div>
                                                    </div>

                                                    <div class="form-group col-sm-12" id="div5" runat="server">
                                                        <asp:Label ID="Label8" runat="server" class="control-label">Notes</asp:Label>
                                                        <asp:TextBox ID="txtCustNotes" runat="server" Width="" TextMode="MultiLine" Columns="4" Rows="4" class="form-control modaltextbox notestextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="graybgarea col-md-12" runat="server" id="divformbayaddress" visible="false">
                                    <div class="form-group spicaldivin textareabox" style="margin-bottom: 15px;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="padding-right: 4px;">
                                                    <asp:CheckBox ID="chkformbayid" runat="server" Checked="false" Class="i-checks" OnCheckedChanged="chkformbayid_CheckedChanged"
                                                        AutoPostBack="true" BorderStyle="Solid" />
                                                    <label for="<%=chkformbayid.ClientID %>"><span></span></label>
                                                </td>
                                                <td>Is Frombay Address </td>
                                            </tr>
                                        </table>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 rightSidePage">
                                    <div class="animate-panel padtopzero">
                                        <div class="header bordered-blue">
                                            <h4 id="Haddcompadd" runat="server">
                                                <asp:Label runat="server" ID="lblcompanyaddress" Text="Add Customer Address"></asp:Label></h4>
                                        </div>
                                        <div class="with-header companyddform  addform">

                                            <div class="formGrid companyFormBody">
                                                <div id="Div6" class="form-group spicaldivin streatfield" visible="false" runat="server" style="display: none">
                                                    <span class="name">
                                                        <label class="control-label">Street Address Line<span class="symbol required"></span> </label>
                                                    </span>
                                                    <span>
                                                        <asp:HiddenField ID="hndaddress" runat="server" />
                                                        <asp:TextBox ID="txtstreetaddressline" runat="server" MaxLength="50" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtstreetaddressline_TextChanged"></asp:TextBox>

                                                        <%-- <asp:RequiredFieldValidator ID="rfvstreetaddressline" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                        ControlToValidate="txtstreetaddressline" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        <%-- <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>--%>
                                                    </span>
                                                    <asp:Label ID="lblexistame" runat="server" Visible="false"></asp:Label>
                                                    <asp:HiddenField ID="hndstreetno" runat="server" />
                                                    <asp:HiddenField ID="hndstreetname" runat="server" />
                                                    <asp:HiddenField ID="hndstreettype" runat="server" />
                                                    <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                    <asp:HiddenField ID="hndunittype" runat="server" />
                                                    <asp:HiddenField ID="hndunitno" runat="server" />
                                                    <div id="validaddressid" style="display: none">
                                                        <img src="../../../images/check.png" alt="check">Address is valid.
                                                   
                                                    </div>
                                                    <div id="invalidaddressid" style="display: none">
                                                        <img src="../../../images/x.png" alt="cross">
                                                        Address is invalid.
                                                   
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group" id="div7" runat="server" style="display: none">
                                                    <asp:Label ID="Label9" runat="server" class="col-md-12">Street Address</asp:Label>
                                                    <div class="col-sm-12">
                                                        <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <%--<div class="row">
                                                <div class="col-md-6 marginbtm15" visible="false" id="divUnitno" runat="server">
                                                    <asp:Label ID="Label10" runat="server">Unit No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" name="address" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged" class="form-control modaltextbox" ></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-6" id="divunittype" runat="server">
                                                    <asp:Label ID="Label11" runat="server">Unit Type</asp:Label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Required" CssClass="" InitialValue="Unit Type"
                                                            ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>--%>
                                                <div class="row">
                                                    <div class="form-group col-md-6" visible="false" id="divStreetno" runat="server">
                                                        <asp:Label ID="Label12" runat="server">Address 1</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                            <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <%-- <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtformbayStreetNo" ErrorMessage="Value must be a numeric number" ForeColor="Red" />--%>
                                                        </div>
                                                    </div>


                                                    <div class="form-group spicaldivin col-md-6 streatfield" visible="false" id="divstname" runat="server">
                                                        <asp:Label ID="Label2" CssClass="marbtmzero" runat="server">Address 2</asp:Label>

                                                        <div class="autocompletedropdown">
                                                            <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <%--AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"--%>
                                                            <%--<asp:DropDownList ID="ddlformbaystreetname" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                                <asp:ListItem Value="">Street Name</asp:ListItem>
                                                                            </asp:DropDownList>--%>


                                                            <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>



                                                            <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                                ClientValidationFunction="validatestreetAddress"></asp:CustomValidator>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <div id="Divvalidstreetname" style="display: none">
                                                                <%--<img src="../../../images/check.png" alt="check">--%>
                                                                <i class="fa fa-check"></i>Address is valid.
                                                           
                                                            </div>
                                                            <div id="DivInvalidstreetname" style="display: none">
                                                                <%--<img src="../../../images/x.png" alt="cross">--%>
                                                                <i class="fa fa-close"></i>Address is invalid.
                                                           
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="form-group col-md-6" visible="false" id="divNearby" runat="server" style="display: none">
                                                        <asp:Label ID="Label10" runat="server">Near By</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtformbayNearby" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                            <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbayNearby" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>

                                                     <div class="form-group col-md-4" id="div18" runat="server">
                                                        <asp:Label ID="Label40" runat="server">State</asp:Label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlStreetState" runat="server" aria-controls="DataTables_Table_0" AppendDataBoundItems="true" CssClass="myvalproject" AutoPostBack="true">
                                                                <asp:ListItem Value="0">Select State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4" visible="false" id="divDist" runat="server">
                                                        <asp:Label ID="Label14" runat="server">District</asp:Label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlDistrict" runat="server" aria-controls="DataTables_Table_0" AppendDataBoundItems="true" CssClass="myvalproject" AutoPostBack="true">
                                                                <asp:ListItem Value="0">Select District</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4" id="Div24" runat="server">
                                                        <asp:Label ID="Label35" runat="server">Taluka</asp:Label>
                                                        <div>
                                                            <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox" OnTextChanged="ddltaluka_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                    <div class="form-group col-md-4" visible="false" id="divsttype" runat="server" style="display: none">
                                                        <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                        <div id="Div8" class="drpValidate" runat="server">
                                                            <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged"--%>
                                                                <asp:ListItem Value="">Street Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-4" id="seq" runat="server">
                                                        <asp:Label ID="lblMobile" runat="server">City</asp:Label>
                                                        <div>
                                                            <asp:TextBox runat="server" ID="ddlCity" Enabled="true" class="form-control modaltextbox" AutoPostBack="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="ddlCity" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllCity"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />


                                                    <div class="form-group col-md-4" id="Div30" runat="server" style="display: none">
                                                        <asp:Label ID="Label62" runat="server">Area</asp:Label>
                                                        <div class="autocompletedropdown">
                                                            <asp:DropDownList ID="ddlArea" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomp"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0">Select Area</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-4" id="Div10" runat="server">
                                                        <asp:Label ID="Label16" runat="server">Pin Code</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="true" class="form-control modaltextbox"></asp:TextBox>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                            --%>
                                                            <%-- <asp:RegularExpressionValidator ID="regularexpressionvalidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                    ValidationGroup="company1" Display="dynamic" ErrorMessage="please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                            <%-- <asp:RequiredFieldValidator ID="requiredfieldvalidator6" runat="server" ErrorMessage="this value is required." CssClass="comperror"
                                                    ValidationGroup="company1" ControlToValidate="txtStreetPostCode" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <asp:Label ID="Label29" runat="server" class="control-label">Kilo Watt</asp:Label>
                                                        <asp:TextBox ID="txtCustWebSiteLink1"
                                                            runat="server" MaxLength="100" Text="India" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group col-sm-6" runat="server">
                                                        <asp:Label ID="Label32" runat="server" class="control-label">Latitude</asp:Label>
                                                        <asp:TextBox ID="txtlatitude1"
                                                            runat="server" MaxLength="20" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group col-sm-6" runat="server">
                                                        <asp:Label ID="Label34" runat="server" class="control-label">Longitude</asp:Label>
                                                        <asp:TextBox ID="txtlongitude1"
                                                            runat="server" MaxLength="20" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group col-sm-6" runat="server">
                                                        <asp:Label ID="Label41" runat="server" class="control-label">Average Monthly Unit Consumed</asp:Label>
                                                        <asp:TextBox ID="txtamuc1"
                                                            runat="server" MaxLength="100" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group col-sm-6" runat="server">
                                                        <asp:Label ID="Label42" runat="server" class="control-label">Average Monthly Bill</asp:Label>
                                                        <asp:TextBox ID="txtamb1"
                                                            runat="server" MaxLength="100" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row radioButon checkbox-info checkbox-info dispnone" visible="false" style="padding-top: 21px; margin-bottom: 13px;">
                                                    <div class="col-md-12 spicaldivin marginbtm17 ">
                                                        <table border="0" cellspacing="0" cellpadding="0" style="display: none">
                                                            <tr>
                                                                <td style="padding-right: 4px;">

                                                                    <label for="<%=chkSameasAbove.ClientID %>">
                                                                        <asp:CheckBox ID="chkSameasAbove" runat="server" OnCheckedChanged="chkSameasAbove_CheckedChanged1" AutoPostBack="true" />
                                                                        <span class="text">&nbsp;</span>
                                                                    </label>

                                                                </td>
                                                                <td>Same as street address </td>
                                                                <%--<asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdateAddress" runat="server" OnClick="btnUpdateAddress_Click"
                                                                                    Text="Update Address" />--%>
                                                                <%--<td runat="server" id="tdupdateaddress" visible="false">
                                                                                <asp:ImageButton ID="btnUpdateAddress" runat="server"
                                                                                    OnClick="btnUpdateAddress_Click" ImageUrl="~/images/icon_updateaddress.png" /></td>--%>
                                                            </tr>
                                                        </table>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div id="Div11" class="spicaldivin marginbtm15" visible="false" runat="server" style="display: none">
                                                    <span class="name">
                                                        <label class="control-label">Postal Address Line </label>
                                                    </span><span>
                                                        <asp:HiddenField ID="hndaddress1" runat="server" />
                                                        <asp:TextBox ID="txtpostaladdressline" runat="server" MaxLength="50" CssClass="form-control" name="address"> </asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="rfvpostaladdress" runat="server" ErrorMessage="* Required" Enabled="false"
                                                        ControlToValidate="txtpostaladdressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>--%>
                                                        <%-- <asp:CustomValidator ID="Custompostaladdress" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="ChkFun1"></asp:CustomValidator>--%>
                                                    </span>

                                                    <asp:HiddenField ID="hndstreetno1" runat="server" />
                                                    <asp:HiddenField ID="hndstreetname1" runat="server" />
                                                    <asp:HiddenField ID="hndstreettype1" runat="server" />
                                                    <asp:HiddenField ID="hndstreetsuffix1" runat="server" />
                                                    <asp:HiddenField ID="hndunittype1" runat="server" />
                                                    <asp:HiddenField ID="hndunitno1" runat="server" />
                                                    <div id="validaddressid1" style="display: none">
                                                        <%--<img src="../../../images/check.png" alt="check">--%>
                                                        <i class="fa fa-check"></i>Address is valid.
                                                   
                                                    </div>
                                                    <div id="invalidaddressid1" style="display: none">
                                                        <%--<img src="../../../images/x.png" alt="cross">--%>
                                                        <i class="fa fa-close"></i>Address is invalid.
                                                   
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="row form-group" style="display: none">
                                                    <div id="Div12" runat="server">
                                                        <asp:Label ID="Label17" runat="server" class="col-sm-12">Postal Address</asp:Label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtPostalAddress" name="address" Enabled="false" MaxLength="50" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row form-group" style="display: none">
                                                    <div class="col-md-6" visible="false" id="divPostalformbayUnitNo" runat="server">
                                                        <asp:Label ID="Label18" runat="server">Postal Unit No</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtPostalformbayUnitNo" name="address" runat="server" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtPostalformbayUnitNo_TextChanged"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" visible="false" id="divPostalformbayunittype" runat="server">
                                                        <asp:Label ID="Label19" runat="server">Postal Unit Type</asp:Label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlPostalformbayunittype" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbayunittype_SelectedIndexChanged">
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbayunittype_SelectedIndexChanged"--%>
                                                                <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row form-group" style="margin-bottom: 20px; display: none">
                                                    <div class="col-md-4" visible="false" id="divPostalformbayStreetNo" runat="server">
                                                        <asp:Label ID="Label20" runat="server">Postal Street No</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtPostalformbayStreetNo" name="address" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                            <%--AutoPostBack="true" OnTextChanged="txtPostalformbayStreetNo_TextChanged"--%>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtPostalformbayStreetNo" ErrorMessage="Value must be a numeric number" ForeColor="Red" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 spicaldivin streatfield" visible="false" id="divPostalformbaystreetname" runat="server">
                                                        <asp:Label ID="Label21" runat="server">Postal Street Name</asp:Label>

                                                        <div class="autocompletedropdown">
                                                            <asp:TextBox ID="txtPostalformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <%--AutoPostBack="true" OnTextChanged="txtPostalformbaystreetname_TextChanged1"--%>


                                                            <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender11" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtPostalformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>


                                                            <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield"
                                                                ClientValidationFunction="postalvalidatestreetAddress"></asp:CustomValidator>
                                                            <%--ControlToValidate="txtPostalformbaystreetname"--%>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <div id="PostalDivvalidstreetname" style="display: none">
                                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                                           
                                                            </div>
                                                            <div id="PostalDivInvalidstreetname" style="display: none">
                                                                <img src="../../../images/x.png" alt="cross">
                                                                Address is invalid.
                                                           
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="marginbtm15 col-md-4" visible="false" id="divPostalnearby" runat="server">
                                                        <asp:Label ID="Label11" runat="server">Postal Near by</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtPostalformbayNearby" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>

                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalformbayNearby" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="marginbtm15 col-md-4" visible="false" id="divPostdist" runat="server" style="display: none">
                                                        <asp:Label ID="Label31" runat="server">District</asp:Label>
                                                        <div id="Div22" class="drpValidate" runat="server">
                                                            <asp:TextBox ID="txtPostalformbaydistrict" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true"></asp:TextBox>
                                                            <%-- <asp:DropDownList ID="ddlPostalformbaydistrict" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbaydistrict_SelectedIndexChanged">
                                                            <asp:ListItem Value="">District</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" visible="false" id="divpostalformbaystreettype" runat="server" style="display: none">
                                                        <asp:Label ID="Label24" runat="server">Postal Street Type</asp:Label>
                                                        <div id="Div13" class="drpValidate" runat="server">
                                                            <asp:DropDownList ID="ddlPostalformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbaystreettype_SelectedIndexChanged"--%>
                                                                <asp:ListItem Value="">Street Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPostalformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row form-group dispnone" style="display: none">
                                                    <div class="col-md-4" id="Div14" runat="server">
                                                        <asp:Label ID="Label25" runat="server">Postal City</asp:Label>
                                                        <div class="autocompletedropdown">
                                                            <%-- <asp:TextBox ID="ddlPostalCity" AutoPostBack="true"
                                                            OnTextChanged="ddlPostalCity_OnTextChanged" MaxLength="50" Enabled="false" runat="server" class="form-control modaltextbox"></asp:TextBox>--%>

                                                            <asp:DropDownList ID="ddlPostalCity" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbaystreettype_SelectedIndexChanged"--%>
                                                                <asp:ListItem Value="">Postal City</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:TextBox ID="ddlPostalCity" AutoPostBack="true"
                                                             MaxLength="50" Enabled="false" runat="server" class="form-control modaltextbox"></asp:TextBox>--%>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPostalCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <%-- <cc1:AutoCompleteExtender ID="AutoCompleteSearch" CompletionListCssClass="autocompletedrop" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlPostalCity" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="Div15" runat="server">
                                                        <asp:Label ID="Label26" runat="server">Postal States</asp:Label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlPostalState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbaystreettype_SelectedIndexChanged"--%>
                                                                <asp:ListItem Value="">Postal States</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%-- <asp:TextBox ID="txtPostalState"
                                                            Enabled="true" runat="server" class="form-control modaltextbox"></asp:TextBox>--%>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPostalState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="Div16" runat="server">
                                                        <asp:Label ID="Label27" runat="server">Postal Post Code</asp:Label>
                                                        <div>
                                                            <asp:TextBox ID="txtPostalPostCode"
                                                                Enabled="true" runat="server" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="ddlPostalformbaypostalstate_SelectedIndexChanged"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                                    ValidationGroup="company1" ControlToValidate="txtPostalPostCode" Display="Dynamic"
                                                                    ErrorMessage="Please enter a number" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group dispnone spicaldivin">
                                                    <span class="name">
                                                        <label class="col-sm-12">Area<span class="symbol required"></span> </label>
                                                    </span>
                                                    <div class="col-sm-12">
                                                        <div id="abc" class="">
                                                            <asp:RadioButtonList ID="rblArea" CausesValidation="true" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                <%-- <asp:ListItem Value="2">Regional</asp:ListItem>--%>
                                                            </asp:RadioButtonList>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" CssClass=""
                                                            ControlToValidate="rblArea" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <%--<div class="radio radio-info radio-inline" style="padding-left: 0px;">
                                                        <label for="<%=rblArea1.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblArea1"  ValidationGroup="ar"/>
                                                            <span class="text">Metro&nbsp;</span>
                                                        </label>

                                                        <label for="<%=rblArea2.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" ValidationGroup ="ar"/>
                                                            <span class="text">Regional&nbsp; </span>
                                                        </label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" ErrorMessage="Area is requried" 
                                                        ControlToValidate="rblArea1" ForeColor="red">
                                                        </asp:RequiredFieldValidator>
                                                    </div>--%>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        <asp:Label ID="Label38" runat="server" class="control-label"> Area&nbsp;Type&nbsp;</asp:Label>
                                                        <asp:DropDownList ID="ddlAreaType" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="">Select AreaType</asp:ListItem>
                                                            <asp:ListItem Value="Sq Ft">Feet</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>

                                                    <div class="form-group col-sm-4" id="Div26" runat="server">
                                                        <asp:Label ID="Label39" runat="server" class="control-label">Heigth of Structure</asp:Label>

                                                        <asp:DropDownList ID="ddlHeightStuc" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="">Select Structure</asp:ListItem>
                                                            <asp:ListItem Value="3*1">3*1</asp:ListItem>
                                                            <asp:ListItem Value="5*8">5*8</asp:ListItem>
                                                            <asp:ListItem Value="8*12">8*12</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <asp:Label ID="Label43" runat="server" class="control-label">Premises</asp:Label>
                                                        <asp:DropDownList ID="ddlpremises" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="">Select Premises</asp:ListItem>
                                                            <asp:ListItem Value="Rented">Rented</asp:ListItem>
                                                            <asp:ListItem Value="Owned">Owned</asp:ListItem>

                                                        </asp:DropDownList>

                                                    </div>

                                                    <div class="form-group col-sm-6">
                                                        <asp:Label ID="Label44" runat="server" class="control-label">Channels Partners &nbsp;</asp:Label>
                                                        <asp:DropDownList ID="ddlchnlpart" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="">Select ChannelsPartners</asp:ListItem>
                                                            <asp:ListItem Value="Dealer">Dealer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                     <div class="form-group col-sm-6">
                                                        <asp:Label ID="Label47" runat="server" class="control-label">Common Meter Connection&nbsp;</asp:Label>
                                                        <asp:DropDownList ID="ddlcmc" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                              <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="0">NO</asp:ListItem>
                                                            <asp:ListItem Value="1">YES</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                    <div class="form-group col-sm-12" id="Div17" runat="server">
                                                        <asp:Label ID="Label28" runat="server" class="control-label">Country</asp:Label>
                                                        <asp:TextBox ID="txtCountry"
                                                            Enabled="false" runat="server" MaxLength="50" value="India" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    </div>

                                                    <div class="form-group" id="Div19" runat="server" style="display: none">
                                                        <asp:Label ID="Label30" runat="server" class="col-sm-12">Fax</asp:Label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtCustFax"
                                                                runat="server" MaxLength="20" class="form-control modaltextbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 textRight">

                                                        <%-- <asp:Button CssClass="btn largeButton greenBtn btnaddicon redreq" ID="btnAdd" ValidationGroup="RoleReqFields"
                                                            CausesValidation="true" runat="server" OnClick="btnAdd_Click" OnClientClick="ActiveLoader()"
                                                            Text="Add" />--%>

                                                        <asp:Button CssClass="btn largeButton blueBtn btnaddicon  redreq " ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                            ValidationGroup="RoleReqFields"
                                                            CausesValidation="true" Text="Add" />


                                                        <asp:Button CssClass="btn largeButton greenBtn savewhiteicon btnsaveicon redreq" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CausesValidation="false"
                                                            Text="Save" Visible="false" />
                                                        <asp:Button CssClass="btn largeButton whiteBtn btnreseticon" OnClientClick="ActiveLoader()" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                            CausesValidation="false" Text="Reset" />
                                                        <asp:Button CssClass="btn largeButton blueBtn btncancelicon" OnClientClick="ActiveLoader()" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                            CausesValidation="false" Text="Cancel" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                <ProgressTemplate>
                    
                </ProgressTemplate>
            </asp:UpdateProgress>
            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />--%>

                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div id="divleft" runat="server">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                                <div class="alert alert-success" id="panelFileupload" runat="server"><i class="icon-ok-sign"></i>&nbsp;Document Upload Successful! </div>

                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <i class="icon-remove-sign"></i>&nbsp;<asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server"><i class="icon-remove-sign"></i>&nbsp;Record with this name already exists. </div>
                                <%--  <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view
                            </div>--%>
                                <div class="alert alert-info" id="PAnAddress" runat="server"><i class="icon-info-sign"></i>&nbsp;Record with this Address already exists. </div>
                                <div class="alert alert-info" id="divTeamTime" runat="server"><i class="icon-info-sign"></i>&nbsp;Appointment Time over. </div>
                            </div>

                            <div class="searchfinal searchFilterSection searchBox">
                                <div class="widget-body shadownone brdrgray">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dataTables_filter">
                                            <div class="dataTables_filter Responsive-search">
                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                                    <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="col-sm-12">
                                                                    <div class="row">


                                                                        <%--<div class="input-group col-sm-1">
                                                                            <asp:DropDownList ID="DddlSearchFilter" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="DddlSearchFilter_SelectedIndexChanged">
                                                                                <asp:ListItem Value="">Select Filter</asp:ListItem>
                                                                                <asp:ListItem Value="1">Customer Type</asp:ListItem>
                                                                                <asp:ListItem Value="2">Rec/Com</asp:ListItem>
                                                                                <asp:ListItem Value="3">Source</asp:ListItem>
                                                                                <asp:ListItem Value="4">Sub Source</asp:ListItem>
                                                                                <asp:ListItem Value="5">Street</asp:ListItem>
                                                                                <asp:ListItem Value="6">City</asp:ListItem>
                                                                                <asp:ListItem Value="7">State</asp:ListItem>
                                                                                <asp:ListItem Value="8">PostCode</asp:ListItem>
                                                                                <asp:ListItem Value="9">Date Filter</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>--%>
                                                                        <div class="input-group col-sm-1" id="ddlSearchType1" runat="server" visible="false">
                                                                            <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Type</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivSearch(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="ddlSearchRec1" runat="server" visible="false">
                                                                            <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                                <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                                <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivRec(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" style="display: none" id="ddlSearchArea1">
                                                                            <asp:DropDownList ID="ddlSearchArea" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Area</asp:ListItem>
                                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivArea(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="ddlSearchSource1" runat="server" visible="false">
                                                                            <asp:DropDownList ID="ddlSearchSource" runat="server"
                                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged1" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Source</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivSource(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-2" id="ddlSearchSubSource1" runat="server" visible="false">
                                                                            <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivSubSource(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" style="display: none" id="ddladdressverification1">
                                                                            <asp:DropDownList ID="ddladdressverification" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">Add.Ver</asp:ListItem>
                                                                                <asp:ListItem Value="0">NO</asp:ListItem>
                                                                                <asp:ListItem Value="1">YES</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivverification(1)"></i></a>
                                                                        </div>

                                                                        <div class="input-group col-sm-2">
                                                                            <asp:TextBox ID="txtSearchCompanyNo" runat="server" placeholder="Customer No" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                                WatermarkText="Customer No" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                                ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                                                        </div>
                                                                        <div class="input-group col-sm-2 autocompletedropdown martop5">
                                                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Customer Name" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                                WatermarkText="Customer Name" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                                ServiceMethod="GetCompanyList"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        </div>
                                                                        <div class="input-group col-sm-1">
                                                                            <asp:TextBox ID="txtmobile" runat="server" placeholder="Mobile" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchStreet"
                                                                                WatermarkText="Mobile" />
                                                                        </div>
                                                                        <div class="input-group col-sm-2">
                                                                            <asp:TextBox ID="txtemail" runat="server" placeholder="Email" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtemail"
                                                                                WatermarkText="Email" />
                                                                        </div>
                                                                        <div class="input-group col-sm-2" id="txtSearchStreet1" runat="server" visible="false">
                                                                            <asp:TextBox ID="txtSearchStreet" runat="server" placeholder="Street" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtSearchStreet"
                                                                                WatermarkText="Street" />
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideSearchStreet(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1 martop5" style="width: 200px" id="txtSerachCity1" runat="server" visible="false">
                                                                            <asp:TextBox ID="txtSerachCity" runat="server" placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSerachCity"
                                                                                WatermarkText="City" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                            </cc1:AutoCompleteExtender>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideSerachCity(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="ddlSearchState1" runat="server" visible="false">
                                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideSearchState(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group col-sm-1 martop5" style="width: 138px" id="txtSearchPostCode1" runat="server" visible="false">
                                                                            <asp:TextBox ID="txtSearchPostCode" runat="server" placeholder="Post Code" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchPostCode"
                                                                                WatermarkText="Post Code" />
                                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                            </cc1:AutoCompleteExtender>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideSearchPostCode(1)"></i></a>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker2" id="divStartDate" runat="server" visible="false">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtStartDate" runat="server" placeholder="From" class="form-control pagel">
                                                                            </asp:TextBox>
                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivfFT(1);"></i></a>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker3" id="DivEndDate" runat="server" visible="false">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtEndDate" runat="server" placeholder="To" class="form-control pagel">
                                                                            </asp:TextBox>
                                                                            <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="company1"></asp:CompareValidator>

                                                                            <a class="crossBtn"><i class="pe-7s-close" onclick="hideDivfTT(1);"></i></a>
                                                                        </div>


                                                                        <div class="input-group verticalMenu">
                                                                            <div class="dropdown d-inline-block">
                                                                                <div class="input-group col-sm-1">
                                                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle noArrow"></button>
                                                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList" id="DddlSearchFilter">

                                                                                        <a id="A9"
                                                                                            onserverclick="A9_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="1" class="listItem">Customer Type</asp:ListItem></a>
                                                                                        <a id="A8"
                                                                                            onserverclick="A8_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="2" class="listItem">Rec/Com</asp:ListItem></a>
                                                                                        <a id="A7"
                                                                                            onserverclick="A7_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="3" class="listItem" id="drpSource">Source</asp:ListItem></a>
                                                                                        <a id="A6"
                                                                                            onserverclick="A6_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="4" class="listItem">Sub Source</asp:ListItem></a>
                                                                                        <a id="A5"
                                                                                            onserverclick="A10_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="5" class="listItem">Street</asp:ListItem></a>
                                                                                        <a id="A4"
                                                                                            onserverclick="A4_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="6" class="listItem">City</asp:ListItem></a>
                                                                                        <a id="A3"
                                                                                            onserverclick="A3_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="7" class="listItem">State</asp:ListItem></a>
                                                                                        <a id="A1"
                                                                                            onserverclick="A1_ClickPostCode" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="8" class="listItem">PostCode</asp:ListItem></a>
                                                                                        <a id="AnchorButton"
                                                                                            onserverclick="AnchorButton_Click" onclick="ActiveLoader()"
                                                                                            runat="server">
                                                                                            <asp:ListItem Value="9" class="listItem">Date Filter</asp:ListItem></a>
                                                                                    </div>

                                                                                    <%-- <asp:DropDownList ID="DddlSearchFilter" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="DddlSearchFilter_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="">Select Filter</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Customer Type</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Rec/Com</asp:ListItem>
                                                                                        <asp:ListItem Value="3">Source</asp:ListItem>
                                                                                        <asp:ListItem Value="4">Sub Source</asp:ListItem>
                                                                                        <asp:ListItem Value="5">Street</asp:ListItem>
                                                                                        <asp:ListItem Value="6">City</asp:ListItem>
                                                                                        <asp:ListItem Value="7">State</asp:ListItem>
                                                                                        <asp:ListItem Value="8">PostCode</asp:ListItem>
                                                                                        <asp:ListItem Value="9">Date Filter</asp:ListItem>
                                                                                    </asp:DropDownList>--%>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="input-group">
                                                                            <asp:Button ID="btnSearch" runat="server" OnClientClick="ActiveLoader()" CausesValidation="false" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="" OnClick="btnSearch_Click" />
                                                                        </div>
                                                                        <div class="input-group" style="display: none;">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                                                CausesValidation="false" OnClientClick="ActiveLoader()" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                        <div class="rightSide dispnone viewAllBtn btn">
                                                                            <div class="">
                                                                                <div class="checkbox-info ">
                                                                                    <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                                                    <span valign="top" id="tdAll1" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">

                                                                                        <label class="control-label">View All</label>
                                                                                        <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                                                        <label for="<%=chkViewAll.ClientID %>">

                                                                                            <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-success btn-xs Excel" data-original-title="Excel Export" data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title=""><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                                    </span>

                                                                                </div>
                                                                                <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%><%-- </ol>--%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="datashowbox dispnone">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer flextopBar" id="PanSearch" runat="server">

                                        <div class="dataTables_length showdata leftSide">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td></td>
                                                    <%-- <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>--%>
                                                </tr>
                                            </table>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <div class="content customerTable searchfinal searchbar mainGridTable main-card card" id="divgrid" runat="server">
                    <div id="leftgrid" runat="server">
                        <asp:HiddenField ID="HfCustId" runat="server" />
                        <div id="PanGrid" runat="server" class="finalgrid" visible="false">
                            <div class="table-responsive xscroll  noPagination">
                                <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand"
                                    OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                    OnDataBound="GridView1_DataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                    <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/arrowright.png" />
                                                    <%--    <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Name" SortExpression="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                <asp:Label ID="Label2" runat="server" Width="250px"><%#Eval("Customer")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="StreetAddress"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                    Width="1000px"><%#Eval("StreetAddress")%> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ContMobile"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lead Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="CustType"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text center">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnCustType" runat="server" Value='<%# Eval("CustType") %>' />
                                                <asp:Label ID="Label81" runat="server" class="badge"><%#Eval("CustType")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actions" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                            <ItemTemplate>


                                                <div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle noArrow"></button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList">



                                                        <asp:HyperLink ID="gvbtnDetail" runat="server" CssClass=""
                                                            Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/Company.aspx?m=comp&compid="+Eval("CustomerID") %>'> <i class="fa fa-link"></i> Detail</asp:HyperLink>
                                                        <asp:LinkButton runat="server" ID="lnkphoto" CommandName="lnkphoto" CssClass="" CommandArgument='<%#Eval("CustomerID")%>' CausesValidation="false" Visible="false"> <i class="fa fa-image"></i> Image</asp:LinkButton>
                                                        <%--  <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"

                                                   

                                                <asp:LinkButton runat="server" ID="lnkphoto" CommandName="lnkphoto" CssClass="" CommandArgument='<%#Eval("CustomerID")%>' CausesValidation="false" Visible="false"> <i class="fa fa-image"></i> Image</asp:LinkButton>
                                                <%--  <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                    Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/customerimage.aspx?id="+Eval("CustomerID") %>'> <i class="fa fa-eye"></i></asp:HyperLink>--%>
                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" OnClientClick="ActiveLoader()" CommandName="Select" CausesValidation="false" CssClass=""> <i class="fa fa-edit"></i> Edit </asp:LinkButton>

                                                        <%--                                                        <asp:LinkButton ID="panelImgLinkBtn" runat="server" OnClientClick="ActiveLoader()" CommandName="AddDocument" CommandArgument='<%#Eval("CustomerID")%>'><i class="fa fa-upload"></i>
                                                                  Upload Document </asp:LinkButton>--%>
                                                        <asp:LinkButton ID="panelImgLinkBtn" runat="server" CausesValidation="false" OnClientClick="ActiveLoader()" CommandName="AddDocument" CommandArgument='<%#Eval("CustomerID")%>'>
                                                            <i class="fa fa-upload"></i> Upload Document</asp:LinkButton>


                                                        <!--DELETE Modal Templates-->

                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="" CausesValidation="false" OnClientClick="ActiveLoader()"
                                                            CommandName="Delete" CommandArgument='<%#Eval("CustomerID")%>'><i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>

                                                        <%-- <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title=" View Project"
                                                            Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/project/project.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectId") %>'> <i class="fa fa-eye"></i> View Project</asp:HyperLink>
                                                        --%>
                                                        <!--DELETE Modal Templates-->

                                                        <asp:HyperLink ID="gvbtndetails" Target="_blank" data-toggle="tooltip" data-placement="top" data-original-title=" View Project" NavigateUrl='<%# "~/admin/adminfiles/project/project.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectId") %>' Visible='<%# (Eval("ProjectNumber")==DBNull.Value ? false : true) %>' runat="server"> <i class="fa fa-eye"></i> View Project</asp:HyperLink>
                                                    </div>


                                                    <!--END DELETE Modal Templates-->
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                            <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                <tr class="GridviewScrollItem">
                                                                    <td runat="server" id="tdassignto"><b>Assigned To</b></td>
                                                                    <td runat="server" id="tdassignto1">
                                                                        <asp:Label ID="Label7" runat="server"><%#Eval("AssignedTo")%> </asp:Label></td>
                                                                    <td><b>Customer Number</b></td>
                                                                    <td runat="server" id="tdcompnum">
                                                                        <asp:Label ID="Label1" runat="server"> <%#Eval("CompanyNumber")%> </asp:Label></td>
                                                                    <td><b>Phone</b></td>
                                                                    <td runat="server" id="td1">
                                                                        <asp:Label ID="Label36" runat="server"> <%#Eval("CustPhone")%> </asp:Label></td>
                                                                    <td><b>Email</b></td>
                                                                    <td runat="server" id="td2">
                                                                        <asp:Label ID="Label46" runat="server"> <%#Eval("ContEmail")%> </asp:Label></td>

                                                                </tr>

                                                                <tr>

                                                                    <td><b>Res Com</b></td>
                                                                    <td>
                                                                        <asp:Label ID="lblres" runat="server"></asp:Label></td>
                                                                    <td><b>Source</b></td>
                                                                    <td>
                                                                        <asp:Label ID="Label9" runat="server"><%#Eval("CustSource")%> </asp:Label></td>
                                                                    <td><b>Notes</b></td>
                                                                    <td>
                                                                        <asp:Label ID="lblNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label></td>

                                                                    <td><b>ChannelsPartners</b></td>
                                                                    <td>
                                                                        <asp:Label ID="Label91" runat="server"><%#Eval("ChannelsPartners")%> </asp:Label></td>
                                                                </tr>

                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr class="paginationRow">
                                        <td class="showEntryBtmBox">
                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                aria-controls="DataTables_Table_0" class="myvalcomp">
                                                <asp:ListItem Value="25">Display</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="dispFlexBox">
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                            <div class="pagination">
                                                <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                        <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="hpanel addEmployee customerTab">
                        <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                        <div class="bodymianbg">
                            <div class="row">
                                <div class="col-md-12" id="divright" runat="server" visible="false">
                                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                        AutoPostBack="true">
                                        <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Summary">
                                            <ContentTemplate>
                                                <div class="table-responsive">
                                                    <uc1:companysummary ID="companysummary1" runat="server" />
                                                </div>
                                            </ContentTemplate>
                                        </cc1:TabPanel>
                                        <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Contact">
                                            <ContentTemplate>
                                                <uc2:contacts ID="contacts1" runat="server" />
                                            </ContentTemplate>
                                        </cc1:TabPanel>
                                        <cc1:TabPanel ID="TabProject" runat="server" HeaderText="Project">
                                            <ContentTemplate>
                                                <uc3:project ID="project1" runat="server" />
                                            </ContentTemplate>
                                        </cc1:TabPanel>
                                        <cc1:TabPanel ID="TabInfo" runat="server" HeaderText="Follow Ups">
                                            <ContentTemplate>
                                                <uc4:info ID="info1" runat="server" />
                                            </ContentTemplate>
                                        </cc1:TabPanel>
                                        <cc1:TabPanel ID="TabDocuments" runat="server" HeaderText="Documents">
                                            <ContentTemplate>
                                                <uc6:docc ID="doc1" runat="server" />
                                            </ContentTemplate>
                                        </cc1:TabPanel>
                                        <cc1:TabPanel ID="TabConversation" runat="server" HeaderText="Conversation" Visible="false">
                                            <ContentTemplate>
                                                <uc5:conversation ID="conversation1" runat="server" />
                                            </ContentTemplate>
                                        </cc1:TabPanel>

                                    </cc1:TabContainer>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderName" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
                        </cc1:ModalPopupExtender>
                        <div id="divName" runat="server" style="display: none" class="modal_popup">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="myModalLabel">Duplicate Name</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                summary="">
                                                <tbody>
                                                    <tr align="center">
                                                        <td>
                                                            <h3 class="noline"><b>The following Location(s) have a<br />
                                                                similar name. Check for Duplicates.</b> </h3>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:Button ID="btnDupeName" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeName_Onclick"
                                                                Text="Dupe" CausesValidation="false" />
                                                            <asp:Button ID="btnNotDupeName" runat="server" OnClick="btnNotDupeName_Onclick" CausesValidation="false"
                                                                CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                            <asp:Button ID="btnOKName" Style="display: none; visible: false;" runat="server"
                                                                CssClass="btn" Text=" OK " /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="table-responsive">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <asp:GridView ID="rptName" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptName_PageIndexChanging" ShowFooter="true"
                                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Contacts">
                                                                <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                        <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                        <EmptyDataRowStyle Font-Bold="True" />
                                                        <RowStyle CssClass="GridviewScrollItem" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLData2" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderMobile" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="false" PopupControlID="divMobileCheck"
                            OkControlID="btnOKMobile" TargetControlID="btnNULLData2">
                        </cc1:ModalPopupExtender>
                        <div id="divMobileCheck" runat="server" style="display: none" class="modal_popup">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="H1">Duplicate Mobile</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body ">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                summary="">
                                                <tbody>
                                                    <tr align="center">
                                                        <td>
                                                            <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b> </h3>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">

                                                        <td>
                                                            <%--<asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />--%>
                                                            <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded"
                                                                Text="Dupe" CausesValidation="false" />
                                                            <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                                                CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                            <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                                                CssClass="btn" Text=" OK " /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="table-responsive">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <asp:GridView ID="rptMobile" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptMobile_PageIndexChanging"
                                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Contacts">
                                                                <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                        <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                        <EmptyDataRowStyle Font-Bold="True" />
                                                        <RowStyle CssClass="GridviewScrollItem" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLDataPhone" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderPhone" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="false" PopupControlID="divPhoneCheck"
                            OkControlID="btnOKPhone" TargetControlID="btnNULLDataPhone">
                        </cc1:ModalPopupExtender>
                        <div id="divPhoneCheck" runat="server" style="display: none" class="modal_popup">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="H2">Duplicate Phone</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                summary="">
                                                <tbody>
                                                    <tr align="center">
                                                        <td>
                                                            <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:Button ID="btnDupePhone" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupePhone_Onclick"
                                                                Text="Dupe" CausesValidation="false" />
                                                            <asp:Button ID="btnNotDupePhone" runat="server" OnClick="btnNotDupePhone_Onclick"
                                                                CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                            <asp:Button ID="btnOKPhone" Style="display: none; visible: false;" runat="server"
                                                                CssClass="btn" Text=" OK " /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="table-responsive">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <asp:GridView ID="rptPhone" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptPhone_PageIndexChanging"
                                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Customer">
                                                                <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("CustPhone")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                        <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                        <EmptyDataRowStyle Font-Bold="True" />
                                                        <RowStyle CssClass="GridviewScrollItem" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderEmail" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="false" PopupControlID="divEmailCheck"
                            OkControlID="btnOKEmail" TargetControlID="btnNULLData3">
                        </cc1:ModalPopupExtender>
                        <div id="divEmailCheck" runat="server" style="display: none" class="modal_popup">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="H3">Duplicate Email</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                <tbody>
                                                    <tr align="center">
                                                        <td>
                                                            <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:Button ID="btnDupeEmail" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeEmail_Onclick"
                                                                Text="Dupe" CausesValidation="false" />
                                                            <asp:Button ID="btnNotDupeEmail" runat="server" OnClick="btnNotDupeEmail_Onclick"
                                                                CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                            <asp:Button ID="btnOKEmail" Style="display: none; visible: false;" runat="server"
                                                                CssClass="btn" Text=" OK " /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="table-responsive">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <asp:GridView ID="rptEmail" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Contacts">
                                                                <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Button ID="Button2" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
                            DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
                            OkControlID="btnOKAddress" TargetControlID="Button2">
                        </cc1:ModalPopupExtender>
                        <div id="divAddressCheck" runat="server" style="display: none">
                            <div class="modal-dialog" style="left: -145px">
                                <div class="modal-content" style="min-width: 880px; overflow: scroll; height: 600px;">
                                    <div class="modal-header">
                                        <div style="float: right">
                                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" cssclass="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                Close

                                           
                                            </button>

                                        </div>
                                        <h4 class="modal-title" id="H4">Duplicate Address/Mobile</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                <tbody>
                                                    <tr align="center">
                                                        <td>
                                                            <h4 class="noline"><b>There is a Contact in the database already who has this Address/Mobile.
                                                   
                                                                <br />
                                                                This looks like a Duplicate Entry.</b></h4>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="tablescrolldiv">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                        PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Customers">
                                                                <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mobile">
                                                                <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Email">
                                                                <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdnupdateaddress" />
                        <asp:Button ID="Button1" Style="display: none;" runat="server" />

                        <asp:Button ID="btnNULLData12" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData12">
                        </cc1:ModalPopupExtender>
                        <div id="div_popup" runat="server" class="modal_popup" style="display: none">
                            <%--style="width: 900px; overflow-x: scroll;display:none;"--%>
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="color-line"></div>
                                    <div class="modal-header text-center">
                                        <div align="right" class="popheadclosebtn">

                                            <!-- <a href="javascript:printContent();" class="printicon marright15"> <i class="icon-print printpage fa fa-print"></i> </a> -->
                                            <a href="javascript:printContent();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                            <button id="ibtnCancelStatus" runat="server" onclick="ibtnCancel1_Click" type="button" class="btn btn-primary btn-xs" data-dismiss="modal" causesvalidation="false"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span> </button>
                                        </div>
                                        <h4 class="modal-title" id="H5">View Image</h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body">
                                            <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group dateimgarea" id="printimage" runat="server">
                                                            <div class="lightBoxGallery">
                                                                <asp:Repeater ID="RepeaterImages" runat="server" OnItemCommand="RepeaterImages_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <div class="galleryimage" style="border: 2px solid #000;">
                                                                            <asp:HyperLink ID="hyp1" runat="server" NavigateUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' title="Image from Unsplash" data-gallery="">
                                                                                <asp:Image ID="imgcustomer" runat="server" BorderStyle="Outset" ImageUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' CssClass="wdth100" />
                                                                            </asp:HyperLink>
                                                                            <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return ComfirmDelete(event,this)"
                                                                                data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" class="btndeleted" CommandArgument='<%# Eval("CustImageID") %>'> <i class="fa fa-times-circle-o"></i> </asp:LinkButton>
                                                                            <div style='clear: both; page-break-before: always;'></div>
                                                                            <%--   <a href="#" class="btndeleted"><i class="fa fa-times-circle-o"></i></a>--%>
                                                                        </div>
                                                                        <br />

                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <%-- Adddoc popup code--%>
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetails" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="lnkcancel">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup companyPopup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-news-paper popupIcon"></i>Add Document</h4>
                                <div>
                                    <asp:LinkButton ID="LinkButton1" runat="server" type="button" class="btn redButton btncancelIcon" data-dismiss="myModal"><i class="fa fa-times"></i>
                        Close
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="modal-body paddnone" runat="server" id="divdetail">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                        </label>
                                    </span>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row spicaldivin " id="div27" runat="server">
                                                <div class="col-sm-12">
                                                    <div class="row marginbtm15">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <asp:Label ID="Label37" runat="server" class="control-label">Document Name</asp:Label>
                                                                <div class="marginbtm15">
                                                                    <asp:DropDownList ID="ddlName" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <br />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:Label ID="Label45" runat="server" class="control-label">Document Number</asp:Label>
                                                            <asp:TextBox ID="txtNumber" runat="server" MaxLength="30" CssClass="form-control"
                                                                placeholder="Number"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                                ControlToValidate="txtNumber" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="formainline">

                                                                <div class="topfileuploadbox">
                                                                    <div class="form-group" style="margin-bottom: 0px;">
                                                                        <div style="word-wrap: break-word;">
                                                                            <span class="name">
                                                                                <label class="control-label">
                                                                                    Upload File <span class="symbol required"></span>
                                                                                </label>
                                                                                <label class="custom-fileupload">
                                                                                    <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                                    <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" class="custom-file-input" onchange="Uploadfilecustomer();" />

                                                                                    <span class="custom-file-control form-control-file form-control" id="spanfile"></span>
                                                                                    <span class="btnbox">Upload file</span>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" CssClass=""
                                                                                        ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                        ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf|.jpeg|.jpg|.PDF|.JPG|.JPEG)$" Style="color: red;"
                                                                                        Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                                    <div class="clear">
                                                                                    </div>

                                                                                </label>
                                                                                <div class="clear">
                                                                                    <asp:Label ID="errorLabel" runat="server"></asp:Label>

                                                                                </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <%--<div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>--%>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="textRight">

                                        <asp:Button CssClass="btn largeButton greenBtn redreq btnaddicon" ID="Button3" runat="server" OnClick="btnAdddoc_Click"
                                            Text="Add" ValidationGroup="uploadpdf" />
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <!--Danger Modal Templates-->




            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>

            <div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message">

                <div class="modal-dialog">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-trash popupIcon"></i>Delete</h4>
                            </div>
                        </div>
                        <%-- <label id="ghh" runat="server"></label> --%>
                        <div class="modal-body">

                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You want to Delete This Entry?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " runat="server">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 textRight">
                                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" OnClientClick="ActiveLoader()" CommandName="deleteRow" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="lnkcancel" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
            <asp:HiddenField ID="hdndelete1" runat="server" />

            <!--End Danger Modal Templates-->
            <asp:HiddenField runat="server" ID="hdncountdata" />
            </span>
        </ContentTemplate>

        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <%--<asp:PostBackTrigger ControlID="btnAdd" />--%>
            <%--<asp:PostBackTrigger ControlID="txtContMiddle" />--%>
            <%--<asp:PostBackTrigger ControlID="txtContLast" />
            <asp:PostBackTrigger ControlID="ddlStreetState" />
            <asp:PostBackTrigger ControlID="ddlDistrict" />
            <asp:PostBackTrigger ControlID="ddltaluka" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
