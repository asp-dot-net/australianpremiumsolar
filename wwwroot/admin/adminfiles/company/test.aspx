﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="admin_adminfiles_company_test" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/projectpreinst.ascx" TagPrefix="uc1" TagName="projectpreinst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-6">
        <%-- <input type="file" title="Search for a file to add" class="btn-primary btn">   --%>
        <div class="form-group dateimgarea fileuploadmain">
            <span class="name">
                <label class="control-label">
                    Signed Paperwork
                </label>
            </span><span class="dateimg">
                <asp:HyperLink ID="lblST" runat="server" Target="_blank" Visible="false"></asp:HyperLink>

                <span class="file-input btn btn-azure btn-file">
                    <asp:FileUpload ID="fuST" runat="server" />
                </span>
            </span>
            <div class="clear">
            </div>
        </div>
    </div>

    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
</asp:Content>

