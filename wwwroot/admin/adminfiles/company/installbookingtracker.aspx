<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Culture="en-GB" UICulture="en-GB" AutoEventWireup="true" CodeFile="installbookingtracker.aspx.cs"
    Inherits="admin_adminfiles_company_installbookingtracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/projectpreinst.ascx" TagPrefix="uc1" TagName="projectpreinst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script language="javascript" type="text/javascript">
        // defult enter
        function DefaultEnter(evnt) {

            var btn = $get('#<%=btnSearch.ClientID %>');

            alert(btn.text);
            var x1 = btn.id;
            if (evnt.keyCode == Sys.UI.Key.enter) {
                //__doPostBack('" + btn.id + "', '');
                location = document.getElementById('<%=btnSearch.ClientID %>').href;
                return false;
            }
            return true;
        }
    </script>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
            }
        }
    </script>
   
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

        <%--   <asp:ScriptManager id="ScriptManager1" runat="server"/>--%>

        <asp:UpdatePanel ID="updatepanelgrid" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            </ContentTemplate>
            <Triggers>

                 <asp:PostBackTrigger ControlID="btnSearch" />
            </Triggers>
        </asp:UpdatePanel>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-display2 icon"></i>Install Booking Tracker</h5>
        <div id="tdExport" class="pull-right" runat="server">
        </div>
    </div>

    <div class="page-body padtopzero">
   <asp:Panel ID="paneldefault" runat="server" DefaultButton="btnSearch">
            <div class="animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">

                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
            </div>


            <div class="searchfinal searchFilterSection" onkeypress="DefaultEnter(event);">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">

                            <div class="dataTables_filter  Responsive-search">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                    <tr>
                                        <td>

                                            <div class="input-group col-sm-1">
                                                <asp:TextBox ID="txtProjectNumber1" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtProjectNumber1"
                                                    WatermarkText="Project Num" />
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender8" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtProjectNumber1" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                    ControlToValidate="txtProjectNumber1" Display="Dynamic" ErrorMessage="Please enter a number"
                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                            </div>

                                            <div class="input-group col-sm-3 ">
                                                <asp:DropDownList ID="ddlElecDist" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myvalproject" OnSelectedIndexChanged="ddlElecDist_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Select Discom</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                             <div class="input-group col-sm-3 ">
                                                <asp:DropDownList ID="ddldivision" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                     <asp:ListItem Value="0">Select Division</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group col-sm-1">
                                                <asp:DropDownList ID="ddlSubDivision" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="0">Sub Division</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-2">
                                                <asp:DropDownList ID="ddlchnlpart" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="0">Select Chanel Partner</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group col-sm-3 " id="Div6" runat="server">
                                                <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox" placeho></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="ddltaluka"
                                                    WatermarkText="Select Taluka" />
                                            </div>
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                            <div class="input-group col-sm-2 ">
                                                <asp:DropDownList ID="ddlSelectitems" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                    <asp:ListItem Value="1">Active Date</asp:ListItem>
                                                    <asp:ListItem Value="2">Install Start Date</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtStartDate1" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="input-group date datetimepicker1">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtEndDate1" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                            </div>
                                            <div class="inlineblock" style="display: none">

                                                <div class="input-group col-sm-1">
                                                    <asp:TextBox ID="txtClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtClient"
                                                        WatermarkText="Client" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtClient" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                        WatermarkText="Manual Num" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>
                                                <div class="input-group col-sm-1">
                                                    <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStreet"
                                                        WatermarkText="Street" />
                                                </div>
                                                <div class="input-group col-sm-1">
                                                    <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                        WatermarkText="City" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                    </cc1:AutoCompleteExtender>
                                                </div>
                                                <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                    <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group groupalign col-sm-2">
                                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text="Project Status :" Enabled="false"></asp:TextBox>
                                                </div>
                                                <div class="input-group col-md-2" style="width: 148px">

                                                    <asp:TextBox ID="txtSearchPostCodeFrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCodeFrom"
                                                        WatermarkText="From" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtSearchPostCodeFrom" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                    </cc1:AutoCompleteExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSearchPostCodeFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-md-2">
                                                    <asp:TextBox ID="txtSearchPostCodeTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender14" runat="server" TargetControlID="txtSearchPostCodeTo"
                                                        WatermarkText="To" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtSearchPostCodeTo" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                    </cc1:AutoCompleteExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSearchPostCodeTo" Display="Dynamic" ErrorMessage="Enter valid value"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-sm-1" style="width: 120px" id="div1" runat="server">
                                                    <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Project Status</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1" style="width: 120px" id="div2" runat="server">
                                                    <asp:DropDownList ID="ddlProjectTypeID" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="" Text="Project Type"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1" style="width: 140px;">
                                                    <asp:TextBox ID="txtPModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtPModel"
                                                        WatermarkText="Panel Model" />
                                                </div>
                                                <div class="input-group col-sm-1" style="width: 145px;">
                                                    <asp:TextBox ID="txtIModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtIModel"
                                                        WatermarkText="Inverter Model" />
                                                </div>
                                                <div class="input-group groupalign col-md-2">
                                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text="System Capacity:" Enabled="false"></asp:TextBox>
                                                </div>
                                                <div class="input-group col-md-2" style="width: 148px">
                                                    <asp:TextBox ID="txtSysCapacityFrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtSysCapacityFrom"
                                                        WatermarkText="From" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSysCapacityFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-md-2" style="width: 148px">
                                                    <asp:TextBox ID="txtSysCapacityTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender15" runat="server" TargetControlID="txtSysCapacityTo"
                                                        WatermarkText="To" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtSysCapacityTo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                </div>
                                                <div class="input-group col-sm-1" style="width: 172px" id="div3" runat="server">
                                                    <asp:DropDownList ID="ddlFinanceOption" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">FinanceOption</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-2" style="width: 248px;">
                                                    <asp:TextBox ID="txtpurchaseNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtpurchaseNo"
                                                        WatermarkText="Purchase Num" />
                                                </div>
                                                <div class="input-group col-sm-1" id="div4" runat="server">
                                                    <asp:DropDownList ID="ddlElecDistAppDate" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Dist Approved Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group col-sm-1" id="div5" runat="server">
                                                    <asp:DropDownList ID="ddldatetype" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval">
                                                        <asp:ListItem Value="">Select Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Invoice Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Next Follow Up</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control "></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                        ControlToValidate="txtStartDate" ValidationGroup="search" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" ValidationGroup="search"
                                                        ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                </div>
                                                <div class="form-group checkbox-info paddtop3td alignchkbox btnviewallorange">

                                                    <label for="<%=chkishistoric.ClientID %>" class="btn btn-magenta">
                                                        <asp:CheckBox ID="chkishistoric" runat="server" />
                                                        <span class="text">Historic</span>
                                                    </label>

                                                </div>


                                            </div>

                                            <div class="input-group">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon wid100btn btngray" Text="Search" OnClick="btnSearch_Click" />
                                            </div>
                                            <div class="input-group">
                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info wid100btn btngray"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                            </div>
                                            <div class="input-group">
                                                <div id="Div7" class="pull-right" runat="server">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-info wid100btn btngray" data-original-title="Excel Export" data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title=""><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <asp:HiddenField runat="server" ID="hdnInvoiceProjectid" />
            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
            </cc1:ModalPopupExtender>
            <div runat="server" id="div_popup" class="modal_popup" style="display: none; width: auto">
                <div class="modal-dialog modal-lg" style="width: 1100px">
                    <div class="modal-content">
                        <div class="color-line"></div>

                        <div class="modal-header text-center">
                            <button id="ibtnCancelStatus" runat="server" type="button" class="close width40" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="H1">
                                <asp:Label runat="server" ID="lbltitle" Text="Pre Inst"></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body" style="height: 500px; overflow-y: auto">
                                <div class="tablescrolldiv">
                                    <%--      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                                                <ContentTemplate>--%>
                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd" id="mdpop">
                                        <uc1:projectpreinst runat="server" ID="projectpreinst" />
                                    </div>
                                    <%--           </ContentTemplate>
                                                    <Triggers>
                                                     <asp:PostBackTrigger ControlID="GridView1" />
                                                         <%--<asp:PostBackTrigger ControlID="lnkjob" />--%>
                                    <%--     <asp:PostBackTrigger ControlID="mdpop" />--%>
                                    <%--  </Triggers>
                                                    </asp:UpdatePanel>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hdn_custpopup" />
        </asp:Panel>

        <div class="customerTable searchfinal searchbar mainGridTable main-card card">
            <asp:Panel ID="panel" runat="server">

                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header">
                        <div>
                            <b>Total Number of panels:&nbsp;</b>
                            <asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>

            </asp:Panel>

            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">
                        <div id="leftgrid" runat="server">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div id="PanGrid" class="wid100" runat="server" visible="false">
                                    <div class="table-responsive xscroll  noPagination">
                                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowCommand="GridView1_RowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                            <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/arrowright.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <asp:Label ID="lblProject9" runat="server">
                                                            <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/project/project.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Project_Status" runat="server"><%#Eval("ProjectStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub division" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Subdivision" runat="server"><%#Eval("Subdivision")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Customername">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject3" runat="server"><%#Eval("CustomerName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPhone" runat="server"><%#Eval("MobileNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Installer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstaller" runat="server"><%#Eval("InstallerName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Installer Start Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallerBooking" runat="server"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stucture height" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="RoofType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProjectRoofType" runat="server"><%#Eval("RooftopArea")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update Data" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="left" SortExpression="InstallerPayDate">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnPaid" CommandName="Paid" CssClass="btn btn-sky btn-xs"
                                                            CommandArgument='<%#Eval("ProjectID") %>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Update Data">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover subtablecolor">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Installation Address</b>
                                                                            </td>
                                                                            <td colspan="5"><%#Eval("Project")%></td>
                                                                           <%-- <td>InstallerName
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label1" runat="server"><%#Eval("InstallerName")%></asp:Label>
                                                                            </td>--%>
                                                                              <td><b>Install Note</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblInstallNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>'><%#Eval("InstallerNotes")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Employee Name</b>
                                                                            </td>
                                                                            <td colspan="5">
                                                                                <asp:Label ID="lblProject18" runat="server"><%#Eval("Employeename")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Application Status</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProjectStatus" runat="server"><%#Eval("ApplicationStatus")%></asp:Label>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td runat="server" id="thtype"><b>Sys</b>
                                                                            </td>
                                                                            <td runat="server" id="tdtype" colspan="5">
                                                                                <asp:Label ID="lblProject11" runat="server"><%#Eval("inverterDetails")%></asp:Label>
                                                                            </td>
                                                                            <td>System Cost
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label4" runat="server"> <%#Eval("Servicevalue","{0:0.00}")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td><b>Active Date</b>
                                                                            </td>
                                                                            <td colspan="5">
                                                                                <asp:Label ID="lblProject14" runat="server"><%# DataBinder.Eval(Container.DataItem, "activedate", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>
                                                                          
                                                                            <td><b>Channels Partners</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label3" runat="server"><%#Eval("ChannelsPartners")%></asp:Label>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td class="showEntryBtmBox">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Display</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>

        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnclosepopup" />
    <asp:Button ID="btnNULLPaid" Style="display: none;" runat="server" />

    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
        PopupControlID="divPaiddate" TargetControlID="btnNULLPaid" CancelControlID="ibtnCancel">
    </cc1:ModalPopupExtender>

    <div id="divPaiddate" runat="server" style="display: none;" class="modal_popup addDocumentPopup">
        <div class="modal-dialog" style="width: 650px;">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">

                    <div class="modalHead">
                        <h4 class="modal-title" id="myModalLabel1">Update Installer Details</h4>
                        <div>
                            <asp:LinkButton ID="ibtnCancel" runat="server" type="button" class="btn redButton btncancelIcon" data-dismiss="modal"
                                OnClick="ibtnCancel_Click"><i class="fa fa-times"></i>Close
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="boxPadding">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <span class="name">
                                <asp:Label ID="Label2" runat="server" class="control-label">
                                               Install Start Date</asp:Label></span>
                            <div class="input-group datevalidation date wid100 datetimepicker1">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                                <asp:TextBox ID="txtInstallBookingDate" runat="server" class="form-control" Width="125px">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group dateimgarea col-md-6">
                            <span class="name disblock">
                                <label class="control-label">
                                    Stock Allocation Store
                                </label>
                            </span>
                            <div class="drpValidate">
                                <asp:DropDownList ID="ddlStockAllocationStore" runat="server" AppendDataBoundItems="true"
                                    aria-controls="DataTables_Table_0" class="myval">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=""
                                    ControlToValidate="ddlStockAllocationStore" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6" style="display: none">
                            <span class="name">
                                <label class="control-label">System Details</label>
                            </span>
                            <asp:TextBox ID="txtsysdetails1" runat="server"
                                CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="form-group col-md-6">
                            <span class="name disblock">
                                <label class="control-label">
                                    Installer
                                </label>
                            </span>
                            <div class="drpValidate">
                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span class="name">
                                <label class="control-label">
                                    Installer Notes
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtInstallerNotes" runat="server" TextMode="MultiLine" Height="70px"
                                    CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div class="form-group center-text">
                        <asp:Button ID="ibtnUpdateInstallerData" runat="server" Text="Update" OnClick="ibtnUpdateInstallerData_Click"
                            ValidationGroup="AddNotesPaid" CssClass="largeButton greenBtn savewhiteicon btnsaveicon" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hndinvoicepaymentid" runat="server" />


    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $("[data-toggle=tooltip]").tooltip();

        }
    </script>
</asp:Content>

