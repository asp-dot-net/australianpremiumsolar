﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="mtcecalldetail.aspx.cs" Inherits="admin_adminfiles_company_mtcecalldetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">

             <div class="finalheader">
                <div class="small-header transition animated fadeIn">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div id="hbreadcrumb" class="pull-right printorder">
                               <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i>Back</asp:LinkButton>
                            </div>

                            <h2 class="font-light m-b-xs printorder">Maintenance Detail
                            </h2>

                        </div>
                    </div>
                </div>
            </div>

          <%--  <h3 class="m-b-xs text-black printorder">Maintenance Detail</h3>--%>
            <div class="contactsarea">
                <div class="addcontent printpage printorder pull-right">
                    
                </div>
                <div class="bodymianbg">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="searcharea paddtopnone printpage">
                                <div class="clear">
                                </div>
                            </div>
                            <div class="panel panel-default " id="PanGrid" runat="server">
                                <div class="table-responsive" style="margin-bottom : 60px">
                                    <asp:Literal ID="liDetail" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

