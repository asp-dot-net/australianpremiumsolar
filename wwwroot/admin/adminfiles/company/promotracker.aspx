<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="promotracker.aspx.cs" Culture="en-GB" UICulture="en-GB" Inherits="admin_adminfiles_company_promotracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Src="~/includes/controls/promo_updatedata.ascx" TagPrefix="uc1" TagName="promo_updatedata" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script> --%>


    <script type="text/javascript">


        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>


            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress


                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress


                    //if (args.get_error() != undefined) {
                    //    args.set_errorhandled(true);
                    //}

                    $(".dropdown dt a").on('click', function () {
                        $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function () {
                        $(".dropdown dd ul").hide();
                    });


                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                    //callMultiCheckbox();
                }

                function pageLoaded() {
                    //alert($(".search-select").attr("class"));

                    $(".myval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                    //gridviewScroll();
                    //$("[data-toggle=tooltip]").tooltip();

                    callMultiCheckbox();
                }
            </script>
            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="pe-7s-display2 icon"></i>Promo Tracker</h5>
                <div id="divpopup" class="pull-right" runat="server">
                    
                        <asp:LinkButton ID="lnkjob" runat="server" data-toggle="tooltip" data-original-title="Update Data" Visible="false"
                            OnClick="lnkjob_Click" CssClass="btn btn-info btngray">Update Data</asp:LinkButton>
                    
                </div>
            </div>
            <div class="page-body padtopzero padbtmzero">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>

                <div class="widget-body shadownone brdrgray" style="margin-bottom: 10px;">
                    <div class="dataTables_wrapper dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <div class="searchfinal searchFilterSection">
                                    <div class="widget-body shadownone brdrgray">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="dataTables_filter">
                                                <div class="row">




                                                    <div class="inlineblock">
                                                        <div class="col-sm-12">
                                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 0px;" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div class="input-group" id="divCustomer" runat="server" style="width: 150px;">
                                                                            <asp:DropDownList ID="ddlinterested2" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                <asp:ListItem Value="0">Interested</asp:ListItem>
                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1">
                                                                            <asp:TextBox ID="txtcontactname2" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtcontactname2"
                                                                                WatermarkText="Contact" />
                                                                        </div>

                                                                        <div class="input-group" style="width: 150px" id="tdEmployee2" runat="server">
                                                                            <asp:DropDownList ID="ddlSearchEmployee2" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Employee</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group col-sm-1" style="width: 130px" id="tdteam2" runat="server">
                                                                            <asp:DropDownList ID="ddlTeam2" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Select Team</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class="input-group date datetimepicker1">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtstart2" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtstart2" ValidationGroup="search" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                        </div>

                                                                        <div class="input-group date datetimepicker1">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtend2" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic" ValidationGroup="search"
                                                                                ControlToValidate="txtend2" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtstart2" ControlToValidate="txtend2" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                        </div>
                                                                        <div class="input-group">
                                                                        <div class="rightSide viewAllBtn btn">
                                                                            <div class="checkbox-info">
                                                                            <label class="control-label">Historic</label>
                                                                            <asp:CheckBox ID="chkhist2" runat="server" />
                                                                            <label for="<%=chkhist2.ClientID %>" class="btn btn-magenta">
                                                                            </label>
                                                                            </div>
                                                                        </div>
                                                                        </div>


                                                                        <div class="input-group">
                                                                            <asp:Button ID="btnSearch2" runat="server" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch2_Click" />
                                                                        </div>
                                                                        <div class="input-group">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left" title="Clean"
                                                                                CausesValidation="false" OnClick="btnClearAll2_Click" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="datashowbox dispnone">
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="row">
                                                    <div class="col-sm-3" style="width: 50%;">
                                                        <div class="dataTables_length">


                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    
                                                                    <td>&nbsp;</td>
                                                                    <div runat="server" visible="false" id="divpromosms" class="contacttoparea form-inline padd10all">
                                                                        <td>

                                                                            <div class="input-group" runat="server">
                                                                                <asp:Button runat="server" ID="btnread" OnClick="btnread_Click" Text="Read" CssClass="btn btn-primary savewhiteicon" />
                                                                            </div>
                                                                            <div class="input-group col-sm-1" style="width: 80px;">
                                                                                <asp:DropDownList runat="server" ID="ddlpromooffer" AppendDataBoundItems="true" CssClass="myval">
                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="input-group" id="Div4" runat="server">
                                                                                <asp:Button runat="server" ID="btnpromooffer" OnClick="btnpromooffer_Click" Text="Update" CssClass="btn btn-primary savewhiteicon" />
                                                                            </div>
                                                                        </td>
                                                                    </div>
                                                                </tr>

                                                            </table>

                                                            <table class="" cellspacing="0" cellpadding="0" rules="all" border="0" id="table2" style="width: 100%; border-collapse: collapse;">
                                                                <%--        <h3 class="m-b-xs text-black">Other Notification</h3>--%>

                                                            </table>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 printorder">
                                                        <div id="tdExport" class="pull-right btnexelicon" runat="server">

                                                            <%-- <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </span>
                 
                    <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>--%>
                                                            <%--  </ol>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div class="finalgrid searchfinal searchbar mainGridTable main-card mb-3 card">

                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer tablegridselect">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive printArea">
                                    <%--ProjectID  PromoID--%>
                                    <asp:GridView ID="GridView2" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging"
                                        OnRowCreated="GridView2_RowCreated1" OnDataBound="GridView2_DataBound" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" HeaderText="Read">
                                                <HeaderTemplate>
                                                    <center>
                                                                Read
                                                        <span class="euroradio">                                      
                                                            
                                                          <%--  <label for='<%# ((GridViewRow)Container).FindControl("chkreadhead").ClientID %>' runat="server" id="lblchk131">
                                                                <asp:CheckBox ID="chkreadhead" runat="server" AutoPostBack="true" OnCheckedChanged="chkreadhead_CheckedChanged"  />
                                                            <span  class="text">&nbsp;</span>
                                                            </label>--%>
                                                        </span>
                                                      </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <label for='<%# ((GridViewRow)Container).FindControl("chkread").ClientID %>' runat="server" id="lblchk789">
                                                        <asp:CheckBox ID="chkread" runat="server" Visible='<%#Eval("ReadFlag").ToString()=="1"?true:false %>' />
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="60px">
                                                <HeaderTemplate>
                                                    <center>Yes/No
                                            <span class="euroradio">                                      
                                               
                                              <%--  <label for='<%# ((GridViewRow)Container).FindControl("chkpromo").ClientID %>' runat="server" id="lblchk111">
                                                     <asp:CheckBox ID="chkpromo" runat="server" AutoPostBack="true" OnCheckedChanged="chkpromo_CheckedChanged" />
                                                    <span class="text">&nbsp;</span>
                                                </label>--%>
                                            </span>
                                        </center>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("PromoID") %>' />

                                                    <label for='<%# ((GridViewRow)Container).FindControl("chktagpromo").ClientID %>' runat="server" id="lblchk">
                                                        <asp:CheckBox ID="chktagpromo" runat="server" Visible='<%#Eval("Interested").ToString()=="3"?true:false %>' />
                                                        <span class="text">&nbsp;</span>
                                                    </label>


                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150px"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label35" runat="server" Width="150px">
                                                                <%#Eval("Employee")%>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150px"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Width="150px">
                                                        <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                                        <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'><%#Eval("fullname")%></asp:HyperLink>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Promo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Promo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl1" runat="server" Width="80px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>' CssClass="tooltipwidth"><%#Eval("Promo")%> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Promo Sent" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl3" runat="server" Width="80px"><%# Eval("PromoSent","{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Interested" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="contactpersonname" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="1"?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title="Yes" CssClass="tooltipwidth">Yes</asp:Label>
                                                    <asp:Label ID="Label2" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="2"?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title="No" CssClass="tooltipwidth">No</asp:Label>
                                                    <asp:Label ID="lbl2" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="3"?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ResponseMsg")%>' CssClass="tooltipwidth"><%#Eval("ResponseMsg")%> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Interested" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="contactpersonname">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblrspmsg" Width="80px"><%# Eval("ResponseMsg")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl35" runat="server" Width="80px"><%# Eval("ContMobile")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl34" runat="server" Width="80px"><%# Eval("ContPhone")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                    <div class="paginationnew1" runat="server" id="divnopage2">
                                        <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td class="showEntryBtmBox">
                                                    <asp:DropDownList ID="ddlSelectRecords2" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords2_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="">Display</asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>
                                                <td>
                                                    <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <%--testttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt--%>
            <asp:Panel runat="server" ID="Panel2" Visible="false">
                <div class="content animate-panel" style="padding-bottom: 0px!important;">
                    <div class="animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="Div2" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="searchfinal">
                    <div class="widget-body shadownone brdrgray">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                                <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch">
                                    <div class="row">




                                        <div class="inlineblock" visible="false">
                                            <div class="col-sm-12">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="contactbrmbtm padd10all">
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtStartDate" ValidationGroup="search" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" ValidationGroup="search"
                                                                        ControlToValidate="txtEndDate" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="ddlinterested" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="0">Interested</asp:ListItem>
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:TextBox ID="txtcontactname" runat="server" Width="130px" CssClass="form-control"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtcontactname"
                                                                        WatermarkText="Contact" />
                                                                </div>
                                                                <div class="form-group spical" style="width: 150px" id="tdEmployee" runat="server" visible="false">
                                                                    <asp:DropDownList ID="ddlSearchEmployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="form-group" style="width: 130px" runat="server" id="tdteam" visible="false">
                                                                    <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Select Team</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="form-group spical multiselect" style="width: 142px">
                                                                    <dl class="dropdown">
                                                                        <dt>
                                                                            <a href="#">
                                                                                <span class="hida" id="spanselect">Select</span>
                                                                                <p class="multiSel"></p>
                                                                            </a>
                                                                        </dt>
                                                                        <dd id="ddproject" runat="server">
                                                                            <div class="mutliSelect" id="mutliSelect">
                                                                                <ul>
                                                                                    <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <li>
                                                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                <%--  <span class="checkbox-info checkbox">--%>
                                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                    <span></span>
                                                                                                </label>
                                                                                                <%-- </span>--%>
                                                                                                <label class="chkval">
                                                                                                    <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                </label>
                                                                                            </li>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </ul>
                                                                            </div>
                                                                        </dd>
                                                                    </dl>
                                                                </div>
                                                                <div class="form-group checkbox-info paddtop3td alignchkbox btnviewallorange">

                                                                    <label for="<%=chkhist.ClientID %>" class="btn btn-magenta">
                                                                        <asp:CheckBox ID="chkhist" runat="server" />
                                                                        <span class="text">Historic</span>
                                                                    </label>

                                                                </div>
                                                                <div class="form-group">
                                                                    <span>

                                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                    </span>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="LinkButton5" runat="server"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn-primary btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>

                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>

                        <div class="datashowbox">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="dataTables_length showdata">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="padtopzero">
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-9 printorder">
                                        <div id="Div9" class="pull-right btnexelicon" runat="server">

                                            <%-- <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </span>
                 
                    <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>--%>
                                            <%--  </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="PanGrid2" runat="server">
                    <div runat="server" visible="false" id="div1" class="contacttoparea form-inline padd10all">
                        <table class="" cellspacing="0" cellpadding="0" rules="all" border="0" id="table3" style="width: 100%; border-collapse: collapse;">
                            <tr>
                                <td width="10%">
                                    <ul class="othernotification">
                                        <li>
                                            <asp:Button runat="server" ID="btnstopsms" OnClick="btnstopsms_Click" Text="Stop SMS" CssClass="btn btn-primary savewhiteicon" />
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="finalgrid searchfinal searchbar mainGridTable main-card mb-3 card">

                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer tablegridselect">
                            <div>
                                <div id="Div10" runat="server">
                                    <div class="table-responsive printArea">
                                        <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                                            OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                                                    <HeaderTemplate>
                                                        <center>
                                                            <span class="euroradio">                                      
                                                            <asp:CheckBox ID="chksmshead" runat="server" AutoPostBack="true" OnCheckedChanged="chksmshead_CheckedChanged" />
                                                            <label for='<%# ((GridViewRow)Container).FindControl("chksmshead").ClientID %>' runat="server" id="lblchk1r1">
                                                            <span></span>
                                                            </label>
                                                            </span>
                                                            </center>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("PromoID") %>' />
                                                        <asp:CheckBox ID="chksms" runat="server" Visible='<%#Eval("SendSMS").ToString()=="True"?true:false %>' />
                                                        <label for='<%# ((GridViewRow)Container).FindControl("chksms").ClientID %>' runat="server" id="lblchk789">
                                                            <span></span>
                                                        </label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150px"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label35" runat="server" Width="150px">
                                                                <%#Eval("Employee")%>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150px"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" Width="150px">
                                                            <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                                            <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'><%#Eval("fullname")%></asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Promo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Promo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl1" runat="server" Width="80px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Description")%>' CssClass="tooltipwidth"><%#Eval("Promo")%> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Promo Sent" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl3" runat="server" Width="80px"><%# Eval("PromoSent","{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Interested" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="contactpersonname">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="Label1" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="1"?true:false%>' Text="Yes"></asp:Label>

                                                            <asp:Label ID="Label2" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="2"?true:false%>'>No</asp:Label>

                                                            <asp:Label ID="lbl2" runat="server" Width="80px" Visible='<%#Eval("Interested").ToString() =="3"?true:false%>'>other </asp:Label>
                                                            <br />--%>
                                                        <asp:Label ID="Label6" runat="server" Width="80px" Visible='<%#Eval("ResponseMsg").ToString() !=string.Empty?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ResponseMsg")%>' CssClass="tooltipwidth"><%#Eval("ResponseMsg")%></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl31" runat="server" Width="80px"><%# Eval("ContMobile")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl33" runat="server" Width="80px"><%# Eval("ContPhone")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl35" runat="server" Width="80px"><%# Eval("ProjectStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Next FollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="NextFollowupDate">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%# Eval("CustomerID")%>' />
                                                        <asp:Label ID="lblFollowUpDate" runat="server"
                                                            Width="120px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>

                                
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
                <%--testttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt--%>
            </asp:Panel>


            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtenderEdit" runat="server" BackgroundCssClass="modalbackground"
                CancelControlID="btnCancelEdit" DropShadow="false" PopupControlID="divEdit" TargetControlID="btnNULLData1">
            </cc1:ModalPopupExtender>


            <div class="modal_popup addDocumentPopup" id="divEdit" runat="server" style="display: none;">
                <div class="modal-dialog" style="width:500px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div class="modalHead">
                                <h4 class="modal-title" id="H3"><i class="pe-7s-diskette popupIcon"></i>Update Promo Data</h4>
                                <div>
                                    <asp:LinkButton ID="btnCancel" CausesValidation="false" runat="server" CssClass="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal" OnClick="btnCancel_Click">Close
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body paddnone formGrid">
                            <div class="panel-body">
                                <div class="formainline formnew">

                                    <div class="clear"></div>

                                    <div class="spicaldivin">


                                        <div class="formainline">
                                            <div class="form-group">
                                                <label class="control-label">Promo Text&nbsp;</label>
                                                <div class="drpValidate">
                                                    <asp:TextBox ID="txtpromo" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtpromo"
                                                        ErrorMessage="" CssClass="reqerror" ValidationGroup="updatedata" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" id="divElecDistApproved" runat="server">
                                                <span class="name">
                                                    <asp:Label ID="Label23" runat="server" class="control-label">Promo Sent</asp:Label></span>
                                                <div class="input-group date wid100 datetimepicker1">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtpromosent" runat="server" class="form-control" Width="210px">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtpromosent"
                                                        ErrorMessage="" CssClass="reqerror" ValidationGroup="updatedata" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="dividerLine"></div>
                                            <div class="textRight">
                                                <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnupdate_Click"
                                                    ValidationGroup="updatedata" CssClass="btn largeButton greenBtn btnsaveicon" CausesValidation="true" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


      </ContentTemplate>
        <Triggers>
            <%--  <asp:PostBackTrigger ControlID="btnread" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnread" />
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnSearch2" />
        </Triggers>
    </asp:UpdatePanel>

    <div class="loaderPopUP">
        <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    //$("[data-toggle=tooltip]").tooltip();
                    $('#<%=btnupdate.ClientID %>').click(function () {
                        formValidate();
                    });
                }
        </script>
    </div>
    <script type="text/javascript">

                $(".dropdown dt a").on('click', function () {
                    $(".dropdown dd ul").slideToggle('fast');

                });

                $(".dropdown dd ul li a").on('click', function () {
                    $(".dropdown dd ul").hide();
                });
                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });


                $(document).ready(function () {


                    HighlightControlToValidate();

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                });

                function callMultiCheckbox() {
                    var title = "";
                    $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });
                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }

                }


                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }
                function HighlightControlToValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            $('#' + Page_Validators[i].controltovalidate).blur(function () {
                                var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                                if (validatorctrl != null && !validatorctrl.isvalid) {
                                    $(this).css("border-color", "#FF5F5F");
                                }
                                else {
                                    $(this).css("border-color", "#B5B5B5");
                                }
                            });
                        }
                    }
                }
                function getValidatorUsingControl(controltovalidate) {
                    var length = Page_Validators.length;
                    for (var j = 0; j < length; j++) {
                        if (Page_Validators[j].controltovalidate == controltovalidate) {
                            return Page_Validators[j];
                        }
                    }
                    return null;
                }
    </script>
    <script type="text/javascript">
                $(document).ready(function () {
                    gridviewScroll();
                });
                $("#nav").on("click", "a", function () {
                    $('#content').animate({ opacity: 0 }, 500, function () {
                        gridviewScroll();
                        $('#content').delay(250).animate({ opacity: 1 }, 500);
                    });
                });
                function gridviewScroll() {
           <%-- $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
                }
    </script>
</asp:Content>




