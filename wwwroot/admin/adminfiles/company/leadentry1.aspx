﻿ <%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="leadentry1.aspx.cs" Inherits="admin_adminfiles_company_leadentry" ValidateRequest="false" EnableEventValidation="false" Theme="admin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/companysummary.ascx" TagName="companysummary" TagPrefix="uc1" %>
<%@ Register Src="~/includes/controls/contacts.ascx" TagName="contacts" TagPrefix="uc2" %>
<%@ Register Src="~/includes/controls/project.ascx" TagName="project" TagPrefix="uc3" %>
<%@ Register Src="~/includes/controls/info.ascx" TagName="info" TagPrefix="uc4" %>
<%@ Register Src="~/includes/controls/conversation.ascx" TagName="conversation" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="chkaddval" value="0" />
    <input type="hidden" id="chkaddval1" value="0" />
    <style type="text/css">
        .ui-autocomplete-loading {
            background: white url("../../../images/indicator.gif") right center no-repeat;
        }

      
    </style>
    <script>

        function CompanyAddress() {
            $("#<%=txtstreetaddressline.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'auto',
                            term: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    //                    log(ui.item ?
                    //					"Selected: " + ui.item.label :
                    //					"Nothing selected, input was " + this.value);
                },
                open: function () {
                    //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
            $("#<%=txtpostaladdressline.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "address.aspx",
                        dataType: "jsonp",
                        data: {
                            s: 'auto',
                            term: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    //                    log(ui.item ?
                    //					"Selected: " + ui.item.label :
                    //					"Nothing selected, input was " + this.value);
                },
                open: function () {
                    //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });
            // });
        }
        function getParsedAddress() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'address',
                    term: $("#<%=txtstreetaddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data != null) {
                        document.getElementById("<%= txtStreetAddress.ClientID %>").value = data.StreetLine;
                        document.getElementById("<%= txtStreetPostCode.ClientID %>").value = data.Postcode;
                        document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                        document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                        document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                        document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                        document.getElementById("<%= ddlStreetCity.ClientID %>").value = data.Suburb;
                        document.getElementById("<%= txtStreetState.ClientID %>").value = data.State;
                        document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                        document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                    }
                    validateAddress();
                }
            });
        }

        function validateAddress() {

            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'validate',
                    term: $("#<%=txtstreetaddressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data == true) {
                        document.getElementById("validaddressid").style.display = "block";
                        document.getElementById("invalidaddressid").style.display = "none";

                        document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                    }
                    else {
                        document.getElementById("validaddressid").style.display = "none";
                        document.getElementById("invalidaddressid").style.display = "block";
                        document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                    }
                }
            });
        }

        function getParsedAddress1() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'address',
                    term: $("#<%=txtpostaladdressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data != null) {
                        document.getElementById("<%= txtPostalAddress.ClientID %>").value = data.StreetLine;
                        document.getElementById("<%= txtPostalPostCode.ClientID %>").value = data.Postcode;
                        document.getElementById("<%= hndstreetno1.ClientID %>").value = data.Number;
                        document.getElementById("<%= hndstreetname1.ClientID %>").value = data.Street;
                        document.getElementById("<%= hndstreettype1.ClientID %>").value = data.StreetType;
                        document.getElementById("<%= hndstreetsuffix1.ClientID %>").value = data.StreetSuffix;
                        document.getElementById("<%= ddlPostalCity.ClientID %>").value = data.Suburb;
                        document.getElementById("<%= txtPostalState.ClientID %>").value = data.State;
                        document.getElementById("<%= hndunittype1.ClientID %>").value = data.UnitType;
                        document.getElementById("<%= hndunitno1.ClientID %>").value = data.UnitNumber;
                    }
                    validateAddress1();
                }
            });
        }

        function validateAddress1() {
            $.ajax({
                url: "address.aspx",
                dataType: "jsonp",
                data: {
                    s: 'validate',
                    term: $("#<%=txtpostaladdressline.ClientID %>").val()
                },
                success: function (data) {
                    if (data == true) {
                        document.getElementById("validaddressid1").style.display = "block";
                        document.getElementById("invalidaddressid1").style.display = "none";
                        //  alert(document.getElementById("<%= hndaddress1.ClientID %>").value);
                        document.getElementById("<%= hndaddress1.ClientID %>").value = "1";
                    }
                    else {
                        document.getElementById("validaddressid1").style.display = "none";
                        document.getElementById("invalidaddressid1").style.display = "block";
                        document.getElementById("<%= hndaddress1.ClientID %>").value = "0";
                    }
                }
            });
        }
        function validatestreetAddress(source, args) {
            var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();
            $.ajax({
                type: "POST",
                //action:"continue.aspx",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        document.getElementById("Divvalidstreetname").style.display = "block";
                        document.getElementById("DivInvalidstreetname").style.display = "none";
                        $("#chkaddval").val("1");
                    }
                    else {
                        document.getElementById("Divvalidstreetname").style.display = "none";
                        document.getElementById("DivInvalidstreetname").style.display = "block";
                        $("#chkaddval").val("0");
                    }
                }
            });
            var mydataval = $("#chkaddval").val();
            if (mydataval == "1") {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }
        function postalvalidatestreetAddress(source, args) {
            var streetname = $("#<%=txtPostalformbaystreetname.ClientID %>").val();
            $.ajax({
                type: "POST",
                //action:"continue.aspx",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        document.getElementById("PostalDivvalidstreetname").style.display = "block";
                        document.getElementById("PostalDivInvalidstreetname").style.display = "none";
                        $("#chkaddval1").val("1");
                    }
                    else {
                        document.getElementById("PostalDivvalidstreetname").style.display = "none";
                        document.getElementById("PostalDivInvalidstreetname").style.display = "block";
                        $("#chkaddval1").val("0");
                    }
                }
            });
            var mydataval1 = $("#chkaddval1").val();
            if (mydataval1 == "1") {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
        }
        function ChkFun(source, args) {
            if (document.getElementById('<%= chkformbayid.ClientID %>') != null) {
                if (document.getElementById('<%= chkformbayid.ClientID %>').checked) {
                    validateAddress();
                    var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

                    if (elem == '1') {
                        args.IsValid = true;
                    }
                    else {

                        args.IsValid = false;
                    }
                }
                else {
                    args.IsValid = true;
                }
            }
        }
        function ChkFun1(source, args) {

            if (document.getElementById('<%= chkformbayid.ClientID %>') != null) {
                if (document.getElementById('<%= chkformbayid.ClientID %>').checked) {
                    validateAddress1();
                    var elem = document.getElementById('<%= hndaddress1.ClientID %>').value;

                    if (elem == '1') {
                        args.IsValid = true;
                    }
                    else {

                        args.IsValid = false;
                    }
                }
                else {
                    args.IsValid = true;
                }
            }
        }
        //function Scroll() {
        //    $(".divShowing").gridviewScroll();
        //}
        //function loader() {
        //    $(window).load(function () {
        //        $(".row m-b-md").fadeOut("slow");
        //    });
        //}
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });

            ChkFun1();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //gridviewScroll();
        });
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
        function dodropdown() {
            $(".ddlval").select2({
                placeholder: "select",
                allowclear: true
            });
            $(".ddlval1").select2({
                minimumResultsForSearch: -1
            });
        }
        function doMyAction() {
            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });

            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
        }
        //function gridviewScroll() {
        //   $('#<%=GridView1.ClientID%>').gridviewScroll({
        //        width: $("#content").width() - 42,
        //         height: 6000,
        //         freezesize: 0
        //     });
        // }
        // $("#nav").on("click", "a", function () {
        //     $('#content').animate({ opacity: 0 }, 500, function () {
        //         gridviewScroll();
        //         $('#content').delay(250).animate({ opacity: 1 }, 500);
        //     });
        // });

    </script>
    <style>
        .modalbackground {
            background-color: Gray;
            opacity: 0.5;
            filter: Alpha(opacity=50);
        }

        .modalpopup {
            background-color: white;
            padding: 6px 6px 6px 6px;
        }

        table.formtable h3 {
            color: #800000;
            font-size: 16px;
        }
    </style>
    <style type="text/css">
        .modalPopup {
            background-color: #696969;
            filter: alpha(opacity=40);
            opacity: 0.7;
            xindex: -1;
        }

        .z_index_loader {
            z-index: 999999!important;
        }
    </style>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
       </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <%--<asp:PostBackTrigger ControlID="lnkAdd" />--%>
            <%--<asp:PostBackTrigger ControlID="btnClearAll" />--%>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
            <asp:PostBackTrigger ControlID="btnUpdateAddressData" />
            <%--<asp:PostBackTrigger ControlID="lnkfirst" />
            <asp:PostBackTrigger ControlID="lnkprevious" />
            <asp:PostBackTrigger ControlID="lnknext" />
            <asp:PostBackTrigger ControlID="lnklast" />
            <asp:PostBackTrigger ControlID="rptpage" />--%>
        </Triggers>

    </asp:UpdatePanel>
    <script>
        var focusedElementId = "";
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        function BeginRequestHandler(sender, args) {
            var popup = $find('<%= newmodalpopup.ClientID %>');
            if (popup != null) {
                popup.show();
            }
            var fe = document.activeElement;
            if (fe != null) {
                focusedElementId = fe.id;
            } else {
                focusedElementId = "";
            }
        }
        function EndRequestHandler(sender, args) {
            //hide the modal popup - the update progress
            var popup = $find('<%= newmodalpopup.ClientID %>');
                    if (popup != null) {
                        popup.hide();
                    }
                    if (args.get_error() != undefined) {
                        args.set_errorhandled(true);
                    }
                    if (focusedElementId != "") {
                        $("#" + focusedElementId).focus();
                    }
                }
                prm.add_pageLoaded(pageLoaded);
                prm.add_beginRequest(BeginRequestHandler);
                prm.add_endRequest(EndRequestHandler);
                function pageLoaded() {
                    $(".ddlval").select2({
                        placeholder: "select",
                        allowclear: true
                    });
                    $(".ddlval1").select2({
                        minimumResultsForSearch: -1
                    });
                    CompanyAddress();
                    //gridviewScroll();
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$(".ddlval1").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    $('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    });
                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    });

                    $("[data-toggle=tooltip]").tooltip();

                    //gridviewScroll();
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptName] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptName] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptMobile] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptMobile] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptPhone] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptPhone] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptEmail] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptEmail] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptaddress] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptaddress] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                    ////alert($(".search-select").attr("class"));
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$("[data-toggle=tooltip]").tooltip();
                    //$("searchbar").attr("imgbtn");
                    //gridviewScroll();

                    //doMyAction(); 
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$(".ddlval1").select2({
                    //    minimumResultsForSearch: -1
                    //});
                }


    </script>

    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black managecompanyttl">Manage Call Entry
            <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
            </h3>
            <div class="contactsarea">
                <div class="addcontent" id="divAddCompany" runat="server">
                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-default btn-rounded addcontact"
                        OnClick="lnkAdd_Click"><i></i>Add</asp:LinkButton>
                    <asp:LinkButton CssClass="btn lineheithnone" ID="lnkBack" runat="server" CausesValidation="false"
                        OnClick="lnkBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkclose" runat="server" CssClass="btn lineheithnone" CausesValidation="false" Visible="false"
                        OnClick="lnkclose_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                </div>
                <div id="divleft" runat="server">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server"><i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong> </div>
                        <div class="alert alert-danger" id="PanError" runat="server"><i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong> </div>
                        <div class="alert alert-info" id="PanAlreadExists" runat="server"><i class="icon-info-sign"></i><strong>&nbsp;Record with this name already exists.</strong> </div>
                        <div class="alert alert-info" id="PAnAddress" runat="server" visible="false"><i class="icon-info-sign"></i><strong>&nbsp;Record with this Address already exists.</strong> </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server"><i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong> </div>
                        <div class="icon-remove-sign" id="divTeamTime" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Appointment Time over.</strong> </div>
                    </div>
                    <div class="contactbottomarea " id="PanAddUpdate" runat="server" visible="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default" style="border-radius: 0px; border-top: none;">
                                    <div class="panel-body">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4 id="Haddcomp" runat="server">Add New Record</h4>
                                                    <div class="graybgarea">
                                                        <div class="form-group spicaldivin" id="divD2DEmployee" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Employee<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlD2DEmployee" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlD2DEmployee_SelectedIndexChanged"
                                                                    aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                    <asp:ListItem Value="">Employee</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                    ControlToValidate="ddlD2DEmployee" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divD2DAppDate" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Appointment Date<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtD2DAppDate" runat="server" Width="100px" CssClass="form-control"></asp:TextBox></td>
                                                                        <td>
                                                                            <asp:ImageButton ID="Image20" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" /></td>
                                                                    </tr>
                                                                </table>
                                                                <cc1:CalendarExtender ID="CalendarExtender20" runat="server" PopupButtonID="Image20"
                                                                    TargetControlID="txtD2DAppDate" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ValidationGroup="company1" ControlToValidate="txtD2DAppDate" ID="RegularExpressionValidator6" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                    ValidationGroup="company1" ControlToValidate="txtD2DAppDate"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divD2DAppTime" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Appointment Time<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlAppTime" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                    <asp:ListItem Value="">Time</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divMrMs" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">Name<span class="symbol required"></span> </label>
                                                            </span><span style="width: 70%!important;">
                                                                <div class="onelindiv">
                                                                    <span class="mrdiv">
                                                                        <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                            placeholder="Salutation"></asp:TextBox>
                                                                    </span><span class="fistname">
                                                                        <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                            placeholder="First Name" AutoPostBack="true" OnTextChanged="txtContFirst_OnTextChanged"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="company1" Style="width: 100%!important;"></asp:RequiredFieldValidator>
                                                                    </span><span class="lastname">
                                                                        <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                            placeholder="Last Name" OnTextChanged="txtContLast_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    </span>
                                                                </div>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divCompany" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Company<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtCompany" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtCompany" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                          <div class="form-group spicaldivin">
                                                                <span class="name">
                                                                    <label class="control-label">Phone </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="10" CssClass="form-control"
                                                                        OnTextChanged="txtCustPhone_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                                        ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                                        ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator></span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        <div class="form-group spicaldivin" id="divMobile" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">Mobile </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"
                                                                    OnTextChanged="txtContMobile_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                                    ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                                    ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divEmail" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">Email <span class="symbol required"></span></label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtContEmail" runat="server" MaxLength="100" CssClass="form-control"
                                                                    OnTextChanged="txtContEmail_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                                    ValidationGroup="company1" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                           
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin textareabox" style="margin-bottom: 15px;">
                                                            <span class="name">
                                                                <label class="control-label">Enquiry Reason</label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtCustNotes" runat="server" TextMode="MultiLine" CssClass="form-control areaheight" Width="350px"></asp:TextBox>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Alt Phone </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtCustAltPhone" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtContMobile"
                                                                    ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number"
                                                                    ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="divCustType" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Type <span class="symbol required"></span></label>
                                                            </span><span>
                                                                <div>
                                                                    <asp:DropDownList ID="ddlCustTypeID" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                        ControlToValidate="ddlCustTypeID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin" id="div1" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">Solar Type </label>
                                                            </span><span>
                                                                <div class="customradio">
                                                                    <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                                    <label for="<%=rblResCom1.ClientID %>" style="width: 60px;"><span></span>Res&nbsp; </label>
                                                                    <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                                    <label for="<%=rblResCom2.ClientID %>" style="width: 60px;"><span></span>Com&nbsp; </label>
                                                                </div>
                                                                <%--<div class="radio i-checks">
                                                                            <asp:RadioButtonList ID="rblResCom" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="1" Selected="True">Res&nbsp;</asp:ListItem>
                                                                                <asp:ListItem Value="2">Com</asp:ListItem>
                                                                            </asp:RadioButtonList>


                                                                        </div>--%>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>

                                                    </div>
                                                    <div class="graybgarea">
                                                    </div>
                                                    <div class="graybgarea" runat="server" id="divformbayaddress" visible="false">
                                                        <div class="form-group spicaldivin textareabox" style="margin-bottom: 15px;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td style="padding-right: 4px;">
                                                                        <asp:CheckBox ID="chkformbayid" runat="server" Checked="false" OnCheckedChanged="chkformbayid_CheckedChanged"
                                                                            AutoPostBack="true" />
                                                                        <label for="<%=chkformbayid.ClientID %>"><span></span></label>
                                                                    </td>
                                                                    <td>Is Frombay Address </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4 id="Haddcompadd" runat="server">Add Company Address</h4>
                                                    <div class="graybgarea">
                                                        <div id="Div2" class="form-group spicaldivin streatfield" visible="false" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">Street Address Line<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <asp:HiddenField ID="hndaddress" runat="server" />
                                                                <asp:TextBox ID="txtstreetaddressline" runat="server" CssClass="form-control" name="address" onblur="getParsedAddress();"></asp:TextBox>
                                                                <%--AutoPostBack="true" OnTextChanged="txtstreetaddressline_TextChanged"--%>
                                                                <asp:RequiredFieldValidator ID="rfvstreetaddressline" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                    ControlToValidate="txtstreetaddressline" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                    ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                    ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                            </span>
                                                            <asp:Label ID="lblexistame" runat="server" Visible="false"></asp:Label>
                                                            <asp:HiddenField ID="hndstreetno" runat="server" />
                                                            <asp:HiddenField ID="hndstreetname" runat="server" />
                                                            <asp:HiddenField ID="hndstreettype" runat="server" />
                                                            <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                            <asp:HiddenField ID="hndunittype" runat="server" />
                                                            <asp:HiddenField ID="hndunitno" runat="server" />
                                                            <div id="validaddressid" style="display: none">
                                                                <img src="../../../images/check.png" alt="check">Address is valid.
                                                            </div>
                                                            <div id="invalidaddressid" style="display: none">
                                                                <img src="../../../images/x.png" alt="cross">
                                                                Address is invalid.
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="form-group spicaldivin streatfield">
                                                            <span class="name">
                                                                <label class="control-label">Street Address<span class="symbol required"></span> </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtStreetAddress" runat="server" CssClass="form-control" name="address" Enabled="true"></asp:TextBox>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtStreetAddress" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="row" runat="server" visible="false">
                                                            <div class="form-group spicaldivin streatfield col-md-6" visible="false" id="divUnitno" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Unit No<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtformbayUnitNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="txtformbayUnitNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin streatfield col-md-6" visible="false" id="divunittype" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Unit Type<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                        <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row" runat="server" visible="false">
                                                            <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divStreetno" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Street No<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtformbayStreetNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divstname" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Street Name<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <%--<asp:DropDownList ID="ddlformbaystreetname" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                                <asp:ListItem Value="">Street Name</asp:ListItem>
                                                                            </asp:DropDownList>--%>
                                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                        ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                        ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                                    <div id="Divvalidstreetname" style="display: none">
                                                                        <img src="../../../images/check.png" alt="check">Address is valid.
                                                                    </div>
                                                                    <div id="DivInvalidstreetname" style="display: none">
                                                                        <img src="../../../images/x.png" alt="cross">
                                                                        Address is invalid.
                                                                    </div>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divsttype" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Street Type<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                        <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group spicaldivin streatfield col-md-4">
                                                                <span class="name">
                                                                    <label class="control-label">Street City<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="ddlStreetCity" runat="server" CssClass="form-control" OnTextChanged="ddlStreetCity_OnTextChanged" AutoPostBack="true"
                                                                        MaxLength="50" Enabled="true"></asp:TextBox>
                                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                    <cc1:AutoCompleteExtender ID="AutoCompletestreet" MinimumPrefixLength="2" runat="server"
                                                                        UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin streatfield col-md-4">
                                                                <span class="name">
                                                                    <label class="control-label">Street State<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtStreetState" runat="server" MaxLength="10" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin streatfield col-md-4">
                                                                <span class="name">
                                                                    <label class="control-label">street post code<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                                        Enabled="false"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="regularexpressionvalidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                        ValidationGroup="company1" Display="dynamic" ErrorMessage="please enter a number"
                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    <%--  <asp:RequiredFieldValidator ID="requiredfieldvalidator6" runat="server" ErrorMessage="this value is required." CssClass="comperror"
                                                                                ValidationGroup="company1" ControlToValidate="txtStreetPostCode" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            </div>
                                                            <div class="form-group spicaldivin marginbtmnone">
                                                                <span class="name">
                                                                    <label class="control-label">Source<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <div class="marginbtm10">
                                                                        <asp:DropDownList ID="ddlCustSourceID" runat="server" AppendDataBoundItems="true"
                                                                            AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="ddlval" OnSelectedIndexChanged="ddlCustSourceID_SelectedIndexChanged">
                                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="ddlCustSourceID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                    </div>
                                                                    <div>
                                                                        <asp:DropDownList ID="ddlCustSourceSubID" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="ddlval" Visible="false">
                                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </span>

                                                            </div>

                                                          








                                                       
                                                        <asp:Panel runat="server" Visible="false">
                                                            <div class="form-group spicaldivin">
                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td style="padding-right: 4px;">
                                                                            <asp:CheckBox ID="chkSameasAbove" runat="server" OnCheckedChanged="chkSameasAbove_CheckedChanged"
                                                                                AutoPostBack="true" />
                                                                            <label for="<%=chkSameasAbove.ClientID %>"><span></span></label>
                                                                        </td>
                                                                        <td>Same as street address </td>
                                                                        <%--<asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdateAddress" runat="server" OnClick="btnUpdateAddress_Click"
                                                                                    Text="Update Address" />--%>
                                                                        <%--<td runat="server" id="tdupdateaddress" visible="false">
                                                                                <asp:ImageButton ID="btnUpdateAddress" runat="server"
                                                                                    OnClick="btnUpdateAddress_Click" ImageUrl="~/images/icon_updateaddress.png" /></td>--%>
                                                                    </tr>
                                                                </table>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div id="Div3" class="form-group spicaldivin" runat="server" visible="false">
                                                                <span class="name">
                                                                    <label class="control-label">Postal Address Line </label>
                                                                </span><span>
                                                                    <asp:HiddenField ID="hndaddress1" runat="server" />
                                                                    <asp:TextBox ID="txtpostaladdressline" runat="server" MaxLength="50" CssClass="form-control" onfocus="SetTextInPostalAddress()" name="address" onblur="getParsedAddress1();"> </asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvpostaladdress" runat="server" ErrorMessage="* Required" Enabled="false"
                                                                        ControlToValidate="txtpostaladdressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                                                    <asp:CustomValidator ID="Custompostaladdress" runat="server"
                                                                        ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                        ClientValidationFunction="ChkFun1"></asp:CustomValidator>
                                                                </span>
                                                                <asp:HiddenField ID="hndstreetno1" runat="server" />
                                                                <asp:HiddenField ID="hndstreetname1" runat="server" />
                                                                <asp:HiddenField ID="hndstreettype1" runat="server" />
                                                                <asp:HiddenField ID="hndstreetsuffix1" runat="server" />
                                                                <asp:HiddenField ID="hndunittype1" runat="server" />
                                                                <asp:HiddenField ID="hndunitno1" runat="server" />
                                                                <div id="validaddressid1" style="display: none">
                                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                                </div>
                                                                <div id="invalidaddressid1" style="display: none">
                                                                    <img src="../../../images/x.png" alt="cross">
                                                                    Address is invalid.
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin">
                                                                <span class="name">
                                                                    <label class="control-label">Postal Address </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtPostalAddress" runat="server" Enabled="false" MaxLength="50" CssClass="form-control" onfocus="SetTextInPostalAddress()" name="address"></asp:TextBox>
                                                                    <div class="clear"></div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group spicaldivin streatfield col-md-6" visible="false" id="divPostalformbayUnitNo" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Unit No<span class="symbol required"></span> </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtPostalformbayUnitNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="txtPostalformbayUnitNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spicaldivin streatfield col-md-6" visible="false" id="divPostalformbayunittype" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Unit Type<span class="symbol required"></span> </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlPostalformbayunittype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ControlToValidate="ddlPostalformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divPostalformbayStreetNo" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Street No<span class="symbol required"></span> </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtPostalformbayStreetNo" runat="server" CssClass="form-control" name="address"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtPostalformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divPostalformbaystreetname" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Street Name<span class="symbol required"></span> </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtPostalformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--<asp:DropDownList ID="ddlPostalformbaystreetname" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                                <asp:ListItem Value="">Street Name</asp:ListItem>
                                                                            </asp:DropDownList>--%>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="txtPostalformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender11" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtPostalformbaystreetname" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                            ClientValidationFunction="PostalvalidatestreetAddress" ControlToValidate="txtPostalformbaystreetname"></asp:CustomValidator>
                                                                        <div id="PostalDivvalidstreetname" style="display: none">
                                                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                                                        </div>
                                                                        <div id="PostalDivInvalidstreetname" style="display: none">
                                                                            <img src="../../../images/x.png" alt="cross">
                                                                            Address is invalid.
                                                                        </div>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spicaldivin streatfield col-md-4" visible="false" id="divpostalformbaystreettype" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Street Type<span class="symbol required"></span> </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlPostalformbaystreettype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                            ControlToValidate="ddlPostalformbaystreettype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group spicaldivin streatfield col-md-4">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal City </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="ddlPostalCity" runat="server" CssClass="form-control" AutoPostBack="true"
                                                                            OnTextChanged="ddlPostalCity_OnTextChanged" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteSearch" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="ddlPostalCity" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spicaldivin streatfield col-md-4">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal State </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtPostalState" runat="server" MaxLength="10" CssClass="form-control"
                                                                            Enabled="false"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group spicaldivin streatfield col-md-4">
                                                                    <span class="name">
                                                                        <label class="control-label">Postal Post Code </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtPostalPostCode" runat="server" MaxLength="10" CssClass="form-control"
                                                                            Enabled="false"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                                            ValidationGroup="company1" ControlToValidate="txtPostalPostCode" Display="Dynamic"
                                                                            ErrorMessage="Please enter a number" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group spicaldivin">
                                                                <span class="name">
                                                                    <label class="control-label">Area<span class="symbol required"></span> </label>
                                                                </span><span>
                                                                    <%--<div class="radio margintopnone">
                                                                            <asp:RadioButtonList ID="rblArea" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ValidationGroup="company1" ControlToValidate="rblArea" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        </div>--%>
                                                                    <div class="customradio">
                                                                        <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                                        <label for="<%=rblArea1.ClientID %>" style="width: 90px;"><span></span>Metro&nbsp; </label>
                                                                        <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                                        <label for="<%=rblArea2.ClientID %>" style="width: 90px;"><span></span>Regional&nbsp; </label>
                                                                    </div>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin">
                                                                <span class="name">
                                                                    <label class="control-label">Country </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtCountry" runat="server" Enabled="false" MaxLength="50" Text="Australia"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin">
                                                                <span class="name">
                                                                    <label class="control-label">Website </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtCustWebSiteLink" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group spicaldivin marginbtmnone">
                                                                <span class="name">
                                                                    <label class="control-label">Fax </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtCustFax" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 textcenterbutton paddtopbottom5px">
                                                <asp:Button CssClass="btn btn-primary addwhiteicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" ValidationGroup="company1" />
                                                <asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                    Text="Save" Visible="false" ValidationGroup="company1" />
                                                <asp:Button CssClass="btn btn-purple resetbutton" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                    CausesValidation="false" Text="Reset" />
                                                <asp:Button CssClass="btn btn-dark-grey calcelwhiteicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                        <ProgressTemplate>
                            <asp:Image ID="imgloading" ImageUrl="~/admin/images/loading.gif" Style="z-index: 1000001111;" AlternateText="processing"
                                runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                        PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />
                    <div class="clear"></div>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                        <div class="contacttoparea" id="PanSearch" runat="server" visible="false">
                            <span id="searchbar" runat="server" class="imgbtn"></span>
                            <div class="toppaneldiv" id="hidesearch" runat="server">
                                <div class="form-inline">
                                    <div class="contactbrmbtm padd10all">
                                        <div class="form-group" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                <asp:ListItem Value="">Type</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                <asp:ListItem Value="1">Residential</asp:ListItem>
                                                <asp:ListItem Value="2">Commercial</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlSearchArea" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                <asp:ListItem Value="">Area</asp:ListItem>
                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group withspc1">
                                            <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true" 
                                                aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                <asp:ListItem Value="">Source</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group withspc1" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                <asp:ListItem Value="">Sub Source</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" runat="server" visible="false">
                                            <asp:TextBox ID="txtSearchCompanyNo" runat="server" CssClass="form-control" placeholder="CompanyNumber"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtSearchCompanyNo"
                                                WatermarkText="CompanyNumber" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                aria-controls="DataTables_Table_0" class="ddlval">
                                                <asp:ListItem Value="">State</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group spical" id="employeeall" runat="server">
                                            <asp:DropDownList ID="ddlsearchemployee" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="ddlval">
                                                <asp:ListItem Value="">Select Employee</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" placeholder="Company Name"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                WatermarkText="Company Name" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtSearchStreet" runat="server" Width="150px" CssClass="form-control" placeholder="Street"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtSearchStreet"
                                                WatermarkText="Street" />
                                        </div>
                                    </div>
                                    <div class="contactbrmbtm padd10all">


                                        <div class="form-group">
                                            <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control" placeholder="City"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSerachCity"
                                                WatermarkText="City" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                            </cc1:AutoCompleteExtender>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control" placeholder="Post Code"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchPostCode"
                                                WatermarkText="Post Code" />
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                            </cc1:AutoCompleteExtender>
                                        </div>
                                        <div class="form-group">
                                            <table class="datedpikartable">
                                                <tr>
                                                    <td class='form-group date' id='datetimepicker9'>
                                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" placeholder="Start Date"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtStartDate"
                                                            WatermarkText="Start Date" />
                                                    </td>
                                                    <td class="calendericonarea">
                                                        <asp:ImageButton ID="Img1" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                        <cc1:CalendarExtender ID="calCheckIn" runat="server" PopupButtonID="Img1" TargetControlID="txtStartDate"
                                                            Format="dd/MM/yyyy">
                                                        </cc1:CalendarExtender>
                                                        <cc1:MaskedEditExtender ID="mskeditCheckIn" runat="server" Mask="99/99/9999" MessageValidatorTip="true"
                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="date"
                                                            TargetControlID="txtStartDate" CultureName="en-GB">
                                                        </cc1:MaskedEditExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <table class="datedpikartable">
                                                <tr>
                                                    <td class='form-group date' id='Td1'>
                                                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" placeholder="End Date"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtEndDate"
                                                            WatermarkText="End Date" />
                                                    </td>
                                                    <td class="calendericonarea">
                                                        <asp:ImageButton ID="Image1" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                                                            TargetControlID="txtEndDate" Format="dd/MM/yyyy">
                                                        </cc1:CalendarExtender>
                                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                            MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                            MaskType="date" TargetControlID="txtEndDate" CultureName="en-GB">
                                                        </cc1:MaskedEditExtender>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <span>
                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-danger btnserchicon"
                                                    CausesValidation="false" OnClick="btnSearch_Click"><img src="../../../images/icon_serach.png" Width="17" Height="16"/> Search</asp:LinkButton>
                                            </span>
                                        </div>
                                        <div class="form-group" style="padding-top: 3px; display: inline-block;">
                                            <asp:ImageButton ID="btnClearAll" runat="server" Width="84" Height="30" CssClass="marginleft5" ImageUrl="~/images/reload.png"
                                                OnClick="btnClearAll_Click" />
                                        </div>
                                    </div>
                                    <div class="padd10all">
                                        <div class="leftarea1">
                                            <div class="showenteries">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>Show</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" Width="200px" CssClass="ddlval1">
                                                            </asp:DropDownList></td>
                                                        <td>entries</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div style="text-align: right;">
                                            <div class="form-group">
                                                <div class="buttonarea">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td id="tdExport" runat="server" width="20px" style="padding-right: 7px; vertical-align: middle;">
                                                                <asp:LinkButton ID="lbtnExport" ForeColor="brown" Font-Underline="false" runat="server" CausesValidation="false"
                                                                    OnClick="lbtnExport_Click" Visible="false"><img src="../../../images/icon_excl.png" data-toggle="tooltip"
                                                                        data-placement="top" title="Excel file"/></asp:LinkButton></td>
                                                            <td valign="top" id="tdAll1" class="paddtop3td" runat="server">&nbsp;
                                    <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                                <label for="<%=chkViewAll.ClientID %>"><span></span></label>
                                                            </td>
                                                            <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="contactbottomarea minheight400">
                        <div class="marginnone">
                            <div class="tableblack minheight400">
                                <div class="table-responsive" id="PanGrid" runat="server">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" OnRowCommand="GridView1_OnRowCommand"
                                        OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true"
                                        OnRowDeleting="GridView1_RowDeleting" OnSorting="GridView1_Sorting"
                                        OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated" CssClass="Gridview">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                        <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                    <asp:Label ID="Label2" runat="server" Width="200px"><%#Eval("Customer")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                          
                                            <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="100px"><%#Eval("CustPhone")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Enquiry Reason" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'
                                                        Width="180px"><%#Eval("CustNotes")%> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Sales Rep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="180px"><%#Eval("AssignedTo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Convert">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label112" runat="server" Width="20px" CssClass="btnicondelete" >
                                                        <asp:ImageButton ID="gvbtnConvert" runat="server" CommandName="Convert" CommandArgument='<%#Eval("CustomerID") %>' Visible='<%#Eval("convertflag").ToString()=="False"?true:false %>'   OnClientClick="return confirm('Are you sure want to Convert this Record?  ');" ImageUrl="../../../images/icon_addcontact.png"
                                                            CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Convert" />
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Width="20px" CssClass="btnicondelete">
                                                        <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select" ImageUrl="../../../images/icon_edit.png"
                                                            CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server" Width="40px" CssClass="btnicondelete">
                                                        <asp:ImageButton ID="gvbtnDelete" runat="server" CommandName="Delete" OnClientClick="return confirm('Are you sure want to delete this Record?  ');"
                                                            ImageUrl="~/admin/images/icons/icon_delet.png" CausesValidation="false" data-original-title="Delete" data-toggle="tooltip" data-placement="top" />
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr class="GridviewScrollItem">
                                                                        <td width="180px"><b>Company Number</b></td>
                                                                        <td runat="server" id="tdcompnum">
                                                                            <asp:Label ID="Label1" runat="server" Width="130px"> <%#Eval("CompanyNumber")%> </asp:Label></td>
                                                                        <td width="180px" runat="server" id="tdassignto"><b>Assigned To</b></td>
                                                                        <td runat="server" id="tdassignto1">
                                                                            <asp:Label ID="Label7" runat="server" Width="130px"><%#Eval("AssignedTo")%> </asp:Label></td>
                                                                    </tr>
                                                                    <tr runat="server" id="tdtype">
                                                                        <td><b>Type</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label8" runat="server" Width="100px"><%#Eval("CustType")%> </asp:Label></td>
                                                                        <td><b>Res Com</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblres" runat="server" Width="100px"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Email</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblEmail" runat="server" Width="100px"><%#Eval("ContEmail")%></asp:Label></td>
                                                                        <td><b>Notes</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblNotes" runat="server" Width="100px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label9" runat="server" Width="100px"><%#Eval("CustSource")%> </asp:Label></td>
                                                                        <td><b>Sub Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblSubSource" runat="server" Width="100px"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#EFF3FB" />
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <RowStyle BackColor="#EFF3FB" />
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <%--<PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </PagerTemplate>--%>
                                    </asp:GridView>
                                    <%--<b id="divShowing" runat="server">
                                                <asp:Literal ID="ltrPage" runat="server"></asp:Literal></b>--%>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label></td>
                                            <td style="float: right;">
                                                <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="pagebtndesign firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="pagebtndesign" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>divleft
                </div>
                <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                <div class="bodymianbg">
                    <div class="row">
                        <div class="col-md-12" id="divright" runat="server" visible="false">
                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                AutoPostBack="true">
                                <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Summary">
                                    <ContentTemplate>
                                        <div class="table-responsive">
                                            <uc1:companysummary ID="companysummary1" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Contact">
                                    <ContentTemplate>
                                        <uc2:contacts ID="contacts1" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="TabProject" runat="server" HeaderText="Project">
                                    <ContentTemplate>
                                        <uc3:project ID="project1" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="TabInfo" runat="server" HeaderText="Follow Ups">
                                    <ContentTemplate>
                                        <uc4:info ID="info1" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="TabConversation" runat="server" HeaderText="Conversation">
                                    <ContentTemplate>
                                        <uc5:conversation ID="conversation1" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                            </cc1:TabContainer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderName" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
    </cc1:ModalPopupExtender>
    <div id="divName" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Duplicate Name</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                            summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>The following Location(s) have a<br />
                                            similar name. Check for Duplicates.</b> </h3>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Button ID="btnDupeName" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeName_Onclick"
                                            Text="Dupe" CausesValidation="false" />
                                        <asp:Button ID="btnNotDupeName" runat="server" OnClick="btnNotDupeName_Onclick" CausesValidation="false"
                                            CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                        <asp:Button ID="btnOKName" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="rptName" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptName_PageIndexChanging" ShowFooter="true"
                                    PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Contacts">
                                            <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                    <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                    <EmptyDataRowStyle Font-Bold="True" />
                                    <RowStyle CssClass="GridviewScrollItem" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULLData2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderMobile" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divMobileCheck"
        OkControlID="btnOKMobile" TargetControlID="btnNULLData2">
    </cc1:ModalPopupExtender>
    <div id="divMobileCheck" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="H1">Duplicate Mobile</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                            summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>There is a Contact in the database already who has this mobile number.
                                                    <br />
                                            This looks like a Duplicate Entry.</b> </h3>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                            Text="Dupe" CausesValidation="false" />
                                        <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                        <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="rptMobile" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptMobile_PageIndexChanging"
                                    PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Contacts">
                                            <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                    <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                    <EmptyDataRowStyle Font-Bold="True" />
                                    <RowStyle CssClass="GridviewScrollItem" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULLDataPhone" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderPhone" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divPhoneCheck"
        OkControlID="btnOKPhone" TargetControlID="btnNULLDataPhone">
    </cc1:ModalPopupExtender>
    <div id="divPhoneCheck" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="H2">Duplicate Phone</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                            summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>There is a Customer in the database already who has this phone number.
                                                    <br />
                                            This looks like a Duplicate Entry.</b></h3>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Button ID="btnDupePhone" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupePhone_Onclick"
                                            Text="Dupe" CausesValidation="false" />
                                        <asp:Button ID="btnNotDupePhone" runat="server" OnClick="btnNotDupePhone_Onclick"
                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                        <asp:Button ID="btnOKPhone" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="rptPhone" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptPhone_PageIndexChanging"
                                    PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer">
                                            <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("CustPhone")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                    <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                    <EmptyDataRowStyle Font-Bold="True" />
                                    <RowStyle CssClass="GridviewScrollItem" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderEmail" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divEmailCheck"
        OkControlID="btnOKEmail" TargetControlID="btnNULLData3">
    </cc1:ModalPopupExtender>
    <div id="divEmailCheck" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="H3">Duplicate Email</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>There is a Contact in the database already who has this email address.
                                                    <br />
                                            This looks like a Duplicate Entry.</b></h3>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:Button ID="btnDupeEmail" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeEmail_Onclick"
                                            Text="Dupe" CausesValidation="false" />
                                        <asp:Button ID="btnNotDupeEmail" runat="server" OnClick="btnNotDupeEmail_Onclick"
                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                        <asp:Button ID="btnOKEmail" Style="display: none; visible: false;" runat="server"
                                            CssClass="btn" Text=" OK " /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="rptEmail" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                    PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Contacts">
                                            <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="Button2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
        OkControlID="btnOKAddress" TargetControlID="Button2">
    </cc1:ModalPopupExtender>
    <div id="divAddressCheck" runat="server" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                     <div style="float: right">
                    <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                        Close
                    </button>
                         </div>
                    <h4 class="modal-title" id="H4">Duplicate Address</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        <h3 class="noline"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                            This looks like a Duplicate Entry.</b></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="tablescrolldiv">
                            <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                <asp:GridView ID="rptaddress" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptaddress_PageIndexChanging"
                                    PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customers">
                                            <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <%--                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate><%# Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnupdateaddress" />
    <asp:Button ID="Button1" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="Button1">
    </cc1:ModalPopupExtender>
    <div runat="server" id="div_popup" class="modalpopup" align="center" style="width: auto;">
        <div class="popupdiv">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading pad15">
                            <asp:Label runat="server" ID="lbltitle"></asp:Label>
                            <div class="panel-tools">
                                <div class="closbtn">
                                    <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                        <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="graybgarea">
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Unit Type </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlUnitType" runat="server" AppendDataBoundItems="true"
                                                    CssClass="form-control search-select">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Unit No </label>
                                            </span><span>
                                                <asp:TextBox ID="txtUnitNo"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Street No </label>
                                            </span><span>
                                                <asp:TextBox ID="txtstreetno"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Street Name </label>
                                            </span><span>
                                                <asp:TextBox ID="txtStreetName"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Street Type </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlstreetType" runat="server" AppendDataBoundItems="true"
                                                    CssClass="form-control search-select">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">City </label>
                                            </span><span>
                                                <asp:TextBox ID="txtCity"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">State </label>
                                            </span><span>
                                                <asp:TextBox ID="txtState"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Post Code </label>
                                            </span><span>
                                                <asp:TextBox ID="txtpostcode"
                                                    runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">&nbsp; </label>
                                            </span><span>
                                                <asp:Button class="btn btn-primary" ID="btnUpdateAddressData" runat="server" OnClick="btnUpdateAddressData_Click"
                                                    CausesValidation="true" Text="Save" />
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .modalbackground {
            background-color: Gray;
            opacity: 0.5;
            filter: Alpha(opacity=50);
        }

        .modalpopup {
            background-color: white;
            padding: 6px 6px 6px 6px;
        }

        table.formtable h3 {
            color: #800000;
            font-size: 16px;
        }
    </style>
    <style type="text/css">
        .modalPopup {
            background-color: #696969;
            filter: alpha(opacity=40);
            opacity: 0.7;
            xindex: -1;
        }
    </style>
    </span></span>
      <asp:HiddenField runat="server" ID="hdncountdata" />
 
</asp:Content>

