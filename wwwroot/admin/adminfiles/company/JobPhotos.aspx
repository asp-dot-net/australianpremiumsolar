﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JobPhotos.aspx.cs" Inherits="admin_adminfiles_company_JobPhotos"
    Culture="en-GB" UICulture="en-GB" MasterPageFile="~/admin/templates/MasterPageAdmin.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            <%--$('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });--%>
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>
    <style>
        .nowrap {
            white-space: normal !important;
        }

        .tooltip {
            z-index: 999999;
        }
    </style>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
            }
        }

        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-add-user icon"></i>Job Photos</h5>

        <%--<div id="hbreadcrumb">
            <a href="#" class="btn btn-info btngray"><i class="fa fa-upload"></i>Upload Leads</a>
            <div id="tdExport" class="pull-right" runat="server">
            </div>
        </div>--%>
    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.custom-file-input').on('change', function (e) {
                var fileName = e.target.files[0].name;
                //  var fileName = document.getElementsByClassName("fileupload").files[0].name;
                //alert(fileName);
                $(this).next('.form-control-file').addClass("selected").html(fileName);
            });


        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {

        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });


            //$("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $(".myvalemployee").select2({
                allowclear: true,
                //minimumResultsForSearch: -1
            });

        }

    </script>
    <style>
        .SelectionHeight .select2-selection.select2-selection--multiple {
            height: 100px;
        }

        .select2-container .select2-selection--multiple .select2-selection__rendered {
            padding-top: 7px;
        }

        .Size{height :160px!important; width:160px!important}
    </style>

    <div class="page-body padtopzero formGrid">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="animate-panelmessesgarea padbtmzero">
                <div class="messesgarea">
                    <div class="alert alert-danger" id="PanEmpty" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Lead Source. </strong>
                    </div>
                </div>
            </div>

            <div class="searchfinal searchFilterSection">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                    <tr>
                                        <td class="">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtProjectNo" runat="server" Placeholder="Project No" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtProjectNo"
                                                            WatermarkText="Project No" />
                                                    </div>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtCustomer" runat="server" Placeholder="Customer" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtCustomer"
                                                            WatermarkText="Customer" />
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlPhoto" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value=0>Photo</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlSerialNo" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value=0>Serial No</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlDateType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">Booking Date</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                            aria-controls="DataTables_Table_0" class="myval">
                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group" style="width: 150px; padding-top: 3px">
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" CssClass="btn btn-success btn-xs Excel" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PanTotal" runat="server" CssClass="hpanel marbtm15" Visible="false">
            <div class="widget-body shadownone brdrgray">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header" style="padding-left: 18px">
                    <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" class="evenRowTable">
                                    <tr>
                                        <%--<asp:Repeater ID="rptTotal" runat="server">
                                            <ItemTemplate>
                                                <td class="paddingleftright10" style="font-size: medium">

                                                    <asp:Literal ID="lblVehicle" runat="server" Text='<%#Eval("VehicleType")%>'></asp:Literal>
                                                    :&nbsp;
                                                            <asp:Literal ID="lblTotalTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Literal>
                                                </td>
                                                <td></td>

                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                        <td class="paddingleftright10" style="font-size: medium">Vehicle Type :&nbsp;
                                            <asp:Literal ID="lblVehicle" runat="server"></asp:Literal>
                                            <%--:&nbsp;--%>

                                        </td>
                                        <td></td>
                                        <td class="paddingleftright10" style="font-size: medium">Total Trip :&nbsp;
                                            <asp:Literal ID="lblTotalTrip" runat="server"></asp:Literal>
                                            <%--:&nbsp;--%>

                                        </td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>

        <div class="fullWidthTable searchfinal customerTable searchbar mainGridTable main-card mb-3 card">

            <div class="finalgrid">
                <div class="padtopzero padrightzero">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server" class="wid100">
                            <div class="table-responsive xscroll leadtablebox">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo  text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand" OnDataBound="GridView1_DataBound"
                                    AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowCreated="GridView1_RowCreated">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px">
                                            <ItemTemplate>

                                                <%--<a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">--%>
                                                <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/arrowright.png" />
                                                <%--</a>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="ProjectNumber">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProjectNumber" runat="server">
                                                    
                                                    <%#Eval("ProjectNumber")%></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server"
                                                    data-placement="top" data-original-title='<%#Eval("Customer")%>' data-toggle="tooltip">
                                                    
                                                    <%#Eval("Customer")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Booking Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallBookingDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallBookingDate" runat="server">
                                                    
                                                    <%#Eval("InstallBookingDate")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallerName" runat="server">
                                                    
                                                    <%#Eval("InstallerName")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="SystemDetails">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVehicleType" runat="server">
                                                    
                                                    <%#Eval("SystemDetails")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Installed Complet Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallCompleted">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallCompleted" runat="server">
                                                    
                                                    <%#Eval("InstallCompleted")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Actions" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                            <ItemTemplate>
                                                <div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle noArrow"></button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList">
                                                        <asp:LinkButton ID="lbtnViewSerialNo" CommandName="ViewSerialNo" CommandArgument='<%#Eval("ProjectID") + ";" + Eval("ProjectNumber")%>'
                                                            CausesValidation="false" runat="server">
                                                               <i class="fa fa-eye"></i> View Serial No </asp:LinkButton>

                                                        <asp:LinkButton ID="lbtnViewPhoto" CommandName="ViewPhoto" CommandArgument='<%#Eval("ProjectID")%>'
                                                            CausesValidation="false" runat="server">
                                                               <i class="fa fa-eye"></i> View Photos </asp:LinkButton>

                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                            <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                <tr class="GridviewScrollItem">
                                                                    <td><b>Rego No</b></td>
                                                                    <td>
                                                                        <%--<asp:Label ID="Label7" runat="server">
                                                                                 <%#Eval("VehicleRegoNo").ToString() == "" ? "-" : Eval("VehicleRegoNo")%>
                                                                            </asp:Label>--%>
                                                                    </td>
                                                                    <td>
                                                                        <b>Driver</b>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:Label ID="Label8" runat="server">
                                                                                <%#Eval("Driver").ToString() == "" ? "-" : Eval("Driver")%>
                                                                            </asp:Label>--%>
                                                                    </td>
                                                                    <td>
                                                                        <b>Delivery Date</b>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:Label ID="Label1" runat="server">
                                                                                <%#Eval("FormattedDeliveryDate").ToString() == "" ? "-" : Eval("FormattedDeliveryDate")%>
                                                                            </asp:Label>--%>
                                                                    </td>
                                                                    <td>
                                                                        <b>Notes</b>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:Label ID="lblNotes" runat="server">
                                                                                <%#Eval("Notes").ToString() == "" ? "-" : Eval("Notes")%>
                                                                            </asp:Label>--%>
                                                                    </td>
                                                                </tr>

                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>

                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                        </td>

                                    </tr>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function pageLoad(sender, args) { if (!args.get_isPartialLoad()) { $addHandler(document, "keydown", onKeyDown); } }
        <%--function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                $find("<%=MPECustomerName.ClientID%>").hide();
                $find("<%=ModalPopupExtender1.ClientID%>").hide();
            }
        }--%>
</script>
    <%--   <script type="text/javascript">
        $(document).ready(function () {
            gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });
        }
    </script>--%>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=.] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView2] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=DataList1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=DataList1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <cc1:ModalPopupExtender ID="ModalPopupExtenderViewSerialNo" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
        OkControlID="btnOK" TargetControlID="btnNULL">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divFollowUp" style="display: none; width: 1000px;" class="modal_popup addDocumentPopup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div class="modalHead">

                        <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            View Serial No</h4>
                        <div>
                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal" causesvalidation="false">
                                Close
                            </button>
                            <asp:Button ID="ibtnPDF" runat="server" CssClass="btn btn-info btngray largeButton wid100btn" Text="PDF" OnClick="ibtnPDF_Click" />
                            <asp:Button ID="ibtnExcel" runat="server" CssClass="btn btn-info btngray largeButton wid100btn" Text="Excel" OnClick="ibtnExcel_Click" />
                            <asp:HiddenField ID="hndProjectID" runat="server" />
                            <asp:HiddenField ID="hndProjectNumber" runat="server" />
                        </div>
                    </div>

                </div>
                <div class="modal-body formGrid paddnone" style="max-height: 500px;">
                    <div class="seachfinal">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tablescrolldiv xscroll" style="overflow: auto; margin-bottom: 10px;">
                                    <div id="divgrdFollowUp" runat="server" class="content padtopzero marbtm50 finalgrid">
                                        <asp:GridView ID="grdSerialNo" DataKeyNames="SerialNo" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                            AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover popupTableGrid1">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("row_num") %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Category" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("Category") %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Serial No" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%#Eval("SerialNo") %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                            <%-- <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />--%>
                                            <EmptyDataRowStyle Font-Bold="True" />
                                            <%--<RowStyle CssClass="GridviewScrollItem" />--%>
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

    <cc1:ModalPopupExtender ID="ModalPopupExtenderViewPhoto" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
        CancelControlID="lnkcancel">
    </cc1:ModalPopupExtender>
    <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup companyPopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line printorder "></div>
                <div class="modal-header printorder">
                    <div class="modalHead">
                        <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-news-paper popupIcon"></i>View Photos</h4>
                        <div>
                            <asp:LinkButton ID="lnkcancel" runat="server" type="button" class="btn redButton btncancelIcon"
                                data-dismiss="myModal"><i class="fa fa-times"></i> Close
                            </asp:LinkButton>
                            <asp:Button ID="btnPhotoDownload" runat="server" CssClass="btn btn-info btngray largeButton wid100btn" 
                                Text="Download" OnClick="btnPhotoDownload_Click" />
                            <asp:HiddenField ID="P_ProjectID" runat="server" />
                        </div>
                    </div>
                </div>

                <div class="modal-body paddnone" runat="server" id="divdetail">
                    <div class="formainline formGrid">
                        <div class="">
                            <span class="name disblock">
                                <label class="control-label">
                                </label>
                            </span>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row spicaldivin " id="div27" runat="server">
                                        <div class="col-sm-12">
                                            <div class="row marginbtm15">
                                                <asp:Repeater runat="server" ID="rptPhoto">
                                                    <ItemTemplate>
                                                        <div class="col-md-3">
                                                            <div class="marginbtm15" style="padding-bottom: 25px!important;">
                                                                <img id="Img1" src='<%= cdnURL + "PanelInverterPhoto/"%><%#  Eval("Photo") %>' alt="" class="Size"/>
                                                                <%--<asp:Image ImageUrl="<%=cdnURL %> + PanelInverterPhoto/ + 637264396834611644.jpeg" runat="server" />--%>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <asp:Button ID="Button1" Style="display: none;" runat="server" />
</asp:Content>
