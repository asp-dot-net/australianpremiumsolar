﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_Meter : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static string SortDir;
    protected static string cdnURL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cdnURL = ConfigurationManager.AppSettings["cdnURL"];
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindProjectStatus();
            BindSubDivision();

            BindGrid(0);
        }
    }

    protected void BindProjectStatus()
    {
        string sql = "select ProjectStatusID, ProjectStatus from tblProjectStatus";
        DataTable dt = ClstblCustomers.query_execute(sql);

        ddlProjectStatus.DataSource = dt;
        ddlProjectStatus.DataTextField = "ProjectStatus";
        ddlProjectStatus.DataValueField = "ProjectStatusID";
        ddlProjectStatus.DataBind();
    }

    protected void BindSubDivision()
    {
        string sql = "select ID, SubDivisionName from tbl_SubDivision";
        DataTable dt = ClstblCustomers.query_execute(sql);

        ddlSubDivision.DataSource = dt;
        ddlSubDivision.DataTextField = "SubDivisionName";
        ddlSubDivision.DataValueField = "ID";
        ddlSubDivision.DataBind();
    }

    #region Bind GridView Code
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblProjects.tblProjects_GetData_MeterConnect(txtProjectNo.Text, txtCustomer.Text, ddlSubDivision.SelectedValue, txtDiscomName.Text, txtConsumerNo.Text, ddlProjectStatus.SelectedValue, txtGovtStatus.Text, ddlDateType.SelectedValue,txtStartDate.Text, txtEndDate.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        int iTotalRecords = 0;

        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        //PanTotal.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //HidePanels();
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                //     PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            //PanTotal.Visible = false;
            divnopage.Visible = false;
            //ltrPage.Text = "";
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {

                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";

                    if (Convert.ToInt32(50) < dt.Rows.Count)
                    {
                        //========label Hide
                        divnopage.Visible = false;
                    }
                    else
                    {
                        divnopage.Visible = true;
                        iTotalRecords = dv.ToTable().Rows.Count;
                        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                        if (iEndRecord > iTotalRecords)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        if (iStartsRecods == 0)
                        {
                            iStartsRecods = 1;
                        }
                        if (iEndRecord == 0)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    }
                }
            }
        }

    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "GeneratePickList")
        {
            string ProjectNo = e.CommandArgument.ToString();
            ClsTelerik_reports.Generate_PicklistPDF(ProjectNo);

            //ClsTelerik_reports.Generate_SerialNumber(ProjectNo);
        }

        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        DataView sortedView = new DataView(dt);

        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////// Don't Change Start
        ////string SortDir = string.Empty;
        //    if (dir == SortDirection.Ascending)
        //    {
        //        dir = SortDirection.Descending;
        //        SortDir = "Desc";
        //    }
        //    else
        //    {
        //        dir = SortDirection.Ascending;
        //        SortDir = "Asc";
        //    }
        ////////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();


    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Descending;
            }

            else
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        //GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //custompageIndex = GridView1.PageIndex + 1;

        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;

        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;
                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;
        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;
        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            //Response.Write(dv.ToTable().Rows.Count);
            //Response.End();
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        }
        else
        {
            ltrPage.Text = "";
        }
        //BindScript();

    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }

        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtProjectNo.Text = string.Empty;
        txtCustomer.Text = string.Empty;
        ddlSubDivision.SelectedValue = "";
        txtDiscomName.Text = string.Empty;
        txtConsumerNo.Text = string.Empty;
        ddlProjectStatus.SelectedValue = "";
        txtGovtStatus.Text = string.Empty;
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }
    
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        Response.Clear();
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();

            string FileName = "MeterConnect_" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 5, 6,
                              7, 8, 9, 10, 11,
                              12 };

            string[] arrHeader = { "Project No", "Govt Status", "Project Status", "Customer", "Install Date", "Sub Division",                             "Discom Name", "Consumer No", "Meter App. Status", "Submission Date", "System Size"
                                   , "Installation Department Notes" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            string DiscomID = dr["ElecDistributorID"].ToString();
            string ProjectID = dr["ProjectID"].ToString();
            
            if (DiscomID != "")
            {
                string sql = "Select ID, DiscomID, DocumentName, "+ ProjectID + " as ProjectID from tbl_DiscomDocumentList Where DiscomID = " + DiscomID + "and Active = 1";
                DataTable dt = ClstblCustomers.query_execute(sql);

                Repeater rptDocumentList = (e.Row.FindControl("rptDocumentList") as Repeater);
                rptDocumentList.DataSource = dt;
                rptDocumentList.DataBind();
            }
        }
    }

    protected void rptDocumentList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string[] arg = e.CommandArgument.ToString().Split(';');

        if (e.CommandName == "Self-Certificate")
        {
            string ProjectID = arg[0];
            string DiscomID = arg[1];

            if (DiscomID == "6" || DiscomID == "11")
            {
                ClsTelerik_reports.Generate_Self_Certification_Torrent(ProjectID);
            }
            else
            {
                ClsTelerik_reports.Generate_Self_Certification_DGVCL(ProjectID);
            }
        }

        if (e.CommandName == "Agreement")
        {
            string ProjectID = arg[0];
            string DiscomID = arg[1];

            if (DiscomID == "6" || DiscomID == "11")
            {
                ClsTelerik_reports.Generate_Agreement_Torrent(ProjectID);
            }
            else
            {
                ClsTelerik_reports.Generate_Agreement(ProjectID);
            }
        }

        if (e.CommandName == "PanelSerialNoSheet")
        {
            string ProjectID = arg[0];
            string DiscomID = arg[1];

            ClsTelerik_reports.Generate_SerialNumber(ProjectID);
        }

        //if (e.CommandName == "Customer_Ownership_Documents")
        //{
        //    string ProjectID = arg[0];
        //    string DiscomID = arg[1];

        //    string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.ID in (1, 2, 4)  and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + ProjectID + ")";
        //    DataTable dt = ClstblCustomers.query_execute(sql);

        //    if(dt.Rows.Count > 0)
        //    {
        //        string pageurl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];

        //        Response.Redirect(pageurl);
        //        //for (int i = 0; i < dt.Rows.Count; i++)
        //        //{
        //        //    string pageurl = cdnURL + "CustomerDocuments/" + dt.Rows[i]["FileUpload"];
        //        //    Response.Write(String.Format("window.open('{0}','_blank')", ResolveUrl(pageurl)));
        //        //}
        //    }
        //}
    }

    protected void rptDocumentList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = (DataRowView)e.Item.DataItem;
            string DocumentName = dr["DocumentName"].ToString();
            string DiscomID = dr["DiscomID"].ToString();

            HyperLink HypDocumentName = (e.Item.FindControl("HypDocumentName") as HyperLink);
            LinkButton lnkDocumentName = (e.Item.FindControl("lnkDocumentName") as LinkButton);
            HiddenField HndProjectID = (e.Item.FindControl("HndProjectID") as HiddenField);

            if (DocumentName.Trim() == "Self-Certificate")
            {
                HypDocumentName.Visible = false;
                lnkDocumentName.Visible = true;

                lnkDocumentName.CommandName = "Self-Certificate";
                lnkDocumentName.CommandArgument = HndProjectID.Value + ";" + DiscomID;
            }
            else if (DocumentName.Trim() == "Agreement")
            {
                HypDocumentName.Visible = false;
                lnkDocumentName.Visible = true;

                lnkDocumentName.CommandName = "Agreement";
                lnkDocumentName.CommandArgument = HndProjectID.Value + ";" + DiscomID;
            }
            else if (DocumentName.Trim() == "Panel Serial No Sheet")
            {
                HypDocumentName.Visible = false;
                lnkDocumentName.Visible = true;

                lnkDocumentName.CommandName = "PanelSerialNoSheet";
                lnkDocumentName.CommandArgument = HndProjectID.Value + ";" + DiscomID;
            }
            else if (DocumentName.Trim() == "Signed Application")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Signed Customer Application' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if(dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Discom Letter")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Discom Letter' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if(dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Structure Report")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Structure Report' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if(dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Shadow Report")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Shadow report' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if(dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Adharcard")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Aadhar Card' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if (dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Lightbill")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Electricity Bill' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if (dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Taxbill")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Tax Bill' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if (dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
            else if (DocumentName.Trim() == "Pancard")
            {
                HypDocumentName.Visible = true;
                lnkDocumentName.Visible = false;

                string sql = "Select CDD.CustomerId, CDD.FileUpload, CD.DocumentName from tblCustomerDocumentsDetail CDD join tbl_CustomerDocuments CD on CDD.DocumentId = CD.Id where CD.DocumentName = 'Pan Card' and CDD.CustomerId = (select CustomerID from tblProjects where ProjectID = " + HndProjectID.Value + ")";
                DataTable dt = ClstblCustomers.query_execute(sql);

                if (dt.Rows.Count > 0)
                {
                    HypDocumentName.NavigateUrl = cdnURL + "CustomerDocuments/" + dt.Rows[0]["FileUpload"];
                    HypDocumentName.Target = "_Blank";
                }
            }
        }
    }
}
