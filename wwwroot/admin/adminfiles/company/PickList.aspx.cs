﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_PickList : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static string SortDir;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindInstaller();

            BindProjectNo();
            BindVehcleType();

            BindGrid(0);
        }
    }

    protected void BindProjectNo()
    {
        string sql = "select convert(varchar(20),ProjectNumber) as ProjectNumber from tblProjects";
        DataTable dt = ClstblCustomers.query_execute(sql);

        lstProjectNo.DataSource = dt;
        lstProjectNo.DataMember = "ProjectNumber";
        lstProjectNo.DataTextField = "ProjectNumber";
        lstProjectNo.DataValueField = "ProjectNumber";
        lstProjectNo.DataBind();
    }

    protected void BindVehcleType()
    {
        string sql = "Select ID, VehicleType From tbl_VehicleType";
        DataTable dt = ClstblCustomers.query_execute(sql);

        ddlVehicleType.DataSource = dt;
        ddlVehicleType.DataTextField = "VehicleType";
        ddlVehicleType.DataValueField = "ID";
        ddlVehicleType.DataBind();

        ddlSerachVehicleType.DataSource = dt;
        ddlSerachVehicleType.DataTextField = "VehicleType";
        ddlSerachVehicleType.DataValueField = "ID";
        ddlSerachVehicleType.DataBind();

    }

    #region Bind GridView Code
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblProjects.tblProjects_GetData(txtProjectNo.Text, ddlInstaller.SelectedValue, txtCustomer.Text, txtCity.Text, ddlDateType.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSerachVehicleType.SelectedValue);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        int iTotalRecords = 0;

        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        //PanTotal.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //HidePanels();
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                //     PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            PanTotal.Visible = false;
            divnopage.Visible = false;
            //ltrPage.Text = "";
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();

            if(ddlSerachVehicleType.SelectedValue != "")
            {
                BindTotal(dt);
                PanTotal.Visible = true;
            }
            
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {

                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";

                    if (Convert.ToInt32(50) < dt.Rows.Count)
                    {
                        //========label Hide
                        divnopage.Visible = false;
                    }
                    else
                    {
                        divnopage.Visible = true;
                        iTotalRecords = dv.ToTable().Rows.Count;
                        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                        if (iEndRecord > iTotalRecords)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        if (iStartsRecods == 0)
                        {
                            iStartsRecods = 1;
                        }
                        if (iEndRecord == 0)
                        {
                            iEndRecord = iTotalRecords;
                        }
                        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    }
                }
            }
        }

    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "GeneratePickList")
        {
            string ProjectNo = e.CommandArgument.ToString();
            ClsTelerik_reports.Generate_PicklistPDF(ProjectNo);
            
            //ClsTelerik_reports.Generate_SerialNumber(ProjectNo);
        }

        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        DataView sortedView = new DataView(dt);

        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////// Don't Change Start
        ////string SortDir = string.Empty;
        //    if (dir == SortDirection.Ascending)
        //    {
        //        dir = SortDirection.Descending;
        //        SortDir = "Desc";
        //    }
        //    else
        //    {
        //        dir = SortDirection.Ascending;
        //        SortDir = "Asc";
        //    }
        ////////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();


    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Descending;
            }

            else
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        //GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //custompageIndex = GridView1.PageIndex + 1;

        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView1.DataSource = dv;
        //GridView1.DataBind();
        BindGrid(0);
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;

        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;
                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;
        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;
        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            //Response.Write(dv.ToTable().Rows.Count);
            //Response.End();
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        }
        else
        {
            ltrPage.Text = "";
        }
        //BindScript();

    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }

        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtProjectNo.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        txtCustomer.Text = string.Empty;
        txtCity.Text = string.Empty;
        ddlSerachVehicleType.SelectedValue = "";
        ddlDateType.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    protected void BindInstaller()
    {
        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ddlVehicleType.SelectedValue = "";
        txtNumber.Text = string.Empty;
        txtDriverName.Text = string.Empty;
        txtDeliveryDate.Text = string.Empty;
        lstProjectNo.ClearSelection();

        ModalPopupExtenderTransport.Show();
    }

    protected void ddlVehicleType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVehicleType.SelectedValue != "")
        {
            if (ddlVehicleType.SelectedValue == "25") // Others
            {
                txtNumber.Text = string.Empty;
                txtNumber.ReadOnly = false;
                ModalPopupExtenderTransport.Show();
            }
            else
            {
                txtNumber.ReadOnly = true;
                string sql = "select RegoNo from tbl_VehicleType where ID = " + ddlVehicleType.SelectedValue + "";
                DataTable dt = ClstblCustomers.query_execute(sql);

                txtNumber.Text = dt.Rows[0]["RegoNo"].ToString();
                ModalPopupExtenderTransport.Show();
            }
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        bool Flag = true;
        if (lstProjectNo.SelectedValue.Length == 0)
        {
            Flag = false;
        }

        if (Flag == true)
        {
            string ProjectNo = "";
            foreach (ListItem itemval in lstProjectNo.Items)
            {
                if (itemval.Selected)
                {
                    ProjectNo += "," + itemval.Value;
                }
            }
            if (ProjectNo != string.Empty)
            {
                ProjectNo = ProjectNo.Substring(1).ToString();
            }

            int Suc = 0;
            string VehicleTypeID = ddlVehicleType.SelectedValue;
            string RegoNo = txtNumber.Text;
            string DriverName = txtDriverName.Text;
            string DeliveryDate = txtDeliveryDate.Text;
            string Notes = txtNotes.Text;

            string[] PNo = ProjectNo.Split(',');

            for (int i = 0; i < PNo.Length; i++)
            {
                string Project = PNo[i];

                Suc = ClstblProjects.tbl_TransportLog_Insert(Project, VehicleTypeID, RegoNo, DriverName, DeliveryDate, Notes);
            }

            if (Suc > 0)
            {
                SetAdd1();
                BindGrid(0);
            }
            //string jsMethodName = "InActiveLoader()";
            //ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
        }
        else
        {
            SetError1();
            ModalPopupExtenderTransport.Show();
        }
    }

    protected void BindTotal(DataTable dt)
    {
        //DataTable dtTotal = new DataTable();

        //string sql = "Select ID, VehicleType From tbl_VehicleType";
        //DataTable dtVehicleType = ClstblCustomers.query_execute(sql);

        //if (dtVehicleType.Rows.Count > 0)
        //{
        //    dtTotal.Columns.Add("VehicleType");
        //    dtTotal.Columns.Add("Trip");
        //    for (int i = 0; i < dtVehicleType.Rows.Count; i++)
        //    {
        //        DataRow row = dtTotal.NewRow();
        //        string VehicleType = dtVehicleType.Rows[i]["VehicleType"].ToString();
        //        row["VehicleType"] = VehicleType;

        //        string Trip = dt.Compute("Count(VehicleType)", "VehicleType='" + VehicleType + "'").ToString();
        //        row["Trip"] = Trip;

        //        dtTotal.Rows.Add(row);
        //    }
        //}

        //rptTotal.DataSource = dtTotal;
        //rptTotal.DataBind();

        if(dt.Rows.Count > 0)
        {
            if(ddlSerachVehicleType.SelectedValue != "")
            {
                string sql = "Select ID, VehicleType From tbl_VehicleType where ID =  "+ ddlSerachVehicleType.SelectedValue +"";
                DataTable dtVehicleType = ClstblCustomers.query_execute(sql);

                string VehicleType = dtVehicleType.Rows[0]["VehicleType"].ToString();
                lblVehicle.Text = VehicleType;

                string Trip = dt.Compute("Count(VehicleType)", "VehicleType='" + VehicleType + "'").ToString();
                lblTotalTrip.Text = Trip;
            }
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();

        Response.Clear();
        try
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                  .Select(x => x.ColumnName)
                .ToArray();

            Export oExport = new Export();
            string FileName = "Picklist" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

            int[] ColList = { 1, 2, 3, 4, 7, 6,
                              9, 10, 12, 13 };

            string[] arrHeader = { "Project No", "Customer", "Address", "Installer", "Vehicle Type", "Booking Date",
                "Rego No", "Driver", "Delivery Date", "Notes" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
}
