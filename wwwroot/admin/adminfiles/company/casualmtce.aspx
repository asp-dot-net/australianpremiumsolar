<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="casualmtce.aspx.cs" Inherits="admin_adminfiles_company_casualmtce" Culture="en-GB" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Casual Maintenance
        
        </h5>      
      </div> --%>
    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

              function (isConfirm) {
                  if (isConfirm) {
                      // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      eval(defaultAction);

                      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      return true;
                  } else {
                      // swal("Cancelled", "Your imaginary file is safe :)", "error");
                      return false;
                  }
              });
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(".myvalproject").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            //  $('.i-checks').iCheck({
            //                        checkboxClass: 'icheckbox_square-green',
            //                        radioClass: 'iradio_square-green'
            //                    });


        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

              function (isConfirm) {
                  if (isConfirm) {
                      // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      eval(defaultAction);

                      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      return true;
                  } else {
                      // swal("Cancelled", "Your imaginary file is safe :)", "error");
                      return false;
                  }
              });
        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate> 
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Casual Maintenance</h5>      
        <div id="Div1" class="pull-right">
          <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
        </div>
      </div>
      
      
 <div class="page-body padtopzero">
   <div class=" padtopzero">
    <div class="finaladdupdate"> 
    <div id="PanAddUpdate" runat="server" visible="false">
        <div class=" animate-panel">
             <div class="form-horizontal ">
            <div class="well with-header  addform">
                                     <div class="header bordered-blue">
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                    Casual Maintenance
                                         </div>
                               
                               
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <asp:Label ID="lblFirstName" runat="server" class="col-sm-2 control-label">
                                                <strong>First</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualContFirst" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtCasualContFirst" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                <strong>Last</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualContLast" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtCasualContLast" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                <strong>Phone</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualCustPhone" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCasualCustPhone"
                                                    ValidationGroup="company" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03XXXXXXXX)"
                                                    ValidationExpression="^(07|03)[\d]{8}$"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                <strong>Mobile</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualContMobile" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCasualContMobile"
                                                    ValidationGroup="company" Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                    ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label">
                                                <strong>Email</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualContEmail" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtCasualContEmail"
                                                    ValidationGroup="contact" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                </asp:RegularExpressionValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label">
                                                <strong>Address</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualAddress" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtCasualAddress" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label">
                                                <strong>City</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualCity" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtCasualCity" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                    UseContextKey="true" TargetControlID="txtCasualCity" ServicePath="~/Search.asmx"
                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label6" runat="server" class="col-sm-2 control-label">
                                                <strong>State</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualState" runat="server" Enabled="false" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label">
                                                <strong>PostCode</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualPostCode" runat="server" Enabled="false" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label8" runat="server" class="col-sm-2 control-label">
                                                <strong>System Details</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualSystemDetails" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                                    ControlToValidate="txtCasualSystemDetails" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblLastName" runat="server" class="col-sm-2 control-label">
                                                <strong>Installer</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                </asp:DropDownList>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="reqerror" runat="server" ErrorMessage="This value is required."
                                                    ControlToValidate="ddlInstaller" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label9" runat="server" class="col-sm-2 control-label">
                                                <strong>RefNumber</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtCasualRefNumber" runat="server" MaxLength="200" CssClass="form-control modaltextbox center-text"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regvalSortOrder" runat="server" ErrorMessage="Number Only"
                                                    ValidationExpression="^[0-9]*$" ControlToValidate="txtCasualRefNumber"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>


                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group ">
                <div class="col-sm-8 col-sm-offset-2">
                  <asp:Button CssClass="btn btn-primary  redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                            Text="Add" ValidationGroup="company1" />
                  <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                            Text="Save" Visible="false" />
                  <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                            CausesValidation="false" Text="Reset" />
                  <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                            CausesValidation="false" Text="Cancel" />
                </div>
              </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
       </div>
       </div> 
     
             
    <asp:Panel runat="server" ID="PanGridSearch">
       <div class="animate-panel">
          <div id="divleft" runat="server">
            <div class=" messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                        Text="Transaction Failed."></asp:Label></strong>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
            
            
            
            <div class=" animate-panel">
                <div >
                    <div>
                        <div class="hpanel marbtmzero">
                            <div class="searchfinal">  
                    <div class="widget-body shadownone brdrgray">          
                                    <div class="row">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dataTables_filter Responsive-search printorder">
                                             <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="inlineblock">
                                                            <div class="col-sm-12">
                                                                <div class="input-group col-sm-2">
                                                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                        WatermarkText="Casual Maintenance" />
                                                                </div>
                                                                <div class="input-group col-sm-2">
                                                                    <asp:DropDownList ID="ddlInstallerSearch" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Installer</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                              </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                </div>


                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnClearAll" runat="server" data-placement="left"
                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                                </asp:Panel>
                                        </div>
                                            </div>
                                    </div>
                       
                               
                             <div class="datashowbox">
                                    <div class="row">
                                        <div class="col-sm-8">
                                        <div class="dataTables_length showdata pabtm10">
                                           
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                           
                                        </div>
                                            </div>
                                    </div>
                               </div>
                             </div>
                        </div>
                         </div>
                        <div class="panel-body padleftzero padrightzero">
                            <div class="">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="">
                                        <div id="PanGrid" runat="server">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView1" DataKeyNames="ProjectMaintenanceID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                                    OnRowDataBound="GridView1_RowDataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%# Eval("CasualCustomer")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%# Eval("CasualContMobile")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" ItemStyle-Width="200px"
                                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%# Eval("CasualContEmail")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="RefNumber" ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%# Eval("CasualRefNumber")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Invoice" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectMaintenanceID") %>' />
                                                                <asp:LinkButton ID="lbtnInvAdd" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ProjectMaintenanceID")%>' CommandName="invoiceadd"
                                                                    data-toggle="tooltip" data-placement="top" data-original-title="Add Invoice"><img src="../../images/addinvoice.png" /></asp:LinkButton>
                                                                <asp:LinkButton ID="lbtnInvUpdate" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ProjectMaintenanceID")%>' CommandName="invoiceupdate"
                                                                    data-toggle="tooltip" data-placement="top" data-original-title="Edit Invoice"><i class="fa fa-edit"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle />
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false"
                                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField ItemStyle-Width="40px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                            <ItemTemplate>
                                                                 <!--DELETE Modal Templates-->
                                                                     <asp:LinkButton ID="gvbtnDelete" runat="server"  CssClass="btn btn-danger btn-xs" CausesValidation="false" 
                                                                     CommandName="Delete" CommandArgument='<%#Eval("ProjectMaintenanceID")%>' >
                                            <i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>
                                                               
                                                                  <!--END DELETE Modal Templates-->
                                                            </ItemTemplate>
                                                            <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
           </div>
    </asp:Panel>
    </div>

          <!--Danger Modal Templates-->
   <asp:Button ID="btndelete" Style="display:none;"  runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class=" modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  

    <!--End Danger Modal Templates-->

        </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
