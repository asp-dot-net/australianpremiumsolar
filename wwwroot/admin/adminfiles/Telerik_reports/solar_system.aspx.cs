﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EurosolarReporting;
//using Telerik.Reporting.Services.WebApi;
using Telerik.ReportViewer.WebForms;
using Telerik.Reporting;

public partial class Telerik_reports_solar_system : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();

        string ProjectID = "841575";
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string inverterdetail = string.Empty;
         dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.solar_system();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox1.Value = dt1.Rows[0]["ContFirst"].ToString();
            textBox2.Value = dt1.Rows[0]["ContLast"].ToString();
            textBox3.Value = dt1.Rows[0]["StreetAddress"].ToString();
            //textBox4.Value = dt1.Rows[0]["StreetAddress"].ToString();
            textBox4.Value = dt1.Rows[0]["InstallAddress"].ToString();
            textBox5.Value = dt1.Rows[0]["PostalCity"].ToString();
            textBox6.Value = dt1.Rows[0]["PostalState"].ToString();
            textBox7.Value = dt1.Rows[0]["PostalPostCode"].ToString();
        }
        catch { }
     
        ir.ReportDocument = rpt;
        ReportViewer1.ReportSource = ir;
        ReportViewer1.RefreshReport();

    }


}