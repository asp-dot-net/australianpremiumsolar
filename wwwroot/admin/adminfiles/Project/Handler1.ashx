﻿<%@ WebHandler Language="C#" Class="Handler1" %>

using System;
using System.Web;

public class Handler1 : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            try
            {
                HttpFileCollection SelectedFiles = context.Request.Files;
                for (int i = 0; i < SelectedFiles.Count; i++)

                {
                    HttpPostedFile PostedFile = SelectedFiles[i];
                    string FileName = context.Server.MapPath("~/userfiles/CustomerDocuments/" + PostedFile.FileName);
                    PostedFile.SaveAs(FileName);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}