﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" 
    AutoEventWireup="true" CodeFile="project.aspx.cs" Inherits="admin_adminfiles_Project_project" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/project.ascx" TagPrefix="uc1" TagName="project" %>
<%@ Register Src="~/includes/controls/projectdetail.ascx" TagName="projectdetail" TagPrefix="uc1" %>
<%@ Register Src="~/includes/controls/projectprice.ascx" TagName="projectprice" TagPrefix="uc2" %>
<%--<%@ Register Src="projectsale.ascx" TagName="projectsale" TagPrefix="uc3" %>--%>
<%@ Register Src="~/includes/controls/projectquote.ascx" TagName="projectquote" TagPrefix="uc4" %>
<%@ Register Src="~/includes/controls/projectpreinst.ascx" TagName="projectpreinst" TagPrefix="uc5" %>
<%@ Register Src="~/includes/controls/projectpostinst.ascx" TagName="projectpostinst" TagPrefix="uc6" %>
<%@ Register Src="~/includes/controls/projectelecinv.ascx" TagName="projectelecinv" TagPrefix="uc7" %>
<%@ Register Src="~/includes/controls/projectrefund.ascx" TagName="projectrefund" TagPrefix="uc8" %>
<%@ Register Src="~/includes/controls/projectforms.ascx" TagName="projectforms" TagPrefix="uc9" %>
<%@ Register Src="~/includes/controls/projectfinance.ascx" TagName="projectfinance" TagPrefix="uc10" %>
<%@ Register Src="~/includes/controls/projectstc.ascx" TagName="projectstc" TagPrefix="uc11" %>
<%@ Register Src="~/includes/controls/projectdocs.ascx" TagName="projectdocs" TagPrefix="uc12" %>
<%@ Register Src="~/includes/controls/projectmtce.ascx" TagName="projectmtce" TagPrefix="uc13" %>
<%@ Register Src="~/includes/controls/projectmaintenance.ascx" TagName="projectmaintenance" TagPrefix="uc14" %>
<%--<%@ Register Src="~/includes/controls/projectvicdoc.ascx" TagName="projectvicdoc" TagPrefix="uc15" %>--%>
<%@ Register Src="~/includes/controls/projectTest.ascx" TagName="projectProject1" TagPrefix="uc16" %>
<%@ Register Src="~/includes/controls/Projectdocuments.ascx" TagName="Projectdocuments" TagPrefix="uc17" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-body headertopbox">
            <h5 class="row-title">
                <i class="pe-7s-note2 icon"></i><asp:Literal ID="ltproject" runat="server"></asp:Literal></h5>

            <div class="flexRow">
                <%-- <asp:ImageButton ID="btnCheckActive" runat="server" ImageUrl="~/images/btn_check_active2.png"
                    CausesValidation="false" OnClick="btnCheckActive_Click" CssClass="btnimagecheckactivity margintopminus" style="margin-right:5px;"/> --%>
                <%--<a href="#" class="badge blueBadge" style="margin-right:8px;">Status</a>--%>
               <asp:Label ID="lblRebateStatus"  runat="server" class="badge blueBadge" style="margin-right:8px;"></asp:Label>
                <asp:Label ID="lblstatus"  runat="server" class="badge blueBadge" style="margin-right:8px;"></asp:Label>
                <asp:LinkButton ID="lnkclose" CausesValidation="false" runat="server"
                    OnClick="lnkclose_Click" CssClass="btn btn-info btngray"><i class="fa fa-backward"></i>Back</asp:LinkButton>
            </div>
    </div>
    <div class="row nopadding" id="divprojecttab" runat="server">
            <div class="col-md-12 ">
                <!-- start: ALERTS PANEL -->
                <div class="tabintab companytab">
                    <div class="addEmployee customerTab">
                       
                        <div class="bodymianbg">
                            <div class="row wid100">
                                    <asp:Panel ID="Panel2" runat="server" class="col-md-12">
                                        <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            AutoPostBack="true">
                                            <cc1:TabPanel ID="TabDetail" runat="server" HeaderText="Detail" CssClass="printpage" TabIndex="0">
                                                <ContentTemplate>
                                                    <uc1:projectdetail ID="projectdetail1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabprojectProject" runat="server" HeaderText="Sales" CssClass="printpage" TabIndex="14">
                                                <ContentTemplate>
                                                    <uc14:projectmaintenance ID="projectmaintenance1" runat="server" />
                                                    <uc16:projectProject1 ID="projectProject1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <%--<cc1:TabPanel ID="TabMaintenance" runat="server" HeaderText="Sale" TabIndex="1" OnClientClick="setActiveTabByTitle(this);">
                                        <ContentTemplate>
                                            <uc14:projectmaintenance ID="projectmaintenance1" runat="server" />
                                            <uc3:projectsale ID="projectsale1" runat="server" />
                                        </ContentTemplate>
                                    </cc1:TabPanel>--%>

                                            <%--<cc1:TabPanel ID="TabPrice" runat="server" HeaderText="Price" TabIndex="2">
                                                <ContentTemplate>
                                                    <uc2:projectprice ID="projectprice1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabQuote" runat="server" HeaderText="Quote" TabIndex="3">
                                                <ContentTemplate>
                                                    <uc4:projectquote ID="projectquote1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>--%>

                                            <cc1:TabPanel ID="TabPreInst" runat="server" HeaderText="Installation" TabIndex="7">
                                                <ContentTemplate>
                                                    <uc5:projectpreinst ID="projectpreinst1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                         
                                            <cc1:TabPanel ID="TabElecInv" runat="server" HeaderText="Invoice" TabIndex="4">
                                                <ContentTemplate>
                                                    <uc7:projectelecinv ID="projectelecinv1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                          
                                            <%--<cc1:TabPanel ID="TabMtce" runat="server" HeaderText="Mtce" TabIndex="10">
                                                <ContentTemplate>
                                                    <uc13:projectmtce ID="projectmtce1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>--%>
                                            <cc1:TabPanel ID="TabDocuments" runat="server" HeaderText="Documents" TabIndex="10">
                                                <ContentTemplate>
                                                    <uc17:Projectdocuments ID="documents1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <%--<cc1:TabPanel ID="TabVicDoc" runat="server" HeaderText="Vic Doc" TabIndex="13">
                                                <ContentTemplate>
                                                    <uc15:projectvicdoc ID="projectvicdoc1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>--%>


<%--                                            <cc1:TabPanel ID="TabFinance" runat="server" HeaderText="Finance" TabIndex="4">
                                                <ContentTemplate>
                                                    <uc10:projectfinance ID="projectfinance1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabForms" runat="server" HeaderText="Forms" TabIndex="5">
                                                <ContentTemplate>
                                                    <uc9:projectforms ID="projectforms1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>




                                            <cc1:TabPanel ID="TabDocs" runat="server" HeaderText="Documents" TabIndex="11">
                                                <ContentTemplate>
                                                    <uc12:projectdocs ID="projectdocs1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabRefund" runat="server" HeaderText="Refund" TabIndex="12">
                                                <ContentTemplate>
                                                    <uc8:projectrefund ID="projectrefund1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>--%>

                                               <cc1:TabPanel ID="TabPostInst" runat="server" HeaderText="Post-Inst" TabIndex="8" Visible="false">
                                                <ContentTemplate>
                                                    <uc6:projectpostinst ID="projectpostinst1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                              <cc1:TabPanel ID="TabSTC" runat="server" HeaderText="STC" TabIndex="6" Visible="false" >
                                                <ContentTemplate>
                                                    <uc11:projectstc ID="projectstc1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>

                                        </cc1:TabContainer>
                                    </asp:Panel>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelActive" DropShadow="false" PopupControlID="divAddActive"
            OkControlID="btnOKActive" TargetControlID="btnNULLActive">
        </cc1:ModalPopupExtender>
        <div id="divAddActive" runat="server">
            <div class="modal-dialog" style="width: 350px;">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <div style="float: right">
                            <asp:LinkButton ID="ibtnCancelActive" CausesValidation="false"
                                runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="ibtnCancelActive_Onclick">
                   Close
                            </asp:LinkButton>
                        </div>
                        <h4 class="modal-title" id="H1">Project Status</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="formainline">
                                <div class="form-group">
                                    <h3 class="martopzero">
                                        <asp:Label ID="lblActive" runat="server" Visible="true"></asp:Label></h3>
                                </div>
                                <div>
                                    <table class="form-group table table-bordered table-striped">
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblSignedQuote" runat="server"></asp:Label>
                                                </b>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblDepositeRec" runat="server"></asp:Label>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblQuoteAccepted" runat="server"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblEleDest" runat="server"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="approveref" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblApprovalRef" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="NMINumber" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblNMINumber" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="meterupgrade" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblmeterupgrade" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="RegPlanNo" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblRegPlanNo" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="LotNum" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblLotNum" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="DocumentVerified" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblDocumentVerified" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>
                                        <tr id="VicAppRefference" runat="server" visible="false">
                                            <td>
                                                <b>
                                                    <asp:Label ID="lblVicAppRefference" runat="server" Visible="false"></asp:Label>
                                                </b>

                                            </td>
                                        </tr>


                                    </table>

                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnOKActive" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULLActive" Style="display: none;" runat="server" />
</asp:Content>

