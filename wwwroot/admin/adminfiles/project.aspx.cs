﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_Project_project : System.Web.UI.Page
{
    private object projectquote1;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string ProjectID = Request.QueryString["proid"];
        //SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //lblstatus.Text = st.ProjectSTatus;
        if (Session["txtboxHideShow"] != null)
        {
            lblstatus.Text = "Document Received";
        }
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
           string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            lblstatus.Text = null;
            lblstatus.Text = st.ProjectSTatus;
            lblRebateStatus.Text = st.ApplicaionStatus;
        }

        if (string.IsNullOrEmpty(Request.QueryString["m"]))
        {
            //Profile.eurosolar.companyid = "";
            //Profile.eurosolar.contactid = "";
            //Profile.eurosolar.projectid = "";
        }
        else
        {
            if (Request.QueryString["m"] == "pro")
            {
                TabContainer1.ActiveTabIndex = 2;
                //project1.ViewProjectDetail();

                ViewProjectDetail();

            }
        }

        AjaxControlToolkit.TabContainer container = (AjaxControlToolkit.TabContainer)TabContainer1;
        foreach (object obj in container.Controls)
        {
            foreach (AjaxControlToolkit.TabPanel tab in TabContainer1.Tabs)
            {
                if (tab.Visible)
                {
                    TabContainer1.ActiveTab = tab;
                    break;
                }
            }
            
        }
        var firstVisibleTab = TabContainer1.Tabs.OfType<AjaxControlToolkit.TabPanel>().FirstOrDefault(tab => tab.Visible);

    }
    public void ViewProjectDetail()
    {

        divprojecttab.Visible = true;
        TabContainer1.ActiveTabIndex = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            lblstatus.Text = null;
            lblstatus.Text = st.ProjectSTatus;
            lblRebateStatus.Text = st.ApplicaionStatus;

            if (!string.IsNullOrEmpty(st.DepositReceived))
            {
                Session["activeText"] = true;
            }
            ltproject.Text = st.ProjectNumber + " -" + st.street_number + " " + st.street_name + ", " + st.StreetCity + ", " + st.StreetState + " - " + st.StreetPostCode;
            if (Request.QueryString["m"] == "proelec")
            {
                TabContainer1.ActiveTabIndex = 9;
                projectelecinv1.BindProjectElecInv();
            }
        }
        if (Request.QueryString["m"] == "propre")
        {
            TabContainer1.ActiveTabIndex = 7;
            projectpreinst1.GetPreInstClick();
            //projectpreinst1.BindProjectPreInst();
        }
        if (Roles.IsUserInRole("Installer"))
        {
            //projectdocs1.BindDocs();
        }
        else
        {
            projectdetail1.BindProjectDetail();
        }

        if ((Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("DSalesRep")))
        {
            //TabRefund.Visible = true;
            // TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            TabElecInv.Visible = false;

            TabSTC.Visible = false;
            // TabDocs.Visible = false;
            // TabMtce.Visible = false;

            this.TabContainer1.Tabs[12].Visible = true;
        }
        if ((Roles.IsUserInRole("Accountant")) || (Roles.IsUserInRole("Administrator")))
        {
            Session["openMaintainace"] = "close";
            if (Session["openMaintainace"].ToString() == "close")
            {
                //Response.Write("Hiiiiiiii");
                //Response.End();
                //TabForms.Visible = true;
                TabPreInst.Visible = true;
                // TabPostInst.Visible = true;
                TabElecInv.Visible = true;
                // TabRefund.Visible = true;
                // TabDocs.Visible = true;
                // TabMtce.Visible = true;
                projectmaintenance1.Visible = false;

                //TabPrice.Visible = true;
                //TabSTC.Visible = true;
                //TabQuote.Visible = true;
                //TabFinance.Visible = true;
            }
            else
            {
                if ((!Roles.IsUserInRole("SalesRep")))
                {
                    HideAndShowPanel();
                }
            }
        }
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")))
        {

            // TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            TabElecInv.Visible = false;
            TabSTC.Visible = false;
            // TabMtce.Visible = false;

        }

        if ((Roles.IsUserInRole("PostInstaller")))
        {

            // TabPostInst.Visible = true;
            TabElecInv.Visible = true;
            //TabRefund.Visible = false;

        }
        if ((Roles.IsUserInRole("Installer")))
        {

            TabDetail.Visible = false;
            // TabSale.Visible = false;
            //TabPrice.Visible = false;
            // TabQuote.Visible = false;
            // TabFinance.Visible = false;
            // TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            //TabRefund.Visible = false;
            TabSTC.Visible = false;
            TabElecInv.Visible = false;
            // TabDocs.Visible = true;

        }
        else
        {
            if (Roles.IsUserInRole("Administrator"))
            {
                if (Session["openMaintainace"].ToString() == "close")
                {
                    //TabDocs.Visible = true;
                    projectmaintenance1.Visible = false;
                }
                else
                {
                    //TabDocs.Visible = false;
                }
            }
            else
            {
                //TabDocs.Visible = false;
            }
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            //Response.Write("Hiiiiiiii");
            //Response.End();
            // TabDocs.Visible = true;
            //TabRefund.Visible = true;
            //TabVicDoc.Visible = false;

        }
        if ((Roles.IsUserInRole("PostInstaller")))
        {
            //TabDocs.Visible = true;
        }
        if ((Roles.IsUserInRole("PreInstaller")))
        {

            TabPostInst.Visible = false;
            TabElecInv.Visible = false;
            //TabDocs.Visible = true;

        }
    }
    public void HideAndShowPanel()
    {
        //TabPrice.Visible = true;
        //TabQuote.Visible = true;
        //TabFinance.Visible = false;
        //TabForms.Visible = false;
        //TabSTC.Visible = true;
        TabPreInst.Visible = true;
        //  TabPostInst.Visible = true;
        TabElecInv.Visible = true;
        //TabMtce.Visible = true;
        //TabDocs.Visible = false;
        //TabRefund.Visible = false;
        //TabMaintenance.Visible = true;
    }
    protected void btnCheckActive_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.SignedQuote != "False")
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote </span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.ElecDistributorID == string.Empty)
        {
            lblEleDest.Text = "Please select Elec Distributor.";
        }
        else
        {
            lblEleDest.Visible = false;
        }
        if (st.ElecDistributorID == "12")
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref </span>:&nbsp; <i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            //if (st.meterupgrade != string.Empty)
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            //}
            //else
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            //}
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblmeterupgrade.Visible = true;
            //meterupgrade.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else if (st.ElecDistributorID == "13")
        {
            if (st.RegPlanNo != string.Empty)
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.LotNumber != string.Empty)
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            //if (st.meterupgrade != string.Empty)
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            //}
            //else
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            //}
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = true;
            RegPlanNo.Visible = true;
            lblLotNum.Visible = true;
            LotNum.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            //meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            //if (st.meterupgrade != string.Empty)
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            //}
            //else
            //{
            //    lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            //}
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = false;
            RegPlanNo.Visible = false;
            lblLotNum.Visible = false;
            LotNum.Visible = false;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            //meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        //else
        //{
        //    lblRegPlanNo.Visible = false;
        //    lblLotNum.Visible = false;
        //    lblApprovalRef.Visible = false;
        //    lblNMINumber.Visible = false;
        //}
        if (st.DepositReceived != string.Empty)
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.QuoteAccepted != string.Empty)
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.InstallState == "VIC")
        {
            VicAppRefference.Visible = true;
            lblVicAppRefference.Visible = true;
            if (!string.IsNullOrEmpty(st.VicAppReference))
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
        }
        else
        {
            VicAppRefference.Visible = false;
            lblVicAppRefference.Visible = false;
        }

        //if (st.SQ != "" && st.SignedQuote != "False" && st.DepositReceived != "" && st.RegPlanNo != "" && st.LotNumber != "" && st.ElecDistApprovelRef != "" && st.NMINumber != "")
        if (st.ProjectStatusID == "3" || st.ProjectStatusID == "5")
        {
            lblActive.Text = "Project is Active";
        }
        else
        {
            lblActive.Text = "Project is not Active";
        }

        ModalPopupExtender2.Show();
        //projectquote1.BindProjectQuote();
    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(panprojects,
        //                                    this.GetType(),
        //                                    "MyAction",
        //                                    "doMyAction();",
        //                                    true);
        //ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "InActiveLoader()", true);
        if (TabContainer1.ActiveTabIndex == 0)
        {
            TabPostInst.Visible = false;
            projectdetail1.BindProjectDetail();
            if ((Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager")))
            {

                //TabForms.Visible = false;
                TabPreInst.Visible = false;
                TabPostInst.Visible = false;
                TabElecInv.Visible = false;
                TabSTC.Visible = false;
                //TabMtce.Visible = false;
                //TabDocs.Visible = false;

            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                //TabDocs.Visible = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabPostInst.Visible = false;
                TabElecInv.Visible = false;
            }
        }
        else if (TabContainer1.ActiveTabIndex == 1)
        {
            projectProject1.BindProjectSale();
            Session["openMaintainace"] = "close";
            if ((Session["openMaintainace"].ToString()) == "open")
            {
                projectmaintenance1.Visible = true;
                projectProject1.Visible = false;


                projectmaintenance1.Bindmaintainance();
            }
            else
            {
                projectmaintenance1.Visible = false;
                projectProject1.Visible = true;
                projectProject1.BindProjectSale();
            }

            //projectsale1.BindProjectSale();
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            //projectprice1.BindProjectPrice();
            //// projectsale1.BindProjectSale();

            if ((Session["openMaintainace"].ToString()) == "open")
            {
                projectmaintenance1.Visible = true;
                // projectsale1.Visible = false;


                projectmaintenance1.Bindmaintainance();
            }
            else
            {
                projectmaintenance1.Visible = false;
                //projectsale1.Visible = true;
                // projectsale1.BindProjectSale();
                projectpreinst1.GetPreInstClick();
            }


            //projectsale1.BindProjectSale();
        }
        else if (TabContainer1.ActiveTabIndex == 3)
        {
            //  projectquote1.BindProjectQuote();
        }
        else if (TabContainer1.ActiveTabIndex == 4)
        {
            projectpreinst1.GetPreInstClick();
        }
        else if (TabContainer1.ActiveTabIndex == 5)
        {

            projectpostinst1.BindProjectPostInst();
            //projectstc1.BindProjectSTC();
            //projectfinance1.BindProjectFinance();
        }
        else if (TabContainer1.ActiveTabIndex == 6)
        {
            projectelecinv1.BindProjectElecInv();
            //projectforms1.BindProjectForms();
        }
        else if (TabContainer1.ActiveTabIndex == 7)
        {
            projectstc1.BindProjectSTC();

            //projectstc1.BindProjectSTC();
        }
        else if (TabContainer1.ActiveTabIndex == 8)
        {
            //if (Roles.IsUserInRole("PreInstaller"))
            //{
            //TabContainer1.ActiveTabIndex = 5;
            //projectmtce1.Visible = true;
            // projectpostinst1.Visible = false;
            //  projectdocs1.BindDocs();
            //  }
            //projectmtce1.BindProjectMtce();

            //Response.Redirect(Request.Url.PathAndQuery);
            //projectpreinst1.BindProjectPreInst();

        }
        else if (TabContainer1.ActiveTabIndex == 9)
        {
            documents1.BindDocuments();
        }
        else if (TabContainer1.ActiveTabIndex == 10)
        {
            documents1.BindDocuments();
        }
        else if (TabContainer1.ActiveTabIndex == 11)
        {
            //projectdocs1.BindDocs();
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 8;
                //projectmtce1.Visible = true;
                //  projectdocs1.BindDocs();
            }
            //projectmtce1.BindProjectMtce();
        }
        else if (TabContainer1.ActiveTabIndex == 12)
        {
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 9;
                //projectdocs1.Visible = true;
            }
            //projectdocs1.BindDocs();
        }
        else if (TabContainer1.ActiveTabIndex == 13)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
            {
                TabContainer1.ActiveTabIndex = 5;

            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                TabContainer1.ActiveTabIndex = 12;
                //  projectrefund1.Visible = true;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 10;
                // projectmtce1.Visible = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                TabContainer1.ActiveTabIndex = 11;
                //projectrefund1.Visible = true;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                TabContainer1.ActiveTabIndex = 11;
                //projectrefund1.Visible = true;
            }
            //projectrefund1.BindProjectRefund();
        }
        else if (TabContainer1.ActiveTabIndex == 14)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
            {
                TabContainer1.ActiveTabIndex = 6;
            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                TabContainer1.ActiveTabIndex = 12;
                //projectrefund1.Visible = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 11;
                ////projectdocs1.Visible = false;
                //projectvicdoc1.BindVicDoc();
                //this.TabContainer1.Tabs[13].Visible = true;
            }
            //projectvicdoc1.BindProjectSale();
            //TabVicDoc.Visible = true;
            //projectvicdoc1.BindVicDoc();

        }

    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ClearProjectSelection();
    }
    public void ClearProjectSelection()
    {
        divprojecttab.Visible = false;
    }
    protected void ibtnCancelActive_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        //projectquote1.BindProjectQuote();
    }
}