﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using AjaxControlToolkit;
using System.Configuration;

public partial class employee_controls_empdocuments : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        //string EmpId = Profile.eurosolar.Empid;
        string EmpId = Request.QueryString["Id"];
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(EmpId))
            {
                BindGrid(0);
            }
            //BindDocNameDropdown();
        }

        //BindDocNameDropdown();
    }

    public void BindDocNameDropdown()
    {
        try
        {
            //string Empid = Session["Empid"].ToString();
            string Empid = Request.QueryString["Id"];

            var dataToBind = ClstblDocuments.tblEmpDocuments_SelectActive();

            DataRow dr = dataToBind.NewRow();
            dr["DocumentName"] = "Select";
            dr["Id"] = 0;
            dataToBind.Rows.Add(dr);
            dataToBind.DefaultView.Sort = "Id";
            ddlName.DataSource = dataToBind;
            ddlName.DataMember = "DocumentName";
            ddlName.DataTextField = "DocumentName";
            ddlName.DataValueField = "Id";
            ddlName.DataBind();
        }
        catch (Exception e)
        {

        }
        //string Empid = Request.QueryString["Empid"];

    }

    public void HideTabContainer()
    {
        //divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }

    public void HideContactAction()
    {
        //  GridView1.Columns[7].Visible = false;
        divrightbtn.Visible = false;

        if (Roles.IsUserInRole("Administrator"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            divrightbtn.Visible = false;
        }
    }
    public void ShowContactAction()
    {
        //GridView1.Columns[7].Visible = true;
        //divrightbtn.Visible = true;
    }

    public void BindDocuments()
    {
        HidePanels();
        // BindDocNameDropdown();
        //TabAccount.Visible = false;
        //divrightbtn.Visible = true;
        BindGrid(0);
        //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        //ddlSelectRecords.DataBind();

        string CustomerID = Request.QueryString["compid"];
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager"))
        {

            if (stCust.CustTypeID == "3" || stCust.CustTypeID == "4")
            {
                //  TabAccount.Visible = true;
            }
            else
            {
                //TabAccount.Visible = false;
            }
        }
    }
    protected DataTable GetGridData()
    {
        //string EmpId = Profile.eurosolar.Empid;
        string EmpId = Request.QueryString["Id"];
        //string EmpId = Session["EmpId"].ToString();
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(EmpId))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            // DataTable dt = ClstblDocuments.tblCustomerDocumentsDetail_SelectByUserIdCust(userid, Request.QueryString["compid"]);
            dt = ClstblDocuments.tblEmpDocumentsDetail_SelectByUserIdEmp(userid, EmpId);
            if (dt.Rows.Count == 0)
            {
                PanSearch.Visible = true;
            }

        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;
        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {


        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string EmpId = Request.QueryString["Id"];
        //string EmployeeID = Request.QueryString["Empid"];
        string DocumentId = ddlName.SelectedValue;
        string number = txtNumber.Text;
        string Filename = "";
        int success = 0;
        int exist = ClstblDocuments.tbldocuments_ExistsByEmpID(EmpId);

        //if (exist == 0)
        //{


        try
        {
            if (FileUpload1.HasFile)
            {
                string filename = hdnFileName.Value.ToString();
                string PDFFilename = string.Empty;
                Filename = FileUpload1.FileName;
                //string ID = hdnItemID.Value;
                //SttblCustomers stemp = ClstblCustomers.tblEmp_SelectByID(userid);
                PDFFilename = EmpId + "_" + Filename;
                SiteConfiguration.DeletePDFFile("EmployeeDocuments", PDFFilename);
                FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/EmployeeDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("EmployeeDocuments", PDFFilename);
                success = ClstblDocuments.tblEmployeeDocuments_Insert(EmpId, DocumentId, number, PDFFilename);
            }
            else
            {
                string filename = hdnFileName.Value.ToString();
                string PDFFilename = string.Empty;

                PDFFilename = EmpId + "_" + filename;
                SiteConfiguration.DeletePDFFile("EmployeeDocuments", PDFFilename);
                FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/EmployeeDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("EmployeeDocuments", PDFFilename);
                success = ClstblDocuments.tblEmployeeDocuments_Insert(EmpId, DocumentId, number, PDFFilename);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        SetAdd1();
        BindGrid(0);
        if (Convert.ToString(success) != "")
        {
            SetAdd();
        }
        else
        {
            SetError();
        }

    }

    protected void ibtnUploadPDF_Click(object sender, EventArgs e)
    {
        try
        {
            //System.Threading.Thread.Sleep(100000);
            if (FileUpload1.HasFile)
            {
                //SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                string PDFFilename = string.Empty;
                string Filename = FileUpload1.FileName;
                string ID = hdnItemID.Value;
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
                string CustomerID = Request.QueryString["compid"];
                //if (!string.IsNullOrEmpty(ID))
                //{
                PDFFilename = Filename;

                //if (!string.IsNullOrEmpty(st.FileName))
                //{
                //    SiteConfiguration.DeletePDFFile("StockItemPDF", PDFFilename);
                //}

                SiteConfiguration.DeletePDFFile("CustomerDocuments", PDFFilename);
                FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/CustomerDocuments/") + PDFFilename);
                SiteConfiguration.UploadPDFFile("CustomerDocuments", PDFFilename);
                //SiteConfiguration.deleteimage(PDFFilename, "CustomerDocuments");

                bool update = ClstblDocuments.tblCustomerDocuments_UpdateFileName(CustomerID, Filename);
                SetAdd1();
                BindGrid(0);
                //}
            }
        }
        catch (Exception ex)
        {
            throw ex;
            // SetError1();
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string EmployeeID = Request.QueryString["Empid"];
        string ContactEntered = DateTime.Now.AddHours(14).ToString();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string ContactEnteredBy = st2.EmployeeID;



        SttblContacts stCon = ClstblContacts.tblContacts_SelectByContactID(id1);


        ////--- do not chage this code Start------
        //if (success)
        //{
        //    SetUpdate();
        //}
        //else
        //{
        //    SetError();
        //}
        BindGrid(0);
        //--- do not chage this code end------
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    public void ClearContactSelection()
    {
        //divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ClearContactSelection();
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);
    }

    //protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

    //    if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
    //    {
    //        GridView1.AllowPaging = false;
    //        BindGrid(0);
    //    }
    //    else
    //    {
    //        GridView1.AllowPaging = true;
    //        GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
    //        BindGrid(0);
    //    }
    //}

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        //ddlName.DataSource = null;
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        BindDocNameDropdown();
        ClearContactSelection();
        InitAdd();
        divrightbtn.Visible = false;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;

        Reset();
        PanAddUpdate.Visible = false;
        divrightbtn.Visible = true;
    }
    public void SetAdd()
    {
        divrightbtn.Visible = true;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }

    [System.Web.Services.WebMethod]
    public List<Documents> geEmployees()
    {
        DataSet ds = new DataSet();
        DataTable dt = ClstblDocuments.tblEmpDocuments_SelectActive();
        List<Documents> studentList = new List<Documents>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Documents student = new Documents();
            //student.Id = (dt.Rows[i]["StudentId"].ToString();
            //student.PropertyName = dt.Rows[i]["StudentName"].ToString();
            studentList.Add(student);
        }
        ds.Tables.Add(dt);
        return studentList;
    }

    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnReset.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = true;
        btnReset.Visible = true;
        // btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = false;

        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;

        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtNumber.Text = string.Empty;
        ddlName.SelectedValue = "0";
    }
    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnStockitemID2 = (HiddenField)e.Row.FindControl("hndContactID");
            string StockItemID = hdnStockitemID2.Value;
            string CustomerID = Request.QueryString["compid"];
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblCustomers stemp = ClstblCustomers.tblCustomer_SelectByID(userid);
            if (!string.IsNullOrEmpty(StockItemID))
            {
                SttblDocuments st = ClstblDocuments.tblEmployeeDocumentsDetail_SelectByID(StockItemID);
                HyperLink lnkPDFUploded = (HyperLink)e.Row.FindControl("lnkPDFUploded");
                HyperLink lnkPDFPending = (HyperLink)e.Row.FindControl("lnkPDFPending");
                if (!string.IsNullOrEmpty(st.FileName))
                {
                    lnkPDFUploded.Visible = true;
                    lnkPDFPending.Visible = false;
                    lnkPDFUploded.NavigateUrl = pdfURL + "EmployeeDocuments/" + st.FileName;
                    lnkPDFUploded.Target = "_blank";
                }
                else
                {
                    lnkPDFUploded.Visible = false;
                    lnkPDFPending.Visible = true;
                }
            }
        }
    }

    //protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    //{
    //    // ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);
    //    //Response.Write(TabContainer2.ActiveTabIndex);
    //    //Response.End();
    //    if (TabContainer2.ActiveTabIndex == 0)
    //    {
    //        //contactdetail1.BindContactDetail();
    //    }
    //    //else if (TabContainer2.ActiveTabIndex == 1)
    //    //{
    //    //    contactinfo2.BindContactInfo();
    //    //}
    //    //else if (TabContainer2.ActiveTabIndex == 3)
    //    //{
    //    //    contactaccount2.BindCreateAccount();
    //    //}
    //}

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        SttblDocuments st = ClstblDocuments.tblCustomerDocList_SelectByID(id);
        SiteConfiguration.deleteimage(st.FileName, "EmployeeDocuments");
        bool sucess1 = ClstblDocuments.tblEmpdocument_Delete(id);

        if (sucess1)
        {
            divrightbtn.Visible = true;
            SetAdd1();
        }
        else
        {
            SetError();
        }
        Reset();
        PanAddUpdate.Visible = false;
        BindGrid(0);
    }
}

public class Documents
{
    public string Id { get; set; }
    public string PropertyName { get; set; }
}