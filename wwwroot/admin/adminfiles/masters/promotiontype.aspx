<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Theme="admin"
    CodeFile="promotiontype.aspx.cs" Inherits="admin_adminfiles_master_promotiontype" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black">Manage Promotion Type</h3>
            <div class="contactsarea">
                <div class="addcontent">
                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" class="btn btn-default btn-rounded addcontact"
                        OnClick="lnkAdd_Click"><i></i>Add</asp:LinkButton>
                </div>
                <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server">
                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                    </div>
                    <div class="alert alert-danger" id="PanError" runat="server">
                        <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                            Text="Transaction Failed."></asp:Label></strong>
                    </div>
                    <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                    </div>
                    <div class="alert alert-info" id="PanNoRecord" runat="server">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
                <div class="contacttoparea form-inline padd10all">
                    <div class="leftarea1">
                        <div class="showenteries">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Show</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myval1">
                                        </asp:DropDownList></td>
                                    <td>entries</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                     <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                    <div class="rightarea1">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="2">Not Active</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <div>
                                <span>
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                        WatermarkText="Type" />
                                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" class="btn btn-danger btnserchicon"
                                        CausesValidation="false" OnClick="btnSearch_Click"><img src="../../../images/icon_serach.png" Width="17" Height="16"/> Search</asp:LinkButton>
                                </span>
                            </div>
                        </div>
                    </div>
                         </asp:Panel>
                    <div class="clear"></div>
                </div>
                <div class="contactbottomarea">
                    <div class="tableblack">
                        <div class="table-responsive noPagination" id="PanGrid" runat="server">
                            <asp:GridView ID="GridView1" DataKeyNames="PromoTypeID" runat="server" OnRowDeleting="GridView1_RowDeleting"
                                OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging"
                                CssClass="Gridview"
OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated" OnRowCommand="GridView1_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="600px">
                                        <ItemTemplate>
                                            <%#Eval("PromoType")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seq" Visible="false" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%#Eval("Seq")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server" Checked='<%# Eval("Active")%>' />
                                            <label for="<%=chk.ClientID %>" runat="server" id="lblchk">
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandName="Select" ImageUrl="../../../images/icon_edit.png"
                                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-Width="40px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                        <ItemTemplate>
                                            < <!--DELETE Modal Templates-->

                                                                    <asp:LinkButton ID="gvbtnDelete" runat="server"  CssClass="btn btn-danger btn-xs" CausesValidation="false" 
                                                                   CommandName="Delete" CommandArgument='<%#Eval("PromoTypeID")%>' >
                                                                    <i class="fa fa-trash"></i> Delete
                                                                    </asp:LinkButton>

                                                                  

                                                                <!--END DELETE Modal Templates-->
                                        </ItemTemplate>
                                        <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#EFF3FB" />
<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
<AlternatingRowStyle BackColor="White" />
<PagerTemplate>
    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
</PagerTemplate>
                            </asp:GridView>
                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
<table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
<tr>
    <td>
	<asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
    </td>
</tr>
</table>
</div>
                    </div>
                </div>
            </div>
        </div>
        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
            CancelControlID="Button1">
        </cc1:ModalPopupExtender>
        <div id="myModal" runat="server" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                         <div style="float: right">
                        <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                             </div>
                        <h4 class="modal-title" id="myModalLabel">
                            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                            Promotion Type</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="formainline">

                                <div class="form-group">
                                    <label>Type&nbsp;</label>
                                    <asp:TextBox ID="txtstockcategory" runat="server" MaxLength="200" Width="200px" class="form-control modaltextbox"></asp:TextBox>
                                    <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" CssClass="reqerror" ErrorMessage="This value is required." ControlToValidate="txtstockcategory"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group " id="divseq" runat="server" visible="false">
                                    <span class="name">
                                        <label class="control-label">Seq&nbsp;<span class="symbol required"></span></label>
                                    </span><span>
                                        <asp:TextBox ID="txtseq" runat="server" MaxLength="3" Text="0" class="form-control"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regvalSortOrder" runat="server" ErrorMessage="Number Only"
                                            ValidationExpression="^[0-9]*$" ControlToValidate="txtseq"></asp:RegularExpressionValidator>
                                        <br /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                            ControlToValidate="txtseq" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear"></div>
                                </div>
                                <div class="form-group checkareanew">
                                    <label>Is Active?</label>
                                    <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                    <label for="<%=chk.ClientID %>">
                                        <span></span>
                                    </label>
                                    <div class="clear"></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer btnheight">
<div class="form-group" style="text-align: right;">
   <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary addwhiteicon" Text="Add" OnClick="btnAdd_Click" CausesValidation="true" />
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-purple resetbutton" Text="Reset" OnClick="btnReset_Click" CausesValidation="false" />
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary savewhiteicon" Text="Save" Visible="false" OnClick="btnUpdate_Click" CausesValidation="true" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-dark-grey calcelwhiteicon" Text="Cancel" Visible="false" OnClick="btnCancel_Click" CausesValidation="false" />
                                    <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " />
</div>
</div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
    </section>
      <!--Danger Modal Templates-->
<asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
            <!--End Danger Modal Templates-->
</asp:Content>
