﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="UploadStateData.aspx.cs" Inherits="admin_adminfiles_masters_UploadStateData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="breadCrumbs">
        <ul>
            <li class="breadCrumbLast" title="Master"><a><strong>Update Product Quentity</strong></a></li>
        </ul>
    </div>
    <div id="main">
        <div id="globalTabContent">
            <ul class="tabs-nav">
                <li class="tabs-selected"><a href="#tab-1"><span><strong>
                    <asp:Literal ID="lblAddUpdate" runat="server" Text=""></asp:Literal>
                   Update Product Quentity</strong></span></a></li>

            </ul>

            <div id="tab-1" class="tabs-container">
                <div class="curveTop">
                </div>


                <asp:Panel ID="PanAddUpdate" runat="server">
                    <div class="globalContainerCA">
                        <ul class="addNewPage" style="width: 100%;">
                            <li>
                                <label style="float: left;">
                                    <strong>Upload Excel File</strong><span class="highlight">*</span></label>

                                <asp:FileUpload ID="fuImport" runat="server" BorderStyle="Solid" BorderWidth="1px" Width="250px"
                                    Height="20px" BorderColor="#eeeeee" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="fuImport"
                                    ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX|.CSV|.csv)$" Display="Dynamic"
                                    ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="fuImport"></asp:RequiredFieldValidator>


                                <span style="margin-left: 150px;display: inline-block;">
                                    <div>
                                    <asp:HyperLink ID="hypsubscriber" ForeColor="Brown" runat="server"  
                                        NavigateUrl="~/userfiles/Excelproduct/format/Adawliah Upadte  Product Qty.xls"
                                        Target="_blank">Click here</asp:HyperLink>
                                    to download Excel Sheet format.<br /> 
                                   
                                        </div>
                                </span>
                               
                            </li>

                        </ul>
                        <ul class="addNewPageEditor">
                            <li class="floatLeft">
                                <p>
                                    <asp:Button ID="btnAdd" runat="server"
                                        CssClass="btn_bak" Text="ADD" OnClick="btnAdd_Click" />
                                </p>
                                <p>
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn_bak"
                                        OnClick="btnReset_Click" Text="RESET" CausesValidation="false" />
                                </p>
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                        <div style="padding: 10px!important; height: auto" class="myerror">

                            <asp:Panel ID="PanSuccess" runat="server" CssClass="tick" Visible="false">
                                <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                            </asp:Panel>
                                <asp:Panel ID="PanError" runat="server" CssClass="cross" Visible="false">
                                <asp:Label ID="Label1" runat="server" Text="Transaction Failed. No Credit available for this user"></asp:Label>
                                </asp:Panel>
                            <asp:Panel ID="panerrortext" runat="server" Visible="false">
                                <div runat="server" id="tbl">
                                    <table border="1" cellpadding="5" cellspacing="0" class="query_info">
                                        <tr>
                                            <td colspan="2" class="tick">
                                                <asp:Label ID="lbltextcontent" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trerror"  >
                                            <th align="left"><strong>Index</strong></th>
                                            <th align="left"><strong>Error</strong></th>
                                        </tr>
                                        <asp:Literal ID="litable" runat="server"></asp:Literal>
                                    </table>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>


                </asp:Panel>

                <%-- END    : MAIN GRID Panels --%>
                <%-- END    : Error Panels --%>
            </div>

        </div>
    </div>
</asp:Content>

