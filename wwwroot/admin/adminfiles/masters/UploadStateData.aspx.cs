﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_masters_UploadStateData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        litable.Text = "";
        lbltextcontent.Text = "";
        if (fuImport.HasFile)
        {
            if ((fuImport.FileName.EndsWith(".xls")) || (fuImport.FileName.EndsWith(".xlsx")))
            {
                ImportExcel();
            }

        }

    }


    public void ImportExcel()
    {
        string errortextcontent = "";
        errortextcontent = string.Empty;
        int success_count = 0;
        int upload_count = 0;
        int fail_count = 0;
        int failExist_count = 0;
        int i_column = 0;
        int matnum = 1;
        int availablecount = 0;

        string emailaddress = string.Empty;
        Response.Expires = 400;
        Server.ScriptTimeout = 1200;

        if (fuImport.HasFile)
        {
            try
            {
                fuImport.SaveAs(Request.PhysicalApplicationPath + "userfiles\\ExcelState\\" + fuImport.FileName);
            }
            catch
            {
                fuImport.SaveAs(Request.PhysicalApplicationPath + "userfiles\\ExcelState\\" + fuImport.FileName);
            }
            string connectionString = "";
            if (fuImport.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\ExcelState\\" + fuImport.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }
            else if (fuImport.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\ExcelState\\" + fuImport.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            //  int count = dr.FieldCount;
                            while (dr.Read())
                            {
                                i_column++;
                                string state = dr["State"].ToString().Trim().TrimStart().TrimEnd();
                                string District = dr["District"].ToString().Trim().TrimStart().TrimEnd();
                                string Taluka = dr["Taluka"].ToString().Trim().TrimStart().TrimEnd();
                                string City = dr["City"].ToString().Trim().TrimStart().TrimEnd();

                                int StateId = ClstblState.tblStatesExistInsert(state);
                                if (StateId > 0)
                                {
                                    int DistrictId = ClstblState.tbl_DistrictNameExistInsert(StateId.ToString(), District);
                                    if (DistrictId > 0)
                                    {
                                        int TalukaId = ClstblState.tbl_TalukaNameExistInsert(DistrictId.ToString(), Taluka);
                                        if (TalukaId > 0)
                                        {
                                            int CityId = ClstblState.tbl_cityExistInsert(TalukaId.ToString(), City);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        HidePanels();
    }

    private void HidePanels()
    {
        PanError.Visible = false;
        PanSuccess.Visible = false;
        panerrortext.Visible = false;
    }
}