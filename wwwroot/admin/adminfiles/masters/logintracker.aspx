<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="logintracker.aspx.cs" Inherits="admin_adminfiles_master_logintracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                     $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $("[data-toggle=tooltip]").tooltip();
                    $(".myvallogin").select2({
                        //placeholder: "select",
                        allowclear: true,
						 minimumResultsForSearch: -1
                    });
                    
                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true,
						 minimumResultsForSearch: -1
                    });

                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>

            <script>



                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Record?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },

                      function (isConfirm) {
                          if (isConfirm) {
                              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              eval(defaultAction);

                              //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              return true;
                          } else {
                              // swal("Cancelled", "Your imaginary file is safe :)", "error");
                              return false;
                          }
                      });
                }


            </script>

<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Cust/Inst Users</h5>
              <div id="hbreadcrumb" class="pull-right">
                                
                                
                                <ol class="hbreadcrumb breadcrumb fontsize16" id="ol" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>
                                </ol>
                                
                            </div>
               </div>
               
             
              <div class="finaladdupdate">
             <div id="PanAddUpdate" runat="server" visible="false">
                <div class="panel-body animate-panel padtopzero"> 
                <div class="well with-header with-footer addform">
                  	 <div class="header bordered-blue">                         
                          <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                Cust/Inst Users
                      </div>
                  </div>
               </div>
             </div>
             </div>    
                  
            <div class="page-body padtopzero">
            <asp:Panel runat="server" ID="PanGridSearch">
              
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                      <div class="searchfinal">
                         <div class="widget-body shadownone brdrgray"> 
                         <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                            <table border="0" cellspacing="0" width="100%" style="text-align: right;" cellpadding="0">
                                                        <tr>
                                                            <td class="left-text dataTables_length showdata padtopzero"><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>                                                                    
                                                                    <asp:TextBox ID="txtStartDate" runat="server" class="form-control" placeholder="Start Date">
                                                                    </asp:TextBox>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" runat="server" class="form-control" placeholder="End Date">
                                                                    </asp:TextBox>
                                                                </div>

                                                                <div class="input-group col-sm-2">
                                                                    <asp:DropDownList ID="ddlUserSelect" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvallogin">
                                                                        <asp:ListItem Value="">Select User</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                            
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="finalgrid">
                        <div class="table-responsive printArea"> 
                            <div id="PanGrid" runat="server">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView1" DataKeyNames="ID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                    OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130px">
                                                            <ItemTemplate>
                                                                <%#Eval("LogInTime", "{0: dd MMM yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="UserName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="">
                                                            <ItemTemplate>
                                                                <%#Eval("UserName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="IP Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="130px">
                                                            <ItemTemplate>
                                                                <%#Eval("IPAddress")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="LogIn Time" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="130px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLogin2" runat="server" Text='<%#Eval("LogInTime","{0:H:mm:ss tt}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="LogOut Time" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="130px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLogOutSesn" runat="server" Text=" ( Session ) " Visible='<%#  Eval("IsSession").ToString()=="True" ?true:false %>'></asp:Label>
                                                                <asp:Label ID="lblLogOut2" runat="server" Text='<%#Eval("LogOutTime","{0:H:mm:ss tt}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle />

                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                            <div class="paginationnew1" runat="server" id="divnopage">
                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                        </div>
                    </div>
            </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
