using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
//using System.Web.UI.UpdatePanel;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Web.Services;

public partial class admin_adminfiles_master_employee : System.Web.UI.Page
{
    protected string SiteURL;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        Profile.eurosolar.Empid = "";
        HidePanels();
        Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "formValidate();");

        if (!IsPostBack)
        {
            custompageIndex = 1;
            password.Visible = true;
            confpassword.Visible = true;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindGrid(0);
            txtHireDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
            //txtStartTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
            //txtEndTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
            //txtBreakTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
        }
    }
    public void BindDropDown()
    {
        ddlLocation.DataSource = ClstblEmployees.tblCompanyLocations_SelectAsc();
        ddlLocation.DataMember = "CompanyLocation";
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        ddlEmployeeStatusID.DataSource = ClstblEmployees.tblEmployeeStatus_select();
        ddlEmployeeStatusID.DataMember = "EmployeeStatus";
        ddlEmployeeStatusID.DataTextField = "EmployeeStatus";
        ddlEmployeeStatusID.DataValueField = "EmployeeStatusID";
        ddlEmployeeStatusID.DataBind();

        lstrole.DataSource = ClstblEmployees.SpRolesGetDataByAsc();
        lstrole.DataMember = "RoleName";
        lstrole.DataTextField = "RoleName";
        lstrole.DataValueField = "RoleName";
        lstrole.DataBind();

        DataTable dtTeam = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlSalesTeamID.DataSource = dtTeam;
        ddlSalesTeamID.DataValueField = "SalesTeamID";
        ddlSalesTeamID.DataTextField = "SalesTeam";
        ddlSalesTeamID.DataMember = "SalesTeam";
        ddlSalesTeamID.DataBind();

        ddlTeam.DataSource = dtTeam;
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlsearchrole.DataSource = ClstblEmployees.SpRolesGetDataByAsc();
        ddlsearchrole.DataMember = "RoleName";
        ddlsearchrole.DataTextField = "RoleName";
        ddlsearchrole.DataValueField = "RoleName";
        ddlsearchrole.DataBind();

        ddlStreetState.DataSource = ClstblState.tblStates_Select();
        ddlStreetState.DataMember = "FullName";
        ddlStreetState.DataTextField = "FullName";
        ddlStreetState.DataValueField = "FullName";
        ddlStreetState.DataBind();
    }

    protected void btnAddEmployee_Click(object sender, EventArgs e)
    {
        string jsActiveLoaderMethodName = "ActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsActiveLoaderMethodName, true);
        string EmpFirst = txtEmpFirst.Text;
        string EmpMiddle = txtEmpMiddle.Text;
        string EmpLast = txtEmpLast.Text;
        string EmpEmail = txtEmpEmail.Text;
        string EmpMobile = txtEmpMobile.Text;
        string HireDate = txtHireDate.Text;
        string Empphone = txtEmpPhone.Text;
        string PanNo = txtpanno.Text;
        string Aadharno = txtaadharno.Text;
        string SourceBy = txtsourceby.Text;
        string GstNo = txtgstno.Text;
        string Address1 = txtformbayStreetNo.Text;
        string Address2 = txtformbaystreetname.Text;
        string Address = txtformbayStreetNo.Text;
        string Taluka = ddltaluka.SelectedValue.ToString();
        string District = ddlDistrict.SelectedValue.ToString();
        string City = ddlCity.SelectedValue.ToString();
        string State = ddlStreetState.SelectedValue.ToString();
        string PinCode = txtpincode.Text;
        string username = txtuname.Text.Trim();
        string password = txtcpassword.Text.Trim();
        string StartTime = txtStartTime.Text;
        string BreakTime = txtBreakTime.Text;
        string EndTime = txtEndTime.Text;
        string EmployeeStatusID = ddlEmployeeStatusID.SelectedValue;
        string Location = ddlLocation.SelectedValue;
        string ActiveEmp = Convert.ToString(chkActiveEmp.Checked);
        string Include = Convert.ToString(chkInclude.Checked);
        string GSTPayment = Convert.ToString(chkGSTPayment.Checked);
        int exist = ClstblEmployees.Spaspnet_UsersAddDataExist(username);
        if (exist == 0)
        {
            try
            {
                Membership.CreateUser(username, password, EmpEmail);
                int success2 = ClstblEmployees.tblEmp_Insert(EmpFirst, EmpMiddle, EmpLast, EmpEmail, EmpMobile, HireDate, Empphone, Address, Taluka, District, City, State, PinCode, GstNo, PanNo, Aadharno, SourceBy, StartTime, EndTime, BreakTime);
                bool success3 = ClstblEmployees.UpdateEmployeeFields(success2, ActiveEmp, Include);
                string userid = Membership.GetUser(username).ProviderUserKey.ToString();
                //int success2 = ClstblEmployees.tblEmployees_Insert("", EmployeeStatusID, "False", "", "", "", "False", HireDate, Location, "", EmpFirst, EmpLast, "", "", "", "", EmpMobile, EmpEmail, "", EmpInitials, EmpInfo, password, TaxFileNumber, "", "", PaysOwnSuper, "", "", Include, ActiveEmp, EmpNicName, "", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", OnRoster, StartTime, EndTime, BreakTime, "", EmpABN, EmpAccountName, GSTPayment);
                bool sucUser = ClstblEmployees.tblEmployees_Update_Userid(success2.ToString(), Membership.GetUser(username).ProviderUserKey.ToString());
                bool sucTeam = ClstblEmployees.tblEmployees_Update_Team(Convert.ToString(success2), ddlSalesTeamID.SelectedValue, ddlEmpType.SelectedValue, Convert.ToString(chkLTeamOutDoor.Checked), Convert.ToString(chkLTeamCloser.Checked));

                bool suc_ShowExcel = ClstblEmployees.tblEmployees_Update_ShowExcel(success2.ToString(), chkshowexcel.Checked.ToString());
                ClstblEmployees.tblEmployees_Update_Location_Status(success2.ToString(), Address2, Location, EmployeeStatusID);
                for (int i = 0; i < ddlSalesTeamID.Items.Count; i++)
                {
                    if (ddlSalesTeamID.Items[i].Selected == true)
                    {
                        int successempteam = ClstblEmployeeTeam.tblEmployeeTeam_Insert(Convert.ToString(success2), ddlSalesTeamID.Items[i].Value.ToString());
                    }

                }
                if (ddlSalesTeamID.SelectedValue == string.Empty)
                {
                    int successempteam = ClstblEmployeeTeam.tblEmployeeTeam_Insert(Convert.ToString(success2), "0");
                }
                //   Response.End();

                foreach (ListItem itemval in lstrole.Items)
                {
                    if (itemval.Selected)
                    {
                        Roles.AddUserToRole(username, itemval.Value);
                    }
                }
                //--- do not chage this code start------
                if (Convert.ToString(success2) != "")
                {
                    SetAdd();
                }
                else
                {
                    SetError();
                }
                Reset();
            }
            catch (MembershipCreateUserException err)
            {
                //Membership.DeleteUser(username);
                lblError.Visible = true;
                lblError.Text = GetErrorMessage(err.StatusCode);
            }

        }

        else
        {
            InitAdd();
            SetExist();
            //PanAlreadExists.Visible = true;
        }
        BindGrid(0);
        string jsMethodName = "InActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
    }

    protected DataTable GetGridData()
    {
        string emptypeValue = string.Empty;
        DataTable dt = new DataTable();
       
        dt = ClstblEmployees.tblEmployeesGetDataBySearch(txtSearch.Text, ddlsearchrole.SelectedValue, ddlTeam.SelectedValue, txtusername.Text, ddlsearchemptype.SelectedValue);
        DataColumn newColumn = new System.Data.DataColumn("EmpTypeValue", typeof(System.String));
        newColumn.DefaultValue = emptypeValue;
        dt.Columns.Add(newColumn);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            emptypeValue = (dt.Rows[i]["EmpType"].ToString());

            if (emptypeValue == "1")
            {
                emptypeValue = "Employee";
            }
            if (emptypeValue == "2")
            {
                emptypeValue = "Dealer";
            }
            if (emptypeValue == "3")
            {
                emptypeValue = "Contractor";
            }
            dt.Rows[i]["EmpTypeValue"] = emptypeValue;
        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {

        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void lnk_AddDocument_Click(object sender, EventArgs e)
    {
        TabDocuments.Visible = true;
        TabSummary.Visible = false;
        TabContact.Visible = false;
    }

    //protected void btnAdd_Click(object sender, EventArgs e)
    //{
    //    string username = txtuname.Text.Trim();
    //    string password = txtcpassword.Text.Trim();
    //    string EmpFirst = txtEmpFirst.Text;
    //    string EmpLast = txtEmpLast.Text;
    //    //string EmpTitle = txtEmpTitle.Text;
    //    //string EmpInitials = txtEmpInitials.Text;
    //    string EmpEmail = txtEmpEmail.Text;
    //    string EmpMobile = txtEmpMobile.Text;
    //    //string EmpNicName = txtEmpNicName.Text;
    //    string EmployeeStatusID = ddlEmployeeStatusID.SelectedValue;
    //    string Location = ddlLocation.SelectedValue;
    //    string HireDate = txtHireDate.Text;
    //    string ActiveEmp = Convert.ToString(chkActiveEmp.Checked);
    //    string StartTime = txtStartTime.Text;

    //    string Include = Convert.ToString(chkInclude.Checked);
    //    string EndTime = txtEndTime.Text;
    //    string OnRoster = Convert.ToString(chkOnRoster.Checked);
    //    string BreakTime = txtBreakTime.Text;
    //    string PaysOwnSuper = Convert.ToString(chkPaysOwnSuper.Checked);
    //    string EmpInfo = txtEmpInfo.Text;
    //    string TaxFileNumber = txtTaxFileNumber.Text;
    //    string EmpABN = txtEmpABN.Text;
    //    string EmpAccountName = txtEmpAccountName.Text;
    //    string GSTPayment = Convert.ToString(chkGSTPayment.Checked);
    //    string empphone = txtEmpPhone.Text;
    //    //string empextno = txtEmpExtNo.Text;

    //    //int teamExist = 0;
    //    //foreach (ListItem itemval in lstrole.Items)
    //    //{
    //    //    if (itemval.Selected)
    //    //    {
    //    //        if (itemval.Value == "Sales Manager" || itemval.Value == "DSales Manager")
    //    //        {
    //    //            teamExist = ClstblEmployees.tblEmployees_TeamExist(ddlSalesTeamID.SelectedValue, ddlEmpType.SelectedValue, itemval.Value);
    //    //        }
    //    //    }
    //    //}

    //    //if (teamExist == 0)
    //    //{
    //    int exist = ClstblEmployees.Spaspnet_UsersAddDataExist(username);
    //    if (exist == 0)
    //    {
    //        try
    //        {
    //            Membership.CreateUser(username, password, EmpEmail);

    //            string userid = Membership.GetUser(username).ProviderUserKey.ToString();
    //            int success2 = ClstblEmployees.tblEmployees_Insert("", EmployeeStatusID, "False", "", "", "", "False", HireDate, Location, "", EmpFirst, EmpLast, "", "", "", "", EmpMobile, EmpEmail, "", EmpInitials, EmpInfo, password, TaxFileNumber, "", "", PaysOwnSuper, "", "", Include, ActiveEmp, EmpNicName, "", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", OnRoster, StartTime, EndTime, BreakTime, "", EmpABN, EmpAccountName, GSTPayment);
    //            bool sucUser = ClstblEmployees.tblEmployees_Update_Userid(success2.ToString(), Membership.GetUser(username).ProviderUserKey.ToString());
    //            bool sucTeam = ClstblEmployees.tblEmployees_Update_Team(Convert.ToString(success2), ddlSalesTeamID.SelectedValue, ddlEmpType.SelectedValue, Convert.ToString(chkLTeamOutDoor.Checked), Convert.ToString(chkLTeamCloser.Checked));
    //            bool sucTeam2 = ClstblEmployees.tblEmployees_Update_Phone(Convert.ToString(success2), Convert.ToString(empphone), Convert.ToString(empextno));
    //            bool suc_ShowExcel = ClstblEmployees.tblEmployees_Update_ShowExcel(success2.ToString(), chkshowexcel.Checked.ToString());
    //            bool suctitle = ClstblEmployees.tbl_employee_emptitle(Convert.ToString(success2), EmpTitle);
    //            for (int i = 0; i < ddlSalesTeamID.Items.Count; i++)
    //            {
    //                if (ddlSalesTeamID.Items[i].Selected == true)
    //                {
    //                    int successempteam = ClstblEmployeeTeam.tblEmployeeTeam_Insert(Convert.ToString(success2), ddlSalesTeamID.Items[i].Value.ToString());
    //                }

    //            }
    //            if (ddlSalesTeamID.SelectedValue == string.Empty)
    //            {
    //                int successempteam = ClstblEmployeeTeam.tblEmployeeTeam_Insert(Convert.ToString(success2), "0");
    //            }
    //            //   Response.End();

    //            foreach (ListItem itemval in lstrole.Items)
    //            {
    //                if (itemval.Selected)
    //                {
    //                    Roles.AddUserToRole(username, itemval.Value);
    //                }
    //            }

    //            //--- do not chage this code start------
    //            if (Convert.ToString(success2) != "")
    //            {
    //                SetAdd();
    //            }
    //            else
    //            {
    //                SetError();
    //            }
    //            Reset();
    //        }
    //        catch (MembershipCreateUserException err)
    //        {
    //            //Membership.DeleteUser(username);
    //            lblError.Visible = true;
    //            lblError.Text = GetErrorMessage(err.StatusCode);
    //        }

    //    }

    //    else
    //    {
    //        InitAdd();
    //        SetExist();
    //        //PanAlreadExists.Visible = true;
    //    }
    //    BindGrid(0);
    //    //}
    //    //else
    //    //{
    //    //    InitAdd();
    //    //    PanAlreadExists.Visible = true;
    //    //    lblalredyRecord.Text = "Team With this Sales Manager already exists.";
    //    //}
    //    //--- do not chage this code end------
    //}
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string jsActiveLoaderMethodName = "ActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsActiveLoaderMethodName, true);
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string EmpFirst = txtEmpFirst.Text;
        string EmpMiddle = txtEmpMiddle.Text;
        string EmpLast = txtEmpLast.Text;
        string EmpTitle = "";
        string EmpInitials = "";
        string EmpEmail = txtEmpEmail.Text;
        string EmpMobile = txtEmpMobile.Text;
        string PanNo = txtpanno.Text;
        string Aadharno = txtaadharno.Text;
        string SourceBy = txtsourceby.Text;
        string Taluka = ddltaluka.SelectedValue.ToString();
        string District = ddlDistrict.SelectedValue.ToString();
        string GstNo = txtgstno.Text;
        string EmpNicName = "";
        string EmployeeStatusID = ddlEmployeeStatusID.SelectedValue;
        string Location = ddlLocation.SelectedValue;
        string HireDate = txtHireDate.Text;
        string ActiveEmp = Convert.ToString(chkActiveEmp.Checked);
        string StartTime = txtStartTime.Text;
        string Address = txtformbayStreetNo.Text;
        string Include = Convert.ToString(chkInclude.Checked);
        string EndTime = txtEndTime.Text;
        string OnRoster = Convert.ToString(chkOnRoster.Checked);
        string BreakTime = txtBreakTime.Text;
        string PaysOwnSuper = Convert.ToString(chkPaysOwnSuper.Checked);
        string EmpInfo = txtEmpInfo.Text;
        string TaxFileNumber = txtTaxFileNumber.Text;
        string EmpABN = txtEmpABN.Text;
        string EmpAccountName = txtEmpAccountName.Text;
        string GSTPayment = Convert.ToString(chkGSTPayment.Checked);
        string empphone = txtEmpPhone.Text;
        string empextno = "";
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(id1);

        //int teamExist = 0;
        //foreach (ListItem itemval in lstrole.Items)
        //{
        //    if (itemval.Selected)
        //    {
        //        if (itemval.Value == "Sales Manager" || itemval.Value == "DSales Manager")
        //        {
        //            teamExist = ClstblEmployees.tblEmployees_TeamExistByID(ddlSalesTeamID.SelectedValue, ddlEmpType.SelectedValue, itemval.Value, st.EmployeeID);
        //        }
        //    }
        //}
        //if (teamExist == 0)
        //{
        string UserName = st.username;
        bool success = ClstblEmployees.tblEmployees_Update(id1, "", "", "False", st.SalesTeamID, "", "", "False", HireDate, Location, "", EmpFirst, EmpLast, Address, ddlCity.SelectedValue.ToString(), ddlStreetState.SelectedValue.ToString(), txtpincode.Text, EmpMobile, EmpEmail, "", EmpInitials, EmpInfo, "", TaxFileNumber, "", "", PaysOwnSuper, "", "", Include, ActiveEmp, EmpNicName, "", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", OnRoster, StartTime, EndTime, BreakTime, "", EmpABN, EmpAccountName, GSTPayment);
        //bool sucTeam = ClstblEmployees.tblEmployees_Update_Team(id1, "", ddlEmpType.SelectedValue, Convert.ToString(chkLTeamOutDoor.Checked), Convert.ToString(chkLTeamCloser.Checked));
        bool sucTeam = ClstblEmployees.tblEmployees_Update_Team(id1, ddlSalesTeamID.SelectedValue, ddlEmpType.SelectedValue, Convert.ToString(chkLTeamOutDoor.Checked), Convert.ToString(chkLTeamCloser.Checked));
        bool update = ClstblEmployees.tblEmp_Update(id1, EmpMiddle, Taluka, District, PanNo, Aadharno, SourceBy, GstNo);
        bool sucTeam2 = ClstblEmployees.tblEmployees_Update_Phone(id1, empphone, empextno);
        bool suctitle = ClstblEmployees.tbl_employee_emptitle(id1, EmpTitle);
        bool suc_ShowExcel = ClstblEmployees.tblEmployees_Update_ShowExcel(id1, chkshowexcel.Checked.ToString());
        bool regionsuccess = ClstblEmployeeTeam.tblEmployeeTeam_DeletebyEmployeeID(id1);
        ClstblEmployees.tblEmployees_Update_Location_Status(id1.ToString(), txtformbaystreetname.Text, Location, EmployeeStatusID);

        for (int i = 0; i < ddlSalesTeamID.Items.Count; i++)
        {
            if (ddlSalesTeamID.Items[i].Selected == true)
            {
                int successempteam = ClstblEmployeeTeam.tblEmployeeTeam_Insert(id1, ddlSalesTeamID.Items[i].Value.ToString());
            }
        }

        foreach (string userrole in Roles.GetAllRoles())
        {
            if ((Roles.IsUserInRole(UserName, userrole)))
            {
                Roles.RemoveUserFromRole(UserName, userrole);
            }
        }
        foreach (ListItem itemval in lstrole.Items)
        {
            if (itemval.Selected)
            {
                Roles.AddUserToRole(UserName, itemval.Value);
            }
        }
        if (success)
        {
            //InitAdd();
            SetUpdate();
        }
        else
        {
            // InitUpdate();
            SetError();
        }
        Reset();
        //}
        //else
        //{
        //    InitUpdate();
        //    PanAlreadExists.Visible = true;
        //    lblalredyRecord.Text = "Team With this Sales Manager already exists.";
        //}
        //--- do not chage this code Start------

        BindGrid(0);
        //string jsMethodName = "InActiveLoader()";
        //ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
        //--- do not chage this code end------
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        TabContainer1.ActiveTabIndex = 0;
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        Profile.eurosolar.Empid = id;
        Session["Empid"] = id;
        //Request.QueryString["Id"] = id;
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(id);
        txtuname.Text = st.username;
        //txtpassword.Text = st.password;
        txtEmpFirst.Text = st.EmpFirst;
        txtEmpLast.Text = st.EmpLast;
        txtEmpMiddle.Text = st.EmpMiddle;
        txtpanno.Text = st.PanNo;
        txtgstno.Text = st.GstNo;
        txtsourceby.Text = st.SourceBy;
        txtaadharno.Text = st.AadharNo;
        try
        {
            ddlStreetState.SelectedValue = st.EmpState;
            ddlStreetState_SelectedIndexChanged(st.EmpState, new EventArgs());
            ddlDistrict.SelectedValue = st.EmpDistrict;
            ddlDistrict_SelectedIndexChanged(st.EmpDistrict, new EventArgs());
            ddltaluka.SelectedValue = st.EmpTaluka;
            ddltaluka_SelectedIndexChanged(st.EmpTaluka, new EventArgs());
            ddlCity.SelectedValue = st.EmpCity;
        }
        catch { }

        txtformbaystreetname.Text = st.EmpAddress2;
        txtpincode.Text = st.EmpPostCode;
        //txtEmpTitle.Text = st.EmpTitle;
        // txtEmpInitials.Text = st.EmpInitials;
        txtEmpEmail.Text = st.EmpEmail;
        txtEmpMobile.Text = st.EmpMobile;
        txtEmpPhone.Text = st.EmpPhone;
        //txtEmpExtNo.Text = st.EmpExtNo;
        //password.Visible = false;
        //confpassword.Visible = false;
        txtformbayStreetNo.Text = st.EmpAddress;
        txtformbaystreetname.Text = st.EmpAddress2;
        //txtEmpNicName.Text = st.EmpNicName;
        try
        {
            ddlEmployeeStatusID.SelectedValue = st.EmployeeStatusID;
            //if (st.EmpLockStatus == "True")
            //{
            //    ddlEmployeeStatusID.SelectedValue = "Lock";
            //}
            //if (st.EmpLockStatus == "False")
            //{
            //    ddlEmployeeStatusID.SelectedValue = "UnLock";
            //}
        }
        catch { }
        txtHireDate.Text = st.HireDate;
        ddlLocation.DataSource = ClstblEmployees.tblCompanyLocations_SelectAsc();
        ddlLocation.DataMember = "CompanyLocation";
        ddlLocation.DataTextField = "CompanyLocation";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();
        try
        {
            ddlLocation.SelectedValue = st.Location;
        }
        catch { }
        chkActiveEmp.Checked = Convert.ToBoolean(st.ActiveEmp);

        chkInclude.Checked = Convert.ToBoolean(st.Include);
        chkOnRoster.Checked = Convert.ToBoolean(st.OnRoster);
        chkPaysOwnSuper.Checked = Convert.ToBoolean(st.PaysOwnSuper);

        //chkgstpayment.checked = convert.toboolean(st.gstpayment);
        chkshowexcel.Checked = Convert.ToBoolean(st.showexcel);

        //ddlSalesTeamID.SelectedValue = st.SalesTeamID;

        DataTable dtTeam = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(id);
        for (int j = 0; j < ddlSalesTeamID.Items.Count; j++)
        {
            ddlSalesTeamID.Items[j].Selected = false;
            for (int i = 0; i < dtTeam.Rows.Count; i++)
            {
                if (dtTeam.Rows[i]["SalesTeamID"].ToString() == ddlSalesTeamID.Items[j].Value)
                {
                    ddlSalesTeamID.Items[j].Selected = true;
                }
            }
        }
        //if (st.SalesTeamID == "10")
        //{
        //  divOutDoor.Visible = true;
        //  divCloser.Visible = true;
        chkLTeamOutDoor.Checked = Convert.ToBoolean(st.LTeamOutDoor);
        chkLTeamCloser.Checked = Convert.ToBoolean(st.LTeamCloser);
        //}
        //else
        //{
        //    divOutDoor.Visible = false;
        //    divCloser.Visible = false;
        //}
        try
        {
            ddlEmpType.SelectedValue = st.EmpType;
        }
        catch { }


        if (!string.IsNullOrEmpty(st.StartTime))
        {
            //txtStartTime.Text = Convert.ToDateTime(st.StartTime).ToString("hh:mm tt");
            txtStartTime.Text = Convert.ToDateTime(st.StartTime).ToString("HH:mm:ss");
        }
        else
        {
            txtStartTime.Text = string.Empty;
        }
        if (!string.IsNullOrEmpty(st.EndTime))
        {
            //txtEndTime.Text = Convert.ToDateTime(st.EndTime).ToString("hh:mm tt");
            txtEndTime.Text = Convert.ToDateTime(st.EndTime).ToString("HH:mm:ss");
        }
        else
        {
            txtEndTime.Text = string.Empty;
        }

        //MaskedEditExtenderST.Enabled = false;
        //MaskedEditExtenderET.Enabled = false;

        txtBreakTime.Text = string.Format("{0:HH:mm}", st.BreakTime);
        txtEmpInfo.Text = st.EmpInfo;
        txtTaxFileNumber.Text = st.TaxFileNumber;
        txtEmpABN.Text = st.EmpABN;
        txtEmpAccountName.Text = st.EmpAccountName;

        DataTable dt = ClstblEmployees.AdminUserRoleMasterGetData(st.userid);
        foreach (ListItem itemval in lstrole.Items)
        {
            itemval.Selected = false;
            foreach (DataRow dr in dt.Rows)
            {
                if (itemval.Value.ToString() == dr["RoleName"].ToString())
                {
                    itemval.Selected = true;
                }
            }
        }
        txtuname.Enabled = false;
        txtpassword.Enabled = false;
        txtcpassword.Enabled = false;
        //  hidepass.Visible = false;
        //   hidecpass.Visible = false;
        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
    }
    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    //[WebMethod]
    //public static bool DeleteEmployee(string empId)
    //{
    //    bool result = ClstblEmployees.tblEmployees_Delete(empId);
    //    if(result==true)
    //    {

    //    }
    //    return result;
    //}


    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {

        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);

    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());

    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        lblAddUpdate.Text = "";
        string jsMethodName = "InActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        //ModalPopupExtender2.Show();
        Reset();
        string jsMethodName = "InActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
        PanGridSearch.Visible = true;
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        //divright.Visible = true;
        //ModalPopupExtender2.Show();
        TabContact.Enabled = false;

        InitAdd();
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    protected void BindUserControls()
    {
        // divright.Visible = true;
        //divgrid.Visible = false;
        //divleft.Visible = false;
        //leftgrid.Visible = false;
        //companysummary1.BindSummary();
        ////project1.BindProjects();
        ////info1.BindInfo();
        ////contacts1.BindContact();
        ////bansi
        //lnkclose.Visible = false;
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //// ModalPopupExtender2.Hide();
        // Reset();
        // btnAdd.Visible = true;
        // btnUpdate.Visible = false;
        // btnReset.Visible = true;
        // btnCancel.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;

        // ModalPopupExtender2.Show();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
        PanGridSearch.Visible = false;
        Reset();
        txtuname.Text = "";
        txtuname.Text = string.Empty;
    }
    public void InitUpdate()
    {
        //TabDocuments.Visible = true;
        // ModalPopupExtender2.Show();
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;
        PanGridSearch.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;

        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        //PanAddUpdate.Visible = true;
        txtformbayStreetNo.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        txtEmpABN.Text = string.Empty;
        txtusername.Text = string.Empty;
        txtpassword.Text = string.Empty;
        txtHireDate.Text = string.Empty;
        txtEmpAccountName.Text = string.Empty;
        txtEmpFirst.Text = string.Empty;
        txtEmpLast.Text = string.Empty;
        txtEmpMiddle.Text = string.Empty;
        txtpincode.Text = string.Empty;
        ddltaluka.SelectedValue = "0";
        ddlDistrict.SelectedValue = "0";
        ddlStreetState.SelectedValue = "0";
        txtpanno.Text = string.Empty;
        txtgstno.Text = string.Empty;
        txtaadharno.Text = string.Empty;
        txtsourceby.Text = string.Empty;
        //txtEmpInitials.Text = string.Empty;
        txtEmpEmail.Text = string.Empty;
        txtEmpMobile.Text = string.Empty;
        ddlEmployeeStatusID.SelectedValue = "";
        txtHireDate.Text = string.Empty;
        ddlLocation.SelectedValue = "";
        txtEmpInfo.Text = string.Empty;
        //txtEmpNicName.Text = string.Empty;

        txtuname.Text = string.Empty;
        txtuname.Text = "";
        //txtEmpExtNo.Text = string.Empty;
        txtEmpPhone.Text = string.Empty;
        /// txtEmpTitle.Text = string.Empty;
        txtTaxFileNumber.Text = string.Empty;
        txtuname.Enabled = true;
        chkActiveEmp.Checked = false;
        chkInclude.Checked = false;
        chkOnRoster.Checked = false;
        chkPaysOwnSuper.Checked = false;
        chkGSTPayment.Checked = false;
        chkshowexcel.Checked = false;
        //ddlSalesTeamID.SelectedValue = "";
        for (int i = 0; i < ddlSalesTeamID.Items.Count; i++)
        {
            ddlSalesTeamID.Items[i].Selected = false;
        }
        ddlEmpType.SelectedValue = "0";
        foreach (ListItem itemval in lstrole.Items)
        {
            itemval.Selected = false;
        }
        chkLTeamOutDoor.Checked = false;
        chkLTeamCloser.Checked = false;
        //txtStartTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
        //txtEndTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
        //txtBreakTime.Text = string.Format("{0:HH:mm}", DateTime.Now.AddHours(14));
        //txtHireDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        txtStartTime.Text = string.Empty;
        txtEndTime.Text = string.Empty;
        txtBreakTime.Text = string.Empty;


        //PanAddUpdate.Visible = true;
        //lnkBack.Visible = true;
        //lnkAdd.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        string jsMethodName = "InActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
    }
    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "User with this name already exists. Please enter a different User Name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A E-mail with address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }

    [System.Web.Services.WebMethod]
    public static string geEmployee()
    {
        DataSet ds = new DataSet();
        DataTable dt = ClstblDocuments.tblEmpDocuments_SelectActive();
        ds.Tables.Add(dt);
        return ds.GetXml();
    }

    [System.Web.Services.WebMethod]
    public static List<DocumentsList> geEmployees()
    {
        DataSet ds = new DataSet();
        DataTable dt = ClstblDocuments.tblEmpDocuments_SelectActive();
        List<DocumentsList> DocumentsLists = new List<DocumentsList>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DocumentsList Document = new DocumentsList();
            Document.Id = dt.Rows[i]["Id"].ToString();
            Document.PropertyName = dt.Rows[i]["DocumentName"].ToString();
            DocumentsLists.Add(Document);
        }
        ds.Tables.Add(dt);
        return DocumentsLists;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "UnLock")
        {
            string Empid = e.CommandArgument.ToString();
            HfEmpId.Value = Empid;
            ModalPopupExtenderUnlock.Show();
        }
        if (e.CommandName == "AddDocument")
        {
            ddlName.DataSource = "";
            ModalPopupExtenderDetails.Show();
            string Empid = e.CommandArgument.ToString();
            //string HfEmpId = Empid;
            HfEmpId.Value = Empid;
            try
            {
                ddlName.DataSource = ClstblDocuments.tblEmpDocuments_SelectActive();
                ddlName.DataMember = "DocumentName";
                ddlName.DataTextField = "DocumentName";
                ddlName.DataValueField = "Id";
                ddlName.DataBind();
            }
            catch
            {
            }

        }
        if (e.CommandName == "Reset")
        {
            string Empid = e.CommandArgument.ToString();
            HfEmpId.Value = Empid;
            ModalPopupExtenderDelete.Show();
        }
        if (e.CommandName == "Lock")
        {
            string Empid = e.CommandArgument.ToString();
            HfEmpId.Value = Empid;
            ModalPopupExtenderLock.Show();
        }


        if (e.CommandName == "Delete")
        {
            string id = e.CommandArgument.ToString();
            hdndelete.Value = id;
            ModalPopupExtenderDelete.Show();
            BindGrid(0);
        }
        BindGrid(0);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = string.Empty;
        if (hdndelete.Value.ToString() != string.Empty)
        {
            id = hdndelete.Value.ToString();
            bool result = ClstblEmployees.tblEmployees_Delete(id);
        }
       
        GridView1.EditIndex = -1;
        BindGrid(0);

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        modal_danger.Visible = true;
        BindGrid(0);
    }
    protected void ddlSalesTeamID_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlSalesTeamID.SelectedValue != string.Empty)
        //{
        //    if (ddlSalesTeamID.SelectedValue == "10")
        //    {
        //        divOutDoor.Visible = true;
        //        divCloser.Visible = true;
        //    }
        //    else
        //    {
        //        divOutDoor.Visible = false;
        //        divCloser.Visible = false;
        //    }
        //}
        //else
        //{
        //    divOutDoor.Visible = false;
        //    divCloser.Visible = false;
        //}
    }
    
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType.ToString() == DataControlRowType.DataRow.ToString())
        {
            Repeater rptTeam = (Repeater)e.Row.FindControl("rptTeam");//rptrole
            Repeater rptrole = (Repeater)e.Row.FindControl("rptrole");//rptrole
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            HiddenField hidstatus = (HiddenField)e.Row.FindControl("hndEmployeeID");
            //HtmlGenericControl divstatus = (HtmlGenericControl)e.Row.FindControl("divstatus");

            //if (hidstatus.Value == "Casual")
            //{
            //    divstatus.Attributes.Add("Class", "badge casual");
            //}
            //if (hidstatus.Value == "Full Time")
            //{
            //    divstatus.Attributes.Add("Class", "badge fulltime");
            //}
            //if (hidstatus.Value == "Part Time")
            //{
            //    divstatus.Attributes.Add("Class", "badge parttime");
            //}
            DataTable dtcat = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
            if (dtcat.Rows.Count > 0)
            {
                //Response.Write(rptTeam);
                //Response.End();
                rptTeam.DataSource = dtcat;
                rptTeam.DataBind();
            }
            DataTable dtc_at = ClstblEmployeeTeam.tblEmployees_Selectbyroles(hndEmployeeID.Value);
            if (dtc_at.Rows.Count > 0)
            {
                //Response.Write(rptTeam);
                //Response.End();
                rptrole.DataSource = dtc_at;
                rptrole.DataBind();
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    public static bool IsBetween(DateTime startTime, DateTime endTime)
    {

        if (startTime.TimeOfDay <= endTime.TimeOfDay)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
    protected void TabContainer1_ActiveTabChanged1(object sender, EventArgs e)
    {

    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        // BindScript();
        //PanGrid.Visible = false;
        if (TabContainer1.ActiveTabIndex != 1)
        {
            // PanGrid.Visible = false;
            // role1.HideTabContainer();
        }
        if (TabContainer1.ActiveTabIndex != 2)
        {
            //  PanGrid.Visible = false;
        }

        if (TabContainer1.ActiveTabIndex == 0)
        {
            //  PanGrid.Visible = false;
            // personaldetails1.BindSummary();
            //contacts1.Visible = false;
            //project1.Visible = false;
            //info1.Visible = false;
            //conversation1.Visible = false;
            //companysummary1.Visible = true;
        }
        else if (TabContainer1.ActiveTabIndex == 1)
        {
            // PanGrid.Visible = false;
            //Profile.eurosolar.contactid = string.Empty;
            //role1.BindRole();
            //personaldetails1.Visible = false;
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            // PanGrid.Visible = false;
            //Profile.eurosolar.projectid = string.Empty;
            // project1.BindProjects();
        }
        else if (TabContainer1.ActiveTabIndex == 3)
        {
            doc1.BindDocuments();
            // PanGrid.Visible = false;
            //info1.BindInfo();
        }
        else if (TabContainer1.ActiveTabIndex == 4)
        {
            // PanGrid.Visible = false;
            //conversation1.BindData();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        TabContainer1.ActiveTabIndex = 1;
        TabContact.Enabled = true;
        TabContact.Visible = true;
        //Response.Write("CLICKED.....");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        string EmpId = HfEmpId.Value;

        //string EmployeeID = Request.QueryString["Empid"];
        string DocumentId = ddlName.SelectedValue;
        string number = txtNumber.Text;
        string Filename = "";
        int success = 0;
        int exist = ClstblDocuments.tbldocuments_ExistsByEmpID(EmpId);

        //if (exist == 0)
        //{
        if (FileUpload1.HasFile)
        {
            string PDFFilename = string.Empty;
            Filename = FileUpload1.FileName;
            //string ID = hdnItemID.Value;
            //SttblCustomers stemp = ClstblCustomers.tblEmp_SelectByID(userid);
            PDFFilename = EmpId + "_" + Filename;
            SiteConfiguration.DeletePDFFile("EmployeeDocuments", PDFFilename);
            FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/EmployeeDocuments/") + PDFFilename);
            SiteConfiguration.UploadPDFFile("EmployeeDocuments", PDFFilename);
            success = ClstblDocuments.tblEmployeeDocuments_Insert(EmpId, DocumentId, number, PDFFilename, userid);
        }
        try
        {
            ddlName.DataSource = "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
        SetAdd1();
        BindGrid(0);
        if (Convert.ToString(success) != "")
        {
            SetAdd();
        }
        else
        {
            SetError();
        }

    }

    protected void ddlStreetState_SelectedIndexChanged(object sender, EventArgs e)
    {
        string state = ddlStreetState.SelectedValue;
        if (!string.IsNullOrEmpty(state))
        {
            ddlDistrict.Items.Clear();
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "Select Distric";
            ddlDistrict.Items.Add(lst);

            ddlDistrict.DataSource = ClstblState.tbl_DistrictName_ByStateName(state);
            ddlDistrict.DataMember = "DistrictName";
            ddlDistrict.DataTextField = "DistrictName";
            ddlDistrict.DataValueField = "DistrictName";
            ddlDistrict.DataBind();
        }
        string jsMethodName = "InActiveLoader()";
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", jsMethodName, true);
        //ddlDistrict
    }

    protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
    {
        string District = ddlDistrict.SelectedValue;
        if (!string.IsNullOrEmpty(District))
        {
            ddltaluka.Items.Clear();
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "Select Taluka";
            ddltaluka.Items.Add(lst);

            ddltaluka.DataSource = ClstblState.tbl_TalukaName_ByStateName(District);
            ddltaluka.DataMember = "TalukaName";
            ddltaluka.DataTextField = "TalukaName";
            ddltaluka.DataValueField = "TalukaName";
            ddltaluka.DataBind();
        }
    }

    protected void ddltaluka_SelectedIndexChanged(object sender, EventArgs e)
    {
        string taluka = ddltaluka.SelectedValue;
        if (!string.IsNullOrEmpty(taluka))
        {
            ddlCity.Items.Clear();
            ListItem lst = new ListItem();
            lst.Value = "0";
            lst.Text = "Select City";
            ddlCity.Items.Add(lst);

            ddlCity.DataSource = ClstblState.tbl_city_ByTalukaName(taluka);
            ddlCity.DataMember = "CityName";
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityName";
            ddlCity.DataBind();
        }
    }

    protected void lnkresetassword_Click(object sender, EventArgs e)
    {
        string EmployeeID = HfEmpId.Value;
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(EmployeeID);
        Random random = new Random();
        string Password = Convert.ToString(random.Next(000000, 999999));
        bool suc = ClstblEmployees.tblEmployees_UpdatePassword(stEmp.userid, Password);
        if (suc)
        {
            string from = ClsAdminUtilities.StUtilitiesGetDataStructById("1").from.ToString();
            TextWriter txtWriter = new StringWriter() as TextWriter;

            Server.Execute("~/mailtemplate/resetpassword.aspx?username=" + stEmp.username + "&password=" + Password, txtWriter);
            try
            {
                //Response.Write(txtWriter.ToString());
                //Response.End();
                Utilities.SendMail(from, stEmp.EmpEmail, "Reset Password", txtWriter.ToString());
            }
            catch
            {
            }
        }
        BindGrid(0);
    }

    protected void btnunlock_Click(object sender, EventArgs e)
    {
        string EmployeeID = HfEmpId.Value;
        ClstblEmployees.aspnetUsers_UnLock(EmployeeID);
        BindGrid(0);
    }

    protected void lnklock_Click(object sender, EventArgs e)
    {
        string EmployeeID = HfEmpId.Value;
        ClstblEmployees.aspnetUsers_Lock(EmployeeID);
        BindGrid(0);
    }
}
public class DocumentsList
{
    public string Id { get; set; }
    public string PropertyName { get; set; }
}