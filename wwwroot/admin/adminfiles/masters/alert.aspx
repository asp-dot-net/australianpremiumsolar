﻿<div id="divVendor" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header text-center">
                            
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                      <div class="form-group col-md-12">
                                        <asp:Label ID="Label10" runat="server" class="col-sm-4 control-label">
                                                <strong>Name:</strong></asp:Label>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContFirst" runat="server" placeholder="First Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass="" 
                                                        ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="vendor"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtContLast" runat="server" placeholder="Last Name" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                  
                                   
                                    <%--<div class="form-group center-text col-sm-12">
                                        <asp:Button ID="ibtnAddVendor" runat="server" Text="Add" OnClick="ibtnAddVendor_Click"
                                                    CssClass="btn btn-primary savewhiteicon" ValidationGroup="stock" />
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>