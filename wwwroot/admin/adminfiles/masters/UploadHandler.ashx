﻿<%@ WebHandler Language="C#" Class="UploadHandler" %>

using System;
using System.Web;

public class UploadHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection SelectedFiles = context.Request.Files;
            for (int i = 0; i < SelectedFiles.Count; i++)

            {
                HttpPostedFile PostedFile = SelectedFiles[i];
                string FileName = context.Server.MapPath("~/userfiles/EmployeeDocuments/" + PostedFile.FileName);
                PostedFile.SaveAs(FileName);
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}