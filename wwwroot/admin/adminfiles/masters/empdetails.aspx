<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Theme="admin" CodeFile="empdetails.aspx.cs" Inherits="admin_adminfiles_master_empdetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/controls/empdocuments.ascx" TagName="docc" TagPrefix="uc6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <script>
        function InActiveLoader() {
            //hide the modal popup - the update progress
            $('.loading-container').addClass('loading-inactive');
        }
        function ActiveLoader() {
            //shows the modal popup - the update progress
            $('.loading-container').removeClass('loading-inactive');
        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-users icon"></i>Manage Employee</h5>
        <div id="hbreadcrumb">
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>

            <%--<asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="ActiveLoader()" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>--%>

        </div>
    </div>

    <div class="addEmployee">
        <div class="bodymianbg finaladdupdate formGrid detailPage">
            <div class="row">
                <div class="col-md-12">
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                        <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Personal" TabIndex="0">
                            <ContentTemplate>
                                <div class="table-responsive">

                                    <div class="centerFormTable">
                                        <h4 class="formHeading">Employee Details</h4>
                                        <div class="row">

                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lblemp" runat="server" class="control-label">
                                               Emp Type </asp:Label>

                                                <asp:Label ID="lblemptype" runat="server" class="form-control"></asp:Label>

                                            </div>
                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lblsb" runat="server" class="control-label">
                                               Source By</asp:Label>
                                                <asp:Label ID="lblsourceby" runat="server" class="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblFirstName" runat="server" class="control-label">
                                               First Name</asp:Label>
                                                <asp:Label ID="lblfirst" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblmn" runat="server" class="control-label">
                                            Middle Name</asp:Label>
                                                <asp:Label ID="lblMiddleName" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblLastName" runat="server" class="control-label">
                                               Last Name</asp:Label>
                                                <asp:Label ID="lbllast" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lblEmail" runat="server" class="control-label">
                                               Email</asp:Label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                                    <asp:Label ID="lblEmpEmail" runat="server" class="form-control"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-3">
                                                <asp:Label ID="lblmobile" runat="server" class="control-label">
                                               Mobile</asp:Label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>
                                                    <asp:Label ID="lblmobilenum" runat="server" class="form-control"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-3">
                                                <asp:Label ID="lblphnno" runat="server" class="control-label">
                                               Contact No</asp:Label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                    <asp:Label ID="lblphonenum" runat="server" class="form-control"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lbladd" runat="server" class="control-label">
                                               Address 1</asp:Label>
                                                <asp:Label ID="lbladdress" runat="server" class="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="Label1" runat="server" class="control-label">
                                               Address 2</asp:Label>
                                                <asp:Label ID="lbladdress2" runat="server" class="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lblstatname" runat="server" class="control-label">
                                               State</asp:Label>
                                                <asp:Label ID="lblstate" runat="server" class="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <asp:Label ID="lbld" runat="server" class="control-label">
                                               District</asp:Label>
                                                <asp:Label ID="lbldistrict" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblt" runat="server" class="control-label"> Taluka </asp:Label>
                                                <asp:Label ID="lbltaluka" runat="server" class="form-control"></asp:Label>
                                            </div>



                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblcity" runat="server" class="control-label">
                                               City</asp:Label>
                                                <asp:Label ID="labelcity" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblpinc" runat="server" class="control-label">
                                               Pincode</asp:Label>
                                                <asp:Label ID="lblpincode" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="dividerLine"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h4 class="formHeading">Tax Details</h4>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblano" runat="server" class="control-label">
                                               AadharCard No</asp:Label>
                                                <asp:Label ID="lblaadharno" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblpno" runat="server" class="control-label">
                                               Pan No</asp:Label>
                                                <asp:Label ID="lblpanno" runat="server" class="form-control"></asp:Label>
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <asp:Label ID="lblgno" runat="server" class="control-label">
                                               Gst No</asp:Label>
                                                <asp:Label ID="lblgstno" runat="server" class="form-control"></asp:Label>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Role">
                            <ContentTemplate>

                                <div class="centerFormTable">
                                    <h4 class="formHeading">Employee Roles & Settings</h4>
                                    <div class="row">

                                        <div class="form-group col-sm-6">
                                            <asp:Label ID="lblempstatusa" runat="server" class="control-label">
                                               User Status </asp:Label>
                                            <asp:Label ID="lblempstatus" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <asp:Label ID="Label13" runat="server" class="control-label">
                                               Location</asp:Label>
                                            <asp:Label ID="lbllocation" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="lblrolename" runat="server" class="control-label">
                                               Role</asp:Label>
                                            <asp:Label ID="lblrole" runat="server" class="form-control"></asp:Label>
                                        </div>

                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="Label2" runat="server" class="control-label">
                                               SalesTeam</asp:Label>

                                            <span class="form-control">
                                                <asp:Repeater ID="rptTeam" runat="server">
                                                    <ItemTemplate><%# Eval("SalesTeam")%> </ItemTemplate>
                                                    <SeparatorTemplate>, </SeparatorTemplate>
                                                </asp:Repeater>
                                            </span>

                                        </div>

                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="Label31" runat="server" class="control-label">
                                               Date Hired</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                                <asp:Label ID="lblhireddate" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <asp:Label ID="Label11" runat="server" class="control-label">
                                               UserName</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                <asp:Label ID="lblusername" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <asp:Label ID="Label3" runat="server" class="control-label">
                                               Password</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                                <asp:Label ID="lblpassword" Text="********" Enabled="false" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="Label4" runat="server" class="control-label">
                                               Start Time</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock"></i></span></div>
                                                <asp:Label ID="lblstarttime" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="Label10" runat="server" class="control-label">
                                               End Time</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock"></i></span></div>
                                                <asp:Label ID="lblendtime" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <asp:Label ID="Label14" runat="server" class="control-label">
                                               Break Time</asp:Label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock"></i></span></div>
                                                <asp:Label ID="lblbreaktime" runat="server" class="form-control"></asp:Label>
                                            </div>
                                        </div>

                                        <%--<div class="form-group col-sm-6">
                            <asp:Label ID="lblteam" runat="server" class="control-label">
                                               Team OutDoor</asp:Label>
                                <asp:Label ID="lblteamoutdoor"  runat="server" class="form-control"></asp:Label>
                        </div>

                       <div class="form-group col-sm-6">
                            <asp:Label ID="lblclose" runat="server" class="control-label">
                                               Team Closer</asp:Label>
                                <asp:Label ID="lblclosers"  runat="server" class="form-control"></asp:Label>
                        </div>

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblactive" runat="server" class="control-label">
                                               Active Employee</asp:Label>
                                <asp:Label ID="lblactiveemployee"  runat="server" class="form-control"></asp:Label>
                        </div>

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblinclude" runat="server" class="control-label">
                                               Include in Lists</asp:Label>
                                <asp:Label ID="lblincludeinlist"  runat="server" class="form-control"></asp:Label>
                        </div>

                        

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblshow" runat="server" class="control-label">
                                               Show Excel</asp:Label>
                                <asp:Label ID="lblshowexcel"  runat="server" class="form-control"></asp:Label>
                        </div>
                            
                                        --%>
                                    </div>
                                    <div class="col-sm-12 checkBoxSection">
                                        <div class="row">
                                            <div class="form-group col-sm-3">
                                                <label class="control-label">D2D</label>
                                            </div>
                                            <div class="form-group col-sm-2 checkBox checkBoxList">
                                                <asp:Label ID="Label12" runat="server" class="control-label"> OutDoor</asp:Label>
                                                <asp:CheckBox ID="chkLTeamOutDoor" runat="server" onclick="return false;" />
                                                <label for="<%=chkLTeamOutDoor.ClientID %>" style="width: 70px;"></label>
                                            </div>
                                            <div class="form-group col-sm-2 checkBox checkBoxList">
                                                <asp:Label ID="Label5" runat="server" class="control-label"> Closer</asp:Label>
                                                <asp:CheckBox ID="chkLTeamCloser" runat="server" onclick="return false;" />
                                                <label for="<%=chkLTeamCloser.ClientID %>" style="width: 70px;"></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-3">
                                                <label class="control-label">User Status</label>
                                            </div>
                                            <div class="form-group col-sm-8 checkBox checkBoxList">

                                                <asp:Label ID="Label25" runat="server" class="control-label">Active&nbsp;Employee</asp:Label>
                                                <asp:CheckBox ID="chkActiveEmp" runat="server" onclick="return false;" />
                                                <label for="<%=chkActiveEmp.ClientID %>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col-sm-3">
                                                <label class="control-label">List Dropdown</label>
                                            </div>
                                            <div class="form-group col-sm-8 checkBox checkBoxList">
                                                <asp:Label ID="Label24" runat="server" class="control-label">Include&nbsp;in&nbsp;Lists</asp:Label>
                                                <asp:CheckBox ID="chkInclude" runat="server" onclick="return false;" />
                                                <label for="<%=chkInclude.ClientID %>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-3">
                                                <label class="control-label">Download Option</label>
                                            </div>
                                            <div class="form-group col-sm-8 checkBox checkBoxList">
                                                <asp:Label ID="Label26" runat="server" class="control-label">Show Excel</asp:Label>
                                                <asp:CheckBox ID="chkshowexcel" runat="server" onclick="return false;" />
                                                <label for="<%=chkshowexcel.ClientID %>">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <asp:Label ID="Label8" runat="server" class="control-label">
                                               Info</asp:Label>
                                                <asp:Label ID="lblinfo" runat="server" class="form-control" Height="100px"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabDocuments" runat="server" HeaderText="Documents" TabIndex="1">
                            <ContentTemplate>
                                <uc6:docc ID="doc1" runat="server" />
                            </ContentTemplate>
                        </cc1:TabPanel>

                    </cc1:TabContainer>
                </div>
            </div>
        </div>
    </div>


    <div class="finaladdupdate formGrid detailPage">
        <div id="PanAddUpdate" runat="server">
            <div class="panel-body animate-panel padtopzero">
                <div class="with-header with-footer addform">


                    <div class="centerFormTable wid80">

                        <div class="row">

                            <%--<div class="form-group">
                            <asp:Label ID="lbltitlename" runat="server" class="control-label">
                               Title</asp:Label>
                            <div class="col-md-3">
                                <asp:Label ID="lbltitile"  runat="server" class="control-label"></asp:Label>
                            </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lblinitialsname" runat="server" class="control-label">
                                   Initials</asp:Label>
                                <div class="col-md-3">
                                    <asp:Label ID="lblinitials"  runat="server" class="control-label"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <asp:Label ID="lblEmail" runat="server" class="control-label">
                                               Email</asp:Label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                    <asp:Label ID="lblEmpEmail"  runat="server" class="form-control"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <asp:Label ID="lblmobile" runat="server" class="control-label">
                                               Mobile</asp:Label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>
                                <asp:Label ID="lblmobilenum"  runat="server" class="form-control"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group col-sm-3">
                            <asp:Label ID="lblphnno" runat="server" class="control-label">
                                               Phone</asp:Label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                <asp:Label ID="lblphonenum"  runat="server" class="form-control"></asp:Label>
                            </div>
                        </div>
                        
                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblano" runat="server" class="control-label">
                                               AadharCard No</asp:Label>
                                <asp:Label ID="lblaadharno"  runat="server" class="form-control"></asp:Label>
                        </div>

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblpno" runat="server" class="control-label">
                                               Pan No</asp:Label>
                                <asp:Label ID="lblpanno"  runat="server" class="form-control"></asp:Label>
                        </div>

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblgno" runat="server" class="control-label">
                                               Gst No</asp:Label>
                                <asp:Label ID="lblgstno"  runat="server" class="form-control"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4">
                            <asp:Label ID="Label16" runat="server" class="control-label">
                                               Date Hired</asp:Label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <asp:Label ID="lblhireddate"  runat="server" class="form-control"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group col-sm-4">
                            <asp:Label ID="lblsb" runat="server" class="control-label">
                                               Source By</asp:Label>
                                <asp:Label ID="lblsourceby"  runat="server" class="form-control"></asp:Label>
                        </div>

                        <%--<div class="form-group">
                            <asp:Label ID="lblphnex" runat="server" class="control-label">
                               PhoneExtNo</asp:Label>
                            <div class="col-sm-6">
                                <asp:Label ID="lblphoneextno"  runat="server" class="control-label"></asp:Label>
                            </div>
                        </div>--%>

                            <%--<div class="form-group">
                            <asp:Label ID="lblnic" runat="server" class="control-label">
                               Nic Name </asp:Label>
                            <div class="col-sm-6">
                                <asp:Label ID="lblnicname"  runat="server" class="control-label"></asp:Label>
                            </div>
                        </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

