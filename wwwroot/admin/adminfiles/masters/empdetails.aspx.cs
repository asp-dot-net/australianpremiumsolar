﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_master_empdetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            string EmployeeID = Request.QueryString["id"];
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(EmployeeID);
            lblemptype.Text = st.EmpType;
            if (st.EmpType == "1")
            {
                lblemptype.Text = "Employee";
            }
            else if (st.EmpType == "2")
            {
                lblemptype.Text = "Dealer";
            }
            else
            {
                lblemptype.Text = "Contractor";
            }
            lblpincode.Text = st.EmpPostCode;
            lbladdress.Text = st.EmpAddress;
            lblstate.Text = st.EmpState;
            labelcity.Text = st.EmpCity;
            lblrole.Text = st.RoleName;
            lblpanno.Text = st.PanNo;
            lblgstno.Text = st.GstNo;
            lblaadharno.Text = st.AadharNo;
            lblsourceby.Text = st.SourceBy;
            lbltaluka.Text = st.EmpTaluka;
            lbldistrict.Text = st.EmpDistrict;
            lblinfo.Text = st.EmpInfo;
            //lblsalesteam.Text = st.TeamName;
            lblfirst.Text = st.EmpFirst;
            lblMiddleName.Text = st.EmpMiddle;
            lbllast.Text = st.EmpLast;
            //lbltitile.Text = st.EmpTitle;
            //lblinitials.Text = st.EmpInitials;
            lblEmpEmail.Text = st.EmpEmail;
            lblmobilenum.Text = st.EmpMobile;
            lblphonenum.Text = st.EmpPhone;
            lbladdress2.Text = st.EmpAddress2;
            //lblphoneextno.Text = st.EmpExtNo;
            //lblnicname.Text = st.EmpNicName;
            lblempstatus.Text = st.EmpStatusName;
            if (st.LTeamOutDoor == "True")
            {

                //lblteamoutdoor.Text = "Yes";
                chkLTeamOutDoor.Checked = true;
                
               

            }
            else
            {
                //lblteamoutdoor.Text = "No";
                chkLTeamOutDoor.Checked = false;
                chkLTeamOutDoor.Enabled = false;
                
            }
            //Response.Write(st.LTeamOutDoor);
            //Response.End();
            if (st.LTeamCloser == "True")
            {
                //lblclosers.Text = "Yes";
                chkLTeamCloser.Checked = true;
            }
            else
            {
                //lblclosers.Text = "No";
                chkLTeamCloser.Checked = false;
                chkLTeamOutDoor.Enabled = false;
            }
            if (st.Include == "True")
            {
                //lblincludeinlist.Text = "Yes";
                chkInclude.Checked = true;
            }
            else
            {
                //lblincludeinlist.Text = "No";
                chkInclude.Checked = false;
                chkInclude.Enabled = false;
            }
            if (st.ActiveEmp == "True")
            {
                //lblactiveemployee.Text = "Yes";
                chkActiveEmp.Checked = true;
            }
            else
            {
                //lblactiveemployee.Text = "No";
                chkActiveEmp.Checked = false;
            }
            //chkActiveEmp.Enabled = false;
            if (st.showexcel == "True")
            {
                //lblshowexcel.Text = "Yes";
                chkshowexcel.Checked = true;
            }
            else
            {
                //lblshowexcel.Text = "No";
                chkshowexcel.Checked = false;
                chkshowexcel.Enabled = false;
            }

            // lblrole.Text = st.
            //if (st.EmpType == "1")
            //{
            //    lblemptype.Text = "Australian Premium Solar";
            //}
            //else
            //{
            //    lblemptype.Text = "Door to Door";
            //}

            //  lblstate.Text = st.StatusName;
            DataTable dtcat = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(EmployeeID);
            if (dtcat.Rows.Count > 0)
            {
                //Response.Write(rptTeam);
                //Response.End();
                rptTeam.DataSource = dtcat;
                rptTeam.DataBind();
            }
            lbllocation.Text = st.LocationName;
            lblusername.Text = st.username;

            lblstarttime.Text = string.Format("{0:HH:mm}", st.StartTime);
            lblendtime.Text = string.Format("{0:HH:mm}", st.EndTime);
            lblbreaktime.Text = string.Format("{0:HH:mm}", st.BreakTime);
            try
            {
                lblhireddate.Text = Convert.ToDateTime(st.HireDate).ToShortDateString();
            }
            catch { }

            lblinfo.Text = st.EmpInfo;


        }
    }


    protected void lnkBack_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "uniqueKey", "InActiveLoader()", true);
        Response.Redirect("~/admin/adminfiles/masters/employee.aspx");
    }
}