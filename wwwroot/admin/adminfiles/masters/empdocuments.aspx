﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="empdocuments.aspx.cs" Inherits="admin_adminfiles_masters_empdocuments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <style>
        .contactsarea {
            position: relative;
        }
    </style>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>

    <script>
        $(document).ready(function () {
            //  alert('kjdhk');

            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });

        });
        function doMyAction() {
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
            loader();
        }
        function loader() {
            $(window).load(function () {
            });
        }

    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedcontacttab);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    $('.loading-container').css('display', 'none');
                    $(".myvalcont").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                }
                function pageLoadedcontacttab() {


                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Documents</h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBackEmployee_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back to Employee</asp:LinkButton>
                    </ol>
                </div>
            </div>
            <section>

                <div class="col-md-12">
                    <div class="contactsarea">
                        <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">
                            <div class="widget flat radius-bordered borderone">
                                <div class="widget-header bordered-bottom bordered-blue">
                                    <span class="widget-caption">
                                        <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Documents</span>
                                </div>
                                <div class="widget-body">
                                    <div class="lightgraybgarea">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group spicaldivin " id="div1" runat="server">
                                                        <div class="col-sm-12">
                                                            <div class="row marginbtm15">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <asp:Label ID="lblLastName" runat="server">
                                                Document Name</asp:Label>
                                                                        <div class="marginbtm15">
                                                                            <asp:DropDownList ID="ddlName" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <br />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                                                                ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Label ID="Label1" runat="server">
                                                Document Number</asp:Label>
                                                                    <asp:TextBox ID="txtNumber" runat="server" MaxLength="30" CssClass="form-control"
                                                                        placeholder="Number"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                        ControlToValidate="txtNumber" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="formainline">

                                                                        <div class="topfileuploadbox marbtm15">

                                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                                <div style="word-wrap: break-word;">
                                                                                    <span class="name">
                                                                                        <label class="control-label">
                                                                                            Upload File <span class="symbol required"></span>
                                                                                        </label>
                                                                                        <span class="">
                                                                                            <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                                            <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="FileUpload1" ClientValidationFunction="getsubcat();" />--%>
                                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" />
                                                                                            <%--onchange="return getsubcat();--%>
                                                                                            <div class="clear">
                                                                                            </div>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" CssClass=""
                                                                                                ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf|.jpeg|.jpg|.PDF|.JPG|.JPEG)$" Style="color: red;"
                                                                                                Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                                            <div class="clear">
                                                                                            </div>

                                                                                        </span>

                                                                                        <div class="clear">
                                                                                        </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <%--<div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>--%>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div class="col-sm-8 col-sm-offset-5">
                                                        <asp:Button CssClass="btn btn-primary  redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                            Text="Add" ValidationGroup="uploadpdf" />
                                                        <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                            Text="Save" Visible="false" />
                                                        <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                            CausesValidation="false" Text="Reset" />
                                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                            CausesValidation="false" Text="Cancel" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                        <div class=" paddleftright10" id="PanSearch" runat="server">
                            <%--<div class="leftarea1">
                        <div class="showenteries">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Show</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" CssClass="myval1">
                                        </asp:DropDownList></td>
                                    <td>entries</td>
                                </tr>
                            </table>
                        </div>
                    </div>--%>
                            <div>
                                <div id="divrightbtn" runat="server" style="text-align: right;">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-default purple"
                                        OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="messesgarea">
                            <div class="alert alert-success" style="margin-bottom: 20px;" id="PanSuccess" runat="server" visible="false">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>
                        </div>
                        <div class="contactbottomarea finalgrid">
                            <div class="tablescrolldiv">
                                <div class="tableblack tableminpadd">
                                    <div class="table-responsive" id="PanGrid" runat="server">
                                        <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" AllowSorting="true" OnSorting="GridView1_Sorting"
                                            OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnPageIndexChanging="GridView1_PageIndexChanging" AutoGenerateColumns="false"
                                            CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Document Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("Id") %>' />
                                                        <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("EmpId") %>' />
                                                        <%#Eval("DocumentName")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Document Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                    <ItemTemplate>
                                                        <%#Eval("Number")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document Attached" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkPDFUploded" runat="server" CommandName="PDFUploded" CommandArgument='<%#Eval("EmpId") %>' CausesValidation="false" CssClass="btn btn-palegreen"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFUploded" Visible="false" Style="font-size: 13px!important; padding: 1px 6px;">
                                                            <i class="btn-label glyphicon glyphicon-ok" style="font-size:13px;"></i> Doc
                                                        </asp:HyperLink>
                                                        <asp:HyperLink ID="lnkPDFPending" runat="server" CommandName="PDFPending" CommandArgument='<%#Eval("EmpId") %>' CausesValidation="false" CssClass="btn btn-darkorange"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="PDFPending" Visible="false" Style="font-size: 13px!important; padding: 1px 6px;">
                                                            <i class="btn-label glyphicon glyphicon-remove" style="font-size:13px;"></i> Doc
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                            CommandName="Delete" CommandArgument='<%#Eval("Id")%>'>
                        <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </section>


            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
            <!--End Danger Modal Templates-->

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAdd" />
        </Triggers>
    </asp:UpdatePanel>
    <%--<script type="text/javascript">

    function getsubcat() {
            var file = document.querySelector('#<%=FileUpload1.ClientID %>').files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (_file) {
            var s = ~~(file.size / 1024) + 'KB',
            size = parseInt(file.size / 1024);
            if (file.type == "application/pdf")
            {
                        if (size > 1024) {
                            alert("File size should be less than 1 MB.");
                            $('#<%=FileUpload1.ClientID%>').val("");
                            $(".file-input-name").empty();
                            //return false;
                        }
            }
            if (file.type == "image/jpeg" || file.type == "image/jpg")
            {
                        if (size > 200) {
                            alert("File size should be less than 200 KB.");
                            $('#<%=FileUpload1.ClientID%>').val("");
                            $(".file-input-name").empty();
                             //return false;
                        }
            }
            };
        }
    </script>--%>
</asp:Content>

