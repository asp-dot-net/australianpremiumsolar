﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="CityMaster.aspx.cs" Inherits="admin_adminfiles_masters_CityMaster" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   <%-- <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>--%>
        
       
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                 function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                     $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                  
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    //$("[data-toggle=tooltip]").tooltip();
                    $(".myvalcustsubsource").select2({
                        //placeholder: "select",
                        allowclear: true,
						 minimumResultsForSearch: -1
                    });
                }
            </script>

           <script>



               function ComfirmDelete(event, ctl) {
                   event.preventDefault();
                   var defaultAction = $(ctl).prop("href");

                   swal({
                       title: "Are you sure you want to delete this Record?",
                       text: "",
                       type: "warning",
                       showCancelButton: true,
                       confirmButtonColor: "#DD6B55",
                       confirmButtonText: "Yes, delete it!",
                       cancelButtonText: "No, cancel!",
                       closeOnConfirm: true,
                       closeOnCancel: true
                   },

                     function (isConfirm) {
                         if (isConfirm) {
                             // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                             eval(defaultAction);

                             //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                             return true;
                         } else {
                             // swal("Cancelled", "Your imaginary file is safe :)", "error");
                             return false;
                         }
                     });
               }


            </script>
                <div class="page-body headertopbox">
                    <h5 class="row-title"><i class="pe-7s-global icon"></i>City</h5>
                    <div id="hbreadcrumb">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-info btngray"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </div>
               </div>
                

               

            <div class="finaladdupdate addEmployee formGrid whitebg">
            <div id="PanAddUpdate" runat="server" visible="false">
               <div class="panel-body animate-panel padtopzero"> 
               <div class="with-header with-footer addform centerFormTable">
                        <div class="header bordered-blue">  
                            <h4 class="formHeading"><asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label> City</h4>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <asp:Label ID="lblFirstName" runat="server" class="control-label">Taluka</asp:Label>
                                
                                    <asp:DropDownList ID="ddState" runat="server" AppendDataBoundItems="true"
                                        aria-controls="DataTables_Table_0" class="myvalcustsubsource">
                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage=""
                                        ControlToValidate="ddState" Display="Dynamic" ></asp:RequiredFieldValidator>
                                
                            </div>
                            <div class="form-group col-sm-6">
                                <asp:Label ID="lblLastName" runat="server" class="control-label">City Name</asp:Label>
                                
                                    <asp:TextBox ID="txtCustSourceSub" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                  
                                    <asp:RequiredFieldValidator ID="rfvcollegename" runat="server" ErrorMessage="" ControlToValidate="txtCustSourceSub"
                                        Display="Dynamic" ></asp:RequiredFieldValidator>
                                
                            </div>
                            <div class="form-group" id="seq" runat="server" visible="false">
                                <asp:Label ID="lblMobile" runat="server" class="col-sm-2 control-label"><strong>Seq</strong></asp:Label>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtSeq" runat="server" MaxLength="3" Text="0" class="form-control modaltextbox"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regvalSortOrder" runat="server" ErrorMessage="Number Only"
                                        ValidationExpression="^[0-9]*$" ControlToValidate="txtSeq"></asp:RegularExpressionValidator>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=" * Required"
                                        ControlToValidate="txtSeq" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group col-sm-12 checkBox checkBoxList">
                                    
                                <asp:Label ID="Label2" runat="server" class="control-label">Is Active?</asp:Label>
                                <asp:CheckBox ID="chkActive" runat="server" />
                                <label for="<%=chkActive.ClientID %>"></label>
                                    
                            </div>
                            <div class="col-sm-12">
                                    <div class="dividerLine"></div>
                            </div>
                            <div class="col-sm-12 textRight">
                                    <asp:Button CssClass="btn largeButton greenBtn btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                        Text="Add" />
                                    <asp:Button CssClass="btn largeButton greenBtn btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                        Text="Save" Visible="false" />
                                    <asp:Button CssClass="btn largeButton whiteBtn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                        CausesValidation="false" Text="Reset" />
                                    <asp:Button CssClass="btn largeButton blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                        CausesValidation="false" Text="Cancel" />
                            </div>
                        </div>
                </div>
                </div>
            </div>
            </div>
                  
                   <div class="page-body padtopzero">
            <asp:Panel runat="server" ID="PanGridSearch">
                <div class="">
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                    
                    <div class="fullWidthTable searchfinal searchbar mainGridTable main-card mb-3 card"> 
                        <div class="widget-body shadownone brdrgray"> 
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header">
                            <div class="dataTables_filter simpleDropdown searchFilters ">
                            <table border="0" cellspacing="0" width="100%" class="simpleDropdown" cellpadding="0">
                                <tr>
                                    <td class="textLeft dataTables_length showdata showEntryBox">
                                        <%--<asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                        aria-controls="DataTables_Table_0" class="myvalcustsubsource">
                                        <asp:ListItem Value="25">Display</asp:ListItem>
                                        </asp:DropDownList>--%>

                                         <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                         aria-controls="DataTables_Table_0" class="myvalcustsubsource">
                                       <asp:ListItem Value="25">Display</asp:ListItem></asp:DropDownList>

                                    </td>
                                    <td class="textRight">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                WatermarkText="City" />
                                        </div>
                                        <div class="input-group col-sm-1">
                                            <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcustsubsource">
                                                <asp:ListItem Value="">All Records</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="2">Not Active</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="input-group">
                                            <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>       
                            </div>                                        
                        </div>
                        </div>
                    
                        <div class="finalgrid">
                        <div class="table-responsive printArea">                                     
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive">
                                   <asp:GridView ID="GridView1" DataKeyNames="Id" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                    OnRowCreated="GridView1_RowCreated" OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                            <asp:TemplateField HeaderText="Taluka" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%# Eval("Taluka")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City" HeaderStyle-CssClass="textLeft" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%# Eval("CityName")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Seq" Visible="false" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate><%# Eval("Seq")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Status" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="150px" HeaderStyle-Width="150px">
                                                <ItemTemplate>
                                                     <span class="">
                                                       <%#Eval("Active").ToString()=="True"?"<span class='badge status active'>&nbsp;Active</span>":"<span class='badge status inactive'>&nbsp;Inactive</span>"%>
                                                       </span>

                                                    <%--<asp:CheckBox ID="chkStatus" runat="server" Checked='<%# Eval("Active")%>' /><i class="fa-check-square-o"></i>--%>
                                                    <%-- <label for="<%=chkStatus.ClientID %>" runat="server" id="lblchk">
                                                        <span></span>
                                                    </label>--%> 
                        
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions" ItemStyle-Width="105px" HeaderStyle-Width="105px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>

                                                    <div class="dropdown d-inline-block">
                                                    <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle noArrow"></button>
                                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList">

                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false"><i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>
                                                    
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server"   CausesValidation="false" 
                                                         CommandName="Delete" CommandArgument='<%#Eval("Id")%>' >
                                                         <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>
                                                    </div>
                        
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                    </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>                                
                        </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            </div>
     <!--Danger Modal Templates-->
    <asp:Button ID="btndelete" Style="display:none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel1"><i class="pe-7s-trash popupIcon"></i>Delete</h4>
                            </div>
                        </div>
                <%-- <label id="ghh" runat="server"></label>--%>
                    <div class="modal-body paddnone" runat="server" id="div4">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You want to Delete This Entry?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 textRight">

                                        <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" CommandName="deleteRow" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="lnkcancel"  runat="server"  data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
                                                                     <!--End Danger Modal Templates-->
 <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

