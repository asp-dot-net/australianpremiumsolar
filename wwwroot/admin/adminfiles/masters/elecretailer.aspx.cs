using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_master_tblElecRetailer : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "formValidate();");

        if (!IsPostBack)
        {
            //PanSearch.Visible = true;
            PanGrid.Visible = false;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindSearchDropdown();
            BindGrid(0);
        }
    }
    public void BindSearchDropdown()
    {
        //lstSearchStatus.DataSource = ClstblElecDistributor.tblstate_SelectActive();
        //lstSearchStatus.DataBind();

        ddllocation.DataSource = ClstblCustSource.tblLocation_Select();
        ddllocation.DataValueField = "CompanyLocationID";
        ddllocation.DataTextField = "CompanyLocation";
        ddllocation.DataBind();
    }
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblElecRetailer.tblElecRetailer_Select();
        if (dt.Rows.Count == 0)
        {
            //PanSearch.Visible = true;
        }
        // HidePanels();
        //if (!string.IsNullOrEmpty(hdncgharid.Value))
        //{
        //    dt = ClstblElecRetailer.tblElecRetailer_GetDataByAlpha(hdncgharid.Value);
        //}
        if (!string.IsNullOrEmpty(txtSearch.Text) || !string.IsNullOrEmpty(ddlActive.SelectedValue))
        {
            dt = ClstblElecRetailer.tblElecRetailer_GetDataBySearch(txtSearch.Text, ddlActive.SelectedValue);
        }
        return dt;
    }
    protected void lstSearchState_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        //if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        //{
        //    if (hdnID.Value == "2")
        //    {
        //        e.Item.Visible = false;
        //    }
        //}

    }
    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
           divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue !="All")
            {
            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            {
                //========label Hide
               divnopage.Visible = false;
            }
            else
            {
                divnopage.Visible = true;
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
               ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            }
			else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                     divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string Location = ddllocation.SelectedValue;
        string ElecRetailer = txtElecRetailer.Text.Trim();
        string Active = chkActive.Checked.ToString();
        string Seq = txtSeq.Text;
        //string NSW = chkNSW.Checked.ToString();
        //string SA = chkSA.Checked.ToString();
        //string QLD = chkQLD.Checked.ToString();
        //string VIC = chkVIC.Checked.ToString();
        //string WA = chkWA.Checked.ToString();
        //string ACT = chkACT.Checked.ToString();
        //string TAS = chkTAS.Checked.ToString();
        //string NT = chkNT.Checked.ToString();
        string ElectricityProviderId = txtelectricityprovider.Text;

        int exist = ClstblElecRetailer.tblElecRetailer_Exists(ElecRetailer);
        if (exist == 0)
        {
            int success = ClstblElecRetailer.tblElecRetailer_Insert(ElecRetailer, Active, Location,Seq, "False", "False", "False", "False", "False", "False", "False", "False", ElectricityProviderId);
            string selectedItem = "";
            //foreach (RepeaterItem item in lstSearchStatus.Items)
            //{
            //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            //    HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
            //    HiddenField hdnstate = (HiddenField)item.FindControl("hdnstate");

            //    if (chkselect.Checked == true)
            //    {
            //        var state = ClstblElecDistributor.tblElecRetailerState_Insert(success, hdnstate.Value.ToString(), hdnID.Value.ToString());
            //        //selectedItem += "," + hdnID.Value.ToString();
            //    }
            //}
            if (selectedItem != "")
            {
                selectedItem = selectedItem.Substring(1);
            }
            //--- do not chage this code start------
            if (Convert.ToString(success) != "")
            {
                SetAdd();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            //InitAdd();
            SetExist();
            //PanAlreadExists.Visible = true;
        }
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string ElecRetailer = txtElecRetailer.Text.Trim();
        string Active = chkActive.Checked.ToString();
        string Seq = txtSeq.Text;
        string Location = ddllocation.SelectedValue;
        //string NSW = chkNSW.Checked.ToString();
        //string SA = chkSA.Checked.ToString();
        //string QLD = chkQLD.Checked.ToString();
        //string VIC = chkVIC.Checked.ToString();
        //string WA = chkWA.Checked.ToString();
        //string ACT = chkACT.Checked.ToString();
        //string TAS = chkTAS.Checked.ToString();
        //string NT = chkNT.Checked.ToString();
        string ElectricityPrividerId = txtelectricityprovider.Text;
        int exist = ClstblElecRetailer.tblElecRetailer_ExistsById(ElecRetailer, id1);
        if (exist == 0)
        {
            var stateDelete = ClstblElecRetailer.tblElecRetailerState_Delete(id1);
            bool success = ClstblElecRetailer.tblElecRetailer_Update(id1, ElecRetailer, Active,Location, Seq, "False", "False", "False", "False", "False", "False", "False", "False", ElectricityPrividerId);
            string selectedItem = "";
            //foreach (RepeaterItem item in lstSearchStatus.Items)
            //{
            //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            //    HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
            //    HiddenField hdnstate = (HiddenField)item.FindControl("hdnstate");

            //    if (chkselect.Checked == true)
            //    {
            //        var state = ClstblElecDistributor.tblElecRetailerState_Insert(Convert.ToInt32(id1), hdnstate.Value.ToString(), hdnID.Value.ToString());
            //        //selectedItem += "," + hdnID.Value.ToString();
            //    }
            //}
            if (selectedItem != "")
            {
                selectedItem = selectedItem.Substring(1);
            }
            //--- do not chage this code Start------
            if (success)
            {
                //InitAdd();
                SetUpdate();
            }
            else
            {
                SetError();
            }
            Reset();
           
            //--- do not chage this code end------
        }
        else
        {
            InitUpdate();
            SetExist();
            //PanAlreadExists.Visible = true;
        }
        BindGrid(0);
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblElecRetailer st = ClstblElecRetailer.tblElecRetailer_SelectByElecRetailerID(id);
        DataTable dt = ClstblElecRetailer.tblElecRetailerstate_SelectByElecRetailerID(id);
        if (dt.Rows.Count > 0)
        {
            //foreach (RepeaterItem item in lstSearchStatus.Items)
            //{
            //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            //    HiddenField hdnstate = (HiddenField)item.FindControl("hdnstate");
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        string state = dt.Rows[i]["State"].ToString();
            //        if (hdnstate.Value.ToString() == state)
            //        {
            //            chkselect.Checked = true;
            //        }
            //    }
            //}
        }
        txtElecRetailer.Text = st.ElecRetailer;
        chkActive.Checked = Convert.ToBoolean(st.Active);
        txtSeq.Text = st.Seq;
        //chkNSW.Checked = Convert.ToBoolean(st.NSW);
        //chkSA.Checked = Convert.ToBoolean(st.SA);
        //chkQLD.Checked = Convert.ToBoolean(st.QLD);
        //chkVIC.Checked = Convert.ToBoolean(st.VIC);
        //chkWA.Checked = Convert.ToBoolean(st.WA);
        //chkACT.Checked = Convert.ToBoolean(st.ACT);
        //chkTAS.Checked = Convert.ToBoolean(st.TAS);
        //chkNT.Checked = Convert.ToBoolean(st.NT);
        txtelectricityprovider.Text = st.ElectricityProviderId;
        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
    }

    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
       else if(Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else{
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        //PanSearch.Visible = true;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
       // ModalPopupExtender2.Show();
        Reset();
        //PanSearch.Visible = false;
        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
       // ModalPopupExtender2.Show();
        //PanSearch.Visible = false;
        InitAdd();
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        //lnkBack.Visible = true;
        //lnkAdd.Visible = false;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;
        PanGrid.Visible = false;
        PanGridSearch.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;

        // ModalPopupExtender2.Show();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
        PanGridSearch.Visible = false;
    }
    public void InitUpdate()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        HidePanels();
        //PanAddUpdate.Visible = true;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;
        //lnkAdd.Visible = false;
        //lnkBack.Visible = true;
        lblAddUpdate.Text = "Update ";
        PanGridSearch.Visible = false;
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
       
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtElecRetailer.Text = string.Empty;
        //chkNSW.Checked = false;
        //chkSA.Checked = false;
        //chkQLD.Checked = false;
        //chkVIC.Checked = false;
        //chkWA.Checked = false;
        //chkACT.Checked = false;
        //chkTAS.Checked = false;
        //chkNT.Checked = false;
        //txtSeq.Text = String.Empty;
        chkActive.Checked = false;
        txtelectricityprovider.Text = string.Empty;
    }
    protected void A_Click(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        string alpha = button.CommandArgument;
        //hdncgharid.Value = alpha;
        BindGrid(0);
    }
    protected void all_Click(object sender, EventArgs e)
    {
        //hdncgharid.Value = string.Empty;
        BindGrid(0);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //hdncgharid.Value = string.Empty;
        BindGrid(0);
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string ID = hdndelete.Value.ToString();
        bool sucess1 = ClstblElecRetailer.tblElecRetailer_Delete(ID);
        if (sucess1)
        {
            SetDelete();

        }
        else
        {
            SetError();
        }
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnDistID = (HiddenField)e.Row.FindControl("hdnDistID");
            DataTable dt = ClstblElecRetailer.tblElecRetailerstate_SelectByElecRetailerID(hdnDistID.Value);
           // Repeater staterepeater = (Repeater)e.Row.FindControl("staterepeater");
            if (dt.Rows.Count > 0)
            {
                //staterepeater.DataSource = dt;
                //staterepeater.DataBind();
            }
        }
    }
}