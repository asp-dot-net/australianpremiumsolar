<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="employee.aspx.cs" Inherits="admin_adminfiles_master_employee" EnableEventValidation="false" ValidateRequest="false" MaintainScrollPositionOnPostback="true" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/controls/personaldetails.ascx" TagName="personaldetails" TagPrefix="uc1" %>
<%@ Register Src="../../../includes/controls/role.ascx" TagName="role" TagPrefix="uc2" %>
<%@ Register Src="../../../includes/controls/project.ascx" TagName="project" TagPrefix="uc3" %>
<%@ Register Src="../../../includes/controls/empdocuments.ascx" TagName="docc" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <style>
                .errorClass {
                    border: 1px solid green !important;
                }
            </style>
            <script type="text/javascript">
                $(document).ready(function () {
                    var ddlTestDropDownListXML = $('#ddlName');
                    var tableName = "someTableName";

                    $.ajax({
                        type: "POST",
                        url: "employee.aspx/geEmployee",
                        data: '{tableName: "' + tableName + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            // Now find the Table from response and loop through each item (row).
                            var s = '<option value="-1">Please Select a Department</option>';
                            $(response.d).find(tableName).each(function () {
                                // Get the OptionValue and OptionText Column values.
                                var OptionValue = $(this).find('OptionValue').text();
                                var OptionText = $(this).find('OptionText').text();

                                // Create an Option for DropDownList.
                                var option = $("<option>" + OptionText + "</option>");
                                option.attr("value", OptionValue);

                                ddlTestDropDownListXML.append(option);
                            });
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                });

                function fnDelete(empId) {
                    alert(empId);
                    $.ajax({
                        type: "POST",
                        url: "employee.aspx/DeleteEmployee",
                        data: "{'empId':'" + empId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                alert("1");
                            }
                            else {
                                alert("0");
                            }
                        }
                    });

                }
                function SomeValueChanged(obj) {
                    alert(obj.value);
                }
                function myjs() {
                    var x = "11:11:11";
                    var reason = document.getElementById("<%=txtStartTime.ClientID %>").value;
                    alert(reason);
                    if (reason == "") {
                        document.getElementById("<%=txtStartTime.ClientID %>").value = " ";
                        alert("ewew");
                    }
                }
                //$(function () {
                //    $('form').on("click", '.POPupLoader', function () {
                //        ShowProgress();
                //    });
                //});
                function ShowProgress() {
                    alert();
                    setTimeout(function () {
                        if (Page_IsValid) {
                            ActiveLoader();
                        }
                        else {
                        }
                    }, 200);
                }
            </script>

            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    //$("[data-toggle=tooltip]").tooltip();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $(".myval").select2({
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });
                    $(".myvalemployee").select2({
                        allowclear: true,
                        //minimumResultsForSearch: -1
                    });
                    $('.redreq').click(function () {
                        console.log("formValidate CAlled");
                        formValidate();
                    });
                    $(".myvalproject").select2({
                        //placeholder: "select",
                        allowclear: true
                    });



                    $('.custom-file-input').on('change', function (e) {
                        var fileName = e.target.files[0].name;
                        //  var fileName = document.getElementsByClassName("fileupload").files[0].name;
                        //alert(fileName);
                        $(this).next('.form-control-file').addClass("selected").html(fileName);
                    });


                    function formValidate() {
                        if (typeof (Page_Validators) != "undefined") {
                            for (var i = 0; i < Page_Validators.length; i++) {
                                if (!Page_Validators[i].isvalid) {
                                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                                }
                                else {
                                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                                }
                            }
                        }
                    }
                }


                function checkEmail(event) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!re.test(event.value)) {
                        $("#spnEmail").text("Please enter a valid email address").css('color', 'red');
                        $("#txtEmpEmail").addClass('errorClass');
                        return false;
                    }
                    else {
                        $("#spnEmail").text("");
                    }
                    return true;
                }
                function checkMobile(event) {
                    var mobileval = document.getElementById("<%= txtEmpMobile.ClientID %>").value;
                    var mobilerslt = false;
                    if (mobileval.length != 10) {
                        $("#spnText").text("Please enter a 10 digit mobile no").css('color', 'red');
                        return false;

                    }
                    else {
                        $("#spnText").text("");
                    }
                    return true;

                }
                function checkCont(event) {
                    var mobileval = document.getElementById("<%= txtEmpPhone.ClientID %>").value;
                    var mobilerslt = false;
                    if (mobileval.length != 10) {
                        $("#spncont").text("Please enter a 10 digit Contact no").css('color', 'red');
                        return false;

                    }
                    else {
                        $("#spncont").text("");
                    }
                    return true;

                }
                function ActiveLoader() {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                    console.log("Function Called Before");
                }
                function InActiveLoader() {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    console.log("Function Called After");
                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="pe-7s-users icon"></i>Manage Employee</h5>
                <div id="hbreadcrumb">
                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                </div>
            </div>


            <div class="addEmployee formGrid">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="with-header with-footer addform">
                            <div class="header bordered-blue dispnone">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Employee
                            </div>
                            <div class="bodymianbg">
                                <div class="row">
                                    <div class="col-md-12">
                                        <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                            <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Personal" TabIndex="0">
                                                <ContentTemplate>
                                                    <div class="table-responsive">
                                                        <div class="centerFormTable">

                                                            <div class="row">

                                                                <div class="form-group col-sm-6 selectDropdown">
                                                                    <asp:Label ID="Label14" runat="server" class="control-label">Emp Type</asp:Label>
                                                                    <asp:DropDownList ID="ddlEmpType" runat="server" AppendDataBoundItems="true"
                                                                        aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                                                        <asp:ListItem Value="1">Employee</asp:ListItem>
                                                                        <asp:ListItem Value="2">Dealer</asp:ListItem>
                                                                        <asp:ListItem Value="3">Contractor </asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage=""
                                                                        ControlToValidate="ddlEmpType" Display="Dynamic" InitialValue="0" SetFocusOnError="true" ValidationGroup="RequiredFields"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="form-group col-sm-6">
                                                                    <asp:Label ID="Label2" runat="server" class="control-label">Source By</asp:Label>
                                                                    <asp:TextBox ID="txtsourceby" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label1" runat="server" class="control-label">First Name</asp:Label>
                                                                    <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="txtEmpFirst"
                                                                        Display="Dynamic" SetFocusOnError="True" ValidationGroup="RequiredFields"></asp:RequiredFieldValidator>

                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label34" runat="server" class="control-label">Middle Name</asp:Label>
                                                                    <asp:TextBox ID="txtEmpMiddle" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ControlToValidate="txtEmpMiddle"
                                                                        Display="Dynamic" ValidationGroup="RequiredFields" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label3" runat="server" class="control-label">Last Name</asp:Label>
                                                                    <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" ControlToValidate="txtEmpLast"
                                                                        Display="Dynamic" ValidationGroup="RequiredFields" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label5" runat="server" class="control-label">Email</asp:Label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>

                                                                        <asp:TextBox ID="txtEmpEmail" runat="server" MaxLength="200" class="form-control modaltextbox" onblur="checkEmail(this)"></asp:TextBox>
                                                                    </div>
                                                                    <span id="spnEmail"></span>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtEmpEmail"
                                                                        Display="Dynamic" ValidationGroup="RequiredFields" ErrorMessage=""
                                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="" ControlToValidate="txtEmpEmail"
                                                                        Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label6" runat="server" class=" control-label">Mobile No</asp:Label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>

                                                                        <asp:TextBox ID="txtEmpMobile" runat="server" MaxLength="200" class="form-control modaltextbox" onblur="checkMobile(this)"></asp:TextBox>
                                                                        <span id="spnText"></span>

                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmpMobile"
                                                                            Display="Dynamic" ValidationGroup="RequiredFields" ErrorMessage=""
                                                                            ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="" ControlToValidate="txtEmpMobile"
                                                                        Display="Dynamic" ValidationGroup="RequiredFields" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label7" runat="server" class="control-label">Contact No</asp:Label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>

                                                                        <asp:TextBox ID="txtEmpPhone" runat="server" MaxLength="200" class="form-control modaltextbox" onblur="checkCont(this)"></asp:TextBox>
                                                                        <span id="spncont"></span>
                                                                    </div>
                                                                </div>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpPhone"
                                                                    Display="Dynamic" ValidationGroup="RequiredFields" ErrorMessage=""
                                                                    ValidationExpression="^([0-9]{10})$"></asp:RegularExpressionValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="" ControlToValidate="txtEmpPhone"
                                                                    Display="Dynamic" ValidationGroup="RequiredFields" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" ControlToValidate="txtEmpPhone"
                                                            Display="Dynamic" ></asp:RequiredFieldValidator>--%>

                                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                                                        ControlToValidate="txtEmpPhone" Display="Dynamic" ErrorMessage="Number Only"
                                                                        ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>--%>

                                                                <%--<div class="form-group col-sm-4 dispnone">
                                                                    <asp:Label ID="Label23" runat="server" class="control-label">Date Hired</asp:Label>
                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtHireDate" runat="server" class="form-control pagel">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>--%>



                                                                <div class="form-group col-sm-6" id="divStreetno" runat="server">
                                                                    <asp:Label ID="Label4" runat="server" CssClass="control-label">Address 1</asp:Label>
                                                                    <div>
                                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                                        <%--AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"--%>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-sm-6 spicaldivin streatfield" id="divstname" runat="server">
                                                                    <asp:Label ID="Label35" CssClass="control-label" runat="server">Address 2</asp:Label>

                                                                    <div class="autocompletedropdown">
                                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control"></asp:TextBox>


                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                                        <%--<asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                            ErrorMessage="Enter Valid Street" Display="Dynamic"
                                                                            ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>
                                                                        --%>
                                                                        <div id="Divvalidstreetname" style="display: none">
                                                                            <i class="fa fa-check"></i>Address is valid.
                                                                        </div>
                                                                        <div id="DivInvalidstreetname" style="display: none">
                                                                            <i class="fa fa-close"></i>Address is invalid.
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>

                                                                <%--<asp:UpdatePanel ID="updateaddress" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>--%>

                                                                <div class="form-group col-sm-6" id="Div9" runat="server">
                                                                    <asp:Label ID="Label37" runat="server" CssClass="control-label">State</asp:Label>
                                                                    <div>
                                                                        <%--<asp:TextBox ID="txtStreetState" runat="server" MaxLength="50" class="form-control modaltextbox"></asp:TextBox>--%>
                                                                        <asp:DropDownList ID="ddlStreetState" runat="server" AppendDataBoundItems="true" CssClass="myvalproject" OnSelectedIndexChanged="ddlStreetState_SelectedIndexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="0">Select State</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-sm-6" id="Div3" runat="server">
                                                                    <asp:Label ID="Label23" runat="server" CssClass="control-label">District</asp:Label>
                                                                    <div class="autocompletedropdown">
                                                                        <%-- <asp:TextBox ID="txtDistrict" runat="server" MaxLength="50"
                                                                            class="form-control modaltextbox"></asp:TextBox>--%>
                                                                        <asp:DropDownList ID="ddlDistrict" runat="server" AppendDataBoundItems="true" CssClass="myvalproject" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="0">Select District</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group col-sm-4" id="Div1" runat="server">
                                                                    <asp:Label ID="Label36" runat="server" CssClass="control-label">Taluka</asp:Label>
                                                                    <div>
                                                                        <%--<asp:TextBox ID="txttaluka" runat="server" MaxLength="50" class="form-control modaltextbox"></asp:TextBox>--%>
                                                                        <asp:DropDownList ID="ddltaluka" runat="server" AppendDataBoundItems="true" CssClass="myvalproject" OnSelectedIndexChanged="ddltaluka_SelectedIndexChanged" AutoPostBack="true">
                                                                            <asp:ListItem Value="0">Select Taluka</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <%--<div class="marginbtm15 col-md-4" visible="false" id="divsttype" runat="server">
                                                                <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                        <div id="Div8" class="drpValidate" runat="server">
                                                            <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                                <asp:ListItem Value="">Street Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                                ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                                        </div>
                                                    </div>--%>
                                                                <div class="form-group col-sm-4" id="seq" runat="server">
                                                                    <asp:Label ID="lblMobile" runat="server" CssClass="control-label">City</asp:Label>
                                                                    <div class="autocompletedropdown">
                                                                        <%-- <asp:TextBox ID="ddlStreetCity" runat="server" MaxLength="50"
                                                                            class="form-control modaltextbox"></asp:TextBox>--%>

                                                                        <asp:DropDownList ID="ddlCity" runat="server" AppendDataBoundItems="true" CssClass="myvalproject">
                                                                            <asp:ListItem Value="0">Select City</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <%--<cc1:AutoCompleteExtender ID="AutoCompletestreet" CompletionListCssClass="autocompletedrop" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                                            ServiceMethod="GetCitiesList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                    </div>
                                                                </div>
                                                                <%-- </ContentTemplate>
                                                            <Triggers>
                                                               <asp:AsyncPostBackTrigger ControlID="ddlStreetState" />
                                                                <asp:AsyncPostBackTrigger ControlID="ddlDistrict" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>


                                                                <div class="form-group col-sm-4" id="Div10" runat="server">
                                                                    <asp:Label ID="Label38" runat="server" CssClass="control-label">Pin Code</asp:Label>
                                                                    <div>
                                                                        <asp:TextBox ID="txtpincode" runat="server" name="address" class="form-control modaltextbox"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="dividerLine"></div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <h4 class="formHeading">Tax Details</h4>
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="lblaadharno" runat="server" class="control-label">Aadhar Card No</asp:Label>
                                                                    <asp:TextBox ID="txtaadharno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label8" runat="server" class="control-label">Pan No</asp:Label>
                                                                    <asp:TextBox ID="txtpanno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <asp:Label ID="Label9" runat="server" class="control-label">Gst No</asp:Label>
                                                                    <asp:TextBox ID="txtgstno" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                </div>

                                                                <div class="form-group col-sm-12">
                                                                    <div class="textRight">
                                                                        <asp:Button CssClass="btn-shadow dropdown-toggle btn largeButton blueBtn redreq nextbtnicon" ID="Button1" runat="server"
                                                                            ValidationGroup="RequiredFields" CausesValidation="true" Text="Next Step" OnClick="Button1_Click" Visible="true" />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--<uc1:personaldetails ID="personaldetails1" runat="server" />--%>
                                                    </div>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Role" ActiveTabIndex="1">
                                                <ContentTemplate>
                                                    <%--<uc2:role ID="role1" runat="server" />--%>
                                                    <div class="centerFormTable">

                                                        <div class="row">
                                                            <div class="form-group col-sm-6 selectDropdown ">
                                                                <asp:Label ID="Label15" runat="server" class="control-label">User Status</asp:Label>

                                                                <asp:DropDownList ID="ddlEmployeeStatusID" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                                                    ControlToValidate="ddlEmployeeStatusID" ValidationGroup="RoleReqFields" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="form-group col-sm-6 selectDropdown">
                                                                <asp:Label ID="Label19" runat="server" class="control-label">Location</asp:Label>
                                                                <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                                                    ControlToValidate="ddlLocation" ValidationGroup="RoleReqFields" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>

                                                            <div class="form-group col-sm-4 multiSelect">
                                                                <asp:Label ID="Label10" runat="server" class="control-label">User Role</asp:Label>
                                                                <asp:ListBox ID="lstrole" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" ControlToValidate="lstrole"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="form-group col-sm-4 multiSelect">
                                                                <asp:Label ID="Label11" runat="server" class="control-label">Sales Team</asp:Label>
                                                                <asp:ListBox ID="ddlSalesTeamID" runat="server" SelectionMode="Multiple" CssClass="myvalemployee"></asp:ListBox>
                                                            </div>


                                                            <div class="form-group col-sm-4">
                                                                <asp:Label ID="Label39" runat="server" class="control-label">Date Hired</asp:Label>
                                                                <div class="input-group date datetimepicker1">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtHireDate" runat="server" class="form-control pagel">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-sm-4">

                                                                <asp:Label ID="Label16" runat="server" class="control-label">UserName</asp:Label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                                    <asp:TextBox ID="txtuname" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtuname"
                                                                    Display="Dynamic" ValidationGroup="RoleReqFields" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            </div>

                                                            <div class="form-group col-sm-4" id="password" runat="server">
                                                                <asp:Label ID="Label17" runat="server" class="control-label">Password</asp:Label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                                                    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>

                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Password has to be 5 characters !"
                                                                    ControlToValidate="txtpassword" ValidationExpression="^.{5,}$" Display="Dynamic" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" ControlToValidate="txtpassword"
                                                                    Display="Dynamic" ValidationGroup="RoleReqFields" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            </div>

                                                            <div class="form-group col-sm-4" id="confpassword" runat="server">
                                                                <asp:Label ID="Label18" runat="server" class="control-label">Confirm Password</asp:Label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                                                    <asp:TextBox ID="txtcpassword" runat="server" TextMode="Password" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                                                </div>

                                                                <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword"
                                                                    ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                                                    Display="Dynamic"></asp:CompareValidator>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" ControlToValidate="txtcpassword"
                                                                    Display="Dynamic" ValidationGroup="RoleReqFields" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            </div>



                                                            <div class="form-group col-sm-4">
                                                                <asp:Label ID="Label20" runat="server" class="control-label">Start&nbsp;Time</asp:Label>
                                                                <%--  <input type="text" runat="server" id="txtStartTime" data-mask="99:99:99" class="form-control" placeholder="00:00:00">--%>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div>
                                                                    <asp:TextBox ID="txtStartTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                                                </div>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtStartTime" Mask="99:99:99"
                                                                    MessageValidatorTip="true" MaskType="Time">
                                                                </cc1:MaskedEditExtender>
                                                                <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtStartTime" ControlExtender="MaskedEditExtenderST"
                                                                    Style="color: red;" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                                                </cc1:MaskedEditValidator>
                                                            </div>

                                                            <div class="form-group col-sm-4">
                                                                <asp:Label ID="Label21" runat="server" class="control-label">End&nbsp;Time</asp:Label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div>
                                                                    <asp:TextBox ID="txtEndTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                                                </div>

                                                                <cc1:MaskedEditExtender ID="MaskedEditExtenderET" runat="server" TargetControlID="txtEndTime" Mask="99:99:99" MessageValidatorTip="true" MaskType="Time">
                                                                </cc1:MaskedEditExtender>
                                                                <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtEndTime" ControlExtender="MaskedEditExtenderET"
                                                                    Style="color: red;" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                                                </cc1:MaskedEditValidator>
                                                            </div>

                                                            <div class="form-group col-sm-4">
                                                                <asp:Label ID="Label22" runat="server" class="control-label">Break&nbsp;Time</asp:Label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-clock-o"></i></span></div>
                                                                    <asp:TextBox ID="txtBreakTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                                                </div>
                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtBreakTime" Mask="99:99:99"
                                                                    MessageValidatorTip="true" MaskType="Time">
                                                                </cc1:MaskedEditExtender>
                                                                <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlToValidate="txtBreakTime" ControlExtender="MaskedEditExtenderST"
                                                                    Style="color: red;" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                                                </cc1:MaskedEditValidator>
                                                            </div>

                                                            <div class="col-sm-12 checkBoxSection">
                                                                <div class="row">
                                                                    <div class="form-group col-sm-3">
                                                                        <label class="control-label">D2D</label>
                                                                    </div>
                                                                    <div class="form-group col-sm-2 checkBox">
                                                                        <asp:Label ID="Label12" runat="server" class="control-label"> OutDoor</asp:Label>
                                                                        <asp:CheckBox ID="chkLTeamOutDoor" runat="server" />
                                                                        <label for="<%=chkLTeamOutDoor.ClientID %>" style="width: 70px;">
                                                                        </label>

                                                                    </div>
                                                                    <div class="form-group col-sm-2 checkBox">
                                                                        <asp:Label ID="Label13" runat="server" class="control-label"> Closer</asp:Label>
                                                                        <asp:CheckBox ID="chkLTeamCloser" runat="server"></asp:CheckBox>
                                                                        <label for="<%=chkLTeamCloser.ClientID %>" style="width: 70px;">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-sm-3">
                                                                        <label class="control-label">User Status</label>
                                                                    </div>
                                                                    <div class="form-group col-sm-8 checkBox checkBoxList">

                                                                        <asp:Label ID="Label25" runat="server" class="control-label">Active&nbsp;Employee</asp:Label>
                                                                        <asp:CheckBox ID="chkActiveEmp" runat="server" />
                                                                        <label for="<%=chkActiveEmp.ClientID %>">
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-sm-3">
                                                                        <label class="control-label">List Dropdown</label>
                                                                    </div>
                                                                    <div class="form-group col-sm-8 checkBox checkBoxList">
                                                                        <asp:Label ID="Label24" runat="server" class="control-label">Include&nbsp;in&nbsp;Lists</asp:Label>
                                                                        <asp:CheckBox ID="chkInclude" runat="server" />
                                                                        <label for="<%=chkInclude.ClientID %>">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-sm-3">
                                                                        <label class="control-label">Download Option</label>
                                                                    </div>
                                                                    <div class="form-group col-sm-8 checkBox">
                                                                        <asp:Label ID="Label26" runat="server" class="control-label">Show Excel</asp:Label>
                                                                        <asp:CheckBox ID="chkshowexcel" runat="server" />
                                                                        <label for="<%=chkshowexcel.ClientID %>">
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-sm-12">
                                                                <asp:Label ID="Label27" runat="server" class="control-label">Info</asp:Label>
                                                                <asp:TextBox ID="txtEmpInfo" runat="server" TextMode="MultiLine" Rows="4" class="form-control modaltextbox"></asp:TextBox>
                                                            </div>


                                                            <div class="col-sm-12 dispnone">
                                                                <div class="dividerLine"></div>
                                                            </div>

                                                            <div class="col-sm-12 dispnone">
                                                                <h4 class="formHeading">User's Account Settings</h4>
                                                            </div>

                                                            <div class="form-group col-sm-3 dispnone">
                                                                <label class="control-label">Login Verification</label>
                                                            </div>

                                                            <div class="form-group col-sm-9 dispnone">
                                                                <a href="#" class="blueButton">Setup login verification</a>
                                                                <span class="d-block form-text text-muted">After you log in, you will be asked for additional information to confirm your identity and protect your account from being compromised. <a href="#">Learn more</a>.
                                                                </span>
                                                            </div>

                                                            <div class="col-sm-12 dispnone">
                                                                <div class="row">
                                                                    <label class="col-sm-3 control-label">Password reset verification</label>
                                                                    <div class="col-sm-9 form-group">

                                                                        <div class="checkBoxRelative">
                                                                            <input type="checkbox" id="selectbox" /><label for="selectbox">Require personal information to reset your password.</label>
                                                                        </div>
                                                                        <span class="form-text text-muted">For extra security, this requires you to confirm your email or phone number when you reset your password.
                                                                            <a href="#" class="">Learn more</a>.
                                                                        </span>
                                                                        <a href="#" class="redButton mgmtop15">Deactivate your account ?</a>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="form-group" id="divtaxfile" runat="server" visible="false">
                                                                <asp:Label ID="Label28" runat="server" class="control-label">TaxFileNumber</asp:Label>
                                                                <asp:TextBox ID="txtTaxFileNumber" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group" id="divempabn" runat="server" visible="false">
                                                                <asp:Label ID="Label29" runat="server" class="control-label">EmpABN</asp:Label>
                                                                <asp:TextBox ID="txtEmpABN" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group" id="divempaccontant" runat="server" visible="false">
                                                                <asp:Label ID="Label30" runat="server" class="control-label">EmpAccountName</asp:Label>
                                                                <asp:TextBox ID="txtEmpAccountName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            </div>

                                                            <div class="form-group" id="divroster" runat="server" visible="false">
                                                                <asp:Label ID="Label31" runat="server" class="control-label">On&nbsp;Roster&nbsp;List</asp:Label>
                                                                <asp:CheckBox ID="chkOnRoster" runat="server" />
                                                                <label for="<%=chkOnRoster.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </div>

                                                            <div class="form-group" id="divpayown" runat="server" visible="false">
                                                                <asp:Label ID="Label32" runat="server" class="control-label">Company&nbsp;Pays&nbsp;Super</asp:Label>
                                                                <asp:CheckBox ID="chkPaysOwnSuper" runat="server" />
                                                                <label for="<%=chkPaysOwnSuper.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </div>

                                                            <div class="form-group" id="divgstyown" runat="server" visible="false">
                                                                <asp:Label ID="Label33" runat="server" class="control-label">GST&nbsp;Payment</asp:Label>
                                                                <asp:CheckBox ID="chkGSTPayment" runat="server"></asp:CheckBox>
                                                                <label for="<%=chkGSTPayment.ClientID %>">
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="dividerLine"></div>
                                                            </div>
                                                            <div class="col-sm-12 textRight">

                                                                <asp:Button CssClass="btn largeButton greenBtn btnaddicon redreq" ID="btnAdd" ValidationGroup="RoleReqFields" CausesValidation="true" runat="server" OnClick="btnAddEmployee_Click"
                                                                    Text="Add" />
                                                                <asp:Button CssClass="largeButton greenBtn btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                                    Text="Save" Visible="false" />
                                                                <asp:Button CssClass="largeButton whiteBtn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                                    CausesValidation="false" Text="Reset" />
                                                                <asp:Button CssClass="largeButton blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                                    CausesValidation="false" Text="Cancel" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabDocuments" runat="server" HeaderText="Documents" Visible="false" TabIndex="1">
                                                <ContentTemplate>
                                                    <uc6:docc ID="doc1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>

                                        </cc1:TabContainer>
                                    </div>
                                </div>
                            </div>
                            <%--  <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                First</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" ControlToValidate="txtEmpFirst"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label">
                                                Last</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" ControlToValidate="txtEmpLast"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label">
                                                Title</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpTitle" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                Initials</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpInitials" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                                                           </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label">
                                                Email</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpEmail" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" class="col-sm-2 control-label">
                                                Phone</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                      
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                            ControlToValidate="txtEmpPhone" Display="Dynamic" ErrorMessage="Number Only"
                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" class="col-sm-2 control-label">
                                                PhoneExtNo</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpExtNo" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ControlToValidate="txtEmpExtNo" Display="Dynamic" ErrorMessage="Number Only"
                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" class="col-sm-2 control-label">
                                                Nic&nbsp;Name&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpNicName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="" ControlToValidate="txtEmpNicName"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                Role</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="lstrole" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" ControlToValidate="lstrole"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" class="col-sm-2 control-label">
                                                SalesTeam</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:ListBox ID="ddlSalesTeamID" runat="server" SelectionMode="Multiple" Width="200px" CssClass="myvalemployee"></asp:ListBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;OutDoor</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkLTeamOutDoor.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamOutDoor" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                Team&nbsp;Closer</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkLTeamCloser.ClientID %>" style="width: 70px;">
                                            <asp:CheckBox ID="chkLTeamCloser" runat="server"></asp:CheckBox>
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label14" runat="server" class="col-sm-2 control-label">
                                                Emp&nbsp;Type&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmpType" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">AchieversEnergy</asp:ListItem>
                                            <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmpType" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label15" runat="server" class="col-sm-2 control-label">
                                                
Emp Status</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlEmployeeStatusID" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlEmployeeStatusID" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                UserName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtuname" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtuname"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="password" runat="server" visible="false">
                                    <asp:Label ID="Label17" runat="server" class="col-sm-2 control-label">
                                                Password</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Password has to be 5 characters !"
                                            ControlToValidate="txtpassword" ValidationExpression="^.{5,}$" Display="Dynamic" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" ControlToValidate="txtpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group" id="confpassword" runat="server" visible="false">
                                    <asp:Label ID="Label18" runat="server" class="col-sm-2 control-label">
                                                Confirm&nbsp;Password&nbsp;</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtcpassword" runat="server" TextMode="Password" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                        <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword"
                                            ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                            Display="Dynamic"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" ControlToValidate="txtcpassword"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <asp:Label ID="Label19" runat="server" class="col-sm-2 control-label">
                                                Location</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage=""
                                            ControlToValidate="ddlLocation" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label20" runat="server" class="col-sm-2 control-label">
                                                Start&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                <asp:TextBox ID="txtStartTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderST" runat="server" TargetControlID="txtStartTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtStartTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label21" runat="server" class="col-sm-2 control-label">
                                                End&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEndTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>

                                        <cc1:MaskedEditExtender ID="MaskedEditExtenderET" runat="server" TargetControlID="txtEndTime" Mask="99:99:99" MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtEndTime" ControlExtender="MaskedEditExtenderET"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label22" runat="server" class="col-sm-2 control-label">
                                                Break&nbsp;Time</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtBreakTime" runat="server" MaxLength="200" class="form-control modaltextbox timepicker1" Text="00:00:00"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtBreakTime" Mask="99:99:99"
                                            MessageValidatorTip="true" MaskType="Time">
                                        </cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlToValidate="txtBreakTime" ControlExtender="MaskedEditExtenderST"
                                            IsValidEmpty="false" style="color:red;" EmptyValueMessage="Enter time" InvalidValueMessage="Invalid Time" CssClass="emperror" Display="Dynamic">  
                                        </cc1:MaskedEditValidator>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                Date&nbsp;Hired</asp:Label>
                                    <div class="col-sm-6">
                                        <div class="input-group date datetimepicker1 col-sm-6" style="width: 255px">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtHireDate" runat="server" class="form-controlpagel">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label24" runat="server" class="col-sm-2 control-label">
                                                Include&nbsp;in&nbsp;Lists</asp:Label>
                                    <div class="col-sm-6">
                                        <label for="<%=chkInclude.ClientID %>">
                                            <asp:CheckBox ID="chkInclude" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label25" runat="server" class="col-sm-2 control-label">
                                                Active&nbsp;Employee</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkActiveEmp.ClientID %>">
                                            <asp:CheckBox ID="chkActiveEmp" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" class="col-sm-2 control-label">
                                                Show Excel</asp:Label>
                                    <div class="col-sm-6">

                                        <label for="<%=chkshowexcel.ClientID %>">
                                            <asp:CheckBox ID="chkshowexcel" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" class="col-sm-2 control-label">
                                                Info</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpInfo" runat="server" TextMode="MultiLine" Rows="2" Width="500px" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divtaxfile" runat="server" visible="false">
                                    <asp:Label ID="Label28" runat="server" class="col-sm-2 control-label">
                                                TaxFileNumber</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtTaxFileNumber" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="divempabn" runat="server" visible="false">
                                    <asp:Label ID="Label29" runat="server" class="col-sm-2 control-label">
                                                EmpABN</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpABN" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divempaccontant" runat="server" visible="false">
                                    <asp:Label ID="Label30" runat="server" class="col-sm-2 control-label">
                                                EmpAccountName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtEmpAccountName" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group" id="divroster" runat="server" visible="false">
                                    <asp:Label ID="Label31" runat="server" class="col-sm-2 control-label">
                                                On&nbsp;Roster&nbsp;List</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkOnRoster" runat="server" />
                                        <label for="<%=chkOnRoster.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group" id="divpayown" runat="server" visible="false">
                                    <asp:Label ID="Label32" runat="server" class="col-sm-2 control-label">
                                                Company&nbsp;Pays&nbsp;Super</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkPaysOwnSuper" runat="server" />
                                        <label for="<%=chkPaysOwnSuper.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group" id="divgstyown" runat="server" visible="false">
                                    <asp:Label ID="Label33" runat="server" class="col-sm-2 control-label">
                                                GST&nbsp;Payment</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkGSTPayment" runat="server"></asp:CheckBox>
                                        <label for="<%=chkGSTPayment.ClientID %>">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <asp:Button CssClass="btn redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" />
                                        <asp:Button CssClass="btn btn-success btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>


                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">

                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i>&nbsp;Transaction Successful!
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <i class="icon-remove-sign"></i>&nbsp;Record with this name already exists.
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i>&nbsp;There are no items to show in this view
                        </div>
                    </div>

                    <div class="searchfinal searchbar mainGridTable main-card mb-3 card">
                        <div class="widget-body shadownone brdrgray ">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header">

                                <div>Active Users</div>

                                <div class="searchFilters btn-actions-pane-right">
                                    <div class="input-group col-sm-3">
                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Employee Name" CssClass="form-control m-b"></asp:TextBox>
                                    </div>
                                    <div class="input-group col-sm-2">
                                        <asp:TextBox ID="txtusername" runat="server" placeholder="UserName" CssClass="form-control m-b"></asp:TextBox>
                                    </div>
                                    <div class="input-group col-sm-2">
                                        <asp:DropDownList ID="ddlsearchemptype" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalproject">
                                            <asp:ListItem Value="0">Employee Type</asp:ListItem>
                                            <asp:ListItem Value="1">Employee</asp:ListItem>
                                            <asp:ListItem Value="2">Dealer</asp:ListItem>
                                            <asp:ListItem Value="3">Contractor </asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="input-group col-sm-2">
                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalproject">
                                            <asp:ListItem Value="">Teams</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="input-group col-sm-4">
                                        <asp:DropDownList ID="ddlsearchrole" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalproject">
                                            <asp:ListItem Value="">Roles</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="input-group col-sm-2">
                                        <asp:Button ID="btnSearch" runat="server" OnClientClick="ActiveLoader()" CausesValidation="false" CssClass="btn-shadow dropdown-toggle btn btn-info btnsearchicon btngray" Text="Search" OnClick="btnSearch_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="finalgrid">
                            <div class="table-responsive printArea employeeTable">
                                <div id="PanGrid" runat="server">
                                    <div class="table-responsive">
                                        <asp:HiddenField ID="HfEmpId" runat="server" />
                                        <asp:GridView ID="GridView1" DataKeyNames="EmployeeID" runat="server" CssClass="align-middle mb-0 table table-borderless table-striped table-hover"
                                            OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnRowCreated="GridView1_RowCreated" OnDataBound="GridView1_DataBound" OnRowDeleting="GridView1_RowDeleting"
                                            AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="EmployeeID">
                                                    <ItemTemplate>
                                                        <%#Eval("EmployeeID")%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                                    <ItemTemplate>
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3">
                                                                    <div class="widget-content-left">
                                                                        <img width="40" class="rounded-circle" src="/assets/images/avatars/4.jpg" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading"><%#Eval("fullname")%></div>
                                                                    <div class="widget-subheading opacity-7"><%#Eval("JobTitle")%></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EmpTypeValue" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="EmpPhoneNo">
                                                    <ItemTemplate>
                                                        <%#Eval("EmpTypeValue")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Telephone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="EmpPhoneNo">
                                                    <ItemTemplate>
                                                        <%#Eval("EmpPhoneNo")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="UserName">
                                                    <ItemTemplate>
                                                        <%#Eval("UserName")%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Telephone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="EmpPhoneNo">
                                                    <ItemTemplate>
                                                        <%#Eval("EmpPhoneNo")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="UserName">
                                                    <ItemTemplate>
                                                        <%#Eval("UserName")%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>--%>
                                                <%-- <asp:TemplateField HeaderText="Telephone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="EmpPhoneNo">
                                                    <ItemTemplate>
                                                        <%#Eval("EmpPhoneNo")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="UserName">
                                                    <ItemTemplate>
                                                        <%#Eval("UserName")%>
                                                    </ItemTemplate>--%>

                                                <%--</asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Password" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Password">
                                                    <ItemTemplate>
                                                        <%#Eval("Password")%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Roles" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Repeater ID="rptrole" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="hypLeadDetail" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title='<%# Eval("RoleName")%>'>
                                                            <i ></i> <%# Eval("RoleName")%> 
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <SeparatorTemplate>, </SeparatorTemplate>
                                                        </asp:Repeater>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sales Team" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="SalesTeam">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hndEmployeeID" Value='<%#Eval("EmployeeID")%>' />
                                                        <asp:Repeater ID="rptTeam" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="hypLeadDetail" runat="server" data-toggle="tooltip" data-placement="top" title="" data-original-title='<%# Eval("SalesTeam")%> '>
                                                            <i ></i> <%# Eval("SalesTeam")%>  
                                                                </asp:Label>
                                                            </ItemTemplate>

                                                            <%-- <SeparatorTemplate >, </SeparatorTemplate>--%>
                                                        </asp:Repeater>

                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="ActiveEmp">
                                                    <ItemTemplate>
                                                        <span id="divstatus" visible='<%# Eval("ActiveEmp").ToString() == "True"?true:false %>' class="badge status active" runat="server">Active</span>
                                                        <span id="Span1" visible='<%# Eval("ActiveEmp").ToString() == "False"?true:false %>' class="badge status inactive" runat="server">In Active</span>

                                                        <%--<asp:HiddenField ID="hidstatus" runat="server" Value='<%# Eval("employeestatusname")%>'></asp:HiddenField>
                                                        <span id="divstatus" class="badge status active" runat="server"><%# Eval("employeestatusname")%></span>--%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Actions" SortExpression="EmployeeId">
                                                    <ItemTemplate>
                                                        <div class="dropdown d-inline-block">
                                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 dropdown-toggle noArrow"></button>
                                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropDownList">
                                                                <asp:HyperLink ID="hypLeadDetail" runat="server" CssClass="" Target="_blank"
                                                                    NavigateUrl='<%#"~/admin/adminfiles/masters/empdetails.aspx?id=" + Eval("EmployeeId")%>'>
                                                            <i class="fa fa-link"></i> Detail
                                                                </asp:HyperLink>
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CssClass="" CommandName="Select" CausesValidation="false" title="" data-original-title="Edit">
                                                                <i class="fa fa-edit"></i> Edit
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="panelImgLinkBtn" runat="server" CommandName="AddDocument" CommandArgument='<%#Eval("EmployeeID")%>'><i class="fa fa-upload"></i>
                                                                  Upload Document </asp:LinkButton>
                                                                <asp:LinkButton ID="lbtnReset" runat="server" CssClass="" CommandName="Reset" data-original-title="Reset Password"
                                                                    CommandArgument='<%#Eval("EmployeeID")%>' CausesValidation="false">
                                                        <i class="fa fa-refresh"></i> Reset Password
                                                                </asp:LinkButton>

                                                                <%--<asp:HyperLink ID="HyperLink1" runat="server" CssClass=""
                                                                    NavigateUrl='<%#"~/admin/adminfiles/masters/empdocuments.aspx?id=" + Eval("EmployeeId")%>'>
                                                            <i class="fa fa-link"></i> Add Document
                                                                </asp:HyperLink>--%>

                                                                <asp:LinkButton ID="lbtnLock" runat="server" CommandName="Lock" CssClass="" CommandArgument='<%#Eval("userid")%>'
                                                                    Visible='<%# Eval("IsLockedOut").ToString()=="False"?true:false %>'
                                                                    CausesValidation="false"><i class="fa fa-lock"></i> Lock User
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="lbtnUnLock" runat="server" CommandName="UnLock" class="tooltips" CommandArgument='<%#Eval("userid")%>'
                                                                    Visible='<%# Eval("IsLockedOut").ToString()=="True"?true:false %>' CausesValidation="false"><i class="fa fa-unlock"></i> Unlock
                                                                </asp:LinkButton>
                                                                <%--<asp:LinkButton  runat="server" CssClass="" CausesValidation="false" OnClientClick='<%# "return fnDelete("+ Eval("EmployeeID")+ ");" %>'
                                                                    CommandArgument='<%#Eval("EmployeeID")%>'><i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>--%>
                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="" CausesValidation="false"
                                                                    CommandName="Delete" CommandArgument='<%#Eval("EmployeeID")%>'><i class="fa fa-trash"></i> Delete
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                </asp:TemplateField>

                                            </Columns>

                                            <AlternatingRowStyle />
                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>

                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                        <asp:HiddenField runat="server" ID="hdncountdata" />

                                        <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr class="paginationRow">
                                                <td class="showEntryBtmBox">
                                                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myvalcomp">
                                                        <asp:ListItem Value="25">Display</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="dispFlexBox">
                                                    <asp:Label ID="Label41" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                                    <div class="pagination">
                                                        <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                        <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                                <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                        <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div class="paginationnew1 xscroll" runat="server" id="divnopage1">
                                        <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td class="showEntryBtmBox">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged" aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Display </asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <asp:HiddenField ID="hdndelete" runat="server" />
            <asp:Button ID="Button5" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>

            <div id="Div14" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message">

                <div class="modal-dialog">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-trash popupIcon"></i>Delete</h4>
                            </div>
                        </div>
                        <%-- <label id="ghh" runat="server"></label> --%>
                        <div class="modal-body">

                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You want to Delete This Entry?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " runat="server">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 textRight">
                                        <asp:LinkButton ID="LinkButton6" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <cc1:ModalPopupExtender ID="ModalPopupExtenderDetails" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="myModal" TargetControlID="btnNULL"
                CancelControlID="lnkcancel">
            </cc1:ModalPopupExtender>
            <div id="myModal" runat="server" style="display: none; width: 100%" class="modal_popup addDocumentPopup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel"><i class="pe-7s-news-paper popupIcon"></i>Add Document</h4>
                                <div>
                                    <asp:LinkButton ID="lnkcancel" runat="server" type="button" class="btn largeButton redBtn btncancelIcon btnClose" data-dismiss="modal">Close
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="modal-body paddnone" runat="server" id="divdetail">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                        </label>
                                    </span>
                                    <div class="">


                                        <div class="spicaldivin " id="div2" runat="server">
                                            <div class="">
                                                <div class="row marginbtm15">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblLastName" class="control-label" runat="server">Document Name</asp:Label>
                                                            <div class="marginbtm15">

                                                                <asp:DropDownList ID="ddlName" runat="server"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="ddlName" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:Label ID="Label40" runat="server" class="control-label"> Document Number</asp:Label>
                                                        <asp:TextBox ID="txtNumber" runat="server" MaxLength="30" CssClass="form-control"
                                                            placeholder="Number"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtNumber" Display="Dynamic" ValidationGroup="uploadpdf" InitialValue=""></asp:RequiredFieldValidator>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="formainline">

                                                            <div class="topfileuploadbox marbtm15">

                                                                <div class="form-group" style="margin-bottom: 0px;">
                                                                    <div style="word-wrap: break-word;">
                                                                        <span class="name">
                                                                            <label class="control-label">
                                                                                Upload File <span class="symbol required"></span>
                                                                            </label>
                                                                            <label class="custom-fileupload">
                                                                                <div class="col-md-12">
                                                                                    <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                                    <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="FileUpload1" ClientValidationFunction="getsubcat();" />--%>
                                                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;" class="custom-file-input" />
                                                                                            <span class="custom-file-control form-control-file form-control"></span>
                                                                                            <span class="btnbox">Upload file</span>

                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" CssClass=""
                                                                                                ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="uploadpdf"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="FileUpload1"
                                                                                                ValidationGroup="uploadpdf" ValidationExpression="^.+(.pdf)$" Style="color: red;"
                                                                                                Display="Dynamic" ErrorMessage=".pdf only"></asp:RegularExpressionValidator>
                                                                                            <div class="clear">
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="Button2" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                    </div>

                                                                    </label>
                                                                            <div class="clear">
                                                                            </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <%--<div class="form-group marginleft center-text" style="margin-top: 25px; margin-bottom: 0px;">
                                                            <asp:Button ID="ibtnUploadPDF" runat="server" Text="Upload" OnClick="ibtnUploadPDF_Click"
                                                                ValidationGroup="uploadpdf" CssClass="btn btn-primary savewhiteicon" CausesValidation="true" />
                                                        </div>--%>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="textRight">

                                    <asp:Button CssClass="btn largeButton greenBtn redreq btnaddicon" ID="Button2" runat="server" OnClick="btnAdd_Click"
                                        Text="Add" ValidationGroup="uploadpdf" />
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <%--reset password--%>
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="LinkButton5" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup">

                <div class="modal-dialog ">
                    <div class=" modal-content ">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel1"><i class="pe-7s-pen popupIcon"></i>Reset Password</h4>
                            </div>
                        </div>
                        <div class="modal-body paddnone" runat="server" id="div4">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You Want To Reset your password?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " id="div5" runat="server">
                                            <div class="col-sm-12">
                                                <div class="row marginbtm15">
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12 textRight">
                                        <asp:LinkButton ID="lnkresetassword" runat="server" OnClick="lnkdelete_Click" CausesValidation="false" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton5" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <%--End--%>
            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

            <%-----lock popup--%>
            <asp:Button ID="Button3" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderLock" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="Div6" DropShadow="false" CancelControlID="lnklockCancel" OkControlID="btnOKMobile" TargetControlID="Button3">
            </cc1:ModalPopupExtender>
            <div id="Div6" runat="server" style="display: none; width: 60%" class="modal_popup">
                <div class="modal-dialog ">
                    <div class=" modal-content ">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel2"><i class="pe-7s-unlock popupIcon"></i>Lock User</h4>
                            </div>
                        </div>
                        <div class="modal-body paddnone" runat="server" id="div7">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You Want To Lock User?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " id="div8" runat="server">
                                            <div class="col-sm-12">
                                                <div class="row marginbtm15">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="textRight">
                                        <asp:LinkButton ID="lnklock" runat="server" OnClick="lnklock_Click" CausesValidation="false" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="lnklockCancel" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <%--End--%>


            <%-----Unlock popup--%>
            <asp:Button ID="Button4" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderUnlock" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="Div11" DropShadow="false" CancelControlID="btnunlockcancel" OkControlID="btnOKMobile" TargetControlID="Button4">
            </cc1:ModalPopupExtender>
            <div id="Div11" runat="server" style="display: none; width: 60%" class="modal_popup">
                <div class="modal-dialog ">
                    <div class=" modal-content ">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel3"><i class="pe-7s-pen popupIcon"></i>UnLock User</h4>
                            </div>
                        </div>
                        <div class="modal-body paddnone" runat="server" id="div12">
                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure You Want To UnLock User?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " id="div13" runat="server">
                                            <div class="col-sm-12">
                                                <div class="row marginbtm15">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 textRight">
                                        <asp:LinkButton ID="btnunlock" runat="server" CausesValidation="false" OnClick="btnunlock_Click" CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                        <asp:LinkButton ID="btnunlockcancel" runat="server" data-dismiss="modal" CssClass="btn-shadow btn btn-danger btngray"><i class="fa fa-times"></i>No</asp:LinkButton>

                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <%--End--%>
            </span>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <%--<asp:PostBackTrigger ControlID="ddlStreetState" />--%>
            <%--   <asp:PostBackTrigger ControlID="ddlDistrict" />
            <asp:PostBackTrigger ControlID="ddltaluka" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
