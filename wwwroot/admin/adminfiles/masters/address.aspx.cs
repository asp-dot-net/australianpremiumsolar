﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Net;
using Newtonsoft.Json;
//using FormbayClientApi;
using System.Configuration;
using RestSharp;
using System.Web.Script.Serialization;
using System.Web.Services;

public partial class admin_adminfiles_company_address : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//dee03942-90cb-4b95-8061-6b18e9d13588
        if (Request.QueryString["s"] == "auto")
        {
            string terms = Request.QueryString["term"];
            string url = "http://api.addressify.com.au/address/autoComplete?api_key=2ead168e-f669-41f2-882e-7b53ebc678a9&term=" + terms;
            CallJSON(terms, url);
        }
        if (Request.QueryString["s"] == "address")
        {
            string terms = Request.QueryString["term"];
            string url = "http://api.addressify.com.au/address/getParsedAddress?api_key=2ead168e-f669-41f2-882e-7b53ebc678a9&term=" + terms;
            CallJSON(terms, url);
        }
        if (Request.QueryString["s"] == "validate")
        {
            string terms = Request.QueryString["term"];
            string url = "http://api.addressify.com.au/address/validate?api_key=2ead168e-f669-41f2-882e-7b53ebc678a9&term=" + terms;
            CallJSON(terms, url);
        }
      
    }


    public void CallJSON(string terms, string url)
    {
        //string data = "{\"service\":\"absence.list\", \"company_id\":3}";
        string data = "{}";
        WebRequest myReq = WebRequest.Create(url);
        myReq.Method = "POST";
        myReq.ContentLength = data.Length;
        myReq.ContentType = "application/json; charset=UTF-8";

        //string usernamePassword = "YOUR API TOKEN HERE" + ":" + "x";

        UTF8Encoding enc = new UTF8Encoding();
        using (Stream ds = myReq.GetRequestStream())
        {
            ds.Write(enc.GetBytes(data), 0, data.Length);
        }
        WebResponse wr = myReq.GetResponse();
        Stream receiveStream = wr.GetResponseStream();
        StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
        string content = reader.ReadToEnd();
        GetCallbackData(content);
    }
    public void GetCallbackData(string data)
    {
        string JsonCallback = Request["jsoncallback"];
        if (string.IsNullOrEmpty(JsonCallback))
            JsonCallback = Request["callback"];
        System.Web.HttpResponse response = HttpContext.Current.Response;
        response.ContentType = "application/json";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //Response.Write(string.Format("{0}({1});", JsonCallback, serializer.Serialize(data)));
        Response.Write(string.Format("{0}({1});", JsonCallback, data));
    }
}