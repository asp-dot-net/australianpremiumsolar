using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_dashboard : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {            
            //Response.Write("hiii");
            //Response.End();
            StUtilities st1 = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                      
            SiteURL = st1.siteurl;

            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("SalesRep"))
            {
                BindLastFollowUp();
                BindNewLead();
            }

            //BindNextFollowUp();
            BindRoles();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            lblusername.Text = st.EmpFirst + " " + st.EmpLast;
            DataTable dt_date = ClstblEmployeeTeam.aspnet_MembershipLastLoginDateSelectByUserId(userid);
            if (dt_date.Rows.Count > 0)
            {
                //string date = dt.Rows[0]["LastActivityDate"].ToString();
                //DateTime dt1 = Convert.ToDateTime(date);
                //lastupdatedate.Text = "LastUpdated" + " " + (dt1.ToString("h:mm tt, MMM dd, yyyy"));

                string date = dt_date.Rows[0]["LastLoginDate"].ToString();
                DateTime time = Convert.ToDateTime(dt_date.Rows[0]["LastLoginDate"].ToString());
                lbldate.Text = (time.ToString("dd MMM yyyy"));
                lbltime.Text = time.ToString("hh:mm tt");
            }


            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                string SalesTeam = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }
                if (SalesTeam != string.Empty)
                {
                    //ddlFollowUpEmployees.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                    //ddlFollowUpEmployees.DataMember = "fullname";
                    //ddlFollowUpEmployees.DataTextField = "fullname";
                    //ddlFollowUpEmployees.DataValueField = "userid";
                    //ddlFollowUpEmployees.DataBind();

                    //ddlnewLeadEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                    //ddlnewLeadEmployee.DataMember = "fullname";
                    //ddlnewLeadEmployee.DataTextField = "fullname";
                    //ddlnewLeadEmployee.DataValueField = "userid";
                    //ddlnewLeadEmployee.DataBind();
                }
            }
        }
    }
    public void BindRoles()
    {
        if (Roles.IsUserInRole("SalesRep"))
        {
            leadcount1.Visible = true;
            weeklystatus1.Visible = true;
            //projectstatus1.Visible = true;
            //paymentstatus1.Visible = true;
            //task1.Visible = true;
            //divTop10.Visible = true;
            //calender.Visible = true;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            leadcount1.Visible = true;
            weeklystatus1.Visible = true;
            //projectstatus1.Visible = true;
            //paymentstatus1.Visible = true;
            //task1.Visible = true;
            //imdeprec1.Visible = true;
            divTeamStatus.Visible = true;
            //calender.Visible = true;
            //divTop10.Visible = true;
            //divemployee.Visible = true;
            //divemployee1.Visible = true;
        }
        //if (Roles.IsUserInRole("PreInstaller"))
        //{
        //    leadcount1.Visible = true;
        //    projectstatus1.Visible = true;
        //    divPreInstaller.Visible = true;
        //    divWeatherReport.Visible = true;
        //}
        //if (Roles.IsUserInRole("PostInstaller"))
        //{
        //    leadcount1.Visible = true;
        //    projectstatus1.Visible = true;
        //    paymentstatus1.Visible = true;
        //    divPostInstaller.Visible = true;
        //}
        //if (Roles.IsUserInRole("Installation Manager"))
        //{
        //    leadcount1.Visible = true;
        //    projectstatus1.Visible = true;
        //    paymentstatus1.Visible = true;
        //    divPreInstaller.Visible = true;
        //    divWeatherReport.Visible = true;
        //    divPostInstaller.Visible = true;
        //}
        //if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager"))
        //{
        //    leadcount1.Visible = true;
        //    projectstatus1.Visible = true;
        //    paymentstatus1.Visible = true;
        //}
        //if (Roles.IsUserInRole("Lead Manager"))
        //{
        //    //leadcount1.Visible = true;
        //    //weeklystatus1.Visible = true;
        //    //projectstatus1.Visible = true;
        //    //paymentstatus1.Visible = true;
        //    //task1.Visible = true;
        //    //imdeprec1.Visible = true;
        //    //divTeamStatus.Visible = true;
        //}
        ////if (Roles.IsUserInRole("leadgen"))
        ////{

        ////}
        //if (Roles.IsUserInRole("Accountant"))
        //{
        //    divAccountant.Visible = true;
        //}
        //if (Roles.IsUserInRole("Purchase Manager"))
        //{
        //    divPurchaseManager.Visible = true;
        //}
        //if (Roles.IsUserInRole("WarehouseManager"))
        //{
        //    divWarehouseManager.Visible = true;
        //}
        //if (Roles.IsUserInRole("STC"))
        //{
        //    divSTC.Visible = true;
        //}
        //if (Roles.IsUserInRole("Verification"))
        //{
        //    divVerification.Visible = true;
        //}
    }
    public void BindNewLead()
    {
        //PanGridNewLead.Visible = false;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt;

        if (Roles.IsUserInRole("SalesRep"))
        {
            dt = ClstblCustomers.tblCustomers_Select_NewLead_ByUserId(userid);
            dv = new DataView(dt);

            //if (dt.Rows.Count > 0)
            //{
            //    PanGridNewLead.Visible = true;
            //    GridViewNewLead.DataSource = dt;
            //    GridViewNewLead.DataBind();
            //    divlead.Attributes.Add("style", "overflow-y:scroll; height:350px;");
            //}
            //else
            //{
            //    Div1.Visible = true;
            //    GridViewNewLead.Visible = false;
            //    divlead.Attributes.Add("style", "height:350px;");
            //}
            //GridViewNewLead.Columns[1].Visible = false;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            //string user = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            //if (ddlnewLeadEmployee.SelectedValue != "")
            //{
            //    user = ddlnewLeadEmployee.SelectedValue;
            //}
            //else
            //{
            //    user = "";
            //}
            //Response.Write(user);
            //   dt = ClstblCustomers.tblCustomers_Select_NewLead_ByUserId(user);

            //  dv = new DataView(dt);
            //if (dt.Rows.Count > 0)
            //{
            //    PanGridNewLead.Visible = true;
            //    GridViewNewLead.DataSource = dt;
            //    GridViewNewLead.DataBind();
            //    divlead.Attributes.Add("style", "overflow-y:scroll; height:350px;");
            //}
            //else
            //{
            //    Div1.Visible = true;
            //    GridViewNewLead.Visible = false;
            //    divlead.Attributes.Add("style", "height:350px;");
            //}
        }
    }
    //public void BindNewLead()
    //{
    //    //PanGridNewLead.Visible = false;
    //    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //    DataTable dt;
    //    if (Roles.IsUserInRole("SalesRep"))
    //    {
    //        dt = ClstblCustomers.tblCustomers_Select_NewLead_ByUserId(userid);
    //        dv = new DataView(dt);
    //        if (dt.Rows.Count > 0)
    //        {
    //            PanGridNewLead.Visible = true;
    //            GridViewNewLead.DataSource = dt;
    //            GridViewNewLead.DataBind();
    //            divlead.Attributes.Add("style", "overflow-y:scroll; height:350px;");
    //        }
    //        else
    //        {
    //            Div1.Visible = true;
    //            GridViewNewLead.Visible = false;
    //            divlead.Attributes.Add("style", "height:350px;");
    //        }
    //        GridViewNewLead.Columns[1].Visible = false;
    //    }
    //    if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager"))
    //    {
    //        dt = ClstblCustomers.tblCustomers_Select_NewLead_ByUserId("");
    //        dv = new DataView(dt);
    //        string EmployeeID = "0";
    //        if (ddlFollowUpEmployees.SelectedValue != "")
    //        {
    //            if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
    //            {
    //                EmployeeID = "0";
    //            }
    //            else
    //            {
    //                EmployeeID = ddlFollowUpEmployees.SelectedValue;
    //            }
    //        }
    //        else
    //        {
    //            EmployeeID = st.EmployeeID;
    //        }

    //        if (dt.Rows.Count > 0)
    //        {
    //            PanGridNewLead.Visible = true;
    //            GridViewNewLead.DataSource = dt;
    //            GridViewNewLead.DataBind(); 
    //            divlead.Attributes.Add("style", "overflow-y:scroll; height:350px;");
    //        }
    //        else
    //        {
    //            Div1.Visible = true;
    //            GridViewNewLead.Visible = false;
    //            divlead.Attributes.Add("style", "height:350px;");
    //        }
    //    }
    //}
    protected void GridViewNewLead_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //GridViewNewLead.PageIndex = e.NewPageIndex;
        BindNewLead();
    }
    public void BindLastFollowUp()
    {
        //  PanGridLast10FollowUps.Visible = true;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";

        //if (ddlFollowUpEmployees.SelectedValue != "")
        //{
        //    if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
        //    {
        //        EmployeeID = "0";
        //    }
        //    else
        //    {
        //        EmployeeID = ddlFollowUpEmployees.SelectedValue;
        //    }
        //}
        //else
        //{
        //    EmployeeID = st.EmployeeID;
        //}
        //Response.Write(EmployeeID);
        //Response.End();
        DataTable dt = ClstblCustInfo.tblCustInfo_TodayFollowup(EmployeeID);

        //if (dt.Rows.Count == 0)
        //{
        //    PanNoRecord.Visible = true;
        //    GridViewLastFollowUp.Visible = false;
        //    divfollow.Attributes.Add("style", "height:350px;");
        //}
        //else
        //{
        //    GridViewLastFollowUp.DataSource = dt;
        //    GridViewLastFollowUp.DataBind();
        //    PanNoRecord.Visible = false;
        //    GridViewLastFollowUp.Visible = true;
        //    divfollow.Attributes.Add("style", "overflow-y:scroll; height:350px;");
        //}
    }

    protected void GridViewLastFollowUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // GridViewLastFollowUp.PageIndex = e.NewPageIndex;
        BindLastFollowUp();
    }

    //public void BindNextFollowUp()
    //{
    //    //PanGridNext10FollowUps.Visible = true;
    //    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //    string EmployeeID = "0";
    //    if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
    //    {
    //        EmployeeID = "0";
    //    }
    //    else
    //    {
    //        EmployeeID = st.EmployeeID;
    //    }

    //}
    protected void GridViewNewLead_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Pager)
        //{
        //    GridViewRow gvr = e.Row;
        //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //}
    }
    //void lb_Command(object sender, CommandEventArgs e)
    //{
    //    GridViewNewLead.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    //}
    //protected void GridViewNewLead_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridViewNewLead.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridViewNewLead.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridViewNewLead.PageIndex - 2;
    //    page[1] = GridViewNewLead.PageIndex - 1;
    //    page[2] = GridViewNewLead.PageIndex;
    //    page[3] = GridViewNewLead.PageIndex + 1;
    //    page[4] = GridViewNewLead.PageIndex + 2;
    //    page[5] = GridViewNewLead.PageIndex + 3;
    //    page[6] = GridViewNewLead.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridViewNewLead.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;
    //            }
    //        }
    //    }
    //    if (GridViewNewLead.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridViewNewLead.PageIndex == GridViewNewLead.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;
    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridViewNewLead.PageSize * (GridViewNewLead.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridViewNewLead.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    protected void btnFollowupgo_Click(object sender, EventArgs e)
    {
        BindLastFollowUp();
        BindScript();
    }
    protected void btnLead_Click(object sender, EventArgs e)
    {
        BindNewLead();
        BindScript();
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(UpdateLead, this.GetType(), "MyAction", "doMyAction();", true);
    }

    
}
