<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="Job.aspx.cs" Inherits="admin_adminfiles_view_project" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            debugger;
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
                //  divexpandcollapse("", "");
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {


            function doMyAction() {
                alert('1');
                $('#<%=ibtnAdd.ClientID %>').click(function (e) {
                    formValidate();

                });
            }


        });


        function doMyAction1() {

            HighlightControlToValidate();
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });

            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=grdFollowUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdFollowUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();
            $('.mutliSelect input[type="checkbox"]').on('click', function () {

                callMultiCheckbox();
            });
        }
        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <%--<asp:Literal runat="server" ID="lt"></asp:Literal>--%>



    <%--        <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                function BeginRequestHandler(sender, args) {
                    $('.splash').css('display', 'block');
                }
                function EndRequestHandler(sender, args) {
                    //hide the modal popup - the update progress
                    //$('.splash').css('display', 'none');
                }
                prm.add_pageLoaded(pageLoaded);
                prm.add_beginRequest(BeginRequestHandler);
                prm.add_endRequest(EndRequestHandler);
                function pageLoaded() {

                    $('.splash').css('display', 'none');
                }

            </script>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        function pageLoaded() {
            $(".myvalproject").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            //  $('.i-checks').iCheck({
            //                        checkboxClass: 'icheckbox_square-green',
            //                        radioClass: 'iradio_square-green'
            //                    });


        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-browser icon"></i>Job</h5>

    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <%-- <asp:PostBackTrigger ControlID="GridView1" />--%>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="content animate-panel" style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
            </div>

            <div class="searchfinal searchFilterSection">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                <div class="">
                                    <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                        <tr>
                                            <td>
                                                <div class="inlineblock col-sm-12">
                                                    <div class="row">
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                                WatermarkText="Project Number" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtClient"
                                                                WatermarkText="Customer Name" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtClient" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>



                                                        <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                WatermarkText="Mobile Number" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group dislpyinlinediv  col-sm-2">

                                                            <asp:TextBox ID="txtSearchPostCodeFrom" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCodeFrom"
                                                                WatermarkText="Address" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchPostCodeFrom" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtSearchPostCodeFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>

                                                        </div>


                                                        <div class="input-group col-sm-1" id="tdEmployee" runat="server">
                                                            <asp:DropDownList ID="ddlStreetState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                        <div class="input-group col-sm-1" id="Div2" runat="server">
                                                            <asp:DropDownList ID="ddlDistrict" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">District</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-3 " id="Div3" runat="server">
                                                            <asp:TextBox runat="server" ID="ddltaluka" Enabled="true" class="form-control modaltextbox"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="ddltaluka"
                                                                WatermarkText="Select Taluka" />
                                                        </div>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddltaluka" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllTaluka"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        <div class="input-group col-sm-1 " id="Div4" runat="server">
                                                            <asp:TextBox runat="server" ID="ddlCity" Enabled="true" class="form-control modaltextbox" AutoPostBack="true"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="ddlCity"
                                                                WatermarkText="Select City" />
                                                        </div>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlCity" ServicePath="~/Search.asmx"
                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetAllCity"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />

                                                        <div class="input-group col-sm-1 ">
                                                            <asp:DropDownList ID="ddlElecDist" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject" OnSelectedIndexChanged="ddlElecDist_SelectedIndexChanged" AutoPostBack="true">
                                                                <asp:ListItem Value="">Select Discom</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 ">
                                                            <asp:DropDownList ID="ddldivision" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject" OnSelectedIndexChanged="ddldivision_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select Division</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 ">
                                                            <asp:DropDownList ID="ddlSubDivision" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">Sub Division</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1 " style="display: none">
                                                            <asp:TextBox ID="txtKw" runat="server"
                                                                Width="100%" CssClass="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtKw"
                                                                WatermarkText="KW" />
                                                        </div>

                                                        <div class="input-group col-sm-1 multiselect" style="display: none">
                                                            <dl class="dropdown">
                                                                <dt>
                                                                    <a href="#">
                                                                        <span class="hida" id="spanselect">Select</span>
                                                                        <p class="multiSel"></p>
                                                                    </a>
                                                                </dt>
                                                                <dd id="ddproject" runat="server">
                                                                    <div class="mutliSelect" id="mutliSelect">
                                                                        <ul>
                                                                            <asp:Repeater ID="lstSearchStatus" runat="server" OnItemDataBound="lstSearchStatus_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                        <%--  <span class="checkbox-info checkbox">--%>
                                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                            <span></span>
                                                                                        </label>
                                                                                        <%-- </span>--%>
                                                                                        <label class="chkval">
                                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                        </label>
                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                </dd>
                                                            </dl>
                                                        </div>

                                                        <div class="input-group">
                                                            <asp:DropDownList ID="ddlProjectStatus" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">Project Status</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-1" id="Div11" runat="server">
                                                            <asp:DropDownList ID="ddlSearchEmployee" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">Employee</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-3 ">
                                                            <asp:DropDownList ID="ddlchnlpart" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="0">Select Chanel Partner</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-3 " id="Div9" runat="server">
                                                            <asp:DropDownList ID="ddlApplicationStatus" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">Application Status</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group col-sm-2 " style="display: none">
                                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="">Select Team</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-3 " id="Div5" runat="server" style="display: none">
                                                            <asp:TextBox ID="txtPanelNo" runat="server"
                                                                Width="100%" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtPanelNo"
                                                                WatermarkText="Panel Number" />
                                                        </div>

                                                        <div class="input-group col-sm-3 " id="Div6" runat="server" style="display: none">
                                                            <asp:TextBox ID="txtInvertorNumber" runat="server"
                                                                Width="100%" CssClass="form-control" Text="Inverter Number"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtInvertorNumber"
                                                                WatermarkText="Inverter Number" />
                                                        </div>
                                                        <div class="input-group col-sm-3 " id="Div7" runat="server" style="display: none">
                                                            <asp:TextBox ID="txtCap" runat="server"
                                                                Width="100%" CssClass="form-control" Text=""></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtCap"
                                                                WatermarkText="System capacity from" />
                                                        </div>
                                                        <div class="input-group col-sm-3 " id="Div8" runat="server" style="display: none">
                                                            <asp:TextBox ID="txtCapTo" runat="server"
                                                                Width="100%" CssClass="form-control" Text=""></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtCapTo"
                                                                WatermarkText="System capacity to" />
                                                        </div>


                                                        <div class="input-group" id="Div10" runat="server">
                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                <asp:ListItem Value="1" Selected="True">Project Opened</asp:ListItem>
                                                                <asp:ListItem Value="3">First Quote Date</asp:ListItem>
                                                                <asp:ListItem Value="4">Quote Accepted</asp:ListItem>
                                                                <asp:ListItem Value="5">Dep. Received</asp:ListItem>
                                                                <asp:ListItem Value="6">Install Date</asp:ListItem>
                                                                <asp:ListItem Value="7">Active Date</asp:ListItem>
                                                                <asp:ListItem Value="8">FollowUp Date</asp:ListItem>
                                                                <asp:ListItem Value="9">Install Comp</asp:ListItem>
                                                                <%--<asp:ListItem Value="11">Stock Pickup</asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        </div>


                                                        <div class="input-group col-sm-1 " id="Div1" runat="server" visible="false">
                                                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtSearch"
                                                                WatermarkText="TEAM (CP)" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender8" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btngray wid100btn btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-info btngray wid100btn Excel" CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>

                </div>
            </div>
            <div>
                <div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <div>
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="row">

                                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                                                CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divFollowUp"
                                                OkControlID="btnOK" TargetControlID="btnNULL">
                                            </cc1:ModalPopupExtender>
                                            <div runat="server" id="divFollowUp" style="display: none;" class="modal_popup">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="color-line"></div>
                                                        <div class="modal-header">
                                                            <div style="float: right">
                                                                <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                                    Close
                                                                </button>
                                                            </div>

                                                            <h4 class="modal-title" id="myModalLabel" style="padding-top: 4px">
                                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                                <b>Add FollowUp</b></h4>
                                                        </div>
                                                        <div class="modal-body paddnone">
                                                            <div>
                                                                <div class="formainline ">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-4">
                                                                            <div class="row">
                                                                                <asp:Label ID="Label14" runat="server" class="col-sm-12 control-label">
                                                <strong>Contact</strong></asp:Label>
                                                                                <div class="col-sm-12">
                                                                                    <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                                                        aria-controls="DataTables_Table_0" CssClass="myvalproject">
                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Eurosolar</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Door to Door</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <br />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                                        ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group col-md-4">
                                                                            <div class="row">
                                                                                <asp:Label ID="Label3" runat="server" class="col-sm-12 control-label">
                                                <strong>Select</strong></asp:Label>
                                                                                <div class="col-sm-12">
                                                                                    <asp:DropDownList ID="ddlManager" runat="server" OnSelectedIndexChanged="ddlManager_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true"
                                                                                        CssClass="myvalproject">
                                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Follow Up</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Manager</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <br />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                                                                        ControlToValidate="ddlManager" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group col-md-4" id="divNextDate" runat="server" visible="false">
                                                                            <div class="row">
                                                                                <asp:Label ID="Label23" runat="server" class="col-md-12 control-label">
                                                <strong>Next Followup Date</strong></asp:Label>
                                                                                <div>
                                                                                    <div class="col-sm-12">
                                                                                        <div class="input-group date datetimepicker1">
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar"></span>
                                                                                            </span>
                                                                                            <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">

                                                                                            </asp:TextBox>


                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                                                ValidationGroup="info" ErrorMessage="" CssClass="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">

                                                                        <div class="form-group col-md-12" style="margin-top: 6px; margin-bottom: 10px;">
                                                                            <div class="row">
                                                                                <div>
                                                                                    <asp:Label ID="Label22" runat="server" class="col-sm-12 control-label">
                                                <strong>Description</strong></asp:Label>
                                                                                    <div class="col-sm-12">
                                                                                        <asp:TextBox ID="txtDescription" runat="server" class="form-control modaltextbox wdth100"></asp:TextBox>
                                                                                        <br />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                                                            ValidationGroup="info" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group  col-md-12  marbtm15 center-text">
                                                                        <!--<span class="name">
                                                                            <label class="control-label">&nbsp;</label>
                                                                        </span>-->
                                                                        <span>
                                                                            <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="ibtnAdd_Click"
                                                                                ValidationGroup="info" CssClass="btn btn-primary btnaddicon redreq addwhiteicon center-text" />
                                                                            <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                                                CssClass="btn" Text=" OK " />
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="seachfinal">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="tablescrolldiv xscroll">
                                                                            <div id="divgrdFollowUp" runat="server" class="table-responsive xscroll">
                                                                                <asp:GridView ID="grdFollowUp" DataKeyNames="CustInfoID" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                                                                                    AutoGenerateColumns="false" CssClass="tooltip-demo text-center GridviewScrollItem table table-striped table-bordered table-hover">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("Contact") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="150px" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Next FollowupDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("NextFollowupDate","{0:dd MMM yyyy}") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="150px" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("Description") %>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                                    <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                                                    <EmptyDataRowStyle Font-Bold="True" />
                                                                                    <RowStyle CssClass="GridviewScrollItem" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <asp:Button ID="btnNULL" Style="display: none;" runat="server" />
                                            <asp:HiddenField ID="hndCustomerID" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panel" runat="server">
            <div class="searchfinal">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header">
                        <div class="printorder" style="font-size: medium">
                            Total Number of panels:&nbsp;<asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="hpanel padtopzero">
            <div class="customerTable searchfinal searchbar mainGridTable main-card mb-3 card">
                <div class="finalgrid">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server" class="wid100">
                            <div class="xscroll">
                                <div class="table-responsive ">
                                    <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo  text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                                        OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                        <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/arrowright.png" />
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="100px" HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndprojectid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                    <asp:Label ID="lblProject9" runat="server">
                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/project/project.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                               <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="ProjectStatus" runat="server"><%#Eval("ProjectStatus")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Application Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="ApplicationStatus1" runat="server"><%#Eval("ApplicationStatus")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="sales RepName " ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Employeename" runat="server"><%#Eval("Employeename")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Sub Division" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Subdivision" runat="server"><%#Eval("Subdivision")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Customer_Name" runat="server"><%#Eval("CustomerName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="MobileNumber" runat="server"><%#Eval("MobileNumber")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="100px" HeaderText="installation booking date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Left" SortExpression="InstallPostCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProject18" runat="server"><%#Eval("BookingDate")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                                <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr>
                                                                        <td><b>Installation Address</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label2" runat="server"><%#Eval("Project")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Sys</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblProject11" runat="server"><%#Eval("stocksize")%></asp:Label>
                                                                        </td>
                                                                        <td><b>System Details</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblProject12" runat="server" data-toggle="tooltip" data-placement="top" CssClass="tooltipwidth"><%#Eval("panelDetail")%> + <%#Eval("inverterDetails")%></asp:Label>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td><b>Project Notes</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblProject7" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("ProjectNotes")%>' CssClass="tooltipwidth"><%#Eval("ProjectNotes")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Install Notes</b>
                                                                        </td>
                                                                        <td>

                                                                            <asp:Label ID="lblInstallNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>'><%#Eval("InstallerNotes")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Structure Height </b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server"><%#Eval("RooftopArea")%></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>


                                                                        <td><b>Discom Name</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server"><%#Eval("Discom")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Company Number</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label5" runat="server"><%#Eval("CompanyNumber")%></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>

                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td class="showEntryBtmBox">
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Display</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>


    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {

                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                //$("[data-toggle=tooltip]").tooltip();
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            }
        </script>
    </div>

    <script type="text/javascript">
        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');
        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


    </script>
    <asp:HiddenField runat="server" ID="hdncountdata" />
</asp:Content>
