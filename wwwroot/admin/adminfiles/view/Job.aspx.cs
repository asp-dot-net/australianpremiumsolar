using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_view_project : System.Web.UI.Page
{
    decimal totalpanels = 0;
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindSearchDropdown();
            BindGrid(0);
            BindScript();
        }

    }


    public void BindSearchDropdown()
    {

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();
        ddlElecDist.DataSource = ClstblCustSource.tblElecDistributor_Select();
        ddlElecDist.DataValueField = "ElecDistributorID";
        ddlElecDist.DataTextField = "ElecDistributor";
        ddlElecDist.DataBind();

        ddlDistrict.DataSource = ClstblState.tbl_DistrictName();
        ddlDistrict.DataMember = "DistrictName";
        ddlDistrict.DataTextField = "DistrictName";
        ddlDistrict.DataValueField = "DistrictName";
        ddlDistrict.DataBind();
        ddlStreetState.DataSource = ClstblState.tblStates_Select();
        ddlStreetState.DataMember = "FullName";
        ddlStreetState.DataTextField = "FullName";
        ddlStreetState.DataValueField = "FullName";
        ddlStreetState.DataBind();

        ddldivision.DataSource = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
        ddldivision.DataValueField = "Id";
        ddldivision.DataMember = "DivisionName";
        ddldivision.DataTextField = "DivisionName";
        ddldivision.DataBind();

        ddlSubDivision.DataSource = ClstblProjects.tbl_GetSubdivision();
        ddlSubDivision.DataMember = "SubDivisionName";
        ddlSubDivision.DataTextField = "SubDivisionName";
        ddlSubDivision.DataValueField = "ID";
        ddlSubDivision.DataBind();

        ddlApplicationStatus.DataSource = ClstblProjects.tbl_GetAllGovStatus();
        ddlApplicationStatus.DataMember = "StatusName";
        ddlApplicationStatus.DataTextField = "StatusName";
        ddlApplicationStatus.DataValueField = "Id";
        ddlApplicationStatus.DataBind();

        ddlProjectStatus.DataSource = ClstblProjects.tbl_GetAllProjectStatus();
        ddlProjectStatus.DataMember = "ProjectStatus";
        ddlProjectStatus.DataTextField = "ProjectStatus";
        ddlProjectStatus.DataValueField = "ProjectStatusID";
        ddlProjectStatus.DataBind();


        ddlchnlpart.DataSource = ClstblState.tblGetChnlPartner();
        ddlchnlpart.DataMember = "empFullName";
        ddlchnlpart.DataTextField = "empFullName";
        ddlchnlpart.DataValueField = "empFullName";
        ddlchnlpart.DataBind();


        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);

            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);



            }
        }
        else
        {
            ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }

        ddlSearchEmployee.DataMember = "fullname";
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();

        BindScript();
    }
    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(projectid);

            if (e.CommandName == "viewproject")
            {
                if (projectid != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    string SalesTeamID = stEmp.SalesTeamID;

                    if (Roles.IsUserInRole("Sales Manager"))
                    {
                        if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
                        {
                            Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
                    }
                }
            }
        }

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);

        if (e.CommandName.ToLower() == "addfollowupnote")
        {
            hndCustomerID.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            ddlContact.DataMember = "Contact";
            ddlContact.DataTextField = "Contact";
            ddlContact.DataValueField = "ContactID";
            ddlContact.DataBind();

            DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(hndCustomerID.Value);
            if (dt.Rows.Count > 0)
            {
                divgrdFollowUp.Visible = true;
                grdFollowUp.DataSource = dt;
                grdFollowUp.DataBind();

                try
                {
                    ddlContact.SelectedValue = dt.Rows[0]["ContactID"].ToString();
                }
                catch
                {
                }
            }
            else
            {
                divgrdFollowUp.Visible = false;
            }
            BindScript();
        }

        BindGrid(0);
    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Finance"))
        {
            Employeeid = "";
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //DataTable dt = new DataTable();

        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";

        DataTable dtcount = ClstblCustomers.query_execute(Count);
        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "(E.EmpType=" + EmpType1 + ") and (Convert(nvarchar(200),ET.SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))) and ";

            }
        }
        //dt = ClstblProjects.tblProjectsGetDataBySearch(employeeId, , , , ,,, , ,, , , , , , , , txtClient.Text, , , , , , , , ddlInstallBookingDate.SelectedValue, ,, , , , );
        string data = string.Empty;
        //string data = "select count(ProjectID) as countdata, count(NumberPanels)as NumberPanels from tblProjects P join tblProjectStatus PS on P.ProjectStatusID=Ps.ProjectStatusID left outer join tblEmployees E on P.EmployeeID=E.EmployeeID outer apply (select top 1 SalesTeamID from tblEmployeeTeam where EmployeeID=E.EmployeeID order by EmployeeID desc) ET  left outer join tblCustomers C on P.CustomerID=C.CustomerID left outer Join tblCustSource CS on C.CustSourceID=CS.CustSourceID left outer join tblContacts CM on P.ContactID=CM.ContactID left outer join tblFinanceWith FW on P.FinanceWithID=FW.FinanceWithID left outer join tblProjects2 P2 on P.ProjectID=P2.ProjectLinkID left outer join tblSalesTeams ST on ST.SalesTeamID=ET.SalesTeamID left outer join tblCustSourceSub CSS on C.CustSourceSubID=CSS.CustSourceSubID where " + data1 + " (P.ProjectTypeID='" + ddlProjectTypeID.SelectedValue.ToString() + "' or '" + ddlProjectTypeID.SelectedValue.ToString() + "'=0) and (P.ProjectStatusID in (select * from Split('" + selectedItem + "',',')) or '" + selectedItem + "'='') and (P.ProjectNumber='" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "' or '" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((upper(P.Project) like '%'+'" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearch.Text) + "'='') and (P.ProjectCancelID='" + ddlSearchCancel.SelectedValue + "' or '" + ddlSearchCancel.SelectedValue + "'=0) and ((upper(P.InstallAddress) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and ((upper(P.InstallCity) like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue.ToString() + "'+'%') or '" + ddlSearchState.SelectedValue.ToString() + "'='') and (C.CustSourceID='" + ddlSearchSource.SelectedValue.ToString() + "' or '" + ddlSearchSource.SelectedValue.ToString() + "'=0) and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "' or '" + System.Security.SecurityElement.Escape(txtCompanyNo.Text) + "'=0) and (ET.SalesTeamID='" + ddlTeam.SelectedValue + "' or '" + ddlTeam.SelectedValue + "'='') and ((upper(P.PanelModel) like '%'+'" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPModel.Text) + "'='') and ((upper(P.InverterModel) like '%'+'" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'+'%')  or '" + System.Security.SecurityElement.Escape(txtIModel.Text) + "'='') and ((C.Customer like '%'+'" + System.Security.SecurityElement.Escape(txtClient.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtClient.Text) + "'='') and (C.CustSourceSubID='" + ddlSerchSub.SelectedValue + "' or '" + ddlSerchSub.SelectedValue + "'=0) and (P.EmployeeID='" + ddlSearchEmployee.SelectedValue + "' or '" + ddlSearchEmployee.SelectedValue + "'=0) and ((P.InstallPostCode >= '" + txtSearchPostCodeFrom.Text + "') or '" + txtSearchPostCodeFrom.Text + "'=0) and ((P.InstallPostCode<= '" + txtSearchPostCodeTo.Text + "') or '" + txtSearchPostCodeTo.Text + "'=0)  and (P.SystemCapKW>='" + txtSysCapacityFrom.Text + "' or '" + txtSysCapacityFrom.Text + "'=0) and (P.SystemCapKW<='" + txtSysCapacityTo.Text + "' or '" + txtSysCapacityTo.Text + "'=0) and ('" + txtStartDate.Text + "' <= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else P.ProjectOpened end end end end end end end end END) and (('" + txtEndDate.Text + "' >= CASE WHEN '" + searchdate + "'=1 THEN P.ProjectOpened else CASE WHEN '" + searchdate + "'=3 then P.QuoteSent else CASE WHEN '" + searchdate + "'=4 then P.QuoteAccepted else CASE WHEN '" + searchdate + "'=5 then P.DepositReceived else CASE WHEN '" + searchdate + "'=6 then P.InstallBookingDate else CASE WHEN '" + searchdate + "'=7 then P.ActiveDate else CASE WHEN '" + searchdate + "'=8 then P.FollowUp else CASE WHEN '" + searchdate + "'=9 then P.InstallCompleted else CASE WHEN '" + searchdate + "'=2 then (select top 1 InvoicePayDate from tblInvoicePayments where ProjectID=P.ProjectID Order by InvoicePayDate ASC) else '" + txtEndDate.Text + "' end end end end end end end end END) or '" + txtEndDate.Text + "'='' ) and (P2.Promo1ID='" + ddlPromoOffer1.SelectedValue + "' or '" + ddlPromoOffer1.SelectedValue + "'=0) and (P2.Promo2ID='" + ddlPromoOffer2.SelectedValue + "' or '" + ddlPromoOffer2.SelectedValue + "'=0) and (P.FinanceWithID='" + ddlFinanceOption.SelectedValue + "' or '" + ddlFinanceOption.SelectedValue + "'=0) and ((P.ElecDistApproved= (case when '" + ddlDistApprovedDate.SelectedValue + "' = 1 then P.ElecDistApproved else case when '" + ddlDistApprovedDate.SelectedValue + "' = 2 then '' else '" + ddlDistApprovedDate.SelectedValue + "' end end )) or '" + ddlDistApprovedDate.SelectedValue + "'='') and ((upper(P.ElecDistApprovelRef) like '%'+'" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtElecDistApprovelRef.Text) + "'='') and ((P.PurchaseNo like '%' +'" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtPurchaseNo.Text) + "'='') and ((P.InstallBookingDate=(case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') = 1 then InstallBookingDate else case when CONVERT(varchar(20),'" + ddlInstallBookingDate.SelectedValue + "') =0 then '' else '" + ddlInstallBookingDate.SelectedValue + "' end END)) or  '" + ddlInstallBookingDate.SelectedValue + "'='')";

        DataTable dt = ClstblCustomers.query_execute(data);
        if (dt.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            hdncountdata.Value = countdata.ToString();
            lblTotalPanels.Text = Convert.ToInt32(dt.Rows[0]["NumberPanels"]).ToString();
        }

        #region MyRegionForPagination

        #endregion
    }
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        if (ddlSelectRecords.SelectedValue != "All")
        {
            startindex = (custompageIndex - 1) * Convert.ToInt32(ddlSelectRecords.SelectedValue) + 1;
        }
        else
        {
            startindex = 1;
        }
        dt = ClstblProjects.tblGetAllJobData();

        if (!string.IsNullOrEmpty(txtClient.Text))
        {
            var results = dt.AsEnumerable().Where(x => (x.Field<string>("CustomerName") != null)).ToList();
            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<string>("CustomerName").ToString() == txtClient.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
        }
        if (!string.IsNullOrEmpty(txtProjectNumber.Text))
        {
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<Int32>("ProjectNumber").ToString() == txtProjectNumber.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();

        }
        if (!string.IsNullOrEmpty(txtMQNsearch.Text))
        {
            var results = dt.AsEnumerable().Where(x => (x.Field<string>("MobileNumber") != null)).ToList();
            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<string>("MobileNumber").ToString() == txtMQNsearch.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

        }
        if (!string.IsNullOrEmpty(ddlDistrict.SelectedItem.Text))
        {
            if (ddlDistrict.SelectedItem.Text != "District")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<string>("district") != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                               where myRow.Field<string>("district").ToString() == ddlDistrict.SelectedItem.Text
                               select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }

        if (ddlchnlpart.SelectedValue != "0")
        {
            var results = dt.AsEnumerable().Where(x => (x.Field<string>("ChannelsPartners") != null)).ToList();
            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<string>("ChannelsPartners").ToString() == ddlchnlpart.SelectedItem.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
        }

        if (!string.IsNullOrEmpty(ddltaluka.Text))
        {
            var results = dt.AsEnumerable().Where(x => (x.Field<string>("taluka") != null)).ToList();

            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<string>("taluka").ToString() == ddltaluka.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
        }
        if (!string.IsNullOrEmpty(ddlCity.Text))
        {

            var results = dt.AsEnumerable().Where(x => (x.Field<string>("streetCity") != null)).ToList();

            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<string>("streetCity").ToString() == ddlCity.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
        }

        


        if (!string.IsNullOrEmpty(ddlElecDist.SelectedItem.Text))
        {
            if (ddlElecDist.SelectedItem.Text != "Select Discom")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<string>("Discom") != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                               where myRow.Field<string>("Discom").ToString() == ddlElecDist.SelectedItem.Text
                               select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }
        if (!string.IsNullOrEmpty(ddldivision.SelectedItem.Text))
        {
            if (ddldivision.SelectedItem.Text != "Select Division")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<Int32?>("divisionid").ToString() != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                               where myRow.Field<Int32?>("divisionid").ToString() == ddldivision.SelectedValue
                               select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }
        if (!string.IsNullOrEmpty(ddlSubDivision.SelectedItem.Text))
        {
            if (ddlSubDivision.SelectedItem.Text != "Sub Division")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<string>("Subdivision") != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                               where myRow.Field<string>("Subdivision").ToString() == ddlSubDivision.SelectedItem.Text
                               select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }
        if (!string.IsNullOrEmpty(txtKw.Text))
        {
            var results = dt.AsEnumerable().Where(x => (x.Field<decimal?>("pvcapacity") != null)).ToList();
            var results1 = from myRow in results.AsEnumerable()
                           where myRow.Field<decimal>("pvcapacity").ToString() == txtKw.Text
                           select myRow;
            dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

        }
        if (!string.IsNullOrEmpty(ddlProjectStatus.SelectedItem.Text))
        {
            if (ddlProjectStatus.SelectedItem.Text != "Project Status")
            {
                var results = from myRow in dt.AsEnumerable()
                              where myRow.Field<string>("ProjectStatus").ToString() == ddlProjectStatus.SelectedItem.Text
                              select myRow;
                dt = results.Any() ? results.CopyToDataTable() : dt.Clone();
            }
        }

        if (!string.IsNullOrEmpty(ddlTeam.SelectedItem.Text))
        {
            if (ddlTeam.SelectedItem.Text != "Select Team")
            {
                var results = from myRow in dt.AsEnumerable()
                              where myRow.Field<string>("Employeename").ToString() == ddlTeam.SelectedItem.Text
                              select myRow;
                dt = results.Any() ? results.CopyToDataTable() : dt.Clone();
            }

        }
        if (!string.IsNullOrEmpty(ddlStreetState.SelectedItem.Text))
        {
            if (ddlStreetState.SelectedItem.Text != "State")
            {
                var results = from myRow in dt.AsEnumerable()
                              where myRow.Field<string>("InstallState").ToString() == ddlStreetState.SelectedItem.Text
                              select myRow;
                dt = results.Any() ? results.CopyToDataTable() : dt.Clone();
            }
        }

        if (!string.IsNullOrEmpty(txtPanelNo.Text))
        {
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<string>("PanelModel").ToString() == txtPanelNo.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();

        }
        if (!string.IsNullOrEmpty(txtInvertorNumber.Text) && txtInvertorNumber.Text != "Inverter Number")
        {
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<string>("InverterModel").ToString() == txtInvertorNumber.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();

        }
        if (!string.IsNullOrEmpty(ddlSearchEmployee.SelectedItem.Text))
        {
            if (ddlSearchEmployee.SelectedItem.Text != "Employee")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<string>("Employeename") != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                              where myRow.Field<string>("Employeename").ToString() == ddlSearchEmployee.SelectedItem.Text
                              select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }
        if (!string.IsNullOrEmpty(ddlApplicationStatus.SelectedItem.Text))
        {
            if (ddlApplicationStatus.SelectedItem.Text != "Application Status")
            {
                var results = dt.AsEnumerable().Where(x => (x.Field<string>("ApplicationStatus") != null)).ToList();

                var results1 = from myRow in results.AsEnumerable()
                               where myRow.Field<string>("ApplicationStatus").ToString() == ddlApplicationStatus.SelectedItem.Text
                               select myRow;
                dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
            }

        }
        if (ddlSearchDate.SelectedValue != "1")
        {
            if ((!string.IsNullOrEmpty(txtStartDate.Text)) && (!string.IsNullOrEmpty(txtEndDate.Text)))
            {
                DateTime startDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
                DateTime edDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);

                string selectedValue = ddlSearchDate.SelectedValue;
                if (selectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteSent").ToString())).ToList();
                    //dt = dt.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
                    //dt = dt.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteSent")).Date <= edDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteSent")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (selectedValue == "4")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteAccepted").ToString())).ToList();
                    //dt = dt.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
                    //dt = dt.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteAccepted")).Date >= startDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteAccepted")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "5")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("DepositReceived").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("DepositReceived")).Date >= startDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("DepositReceived")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "6")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= startDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "7")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    //dt = dt.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
                    //dt = dt.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date >= startDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "8")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("followupdate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("followupdate")).Date >= startDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("followupdate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "9")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallCompleted").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallCompleted")).Date >= startDate.Date
                                   &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallCompleted")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                //if (selectedValue == "11")
                //    dt = dt.Where(x => Convert.ToDateTime(x.QuoteSent).Date >= startDate.Date && Convert.ToDateTime(x.QuoteSent).Date < endDate.Date).ToList();
            }
            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                DateTime startDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
                string selectedValue = ddlSearchDate.SelectedValue;
                if (selectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteSent").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteSent")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (selectedValue == "4")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteAccepted").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteAccepted")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "5")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("DepositReceived").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("DepositReceived")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "6")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "7")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "8")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("followupdate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("followupdate")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "9")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallCompleted").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallCompleted")).Date >= startDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }

            }
            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                DateTime edDate = Convert.ToDateTime(txtEndDate.Text);
                string selectedValue = ddlSearchDate.SelectedValue;
                if (selectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteSent").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteSent")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (selectedValue == "4")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("QuoteAccepted").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("QuoteAccepted")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "5")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("DepositReceived").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("DepositReceived")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "6")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "7")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("ActiveDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("ActiveDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "8")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("followupdate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("followupdate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
                if (selectedValue == "9")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallCompleted").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallCompleted")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }

            }
        }

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = true;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Sales Manager"))
        {
            employeeId = "";
        }

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
        BindScript();
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindGrid(0);
        BindScript();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start

        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;

        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);

        //List<FilterData> studentList = new List<FilterData>();

        //DataTable dt = new DataTable();
        //dv = new DataView(dt);
        //dt = ClstblProjects.tblGetAllJobDataBySearch();
        //List<FilterData> mDataList = new List<FilterData>();

        //studentList = (from DataRow dr in dt.Rows
        //               select new FilterData()
        //               {
        //                   ProjectId = dr["ProjectID"].ToString(),
        //                   customername = dr["CustomerName"].ToString(),
        //                   projectnumber = dr["ProjectNumber"].ToString(),
        //                   ConsumerNumber = dr["ConsumerNumber"].ToString(),
        //                   MobileNumber = dr["MobileNumber"].ToString(),
        //                   Discom = dr["Discom"].ToString(),
        //                   Divison = dr["divisionId"].ToString(),
        //                   SubDivision = dr["Subdivision"].ToString(),
        //                   Address = dr["Paddress"].ToString(),
        //                   state = dr["StreetState"].ToString(),
        //                   City = dr["StreetCity"].ToString(),
        //                   Taluka = dr["Taluka"].ToString(),
        //                   FirstQuoteDate = dr["ProjectQuoteDate"].ToString(),
        //                   pannelmodel = dr["panelModel"].ToString(),
        //                   nvertormodel = dr["panelModel"].ToString(),
        //                   Kw = dr["KW"].ToString(),
        //                   Employeename = dr["Employeename"].ToString(),
        //                   ApplicationStatus = dr["ApplicationStatus"].ToString(),
        //                   ProjectStatus = dr["ProjectStatus"].ToString(),
        //                   BookingDate = dr["BookingDate"].ToString(),
        //                   Project = dr["Project"].ToString(),
        //                   inverterDetails = dr["inverterDetails"].ToString(),
        //                   ProjectNotes = dr["ProjectNotes"].ToString(),
        //                   InstallerNotes = dr["InstallerNotes"].ToString(),
        //                   RooftopArea = dr["RooftopArea"].ToString(),
        //                   stocksize = dr["stocksize"].ToString(),
        //                   panelDetail = dr["panelDetail"].ToString(),
        //                   CustomerID = dr["CustomerID"].ToString(),
        //                   QuoteSent = dr["QuoteSent"].ToString(),
        //                   QuoteAccepted = dr["QuoteAccepted"].ToString(),
        //                   DepositReceived = dr["DepositReceived"].ToString(),
        //                   ActiveDate = dr["ActiveDate"].ToString(),
        //                   InstallCompleted = dr["InstallCompleted"].ToString(),
        //                   followupdate = dr["followupdate"].ToString(),
        //                   InstallBookingDate = dr["InstallBookingDate"].ToString(),
        //                   CompanyNumber = dr["CompanyNumber"].ToString(),


        //               }).ToList();


        //if (!string.IsNullOrEmpty(txtClient.Text))
        //{
        //    studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();
        //}
        //if (!string.IsNullOrEmpty(txtProjectNumber.Text))
        //{
        //    studentList = studentList.Where(x => x.projectnumber.Contains(txtProjectNumber.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtMQNsearch.Text))
        //{
        //    studentList = studentList.Where(x => x.MobileNumber.Contains(txtMQNsearch.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlElecDist.SelectedItem.Text))
        //{
        //    if (ddlElecDist.SelectedItem.Text != "Select Discom")
        //        studentList = studentList.Where(x => x.Discom.Contains(ddlElecDist.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddldivision.SelectedItem.Text))
        //{
        //    if (ddldivision.SelectedItem.Text != "Select Division")
        //        studentList = studentList.Where(x => x.Divison.Contains(ddldivision.SelectedValue)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlSubDivision.SelectedItem.Text))
        //{
        //    if (ddlSubDivision.SelectedItem.Text != "Sub Division")
        //        studentList = studentList.Where(x => x.SubDivision.Contains(ddlSubDivision.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtKw.Text))
        //{
        //    studentList = studentList.Where(x => x.Kw.Contains(txtKw.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlProjectStatus.SelectedItem.Text))
        //{
        //    if (ddlProjectStatus.SelectedItem.Text != "Project Status")
        //        studentList = studentList.Where(x => x.ProjectStatus.Contains(ddlProjectStatus.SelectedItem.Text)).ToList();

        //}

        //if (!string.IsNullOrEmpty(ddlTeam.SelectedItem.Text))
        //{
        //    if (ddlTeam.SelectedItem.Text != "Select Team")
        //        studentList = studentList.Where(x => x.team.Contains(ddlTeam.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlStreetState.SelectedItem.Text))
        //{
        //    if (ddlStreetState.SelectedItem.Text != "State")
        //        studentList = studentList.Where(x => x.state.Contains(ddlStreetState.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddltaluka.Text))
        //{
        //    studentList = studentList.Where(x => x.Taluka.Contains(ddltaluka.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlCity.Text))
        //{
        //    studentList = studentList.Where(x => x.City.Contains(ddlCity.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtPanelNo.Text))
        //{
        //    studentList = studentList.Where(x => x.pannelmodel.Contains(txtPanelNo.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtInvertorNumber.Text))
        //{
        //    studentList = studentList.Where(x => x.pannelmodel.Contains(txtInvertorNumber.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtCap.Text))
        //{
        //    studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(txtCapTo.Text))
        //{
        //    // studentList = studentList.Where(x => x.customername.Contains(txtClient.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlSearchEmployee.SelectedItem.Text))
        //{
        //    if (ddlSearchEmployee.SelectedItem.Text != "Employee")
        //        studentList = studentList.Where(x => x.Employeename.Contains(ddlSearchEmployee.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlApplicationStatus.SelectedItem.Text))
        //{
        //    if (ddlApplicationStatus.SelectedItem.Text != "Application Status")
        //        studentList = studentList.Where(x => x.ApplicationStatus.Contains(ddlApplicationStatus.SelectedItem.Text)).ToList();

        //}
        //if (!string.IsNullOrEmpty(ddlSearchDate.SelectedItem.Text) && ddlSearchDate.SelectedItem.Text != "Project Opened")
        //{
        //    if (ddlSearchDate.SelectedValue != "1" && (!string.IsNullOrEmpty(txtStartDate.Text)) && (!string.IsNullOrEmpty(txtEndDate.Text)))
        //    {
        //        DateTime startDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
        //        DateTime endDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);

        //        string selectedValue = ddlSearchDate.SelectedValue;
        //        if (selectedValue == "3")

        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "4")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteAccepted)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "5")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.DepositReceived)).ToList();
        //            studentList = studentList.Where(x => Convert.ToDateTime(x.DepositReceived).Date >= startDate.Date && Convert.ToDateTime(x.DepositReceived).Date <= endDate.Date).ToList();
        //        }
        //        if (selectedValue == "6")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
        //        }
        //        if (selectedValue == "7")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.ActiveDate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.ActiveDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.ActiveDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "8")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.followupdate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "9")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallCompleted)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date >= startDate && DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        //if (selectedValue == "11")
        //        //    studentList = studentList.Where(x => Convert.ToDateTime(x.QuoteSent).Date >= startDate.Date && Convert.ToDateTime(x.QuoteSent).Date < endDate.Date).ToList();
        //    }
        //    else if (string.IsNullOrEmpty(txtStartDate.Text))
        //    {

        //        //DateTime startDate = Convert.ToDateTime(txtStartDate.Text);
        //        DateTime endDate = Convert.ToDateTime(txtEndDate.Text);
        //        string selectedValue = ddlSearchDate.SelectedValue;
        //        if (selectedValue == "3")

        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteSent)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteSent, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "4")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.QuoteAccepted)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.QuoteAccepted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "5")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.DepositReceived)).ToList();
        //            studentList = studentList.Where(x => Convert.ToDateTime(x.DepositReceived).Date <= endDate.Date).ToList();
        //        }
        //        if (selectedValue == "6")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallBookingDate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallBookingDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();
        //        }
        //        if (selectedValue == "7")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.ActiveDate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.ActiveDate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "8")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.followupdate)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.followupdate, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }
        //        if (selectedValue == "9")
        //        {
        //            studentList = studentList.Where(x => !string.IsNullOrEmpty(x.InstallCompleted)).ToList();
        //            studentList = studentList.Where(x => DateTime.ParseExact(x.InstallCompleted, "dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("fr-FR")).Date <= endDate).ToList();

        //        }

        //    }
        //}

        //GridView1.DataSource = studentList;
        //GridView1.DataBind();

        //ddlSelectRecords.Visible = false;
        //GridView1.PagerSettings.Visible = false;
        //if (studentList.Count == 0)
        //{
        //    SetNoRecords();
        //    //PanNoRecord.Visible = true;
        //    PanGrid.Visible = false;
        //    divnopage.Visible = false;
        //}
        //else
        //{
        //    GridView1.DataSource = studentList;
        //    GridView1.DataBind();
        //    PanNoRecord.Visible = false;
        //    divnopage.Visible = true;
        //    if (studentList.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
        //    {
        //        if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < studentList.Count)
        //        {
        //            //========label Hide
        //            divnopage.Visible = false;
        //        }
        //        else
        //        {
        //            divnopage.Visible = true;
        //            int iTotalRecords = studentList.Count;
        //            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //            if (iEndRecord > iTotalRecords)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            if (iStartsRecods == 0)
        //            {
        //                iStartsRecods = 1;
        //            }
        //            if (iEndRecord == 0)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //        }
        //    }
        //    else
        //    {
        //        if (ddlSelectRecords.SelectedValue == "All")
        //        {
        //            divnopage.Visible = true;
        //            ltrPage.Text = "Showing " + studentList.Count + " entries";
        //        }
        //    }
        //    ltrPage.Text = "Showing " + studentList.Count + " entries";
        //}

    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        try
        {
            txtProjectNumber.Text = string.Empty;
            txtMQNsearch.Text = string.Empty;
            txtSearch.Text = string.Empty;
            ddlTeam.SelectedValue = string.Empty;
            ddlSearchDate.SelectedValue = string.Empty;
            txtClient.Text = string.Empty;
            ddlSearchEmployee.SelectedValue = string.Empty;
            txtSearchPostCodeFrom.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtClient.Text = string.Empty;
            ddltaluka.Text = string.Empty;
            ddlCity.Text = string.Empty;
            ddlElecDist.SelectedValue = string.Empty;
            ddldivision.SelectedValue = "0";
            ddlSubDivision.SelectedValue = string.Empty;
            ddlStreetState.SelectedValue = string.Empty;
            ddlDistrict.SelectedValue = string.Empty;
            ddlApplicationStatus.SelectedValue = string.Empty;
            ddlProjectStatus.SelectedValue = string.Empty;
            ddlSearchDate.SelectedValue = "1";
            ddlSearchEmployee.SelectedValue = string.Empty;


            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;

            foreach (RepeaterItem item in lstSearchStatus.Items)
            {
                CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
                chkselect.Checked = false;
            }

            BindGrid(0);
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        ddlContact.SelectedValue = "";
        //txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddlManager.ClearSelection();
    }
    protected void ibtnAdd_Click(object sender, EventArgs e)
    {
        BindScript();
        string CustomerID = hndCustomerID.Value;
        string Description = txtDescription.Text;
        //string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;

        if (ddlManager.SelectedValue == "1")
        {
            NextFollowupDate = txtNextFollowupDate.Text;
        }
        if (ddlManager.SelectedValue == "2")
        {
            NextFollowupDate = DateTime.Now.AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, ddlManager.SelectedValue);

        if (Convert.ToString(success) != string.Empty)
        {
            ModalPopupExtender2.Hide();
            ddlContact.SelectedValue = "";
            //txtFollowupDate.Text = string.Empty;
            txtNextFollowupDate.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }
        ddlManager.ClearSelection();

        ddlContact.SelectedValue = "";
        //txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;


        BindGrid(0);
    }
    protected void ddlManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        if (ddlManager.SelectedValue == "1")
        {
            divNextDate.Visible = true;
        }
        else
        {
            divNextDate.Visible = false;
        }
        BindScript();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //int nopanel = 0;
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");
            Label lblFollowUpDate = (Label)e.Row.FindControl("lblFollowUpDate");
            Label lblFollowUpNote = (Label)e.Row.FindControl("lblFollowUpNote");
            HyperLink detail1 = (HyperLink)e.Row.FindControl("hypDetail");
            HyperLink detail2 = (HyperLink)e.Row.FindControl("hypDetail2");
            if (Roles.IsUserInRole("Lead Manager"))
            {
                detail1.Visible = false;
                detail2.Visible = true;
            }
            HtmlGenericControl spnfollowup = (HtmlGenericControl)e.Row.FindControl("spnfollowup");
            DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID("5");

            if (dtFollwUp.Rows.Count > 0)
            {
                try
                {
                    lblFollowUpDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));

                }
                catch { }
                try
                {
                    lblFollowUpNote.Text = dtFollwUp.Rows[0]["Description"].ToString();
                    spnfollowup.Attributes.Add(" data-original-title", dtFollwUp.Rows[0]["Description"].ToString());
                }
                catch
                {
                }
            }
            //SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(hndprojectid.Value);
            //nopanel += Convert.ToInt32(hndNumberPanels.Value);
            //Response.Write(nopanel + "=");
            //lblTotalPanels.Text +=nopanel;
        }

    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeId = st.EmployeeID;
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("Sales Manager"))
        {
            employeeId = "";
        }
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //Response.Write('1');
        //Response.End();
        //DataTable dt = ClstblProjects.tblProjectsGetDataBySearch(employeeId, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, ddlSearchDate.SelectedValue, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue,ddlreadyactive.SelectedValue);
        //DataTable dt = ClstblProjects.tblProjectsGetDataBySearchForExcel2(employeeId, ddlProjectTypeID.SelectedValue.ToString(), selectedItem, txtProjectNumber.Text, txtMQNsearch.Text, txtSearch.Text, ddlSearchCancel.SelectedValue, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue.ToString(), ddlSearchSource.SelectedValue.ToString(), ddlTeam.SelectedValue, txtCompanyNo.Text, txtPModel.Text, txtIModel.Text, txtStartDate.Text, txtEndDate.Text, txtClient.Text, ddlSerchSub.SelectedValue, txtSearchPostCodeFrom.Text, txtSearchPostCodeTo.Text, txtSysCapacityFrom.Text, txtSysCapacityTo.Text, ddlSearchEmployee.SelectedValue, ddlSearchDate.SelectedValue, ddlInstallBookingDate.SelectedValue, ddlPromoOffer1.SelectedValue, ddlPromoOffer2.SelectedValue, ddlFinanceOption.SelectedValue, txtElecDistApprovelRef.Text, txtPurchaseNo.Text, ddlDistApprovedDate.SelectedValue, ddlmeterupgrade.SelectedValue, ddlreadyactive.SelectedValue);        
        Response.Clear();
        //int count=dt.Rows.Count;
        try
        {
            Export oExport = new Export();
            string FileName = "Projects" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 6, 29, 28,
                                 24, 9, 11, 12,
                                13, 14,26, 15,
                                17, 20, 21, 18,
                                19, 27, 32, 25,
                                  33, 34, 23, 35,
                                  36, 39, 37, 38,
                    42,43,44,45,46
                ,47,48,49
                };
            string[] arrHeader = { "Project", "P/CD", "Quote", "Contact",
                                         "Mobile", "Project Notes", "Project#", "Man#",
                                         "System Cap", "System Details", "House", "Install Book Date",
                                         "State", "Status", "Employee Name", "Dep Rec",
                                         "Price", "Roof Type", "Panels", "Inverter",
                                         "Team","FinanceWith", "Source", "Sub Source",
                                         "First Dep. Date", "First Dep. Amount", "NextFollowupDate", "Description",
                    "InstallerName","PickList Created Date","Stock PickUp Date","Stock PickUp Installer","Stock Pickup Note"
                    ,"Stock PickUp Return Date"
                    ,"Stock Return PickUp Installer","Stock Return Pickup Note"
                };
            //only change file extension to .xls for excel file
            //oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        {
            if (hdnID.Value == "2")
            {
                e.Item.Visible = false;
            }
        }

    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        txtSearch.Text = string.Empty;
        ddlTeam.SelectedValue = string.Empty;
        ddlSearchDate.SelectedValue = string.Empty;
        txtClient.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = string.Empty;
        txtSearchPostCodeFrom.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtClient.Text = string.Empty;
        ddltaluka.Text = string.Empty;
        ddlCity.Text = string.Empty;
        ddlElecDist.SelectedValue = string.Empty;
        ddlchnlpart.SelectedValue = "0";

        ddldivision.Items.Clear();

        ddldivision.Items.Add(new ListItem("Select Division", "0"));
        ddlSubDivision.SelectedValue = string.Empty;
        ddlStreetState.SelectedValue = string.Empty;
        ddlDistrict.SelectedValue = string.Empty;
        ddlApplicationStatus.SelectedValue = string.Empty;
        ddlProjectStatus.SelectedValue = string.Empty;
        ddlSearchDate.SelectedValue = "1";
        ddlSearchEmployee.SelectedValue = string.Empty;

        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;


        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        if (gvrow != null)
        {
            Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }


    protected void ddlElecDist_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlElecDist.SelectedValue != string.Empty)
        {
            DataTable dt = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
            ddldivision.Items.Clear();
            ListItem item2 = new ListItem();
            item2.Text = "Select Division";
            item2.Value = "0";
            ddldivision.Items.Add(item2);

            ddldivision.DataSource = dt;
            ddldivision.DataValueField = "Id";
            ddldivision.DataMember = "DivisionName";
            ddldivision.DataTextField = "DivisionName";
            ddldivision.DataBind();

        }
        else
        {

        }
    }


    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    public class FilterData
    {
        public string CustomerID { get; set; }
        public string ProjectId { get; set; }
        public string customername { get; set; }
        public string projectnumber { get; set; }
        public string ConsumerNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Discom { get; set; }
        public string Divison { get; set; }
        public string SubDivision { get; set; }
        public string CurrentStatus { get; set; }
        public string Kw { get; set; }
        public string Address { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string City { get; set; }
        public string state { get; set; }
        public string pannelmodel { get; set; }
        public string nvertormodel { get; set; }
        public string capacity { get; set; }
        public string Employeename { get; set; }
        public string team { get; set; }
        public string FirstQuoteDate { get; set; }
        public string ApplicationStatus { get; set; }
        public string ProjectStatus { get; set; }
        public string BookingDate { get; set; }
        public string Project { get; set; }
        public string inverterDetails { get; set; }
        public string ProjectNotes { get; set; }
        public string InstallerNotes { get; set; }
        public string RooftopArea { get; set; }
        public string stocksize { get; set; }
        public string panelDetail { get; set; }
        public string QuoteSent { get; set; }
        public string QuoteAccepted { get; set; }
        public string DepositReceived { get; set; }
        public string ActiveDate { get; set; }
        public string InstallCompleted { get; set; }
        public string InstallBookingDate { get; set; }
        public string followupdate { get; set; }
        public string CompanyNumber { get; set; }

    }


    protected void ddldivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlElecDist.SelectedValue != string.Empty)
        {
            DataTable dt = ClsProjectSale.tbl_DivisionName_SelectByDISCOM(ddlElecDist.SelectedValue);
            ddldivision.Items.Clear();
            ddldivision.DataSource = dt;
            ddldivision.DataValueField = "Id";
            ddldivision.DataMember = "DivisionName";
            ddldivision.DataTextField = "DivisionName";
            ddldivision.DataBind();
        }
        else
        {

        }
    }
}