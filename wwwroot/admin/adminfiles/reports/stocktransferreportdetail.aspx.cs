﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stocktransferreportdetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropDown();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
             DataTable dtM = ClstblStockTransfers.tblStockTransfers_report(ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, txtStockItem.Text);
             if (dtM.Rows.Count > 0)
             {
                 rptModules.DataSource = dtM;
                 rptModules.DataBind();
             }

        }
    }
    public void BindDropDown()
    {
        DataTable dt2 = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlsearchtransferfrom.DataSource = dt2;
        ddlsearchtransferfrom.DataTextField = "location";
        ddlsearchtransferfrom.DataValueField = "CompanyLocationID";
        ddlsearchtransferfrom.DataBind();

        ddlsearchtransferto.DataSource = dt2;
        ddlsearchtransferto.DataTextField = "location";
        ddlsearchtransferto.DataValueField = "CompanyLocationID";
        ddlsearchtransferto.DataBind();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptModules.DataSource = null;
        rptModules.DataBind();
        DataTable dtM = ClstblStockTransfers.tblStockTransfers_report(ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, txtStockItem.Text);
        if (dtM.Rows.Count > 0)
        {
            rptModules.DataSource = dtM;
            rptModules.DataBind();

            if (ddlshowdata.SelectedValue == "1")
            {


                foreach (RepeaterItem item in rptModules.Items)
                {
                    Repeater rptdet = (Repeater)item.FindControl("rptdet");
                    rptdet.Visible = true;
                }
            }
            if (ddlshowdata.SelectedValue == "2")
            {

                foreach (RepeaterItem item in rptModules.Items)
                {
                    Repeater rptdet = (Repeater)item.FindControl("rptdet");
                    rptdet.Visible = false;
                }
            }
            //for (int i = 0; i < dtM.Rows.Count; i++)
            //{
            //    lblFrom.Text = dtM.Rows[i]["LocationFrom"].ToString();
            //    lblTo.Text = dtM.Rows[i]["LocationTo"].ToString();
            //}
        }
        else
        {
            SetNoRecords();
        }
        //=============================assighn
      
       // lblFrom.Text = ddlsearchtransferfrom.SelectedItem.Text;
       //lblTo.Text = ddlsearchtransferto.SelectedItem.Text;
       // if (txtStartDate.Text != string.Empty)
       // {
       //     lblstartdate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtStartDate.Text));
       // }
       // if (txtEndDate.Text != string.Empty)
       // {
       //     lblenddate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtEndDate.Text));
       // }
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlsearchtransferfrom.SelectedValue = "";
    //    ddlsearchtransferto.SelectedValue = "";
    //    ddlSearchDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;


    //    rptModules.DataSource = null;
    //}
    protected void rptModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptdet = (Repeater)e.Item.FindControl("rptdet");
        HiddenField hdnStockTransferID = (HiddenField)e.Item.FindControl("hdnStockTransferID");
        Label lblFrom = (Label)e.Item.FindControl("lblFrom");
        Label lblTo = (Label)e.Item.FindControl("lblTo");
        Label lblstartdate = (Label)e.Item.FindControl("lblstartdate");
        Label lblenddate = (Label)e.Item.FindControl("lblenddate");

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_GetReportDetails(hdnStockTransferID.Value, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString());
        if (dt.Rows.Count > 0)
        {
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    lblFrom.Text = dt.Rows[i]["LocationFrom"].ToString();
                
            //    lblTo.Text = dt.Rows[i]["LocationTo"].ToString();
            //   // Response.Write( lblTo.Text);
            //}
            rptdet.DataSource = dt;
            rptdet.DataBind();
            
         //   Response.End();


        }
        if (txtStartDate.Text != string.Empty)
        {
            lblstartdate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }
        if (txtEndDate.Text != string.Empty)
        {
            lblenddate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }

    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        dt = ClstblStockTransfers.tblStockTransfers_report(ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, txtStockItem.Text);
        //String content = "<html><body><table><tr><td>your table</td></tr></table></body></html>";
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockTransfer" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = {2,1};
            string[] arrHeader = { "Stock Item", "Transfer Qty" };
            //only change file extension to .xls for excel file

            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlsearchtransferfrom.SelectedValue = "";
        ddlsearchtransferto.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;


        rptModules.DataSource = null;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}