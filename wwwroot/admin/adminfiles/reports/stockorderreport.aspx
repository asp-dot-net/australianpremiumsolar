<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stockorderreport.aspx.cs" Inherits="admin_adminfiles_reports_stockorderreport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid.ClientID) %>').innerHTML;
             var html = '<html><head>' +
              '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
         '</head><body style="background:#ffffff;">' +
         PageHTML +
         '</body></html>';
             var WindowObject = window.open("", "PrintWindow",
         "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
             WindowObject.document.writeln(html);
             WindowObject.document.close();
             WindowObject.focus();
             WindowObject.print();
             WindowObject.close();
             document.getElementById('print_link').style.display = 'block';
         }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Order Report
          <div id="tdExport" class="pull-right printorder" runat="server"></div>
        </h5>
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }
            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panelmessesgarea padbtmzero" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter Responsive-search printorder">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0" class="printorder">
                                            <tr>
                                                <td>
                                                    <div class="input-group col-sm-1">
                                                        <asp:DropDownList ID="ddlitem" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Stock Item</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1">
                                                        <asp:DropDownList ID="ddlcategorysearch" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Category</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-1">
                                                        <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Vendor</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-1">
                                                        <asp:DropDownList ID="ddlShow" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="2">Show</asp:ListItem>
                                                            <asp:ListItem Value="0">Not Received</asp:ListItem>
                                                            <asp:ListItem Value="1">Received</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1">
                                                        <asp:DropDownList ID="ddlDate" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Date</asp:ListItem>
                                                            <asp:ListItem Value="1">DateOrdered</asp:ListItem>
                                                            <asp:ListItem Value="2">BOLReceived</asp:ListItem>
                                                            <asp:ListItem Value="3">ActualDelivery</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                            <%-- <div class="showdatabox printorder">
                       	<div class="row">
                        <div class="dataTables_length showdata col-sm-2" id="show" runat="server" visible="false">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server"
                                                                aria-controls="DataTables_Table_0" class="form-control input-sm">
                                                            </asp:DropDownList>
                                                        entries
                                                    </td>
                                                </tr>
                                            </table>
                            </div>--%>
                            <div align="right">
                                <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                            </div>
                            <%--  </div>
                        	
                     
                       </div>--%>
                        </div>
                    </div>

                    <div class="panel-body animate-panel">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hpanel marbtmzero">
                                    <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                                    <div class="panel-body ">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="finalgrid">
                                                <div id="dvprojectstate" runat="server" visible="false" class="printarea">
                                                    <dl id="dlprojectstate">
                                                        <dt>
                                                            <a href="#">
                                                                <span class="hida" id="spanselectstate">State</span>
                                                                <p id="pselectstate"></p>
                                                            </a>
                                                        </dt>
                                                        <dd id="ddprojectstate" runat="server">
                                                            <div id="dvmutliSelectstate">
                                                                <ul>
                                                                    <asp:Repeater ID="rptState" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("State") %>' />
                                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                                                <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="chkval">
                                                                                    <asp:Literal runat="server" ID="ltteam" Text='<%# Eval("State")%>'></asp:Literal></label>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ul>
                                                            </div>
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="messesgarea printpage">
                                                <div class="alert alert-success" id="PanSuccess" runat="server">
                                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                                </div>
                                                <div class="alert alert-danger" id="PanError" runat="server">
                                                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                                                </div>
                                                <div class="alert alert-info" id="PanAlreadExists" runat="server">
                                                    <i class="icon-info-sign"></i><strong>&nbsp;Stock Order already exists.</strong>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--    <div class="contactbottomarea">
                            <div class="marginnone">
                                <div class="tableblack">
                                    <div class="tooltipareanewshow">
                                        <div id="PanGrid" runat="server" class="table-responsive noPagination" visible="false">
                                            <div class="table-responsive datahidden noPagination" id="Div6" runat="server">--%>
                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                        <div class="xscroll">
                            <div id="PanGrid" runat="server">
                                <div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <tr>
                                                <td></td>
                                                <asp:Repeater runat="server" ID="rptlocations">
                                                    <ItemTemplate>
                                                        <td class="center-text">

                                                            <%# Eval("State") %>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                            <asp:Repeater runat="server" ID="rpt" OnItemDataBound="rpt_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%# Eval("StockItem") %>
                                                            <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' />
                                                            <%--<asp:HiddenField runat="server" ID="hdnStockOrderItemID" Value='<%# Eval("StockOrderItemID") %>' />--%>
                                                            <%-- <asp:HiddenField runat="server" ID="hdnState" Value='<%# Eval("state") %>' />--%>
                                                            <%--<asp:HiddenField runat="server" ID="hdnQty" Value='<%# Eval("Qty") %>' />--%>
                                                        </td>
                                                        <asp:Repeater runat="server" ID="rptqty" OnItemDataBound="rptqty_ItemDataBound">
                                                            <ItemTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdnState" Value='<%# Eval("state") %>' />
                                                                <td class="center-text">
                                                                    <%--  <%# Eval("Qty") %>--%>
                                                                    <asp:Literal runat="server" ID="ltstate"></asp:Literal>
                                                                </td>

                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <%-- <asp:Repeater runat="server" ID="rpt"  OnItemDataBound="rpt_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%# Eval("StockOrderItem") %>
                                                                    <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnState" Value='<%# Eval("State") %>' />
                                                                </td>
                                                                <asp:Repeater runat="server" ID="rptLoc" OnItemDataBound="rptLoc_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <td>
                                                                            <asp:Literal runat="server" ID="ltQty" Text='<%# Eval("Qty") %>'></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>--%>


                                            <%--<asp:Repeater runat="server" ID="rpt" OnItemDataBound="rpt_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%# Eval("StockOrderItem") %> <%# Eval("StockItemID") %>
                                                                    <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' />
                                                                    <asp:Literal runat="server" ID="ltStockItem"></asp:Literal>
                                                                </td>
                                                                <asp:Repeater runat="server" ID="rptLoc">
                                                                    <ItemTemplate>
                                                                        <td>
                                                                            <%# Eval("Qty") %>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>--%>
                                        </table>
                                    </div>
                                    <%--   <b>
                                    <asp:Literal ID="ltrPage" runat="server"></asp:Literal></b>--%>
                                </div>
                            </div>
                        </div>


                    </asp:Panel>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
