<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="stockusage.aspx.cs" Inherits="admin_adminfiles_reports_stockusage" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Usage Report
          <div id="tdExport" class="pull-right printorder" runat="server">

              <%--  <asp:LinkButton ID="lbtnExport" CssClass="excelicon" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                        CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                    <a href="javascript:window.print();" class="printicon">
                       
                        <i class="fa fa-print"></i>
                    </a>--%>
          </div>
                </h5>

            </div>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {

                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                    $("select").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                }


            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
                    <div class="animate-panelmessesgarea padbtmzero printorder">
                        <div class="messesgarea">

                            <%-- <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
                        </div>
                    </div>
                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter Responsive-search printorder">
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                        <table border="0" class="printorder" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <%-- <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="ddlLocation" runat="server"  CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStockItem"
                                                                WatermarkText="StockItem" />
                                                        </div>--%>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockusage">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlStockCategory" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockusage">
                                                            <asp:ListItem Value="">Panels/Inv</asp:ListItem>
                                                            <asp:ListItem Value="2">Modules</asp:ListItem>
                                                            <asp:ListItem Value="3">Inverters</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockusage">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                            ControlToValidate="txtStartDate" ErrorMessage=""></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                            ControlToValidate="txtEndDate" ErrorMessage=""></asp:RequiredFieldValidator>
                                                        <%--<asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>--%>
                                                    </div>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlshowdata" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalstockusage">
                                                            <asp:ListItem Value="1">Show Details</asp:ListItem>
                                                            <asp:ListItem Value="2">Hide Details</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon redreq" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div align="right">
                                <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                            </div>


                            <%--  </ol>--%>
                        </div>
                    </div>



                </asp:Panel>
                <div class="finalgrid">
                    <div class="contactbottomarea">

                        <div class="row" id="PanAddUpdate" runat="server">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="stocktransfer">
                                                    <h1>Stock Usage</h1>
                                                </div>
                                                <div class="qty">
                                                    <div class="stockusage" style="min-width: 600px!important;">
                                                        <table style="width: 100%" border="0" cellspacing="0" cellpadding="0" class="table table-border table-striped transferdetail">
                                                            <tr>
                                                                <th align="left">Location: </th>
                                                                <td align="left">
                                                                    <asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                                                <th align="left">&nbsp;&nbsp;&nbsp;&nbsp;Panels/Inv:</th>
                                                                <td align="left">
                                                                    <asp:Label ID="lblddlStockCategory" runat="server"></asp:Label></td>
                                                                <th align="left">&nbsp;&nbsp;&nbsp;&nbsp;Installer:</th>
                                                                <td align="left">
                                                                    <asp:Label ID="lblddlInstaller" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <th align="left">Start Date: </th>
                                                                <td align="left">
                                                                    <asp:Label ID="lblstartdate" runat="server"></asp:Label></td>
                                                                <th align="left">&nbsp;&nbsp;&nbsp;&nbsp;End Date:</th>
                                                                <td align="left">
                                                                    <asp:Label ID="lblenddate" runat="server"></asp:Label></td>
                                                                <th>&nbsp;</th>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>

                                                <table class="stockitem" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                    </tr>
                                                    <tr>
                                                        <th width="60%" align="left">StockItem</th>
                                                        <th width="10%" align="center" id="panel" runat="server">Panel/Inv</th>
                                                        <th width="10%" align="center" id="Stockfrom" runat="server">StockFrom</th>
                                                        <th width="10%" align="center">Allocation</th>
                                                        <th width="10%" align="center" id="Instock" runat="server">In Stock</th>
                                                    </tr>
                                                    <asp:Repeater ID="rptModules" runat="server" OnItemDataBound="rptModules_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td width="60%" align="left"><%# Eval("StockItem") %>
                                                                    <br />
                                                                    <table>
                                                                        <tr>
                                                                            <td width="8%" align="left"></td>
                                                                            <td width="30%" align="left">
                                                                                <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockItemID") %>' />
                                                                                <asp:Repeater runat="server" ID="rptdet">
                                                                                    <ItemTemplate>
                                                                                        <%# Eval("ProjectNumber") %> - 
                                                                              <%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}") %>  = &nbsp;
                                                                             <%#Eval("Stock","{0:0}")%>
                                                                                        <%--<%# DataBinder.Eval(Container.DataItem, "TransferDate", "{0:dd MMM yyyy}") %>  - 
                                                                              <%# Eval("TransferQty") %>--%>
                                                                                    </ItemTemplate>
                                                                                    <SeparatorTemplate>
                                                                                        <br />
                                                                                    </SeparatorTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td width="7%" align="left" id="rptpanel" runat="server"><%# Eval("StockCategory") %></td>
                                                                <td width="7%" align="left" id="rptStockFrom" runat="server"><%# Eval("StockFrom") %></td>
                                                                <td width="7%" align="left"><%#Eval("StockSold","{0:0}")%></td>
                                                                <td width="7%" align="left" id="rptInStock" runat="server"><%# Eval("StockQuantity") %></td>

                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

