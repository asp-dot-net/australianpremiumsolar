﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_panelstock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState.DataBind();

            rptState2.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState2.DataBind();

            rptStockItem.DataSource = Reports.tblStockItems_PanelStock();
            rptStockItem.DataBind();


        }
    }
    protected void rptStockItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptHidState = (Repeater)e.Item.FindControl("rptHidState");
            rptHidState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptHidState.DataBind();
        }
    }
    protected void rptHidState_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndStockItem = ((HiddenField)((Repeater)sender).Parent.FindControl("hndStockItem"));
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptCount = (Repeater)e.Item.FindControl("rptCount");

            rptCount.DataSource = Reports.tblStockItems_Count_Stock(hndStockItem.Value, hndState.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptCount.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptStockItem.DataSource = Reports.tblStockItems_PanelStock();
        rptStockItem.DataBind();
    }
    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        rptStockItem.DataSource = Reports.tblStockItems_PanelStock();
        rptStockItem.DataBind();
    }
}