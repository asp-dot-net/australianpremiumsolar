//using FormbayClientApi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_formbaydocsreport : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();



            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindGrid(0);
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
        }
    }
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();
        try
        {
            dt = ClstblProjects.tblProjects_Searchby_FormbayDocs(txtProjectNumber.Text, txtcompany.Text, txtformbayid.Text, txtStartDate.Text, txtEndDate.Text, ddlInstaller.SelectedValue);
        }
        catch { }
        int iTotalRecords = dt.Rows.Count;
        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
       // ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                     divnopage.Visible = false;
                }
                else
                {
                     divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    ddlInstaller.SelectedValue = "";
    //    txtProjectNumber.Text = string.Empty;
    //    txtcompany.Text = string.Empty;
    //    txtformbayid.Text = string.Empty;
    //    BindGrid(0);
    //}

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
        string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
        string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
        string username = ConfigurationManager.AppSettings["username"].ToString();
        string password = ConfigurationManager.AppSettings["password"].ToString();
        //FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);
        #region rec
        if (e.CommandName == "lnkrec")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
           // var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=1";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Rec";
        }
        #endregion
        #region invoice
        if (e.CommandName == "lnkinvoice")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=2";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Invoice";
        }
        #endregion
        #region ccp
        if (e.CommandName == "lnkccp")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            //dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=3";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "CCP";
        }
        #endregion
        #region design
        if (e.CommandName == "lnkdesign")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=4";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Design";
        }
        #endregion
        #region photos
        if (e.CommandName == "lnkphoto")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=5";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Photos";
        }
        #endregion
        #region instinv
        if (e.CommandName == "lnkinstinv")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=8";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Instinv";
        }
        #endregion
        #region invstcs
        if (e.CommandName == "lnkinvstcs")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = item["URL"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvphotos = dt.DefaultView;
            dvphotos.RowFilter = "doctype=9";
            rpt.DataSource = dvphotos;
            rpt.DataBind();

            lbltitle.Text = "Invstcs";
        }
        #endregion

        //other
        #region lnkcerti
        if (e.CommandName == "lnkcerti")
        {

            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            dt.Columns.Add("name");

            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = "https://eurosolar.formbay.com.au/docs/load_assignment_page?href=" + item["URL"].ToString().Replace("https://api.formbay.com.au", "").Replace("\"", "");
            //    dr["name"] = item["name"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvcerti = dt.DefaultView;
            dvcerti.RowFilter = "doctype=100 and name='Certificate of testing'";
            if (dvcerti.ToTable().Rows.Count > 0)
            {
                rpt.DataSource = dvcerti;
                rpt.DataBind();

                ModalPopupExtender1.Show();
                div_popup.Visible = true;

            }

            lbltitle.Text = "Certificate of testing";
        }
        #endregion
        #region lnkackno
        if (e.CommandName == "lnkackno")
        {

            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            dt.Columns.Add("name");

            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = "https://eurosolar.formbay.com.au/docs/load_assignment_page?href=" + item["URL"].ToString().Replace("https://api.formbay.com.au", "").Replace("\"", "");
            //    dr["name"] = item["name"];
            //    dt.Rows.Add(dr);

            //}
            DataView dvackno = dt.DefaultView;
            dvackno.RowFilter = "doctype=100 and name='Customer Acknowledgement'";
            if (dvackno.ToTable().Rows.Count > 0)
            {
                rpt.DataSource = dvackno;
                rpt.DataBind();

                ModalPopupExtender1.Show();
                div_popup.Visible = true;
            }

            lbltitle.Text = "Customer Acknowledgement";
        }
        #endregion
        #region lnkfeedback
        if (e.CommandName == "lnkfeedback")
        {

            string formbayid = e.CommandArgument.ToString();//formbayid
            //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
            DataTable dt = new DataTable();
            dt.Columns.Add("doctype");
            dt.Columns.Add("URL");
            dt.Columns.Add("name");

            //foreach (var item in jobDoc)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["doctype"] = item["doctype"];
            //    dr["URL"] = "https://eurosolar.formbay.com.au/docs/load_assignment_page?href=" + item["URL"].ToString().Replace("https://api.formbay.com.au", "").Replace("\"", "");
            //    dr["name"] = item["name"];
            //    dt.Rows.Add(dr);
            //}
            DataView dvfeedback = dt.DefaultView;
            dvfeedback.RowFilter = "doctype=100 and name='Customer Feedback'";
            if (dvfeedback.ToTable().Rows.Count > 0)
            {
                rpt.DataSource = dvfeedback;
                rpt.DataBind();

                ModalPopupExtender1.Show();
                div_popup.Visible = true;
            }

            lbltitle.Text = "Certificate of testing";
        }
        #endregion
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjects_SelectFormbayDocsReport();

        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "SalesReprot" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 3, 4, 9, 5, 6, 10, 8, 12, 11 };
            string[] arrHeader = { "ProjectNumber", "Project", "Customer", "Panels", "QuotePrice", "Dep Rec", "Status", "Opened", "SalesRep" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndformbay = (HiddenField)e.Row.FindControl("hndformbay");
            string formbayid = hndformbay.Value;
            LinkButton lnkrec = (LinkButton)e.Row.FindControl("lnkrec");
            LinkButton lnkinvoice = (LinkButton)e.Row.FindControl("lnkinvoice");
            LinkButton lnkccp = (LinkButton)e.Row.FindControl("lnkccp");
            LinkButton lnkdesgin = (LinkButton)e.Row.FindControl("lnkdesgin");
            LinkButton lnkphoto = (LinkButton)e.Row.FindControl("lnkphoto");
            LinkButton lnkinstinv = (LinkButton)e.Row.FindControl("lnkinstinv");
            LinkButton lnkinvstcs = (LinkButton)e.Row.FindControl("lnkinvstcs");

            LinkButton lnkcerti = (LinkButton)e.Row.FindControl("lnkcerti");
            LinkButton lnkackno = (LinkButton)e.Row.FindControl("lnkackno");
            LinkButton lnkfeedback = (LinkButton)e.Row.FindControl("lnkfeedback");

            Label lblformstatus = (Label)e.Row.FindControl("lblformstatus");


            string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
            string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
            string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
            string username = ConfigurationManager.AppSettings["username"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();
            //try
            //{
            //FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);
           // var jobForms = c.GetJobForms();
            //string[] formsdocs = jobForms.ToObject<string[]>();

            bool checkjob = false;
            //int pos = Array.IndexOf(formsdocs, formbayid);
            //if (pos >= 0)
            //{
            //    checkjob = true;
            //}

            if (checkjob)
            {

                //try
                {
                  //  var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
                  //  Job job = new Job();
                   // job = c.GetJob(Convert.ToInt32(formbayid));
                   // lblformstatus.Text = job.status;

                    DataTable dt = new DataTable();
                    dt.Columns.Add("doctype");
                    dt.Columns.Add("URL");
                    dt.Columns.Add("name");
                    //foreach (var item in jobDoc)
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["doctype"] = item["doctype"];
                    //    dr["URL"] = item["URL"];
                    //    if (item["doctype"].ToString() == "100")
                    //    {
                    //        dr["name"] = item["name"];
                    //    }
                    //    else
                    //    {
                    //        dr["name"] = "";
                    //    }
                    //    dt.Rows.Add(dr);
                    //}
                    if (dt.Rows.Count > 0)
                    {
                        DataView dv = dt.DefaultView;
                        dv.RowFilter = "doctype=1";
                        if (dv.ToTable().Rows.Count > 0)
                        {
                            lnkrec.Visible = true;
                        }
                        else
                        {
                            lnkrec.Visible = false;
                        }

                        DataView dvinvoice = dt.DefaultView;
                        dvinvoice.RowFilter = "doctype=2";
                        if (dvinvoice.ToTable().Rows.Count > 0)
                        {
                            lnkinvoice.Visible = true;
                        }
                        else
                        {
                            lnkinvoice.Visible = false;
                        }

                        DataView dvccp = dt.DefaultView;
                        dvccp.RowFilter = "doctype=3";
                        if (dvccp.ToTable().Rows.Count > 0)
                        {
                            lnkccp.Visible = true;
                        }
                        else
                        {
                            lnkccp.Visible = false;
                        }

                        DataView dvdesgin = dt.DefaultView;
                        dvdesgin.RowFilter = "doctype=4";
                        if (dvdesgin.ToTable().Rows.Count > 0)
                        {
                            lnkdesgin.Visible = true;
                        }
                        else
                        {
                            lnkdesgin.Visible = false;
                        }

                        DataView dvphotos = dt.DefaultView;
                        dvphotos.RowFilter = "doctype=5";
                        if (dvphotos.ToTable().Rows.Count > 0)
                        {
                            lnkphoto.Visible = true;
                        }
                        else
                        {
                            lnkphoto.Visible = false;
                        }

                        DataView dvinstinv = dt.DefaultView;
                        dvinstinv.RowFilter = "doctype=8";
                        if (dvinstinv.ToTable().Rows.Count > 0)
                        {
                            lnkinstinv.Visible = true;
                        }
                        else
                        {
                            lnkinstinv.Visible = false;
                        }

                        DataView dvinvstcs = dt.DefaultView;
                        dvinvstcs.RowFilter = "doctype=9";
                        if (dvinvstcs.ToTable().Rows.Count > 0)
                        {
                            lnkinvstcs.Visible = true;
                        }
                        else
                        {
                            lnkinvstcs.Visible = false;
                        }

                        //other
                        DataView dvcerti = dt.DefaultView;

                        dvcerti.RowFilter = "doctype=100 and name='Certificate of testing'";

                        if (dvcerti.ToTable().Rows.Count > 0)
                        {
                            lnkcerti.Visible = true;
                        }
                        else
                        {
                            lnkcerti.Visible = false;
                        }


                        DataView dvackno = dt.DefaultView;
                        dvackno.RowFilter = "doctype=100 and name='Customer Acknowledgement'";

                        if (dvackno.ToTable().Rows.Count > 0)
                        {

                            lnkackno.Visible = true;

                        }
                        else
                        {
                            lnkackno.Visible = false;
                        }
                        DataView dvfeedback = dt.DefaultView;
                        dvfeedback.RowFilter = "doctype=100 and name='Customer Feedback'";
                        if (dvfeedback.ToTable().Rows.Count > 0)
                        {
                            lnkfeedback.Visible = true;
                        }
                        else
                        {
                            lnkfeedback.Visible = false;
                        }
                    }
                }
                //catch
                {
                }
            }

            //}
            //catch
            //{
            //    //Response.Write(formbayid+"<br/>");
            //    //Response.End();

            //}
        }
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
        //BindScript();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }

    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        txtProjectNumber.Text = string.Empty;
        txtcompany.Text = string.Empty;
        txtformbayid.Text = string.Empty;
        BindGrid(0);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


}