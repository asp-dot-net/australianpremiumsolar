<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="formbaydocsreport.aspx.cs" Inherits="admin_adminfiles_reports_formbaydocsreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Formbay Docs Report</h5>
              <div id="tdExport" class="pull-right" runat="server">
                            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                                <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                            </ol>
                        </div>
               </div>

    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    $('.loading-container').css('display', 'none');
                }
                function pageLoaded() {

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.loading-container').css('display', 'none');
                    // CompanyAddress();
                    // gridviewScroll();
                    //alert($(".search-select").attr("class"));
                    $(".myvalformbay").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                     $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }



                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                   

                   
                }
            </script>
             <div class="page-body padtopzero"> 
            <asp:Panel runat="server" ID="PanGridSearch">
                <div class="animate-panel" style="padding-bottom: 0px!important;">
                    <div class="messesgarea">

                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                </div>
                <div class="searchfinal">
                     <div class="widget-body shadownone brdrgray"> 
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                              <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                        <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                            <div class="inlineblock martop5">
                                                                            <div class="">
                                                                    <div class="input-group" id="divCustomer" runat="server">
                                                                        <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myvalformbay" Width="150px">
                                                                            <asp:ListItem Value="" Text="State"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group col-sm-2">
                                                                        <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                                            WatermarkText="Project Number" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="input-group col-sm-2">
                                                                        <asp:TextBox ID="txtformbayid" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtformbayid"
                                                                            WatermarkText="FormbayID" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtformbayid" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="input-group col-sm-2">
                                                                        <asp:TextBox ID="txtcompany" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtcompany"
                                                                            WatermarkText="Company Name" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtcompany" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    </div>

                                                                    <div class="input-group" id="div3" runat="server">
                                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myvalformbay" Width="150px">
                                                                            <asp:ListItem Value="" Text="Installer"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                        <%--                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                    </div>
                                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                     <div class="inlineblock martop5">
                                                                            <div class="">
                                                                    <div class="input-group">
                                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                                </div></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                            </asp:Panel>
                       </div>
                       </div>
                       <div class="datashowbox martop5">                           
                            <div class="dataTables_length showdata">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="padtopzero"><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div> 
                        </div>
                       </div>
                       </div>
                <div class="panel-body animate-panel padtopzero">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel marbtmzero">
                                <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                                
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                        CancelControlID="ibtnCancelStatus" DropShadow="true" PopupControlID="div_popup" TargetControlID="btnNULLData1">
                    </cc1:ModalPopupExtender>
                    <div runat="server" id="div_popup" class="modal_popup" align="center" style="width: 900px; overflow-x: scroll;">
                        <div class="popupdiv">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading pad15">
                                            <asp:Label runat="server" ID="lbltitle"></asp:Label>
                                            <div class="panel-tools">
                                                <div class="closbtn">
                                                    <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                                        <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group dateimgarea">
                                                        <asp:Repeater runat="server" ID="rpt">
                                                            <ItemTemplate>
                                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("URL") %>' />
                                                            </ItemTemplate>
                                                            <SeparatorTemplate>
                                                                <br />
                                                                <br />
                                                            </SeparatorTemplate>
                                                        </asp:Repeater>
                                                        <div class="clear">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="content padtopzero">
                            <div class="">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="Row">
                                            <div id="PanGrid" runat="server">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                    OnRowDataBound="GridView1_RowDataBound"    OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="FormbayID" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="hndformbay" Value='<%#Eval("FormbayID")%>' />
                                                                    <%#Eval("FormbayID")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                                                ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <%#Eval("Customername")%> - <%#Eval("ProjectNumber")%><br />
                                                                    <%#Eval("CustAddress")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Installation Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <%#Eval("installation_status")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblformstatus"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="rec" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkrec" CommandName="lnkrec" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="invoice" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkinvoice" CommandName="lnkinvoice" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ccp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkccp" CommandName="lnkccp" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="design" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkdesgin" CommandName="lnkdesgin" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="photos" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkphoto" CommandName="lnkphoto" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="instinv" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkinstinv" CommandName="lnkinstinv" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="invstcs" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkinvstcs" CommandName="lnkinvstcs" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CFT" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkcerti" CommandName="lnkcerti" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CA" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkackno" CommandName="lnkackno" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CF" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkfeedback" CommandName="lnkfeedback" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false">
                                            View
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <AlternatingRowStyle />

                                                        <PagerTemplate>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            <div class="pagination">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                            </div>
                                                        </PagerTemplate>
                                                        <PagerStyle CssClass="paginationGrid" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                    </asp:GridView>
                                                </div>
                                                <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </div>
                                                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

