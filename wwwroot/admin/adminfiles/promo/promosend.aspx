<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="promosend.aspx.cs" Inherits="admin_adminfiles_promo_promosend" UICulture="en-GB" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            doMyAction();


        });
        function doMyAction() {

            $('#dvmutliSelectproject input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();

            });
            $('#dvmutliSelectteam input[type="checkbox"]').on('click', function () {
                callMultiCheckboxTeam();
            });
            $('#dvmutliSelectstate input[type="checkbox"]').on('click', function () {
                callMultiCheckboxState();
            });
            $('#dvmutliSelectlead input[type="checkbox"]').on('click', function () {
                callMultiCheckboxLead();
            });


            $(".myvalpromo").select2();
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });

            HighlightControlToValidate();
            $('#<%= btnSearch.ClientID %>').click(function () {
                formValidate();
            });



        }
        function callMultiCheckbox() {

            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('#pselectproject').show();
                $('#pselectproject').html(html);
                $("#spanselectproject").hide();
            }
            else {
                $('#spanselectproject').show();
                $('#pselectproject').hide();
            }
        }

        function callMultiCheckboxTeam() {
            var title = "";
            $("#<%=ddprojectteam.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('#pselectteam').show();
                $('#pselectteam').html(html);
                $("#spanselectteam").hide();
            }
            else {
                $('#spanselectteam').show();
                $('#pselectteam').hide();
            }
        }
        function callMultiCheckboxState() {
            var title = "";
            $("#<%=ddprojectstate.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('#pselectstate').show();
                $('#pselectstate').html(html);
                $("#spanselectstate").hide();
            }
            else {
                $('#spanselectstate').show();
                $('#pselectstate').hide();
            }
        }
        function callMultiCheckboxLead() {
            var title = "";
            $("#<%=ddprojectlead.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('#pselectlead').show();
                $('#pselectlead').html(html);
                $("#spanselectlead").hide();
            }
            else {
                $('#spanselectlead').show();
                $('#pselectlead').hide();
            }
        }
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                        $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                    }
                }
            }
        }


    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.

                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });


                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress

                    $('.loading-container').css('display', 'none');



                    $("#dlprojectstatus dt a").on('click', function () {
                        $("#dlprojectstatus dd ul").slideToggle('fast');
                    });

                    $("#dlprojectstatus dd ul li a").on('click', function () {
                        $("#dlprojectstatus dd ul").hide();
                    });

                    $("#dlprojectteam dt a").on('click', function () {
                        $("#dlprojectteam dd ul").slideToggle('fast');
                    });

                    $("#dlprojectteam dd ul li a").on('click', function () {
                        $("#dlprojectteam dd ul").hide();
                    });

                    $("#dlprojectstate dt a").on('click', function () {
                        $("#dlprojectstate dd ul").slideToggle('fast');
                    });

                    $("#dlprojectstate dd ul li a").on('click', function () {
                        $("#dlprojectstate dd ul").hide();
                    });

                    $("#dlprojectlead dt a").on('click', function () {
                        $("#dlprojectlead dd ul").slideToggle('fast');
                    });

                    $("#dlprojectlead dd ul li a").on('click', function () {
                        $("#dlprojectlead dd ul").hide();
                    });


                    $(document).bind('click', function (e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });



                    //callMultiCheckbox();
                }

                function pageLoaded() {

                    $('.loading-container').css('display', 'none');

                    $(".myvalpromo").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }

                    callMultiCheckbox();
                    callMultiCheckboxTeam();
                    callMultiCheckboxState();
                    callMultiCheckboxLead();
                }
            </script>

            <div class="finalheader">
                <div class="small-header transition animated fadeIn">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div id="tdExport" class="pull-right" runat="server">
                                <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                <%--    <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                                <%-- </ol>--%>
                            </div>
                            <h2 class="font-light m-b-xs">Promo
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel runat="server" ID="PanGridSearch">
                <div class="">
                    <div class="panel-body animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                </div>

                <div class="searchfinal">
                    <div class="panel-body padbtmzero">
                        <div class="">
                            <div class="">
                                <div class="hpanel marbtmzero">
                                    <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                                    <div class="panel-body marbtmzero">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="">
                                                <div class="dataTables_filter ">

                                                    <div class="dataTables_filter Responsive-search">
                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px!important;" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <div class="inlineblock">
                                                                        <div class="">

                                                                            <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                                                <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalpromo" Width="150px">
                                                                                    <asp:ListItem Value="">Type</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="input-group col-sm-1" id="div1" runat="server">
                                                                                <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalpromo">
                                                                                    <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="input-group col-sm-2" id="div2" runat="server">
                                                                                <asp:DropDownList ID="ddlSearchSource" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalpromo" AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="">Source</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group col-sm-2" id="div3" runat="server">
                                                                                <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalpromo">
                                                                                    <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="input-group spical multiselect" id="dvprojectstatus">
                                                                                <dl class="dropdown" id="dlprojectstatus">
                                                                                    <dt>
                                                                                        <a href="#">
                                                                                            <span class="hida" id="spanselectproject">Project Status</span>
                                                                                            <p class="multiSel" id="pselectproject"></p>
                                                                                        </a>
                                                                                    </dt>
                                                                                    <dd id="ddproject" runat="server">
                                                                                        <div class="mutliSelect" id="dvmutliSelectproject">
                                                                                            <ul>
                                                                                                <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                                    <ItemTemplate>
                                                                                                        <li>
                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                            <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                                                <span></span>
                                                                                                            </label>
                                                                                                            <label class="chkval">
                                                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal></label>
                                                                                                        </li>
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </dd>
                                                                                </dl>
                                                                            </div>

                                                                            <div class="input-group col-sm-2" id="div4" runat="server">
                                                                                <asp:DropDownList ID="ddlsearchsalerap" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myvalpromo">
                                                                                    <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="inlineblock martop5">
                                                                        <div class="">
                                                                            <div class="input-group spical multiselect" id="dvprojectteam">
                                                                                <dl class="dropdown" id="dlprojectteam">
                                                                                    <dt>
                                                                                        <a href="#">
                                                                                            <span class="hida" id="spanselectteam">Team</span>
                                                                                            <p class="multiSel" id="pselectteam"></p>
                                                                                        </a>
                                                                                    </dt>
                                                                                    <dd id="ddprojectteam" runat="server">
                                                                                        <div class="mutliSelect" id="dvmutliSelectteam">
                                                                                            <ul>
                                                                                                <asp:Repeater ID="rptTeam" runat="server">
                                                                                                    <ItemTemplate>
                                                                                                        <li>
                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("SalesTeamID") %>' />
                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                            <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                                                <span></span>
                                                                                                            </label>
                                                                                                            <label class="chkval">
                                                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("SalesTeam")%>'></asp:Literal></label>
                                                                                                        </li>
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater>


                                                                                            </ul>
                                                                                        </div>
                                                                                    </dd>
                                                                                </dl>
                                                                            </div>
                                                                            <div class="input-group col-sm-2">
                                                                                <asp:TextBox ID="txtSearchFirst" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSearchFirst"
                                                                                    WatermarkText="Contact First" />
                                                                            </div>

                                                                            <div class="input-group col-sm-2">
                                                                                <asp:TextBox ID="txtSearchLast" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtSearchLast"
                                                                                    WatermarkText="Contact Last" />
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchMobile" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtSearchMobile"
                                                                                    WatermarkText="Mobile" />
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchPhone" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtSearchPhone" WatermarkText="Phone" />
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtSearchStreet"
                                                                                    WatermarkText="Street" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorStreet" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="txtSearchStreet" Display="Dynamic" Visible="false" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtSerachCity"
                                                                                    WatermarkText="City" />
                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                                    UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                                </cc1:AutoCompleteExtender>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCity" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                    ControlToValidate="txtSerachCity" Display="Dynamic" Visible="false" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                                            </div>

                                                                         

                                                                        </div>
                                                                    </div>

                                                                    <div class="inlineblock martop5">
                                                                        <div class="">
                                                                               <div class="input-group spical multiselect col-sm-1" id="dvprojectstate">
                                                                                <dl class="dropdown" id="dlprojectstate">
                                                                                    <dt>
                                                                                        <a href="#">
                                                                                            <span class="hida" id="spanselectstate">State</span>
                                                                                            <p class="multiSel" id="pselectstate"></p>
                                                                                        </a>
                                                                                    </dt>
                                                                                    <dd id="ddprojectstate" runat="server">
                                                                                        <div class="mutliSelect" id="dvmutliSelectstate">
                                                                                            <ul>
                                                                                                <asp:Repeater ID="rptState" runat="server">
                                                                                                    <ItemTemplate>
                                                                                                        <li>
                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("State") %>' />
                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                            <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                                                <span></span>
                                                                                                            </label>
                                                                                                            <label class="chkval">
                                                                                                                <asp:Literal runat="server" ID="ltteam" Text='<%# Eval("State")%>'></asp:Literal></label>
                                                                                                        </li>
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </dd>
                                                                                </dl>
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchPostCode"
                                                                                    WatermarkText="Post Code" />
                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                                    UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCode" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                                    ControlToValidate="txtSearchPostCode" Display="Dynamic" Visible="false" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                                            </div>
                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchClient"
                                                                                    WatermarkText="Company" />
                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server" UseContextKey="true" TargetControlID="txtSearchClient" ServicePath="~/Search.asmx"
                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList" EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                            </div>
                                                                            <div class="input-group col-sm-2">
                                                                                <asp:TextBox ID="txtSearchCompanyNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                                    WatermarkText="Company Number" />
                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                    UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                                    ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                            </div>

                                                                            <div class="input-group col-sm-1">
                                                                                <asp:TextBox ID="txtSearchEmail" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchEmail"
                                                                                    WatermarkText="Email" />
                                                                            </div>

                                                                            <div class="form-group spical multiselect" id="dvprojectlead">
                                                                                <dl class="dropdown" id="dlprojectlead">
                                                                                    <dt>
                                                                                        <a href="#">
                                                                                            <span class="hida" id="spanselectlead">Lead Status</span>
                                                                                            <p class="multiSel" id="pselectlead"></p>
                                                                                        </a>
                                                                                    </dt>
                                                                                    <dd id="ddprojectlead" runat="server">
                                                                                        <div class="mutliSelect" id="dvmutliSelectlead">
                                                                                            <ul>
                                                                                                <asp:Repeater ID="rptLeadStatus" runat="server">
                                                                                                    <ItemTemplate>
                                                                                                        <li>
                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ContLeadStatusID") %>' />
                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                            <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                                                                <span></span>
                                                                                                            </label>
                                                                                                            <label class="chkval">
                                                                                                                <asp:Literal runat="server" ID="ltleadstatus" Text='<%# Eval("ContLeadStatus")%>'></asp:Literal></label>
                                                                                                        </li>
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </dd>
                                                                                </dl>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="groupalign">
                                                                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text="Contact Entered" Enabled="false"></asp:TextBox>
                                                                                </div>
                                                                                <%--      <b>
                                                                                <asp:Label ID="LabelDate" runat="server">Contact Entered</asp:Label></b>--%>
                                                                            </div>
                                                                            

                                                                        </div>
                                                                    </div>
                                                                    <div class="inlineblock martop5">
                                                                        <div class="">
                                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtStartDate" placeholder="From" runat="server" class="form-control"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                            </div>

                                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtEndDate" placeholder="To" runat="server" class="form-control"></asp:TextBox>
                                                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                            </div>
                                                                            <div class="input-group">
                                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="dataTables_length showdata col-sm-6" id="show" runat="server">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myvalpromo">
                                                            </asp:DropDownList>
                                                                entries
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="Div5" class="pull-right" runat="server">
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                            CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                                                        <%-- </ol>--%>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body">
                <div class="finalgrid">
                    <div>
                        <div id="PanGrid" runat="server">
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" OnRowDataBound="GridView1_RowDataBound1"
                                    AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <center>
                                            <span class="euroradio">                                      
                                                <asp:CheckBox ID="chksmsheadertag" runat="server" AutoPostBack="true" OnCheckedChanged="chksmsheadertag_CheckedChanged" />
                                                <label for='<%# ((GridViewRow)Container).FindControl("chksmsheadertag").ClientID %>' runat="server" id="lblchk">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </center>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Width="75px">

                                                    <asp:CheckBox ID="chktagsms" runat="server" Visible='<%#Eval("SendSMS").ToString()=="True"?true:false %>' />
                                                    <label for='<%# ((GridViewRow)Container).FindControl("chktagsms").ClientID %>' runat="server" id="lblchk">
                                                        <span></span>
                                                    </label>
                                                    <asp:HiddenField ID="hndid" runat="server" Value='<%#Eval("ContactID") %>' />
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="fullname">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Width="150px">
                                                    <asp:HiddenField ID="hndContactID" runat="server" Value='<%#Eval("ContactID") %>' />
                                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                    <asp:HyperLink ID="lnkFullname" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=c&compid="+Eval("CustomerID")+"&contid="+ Eval("ContactID") %>'><%#Eval("fullname")%></asp:HyperLink>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="tdspecialclass"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomer" runat="server" Width="200px" CssClass="gridmainspan">
                                                    <asp:HyperLink ID="lnkCustomer" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsalesrep" runat="server" Width="100px"><%#Eval("salesrep")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Suburb" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("Suburb")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="P/Cd" ItemStyle-HorizontalAlign="Center" SortExpression="PCd" HeaderStyle-CssClass="center-text">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Width="35px"><%#Eval("PCd")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Lead Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="leadtype">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label9" runat="server" Width="80px"><%#Eval("leadtype")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="projectstatus">
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Width="110px"> <%#Eval("projectstatus")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Quote Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblQuoteDate" runat="server" Width="90px"><%#Eval("QuoteDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NextFollowUp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNextFollowUpDate" runat="server" Width="90px"><%#Eval("NextFollowUpDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    </Columns>
                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                                <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div runat="server" visible="false" id="divpromosms">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="table1" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td width="20%">
                                                <asp:DropDownList runat="server" ID="ddlpromooffer" AppendDataBoundItems="true" CssClass="myvalpromo">
                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="searchfinal">
                                                <asp:Button runat="server" ID="btnpromooffer" OnClick="btnpromooffer_Click" Text=" Send Promo SMS" CssClass="btn btn-primary savewhiteicon" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <%-- <asp:PostBackTrigger ControlID="btnSearch" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>


    <script type="text/javascript">
        $("#dlprojectstatus dt a").on('click', function () {
            $("#dlprojectstatus dd ul").slideToggle('fast');
        });

        $("#dlprojectstatus dd ul li a").on('click', function () {
            $("#dlprojectstatus dd ul").hide();
        });

        $("#dlprojectteam dt a").on('click', function () {
            $("#dlprojectteam dd ul").slideToggle('fast');
        });

        $("#dlprojectteam dd ul li a").on('click', function () {
            $("#dlprojectteam dd ul").hide();
        });

        $("#dlprojectstate dt a").on('click', function () {
            $("#dlprojectstate dd ul").slideToggle('fast');
        });

        $("#dlprojectstate dd ul li a").on('click', function () {
            $("#dlprojectstate dd ul").hide();
        });

        $("#dlprojectlead dt a").on('click', function () {
            $("#dlprojectlead dd ul").slideToggle('fast');
        });

        $("#dlprojectlead dd ul li a").on('click', function () {
            $("#dlprojectlead dd ul").hide();
        });



        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });
    </script>

</asp:Content>
