using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_invoice_invcommon : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();

        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["id"];
        string InvType = Request.QueryString["type"];
        string InvNo = txtInstallerInvNo.Text;
        string InvAmnt = txtInstallerAmnt.Text;
        string InvDate = txtInstallerInvDate.Text;
        string PayDate = txtInstallerPayDate.Text;
        string Notes = txtElectricianInvoiceNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string InvEntered = stEmp.EmployeeID;
        string InvDoc = "";

        int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert(InvType, ProjectID, InvNo, InvAmnt, InvDate, PayDate, Notes, InvEntered, InvEntered,"","","",ddlInstaller.SelectedValue);
        if (fuInvDoc.HasFile)
        {
            InvDoc = Convert.ToString(success) + fuInvDoc.FileName;
            fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDoc);
            bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDoc);
            SiteConfiguration.UploadPDFFile("commoninvdoc", InvDoc);
            SiteConfiguration.deleteimage(InvDoc, "commoninvdoc");
        }

        if (Convert.ToString(success) != string.Empty)
        {
            if (Request.QueryString["type"] == "4")
            {
                Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
            }

            if (Request.QueryString["type"] == "5")
            {
                Response.Redirect("~/admin/adminfiles/company/casualmtce.aspx");
            }
        }
    }
    protected void lbtnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["type"] == "4")
        {
            Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
        }

        if (Request.QueryString["type"] == "5")
        {
            Response.Redirect("~/admin/adminfiles/company/casualmtce.aspx");
        }
    }
}