<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="instinvoice.aspx.cs" Inherits="admin_adminfiles_invoice_instinvoice" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script> --%>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(document).ready(function () {
            doMyAction();
        });



        function doMyAction() {

            $('#<%=ibtnAddPAidDate.ClientID %>').click(function () {
                formValidate();
            });

            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();


        }


    </script>
    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/arrowbottom.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/arrowright.png";
            }
        }
    </script>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-note2 icon"></i>Inst Invoice Tracker</h5>
        <div id="tdExport" class="pull-right" runat="server">

            <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
        </div>
    </div>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {

        }
        function pageLoaded() {

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            //alert($(".search-select").attr("class"));
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();

            $(".myvalinstinvoice").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval").select2({
                minimumResultsForSearch: -1
            });
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
        }
    </script>
    <div class="page-body padtopzero">
        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="content animate-panel " style="padding-bottom: 0px!important;">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
            </div>

            <div class="searchfinal searchFilterSection">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="inlineblock ">

                                                <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                    <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice" Width="150px">
                                                        <asp:ListItem Value="">Installer</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                        WatermarkText="Project Num" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                        ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                        WatermarkText="Manual Num" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                </div>

                                                <div class="input-group col-sm-2">
                                                    <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender16" runat="server" TargetControlID="txtContactSearch"
                                                        WatermarkText="Contact" />
                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" MinimumPrefixLength="2" runat="server"
                                                        UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                    </cc1:AutoCompleteExtender>
                                                </div>


                                                <div class="input-group col-sm-1" style="width: 120px" id="div3" runat="server">
                                                    <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice">
                                                        <asp:ListItem Value="0">Select Date</asp:ListItem>
                                                        <asp:ListItem Value="1">Invoice Date</asp:ListItem>
                                                        <asp:ListItem Value="2">Install Date</asp:ListItem>
                                                        <asp:ListItem Value="3">Paid Date</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                </div>
                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                </div>

                                                <div class="input-group">
                                                    <div class="viewAllBtn btn">
                                                        <div class="checkbox-info">
                                                            <label class="control-label">Historic</label>
                                                            <asp:CheckBox ID="chkHistoric" runat="server" />
                                                            <label for="<%=chkHistoric.ClientID %>" style="top: 0px;">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="input-group">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                                </div>
                                                <div class="input-group">
                                                    <asp:LinkButton ID="btnClearAll" runat="server"
                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray wid100btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                </div>
                                                <div class="input-group">
                                                    <div id="Div4" class="pull-right" runat="server">
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-info btngray wid100btn"
                                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="datashowbox dispnone">
                        <div class="row">
                            <div class="dataTables_length showdata col-sm-6">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </asp:Panel>
        <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body padtopzero finalgrid">
            <div class="row">
                <div id="PanGrid" runat="server" class="wid100">
                    <div class="customerTable searchfinal searchbar mainGridTable main-card card">
                        <div style="width: 100%; overflow-x: auto;">
                            <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("InvoiceID") %>' />
                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("InvoiceID") %>','tr<%# Eval("InvoiceID") %>');">
                                                <img id='imgdiv<%# Eval("InvoiceID") %>' src="../../../images/arrowright.png" />

                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnProjectID" Value='<%#Eval("ProjectID")%>' />

                                            <asp:Label ID="lblProjectNumber" runat="server" Width="70px">
                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=proelec&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                <%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="customer">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomer" runat="server" Width="100px">
                                                <%#Eval("Contact")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   
                                    <asp:TemplateField HeaderText="Installer Inv Amnt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="invamount"
                                        ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Width="90px"><%#Eval("InvAmnt","{0:0.00}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInstallBookingDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Install Inv  Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInstallInvDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InvDate", "{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Install Pay Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInstallPayDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "PayDate", "{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Install Complete" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallCompleted">
                                        <ItemTemplate>
                                            <asp:Label ID="lblConfirmed" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InstallCompleted", "{0:dd/MMM/yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Payment Status" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="textLeft" ItemStyle-CssClass="textLeft" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallerInvDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaymentstatus" runat="server" Width="110px"> <%# Eval("paymentstatus") %></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Paid Date" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="center" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" SortExpression="InstallerPayDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInstallerPayDate" runat="server"><%# DataBinder.Eval(Container.DataItem, "paydate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                            <div class="contacticonedit">
                                            <asp:LinkButton ID="lbtnPaid" CommandName="Paid" CssClass="btn btn-info btngray btn-xs"
                                                CommandArgument='<%#Eval("InvoiceID") %>' CausesValidation="false" runat="server" Visible='<%#Convert.ToString( Eval("paydate"))==""?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title="Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>

                                            <asp:LinkButton ID="lbtnRevert" CommandName="PaidRevert" CssClass="btn btn-info btngray btn-xs"
                                                CommandArgument='<%#Eval("InvoiceID") %>' Visible='<%#Convert.ToString( Eval("paydate"))!=""?true:false%>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>
                                                        </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-CssClass="dispnone" ItemStyle-CssClass="dispnone">
                                        <ItemTemplate>
                                            <tr id='tr<%# Eval("InvoiceID") %>' style="display: none;" class="dataTable left-text">
                                                <td colspan="98%" class="details">
                                                    <div id='div<%# Eval("InvoiceID") %>' style="display: none; position: relative; left: 0px; overflow: auto" class="subTable">
                                                        <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover subtablecolor">
                                                            <tr>
                                                                <td><b>Advance Amount</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblManualQuoteNumber" runat="server"><%#Eval("advanceTotalAmount","{0:0.00}")%></asp:Label>
                                                                </td>
                                                                <td><b>Total Amount</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblSystemDetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InvAmnt","{0:0.00}")%>'><%#Eval("InvAmnt","{0:0.00}")%></asp:Label>
                                                                </td>

                                                                <td><b>Payment Type</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblHouseType" runat="server"><%#Eval("FpTransType")%></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Installer Name</b>
                                                                </td>
                                                                <td runat="server" id="tdtype">
                                                                    <asp:Label ID="lblRoofType" runat="server"><%#Eval("InstallerName")%></asp:Label>
                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <div class="pagination">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                    </div>
                                </PagerTemplate>
                                <PagerStyle CssClass="paginationGrid" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                            </asp:GridView>

                            <%--    <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                        <td style="float: right;">
                                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="pagebtndesign firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="pagebtndesign" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn">
                                                                        <%#Eval("ID") %>

                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>--%>
                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td class="showEntryBtmBox">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="25">Display</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
            CancelControlID="ibtnCancelStatus" DropShadow="true" PopupControlID="div_popup" TargetControlID="btnNULLData1">
        </cc1:ModalPopupExtender>
        <div runat="server" id="div_popup" class="modalpopup" align="center" style="width: 900px; overflow-x: scroll;">
            <div class="popupdiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading pad15">
                                <asp:Label runat="server" ID="lbltitle"></asp:Label>
                                <div class="panel-tools">
                                    <div class="closbtn">
                                        <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                            <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group dateimgarea">
                                            <asp:Repeater runat="server" ID="rpt">
                                                <ItemTemplate>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("URL") %>' />
                                                </ItemTemplate>
                                                <SeparatorTemplate>
                                                    <br />
                                                    <br />
                                                </SeparatorTemplate>
                                            </asp:Repeater>
                                            <div class="clear">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bodymianbg">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>



        <asp:Button ID="btnNULLPaid" Style="display: none;" runat="server" />

        <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="divPaiddate" TargetControlID="btnNULLPaid" CancelControlID="ibtnCancel">
        </cc1:ModalPopupExtender>

        <div id="divPaiddate" runat="server" style="display: none;" class="modal_popup addDocumentPopup">
            <div class="modal-dialog" style="width: 500px;">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">

                        <div class="modalHead">
                            <h4 class="modal-title" id="myModalLabel1">Change Paid Date</h4>
                            <div>
                                <asp:LinkButton ID="ibtnCancel" runat="server" type="button" class="btn redButton btncancelIcon" data-dismiss="modal"
                                    OnClick="ibtnCancel_Click"><i class="fa fa-times"></i>Close
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="">
                            <div class="formainline formGrid">

                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" class="control-label">
                                               Paid Date</asp:Label>

                                    <%-- <div class="input-group date datetimepicker1 col-sm-3">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                    <asp:TextBox ID="txtActualPayDate" runat="server" class="form-control" placeholder="Actual Pay Date">
                                    </asp:TextBox>
                                  
                                </div>--%>
                                    <div>

                                        <div class="input-group date datetimepicker1 wid100">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <asp:TextBox ID="txtpaiddate" placeholder="Paid Date" runat="server" class="form-control"></asp:TextBox>
                                            <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtActualPayDate"
                                            WatermarkText="Actual Date" />--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" class="control-label">
                                               Comment</asp:Label>
                                    <div>
                                        <asp:TextBox ID="txtpaidcomment" TextMode="MultiLine" Rows="3" Columns="5" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpaidcomment"
                                            ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotesPaid" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group center-text">
                                    <asp:Button ID="ibtnAddPAidDate" runat="server" Text="Update" OnClick="ibtnAddPAidDate_Click"
                                        ValidationGroup="AddNotesPaid" CssClass="largeButton greenBtn savewhiteicon btnsaveicon" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none; width: 60%" class="modal_popup modal-danger modal-message">

            <div class="modal-dialog ">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <div class="modalHead">
                            <h4 class="modal-title" id="myModalLabelh"><i class="pe-7s-back popupIcon"></i>Revert</h4>
                        </div>
                    </div>
                    <div class="modal-body">

                            <div class="formainline formGrid">
                                <div class="">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Are You Sure Revert the Paid Date?
                                        </label>
                                    </span>
                                    <div class="">
                                        <div class="form-group spicaldivin " runat="server">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 textRight">
                                       

                                         <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click"  CssClass="btn-shadow btn btn-info btngray"><i class="fa fa-check"></i>Yes</asp:LinkButton>
                                            <asp:LinkButton ID="lnkcancel" runat="server" CssClass="btn-shadow btn btn-danger btngray" data-dismiss="modal"><i class="fa fa-times"></i>No</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />

        <asp:HiddenField ID="hndinvoicepaymentid" runat="server" />

        <asp:HiddenField runat="server" ID="hdncountdata" />
    </div>





</asp:Content>

