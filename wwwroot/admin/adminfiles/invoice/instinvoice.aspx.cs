using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class admin_adminfiles_invoice_instinvoice : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;

            BindDropDown();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataBind();

        if (Roles.IsUserInRole("Installer"))
        {
            ddlInstaller.Enabled = false;
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
            ddlInstaller.SelectedValue = stCont.ContactID;
        }
        else
        {
            ddlInstaller.Enabled = true;
        }

    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        string data = "", data1 = "";
        string InstallerID = "";
        if (Roles.IsUserInRole("Installer"))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
            InstallerID = stCont.ContactID;
        }
        else
        {
            InstallerID = ddlInstaller.SelectedValue;
        }

        if (Roles.IsUserInRole("Installer"))
        {
            if (InstallerID != string.Empty)
            {
                //dt = ClstblProjects.tblProjects_SelectInstInvoiceInstaller(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, InstallerID, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkHistoric.Checked), ddlmeterapplyref.SelectedValue);
            }
        }
        else
        {
            //dt = ClstblProjects.tblProjects_SelectInstInvoice(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, InstallerID, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkHistoric.Checked), ddlmeterapplyref.SelectedValue);

        }
        if (chkHistoric.Checked == true)
        {
            data1 = "(inv.PayDate is null or inv.PayDate is not null) and (P.InstallBookingDate is not null) and ";
        }
        else
        {
            data1 = "(inv.PayDate is null) and (P.InstallBookingDate is not null) and ";
        }
        string enddate = "";
        if (txtEndDate.Text != string.Empty)
        {
            enddate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtEndDate.Text));
        }
        string startdate = "";
        if (txtStartDate.Text != string.Empty)
        {
            startdate = string.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(txtStartDate.Text));
        }

        //data = "select count(P.ProjectID) as countdata from tblProjects P join tblProjectStatus PS on PS.ProjectStatusID=P.ProjectStatusID join tblContacts CNT on CNT.ContactID=P.ContactID left outer join tblProjects2 P2 on P2.ProjectLinkID=P.ProjectID  left outer join tblInvoiceCommon inv on inv.ProjectID=P.ProjectID  left outer join tblHouseType HT on HT.HouseTypeID=P.HouseTypeID left outer join tblRoofTypes RT on RT.RoofTypeID=P.RoofTypeID left outer join tblRoofAngles RA on RA.RoofAngleID=P.RoofAngleID where " + data1 + "(((upper(CNT.ContFirst+' '+CNT.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(CNT.ContFirst) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or (upper(CNT.ContLast) like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%')) or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='') and ((P.ProjectNumber='" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "') or '" + System.Security.SecurityElement.Escape(txtProjectNumber.Text) + "'=0) and ((upper(P.ManualQuoteNumber) like '%'+'" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtMQNsearch.Text) + "'='') and ((P.Installer='" + InstallerID + "' or '" + InstallerID + "'=0)) and ((upper(P.InstallCity) like '%'+'" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSerachCity.Text) + "'='') and ((upper(P.InstallState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%') or '" + ddlSearchState.SelectedValue + "'='') and ((upper(P.InstallPostCode) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (isnull('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "','') <= CASE WHEN '" + ddlSearchDate.SelectedValue + "'=1 THEN P.InvoiceSent else CASE WHEN '" + ddlSearchDate.SelectedValue + "'=2 then P.ProjectOpened else CASE WHEN '" + ddlSearchDate.SelectedValue + "'=3 then P.InstallBookingDate else CASE WHEN '" + ddlSearchDate.SelectedValue + "'=4 then P.InstallerPayDate else isnull('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "','') end end end END) and (('" + System.Security.SecurityElement.Escape(enddate) + "' >= CASE WHEN '" + ddlSearchDate.SelectedValue + "'=1 THEN P.InvoiceSent else CASE WHEN '" + ddlSearchDate.SelectedValue + "'=3 then P.InstallBookingDate else CASE WHEN '" + ddlSearchDate.SelectedValue + "'=4 then P.InstallerPayDate else isnull('" + System.Security.SecurityElement.Escape(enddate) + "','') end end END) or (isnull('" + System.Security.SecurityElement.Escape(enddate) + "','')=''))  and  (isnull(P.MeterAppliedRef,'')= CASE WHEN '" + ddlmeterapplyref.SelectedValue + "'=1 THEN P.MeterAppliedRef else case when '" + ddlmeterapplyref.SelectedValue + "'=0 then isnull(P.MeterAppliedRef,'') else isnull('',0)end end)";
        DataTable dt = ClstblCustomers.query_execute(data);
        //   Response.Write(data);
        //Response.End();
        if (dt.Rows.Count > 0)
        {
            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            hdncountdata.Value = countdata.ToString();
        }

    }
    protected DataTable GetGridData()
    {
        BindDataCount();
        DataTable dt = new DataTable();

        dt = ClstblProjects.GetAllInstallerData();
        if (!string.IsNullOrEmpty(txtProjectNumber.Text))
        {
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<Int32>("ProjectNumber").ToString() == txtProjectNumber.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();
        }
        if (!string.IsNullOrEmpty(txtContactSearch.Text))
        {
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<string>("Contact") == txtContactSearch.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();

        }
        if (ddlInstaller.SelectedValue != "0" && !string.IsNullOrEmpty(ddlInstaller.SelectedValue))
        {
            var aa = ddlInstaller.SelectedItem.Text;
            var results = from myRow in dt.AsEnumerable()
                          where myRow.Field<string>("InstallerName") == ddlInstaller.SelectedItem.Text
                          select myRow;
            dt = results.Any() ? results.CopyToDataTable() : dt.Clone();
        }
        if (ddlSearchDate.SelectedValue != "0")
        {
            if (!string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
            {
                DateTime stDate = Convert.ToDateTime(txtStartDate.Text);
                DateTime edDate = Convert.ToDateTime(txtEndDate.Text);

                if (ddlSearchDate.SelectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InvDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("InvDate")).Date >= stDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InvDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= stDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("PayDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("PayDate")).Date >= stDate.Date &&
                                   Convert.ToDateTime(myRow.Field<DateTime?>("PayDate")).Date <= edDate.Date
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
            if (string.IsNullOrEmpty(txtStartDate.Text) && !string.IsNullOrEmpty(txtEndDate.Text))
            {
                DateTime edDate = Convert.ToDateTime(txtEndDate.Text);

                if (ddlSearchDate.SelectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InvDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InvDate")).Date <= edDate.Date
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date <= edDate.Date
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("PayDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where
                                   Convert.ToDateTime(myRow.Field<DateTime?>("PayDate")).Date <= edDate.Date
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
            if (!string.IsNullOrEmpty(txtStartDate.Text) && string.IsNullOrEmpty(txtEndDate.Text))
            {
                DateTime stDate = Convert.ToDateTime(txtStartDate.Text);

                if (ddlSearchDate.SelectedValue == "1")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InvDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("InvDate")).Date >= stDate.Date 
                                   select myRow;

                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "2")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("InstallBookingDate").ToString())).ToList();

                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("InstallBookingDate")).Date >= stDate.Date
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();

                }
                if (ddlSearchDate.SelectedValue == "3")
                {
                    var results = dt.AsEnumerable().Where(x => !string.IsNullOrEmpty(x.Field<DateTime?>("PayDate").ToString())).ToList();
                    var results1 = from myRow in results
                                   where Convert.ToDateTime(myRow.Field<DateTime?>("PayDate")).Date >= stDate.Date 
                                   select myRow;
                    dt = results1.Any() ? results1.CopyToDataTable() : dt.Clone();
                }
            }
        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();

            PanNoRecord.Visible = false;
            divnopage.Visible = true;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = true;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private void HidePanels()
    {
        PanNoRecord.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ProjectID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "docscheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "docsuncheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "False");
        }
        if (e.CommandName.ToLower() == "nocheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "nouncheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "False");
        }
        if (e.CommandName.ToLower() == "paid")
        {

            hndinvoicepaymentid.Value = ProjectID;
            txtpaiddate.Text = string.Empty;
            txtpaidcomment.Text = string.Empty;
            ModalPopupExtender2.Show();
            divPaiddate.Visible = true;
            //  divAddComment.Visible = true;
        }

        if (e.CommandName.ToLower() == "paidrevert")
        {

            hdndelete.Value = ProjectID;
            ModalPopupExtenderDelete.Show();
            //  divAddComment.Visible = true;
        }

        string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
        string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
        string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
        string username = ConfigurationManager.AppSettings["username"].ToString();
        string password = ConfigurationManager.AppSettings["password"].ToString();
        //FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);


        #region rec
        if (e.CommandName == "lnkrec")
        {

            string formbayid = e.CommandArgument.ToString();//formbayid
            if (formbayid != string.Empty)
            {
                ModalPopupExtender1.Hide();
                div_popup.Visible = true;
                //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
                DataTable dt = new DataTable();
                dt.Columns.Add("doctype");
                dt.Columns.Add("URL");
                //foreach (var item in jobDoc)
                //{
                //    DataRow dr = dt.NewRow();
                //    dr["doctype"] = item["doctype"];
                //    dr["URL"] = item["URL"];
                //    dt.Rows.Add(dr);
                //}
                DataView dvphotos = dt.DefaultView;
                dvphotos.RowFilter = "doctype=1";
                rpt.DataSource = dvphotos;
                rpt.DataBind();

                lbltitle.Text = "Rec";
            }
        }
        #endregion

        #region ccp
        if (e.CommandName == "lnkccp")
        {
            string formbayid = e.CommandArgument.ToString();//formbayid
            if (formbayid != string.Empty)
            {
                ModalPopupExtender1.Hide();
                div_popup.Visible = true;
                //var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
                DataTable dt = new DataTable();
                dt.Columns.Add("doctype");
                dt.Columns.Add("URL");
                //foreach (var item in jobDoc)
                //{
                //    DataRow dr = dt.NewRow();
                //    dr["doctype"] = item["doctype"];
                //    dr["URL"] = item["URL"];
                //    dt.Rows.Add(dr);
                //}
                DataView dvphotos = dt.DefaultView;
                dvphotos.RowFilter = "doctype=3";
                rpt.DataSource = dvphotos;
                rpt.DataBind();

                lbltitle.Text = "CCP";
            }
        }
        #endregion
        BindGrid(0);
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtContactSearch.Text = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    txtSearchPostCode.Text = string.Empty;
    //    ddlSearchDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    chkHistoric.Checked = false;

    //    string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //    SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
    //    ddlInstaller.SelectedValue = stCont.ContactID;

    //    BindGrid(0);
    //    //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();

        string InstallerID = "";
        if (Roles.IsUserInRole("Installer"))
        {
            SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
            if (stCont.ContactID != string.Empty)
            {
                InstallerID = stCont.ContactID;
            }
        }
        else
        {
            InstallerID = ddlInstaller.SelectedValue;
        }

        if (InstallerID != string.Empty)
        {
            if (Roles.IsUserInRole("Installer"))
            {
                dt = ClstblProjects.tblProjects_SelectInstInvoiceInstaller(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, InstallerID, "", "", "", ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkHistoric.Checked), "");
            }
            else
            {
                dt = ClstblProjects.tblProjects_SelectInstInvoice(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, InstallerID, "", "", "", ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkHistoric.Checked), "");
            }
        }
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Installer Invoive Tracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 6, 25, 0, 8, 9, 26, 22, 23, 24, 12, 27, 14, 15, 28, 29, 30, 19 };
            string[] arrHeader = { "Project#", "Manual#", "Quote", "Address", "PCode", "SystemDetails", "Install Date", "House Type", "Roof Type", "Angle", "Trav", "Confirmed", "Inv #", "Amount", "Issued", "Paid Date", "Foll-Up", "Meter Notes" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndformbay = (HiddenField)e.Row.FindControl("hndformbay");
            HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");

            if (hndformbay.Value != string.Empty)
            {
                string formbayid = hndformbay.Value;
                LinkButton lnkrec = (LinkButton)e.Row.FindControl("lnkrec");
                LinkButton lnkccp = (LinkButton)e.Row.FindControl("lnkccp");

                string subdomain = ConfigurationManager.AppSettings["subdomain"].ToString();
                string clientid = ConfigurationManager.AppSettings["clientid"].ToString();
                string clientSecret = ConfigurationManager.AppSettings["clientSecret"].ToString();
                string username = ConfigurationManager.AppSettings["username"].ToString();
                string password = ConfigurationManager.AppSettings["password"].ToString();

                //FormbayClient c = new FormbayClient(subdomain, clientid, clientSecret, username, password);
                //var jobForms = c.GetJobForms();
                //string[] formsdocs = jobForms.ToObject<string[]>();

                bool checkjob = false;
                // int pos = Array.IndexOf(formsdocs, formbayid);
                //if (pos >= 0)
                //{
                //    checkjob = true;
                //}

                if (checkjob)
                {

                    //try
                    {
                        // var jobDoc = c.GetJob_Document(Convert.ToInt32(formbayid));
                        //Job job = new Job();
                        //job = c.GetJob(Convert.ToInt32(formbayid));

                        DataTable dt = new DataTable();
                        dt.Columns.Add("doctype");
                        dt.Columns.Add("URL");
                        dt.Columns.Add("name");
                        //foreach (var item in jobDoc)
                        //{
                        //    DataRow dr = dt.NewRow();
                        //    dr["doctype"] = item["doctype"];
                        //    dr["URL"] = item["URL"];
                        //    if (item["doctype"].ToString() == "100")
                        //    {
                        //        dr["name"] = item["name"];
                        //    }
                        //    else
                        //    {
                        //        dr["name"] = "";
                        //    }
                        //    dt.Rows.Add(dr);
                        //}
                        SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
                        if (dt.Rows.Count > 0)
                        {
                            DataView dv = dt.DefaultView;
                            dv.RowFilter = "doctype=1";
                            if (dv.ToTable().Rows.Count > 0 && stpro.FormbayId != string.Empty)
                            {
                                lnkrec.Visible = true;
                            }
                            else
                            {
                                lnkrec.Visible = false;
                            }

                            DataView dvccp = dt.DefaultView;
                            dvccp.RowFilter = "doctype=3";
                            if (dvccp.ToTable().Rows.Count > 0 && stpro.FormbayId != string.Empty)
                            {
                                lnkccp.Visible = true;
                            }
                            else
                            {
                                lnkccp.Visible = false;
                            }

                        }

                    }

                }
            }
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtContactSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;

        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        chkHistoric.Checked = false;

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblContacts stCont = ClstblContacts.tblContacts_StructByUserId(userid);
        ddlInstaller.SelectedValue = stCont.ContactID;

        BindGrid(0);
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void ibtnAddPAidDate_Click(object sender, EventArgs e)
    {
        string projectid = hndinvoicepaymentid.Value;
        bool suc = ClstblInvoicePayments.tblInvoicePayments_UpdatePaidDate(projectid, txtpaiddate.Text, txtpaidcomment.Text);

        if (suc)
        {
            ModalPopupExtender2.Hide();
        }
        BindGrid(0);

    }
    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        divPaiddate.Visible = false;
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {

        if (hdndelete.Value.ToString() != string.Empty)
        {
            string projectid = hdndelete.Value;
            bool suc = ClstblInvoicePayments.tblInvoicePayments_UpdatePaidDate(projectid, "", "");
            if (suc)
            {
                ModalPopupExtenderDelete.Hide();

            }
        }

        BindGrid(0);
    }
}