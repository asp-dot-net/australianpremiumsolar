<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" UICulture="en-GB" Culture="en-GB"
    AutoEventWireup="true" CodeFile="updateinvcomm.aspx.cs" Inherits="admin_adminfiles_invoice_updateinvcomm" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <%--  <div id="tdExport" class="pull-right" runat="server">
             <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click"  CausesValidation="false"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>
                  </div>
            <h3 class="m-b-xs text-black">Update Invoice</h3>--%>
               <div class="small-header transition animated fadeIn">
                <div class="hpanel">
                    <div class="panel-body">
                        <div id="hbreadcrumb" class="pull-right">
                            <ol class="hbreadcrumb breadcrumb fontsize16">
                              <%--  <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                                <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>
                            </ol>
                        </div>
                        <h2 class="font-light m-b-xs">Update Invoice
                        </h2>

                    </div>
                </div>
            </div>
                
           <br />
            
            <div class="contactsarea">
               <%-- <div class="addcontent">
                    <asp:LinkButton ID="lbtnBack" runat="server" CausesValidation="false" OnClick="lbtnBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                    
                </div>--%>
                <div class="messesgarea">
                    <asp:Panel ID="PanSuccess" runat="server" CssClass="pansucess" Visible="false">
                        <i class="icon-ok-sign"></i>
                        <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanError" runat="server" CssClass="failure" Visible="false">
                        <i class="icon-remove-sign"></i>
                        <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                    </asp:Panel>
                </div>
                <div class="content animate-panel">
                       <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">Inv No </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtInstallerInvNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            SetFocusOnError="true" ControlToValidate="txtInstallerInvNo" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group  ">
                                                    <span class="name">
                                                        <label class="control-label">Inv Amnt </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtInstallerAmnt" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                            ControlToValidate="txtInstallerAmnt" Display="Dynamic"
                                                            ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            SetFocusOnError="true" ControlToValidate="txtInstallerAmnt" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group dateimgarea">

                                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" class="col-sm-2  control-label">
                                                <strong>Inv Date</strong></asp:Label>
                                                     <div class="col-sm-6">
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtInstallerInvDate" runat="server" class="form-control" placeholder="Inv Date">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                </div>


                                                    <%--<span class="name">
                                                        <label class="control-label">Inv Date </label>
                                                    </span><span class="dateimg">
                                                        <div class="form-group">
                                                            <table class="datedpikartable">
                                                                <tr>
                                                                    <td class='form-group date' id='datetimepicker9'>
                                                                        <asp:TextBox ID="txtInstallerInvDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtInstallerInvDate"
                                                                            WatermarkText="Start Date" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Img1" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />&nbsp;
                                            <cc1:CalendarExtender ID="calCheckIn" runat="server" PopupButtonID="Img1" TargetControlID="txtInstallerInvDate"
                                                Format="dd/MM/yyyy">
                                            </cc1:CalendarExtender>
                                                                        <cc1:MaskedEditExtender ID="mskeditCheckIn" runat="server" Mask="99/99/9999" MessageValidatorTip="true"
                                                                            OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" MaskType="date"
                                                                            TargetControlID="txtInstallerInvDate" CultureName="en-GB">
                                                                        </cc1:MaskedEditExtender>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>--%>

                                                        <%-- <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtInstallerInvDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox></td>
                                                                <td>&nbsp;<asp:ImageButton ID="Image30" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender30" runat="server" PopupButtonID="Image30"
                                                                        TargetControlID="txtInstallerInvDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                        SetFocusOnError="true" ControlToValidate="txtInstallerInvDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ControlToValidate="txtInstallerInvDate" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">

                                                    <div class="form-group">
                                                    <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                Pay Date</asp:Label>
                                                     <div class="col-sm-6">
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtInstallerPayDate" runat="server" class="form-control" >
                                                        </asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                        SetFocusOnError="true" ControlToValidate="txtInstallerPayDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                </div>

                                                <%--<div class="form-group dateimgarea">
                                                    <span class="name">
                                                        <label class="control-label">Pay Date </label>
                                                    </span><span class="dateimg">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtInstallerPayDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                </td>
                                                                <td>&nbsp;<asp:ImageButton ID="Image31" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender31" runat="server" PopupButtonID="Image31"
                                                                        TargetControlID="txtInstallerPayDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                        SetFocusOnError="true" ControlToValidate="txtInstallerPayDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>--%>
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Installer
                                                        </label>
                                                    </span><span>
                                                        <asp:DropDownList ID="ddlInstaller" Width="200px" runat="server" AppendDataBoundItems="true"
                                                            CssClass="form-control search-select">
                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">Upload Inv Doc </label>
                                                    </span><span>
                                                        <asp:HyperLink ID="lblInvDoc" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                        <asp:FileUpload ID="fuInvDoc" runat="server" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorInvDoc" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            SetFocusOnError="true" ControlToValidate="fuInvDoc" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">Notes </label>
                                                    </span><span>
                                                        <asp:TextBox ID="txtElectricianInvoiceNotes" runat="server" TextMode="MultiLine"
                                                            Height="50px" CssClass="form-control"></asp:TextBox>
                                                    </span>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row" id="divAddUpdate" runat="server">
                                            <div class="col-md-12 text-center">
                                                <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                    Text="Save" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
             
            </div>
        </div>
    </section>

        </ContentTemplate>

        </asp:UpdatePanel>
</asp:Content>

