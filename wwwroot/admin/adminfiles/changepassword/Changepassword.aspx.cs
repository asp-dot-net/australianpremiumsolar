﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_Masters_Editprofile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (User.Identity.IsAuthenticated == false)
        {

            Response.Redirect("~/default.aspx?ReturnUrl");
        }
        if (!IsPostBack)
        {
            string Userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        lblAddUpdate.Text = "";
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
    }
    public void SetUpdate()
    {
        // Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAddUpdate.Visible = false;
    }
    public void SetDelete()
    {
        //  Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        //   btnUpdate.Visible = true;
        //   btnCancel.Visible = false;
        PanAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        //      btnUpdate.Visible = true;
        //     btnCancel.Visible = true;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        //PanAlreadExists.Visible = false;
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        //NewPassword.Text = string.Empty;
        //ConfirmNewPassword.Text=string.Empty;
        //txtPassword.Text = string.Empty;
        PanAddUpdate.Visible = true;
    }
    protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
    {
        string Userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        //Response.Write(Userid + "," + CurrentPassword.Text);
        //Response.End();
        int exist_pass = ClsClientuser.changepassword_Password_Exists(Userid, CurrentPassword.Text);
        if (exist_pass == 1)
        {
            ClsClientuser.ClientUserUpdateUserPassword(Userid, NewPassword.Text);
            SetAdd1();
            //PanSuccess.Visible = true;
            Panfail.Visible = false;
        }
        else
        {
            Panfail.Visible = true;
            PanSuccess.Visible = false;
        }
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

}