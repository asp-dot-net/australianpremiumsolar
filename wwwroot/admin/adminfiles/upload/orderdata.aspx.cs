﻿using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;

public partial class admin_adminfiles_upload_orderdata : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HidePanel();
        }
    }
    public void HidePanel()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
    }
     
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("orderdata.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int success = 0;
        Response.Expires = 400;
        Server.ScriptTimeout = 1200;
        if (FileUpload1.HasFile)
        {
            FileUpload1.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\OrderData\\" + FileUpload1.FileName);
            string connectionString = "";
            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}

            // connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "\\userfiles\\OrderData\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (FileUpload1.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "\\userfiles\\OrderData\\" + FileUpload1.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;
                   

                    //Response.End();
                    using (DbDataReader dr1 = command.ExecuteReader())
                    {
                        if (dr1.HasRows)
                        {
                            while (dr1.Read())
                            {
                                string OrderNo = "";
                                string SerialNo = "";

                                OrderNo = dr1["OrderNo"].ToString();
                                SerialNo = dr1["SerialNo"].ToString();
                                if (OrderNo != string.Empty && SerialNo != string.Empty)
                                {
                                    try
                                    {
                                        success = ClstblOrderData.tblOrderData_Insert(OrderNo, SerialNo);
                                    }
                                    catch { }
                                    if (success > 0)
                                    {
                                        SetAdd1();
                                        //PanSuccess.Visible = true;
                                        PanError.Visible = false;
                                    }
                                    else
                                    {
                                        SetError1();
                                        //PanError.Visible = true;
                                        PanSuccess.Visible = false;
                                    }
                                }
                            }
                             
                        }
                    }
                }
            }
            
        }
        
       
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}