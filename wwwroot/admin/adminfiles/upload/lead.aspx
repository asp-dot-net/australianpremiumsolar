<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="lead.aspx.cs" Inherits="admin_adminfiles_upload_lead" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

              function (isConfirm) {
                  if (isConfirm) {
                      // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      eval(defaultAction);

                      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      return true;
                  } else {
                      // swal("Cancelled", "Your imaginary file is safe :)", "error");
                      return false;
                  }
              });
        }
    </script>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-add-user icon"></i>Lead</h5>
        <div id="hbreadcrumb">
            <asp:Button ID="btnLead" runat="server" OnClick="btnLead_Click1" CssClass="btneditinvoice martopzero btn btn-primary btnPayment"
                    Text="Upload Leads" CausesValidation="false" />
              <asp:Button ID="btnUploadGovStatus" runat="server" OnClick="btnUploadGovStatus_Click" CssClass="btneditinvoice martopzero btn btn-primary btnPayment"
                    Text="Upload Gov. Status" CausesValidation="false" />
            <div id="tdExport" class="pull-right" runat="server">
                  <asp:Button ID="btnNULLInvPay" Style="display: none;" runat="server" />
            </div>
        </div>
    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.custom-file-input').on('change', function (e) {
                var fileName = e.target.files[0].name;
                //  var fileName = document.getElementsByClassName("fileupload").files[0].name;
                //alert(fileName);
                $(this).next('.form-control-file').addClass("selected").html(fileName);
            });
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {

        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });


            //$("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>

    </asp:UpdatePanel>

    <div class="page-body padtopzero formGrid">
    



        <asp:Panel runat="server" ID="PanGridSearch">

            <div class="animate-panelmessesgarea padbtmzero">
                <div class="messesgarea">
                   
                    <div class="alert alert-danger" id="PanEmpty" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Lead Source. </strong>
                    </div>
                   
                </div>
            </div>

            <div class="searchfinal searchFilterSection">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                    <tr>
                                        <td class="">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="input-group" style="width: 150px;">
                                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender1" runat="server" targetcontrolid="txtSearch"
                                                            watermarktext="Company Name" />
                                                    </div>
                                                    <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div1" runat="server">
                                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Team</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div2" runat="server">
                                                        <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div3" runat="server">
                                                        <asp:DropDownList ID="ddlDup" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Dup</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Dup</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group" id="div5" runat="server">
                                                        <asp:DropDownList ID="ddlwebdup" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select Webdup</asp:ListItem>
                                                            <asp:ListItem Value="1">Dup</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Dup</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="input-group date datetimepicker1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btngray btnsearchicon wid100btn" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info btngray btnClear wid100btn"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <asp:Button ID="btnNULLData1" Style="display: none;" Text="test" runat="server" />
                <cc1:modalpopupextender id="MPECustomerName" runat="server" backgroundcssclass="modalbackground"
                    dropshadow="false" popupcontrolid="divCustomerName" targetcontrolid="btnNULLData1"
                    cancelcontrolid="btncanelCustomer">
                </cc1:modalpopupextender>

                <div id="divCustomerName" runat="server" style="display: none; width: 1000px;" class="modal_popup">
                    <div>
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header text-center">
                                <%--<asp:LinkButton ID="btncanelCustomer" CssClass="close" runat="server"><span aria-hidden="true">
                        &times;</span><span class="sr-only">Close</span></asp:LinkButton>--%>
                                <button id="btncanelCustomer" runat="server" type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;
                                    </span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Duplicate Record</h4>
                            </div>
                            <asp:HiddenField ID="hdnWebDownloadID" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                            <div class="panel-body">
                                <asp:GridView ID="DataList1" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="DataList1_PageIndexChanging" AutoGenerateColumns="false"
                                    CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Customer")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("CustPhone")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ContMobile")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%# Eval("ContEmail")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("EmployeeName")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Source" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("sourcename")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("projectstatus")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Address")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>





                <asp:Button ID="btnNULLData2" Style="display: none;" Text="test" runat="server" />
                <cc1:modalpopupextender id="ModalPopupExtender1" runat="server" backgroundcssclass="modalbackground"
                    dropshadow="false" popupcontrolid="divwebdup" targetcontrolid="btnNULLData2"
                    cancelcontrolid="btncanelCustomer1">
                </cc1:modalpopupextender>

                <div id="divwebdup" runat="server" style="display: none; width: 1000px;" class="modal_popup">
                    <div>
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header">
                                <div style="float: right">
                                    <button id="btncanelCustomer1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                        Close</button>
                                </div>
                                <h4 class="modal-title" id="H1">Duplicate Record</h4>
                            </div>
                            <asp:HiddenField ID="hdnwebdup" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                            <div class="panel-body">
                                <asp:GridView ID="GridView2" DataKeyNames="WebDownloadID" runat="server" OnPageIndexChanging="GridView2_PageIndexChanging"
                                    CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-CssClass="gridpagination" AllowSorting="true" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Customer")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("CustPhone")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ContMobile")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%# Eval("ContEmail")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Address")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </asp:Panel>

        

        <div class="fullWidthTable searchfinal customerTable searchbar mainGridTable main-card mb-3 card">
        <asp:Panel ID="upload" runat="server">
            <div class="widget-body shadownone brdrgray marbtm15 ">
                <div id="Div4" runat="server">
                    <div id="tblassign" class="">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer card-header pddright0">
                    <div class="row wid100">
                    <div class="col-sm-6">
                        <div class="textLeft">
                            <div class="input-group">
                                <asp:DropDownList ID="ddldupdrp" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Dup</asp:ListItem>
                                    <asp:ListItem Value="2">Not Dup</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="input-group">
                                <asp:Button runat="server" ID="btndup" OnClick="btndup_Click" Text="Update" CssClass="btn largeButton smallbtn greenBtn savewhiteicon btnsaveicon" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 center pddright0">
                        <div class="textRight">
                            <div class="input-group">
                                <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                    <asp:ListItem Value="">Select Sales Rep</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="input-group">
                                <asp:Button ID="btnAssidnLead" runat="server" Text="Assign lead" class="btn btn-info btngray smallgrayBtn wid100 addwhiteicon"
                                    OnClick="btnAssidnLead_Click" />
                            </div>
                            
                            <div class="input-group">
                                <asp:DropDownList ID="action" class="myval" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" AutoPostBack="true" OnSelectedIndexChanged="action_SelectedIndexChanged">
                                    <asp:ListItem Value="">Action</asp:ListItem>
                                    <asp:ListItem Value="1">Delete</asp:ListItem>
                                    <asp:ListItem Value="2">Export</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </asp:Panel>
        <div class="finalgrid">
            <div class="padtopzero padrightzero">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div id="PanGrid" runat="server" class="wid100">
                        <div class="table-responsive xscroll leadtablebox">
                            <asp:GridView ID="GridView1" DataKeyNames="WebDownloadID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowCommand="GridView1_RowCommand"
                                OnDataBound="GridView1_DataBound" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowCreated="GridView1_RowCreated">
                                <Columns>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            
                                        <%-- <label for="<%=chkActive.ClientID %>">
                                            <asp:CheckBox ID="chkActive" runat="server" />
                                            <span class="text">&nbsp;</span>
                                        </label>
                                        --%>
                                        <%-- <span class="euroradio">--%>                                      
                                                       
                                            <div class="checkBox checkBoxList">                 
                                            <asp:CheckBox ID="chkheader" runat="server" AutoPostBack="true" OnCheckedChanged="chkheader_CheckedChanged" />
                                            <label for='<%# ((GridViewRow)Container).FindControl("chkheader").ClientID %>' runat="server" id="lblchk123"></label>

                                            </div>
                                        <%-- </span>--%>
                                                      
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        <div class="checkBox checkBoxList"> 
                                            <asp:Label ID="Label2" runat="server" Width="30px">
                                                <asp:HiddenField ID="hdnWebDownloadID1" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                                                <asp:CheckBox ID="chk1" runat="server" Visible='<%# Convert.ToBoolean(Eval("duplicaterecord"))%>' />
                                                <label for='<%# ((GridViewRow)Container).FindControl("chk1").ClientID %>' runat="server" id="lblchk4543"></label>
                                            </asp:Label>
                                        </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="170px">
                                                <asp:HiddenField ID="hdnSource" runat="server" Value='<%# Eval("Source")%>' />
                                                <asp:HiddenField ID="hdnSourceSub" runat="server" Value='<%# Eval("SubSource")%>' />
                                                <%#Eval("Customer")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="ContEmail">
                                        <ItemTemplate>
                                            <asp:Label ID="Label21" runat="server" Width="200px">
                                                <%#Eval("ContEmail")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="CustPhone">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Width="100px">
                                                <%#Eval("CustPhone")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ContMobile">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Width="100px">
                                                <%#Eval("ContMobile")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Address"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Width="150px" data-placement="top" data-original-title='<%#Eval("Address")%>' data-toggle="tooltip">
                                                <%#Eval("Address")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="City"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Width="100px">
                                                <%#Eval("City")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="State">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Width="50px">
                                                <%#Eval("State")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PostCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="PostCode">
                                        <ItemTemplate>
                                            <asp:Label ID="Label99" runat="server" Width="60px">
                                                <%#Eval("PostCode")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SubSource" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="SubSource">
                                        <ItemTemplate>
                                            <asp:Label ID="Label199" runat="server" Width="100px">
                                                <%#Eval("SubSource")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <%--<asp:TemplateField HeaderText="Dup." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="duplicaterecord"
                                            HeaderStyle-CssClass="gridheadertext">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Width="50px">
                                                    <asp:CheckBox ID="chkDuplicateRecord" runat="server" AutoPostBack="true" OnCheckedChanged="chkDuplicateRecord_OnCheckedChanged"
                                                        Visible='<%#Eval("duplicaterecord")%>' />
                                                    <label for='<%# ((GridViewRow)Container).FindControl("chkDuplicateRecord").ClientID %>' runat="server" id="lblchk">
                                                        <span></span>
                                                    </label>
                                                    <asp:HiddenField ID="hdnWebDownloadID1" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Not Dup." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="duplicaterecord">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Width="70px">
                                                    <asp:HiddenField ID="hdnWebDownloadID2" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                                                    <asp:CheckBox ID="chkNotduplicate" runat="server" AutoPostBack="true" OnCheckedChanged="chkNotduplicate_OnCheckedChanged"
                                                        Visible='<%#Eval("duplicaterecord")%>' />
                                                    <label for='<%# ((GridViewRow)Container).FindControl("chkNotduplicate").ClientID %>' runat="server" id="lblchk1">
                                                        <span></span>
                                                    </label>

                                                </asp:Label>
                                            </ItemTemplate>
                                    </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="View Dup." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="duplicaterecord"
                                        ItemStyle-CssClass="center"
                                        HeaderStyle-CssClass="gridheadertext center" HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Width="80px">
                                                <asp:HiddenField ID="hdncustomer" runat="server" Value='<%# Eval("Customer")%>' />
                                                <asp:HiddenField ID="hdnphone" runat="server" Value='<%# Eval("CustPhone")%>' />
                                                <asp:HiddenField ID="hdnemail" runat="server" Value='<%# Eval("ContEmail")%>' />
                                                <asp:LinkButton ID="btnduplicate" runat="server" CommandName="Select" CausesValidation="false" CssClass="badge badge-danger"
                                                    Visible='<%# Convert.ToBoolean((Eval("duplicaterecord")))%>' data-placement="top" data-original-title="View Duplicate" data-toggle="tooltip">
                                                       Dup</asp:LinkButton></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Webdup." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" SortExpression="duplicaterecord"
                                        HeaderStyle-CssClass="gridheadertext">
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" runat="server">
                                                <asp:LinkButton ID="btn_duplicate" runat="server" CssClass="btn btn-info btn-xs" CommandName="webdup" CommandArgument='<%#Eval("WebDownloadID")%>' CausesValidation="false"
                                                    Visible='<%# Convert.ToBoolean((Eval("duplicaterecord1")))%>' data-placement="top" data-original-title="Web Duplicate" data-toggle="tooltip">
                                                        <i class="fa fa-copy"></i> </asp:LinkButton>
                                                <span></span>
                                                <%-- <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" OnClientClick="return ComfirmDelete(event,this)"
                                                                        CausesValidation="false" data-original-title="Delete" data-toggle="tooltip" data-placement="top">
                                                    <i class="fa fa-trash"></i></asp:LinkButton>--%></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Assign To Sales Rep">
                                        <HeaderTemplate>
                                            
                                            <%--<span class="euroradio">                                      --%>
                                            <div class="checkBox checkBoxList"> 
                                                <asp:CheckBox ID="chksalesheader" runat="server" AutoPostBack="true" OnCheckedChanged="chksalesheader_CheckedChanged" />
                                                 <label for='<%# ((GridViewRow)Container).FindControl("chksalesheader").ClientID %>' runat="server" id="lblchk1">
                                                
                                                </label>
                                            </div>
                                          <%--  </span>--%>
                                       
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="Label11" runat="server" Width="150px">
                                                    <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                        AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">Select Sales Rep</asp:ListItem>
                                                    </asp:DropDownList></asp:Label>--%>
                                                <div class="checkBox checkBoxList"> 
                                                    <asp:Label ID="Label22" runat="server" Width="30px">
                                                     <asp:CheckBox ID="chksales" runat="server" />
                                                    <label for='<%# ((GridViewRow)Container).FindControl("chksales").ClientID %>' runat="server" id="lblchk">
                                                    </label>
                                                    </asp:Label>
                                                </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            

                                              <%--  <label for='<%# ((GridViewRow)Container).FindControl("chkheader").ClientID %>' runat="server" id="lblchk123">
                                                                   <asp:CheckBox ID="chkheader" runat="server" AutoPostBack="true" OnCheckedChanged="chkheader_CheckedChanged" />
                                                        <span class="text">&nbsp;</span>
                                                         </label>--%>


                                                       <%--   <span class="checkbox checkbox-info">     --%>
                                                       <div class="checkBox checkBoxList"> 
                                                            <asp:CheckBox ID="chkdeletewhole" runat="server"  AutoPostBack="true" OnCheckedChanged="chkdeletewhole_CheckedChanged" />   
                                                            <label for='<%# ((GridViewRow)Container).FindControl("chkdeletewhole").ClientID %>' runat="server" id="lblchk1223">
                                                           </label>
                                                        </div>
                                                    <%-- </span>--%>
                                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                         <div class="checkBox checkBoxList"> 
                                            <asp:Label ID="Label234" runat="server" Width="30px">
                                                    <asp:CheckBox ID="cbxDelete" runat="server" />
                                                <label for='<%# ((GridViewRow)Container).FindControl("cbxDelete").ClientID %>' runat="server" id="lblchk452643">
                                                </label>
                                            </asp:Label>
                                        </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle />
                                <%--<PagerTemplate>--%>
                                <%--<asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                    <div class="pagination">
                      <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                      <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                      <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                      <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                      <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                      <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                    </div>
                  </PagerTemplate>
                  <PagerStyle CssClass="paginationGrid" />
                  <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />--%>
                            </asp:GridView>
                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table card-footer" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td class="showEntryBtmBox">
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Display</asp:ListItem>
                                                </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                        <div class="pagination paginationGrid">
                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>
                        <div style="float: right;" runat="server" visible="false">
                            <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" OnClick="lnkdelete_Click" CssClass="btn btn-info margin10">Delete</asp:LinkButton>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>
    
        <cc1:ModalPopupExtender ID="ModalPopupExtenderInvPay" runat="server" BackgroundCssClass="modalpopupbackground"
            CancelControlID="ibtnCancelActive" DropShadow="true" PopupControlID="modal_danger"
            OkControlID="btnOKActive" TargetControlID="btnNULLInvPay">
        </cc1:ModalPopupExtender>

    <div id="modal_danger"  class="modal_popup addDocumentPopup" style="display: none;" >

                <div class="modal-dialog " style="width:500px;">
                    <div class=" modal-content ">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel1"><i class="pe-7s-upload popupIcon"></i>Upload Leads</h4>
                                  <div>
                            <button id="btnClose" runat="server" type="button" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal" onclick="btnClose_Click">Close</button>
                        </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="formainline formGrid">
                                
                                    <div class="form-group">
                                        <label class="control-label">
                                        Upload Excel File <span class="symbol required"></span>
                                        </label>
                                        <label class="custom-fileupload">
                                            <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-block;"  class="custom-file-input"/>
                                            <span class="custom-file-control form-control-file form-control"></span>
                                            <span class="btnbox">Upload file</span>
                                        </label>
                                    
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                            ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>
                                         </div>
                                        
                                         <div class="textRight form-group">
                                            <asp:Button class="btn largeButton smallbtn greenBtn addwhiteicon btnaddicon" ID="btnAdd" runat="server" OnClick="btnSave_Click"
                                                    Text="Add" ValidationGroup="success" />
                                            <asp:Button class="btn largeButton smallbtn blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel" />
                                        </div>
                                   
                                
                                    
                                <div class="row alignCntr">
                                <label class="control-label col-sm-5">Excel Sheet Format</label>
                                <asp:HyperLink ID="hypsubscriber" class="pull-right" CssClass="btn btn-info btngray" runat="server" NavigateUrl="~/userfiles/Lead/format/Lead.xls"><i class="fa fa-download"></i> Download
                                </asp:HyperLink>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
    </div>


     <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalpopupbackground"
            CancelControlID="ibtnCancelActive" DropShadow="true" PopupControlID="modal_danger1"
            OkControlID="btnOKActive" TargetControlID="btnNULLInvPay">
        </cc1:ModalPopupExtender>

    <div id="modal_danger1"  class="modal_popup addDocumentPopup" style="display: none;" >

                <div class="modal-dialog" style="width: 500px;">
                    <div class=" modal-content ">
                        <div class="color-line printorder "></div>
                        <div class="modal-header printorder">
                            <div class="modalHead">
                                <h4 class="modal-title" id="myModalLabel12"><i class="pe-7s-upload popupIcon"></i>Upload Gov. Status</h4>
                                <div>
                                    <button id="Button1" runat="server" type="button" class="btn largeButton redBtn btncancelicon btnClose" data-dismiss="modal" onclick="Button1_Click">Close</button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="formainline formGrid">
                                
                                    <div class="form-group">
                                        <label class="control-label">
                                        Upload Excel File <span class="symbol required"></span>
                                        </label>
                                        <label class="custom-fileupload">
                                            <asp:FileUpload ID="FileUpload2" runat="server" Style="display: inline-block;"  class="custom-file-input"/>
                                            <span class="custom-file-control form-control-file form-control"></span>
                                            <span class="btnbox">Upload file</span>
                                        </label>
                                    
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FileUpload2"
                                            ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                            Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                            ControlToValidate="FileUpload2" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <div class="textRight">
                                        <asp:Button  ID="btnAddGovStatus" class="btn largeButton smallbtn greenBtn addwhiteicon btnaddicon" runat="server" OnClick="btnAddGovStatus_Click"  Text="Add"/>
                                        <asp:Button class="btn largeButton smallbtn blueBtn btncancelicon" ID="Button3" runat="server" OnClick="btnCancel_Click" CausesValidation="false" Text="Cancel" />
                                        </div>
                                    
                                
                                    
                              
                            </div>
                        </div>

                    </div>
                </div>
    </div>

    <asp:HiddenField runat="server" ID="hdncountdata" />

    <script type="text/javascript">
        function pageLoad(sender, args) { if (!args.get_isPartialLoad()) { $addHandler(document, "keydown", onKeyDown); } }
        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                $find("<%=MPECustomerName.ClientID%>").hide();
                $find("<%=ModalPopupExtender1.ClientID%>").hide();
            }
        }
    </script>
    <%--   <script type="text/javascript">
        $(document).ready(function () {
            gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });
        }
    </script>--%>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
        #modal_danger
        {
            margin-top:25px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=.] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView2] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=DataList1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=DataList1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

</asp:Content>
