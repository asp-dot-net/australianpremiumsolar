<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Theme="admin" CodeFile="pvdstatus.aspx.cs" Inherits="admin_adminfiles_upload_pvdstatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }


        function redirect() {
            location.href = 'page.aspx';
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <section class="row m-b-md">
                      </section>
    
                <div class="col-sm-12 minhegihtarea">

                    <div class="page-body headertopbox">
                        <h5 class="row-title"><i class="pe-7s-cloud-upload icon"></i>Upload PVD Status</h5>
                        <div>
                            <asp:HyperLink ID="hypNavi" class="pull-right" CssClass="btn btn-info btngray" runat="server" NavigateUrl="~/admin/adminfiles/company/stctracker.aspx"><i class="fa fa-backward"></i> Back</asp:HyperLink>
                        </div>
                    </div>

                    <div class="page-body padtopzero">

                        <div class="contactsarea">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server">
                                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful.</strong>
                                </div>
                                <div class="alert alert-danger" id="PanError" runat="server">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed. </strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="finaladdupdate addEmployee formGrid whitebg">
                                        <div class="centerFormTable">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <h4 class="formHeading">
                                                                Upload Excel File <span class="symbol required"></span>
                                                            </h4>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 textRight">
                                                    <div class="form-group">
                                                        <asp:HyperLink ID="hypsubscriber" class="pull-right" CssClass="btn btn-info btngray" runat="server" NavigateUrl="~/userfiles/PVDStatus/format/PVDStatus.xls"><i class="fa fa-download"></i> Download</asp:HyperLink> Excel Sheet Format
                                                        <%--<asp:HyperLink ID="hypsubscriber" runat="server" NavigateUrl="~/userfiles/PVDStatus/format/PVDStatus.xls" 
                                                            Target="_blank"> <asp:LinkButton ID="Image2" style="width:140px" runat="server"   CausesValidation="false"  class="btn btn-labeled btn-blue"><i class="btn-label fa fa-download"></i>Download
                                                                           </asp:LinkButton></asp:HyperLink>
                                                        Excel Sheet format. --%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <%--<span class="file-input btn btn-azure btn-file">--%>
                                                        <label class="custom-fileupload"> 
                                                            <asp:FileUpload ID="FileUpload1" runat="server" class="custom-file-input" />
                                                            <span class="custom-file-control form-control-file form-control"></span>
                                                            <span class="btnbox">Upload file</span>
                                                        </label>

                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FileUpload1"
                                                                ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                                Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>

                                                        <%--/span>--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>

                                                        

                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="dividerLine"></div>
                                                </div>
                                                <div class="col-md-12 textRight">
                                                    <span>
                                                        <asp:Button CssClass="btn largeButon greenBtn redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                            Text="Add" ValidationGroup="success" />



                                                        <asp:Button CssClass="btn largeButon blueBtn btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                            CausesValidation="false" Text="Cancel" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </ContentTemplate>
                <Triggers>
                  <asp:PostBackTrigger ControlID="btnAdd" />
               </Triggers>
    </asp:UpdatePanel>
</asp:Content>
