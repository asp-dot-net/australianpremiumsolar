﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin/templates/MasterPageAdmin.master" CodeFile="Profile.aspx.cs" Inherits="admin_Profile_Profile" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/Profile/Contact.ascx" TagName="contact" TagPrefix="uc1" %>
<%@ Register Src="~/includes/Profile/Setting.ascx" TagName="setting" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="pe-7s-user icon"></i>Update Profile
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
        </h5>

    </div>


    <div class="animate-panel">

        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="searchfinal">

                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-container">
                            <div class="profile-header row">
                                <div class="col-sm-4">
                                <div class="profileSection">
                                    <div clas="row">
                                        <div class="col-sm-3 text-center">
                                            <asp:Image runat="server" class="avatar" ID="EmpImage" Style="width: 90px; height: 90px;" AlternateText="..." />
                                        </div>
                                        <div class="col-sm-9 profile-info" style="border-right: none">
                                            <div class="header-information">
                                                <h3 class="font-bold m-b-none m-t-none">
                                                    <asp:Literal runat="server" ID="lblfullname"></asp:Literal></h3>
                                                <p>
                                                    <asp:Literal runat="server" ID="ltemail"></asp:Literal>
                                                </p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="contactDetails">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                
                                                <div class="profile-contacts mgmbtm">
                                                <h4 class="formHeading"><i class="pe-7s-call"></i>Contact Details</h4>
                                                    <div class="contact-info">
                                                        <ul id="divcell" runat="server">
                                                            <li><asp:Label ID="lblphone" Visible="false" runat="server"></asp:Label></li>
                                                            <li><asp:Label ID="lblcell" Visible="false" runat="server"></asp:Label></li>
                                                            <li><asp:Label ID="lblEmail" Visible="false" runat="server"></asp:Label></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                
                                                <div class="profile-contacts">
                                                <h4 class="formHeading"><i class="pe-7s-map-marker"></i>Location</h4>
                                                    <div class="contact-info">
                                                        <span>
                                                            <strong>Office : </strong>
                                                            Level 6/140 Creek Street,
                                                            <br />
                                                            Brisbane QLD 4000
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                
                                <div class="col-sm-8">
                                <div class="profileSection formGrid">
                                    <%-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3540.097712722561!2d153.02522671465!3d-27.46621718289238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b915a1d3ab8c271%3A0x66b67afdfa77bb3f!2s6%2F140+Creek+St%2C+Brisbane+City+QLD+4000%2C+Australia!5e0!3m2!1sen!2sus!4v1516085876454" class="map" frameborder="2" style="border: 0" allowfullscreen></iframe> --%>
                                    <div id="settings" class="whitebg boxPadding personalInfoForm">
                                        <form role="form">
                                            <div class="form-title">
                                                <h4 class="formHeading">Personal Information</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">First Name</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                            <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" placeholder="First Name" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtEmpFirst" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Last Name</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                            <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" placeholde="Last Name" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtEmpLast" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Mobile</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-mobile"></i></span></div>
                                                            <asp:TextBox ID="txtEmpMobile" placeholder="Phone" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpMobile" SetFocusOnError="true" Display="Dynamic"
                                                            ValidationGroup="profile" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)" ForeColor="Red"
                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtEmpMobile" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Phone</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-phone"></i></span></div>
                                                            <asp:TextBox ID="txtEmpPhone" placeholder="Phone" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtEmpPhone"
                                                            ValidationGroup="profile" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                            ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Employee Title</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                            <asp:TextBox ID="txtEmpTitle" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Employee Initials</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                            <asp:TextBox ID="txtEmpInitials" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                            
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtEmpInitials" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Email Address</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-at"></i></span></div>
                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="200" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Employee Name</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user"></i></span></div>
                                                            <asp:TextBox ID="txtEmpNicName" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                            ControlToValidate="txtEmpNicName" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-title">
                                                <h4 class="formHeading">Password</h4>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Password</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                                            <asp:TextBox ID="NewPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            
                                                        </div>
                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="regConfirmPassword" runat="server" ValidationGroup="profile"
                                                            ControlToValidate="NewPassword" ErrorMessage="Minimum password length is 5" ValidationExpression=".{5}.*" />
                                                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                            ErrorMessage="This value is required." CssClass="comperror" ToolTip="New Password is required." Display="Dynamic"
                                                            ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Confirm Password</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key"></i></span></div>
                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" placeholder="Password Again" TextMode="Password" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            
                                                        </div>
                                                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                            ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                            ValidationGroup="profile"></asp:CompareValidator>
                                                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                            ErrorMessage="This value is required." CssClass="comperror" ToolTip="Confirm New Password is required." Display="Dynamic"
                                                            ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-title">
                                                <h4 class="formHeading">Contact Information</h4>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">City</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-map-marker"></i></span></div>
                                                            <asp:TextBox ID="txtcity" placeholder="City" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">State</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-map-marker"></i></span></div>
                                                            <asp:TextBox ID="txtstate" placeholder="State" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Address</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-map-marker"></i></span></div>
                                                            <asp:TextBox ID="txtaddress" placeholder="Address" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                            
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <label class="control-label col-sm-3">Post Code</label>
                                                        <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-map-marker"></i></span></div>
                                                            <asp:TextBox ID="txtpostcode" placeholder="Post Code" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="dividerLine"></div>
                                            
                                            <div class="textRight">
                                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="  btn largeButton greenBtn btnsaveicon" Text="Update" ValidationGroup="profile" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                </div>
                                
                            </div>
                            
                                <div class="row dispnone">
                                    <div>
                                        <div class="hpanel">
                                            <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                                            <div class="bodymianbg">
                                                <div>
                                                    <div class="col-md-12" id="divright" runat="server">
                                                        <section class="scrollable bg-white">

                                                            <ul class="nav nav-tabs m-b-n-xxs bg-light">
                                                                <%--<li class="active"> <a href="#activities" data-toggle="tab" class="m-l">Activities<span class="badge bg-primary badge-sm m-l-xs">10</span></a> </li>--%>
                                                                <li class="active"><a href="#bio" data-toggle="tab" class="m-l ">Contact</a> </li>
                                                                <li> </li>
                                                            </ul>
                                                            <div class="tab-content tab_white marginleftright15">

                                                                <div class="tab-pane wrapper-lg active" id="bio">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>





                                                                <div class="tab-pane wrapper-lg" id="edit">

                                                                    
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </asp:Panel>
    </div>




    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
        });
    </script>


</asp:Content>
