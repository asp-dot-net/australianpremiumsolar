﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/loginmaster.master" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
.toast-error2{background-color:#fff!important;
 background: #fff!important; padding:10px; border:1px solid #eaeaea; border-radius:3px;
    border-left: 6px solid #e74c3c!important;}
</style>

    <div class="app-container app-theme-white body-tabs-shadow loginPage">
                <div class="app-container">
                <div class="h-100">
                    <div class="h-100 no-gutters row">
                        <div class="d-none d-lg-block col-lg-4">
                            <div class="position-relative h-100 d-flex justify-content-center align-items-center" tabindex="-1">
                                <div class="leftSectionImage"></div>
                                <div class="app-logo"></div>
                                <div class="leftSectionContent"><h1>Welcome to Australian Premium Solar</h1>
                                    <p>The ultimate solar company for solar installation and wholesale</p></div>

                                <ul class="copyRightFooter">
                                    <li>&copy; Copyright APS</li>
                                    <li><a href="#">Privacy</a><a href="#">Legal</a><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="h-100 position-relative d-flex justify-content-center align-items-center col-md-12 col-lg-8">
                         <h6 class="signUpLink">Don't have an account yet? <a href="javascript:void(0);" class="text-primary">Sign up!</a></h6>
                            <div class="mx-auto app-login-box col-sm-12 col-md-6 col-lg-5">
                                
                                <h4 class="text-center signIn">Forgot Password</h4>
                               
                                <div>
                                    <asp:Panel ID="pan" CssClass="hpanel" runat="server" >
                                        <div class="hpanel">
                        
                                        <asp:Panel runat="server" ID="panel11">
                                            <div class="panel-body">
                                                <asp:Panel ID="PanSuccess" runat="server" Visible="false" CssClass="alert alert-success">

                                            <strong>Success ! </strong>
                                            &nbsp;
                                            <asp:Label ID="lblSuccess" runat="server"
                                                Text="Your password has been sent to you. Please check your Email."></asp:Label>
                                        </asp:Panel>
                                        <asp:Panel ID="Panfail" runat="server" Visible="false">
                                            <div class="toast-error2">
                                                <strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text="Failure !"></asp:Literal>
                                                    <%-- Success !--%>
                                                </strong>
                                                <br />
                                                <asp:Label ID="Label1" runat="server"
                                                    Text="Invalid Username"></asp:Label>
                                                <%-- password_change--%>
                                            </div>
                                        </asp:Panel>
                                        <table class="loginForm w300" runat="server" id="tabform">
                                            <tr>
                                                <td>
                                                    <div class="username">
                                                    <asp:TextBox CssClass="form-control" ID="UserName" runat="server" MaxLength="100" autofocus placeholder="Username"></asp:TextBox>
                                                    </div>

                                                    <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
                                                            ErrorMessage=" Enter Valid E-Mail Address" Display="Dynamic" SetFocusOnError="True"
                                                            ValidationGroup="PasswordRecovery1" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                        </asp:RegularExpressionValidator>--%>
                                                    <asp:RequiredFieldValidator ID="reqvalUserName" runat="server"
                                                        ControlToValidate="UserName" CssClass="requiredfield" ErrorMessage="* Required"
                                                        Display="dynamic" ValidationGroup="PasswordRecovery1"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <div class="d-flex justify-content-between btnRow">
                                                    <asp:LinkButton ID="SubmitButton" CausesValidation="true" CssClass="btn btn-primary btn-lg" runat="server"
                                                        OnClick="SubmitButton_Click1" CommandName="Submit" ValidationGroup="PasswordRecovery1" 
                                                        Text=" Submit " />
                                                    <asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="btn btn-danger btn-lg" runat="server"
                                                        OnClick="LinkButton1_Click"
                                                        Text=" Cancel " />
                                                </div>
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                        </asp:Panel>
                                        </div>
                                    </asp:Panel>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>


</asp:Content>

