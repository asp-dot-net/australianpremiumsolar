using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblPromoOffer
	{
		public string PromoOffer;
		public string Seq;
		public string Active;
		public string Description;

	}


public class ClstblPromoOffer
{
	public static SttblPromoOffer tblPromoOffer_SelectByPromoOfferID (String PromoOfferID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_SelectByPromoOfferID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@PromoOfferID";
		param.Value = PromoOfferID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblPromoOffer details = new SttblPromoOffer();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.PromoOffer = dr["PromoOffer"].ToString();
			details.Seq = dr["Seq"].ToString();
			details.Active = dr["Active"].ToString();
			details.Description = dr["Description"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblPromoOffer_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblPromoOffer_Insert ( String PromoOffer, String Seq, String Active, String Description)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@PromoOffer";
		param.Value = PromoOffer;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Description";
		param.Value = Description;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	
    }
	public static bool tblPromoOffer_Update (string PromoOfferID, String PromoOffer, String Seq, String Active, String Description)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@PromoOfferID";
		param.Value = PromoOfferID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PromoOffer";
		param.Value = PromoOffer;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Description";
		param.Value = Description;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblPromoOffer_InsertUpdate (Int32 PromoOfferID, String PromoOffer, String Seq, String Active, String Description)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@PromoOfferID";
		param.Value = PromoOfferID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PromoOffer";
		param.Value = PromoOffer;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Description";
		param.Value = Description;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblPromoOffer_Delete (string PromoOfferID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblPromoOffer_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@PromoOfferID";
		param.Value = PromoOfferID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static DataTable tblPromoOfferGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblPromoOfferGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}