using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblProjectOnHold
	{
		public string ProjectOnHold;
		public string Active;
		public string Seq;

	}


public class ClstblProjectOnHold
{
	public static SttblProjectOnHold tblProjectOnHold_SelectByProjectOnHoldID (String ProjectOnHoldID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectOnHold_SelectByProjectOnHoldID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectOnHoldID";
		param.Value = ProjectOnHoldID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblProjectOnHold details = new SttblProjectOnHold();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ProjectOnHold = dr["ProjectOnHold"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblProjectOnHold_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectOnHold_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblProjectOnHold_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectOnHold_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
	public static int tblProjectOnHold_Insert ( String ProjectOnHold, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectOnHold_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectOnHold";
		param.Value = ProjectOnHold;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblProjectOnHold_Update (string ProjectOnHoldID, String ProjectOnHold, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectOnHold_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectOnHoldID";
		param.Value = ProjectOnHoldID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectOnHold";
		param.Value = ProjectOnHold;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblProjectOnHold_Delete (string ProjectOnHoldID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectOnHold_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectOnHoldID";
		param.Value = ProjectOnHoldID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}


    public static int tblProjectOnHold_Exists(string ProjectOnHold)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectOnHold_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHold";
        param.Value = ProjectOnHold;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblProjectOnHold_ExistsById(string ProjectOnHold, string ProjectOnHoldID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectOnHold_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHold";
        param.Value = ProjectOnHold;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectOnHoldID";
        param.Value = ProjectOnHoldID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblProjectOnHold_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectOnHold_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectOnHold_GetDataBySearch(string alpha, string Active )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectOnHold_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}