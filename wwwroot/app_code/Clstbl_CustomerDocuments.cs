using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct Sttbl_CustomerDocuments
	{
		public string DocumentName;
        public string IsActive;

	}


public class Clstbl_CustomerDocuments
{
	public static Sttbl_CustomerDocuments tbl_CustomerDocuments_SelectById (String Id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_SelectById";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@Id";
		param.Value = Id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 Sttbl_CustomerDocuments details = new Sttbl_CustomerDocuments();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.DocumentName = dr["DocumentName"].ToString();
            details.IsActive = dr["IsActive"].ToString();


        }
		// return structure details
		return details;
	}

	public static DataTable tbl_CustomerDocuments_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tbl_CustomerDocuments_Insert ( String DocumentName,String Active)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@DocumentName";
		param.Value = DocumentName;
		param.DbType = DbType.String;
		param.Size = 500;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tbl_CustomerDocuments_Update (string Id, String DocumentName, String Active)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@Id";
		param.Value = Id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@DocumentName";
		param.Value = DocumentName;
		param.DbType = DbType.String;
		param.Size = 500;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tbl_CustomerDocuments_InsertUpdate (Int32 Id, String DocumentName)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@Id";
		param.Value = Id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@DocumentName";
		param.Value = DocumentName;
		param.DbType = DbType.String;
		param.Size = 500;
		comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tbl_CustomerDocuments_Delete (string Id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tbl_CustomerDocuments_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@Id";
		param.Value = Id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}

    public static DataTable tblcustomerdocumentsGetDataBySearch(string alpha, string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblcustomerdocumentsGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    
}