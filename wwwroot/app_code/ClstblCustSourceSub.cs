using System;
using System.Data;
using System.Data.Common;

public struct SttblCustSourceSub
{
    public string CustSourceSubID;
    public string CustSourceID;
    public string CustSourceSub;
    public string Active;
    public string Seq;
    public string DISCOMID;
    public string DivisionName;
    public string FullName;
    public string StateActive;
    public string IsActive;

}


public class ClstblCustSourceSub
{
    public static SttblCustSourceSub tblCustSourceSub_SelectByCustSourceSubID(String CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCustSourceSubID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSourceSub = dr["CustSourceSub"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
       
        // return structure details
        return details;
    }

    public static SttblCustSourceSub tblDivision_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.DISCOMID = dr["ElecDistributorID"].ToString();
            details.DivisionName = dr["DivisionName"].ToString();
            details.IsActive = dr["IsActive"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }

    public static SttblCustSourceSub tblState_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblState_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StateId";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            //details.DISCOMID = dr["ElecDistributorID"].ToString();
            details.FullName = dr["FullName"].ToString();
            details.StateActive = dr["Active"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }

    public static SttblCustSourceSub tblCustSourceSub_SelectByCustSourceSub(String CustSourceSub)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCustSourceSub";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSourceSubID = dr["CustSourceSubID"].ToString();
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSourceSub = dr["CustSourceSub"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }


    public static DataTable tblCustSourceSub_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblDivision_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblState_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblState_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustSourceSub_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustSourceSub_SelectByCSId(string CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCSId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblCustSourceSub_Insert(String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblDivision_Insert(String DiscomId, String DivisionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (DiscomId != string.Empty)
            param.Value = DiscomId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DivisionName";
        param.Value = DivisionName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblState_Insert( String DivisionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblState_Insert";

        DbParameter param = comm.CreateParameter();
       

        param = comm.CreateParameter();
        param.ParameterName = "@statefullname";
        param.Value = DivisionName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblCustSourceSub_Update(string CustSourceSubID, String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblDivision_Update(string DivisionId, String DiscomId, String DivisionName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = DivisionId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ElecDistributorID";
        if (DiscomId != string.Empty)
            param.Value = DiscomId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DivisionName";
        param.Value = DivisionName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblState_Update(string DivisionId, String FullName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblState_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StateId";
        param.Value = DivisionId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FullName";
        param.Value = FullName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static string tblCustSourceSub_InsertUpdate(Int32 CustSourceSubID, String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustSourceSub_Delete(string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblDivision_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblState_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblState_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StateId";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblCustSourceSub_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustSourceSub_GetDataBySearch(string alpha ,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblDivision_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tblStates_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStates_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static int tblCustSourceSub_Exists(string CustSourceSub)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblDivision_Exists(string DivisionName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DivisionName";
        param.Value = DivisionName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblstates_Exists (string FullName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblstates_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FullName";
        param.Value = FullName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblCustSourceSub_ExistsById(string CustSourceSub, string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblDivision_ExistsById(string DivisionName, string DivisionId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDivision_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DivisionName";
        param.Value = DivisionName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DivisionId";
        param.Value = DivisionId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblStates_ExistsById(string FullName, string StateId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStates_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FullName";
        param.Value = FullName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StateId";
        param.Value = StateId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
}