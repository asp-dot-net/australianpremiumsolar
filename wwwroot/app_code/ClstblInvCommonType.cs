using System;
using System.Data;
using System.Data.Common;

public struct SttblInvCommonType
{
    public string InvCommonType;
    public string Active;

}

public class ClstblInvCommonType
{
    public static SttblInvCommonType tblInvCommonType_SelectByInvCommonTypeID(String InvCommonTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_SelectByInvCommonTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvCommonTypeID";
        param.Value = InvCommonTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblInvCommonType details = new SttblInvCommonType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.InvCommonType = dr["InvCommonType"].ToString();
            details.Active = dr["Active"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblInvCommonType_Select(string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblInvCommonType_Insert(String InvCommonType, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvCommonType";
        param.Value = InvCommonType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblInvCommonType_Update(string InvCommonTypeID, String InvCommonType, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvCommonTypeID";
        param.Value = InvCommonTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvCommonType";
        param.Value = InvCommonType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblInvCommonType_InsertUpdate(Int32 InvCommonTypeID, String InvCommonType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvCommonTypeID";
        param.Value = InvCommonTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvCommonType";
        param.Value = InvCommonType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblInvCommonType_Delete(string InvCommonTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvCommonTypeID";
        param.Value = InvCommonTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}