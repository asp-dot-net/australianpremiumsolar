using System;
using System.Data;
using System.Data.Common;

public class ClstInstallations
{
    public static DataTable tblProjects_GetData_ByInstallBookingDate(string date, string alpha, string InstallState, string Installer, string userid,string formbay)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetData_ByInstallBookingDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@date";
        param.Value = date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbay";
        if (formbay != string.Empty)
            param.Value = formbay;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjects_GetData_ByInstallBookingDate_BookingAmPm(string date, string ProjectID, string alpha, string InstallState, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetData_ByInstallBookingDate_BookingAmPm";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@date";
        param.Value = date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_AllInstaller_ByBookingDate(string date, string userid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_AllInstaller_ByBookingDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@date";
        param.Value = date;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_GetProjectId()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_GetProjectId";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContacts_GetInstaller()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_GetInstaller";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjects_SelectByInstaller()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByInstaller";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}