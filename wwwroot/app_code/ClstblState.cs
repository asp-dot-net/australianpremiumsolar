﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblState
/// </summary>
public class ClstblState
{
    public ClstblState()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static int tblStatesExistInsert(String State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStatesExistInsert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tbl_DistrictNameExistInsert(String StateId, string DistrictName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_DistrictNameExistInsert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StateId";
        if (!string.IsNullOrEmpty(StateId))
            param.Value = StateId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";
        param.Value = DistrictName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tbl_TalukaNameExistInsert(String DistrictId, string TalukaName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TalukaNameExistInsert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DistrictId";
        if (!string.IsNullOrEmpty(DistrictId))
            param.Value = DistrictId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tbl_cityExistInsert(String TalukaId, string CityName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_cityExistInsert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TalukaId";
        if (!string.IsNullOrEmpty(TalukaId))
            param.Value = TalukaId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CityName";
        param.Value = CityName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tblStates_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStates_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tblGetChnlPartner()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblGetChnlPartner";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_DistrictName_ByStateName(string State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_DistrictName_ByStateName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@State";
        param.Value = State;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_DistrictName()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GetAllDistrictNames";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_TalukaName()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GetAllTalukaNames";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_GetAllCities()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GetAllCities";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_GetAllArea()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_GetAllArea";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_TalukaName_ByStateName(string DistrictName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TalukaName_ByStateName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";
        param.Value = DistrictName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_city_ByTalukaName(string TalukaName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_city_ByTalukaName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_Area_ByCityName(string city)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Area_ByCityName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CityaName";
        param.Value = city;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}