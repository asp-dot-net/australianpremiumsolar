using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblRoofTypes
{
    public string RoofType;
    public string Active;
    public string Seq;
    public string Variation;

}


public class ClstblRoofTypes
{
    public static SttblRoofTypes tblRoofTypes_SelectByRoofTypeID(String RoofTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_SelectByRoofTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        param.Value = RoofTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblRoofTypes details = new SttblRoofTypes();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.RoofType = dr["RoofType"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.Variation = dr["Variation"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblRoofTypes_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblRoofTypes_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblRoofTypes_SelectByRoofType(string RoofType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_SelectByRoofType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        param.Value = RoofType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblRoofTypes_Insert(String RoofType, String Active, String Seq, String Variation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        param.Value = RoofType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        if (Variation != string.Empty)
            param.Value = Variation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblRoofTypes_Update(string RoofTypeID, String RoofType, String Active, String Seq, String Variation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        param.Value = RoofTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        param.Value = RoofType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        if (Variation != string.Empty)
            param.Value = Variation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblRoofTypes_InsertUpdate(Int32 RoofTypeID, String RoofType, String Active, String Seq, String Variation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        param.Value = RoofTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofType";
        param.Value = RoofType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Variation";
        param.Value = Variation;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblRoofTypes_Delete(string RoofTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypes_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofTypeID";
        param.Value = RoofTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblRoofTypesNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypesNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblRoofTypesExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypesExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }


    public static DataTable tblRoofTypesGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofTypesGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblRoofTypesGetDataBySearch(string alpha,string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblRoofTypesGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}