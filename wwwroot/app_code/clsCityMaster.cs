﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsTalukaMaster
/// </summary>
public class clsCityMaster
{
    public clsCityMaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public struct SttblCity
    {

        public string CityName;
        public string Taluka;
        public string IsActive;

    }
    public static DataTable tbl_TalukaNames_Select()

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_TalukaNames_Select";

        DataTable result = new DataTable();

        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_City_Select()

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_City_Select";

        DataTable result = new DataTable();

        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblCity_Exists(string CityName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCity_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CityName";
        param.Value = CityName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }


    public static int tblCity_Insert (String TalukaID, String CityName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCity_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "TalukaID";
        if (TalukaID != string.Empty)
            param.Value = TalukaID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CityName";
        param.Value = CityName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblCity_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCity_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tblCity_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCity_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static SttblCity tblCity_SelectByID (String ID)

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCity_SelectByID";

        DbParameter param = comm.CreateParameter();

        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCity details = new SttblCity();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Taluka = dr["TalukaID"].ToString();
            details.CityName = dr["CityName"].ToString();
            details.IsActive = dr["Active"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }

    public static int tbl_City_ExistsById(string CityName, string Id, string TalukaId )
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_City_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CityName";
        param.Value = CityName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TalukaId";
        param.Value = TalukaId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tbl_City_Update (string Id, String TalukaId, String CityName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_City_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TalukaId";
        if (TalukaId != string.Empty)
            param.Value = TalukaId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CityName";
        param.Value = CityName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

}
