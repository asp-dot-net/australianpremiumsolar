using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblOrderData
	{
		public string OrderNo;
		public string SerialNo;

	}


public class ClstblOrderData
{
	public static SttblOrderData tblOrderData_SelectByid (String id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_SelectByid";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblOrderData details = new SttblOrderData();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.OrderNo = dr["OrderNo"].ToString();
			details.SerialNo = dr["SerialNo"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblOrderData_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblOrderData_Insert ( String OrderNo, String SerialNo)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@OrderNo";
		param.Value = OrderNo;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SerialNo";
		param.Value = SerialNo;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblOrderData_Update (string id, String OrderNo, String SerialNo)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@OrderNo";
		param.Value = OrderNo;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SerialNo";
		param.Value = SerialNo;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblOrderData_InsertUpdate (Int32 id, String OrderNo, String SerialNo)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@OrderNo";
		param.Value = OrderNo;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SerialNo";
		param.Value = SerialNo;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblOrderData_Delete (string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblOrderData_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
}