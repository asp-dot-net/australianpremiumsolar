using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblStockOrders
{
    public string OrderNumber;
    public string StockCategoryID;
    public string StockItemID;
    public string OrderQuantity;
    public string DateOrdered;
    public string ExpectedDelivery;
    public string Delivered;
    public string Cancelled;
    public string OrderedBy;
    public string ReceivedBy;
    public string Notes;
    public string CompanyLocationID;
    public string CustomerID;
    public string BOLReceived;
    public string upsize_ts;
    public string ManualOrderNumber;
    public string ActualDelivery;

    public string CompanyLocation;
    public string Vendor;
    public string OrderedName;
    public string VendorInvoiceNo;
}

public struct SttblStockOrderItems
{
    public string StockOrderID;
    public string StockItemID;
    public string OrderQuantity;
    public string StockOrderItem;
    public string StockCode;
    public string StockLocation;


}
public struct sttblStockSerialNo
{
    public string id;
    public string StockItemID;
    public string StockLocationID;
    public string SerialNo;
    public string Pallet;
    public string StockOrderID;
}
public class ClstblStockOrders
{
    public static SttblStockOrders tblStockOrders_SelectByStockOrderID(String StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockOrders details = new SttblStockOrders();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OrderNumber = dr["OrderNumber"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.DateOrdered = dr["DateOrdered"].ToString();
            details.ExpectedDelivery = dr["ExpectedDelivery"].ToString();
            details.Delivered = dr["Delivered"].ToString();
            details.Cancelled = dr["Cancelled"].ToString();
            details.OrderedBy = dr["OrderedBy"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.BOLReceived = dr["BOLReceived"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ManualOrderNumber = dr["ManualOrderNumber"].ToString();
            details.ActualDelivery = dr["ActualDelivery"].ToString();

            details.CompanyLocation = dr["CompanyLocation"].ToString();
            details.Vendor = dr["Vendor"].ToString();
            details.OrderedName = dr["OrderedName"].ToString();
            details.VendorInvoiceNo = dr["VendorInvoiceNo"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblStockOrders_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectBySearch(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectBySearchExcel(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string State, string stockitem)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearchExcel";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrders_SelectByLocationID(string Delivered,  string CompanyLocationID, string DeliveryOrderStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_SelectBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeliveryOrderStatus";
        if (CompanyLocationID != string.Empty)
            param.Value = DeliveryOrderStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockOrders_Insert(String DateOrdered, String OrderedBy, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ManualOrderNumber, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DateOrdered";
        param.Value = DateOrdered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderedBy";
        param.Value = OrderedBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        if (ManualOrderNumber != string.Empty)
            param.Value = ManualOrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblStockOrders_Update_OrderNumber(string StockOrderID, String OrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_OrderNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        param.Value = OrderNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblStockOrders_Update_Cancelled(string StockOrderID, String Cancelled)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Cancelled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Cancelled";
        param.Value = Cancelled;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblStockOrders_Update_Delivered(string StockOrderID, string ReceivedBy, string Delivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_Delivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        param.Value = ReceivedBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        param.Value = Delivered;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_Update_ActualDelivery(string StockOrderID, string ActualDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_ActualDelivery";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualDelivery";
        if (ActualDelivery != string.Empty)
            param.Value = ActualDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update(string StockOrderID, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ManualOrderNumber, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualOrderNumber";
        if (ManualOrderNumber != string.Empty)
            param.Value = ManualOrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrders_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblStockOrders_Update_VendorInvoiceNo(string StockOrderID, String VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Update_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID(string StockOrderID, string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select_ByStockOrderIDAndStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;       
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            //result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockOrders_Exits_VendorInvoiceNo(string VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_Exits_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static int tblStockOrders_ExistsByIdVendorInvoiceNo(string StockOrderID, string VendorInvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_ExistsByIdVendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@VendorInvoiceNo";
        param.Value = VendorInvoiceNo;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    /////////////////////////////////////////////////	tblStockOrderItems

    public static SttblStockOrderItems tblStockOrderItems_SelectByStockOrderItemID(String StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_SelectByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockOrderItems details = new SttblStockOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockOrderID = dr["StockOrderID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.StockOrderItem = dr["StockOrderItem"].ToString();
            details.StockCode = dr["StockCode"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblStockOrderItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_Select_ByStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Select_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblStockOrderItems_Insert(String StockOrderID, String StockItemID, String OrderQuantity, String StockOrderItem, String StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItem";
        if (StockOrderItem != string.Empty)
            param.Value = StockOrderItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        if (StockLocation != string.Empty)
            param.Value = StockLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblStockOrderItems_Update(string StockOrderItemID, String StockOrderID, String StockItemID, String OrderQuantity, String StockOrderItem, String StockCode, String StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        param.Value = OrderQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItem";
        param.Value = StockOrderItem;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCode";
        param.Value = StockCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblStockOrderItems_Delete(string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        param.Value = StockOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblStockOrderItems_DeleteStockOrderID(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_DeleteStockOrderID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblStockOrderItems_SelectQty(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_SelectQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockOrdersReport_Search(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location, string StockCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrdersReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrdersReport_ByLocSearch(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrdersReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblStockOrders_getStockItemState(string StockItemID, String State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrders_getStockItemState";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    ///ORDER INSTALLED REPORT

    public static DataTable tblOrderInstalledReport_Search(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblOrderInstalledReport_ByLocSearch(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblOrderInstalledReport_ByInstalled(string CustomerID, string startdate, string enddate, string StockItemID, String Location, string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblOrderInstalledReport_ByInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblStockOrderItems_whole_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_whole_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SStockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblStockSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockSerialNo_InsertWithOrderID(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_InsertWithOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }


    public static bool tblStockSerialNo_Delete(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblStockSerialNoByProjNo_Insert(String ProjectNumber, String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByProjNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockSerialNoByInvNo_Insert(String InvoiceNo, String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByInvNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);


        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static int tblStockDeductSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String ProjectNumber, String DeductedBy, string DeductedDate, string InventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedBy";
        if (DeductedBy != string.Empty)
            param.Value = DeductedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedDate";
        if (DeductedDate != string.Empty)
            param.Value = DeductedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        if (InventoryHistoryId != string.Empty)
            param.Value = InventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblStockOrderItems_QtySum(string StockOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_QtySum";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        param.Value = StockOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNo_SearchByPallet(string StockItemID, string StockLocationID, string Pallet)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_SearchByPallet";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNoByProjNo_SearchByProjNo(string ProjectNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByProjNo_SearchByProjNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockSerialNoByInvNo_SearchByInvNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoByInvNo_SearchByInvNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockDeductSerialNo_SelectBy_ProjectNumber_RFlag(string ProjectNumber, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_SelectBy_ProjectNumber_RFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblStockDeductSerialNo_UpdateRevert(string SerialNo, String Pallet, string ProjectNumber, string RevertDate, string RevertedBy, string InventoryHistoryId, string RevertInventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockDeductSerialNo_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        param.Value = RevertDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertedBy";
        param.Value = RevertedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        param.Value = InventoryHistoryId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertInventoryHistoryId";
        param.Value = RevertInventoryHistoryId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tblStockRevertSerialNo_Insert(String ProjectNumber, String SerialNo, String Pallet, string DeductInventoryHistoryId, string RevertInventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockRevertSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductInventoryHistoryId";
        if (DeductInventoryHistoryId != string.Empty)
            param.Value = DeductInventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertInventoryHistoryId";
        if (RevertInventoryHistoryId != string.Empty)
            param.Value = RevertInventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblStockSerialNoCount_ByStockOrderID(string StockOrderID, string StockCategoryID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNoCount_ByStockOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockCategoryID";
        if (StockCategoryID != string.Empty)
            param.Value = StockCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;        
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblStockOrderItems_QtySum_ByStockOrderItemID(string StockOrderID, string StockOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockOrderItems_QtySum_ByStockOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockOrderID";
        if (StockOrderID != string.Empty)
            param.Value = StockOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);       

        param = comm.CreateParameter();
        param.ParameterName = "@StockOrderItemID";
        if (StockOrderItemID != string.Empty)
            param.Value = StockOrderItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblStockSerialNo_Exist(String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Exist";

        DbParameter param = comm.CreateParameter();        
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        
        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static sttblStockSerialNo tblStockSerialNo_Getdata_BySerialNo(String SerialNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockSerialNo_Getdata_BySerialNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (!string.IsNullOrEmpty(SerialNo))
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        sttblStockSerialNo details = new sttblStockSerialNo();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.id = dr["id"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockLocationID = dr["StockLocationID"].ToString();
            details.SerialNo = dr["SerialNo"].ToString();
            details.Pallet = dr["Pallet"].ToString();
            details.StockOrderID = dr["StockOrderID"].ToString();         

}
        // return structure details
        return details;
    }

}