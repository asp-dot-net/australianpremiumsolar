﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClstblDocuments
/// </summary>
/// 
public struct SttblDocuments
{
    public string FileName;
}
public class ClstblDocuments
{

    public static DataTable tblCustomerDocumentsDetail_SelectByUserIdCust(String userid, String CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerDocumentsDetail_SelectByUserIdCust";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmpDocumentsDetail_SelectByUserIdEmp(String userid, String EmpId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpDocumentsDetail_SelectByUserIdEmp";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        param.Value = EmpId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static int tbldocuments_ExistsByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbldocuments_ExistsByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static int tbldocuments_ExistsByEmpID(string EmpId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbldocuments_ExistsByEmpID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        param.Value = EmpId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tblCustomerDocuments_Insert(String CustomerID, String DocumentId, String Number, String FileUpload, String UploadedBy , DateTime UploadedDateTime)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerDocuments_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentId";
        if (DocumentId != string.Empty)
            param.Value = DocumentId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Number";
        if (Number != string.Empty)
            param.Value = Number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileUpload";
        if (FileUpload != string.Empty)
            param.Value = FileUpload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadedBy";
        if (UploadedBy != string.Empty)
            param.Value = UploadedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadedDateTime";

        if (UploadedDateTime.ToString() != string.Empty)
            param.Value = UploadedDateTime;

        if (UploadedDateTime != DateTime.Now)
           param.Value = UploadedDateTime;

        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }


    public static int InserQuotePayment(String ProjectID, String ProjectNumber, String UserId, String PayAmt, String Paymethod, String PayReceipt, String PayDoc, String Datetime)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "InserQuotePayment";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayAmt";
        if (PayAmt != string.Empty)
            param.Value = PayAmt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Paymethod";
        if (Paymethod != string.Empty)
            param.Value = Paymethod;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayReceipt";
        if (PayReceipt != string.Empty)
            param.Value = PayReceipt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDoc";
        if (PayDoc != string.Empty)
            param.Value = PayDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateTime";
        if (Datetime != string.Empty)
            param.Value = Datetime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }


    public static int tblEmployeeDocuments_Insert(String EmpId, String DocumentId, String Number, String FileUpload, string UserId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeDocuments_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpID";
        if (EmpId != string.Empty)
            param.Value = EmpId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DocumentId";
        if (DocumentId != string.Empty)
            param.Value = DocumentId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Number";
        if (Number != string.Empty)
            param.Value = Number;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileUpload";
        if (FileUpload != string.Empty)
            param.Value = FileUpload;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (DocumentId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UploadedOn";
        if (FileUpload != string.Empty)
            param.Value = DateTime.Now;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblDocuments_SelectActive(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDocuments_SelectActive";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblDocuments_SelectActive_Project(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDocuments_SelectActive_Project";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmpDocuments_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpDocuments_SelectActive";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@EmpId";
        //if (EmpId != string.Empty)
        //    param.Value = EmpId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblDocuments_Other()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDocuments_Other";

        //DbParameter param = comm.CreateParameter();
        //param.ParameterName = "@EmpId";
        //if (EmpId != string.Empty)
        //    param.Value = EmpId;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.Int32;
        //comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static bool tblCustomerDocuments_UpdateFileName(string CustomerId, string FileName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerDocuments_UpdateFileName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerId";
        param.Value = CustomerId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileName";
        if (FileName != string.Empty)
            param.Value = FileName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblEmpDocuments_UpdateFileName(string EmpId, string FileName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpDocuments_UpdateFileName";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpId";
        param.Value = EmpId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FileName";
        if (FileName != string.Empty)
            param.Value = FileName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttblDocuments tblCustomerDocList_SelectByID(String Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerDocList_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblDocuments details = new SttblDocuments();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.FileName = dr["FileUpload"].ToString();
        }
        // return structure details
        return details;
    }
    public static SttblDocuments tblEmployeeDocumentsDetail_SelectByID(String Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployeeDocumentsDetail_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblDocuments details = new SttblDocuments();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.FileName = dr["FileUpload"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblDocuments tblEmpDocList_SelectByID(String Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpDocList_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblDocuments details = new SttblDocuments();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.FileName = dr["FileUpload"].ToString();
        }
        // return structure details
        return details;
    }

    public static bool tblCustomerdocument_Delete(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerdocument_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblEmpdocument_Delete(string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpdocument_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tblCustomerdocument_DeleteByCusomerProjectID(string customerID, string documentId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerdocument_DeleteByCusomerProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerID";
        param.Value = customerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@documentId";
        param.Value = documentId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblGetCustomerDocuments(string CustomerID, string docId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblGetCustomerDocuments";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustomerDocumentsDetail_GetFiles(string customerId,string Doc_Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustomerDocumentsDetail_GetFiles";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@customerID";
        if (customerId != string.Empty)
            param.Value = customerId;

        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Document_Id";
        if (Doc_Id != string.Empty)
            param.Value = Doc_Id;

        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }




}