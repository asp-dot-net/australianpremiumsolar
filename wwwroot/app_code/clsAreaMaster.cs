﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsTalukaMaster
/// </summary>
public class clsAreaMaster
{
    public clsAreaMaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public struct SttblArea
    {

        public string AreaName;
        public string IsActive;

    }


    public static DataTable tblArea_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblArea_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static int tblArea_Exists(string FullName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FullName";
        param.Value = FullName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblArea_Insert( String CityId ,String AreaName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_Insert";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@cityid";
        param.Value = CityId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@areaname";
        param.Value = AreaName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblArea_ExistsById(string FullName, string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@AreaName";
        param.Value = FullName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tblArea_Update(string AreaId, String AreaName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = AreaId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AreaName";
        param.Value = AreaName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttblArea tblArea_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblArea details = new SttblArea();
        if (table.Rows.Count > 0)

        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            //details.DISCOMID = dr["ElecDistributorID"].ToString();
            details.AreaName = dr["AreaName"].ToString();
            details.IsActive = dr["Active"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }
    public static bool tblArea_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblArea_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

}
