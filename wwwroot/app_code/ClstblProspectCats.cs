using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblProspectCats
{
    public string ProspectCat;
    public string Active;
    public string Seq;
    public string ProspectCatABB;

}


public class ClstblProspectCats
{
    public static SttblProspectCats tblProspectCats_SelectByProspectCatID(String ProspectCatID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_SelectByProspectCatID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatID";
        param.Value = ProspectCatID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProspectCats details = new SttblProspectCats();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProspectCat = dr["ProspectCat"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();
            details.ProspectCatABB = dr["ProspectCatABB"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblProspectCats_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblProspectCats_Insert(String ProspectCat, String Active, String Seq, String ProspectCatABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProspectCat";
        param.Value = ProspectCat;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatABB";
        if (ProspectCatABB != String.Empty)
            param.Value = ProspectCatABB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblProspectCats_Update(string ProspectCatID, String ProspectCat, String Active, String Seq, String ProspectCatABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatID";
        param.Value = ProspectCatID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProspectCat";
        param.Value = ProspectCat;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatABB";
        if (ProspectCatABB != String.Empty)
            param.Value = ProspectCatABB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblProspectCats_InsertUpdate(Int32 ProspectCatID, String ProspectCat, String Active, String Seq, String ProspectCatABB)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatID";
        param.Value = ProspectCatID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProspectCat";
        param.Value = ProspectCat;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatABB";
        if (ProspectCatABB != String.Empty)
            param.Value = ProspectCatABB;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 5;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProspectCats_Delete(string ProspectCatID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCats_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProspectCatID";
        param.Value = ProspectCatID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int ProspectCategoryNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ProspectCategoryNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int ProspectCategoryNameExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ProspectCategoryNameExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblProspectCategoryGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProspectCategoryGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProspectCategoryGetDataBySearch(string alpha, string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblProspectCategoryGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}