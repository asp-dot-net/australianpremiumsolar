
using System;
using System.Data;
using System.Configuration;
using System.Data.Common;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//STRUCTURE FOR PRODUCTMASTER TABLE
public struct StSiteConfiguration
{
	public int id;
	public string level;
	public string Symbol;
	public string CategoryLarge;
	public string ProductLarge;
	public string BannerLarge;
	public string CategoryMedium;
	public string ProductMedium;
	public string BannerMedium;
	public string CategorySmall;
	public string ProductSmall;
	public string BannerSmall;
	public string Currencydigit;
    public string GST;

}
public class ClsAdminSiteConfiguration
{
	public ClsAdminSiteConfiguration()
	{
	}
	//-------------------Get Data Start------------------------------
	public static DataTable AdminSiteConfigurationGetData()
	{
		// get a configured DbCommand object
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationGetData";
		// execute the stored procedure
		return DataAccess.ExecuteSelectCommand(comm);
	}
	public static StSiteConfiguration AdminSiteConfigurationGetDataStructById(string id)
	{

		DbCommand comm = DataAccess.CreateCommand();

		comm.CommandText = "AdminSiteConfigurationGetDataStructById";

		DbParameter param = comm.CreateParameter();

		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		StSiteConfiguration details = new StSiteConfiguration();

		if (table.Rows.Count > 0)
		{

			DataRow dr = table.Rows[0];

			details.id = Convert.ToInt32(dr["id"]);
			details.level = dr["level"].ToString();
			details.Currencydigit = dr["Currencydigit"].ToString();
			details.Symbol = dr["Symbol"].ToString();
			details.CategoryLarge = dr["CategoryLarge"].ToString();
			details.CategoryMedium = dr["CategoryMedium"].ToString();
			details.CategorySmall = dr["CategorySmall"].ToString();
			details.ProductLarge = dr["ProductLarge"].ToString();
			details.ProductMedium = dr["ProductMedium"].ToString();			
			details.ProductSmall = dr["ProductSmall"].ToString();
			details.BannerLarge = dr["BannerLarge"].ToString();
			details.BannerMedium = dr["BannerMedium"].ToString();	
			details.BannerSmall = dr["BannerSmall"].ToString();
            details.GST = dr["GST"].ToString();
		}
		// return product details
		return details;

	}
	public static bool AdminSiteConfigurationUpdateData(int id, int level, int Currencydigit,string Symbol,
		int CategoryLarge,int CategoryMedium,int CategorySmall,int ProductLarge,int ProductMedium,int ProductSmall,
		int BannerLarge,int BannerMedium,int BannerSmall)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateData";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@level";
		param.Value = level;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@Currencydigit";
		param.Value = Currencydigit;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@Symbol";
		param.Value = Symbol;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@CategoryLarge";
		param.Value = CategoryLarge;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@CategoryMedium";
		param.Value = CategoryMedium;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@CategorySmall";
		param.Value = CategorySmall;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProductLarge";
		param.Value = ProductLarge;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProductMedium";
		param.Value = ProductMedium;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProductSmall";
		param.Value = ProductSmall;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@BannerLarge";
		param.Value = BannerLarge;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@BannerMedium";
		param.Value = BannerMedium;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@BannerSmall";
		param.Value = BannerSmall;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateCategoryImageSize(string CategoryLarge, string CategoryMedium, string CategorySmall, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateCategoryImageSize";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@CategoryLarge";
		param.Value = CategoryLarge;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@CategoryMedium";
		param.Value = CategoryMedium;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@CategorySmall";
		param.Value = CategorySmall;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateProductmageSize(string ProductLarge, string ProductMedium, string ProductSmall, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateProductmageSize";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@ProductLarge";
		param.Value = ProductLarge;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProductMedium";
		param.Value = ProductMedium;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProductSmall";
		param.Value = ProductSmall;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateCurrencydigit(string Currencydigit, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateCurrencydigit";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@Currencydigit";
		param.Value = Currencydigit;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateSymbol(string Symbol, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateSymbol";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@Symbol";
		param.Value = Symbol;
		param.DbType = DbType.String;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateCategoryFolder(string CategoryFolder, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateCategoryFolder";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@CategoryFolder";
		param.Value = CategoryFolder;
		param.DbType = DbType.String;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
	public static bool AdminSiteConfigurationUpdateProductFolder(string ProductFolder, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateProductFolder";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@ProductFolder";
		param.Value = ProductFolder;
		param.DbType = DbType.String;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}

	public static bool AdminSiteConfigurationUpdateFeaturedProductSize(string ProductSize, string id)
	{
		DbCommand comm = DataAccess.CreateCommand();
		// set the stored procedure name
		comm.CommandText = "AdminSiteConfigurationUpdateFeaturedProductSize";

		// create a new parameter
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@id";
		param.Value = id;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		// create a new parameter
		param = comm.CreateParameter();
		param.ParameterName = "@ProductSize";
		param.Value = ProductSize;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// result will represent the number of changed rows
		int result = -1;
		try
		{
			// execute the stored procedure
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		// result will be 1 in case of success 
		return (result != -1);
	}
    public static bool AdminSiteConfiguration_update_GST(string id,string GST)
    {
        DbCommand comm = DataAccess.CreateCommand();
        // set the stored procedure name
        comm.CommandText = "AdminSiteConfiguration_update_GST";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // create a new parameter
        param = comm.CreateParameter();
        param.ParameterName = "@GST";
        param.Value = GST;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // result will represent the number of changed rows
        int result = -1;
        try
        {
            // execute the stored procedure
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        // result will be 1 in case of success 
        return (result != -1);
    }
	//----------------------ChangeCurrency Start-----------------------------
	public static string ChangeCurrency(string Price)
	{
		StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
		int Currencydigit = Convert.ToInt32(st.Currencydigit);
		string Currency = st.Symbol;
		//Price = string.Format("{0:0.000 Rs}", Convert.ToDecimal(Price));
		Price = Convert.ToString(Math.Round(Convert.ToDecimal(Price), Currencydigit)) + Currency;
		return Price;
	}
    //----------------------ChangeCurrency End-----------------------------

    #region RandomNumber

    public static String RandomNumber()
    {
        String pwd = "";
        int length = 0;
        int index = 0;
        int numericIndex = -1;
        int upperCaseIndex = -1;
        Random rnd = new Random();
        const int MINLENGTH = 4;
        const int MAXLENGTH = 4;
        const String allowedChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const String upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const String numericChars = "0123456789";
        //Length of your password
        length = rnd.Next(MINLENGTH, MAXLENGTH);

        // You generate a password of the desired length
        for (int i = 0; i < length; i++)
        {
            // Generate an index that smaller than the size of your allowed chars
            index = rnd.Next(0, allowedChars.Length);

            pwd += allowedChars[index];
        }

        ////*********************************************************
        // We make sure that there is at least one numeric
        // Replace one random char by a numeric
        numericIndex = rnd.Next(0, pwd.Length);

        // Generate a numeric, delete one char and replace it with a numeric
        index = rnd.Next(0, numericChars.Length);
        pwd = pwd.Remove(numericIndex, 1);
        pwd = pwd.Insert(numericIndex, numericChars[index].ToString());
        ////*********************************************************


        ////*********************************************************
        // We make sure that there is at least one uppercase
        // Replace one random char by a numeric
        upperCaseIndex = rnd.Next(0, pwd.Length);

        // We make sure our uppercase index is different
        // from our numeric index or we will overwrite our
        // only numeric value with our uppercase value
        while (upperCaseIndex == numericIndex)
        {
            upperCaseIndex = rnd.Next(0, pwd.Length);
        }

        // Generate a numeric, delete one char and replace it with a numeric
        index = rnd.Next(0, upperCaseChars.Length);
        pwd = pwd.Remove(upperCaseIndex, 1);
        pwd = pwd.Insert(upperCaseIndex, upperCaseChars[index].ToString());
        ////*********************************************************

        return pwd;
    }
    #endregion


    #region RandomToken

    public static String RandomStrongToken()
    {
        String pwd = "";
        int length = 0;
        int index = 0;
        int numericIndex = -1;
        int upperCaseIndex = -1;
        Random rnd = new Random();
        const int MINLENGTH = 8;
        const int MAXLENGTH = 8;
        const String allowedChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@$!";
        const String upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const String numericChars = "0123456789";
        //Length of your password
        length = rnd.Next(MINLENGTH, MAXLENGTH);

        // You generate a password of the desired length
        for (int i = 0; i < length; i++)
        {
            // Generate an index that smaller than the size of your allowed chars
            index = rnd.Next(0, allowedChars.Length);

            pwd += allowedChars[index];
        }

        ////*********************************************************
        // We make sure that there is at least one numeric
        // Replace one random char by a numeric
        numericIndex = rnd.Next(0, pwd.Length);

        // Generate a numeric, delete one char and replace it with a numeric
        index = rnd.Next(0, numericChars.Length);
        pwd = pwd.Remove(numericIndex, 1);
        pwd = pwd.Insert(numericIndex, numericChars[index].ToString());
        ////*********************************************************


        ////*********************************************************
        // We make sure that there is at least one uppercase
        // Replace one random char by a numeric
        upperCaseIndex = rnd.Next(0, pwd.Length);

        // We make sure our uppercase index is different
        // from our numeric index or we will overwrite our
        // only numeric value with our uppercase value
        while (upperCaseIndex == numericIndex)
        {
            upperCaseIndex = rnd.Next(0, pwd.Length);
        }

        // Generate a numeric, delete one char and replace it with a numeric
        index = rnd.Next(0, upperCaseChars.Length);
        pwd = pwd.Remove(upperCaseIndex, 1);
        pwd = pwd.Insert(upperCaseIndex, upperCaseChars[index].ToString());
        ////*********************************************************

        return pwd;
    }
    #endregion

    #region============== Get Date Code Start

    public static DateTime GetDate_Aus(DateTime Date)
    {
        try
        {
            Date = Convert.ToDateTime(Date).AddHours(14);
        }
        catch
        {
        }
        return Date;
    }

    public static string GetDate(string date)
    {
        string retval = "";
        try
        {
            retval = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(date));
        }
        catch
        {
        }
        return retval;
    }

    public static string GetDateDisplay(string date)
    {
        string retval = "";
        try
        {
            retval = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(date));
        }
        catch
        {
        }

        return retval;
    }

    public static string GetDateDisplayWithDay(string date)
    {
        string retval = "";
        try
        {
            retval = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(date));
        }
        catch
        {
        }

        return retval;
    }


    public static string GetDateDisplay_(string date)
    {
        string retval = "";
        try
        {
            retval = string.Format("{0:dd/MM/yy}", Convert.ToDateTime(date));
        }
        catch
        {
        }

        return retval;
    }
    public static string GetDateAndTime(string date)
    {
        string retval = "";
        try
        {
            retval = string.Format("{0:dd MMM yyyy hh:mm tt}", Convert.ToDateTime(date));
        }
        catch
        {
        }

        return retval;

    }
    #endregion
}

