﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Security;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CompanyService" in code, svc and config file together.
public class CompanyService : ICompanyService
{
    public class GetCustomer
    {
        public decimal total { get; set; }
    }

    public class CustomerType
    {
        public string CustTypeID { get; set; }
        public string CustType { get; set; }
    }

    public class UnitTypes
    {
        public string UnitType { get; set; }
    }
    public class PostalUnitType
    {
        public string UnitType { get; set; }
    }

    public class StreetTypes
    {
        public string StreetType { get; set; }
        public string StreetCode { get; set; }
    }

    public class CustomerSource
    {
        public string CustSourceID { get; set; }
        public string CustSource { get; set; }
    }

    public class CustomerSubSource
    {
        public string CustSourceSubID { get; set; }
        public string CustSourceSub { get; set; }
    }

    public class GetState
    {
        public string State { get; set; }
    }

    public class company
    {
         public string CustomerID { get; set; }
        public string Customer { get; set; }
        public string CompanyNumber { get; set; }
        public string StreetAddress { get; set; }
        public string Location { get; set; }
        public string ContMobile { get; set; }
        public string CustPhone { get; set; }
        public string AssignedTo { get; set; }
        public string CustType { get; set; }
        public string ContEmail { get; set; }
        public string CustNotes { get; set; }
        public string CustSource { get; set; }
        public string CustSubSource { get; set; }
        public string ResCom { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Area { get; set; }
        public string Country { get; set; }
        

        public string StreetCity { get; set; }
        public string StreetState { get; set; }
        public string StreetPostCode { get; set; }
        public string PostalAddress { get; set; }

        public string PostalCity { get; set; }
        public string PostalState { get; set; }
        public string PostalPostCode { get; set; }
        public string CustWebSiteLink { get; set; }
        public string CustAltPhone { get; set; }
        public string CustSourceSub { get; set; }
        public string CustFax { get; set; }
        public string D2DAppDate { get; set; }
        public string D2DAppTime { get; set; }
        public string D2DEmpName { get; set; }
        
    }

    public class CustomerImage
    {
        public string CustImageID { get; set; }
        public string CustomerID { get; set; }
        public string Imagename { get; set; }
    }

    public string PrintData(string html)
    {
        string returnHTML = html;
        return returnHTML;
    }
    public string CheckUserStatus(string Uname, string Pass)
    {
        string UserID = "";

        if (Membership.ValidateUser(Uname, Pass))
        {
            int exist = 0;
            UserID = Membership.GetUser(Uname).ProviderUserKey.ToString();
        }
        return UserID;
    }

    public string CheckUserByRole(string Userid)
    {
        string Data = "";

            int exist = 0;
            exist = ClstblCustomers.aspnet_Users_ExistsByRole(Userid);

            if (exist == 0)
            {
                Data = "";
            }
            else
            {
                Data = "1";
            }
            return Data;
    }
    #region Dashboard Customer Count
    public GetCustomer[] GetCustomerCount(string userid)
    {
        DataTable dt1 = new DataTable();
        List<GetCustomer> custcounts = new List<GetCustomer>();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;
        dt1 = ClstblCustomers.tblCustomers_getcount(EmployeeID);
        if (dt1.Rows.Count > 0)
        {
            GetCustomer data1 = new GetCustomer();
          
            data1.total = Convert.ToDecimal(dt1.Rows[0]["total"].ToString());
            custcounts.Add(data1);
        }
        return custcounts.ToArray();
    }
    #endregion


    public company[] GetCompany(string userid, string CompanyNo, string State, string Customer, string streetaddress, string streetcity, string startdate, string enddate, string startindex)
    {
        List<company> companylist = new List<company>();
        DataTable dt = new DataTable();
        string datagrid = GetCustomerString(userid, CompanyNo, State, Customer, streetaddress, streetcity, startdate, enddate, startindex);


        dt = ClstblCustomers.query_execute(datagrid);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            company data = new company();
            data.Customer = dt.Rows[i]["Customer"].ToString();
            data.Location = dt.Rows[i]["Location"].ToString();
            data.StreetAddress = dt.Rows[i]["StreetAddress"].ToString();
            data.ContMobile = dt.Rows[i]["ContMobile"].ToString();
            data.AssignedTo = dt.Rows[i]["AssignedTo"].ToString();
            data.CompanyNumber = dt.Rows[i]["CompanyNumber"].ToString();
            data.ContEmail = dt.Rows[i]["ContEmail"].ToString();
            data.ContMobile = dt.Rows[i]["ContMobile"].ToString();
            data.CustNotes = dt.Rows[i]["CustNotes"].ToString();
            data.CustomerID = dt.Rows[i]["CustomerID"].ToString();
            data.CustPhone = dt.Rows[i]["CustPhone"].ToString();
            data.CustType = dt.Rows[i]["CustType"].ToString();
            data.CustSource = dt.Rows[i]["CustSource"].ToString();

            string CustomerID = dt.Rows[i]["CustomerID"].ToString();
            SttblCustomers stcust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            data.CustSubSource = stcust.CustSourceSub;

            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);

            if (st.ResCom == "1")
            {
                data.ResCom = "Res";
            }
            else
            {
                data.ResCom = "Com";
            }
            companylist.Add(data);
        }
        return companylist.ToArray();
    }


    public string GetCustomerString(string userid, string CompanyNo, string State, string Customer, string streetaddress, string streetcity, string startdate, string enddate, string startindex)
    {
        string myval = "";

        string CustTypeID = "";
        string CustSourceID = "";
        string ResCom = "";
        string Area = "";
        string CustSourceSubID = "";
        string postalcode = "";
        string custompagesize = "25";

        if (CompanyNo == "0" || CompanyNo == "undefined")
        {
            CompanyNo = "";
        }

        if (State == "0" || State == "undefined")
        {
            State = "";
        }

        if (Customer == "0" || Customer == "undefined")
        {
            Customer = "";
        }

        if (streetaddress == "0" || streetaddress == "undefined")
        {
            streetaddress = "";
        }

        if (streetcity == "0" || CompanyNo == "undefined")
        {
            streetcity = "";
        }

        if (startdate == "0" || startdate == "undefined")
        {
            startdate = "";
        }

        if (enddate == "0" || enddate == "undefined")
        {
            enddate = "";
        }



        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;

        string datagrid = "", Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "", data2 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        DataTable dtcount = ClstblCustomers.query_execute(Count);

        if (dtcount.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                //Response.Write(dtEmpType.Rows.Count + "<=emp<br/>");
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                //Response.Write(dtSalesTeamID.Rows.Count + "<=<br/>");
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmpType=" + EmpType1 + ")) and (C.EmployeeID in (select EmployeeID from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))))";
            }
        }

        if (Roles.IsUserInRole("Maintenance") || Roles.IsUserInRole("BookInstallation") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC"))
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and  C.CustTypeID!=3 and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
            }
            else
            {
                Employeeid = "";
            }
        }
        else
        {
            if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager"))
            {
                Employeeid = "";
                data2 = "and  C.CustTypeID!=3 and C.CustomerID in (select CustomerID from tblProjects where ProjectStatusID!=2)";
            }
            else
            {
                Employeeid = st.EmployeeID;
            }
        }

        string empid = "";
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
        {
            empid = "and (C.EmployeeID in (select EmployeeID from tblEmployees where EmployeeID!=1 and userid in (select userid from aspnet_membership where islockedout=0)and EmployeeID in (select Employeeid from tblEmployeeTeam where Convert(nvarchar(200),SalesTeamID) in (select  * from Split('" + SalesTeamID1 + "',',')))))";
        }
        else
        {
            empid = "and (C.EmployeeID='" + Employeeid + "' or '" + Employeeid + "'=0)";
        }
        //string userid, string CustTypeID, string CustSourceID, string ResCom, string Area, string CustSourceSubID, string CompanyNo, string State,string Customer,string postalcode,string streetaddress,string streetcity,string startdate,string enddate                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          and ('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "'='')  and ('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') " + data1 + "" + data2 + " order by C.CustomerID DESC
        datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location,C.CustomerID,C.CompanyNumber,C.EmployeeID,C.Customer,C.CustPhone,(select case when EmpLast is null then EmpFirst else EmpFirst+' '+EmpLast end from tblEmployees where EmployeeID=C.EmployeeID)as AssignedTo,(select CustType from tblCustType where CustTypeID=C.CustTypeID)as CustType,(select CustSource from tblCustSource where CustSourceID=C.CustSourceID)as CustSource,C.StreetAddress,C.CustAltPhone,(select top 1 ContMobile from tblContacts where CustomerID=C.CustomerID) as ContMobile,(select top 1 ContEmail from tblContacts where CustomerID=C.CustomerID) as ContEmail,C.CustNotes,(select count(CustomerID) from tblCustomers)as countdata from tblCustomers C where (C.CustTypeID='" + CustTypeID + "' or  '" + CustTypeID + "'=0) and(C.CustSourceID='" + CustSourceID + "' or '" + CustSourceID + "'=0) and(C.ResCom='" + ResCom + "' or '" + ResCom + "'=0) and (C.Area='" + Area + "' or '" + Area + "'=0) and (C.CustSourceSubID='" + CustSourceSubID + "' or '" + CustSourceSubID + "'=0) " + empid + " and (C.CompanyNumber='" + System.Security.SecurityElement.Escape(CompanyNo) + "' or '" + System.Security.SecurityElement.Escape(CompanyNo) + "'=0) and (C.StreetState like '%'+'" + State + "'+'%'  or  '" + State + "'='') and (C.Customer like '%'+'" + System.Security.SecurityElement.Escape(Customer) + "'+'%' or '" + System.Security.SecurityElement.Escape(Customer) + "'='')  and (C.StreetPostCode like '%'+'" + System.Security.SecurityElement.Escape(postalcode) + "'+'%'  or '" + System.Security.SecurityElement.Escape(postalcode) + "'='') and (C.StreetAddress like '%'+'" + System.Security.SecurityElement.Escape(streetaddress) + "'+'%'  or '" + System.Security.SecurityElement.Escape(streetaddress) + "'='') and (C.StreetCity like '%'+'" + System.Security.SecurityElement.Escape(streetcity) + "'+'%' or '" + System.Security.SecurityElement.Escape(streetcity) + "'='')  and ('" + System.Security.SecurityElement.Escape(startdate) + "' <= C.CustEntered or '" + System.Security.SecurityElement.Escape(startdate) + "'='')  and ('" + System.Security.SecurityElement.Escape(enddate) + "' >=C.CustEntered  or '" + System.Security.SecurityElement.Escape(enddate) + "'='') " + data1 + "" + data2 + " order by C.CustomerID DESC  OFFSET (" + startindex.ToString() + ") ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
        //OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";


        return datagrid;
    }

    public CustomerType[] GetCustomerType()
    {
        DataTable dt = new DataTable();
        List<CustomerType> CustomerTypelist = new List<CustomerType>();

        dt = ClstblCustType.tblCustType_SelectActive();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CustomerType data = new CustomerType();

            data.CustTypeID = dt.Rows[i]["CustTypeID"].ToString();
            data.CustType = dt.Rows[i]["CustType"].ToString();
            CustomerTypelist.Add(data);
        }
        return CustomerTypelist.ToArray();
    }



    public CustomerSource[] GetCustomerSource()
    {
        DataTable dt = new DataTable();
        List<CustomerSource> CustomerSourcelist = new List<CustomerSource>();

        dt = ClstblCustSource.tblCustSource_SelectbyAsc();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CustomerSource data = new CustomerSource();

            data.CustSourceID = dt.Rows[i]["CustSourceID"].ToString();
            data.CustSource = dt.Rows[i]["CustSource"].ToString();
            CustomerSourcelist.Add(data);
        }
        return CustomerSourcelist.ToArray();
    }

    //public CustomerSubSource[] GetCustomerSubSource(string sourceid)
    //{
    //    DataTable dt = new DataTable();
    //    List<CustomerSubSource> CustomerSubSourcelist = new List<CustomerSubSource>();

    //    dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(sourceid);

    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CustomerSubSource data = new CustomerSubSource();

    //        data.CustSourceSubID = dt.Rows[i]["CustSourceSubID"].ToString();
    //        data.CustSourceSub = dt.Rows[i]["CustSourceSub"].ToString();
    //        CustomerSubSourcelist.Add(data);
    //    }
    //    return CustomerSubSourcelist.ToArray();
    //}

    public GetState[] GetStateData()
    {
        DataTable dt = new DataTable();
        List<GetState> GetStateStatelist = new List<GetState>();

        dt = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            GetState data = new GetState();
            data.State = dt.Rows[i]["State"].ToString();
            GetStateStatelist.Add(data);
        }
        return GetStateStatelist.ToArray();
    }

    public PostalUnitType[] GetPostalUnitType()
    {

        DataTable dt = new DataTable();

        List<PostalUnitType> PostalUnitTypelist = new List<PostalUnitType>();

        dt = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            PostalUnitType data = new PostalUnitType();
            data.UnitType = dt.Rows[i]["UnitType"].ToString();
            PostalUnitTypelist.Add(data);
        }
        return PostalUnitTypelist.ToArray();
    }


    public UnitTypes[] GetUnitType()
    {

        DataTable dt = new DataTable();
        List<UnitTypes> UnitTypelist = new List<UnitTypes>();

        dt = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            UnitTypes data = new UnitTypes();

            data.UnitType = dt.Rows[i]["UnitType"].ToString();
            UnitTypelist.Add(data);
        }
        return UnitTypelist.ToArray();

    }

 

    public StreetTypes[] GetStreetTypes()
    {
        DataTable dt = new DataTable();
        List<StreetTypes> StreetTypeslist = new List<StreetTypes>();

        dt = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            StreetTypes data = new StreetTypes();
            data.StreetType = dt.Rows[i]["StreetType"].ToString();
            data.StreetCode = dt.Rows[i]["StreetCode"].ToString();
            StreetTypeslist.Add(data);
        }
        return StreetTypeslist.ToArray();
    }
    //ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
    //    ddlformbaystreettype.DataMember = "StreetType";
    //    ddlformbaystreettype.DataTextField = "StreetType";
    //    ddlformbaystreettype.DataValueField = "StreetCode";
    //    ddlformbaystreettype.DataBind();


    public string CustomersInsert(string userid, string Salutation, string FirstName, string MiddleName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area, string Nearby, string PostalNearby,  string PostalDistrict)
    {
        {
            string data = "";
            string ResCom = "1";
            string CustTypeID = "3";
            string CustSourceID = "21";
            string CustSourceSubID = "";
            string StreetCity = "";
            string District = "";
            string Taluka = "";
            string StreetState = "";
            string StreetPostCode = "";
            string PostalAddress = "";
            string PostalCity = "";
            string PostalState = "";
            string PostalPostCode = "";


            string CustFax = "";
            string CustNotes = "";
            string CustWebSite = "";
            string CustWebSiteLink = "";
            string Customer = FirstName + " " + MiddleName + "" +LastName;
            string Country = "Australia";
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = st.EmployeeID;
            string CustEnteredBy = st.EmployeeID;
            string BranchLocation = "";
            //string ResCom = false.ToString();
            //if (rblResCom1.Checked == true)
            //{
            //    ResCom = "1";
            //}
            //if (rblResCom2.Checked == true)
            //{
            //    ResCom = "2";
            //}


            //if (rblArea1.Checked == true)
            //{
            //    Area = "1";
            //}
            //if (rblArea2.Checked == true)
            //{
            //    Area = "2";
            //}
            if (Salutation == "0")
            {
                Salutation = "";
            }
            if (FirstName == "0")
            {
                FirstName = "";
            }

            if (MiddleName == "0")
            {
                MiddleName = "";
            }

            if (LastName == "0")
            {
                LastName = "";
            }
            if (Mobile == "0")
            {
                Mobile = "";
            }
            if (Email == "0")
            {
                Email = "";
            }

            if (StreetAddress == "0")
            {
                StreetAddress = "";
            }
            if (Phone == "0")
            {
                Phone = "";
            }
            if (Area == "0")
            {
                Area = "";
            }

            string CustAltPhone = Mobile;


            int success = 0;
            success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress,District,Taluka, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, Phone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);
            int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), Salutation.Trim(), FirstName.Trim(),MiddleName.Trim() ,LastName.Trim(), Mobile, Email, EmployeeID, CustEnteredBy);
            int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");
            //ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);
            //bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
            bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), StreetAddress);

            //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);

            bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), StreetAddress);

            //bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), chkformbayid.Checked.ToString());

            ////----tblcontact contphone update - jigar chokshi
            bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), Phone);

            //if (txtD2DAppDate.Text != string.Empty)
            //{
            //    bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
            //    bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
            //}

            int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
            string LogReport = "";

            if (Convert.ToString(suc) != "")
            {
                int exist = ClstblCustomers.tblContacts_ExistName(FirstName + ' ' + MiddleName + ' ' + LastName);
                if (exist == 1)
                {
                    LogReport = "Update tblCustLogReport set Name='" + FirstName + ' ' + MiddleName + ' ' + LastName + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                }

                int exist2 = ClstblCustomers.tblContacts_ExistMobile(Mobile);
                if (exist2 == 1)
                {
                    LogReport = "Update tblCustLogReport set Mobile='" + Mobile + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                }

                int exist3 = ClstblCustomers.tblContacts_ExistEmail(Email);
                if (exist3 == 1)
                {
                    LogReport = "Update tblCustLogReport set Email='" + Email + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                }

                int exist4 = ClstblCustomers.tblCustomers_ExistPhone(Phone);
                if (exist4 == 1)
                {
                    LogReport = "Update tblCustLogReport set LocalNo='" + Phone + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                }
                ClsProjectSale.ap_form_element_execute(LogReport);
            }
            data += success.ToString();

            return data;
        }
    }
    public string CustomersUpdate(string CustomerID,string userid, string Salutation, string FirstName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area)
    {
        string data = "";
        string ResCom = "1";
        string CustTypeID = "3";
        string CustSourceID = "21";
        string CustSourceSubID = "";
        string StreetCity = "";
        string StreetState = "";
        string StreetPostCode = "";
        string PostalAddress = "";
        string PostalCity = "";
        string PostalState = "";
        string PostalPostCode = "";

       
        string CustFax = "";
        string CustNotes = "";
        string CustWebSite = "";
        string CustWebSiteLink = "";
        string Customer = FirstName + " " + LastName;
        string Country = "Australia";
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;
        string CustEnteredBy = st.EmployeeID;
        string BranchLocation = "";


        if (Salutation == "0")
        {
            Salutation = "";
        }
        if (FirstName == "0")
        {
            FirstName = "";
        }

        if (LastName == "0")
        {
            LastName = "";
        }
        if (Mobile == "0")
        {
            Mobile = "";
        }
        if (Email == "0")
        {
            Email = "";
        }

        if (StreetAddress == "0")
        {
            StreetAddress = "";
        }
        if (Phone == "0")
        {
            Phone = "";
        }
        if (Area == "0")
        {
            Area = "";
        }
        
        bool success = false;
        string CustAltPhone = Mobile;

        bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(CustomerID, StreetAddress);
        success = ClstblCustomers.tblCustomers_Update(CustomerID, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, Phone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, Customer);
       // bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(CustomerID, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
        bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(CustomerID, StreetAddress);
       // bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(CustomerID, chkformbayid.Checked.ToString());
        //----tblcontact contphone update - jigar chokshi
        bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(CustomerID, Phone);
        bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(CustomerID, Mobile);

        string LogReport = "";
        int exist = ClstblCustomers.tblContacts_ExistName(FirstName+ ' ' + LastName);
        if (exist == 1)
        {
            LogReport = "Update tblCustLogReport set Name='" + FirstName + ' ' + LastName + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist2 = ClstblCustomers.tblContacts_ExistMobile(Mobile);
        if (exist2 == 1)
        {
            LogReport = "Update tblCustLogReport set Mobile='" + Mobile + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist3 = ClstblCustomers.tblContacts_ExistEmail(Email);
        if (exist3 == 1)
        {
            LogReport = "Update tblCustLogReport set Email='" + Email + "' WHERE CustomerID='" + CustomerID + "'";
        }
        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(Phone);
        if (exist4 == 1)
        {
            LogReport = "Update tblCustLogReport set LocalNo='" + Phone + "' WHERE CustomerID='" + CustomerID + "'";
        }
        ClsProjectSale.ap_form_element_execute(LogReport);



        data += success.ToString();

        return data;
    }

    public company[] GetCompanyDetailOnEdit(string CustomerID)
    {
        List<company> companylist = new List<company>();
        company objcompany = new company();
      
        string id = CustomerID;
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(id);
        //Response.Write(id);
        //Response.End();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(id);

        objcompany.StreetAddress = st.street_address;
        objcompany.Customer = st.Customer;
        objcompany.CustPhone = st.CustPhone;
        objcompany.Area = st.Area;

        if (dtCont.Rows.Count > 0)
        {
            objcompany.Salutation = dtCont.Rows[0]["ContMr"].ToString();
            objcompany.FirstName = dtCont.Rows[0]["ContFirst"].ToString();
            objcompany.LastName = dtCont.Rows[0]["ContLast"].ToString();
            objcompany.ContEmail = dtCont.Rows[0]["ContEmail"].ToString();
            objcompany.ContMobile = dtCont.Rows[0]["ContMobile"].ToString();
        }
      
       companylist.Add(objcompany);
       return companylist.ToArray();
    }


    public CustomerImage[] GetCustomerImage(string CustomerID)
    {
        List<CustomerImage> custimagelist = new List<CustomerImage>();
        DataTable dtcust = ClstblCustomers.tbl_CustImage_SelectByCustomerID(CustomerID);

        for (int i = 0; i < dtcust.Rows.Count; i++)
        {
            CustomerImage obj = new CustomerImage();
            obj.CustImageID = dtcust.Rows[i]["CustImageID"].ToString();
            obj.CustomerID = dtcust.Rows[i]["CustomerID"].ToString();
            obj.Imagename = dtcust.Rows[i]["Imagename"].ToString();
            custimagelist.Add(obj);
        }
        return custimagelist.ToArray();
    }

    public company[] GetCompanyDetail(string CustomerID,string userid)
    {
        List<company> companylist = new List<company>();
        company objcompany = new company();
        string id = CustomerID;


        //if (!string.IsNullOrEmpty(CustomerID))
        //{
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByCustomerID(CustomerID);
        objcompany.Salutation = stCont.ContMr;
        objcompany.FirstName = stCont.ContFirst;
        objcompany.LastName = stCont.ContLast;
        objcompany.ContEmail = stCont.ContEmail;

        if (stCont.ContMobile != "")
        {
            objcompany.ContMobile = stCont.ContMobile;
        }
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        objcompany.CompanyNumber = st.CompanyNumber;
        objcompany.Customer = st.Customer;

        objcompany.StreetAddress = st.StreetAddress;

        objcompany.StreetCity = st.StreetCity;
        objcompany.StreetState = st.StreetState;
        objcompany.StreetPostCode = st.StreetPostCode;
        // StreetCity StreetState StreetPostCode  PostalAddress PostalCity PostalState PostalPostCode CustWebSiteLink CustAltPhone CustSourceSubID CustSourceSub CustFax D2DAppDate D2DAppTime D2DEmpName
         
        if (st.PostalAddress != "")
        {
            objcompany.PostalAddress = st.PostalAddress;
        }
        if (st.PostalCity != "")
        {
            objcompany.PostalCity= st.PostalCity;
        }
        if (st.PostalState != "")
        {
            objcompany.PostalState = st.PostalState;
        }

        if (st.PostalPostCode != "")
        {
            objcompany.PostalPostCode = st.PostalPostCode;
        }
        if (st.Country != "")
        {
            objcompany.Country = st.Country;
        }

        if (st.CustWebSiteLink != "")
        {
            objcompany.CustWebSiteLink = st.CustWebSiteLink;
        }

        if (st.CustPhone != "")
        {
            objcompany.CustPhone = st.CustPhone;
        }
        if (st.CustAltPhone != "")
        {
            objcompany.CustAltPhone = st.CustAltPhone;
        }
        objcompany.CustType = st.CustType;
        objcompany.CustSource = st.CustSource;

        if (st.CustSourceSubID != "0" && st.CustSourceSubID != "")
        {
            objcompany.CustSourceSub = st.CustSourceSub;
        }
        if (st.CustFax != "")
        {
            objcompany.CustFax = st.CustFax;
        }

        if (st.CustNotes != "")
        {
            objcompany.CustNotes = st.CustNotes;
        }

        if (st.ResCom == "1")
        {
            objcompany.ResCom = "Res";
        }
        else
        {
            objcompany.ResCom = "Com";
        }
        if (st.Area == "1")
        {
            objcompany.Area = "Metro";
        }
        else if (st.Area == "2")
        {
            objcompany.Area = "Regional";
        }


        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (stEmp.SalesTeamID != string.Empty)
        {
            //if (Roles.IsUserInRole("Lead Manager") || Roles.IsUserInRole("leadgen"))
            //{
            objcompany .D2DEmpName= st.D2DEmpName;
            try
            {
                objcompany.D2DAppDate = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.D2DAppDate));
            }
            catch { }
            objcompany.D2DAppTime = st.D2DAppTime;
        //}
        }
       
        companylist.Add(objcompany);
        return companylist.ToArray();
    }
    public string CustomersDelete(string CustomerID)
    {
        string data = "";
        bool success1 = false;
        bool sucess1 = ClstblCustomers.tblCustomers_Delete(CustomerID);
        data += sucess1.ToString();
        return data;
    }
    public string CustImageDelete(string CustImageID)
    {
        string data = "";
        bool success1 = false;
        bool sucess1 = ClstblCustomers.tbl_CustImage_DeleteByCustImageID(CustImageID);
        data += sucess1.ToString();
        return data;
    }
}
