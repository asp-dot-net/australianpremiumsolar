﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsTalukaMaster
/// </summary>
public class clsTalukaMaster
{
    public clsTalukaMaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public struct SttblTaluka
    {

        public string TalukaName;
        public string District;
        public string IsActive;

    }
    public static DataTable tbl_DistrictName_Select()

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_DistrictName_Select";

        DataTable result = new DataTable();

        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_talukaname_Select()

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_talukaname_Select";

        DataTable result = new DataTable();

        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblTaluka_Exists (string TalukaName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }


    public static int tblTaluka_Insert(String DistrictId, String TalukaName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "DistrictID";
        if (DistrictId != string.Empty)
            param.Value = DistrictId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblTaluka_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static bool tblTaluka_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static SttblTaluka tblTaluka_SelectByID(String ID)

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_SelectByID";

        DbParameter param = comm.CreateParameter();

        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblTaluka details = new SttblTaluka();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.District = dr["DistrictId"].ToString();
            details.TalukaName = dr["TalukaName"].ToString();
            details.IsActive = dr["Active"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }

    public static int tblTaluka_ExistsById(string TalukaName, string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tblTaluka_Update (string Id, String DitrictId, String TalukaName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTaluka_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistrictId";
        if (DitrictId != string.Empty)
            param.Value = DitrictId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TalukaName";
        param.Value = TalukaName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

}
