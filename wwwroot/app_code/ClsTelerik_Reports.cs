﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Telerik.Reporting.Services.WebApi;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Web.UI;

//using iTextSharp.text;
//using iTextSharp.text.pdf;

using Spire.Pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf.Exporting;
/// <summary>
/// Summary description for Telerik_reports
/// </summary>
public class ClsTelerik_reports
{

    public ClsTelerik_reports()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void ExportPdf(Telerik.Reporting.Report rpt)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = result.DocumentName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
        //HttpContext.Current.Response.End();
    }

    public static void ExportPdfWithFileName(Telerik.Reporting.Report rpt, string Name)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = Name + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);
        //HttpContext.Current.Response.End();
    }

    public static void SavePDF(Telerik.Reporting.Report rpt, string QuoteID)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = QuoteID + result.DocumentName + "." + result.Extension;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        SiteConfiguration.deleteimage(fileName, "quotedoc");

        //var convertApi = new ConvertApi("<YOUR SECRET HERE>");
        //convertApi.ConvertAsync("pdf", "compress",
        // new ConvertApiFileParam(@"C:\path\to\my_file.pdf")
        //).Result.SaveFiles(@"C:\converted-files\");

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        // if (page != null)
        // {
        // if (page.ImagesInfo != null)
        // {
        // foreach (PdfImageInfo info in page.ImagesInfo)
        // {
        // page.TryCompressImage(info.Index);
        // }
        // }
        // }
        //}
        //doc.SaveToFile(fullPath);
        //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        //SiteConfiguration.deleteimage(fileName, "quotedoc");

    }
    
    public static void SavePDFForSQ(Telerik.Reporting.Report rpt, string FileName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        //string fileName = QuoteID + result.DocumentName + "." + result.Extension;
        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQ\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();
        SiteConfiguration.UploadPDFFile("SQ", fileName);
        SiteConfiguration.deleteimage(fileName, "SQ");

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        // if (page != null)
        // {
        // if (page.ImagesInfo != null)
        // {
        // foreach (PdfImageInfo info in page.ImagesInfo)
        // {
        // page.TryCompressImage(info.Index);
        // }
        // }
        // }
        //}
        //doc.SaveToFile(fullPath);
        //SiteConfiguration.UploadPDFFile("SQ", fileName);
        //SiteConfiguration.deleteimage(fileName, "SQ");

    }

    public static void SavePDF2(Telerik.Reporting.Report rpt, string FileName, string FolderName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\" + FolderName + "\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
    }

    public static void Generate_PicklistPDF(string ProjectID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Picklist();

        Telerik.Reporting.TextBox txtProjectNo = rpt.Items.Find("txtProjectNo", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNo.Value = stPro.ProjectNumber;

        Telerik.Reporting.TextBox txtCustomerName = rpt.Items.Find("txtCustomerName", true)[0] as Telerik.Reporting.TextBox;
        txtCustomerName.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtContNo = rpt.Items.Find("txtContNo", true)[0] as Telerik.Reporting.TextBox;
        txtContNo.Value = stCont.ContMobile;

        Telerik.Reporting.TextBox txtAltNo = rpt.Items.Find("txtAltNo", true)[0] as Telerik.Reporting.TextBox;
        txtAltNo.Value = stCont.ContPhone;

        Telerik.Reporting.TextBox txtInstalledAddress = rpt.Items.Find("txtInstalledAddress", true)[0] as Telerik.Reporting.TextBox;
        txtInstalledAddress.Value = stPro.InstallAddress;

        Telerik.Reporting.TextBox txtApplicationNo = rpt.Items.Find("txtApplicationNo", true)[0] as Telerik.Reporting.TextBox;
        txtApplicationNo.Value = stPro.ApplicationNo;

        Telerik.Reporting.TextBox txtSystemSize = rpt.Items.Find("txtSystemSize", true)[0] as Telerik.Reporting.TextBox;
        txtSystemSize.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtStruHeight = rpt.Items.Find("txtStruHeight", true)[0] as Telerik.Reporting.TextBox;
        txtStruHeight.Value = stPro.RoofTopArea;

        Telerik.Reporting.TextBox txtInstallationDate = rpt.Items.Find("txtInstallationDate", true)[0] as Telerik.Reporting.TextBox;
        txtInstallationDate.Value = stPro.InstallBookingDateformeted;

        Telerik.Reporting.TextBox txtPanelDescription = rpt.Items.Find("txtPanelDescription", true)[0] as Telerik.Reporting.TextBox;
        txtPanelDescription.Value = stPro.PanelBrandName + "(" + stPro.PanelModel + ")";

        Telerik.Reporting.TextBox txtNoPanels = rpt.Items.Find("txtNoPanels", true)[0] as Telerik.Reporting.TextBox;
        txtNoPanels.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtInverterDescription = rpt.Items.Find("txtInverterDescription", true)[0] as Telerik.Reporting.TextBox;
        txtInverterDescription.Value = stPro.InverterDetailsName + "(" + stPro.InverterModel + ")";

        Telerik.Reporting.TextBox txtNoInverter = rpt.Items.Find("txtNoInverter", true)[0] as Telerik.Reporting.TextBox;
        txtNoInverter.Value = stPro.inverterqty;

        if (stPro.RoofTopArea != "" && stPro.NumberPanels != "0" && stPro.NumberPanels != "")
        {
            DataTable dtDetails = ClstblProjects.tbl_RequireMaterialSiteWise_GetDatabyHeightPanels(stPro.RoofTopArea, stPro.NumberPanels);

            if (dtDetails.Rows.Count > 0)
            {
                Telerik.Reporting.TextBox txt3 = rpt.Items.Find("txt3", true)[0] as Telerik.Reporting.TextBox;
                txt3.Value = dtDetails.Rows[0]["G.I. Pipes (50*50*2.0 MM)"].ToString();

                Telerik.Reporting.TextBox txt4 = rpt.Items.Find("txt4", true)[0] as Telerik.Reporting.TextBox;
                txt4.Value = dtDetails.Rows[0]["Flange (150*150*2.0 MM)"].ToString();

                Telerik.Reporting.TextBox txt5 = rpt.Items.Find("txt5", true)[0] as Telerik.Reporting.TextBox;
                txt5.Value = dtDetails.Rows[0]["Anchor Fastner (10*100 MM)"].ToString();

                Telerik.Reporting.TextBox txt6 = rpt.Items.Find("txt6", true)[0] as Telerik.Reporting.TextBox;
                txt6.Value = dtDetails.Rows[0]["Justice Khilla"].ToString();

                Telerik.Reporting.TextBox txt7 = rpt.Items.Find("txt7", true)[0] as Telerik.Reporting.TextBox;
                txt7.Value = dtDetails.Rows[0]["Alluminium Rail (41*41*4150 MM)"].ToString();

                Telerik.Reporting.TextBox txt8 = rpt.Items.Find("txt8", true)[0] as Telerik.Reporting.TextBox;
                txt8.Value = dtDetails.Rows[0]["Hex Bolt 10*80 MM (Purline)"].ToString();

                Telerik.Reporting.TextBox txt9 = rpt.Items.Find("txt9", true)[0] as Telerik.Reporting.TextBox;
                txt9.Value = dtDetails.Rows[0]["SDS Bolt 55 MM"].ToString();

                Telerik.Reporting.TextBox txt10 = rpt.Items.Find("txt10", true)[0] as Telerik.Reporting.TextBox;
                txt10.Value = dtDetails.Rows[0]["Mid Clamp (T)"].ToString();

                Telerik.Reporting.TextBox txt11 = rpt.Items.Find("txt11", true)[0] as Telerik.Reporting.TextBox;
                txt11.Value = dtDetails.Rows[0]["LN CAP 8*50 Bolt (T)"].ToString();

                Telerik.Reporting.TextBox txt12 = rpt.Items.Find("txt12", true)[0] as Telerik.Reporting.TextBox;
                txt12.Value = dtDetails.Rows[0]["End Clamp (Z)"].ToString();

                Telerik.Reporting.TextBox txt13 = rpt.Items.Find("txt13", true)[0] as Telerik.Reporting.TextBox;
                txt13.Value = dtDetails.Rows[0]["LN CAP 8*25 (Bolt) (Z)"].ToString();

                Telerik.Reporting.TextBox txt14 = rpt.Items.Find("txt14", true)[0] as Telerik.Reporting.TextBox;
                txt14.Value = dtDetails.Rows[0]["M8 Sring Nut"].ToString();

                Telerik.Reporting.TextBox txt15 = rpt.Items.Find("txt15", true)[0] as Telerik.Reporting.TextBox;
                txt15.Value = dtDetails.Rows[0]["Jointner"].ToString();

                Telerik.Reporting.TextBox txt16 = rpt.Items.Find("txt16", true)[0] as Telerik.Reporting.TextBox;
                txt16.Value = dtDetails.Rows[0]["Hex Bolt 8*25 MM (Jointer)"].ToString();

                Telerik.Reporting.TextBox txt17 = rpt.Items.Find("txt17", true)[0] as Telerik.Reporting.TextBox;
                txt17.Value = dtDetails.Rows[0]["DCDB (1 to 3)"].ToString();

                Telerik.Reporting.TextBox txt18 = rpt.Items.Find("txt18", true)[0] as Telerik.Reporting.TextBox;
                txt18.Value = dtDetails.Rows[0]["DCDB (4 to 7)"].ToString();

                Telerik.Reporting.TextBox txt19 = rpt.Items.Find("txt19", true)[0] as Telerik.Reporting.TextBox;
                txt19.Value = dtDetails.Rows[0]["DCDB (8 to 10)"].ToString();

                Telerik.Reporting.TextBox txt20 = rpt.Items.Find("txt20", true)[0] as Telerik.Reporting.TextBox;
                txt20.Value = dtDetails.Rows[0]["ACDB (1 to 6)"].ToString();

                Telerik.Reporting.TextBox txt21 = rpt.Items.Find("txt21", true)[0] as Telerik.Reporting.TextBox;
                txt21.Value = dtDetails.Rows[0]["ACDB (7 to 10)"].ToString();

                Telerik.Reporting.TextBox txt22 = rpt.Items.Find("txt22", true)[0] as Telerik.Reporting.TextBox;
                txt22.Value = dtDetails.Rows[0]["1CX4 Sqmm DC Cable (Positive) Red & Black"].ToString();

                Telerik.Reporting.TextBox txt23 = rpt.Items.Find("txt23", true)[0] as Telerik.Reporting.TextBox;
                txt23.Value = dtDetails.Rows[0]["2C X 4 Sqmm AC Cable"].ToString();

                Telerik.Reporting.TextBox txt24 = rpt.Items.Find("txt24", true)[0] as Telerik.Reporting.TextBox;
                txt24.Value = dtDetails.Rows[0]["4C X 6 Sqmm AC Cable"].ToString();

                Telerik.Reporting.TextBox txt25 = rpt.Items.Find("txt25", true)[0] as Telerik.Reporting.TextBox;
                txt25.Value = dtDetails.Rows[0]["1C X 4Sqmm Earthing Cable (Green)"].ToString();

                Telerik.Reporting.TextBox txt26 = rpt.Items.Find("txt26", true)[0] as Telerik.Reporting.TextBox;
                txt26.Value = dtDetails.Rows[0]["1C X 16 Sqmm Earthing Cable"].ToString();

                Telerik.Reporting.TextBox txt27 = rpt.Items.Find("txt27", true)[0] as Telerik.Reporting.TextBox;
                txt27.Value = dtDetails.Rows[0]["MC-4 Connector"].ToString();

                Telerik.Reporting.TextBox txt28 = rpt.Items.Find("txt28", true)[0] as Telerik.Reporting.TextBox;
                txt28.Value = dtDetails.Rows[0]["UPVC Conduit Pipes 3m"].ToString();

                Telerik.Reporting.TextBox txt29 = rpt.Items.Find("txt29", true)[0] as Telerik.Reporting.TextBox;
                txt29.Value = dtDetails.Rows[0]["Flexible Pipe"].ToString();

                Telerik.Reporting.TextBox txt30 = rpt.Items.Find("txt30", true)[0] as Telerik.Reporting.TextBox;
                txt30.Value = dtDetails.Rows[0]["Elbow"].ToString();

                Telerik.Reporting.TextBox txt31 = rpt.Items.Find("txt31", true)[0] as Telerik.Reporting.TextBox;
                txt31.Value = dtDetails.Rows[0]["Tee"].ToString();

                Telerik.Reporting.TextBox txt32 = rpt.Items.Find("txt32", true)[0] as Telerik.Reporting.TextBox;
                txt32.Value = dtDetails.Rows[0]["China Clamp"].ToString();

                Telerik.Reporting.TextBox txt33 = rpt.Items.Find("txt33", true)[0] as Telerik.Reporting.TextBox;
                txt33.Value = dtDetails.Rows[0]["Earthing Rod,(1.5 Mtr) With Clamp"].ToString();

                Telerik.Reporting.TextBox txt34 = rpt.Items.Find("txt34", true)[0] as Telerik.Reporting.TextBox;
                txt34.Value = dtDetails.Rows[0]["Lightning Arrestor"].ToString();

                Telerik.Reporting.TextBox txt35 = rpt.Items.Find("txt35", true)[0] as Telerik.Reporting.TextBox;
                txt35.Value = dtDetails.Rows[0]["Backfill Compound Bag Smart (Chemical Bag)"].ToString();

                Telerik.Reporting.TextBox txt36 = rpt.Items.Find("txt36", true)[0] as Telerik.Reporting.TextBox;
                txt36.Value = dtDetails.Rows[0]["RCC BAG (Wall Plaster)"].ToString();

                Telerik.Reporting.TextBox txt37 = rpt.Items.Find("txt37", true)[0] as Telerik.Reporting.TextBox;
                txt37.Value = dtDetails.Rows[0]["Cable Tie (300 mm)"].ToString();

                Telerik.Reporting.TextBox txt38 = rpt.Items.Find("txt38", true)[0] as Telerik.Reporting.TextBox;
                txt38.Value = dtDetails.Rows[0]["PVC Insulation Tape - White colour (Nos.)"].ToString();

                Telerik.Reporting.TextBox txt39 = rpt.Items.Find("txt39", true)[0] as Telerik.Reporting.TextBox;
                txt39.Value = dtDetails.Rows[0]["Zinc Spray (Nos.)"].ToString();

                Telerik.Reporting.TextBox txt40 = rpt.Items.Find("txt40", true)[0] as Telerik.Reporting.TextBox;
                txt40.Value = dtDetails.Rows[0]["APS Sign Board (Nos.)"].ToString();

                Telerik.Reporting.TextBox txt41 = rpt.Items.Find("txt41", true)[0] as Telerik.Reporting.TextBox;
                txt41.Value = dtDetails.Rows[0]["Earthing Pit set"].ToString();

                Telerik.Reporting.TextBox txt42 = rpt.Items.Find("txt42", true)[0] as Telerik.Reporting.TextBox;
                txt42.Value = dtDetails.Rows[0]["Pin Lug (4 SQ MM)"].ToString();

                Telerik.Reporting.TextBox txt43 = rpt.Items.Find("txt43", true)[0] as Telerik.Reporting.TextBox;
                txt43.Value = dtDetails.Rows[0]["Pin Lug (6 SQ MM)"].ToString();

                Telerik.Reporting.TextBox txt44 = rpt.Items.Find("txt44", true)[0] as Telerik.Reporting.TextBox;
                txt44.Value = dtDetails.Rows[0]["Rounnd Lug (4 SQ MM)"].ToString();

                Telerik.Reporting.TextBox txt45 = rpt.Items.Find("txt45", true)[0] as Telerik.Reporting.TextBox;
                txt45.Value = dtDetails.Rows[0]["Rounnd Lug (16 SQ MM)"].ToString();

                Telerik.Reporting.TextBox txt46 = rpt.Items.Find("txt46", true)[0] as Telerik.Reporting.TextBox;
                txt46.Value = dtDetails.Rows[0]["Isolation Switch (2 Pole MCB)"].ToString();

                Telerik.Reporting.TextBox txt47 = rpt.Items.Find("txt47", true)[0] as Telerik.Reporting.TextBox;
                txt47.Value = dtDetails.Rows[0]["Isolation Switch (4 Pole MCB)"].ToString();
            }
        }

        Telerik.Reporting.TextBox txtInstallerName = rpt.Items.Find("txtInstallerName", true)[0] as Telerik.Reporting.TextBox;
        txtInstallerName.Value = stPro.InstallerName;

        Telerik.Reporting.TextBox txtInstallerNo = rpt.Items.Find("txtInstallerNo", true)[0] as Telerik.Reporting.TextBox;
        txtInstallerNo.Value = stPro.InstallerMobile;

        if (stPro.TotalQuotePrice != "")
        {
            Telerik.Reporting.TextBox txtTotalCost = rpt.Items.Find("txtTotalCost", true)[0] as Telerik.Reporting.TextBox;
            txtTotalCost.Value = Convert.ToDecimal(stPro.TotalQuotePrice).ToString("F");
        }

        if (stPro.TotalQuotePrice != "" && stPro.InvoicePayTotal != "")
        {
            Telerik.Reporting.TextBox txtBalOwing = rpt.Items.Find("txtBalOwing", true)[0] as Telerik.Reporting.TextBox;
            txtBalOwing.Value = (Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");
        }

        if (stPro.InvoicePayTotal != "")
        {
            Telerik.Reporting.TextBox txtPaidDate = rpt.Items.Find("txtPaidDate", true)[0] as Telerik.Reporting.TextBox;
            txtPaidDate.Value = (Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");
        }

        Telerik.Reporting.TextBox txtInstallerNotes = rpt.Items.Find("txtInstallerNotes", true)[0] as Telerik.Reporting.TextBox;
        txtInstallerNotes.Value = stPro.InstallerNotes;

        Telerik.Reporting.TextBox txtCPName = rpt.Items.Find("txtCPName", true)[0] as Telerik.Reporting.TextBox;
        txtCPName.Value = stPro.CPName;

        Telerik.Reporting.TextBox txtCPContactNo = rpt.Items.Find("txtCPContactNo", true)[0] as Telerik.Reporting.TextBox;
        txtCPContactNo.Value = stPro.CPMobile;

        string FileName = "Picklist_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }

    public static void Generate_PaymentReceipt(string ProjectID)   // This Method Generate Tax Invoice 
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.TexInvoice();

        Telerik.Reporting.TextBox txtProjectNo = rpt.Items.Find("txtProjectNo", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNo.Value = stPro.ProjectNumber;

        Telerik.Reporting.TextBox txtPaymentDate = rpt.Items.Find("txtPaymentDate", true)[0] as Telerik.Reporting.TextBox;
        txtPaymentDate.Value = string.Format("{0:dd MMM yyyy}", DateTime.Now);

        Telerik.Reporting.TextBox txtName = rpt.Items.Find("txtName", true)[0] as Telerik.Reporting.TextBox;
        txtName.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtMobile = rpt.Items.Find("txtMobile", true)[0] as Telerik.Reporting.TextBox;
        txtMobile.Value = stCont.ContMobile;

        Telerik.Reporting.TextBox txtPhone = rpt.Items.Find("txtPhone", true)[0] as Telerik.Reporting.TextBox;
        txtPhone.Value = stCont.ContPhone;

        Telerik.Reporting.TextBox txtEmail = rpt.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        txtEmail.Value = stCont.ContEmail;

        Telerik.Reporting.TextBox txtInstallAddress = rpt.Items.Find("txtInstallAddress", true)[0] as Telerik.Reporting.TextBox;
        txtInstallAddress.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress = rpt.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        txtAddress.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtPanels = rpt.Items.Find("txtPanels", true)[0] as Telerik.Reporting.TextBox;
        txtPanels.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtPanelsName = rpt.Items.Find("txtPanelsName", true)[0] as Telerik.Reporting.TextBox;
        txtPanelsName.Value = stPro.PanelBrandName + "(" + stPro.PanelModel + ")";

        Telerik.Reporting.TextBox txtInverter = rpt.Items.Find("txtInverter", true)[0] as Telerik.Reporting.TextBox;
        txtInverter.Value = stPro.inverterqty;

        Telerik.Reporting.TextBox txtInverterName = rpt.Items.Find("txtInverterName", true)[0] as Telerik.Reporting.TextBox;
        txtInverterName.Value = stPro.InverterDetailsName + "(" + stPro.InverterModel + ")";

        Telerik.Reporting.TextBox txtTotalCost = rpt.Items.Find("txtTotalCost", true)[0] as Telerik.Reporting.TextBox;
        txtTotalCost.Value = (Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.VarOther)).ToString("F");

        Telerik.Reporting.TextBox txtlessSubsidy = rpt.Items.Find("txtlessSubsidy", true)[0] as Telerik.Reporting.TextBox;
        txtlessSubsidy.Value = (Convert.ToDecimal(stPro.RECRebate)).ToString("F");

        Telerik.Reporting.TextBox txtTotalNetCost = rpt.Items.Find("txtTotalNetCost", true)[0] as Telerik.Reporting.TextBox;
        txtTotalNetCost.Value = (Convert.ToDecimal(stPro.ServiceValue) - Convert.ToDecimal(stPro.RECRebate)).ToString("F");

        Telerik.Reporting.TextBox txtPaidToDate = rpt.Items.Find("txtPaidToDate", true)[0] as Telerik.Reporting.TextBox;
        txtPaidToDate.Value = (Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");

        Telerik.Reporting.TextBox txtBalanceDue = rpt.Items.Find("txtBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        txtBalanceDue.Value = (Convert.ToDecimal(stPro.TotalQuotePrice)- Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");

        ExportPdfWithFileName(rpt, "Tax Invoice");
    }

    public static void Generate_Quotation(string ProjectID, string QuoteID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Quotation();

        Telerik.Reporting.TextBox txtCustomerName = rpt.Items.Find("txtCustomerName", true)[0] as Telerik.Reporting.TextBox;
        txtCustomerName.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtAddress1 = rpt.Items.Find("txtAddress1", true)[0] as Telerik.Reporting.TextBox;
        txtAddress1.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress2 = rpt.Items.Find("txtAddress2", true)[0] as Telerik.Reporting.TextBox;
        txtAddress2.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtMobile = rpt.Items.Find("txtMobile", true)[0] as Telerik.Reporting.TextBox;
        txtMobile.Value = stCont.ContMobile;

        Telerik.Reporting.TextBox txtPhone = rpt.Items.Find("txtPhone", true)[0] as Telerik.Reporting.TextBox;
        txtPhone.Value = stCont.ContPhone;

        Telerik.Reporting.TextBox txtEmail = rpt.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        txtEmail.Value = stCont.ContEmail;

        Telerik.Reporting.TextBox txtQuoteNo = rpt.Items.Find("txtQuoteNo", true)[0] as Telerik.Reporting.TextBox;
        txtQuoteNo.Value = stPro.ProjectNumber;

        Telerik.Reporting.TextBox txtIssueDate = rpt.Items.Find("txtIssueDate", true)[0] as Telerik.Reporting.TextBox;
        txtIssueDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.ProjectOpened));

        Telerik.Reporting.TextBox txtSolarAdvisor = rpt.Items.Find("txtSolarAdvisor", true)[0] as Telerik.Reporting.TextBox;
        txtSolarAdvisor.Value = stPro.SalesRepName;

        Telerik.Reporting.TextBox txtSalesRepPhone = rpt.Items.Find("txtSalesRepPhone", true)[0] as Telerik.Reporting.TextBox;
        txtSalesRepPhone.Value = stPro.SalesRepMobile;

        Telerik.Reporting.TextBox txtSalesRepEmail = rpt.Items.Find("txtSalesRepEmail", true)[0] as Telerik.Reporting.TextBox;
        txtSalesRepEmail.Value = stPro.SalesRepEmail;

        Telerik.Reporting.TextBox txtSystemSize = rpt.Items.Find("txtSystemSize", true)[0] as Telerik.Reporting.TextBox;
        txtSystemSize.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtPanels = rpt.Items.Find("txtPanels", true)[0] as Telerik.Reporting.TextBox;
        txtPanels.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtPanelName = rpt.Items.Find("txtPanelName", true)[0] as Telerik.Reporting.TextBox;
        txtPanelName.Value = stPro.PanelBrandName + "(" + stPro.PanelModel + ")";

        Telerik.Reporting.TextBox txtInverter = rpt.Items.Find("txtInverter", true)[0] as Telerik.Reporting.TextBox;
        txtInverter.Value = stPro.inverterqty;

        Telerik.Reporting.TextBox txtInverterName = rpt.Items.Find("txtInverterName", true)[0] as Telerik.Reporting.TextBox;
        txtInverterName.Value = stPro.InverterDetailsName + "(" + stPro.InverterModel + ")";

        Telerik.Reporting.TextBox txtSubsidy40 = rpt.Items.Find("txtSubsidy40", true)[0] as Telerik.Reporting.TextBox;
        txtSubsidy40.Value = (Convert.ToDecimal(stPro.FourtySubsidy)).ToString("F");

        Telerik.Reporting.TextBox txtSubsidy20 = rpt.Items.Find("txtSubsidy20", true)[0] as Telerik.Reporting.TextBox;
        txtSubsidy20.Value = (Convert.ToDecimal(stPro.TwentySubsidy)).ToString("F");

        Telerik.Reporting.TextBox txtExtraCost = rpt.Items.Find("txtExtraCost", true)[0] as Telerik.Reporting.TextBox;
        txtExtraCost.Value = (Convert.ToDecimal(stPro.VarOther)).ToString("F");

        Telerik.Reporting.TextBox txtOtherCost = rpt.Items.Find("txtOtherCost", true)[0] as Telerik.Reporting.TextBox;
        txtOtherCost.Value = "0.00";

        Telerik.Reporting.TextBox txtTotalPaybleAmount = rpt.Items.Find("txtTotalPaybleAmount", true)[0] as Telerik.Reporting.TextBox;
        txtTotalPaybleAmount.Value = (Convert.ToDecimal(stPro.TotalPayableAmount) + Convert.ToDecimal(stPro.VarOther)).ToString("F");

        Telerik.Reporting.TextBox txtDeposit = rpt.Items.Find("txtDeposit", true)[0] as Telerik.Reporting.TextBox;
        txtDeposit.Value = (Convert.ToDecimal(stPro.DepositRequired)).ToString("F"); ;

        Telerik.Reporting.TextBox txtDiscom = rpt.Items.Find("txtDiscom", true)[0] as Telerik.Reporting.TextBox;
        txtDiscom.Value = stPro.ElecDistributor;

        Telerik.Reporting.TextBox txtDivision = rpt.Items.Find("txtDivision", true)[0] as Telerik.Reporting.TextBox;
        txtDivision.Value = stPro.DivisionName;

        Telerik.Reporting.TextBox txtStuctureHeight = rpt.Items.Find("txtStuctureHeight", true)[0] as Telerik.Reporting.TextBox;
        txtStuctureHeight.Value = stPro.RoofTopArea;

        Telerik.Reporting.TextBox txtQuoteNotes = rpt.Items.Find("txtQuoteNotes", true)[0] as Telerik.Reporting.TextBox;
        txtQuoteNotes.Value = stPro.ProjectNotes;

        //ExportPdf(rpt);
        SavePDF(rpt, QuoteID);
    }

    public static void Generate_PaymentReceiptNew(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.PaymentReceipt();

        Telerik.Reporting.TextBox txtProjectNo = rpt.Items.Find("txtProjectNo", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNo.Value = stPro.ProjectNumber;

        Telerik.Reporting.TextBox txtDate = rpt.Items.Find("txtDate", true)[0] as Telerik.Reporting.TextBox;
        txtDate.Value = string.Format("{0:dd MMM yyyy}", DateTime.Now);

        Telerik.Reporting.TextBox txtCustName = rpt.Items.Find("txtCustName", true)[0] as Telerik.Reporting.TextBox;
        txtCustName.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtCustMobile = rpt.Items.Find("txtCustMobile", true)[0] as Telerik.Reporting.TextBox;
        txtCustMobile.Value = stCont.ContMobile;

        Telerik.Reporting.TextBox txtCustPhone = rpt.Items.Find("txtCustPhone", true)[0] as Telerik.Reporting.TextBox;
        txtCustPhone.Value = stCont.ContPhone;

        Telerik.Reporting.TextBox txtCustEmail = rpt.Items.Find("txtCustEmail", true)[0] as Telerik.Reporting.TextBox;
        txtCustEmail.Value = stCont.ContEmail;

        Telerik.Reporting.TextBox txtAddress1 = rpt.Items.Find("txtAddress1", true)[0] as Telerik.Reporting.TextBox;
        txtAddress1.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress2 = rpt.Items.Find("txtAddress2", true)[0] as Telerik.Reporting.TextBox;
        txtAddress2.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtPanels = rpt.Items.Find("txtPanels", true)[0] as Telerik.Reporting.TextBox;
        txtPanels.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtPanelName = rpt.Items.Find("txtPanelName", true)[0] as Telerik.Reporting.TextBox;
        txtPanelName.Value = stPro.PanelBrandName + "(" + stPro.PanelModel + ")";

        Telerik.Reporting.TextBox txtInverter = rpt.Items.Find("txtInverter", true)[0] as Telerik.Reporting.TextBox;
        txtInverter.Value = stPro.inverterqty;

        Telerik.Reporting.TextBox txtInverterName = rpt.Items.Find("txtInverterName", true)[0] as Telerik.Reporting.TextBox;
        txtInverterName.Value = stPro.InverterDetailsName + "(" + stPro.InverterModel + ")";

        Telerik.Reporting.TextBox txtTotalCost = rpt.Items.Find("txtTotalCost", true)[0] as Telerik.Reporting.TextBox;
        txtTotalCost.Value = (Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.VarOther)).ToString("F");

        Telerik.Reporting.TextBox txtLessSubsidy = rpt.Items.Find("txtLessSubsidy", true)[0] as Telerik.Reporting.TextBox;
        txtLessSubsidy.Value = (Convert.ToDecimal(stPro.RECRebate)).ToString("F");

        Telerik.Reporting.TextBox txtNetCost = rpt.Items.Find("txtNetCost", true)[0] as Telerik.Reporting.TextBox;
        txtNetCost.Value = (Convert.ToDecimal(stPro.ServiceValue) - Convert.ToDecimal(stPro.RECRebate)).ToString("F");

        Telerik.Reporting.TextBox txtPaidtoDate = rpt.Items.Find("txtPaidtoDate", true)[0] as Telerik.Reporting.TextBox;
        txtPaidtoDate.Value = (Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");

        Telerik.Reporting.TextBox txtBalanceDue = rpt.Items.Find("txtBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        txtBalanceDue.Value = (Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");

        DataTable dtInvoicePayment = ClsTelerikReportData.tblInvoicePayments_GetDataByProjectID(ProjectID);

        if(dtInvoicePayment.Rows.Count > 0)
        {
            Telerik.Reporting.Table tblPaymentDetails = rpt.Items.Find("tblPaymentDetails", true)[0] as Telerik.Reporting.Table;
            tblPaymentDetails.DataSource = dtInvoicePayment;
        }

        Telerik.Reporting.TextBox txtBalanceDua2 = rpt.Items.Find("txtBalanceDua2", true)[0] as Telerik.Reporting.TextBox;
        txtBalanceDua2.Value = (Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(stPro.InvoicePayTotal)).ToString("F");

        ExportPdf(rpt);
    }

    public static void Generate_SerialNumber(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.SerialNumber();

        Telerik.Reporting.TextBox txtCustName = rpt.Items.Find("txtCustName", true)[0] as Telerik.Reporting.TextBox;
        txtCustName.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtProjectNo = rpt.Items.Find("txtProjectNo", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNo.Value = stPro.ProjectNumber;

        Telerik.Reporting.TextBox txtApplicationNo = rpt.Items.Find("txtApplicationNo", true)[0] as Telerik.Reporting.TextBox;
        txtApplicationNo.Value = stPro.ApplicationNo;
        
        //Bind SerialNumber
        DataTable dtPanelSerialNo = ClsTelerikReportData.tbl_Installation_Project_ScannedSerial_GetPenelSerialNoByProjectID(ProjectID);
        if (dtPanelSerialNo.Rows.Count > 0)
        {
            Telerik.Reporting.Table tblPanels = rpt.Items.Find("tblPanels", true)[0] as Telerik.Reporting.Table;
            tblPanels.DataSource = dtPanelSerialNo;
        }

        DataTable dtInverterSerialNo = ClsTelerikReportData.tbl_Installation_Project_ScannedSerial_GetInverterSerialNoByProjectID(ProjectID);
        if (dtInverterSerialNo.Rows.Count > 0)
        {
            Telerik.Reporting.Table tblInverter = rpt.Items.Find("tblInverter", true)[0] as Telerik.Reporting.Table;
            tblInverter.DataSource = dtInverterSerialNo;
        }

        string FileName = "SerialNo_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }

    public static void Generate_Self_Certification_DGVCL(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Self_Certification_DGVCL();

        Telerik.Reporting.TextBox txtConsumerNo = rpt.Items.Find("txtConsumerNo", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNo.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtGUVNLNo = rpt.Items.Find("txtGUVNLNo", true)[0] as Telerik.Reporting.TextBox;
        txtGUVNLNo.Value = stPro.GUVNL_Reg_No;

        Telerik.Reporting.TextBox txtCustomer = rpt.Items.Find("txtCustomer", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtCircle = rpt.Items.Find("txtCircle", true)[0] as Telerik.Reporting.TextBox;
        txtCircle.Value = stPro.Circle.ToString().Split(' ')[0].ToString();

        Telerik.Reporting.TextBox txtConsumerNoBSI = rpt.Items.Find("txtConsumerNoBSI", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoBSI.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtDivision = rpt.Items.Find("txtDivision", true)[0] as Telerik.Reporting.TextBox;
        txtDivision.Value = stPro.DivisionName.ToString().Split(' ')[0].ToString(); ;

        Telerik.Reporting.TextBox txtSubDivision = rpt.Items.Find("txtSubDivision", true)[0] as Telerik.Reporting.TextBox;
        txtSubDivision.Value = stPro.SubDivisionName;

        Telerik.Reporting.TextBox txtAddress1 = rpt.Items.Find("txtAddress1", true)[0] as Telerik.Reporting.TextBox;
        txtAddress1.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress2 = rpt.Items.Find("txtAddress2", true)[0] as Telerik.Reporting.TextBox;
        txtAddress2.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtLoad = rpt.Items.Find("txtLoad", true)[0] as Telerik.Reporting.TextBox;
        txtLoad.Value = stPro.LotNumber;

        Telerik.Reporting.TextBox txtKV = rpt.Items.Find("txtKV", true)[0] as Telerik.Reporting.TextBox;
        txtKV.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtKW1 = rpt.Items.Find("txtKW1", true)[0] as Telerik.Reporting.TextBox;
        txtKW1.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtPhase1 = rpt.Items.Find("txtPhase1", true)[0] as Telerik.Reporting.TextBox;
        txtPhase1.Value = stPro.MeterPhase;

        Telerik.Reporting.TextBox txtConsumerNoPage2 = rpt.Items.Find("txtConsumerNoPage2", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage2.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtConsumerNoPage3 = rpt.Items.Find("txtConsumerNoPage3", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage3.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtConsumerNoPage4 = rpt.Items.Find("txtConsumerNoPage4", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage4.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtConsumerNoPage5 = rpt.Items.Find("txtConsumerNoPage5", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage5.Value = stPro.NMINumber;

        ClsTelerikReportData.SttblStockItemsReportData stStockItemPanel = ClsTelerikReportData.tblStockItems_SelectByStockItemID_ReportData(stPro.PanelBrandID);

        Telerik.Reporting.TextBox txtPanelName = rpt.Items.Find("txtPanelName", true)[0] as Telerik.Reporting.TextBox;
        txtPanelName.Value = stStockItemPanel.StockItem;

        Telerik.Reporting.TextBox txtSolarPV = rpt.Items.Find("txtSolarPV", true)[0] as Telerik.Reporting.TextBox;
        txtSolarPV.Value = stStockItemPanel.StockManufacturer == "Australian Premium Solar" ? "APS" : stStockItemPanel.StockManufacturer;
            
        Telerik.Reporting.TextBox txtModelNo = rpt.Items.Find("txtModelNo", true)[0] as Telerik.Reporting.TextBox;
        txtModelNo.Value = stStockItemPanel.StockModel;

        Telerik.Reporting.TextBox txtSerise = rpt.Items.Find("txtSeries", true)[0] as Telerik.Reporting.TextBox;
        txtSerise.Value = stStockItemPanel.StockSeries;

        Telerik.Reporting.TextBox txtStockSize = rpt.Items.Find("txtStockSize", true)[0] as Telerik.Reporting.TextBox;
        txtStockSize.Value = stStockItemPanel.StockSize;

        Telerik.Reporting.TextBox txtNoPanel = rpt.Items.Find("txtNoPanel", true)[0] as Telerik.Reporting.TextBox;
        txtNoPanel.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtTotalKW = rpt.Items.Find("txtTotalKW", true)[0] as Telerik.Reporting.TextBox;
        txtTotalKW.Value = (Convert.ToInt32(stStockItemPanel.StockSize) * Convert.ToInt32(stPro.NumberPanels)).ToString();

        ClsTelerikReportData.SttblStockItemsReportData stStockItemInverter = ClsTelerikReportData.tblStockItems_SelectByStockItemID_ReportData(stPro.InverterDetailsID);

        Telerik.Reporting.TextBox txtInverterName = rpt.Items.Find("txtInverterName", true)[0] as Telerik.Reporting.TextBox;
        txtInverterName.Value = stStockItemInverter.StockItem;

        Telerik.Reporting.TextBox txtInverterModelNo = rpt.Items.Find("txtInverterModelNo", true)[0] as Telerik.Reporting.TextBox;
        txtInverterModelNo.Value = stStockItemInverter.StockModel;

        Telerik.Reporting.TextBox txtInverterPhase = rpt.Items.Find("txtInverterPhase", true)[0] as Telerik.Reporting.TextBox;
        txtInverterPhase.Value = stPro.MeterPhase;

        Telerik.Reporting.TextBox txtInverterKW = rpt.Items.Find("txtInverterKW", true)[0] as Telerik.Reporting.TextBox;
        txtInverterKW.Value = stPro.SystemCapKW;
        
        DataTable dtInverterSerialNo = ClsTelerikReportData.tbl_Installation_Project_ScannedSerial_GetInverterSerialNoByProjectID(ProjectID);
        if (dtInverterSerialNo.Rows.Count > 0)
        {
            Telerik.Reporting.TextBox txtInverterSerialNo = rpt.Items.Find("txtInverterSerialNo", true)[0] as Telerik.Reporting.TextBox;
            txtInverterSerialNo.Value = dtInverterSerialNo.Rows[0]["Inverter"].ToString();
        }

        Telerik.Reporting.TextBox txtDisComName = rpt.Items.Find("txtDisComName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDiscomName2 = rpt.Items.Find("txtDiscomName2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDiscomName3 = rpt.Items.Find("txtDiscomName3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDiscomName4 = rpt.Items.Find("txtDiscomName4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDiscomName5 = rpt.Items.Find("txtDiscomName5", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.ElecDistributor == "DGVCL")
        {
            txtDisComName.Value = "Dakshin Gujarat Vij Company Limited";
            txtDiscomName2.Value = "Dakshin Gujarat Vij Company Limited";
            txtDiscomName3.Value = "Dakshin Gujarat Vij Company Limited";
            txtDiscomName4.Value = "Dakshin Gujarat Vij Company Limited";
            txtDiscomName5.Value = "Dakshin Gujarat Vij Company Limited";
        }
        else if (stPro.ElecDistributor == "PGVCL")
        {
            txtDisComName.Value = "Paschim Gujarat Vij Company Limited";
            txtDiscomName2.Value = "Paschim Gujarat Vij Company Limited";
            txtDiscomName3.Value = "Paschim Gujarat Vij Company Limited";
            txtDiscomName4.Value = "Paschim Gujarat Vij Company Limited";
            txtDiscomName5.Value = "Paschim Gujarat Vij Company Limited";
        }
        else if (stPro.ElecDistributor == "UGVCL")
        {
            txtDisComName.Value = "Utter Gujarat Vij Company Limited";
            txtDiscomName2.Value = "Utter Gujarat Vij Company Limited";
            txtDiscomName3.Value = "Utter Gujarat Vij Company Limited";
            txtDiscomName4.Value = "Utter Gujarat Vij Company Limited";
            txtDiscomName5.Value = "Utter Gujarat Vij Company Limited";
        }
        else if (stPro.ElecDistributor == "MGVCL")
        {
            txtDisComName.Value = "Madhya Gujarat Vij Company Limited";
            txtDiscomName2.Value = "Madhya Gujarat Vij Company Limited";
            txtDiscomName3.Value = "Madhya Gujarat Vij Company Limited";
            txtDiscomName4.Value = "Madhya Gujarat Vij Company Limited";
            txtDiscomName5.Value = "Madhya Gujarat Vij Company Limited";
        }

        string FileName = "SelfCertification_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }

    public static void Generate_Self_Certification_Torrent(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Self_Certification_TORRENT();

        Telerik.Reporting.TextBox txtConsumerNo = rpt.Items.Find("txtConsumerNo", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNo.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtGUVNLNo = rpt.Items.Find("txtGUVNLNo", true)[0] as Telerik.Reporting.TextBox;
        txtGUVNLNo.Value = stPro.GUVNL_Reg_No;

        Telerik.Reporting.TextBox txtCustomer = rpt.Items.Find("txtCustomer", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer.Value = stPro.Customer;

        //Telerik.Reporting.TextBox txtCircle = rpt.Items.Find("txtCircle", true)[0] as Telerik.Reporting.TextBox;
        //txtCircle.Value = stPro.Circle.ToString().Split(' ')[0].ToString();

        Telerik.Reporting.TextBox txtConsumerNoBSI = rpt.Items.Find("txtConsumerNoBSI", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoBSI.Value = stPro.NMINumber;

        //Telerik.Reporting.TextBox txtDivision = rpt.Items.Find("txtDivision", true)[0] as Telerik.Reporting.TextBox;
        //txtDivision.Value = stPro.DivisionName.ToString().Split(' ')[0].ToString();

        Telerik.Reporting.TextBox txtSubDivision = rpt.Items.Find("txtSubDivision", true)[0] as Telerik.Reporting.TextBox;
        txtSubDivision.Value = stPro.SubDivisionName;

        Telerik.Reporting.TextBox txtAddress1 = rpt.Items.Find("txtAddress1", true)[0] as Telerik.Reporting.TextBox;
        txtAddress1.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress2 = rpt.Items.Find("txtAddress2", true)[0] as Telerik.Reporting.TextBox;
        txtAddress2.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtLoad = rpt.Items.Find("txtLoad", true)[0] as Telerik.Reporting.TextBox;
        txtLoad.Value = stPro.LotNumber;

        Telerik.Reporting.TextBox txtKV = rpt.Items.Find("txtKV", true)[0] as Telerik.Reporting.TextBox;
        txtKV.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtKW1 = rpt.Items.Find("txtKW1", true)[0] as Telerik.Reporting.TextBox;
        txtKW1.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtPhase1 = rpt.Items.Find("txtPhase1", true)[0] as Telerik.Reporting.TextBox;
        txtPhase1.Value = stPro.MeterPhase;

        Telerik.Reporting.TextBox txtConsumerNoPage2 = rpt.Items.Find("txtConsumerNoPage2", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage2.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtConsumerNoPage3 = rpt.Items.Find("txtConsumerNoPage3", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNoPage3.Value = stPro.NMINumber;
        
        ClsTelerikReportData.SttblStockItemsReportData stStockItemPanel = ClsTelerikReportData.tblStockItems_SelectByStockItemID_ReportData(stPro.PanelBrandID);

        Telerik.Reporting.TextBox txtPanelName = rpt.Items.Find("txtPanelName", true)[0] as Telerik.Reporting.TextBox;
        txtPanelName.Value = stStockItemPanel.StockItem;

        Telerik.Reporting.TextBox txtSolarPV = rpt.Items.Find("txtSolarPV", true)[0] as Telerik.Reporting.TextBox;
        txtSolarPV.Value = stStockItemPanel.StockManufacturer == "Australian Premium Solar" ? "APS" : stStockItemPanel.StockManufacturer;

        Telerik.Reporting.TextBox txtModelNo = rpt.Items.Find("txtModelNo", true)[0] as Telerik.Reporting.TextBox;
        txtModelNo.Value = stStockItemPanel.StockModel;

        Telerik.Reporting.TextBox txtSerise = rpt.Items.Find("txtSeries", true)[0] as Telerik.Reporting.TextBox;
        txtSerise.Value = stStockItemPanel.StockSeries;

        Telerik.Reporting.TextBox txtStockSize = rpt.Items.Find("txtStockSize", true)[0] as Telerik.Reporting.TextBox;
        txtStockSize.Value = stStockItemPanel.StockSize;

        Telerik.Reporting.TextBox txtNoPanel = rpt.Items.Find("txtNoPanel", true)[0] as Telerik.Reporting.TextBox;
        txtNoPanel.Value = stPro.NumberPanels;

        Telerik.Reporting.TextBox txtTotalKW = rpt.Items.Find("txtTotalKW", true)[0] as Telerik.Reporting.TextBox;
        txtTotalKW.Value = (Convert.ToInt32(stStockItemPanel.StockSize) * Convert.ToInt32(stPro.NumberPanels)).ToString();

        ClsTelerikReportData.SttblStockItemsReportData stStockItemInverter = ClsTelerikReportData.tblStockItems_SelectByStockItemID_ReportData(stPro.InverterDetailsID);

        Telerik.Reporting.TextBox txtInverterName = rpt.Items.Find("txtInverterName", true)[0] as Telerik.Reporting.TextBox;
        txtInverterName.Value = stStockItemInverter.StockItem;

        Telerik.Reporting.TextBox txtInverterModelNo = rpt.Items.Find("txtInverterModelNo", true)[0] as Telerik.Reporting.TextBox;
        txtInverterModelNo.Value = stStockItemInverter.StockModel;

        Telerik.Reporting.TextBox txtInverterPhase = rpt.Items.Find("txtInverterPhase", true)[0] as Telerik.Reporting.TextBox;
        txtInverterPhase.Value = stPro.MeterPhase;

        Telerik.Reporting.TextBox txtInverterKW = rpt.Items.Find("txtInverterKW", true)[0] as Telerik.Reporting.TextBox;
        txtInverterKW.Value = stPro.SystemCapKW;

        DataTable dtInverterSerialNo = ClsTelerikReportData.tbl_Installation_Project_ScannedSerial_GetInverterSerialNoByProjectID(ProjectID);
        if (dtInverterSerialNo.Rows.Count > 0)
        {
            Telerik.Reporting.TextBox txtInverterSerialNo = rpt.Items.Find("txtInverterSerialNo", true)[0] as Telerik.Reporting.TextBox;
            txtInverterSerialNo.Value = dtInverterSerialNo.Rows[0]["Inverter"].ToString();
        }

        

        string FileName = "SelfCertification_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }

    public static void Generate_Agreement(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Agreement();

        Telerik.Reporting.TextBox txtCustomer = rpt.Items.Find("txtCustomer", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer.Value = stPro.Customer;
        
        Telerik.Reporting.TextBox txtAddress = rpt.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        txtAddress.Value = stPro.street_number + ", " + stPro.street_name;
        
        Telerik.Reporting.TextBox txtDiscomName = rpt.Items.Find("txtDiscomName", true)[0] as Telerik.Reporting.TextBox;
        txtDiscomName.Value = stPro.ElecDistributor;

        Telerik.Reporting.TextBox txtDivision = rpt.Items.Find("txtDivision", true)[0] as Telerik.Reporting.TextBox;
        txtDivision.Value = stPro.DivisionName.ToString().Split(' ')[0].ToString();

        Telerik.Reporting.TextBox txtCustomer1 = rpt.Items.Find("txtCustomer1", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer1.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtKW = rpt.Items.Find("txtKW", true)[0] as Telerik.Reporting.TextBox;
        txtKW.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtDiscomName1 = rpt.Items.Find("txtDiscomName1", true)[0] as Telerik.Reporting.TextBox;
        txtDiscomName1.Value = stPro.ElecDistributor;
        
        Telerik.Reporting.TextBox txtDiscomShortName = rpt.Items.Find("txtDiscomShortName", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.ElecDistributor == "DGVCL")
        {
            txtDiscomShortName.Value = "Dakshin";
        }
        else if (stPro.ElecDistributor == "PGVCL")
        {
            txtDiscomShortName.Value = "Paschim";
        }
        else if (stPro.ElecDistributor == "UGVCL")
        {
            txtDiscomShortName.Value = "Utter";
        }
        else if (stPro.ElecDistributor == "MGVCL")
        {
            txtDiscomShortName.Value = "Madhya";
        }

        Telerik.Reporting.TextBox txtKW1 = rpt.Items.Find("txtKW1", true)[0] as Telerik.Reporting.TextBox;
        txtKW1.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtKW2 = rpt.Items.Find("txtKW2", true)[0] as Telerik.Reporting.TextBox;
        txtKW2.Value = stPro.SystemCapKW;
        
        string FileName = "Agteement_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }

    public static void Generate_Agreement_Torrent(string ProjectID)
    {
        ClsTelerikReportData.SttblProjectsReportData stPro = ClsTelerikReportData.tblProjects_SelectByProjectID_ReportData(ProjectID);
        ClsTelerikReportData.SttblContactsReportData stCont = ClsTelerikReportData.tblContacts_SelectByContactID_ReportData(stPro.ContactID);

        Telerik.Reporting.Report rpt = new APS_Reporting.Agreement_Torrent();

        Telerik.Reporting.TextBox txtDivision = rpt.Items.Find("txtDivision", true)[0] as Telerik.Reporting.TextBox;
        txtDivision.Value = stPro.DivisionName.ToString().Split(' ')[0].ToString();

        Telerik.Reporting.TextBox txtConsumerNo = rpt.Items.Find("txtConsumerNo", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNo.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtCustomer = rpt.Items.Find("txtCustomer", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtAddress = rpt.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        txtAddress.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress2 = rpt.Items.Find("txtAddress2", true)[0] as Telerik.Reporting.TextBox;
        txtAddress2.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtCustomer2 = rpt.Items.Find("txtCustomer2", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer2.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtKW = rpt.Items.Find("txtKW", true)[0] as Telerik.Reporting.TextBox;
        txtKW.Value = stPro.SystemCapKW;
        
        Telerik.Reporting.TextBox txtAddress3 = rpt.Items.Find("txtAddress3", true)[0] as Telerik.Reporting.TextBox;
        txtAddress3.Value = stPro.street_number + ", " + stPro.street_name;

        Telerik.Reporting.TextBox txtAddress4 = rpt.Items.Find("txtAddress4", true)[0] as Telerik.Reporting.TextBox;
        txtAddress4.Value = stPro.InstallCity + ", " + stPro.Taluka + ", " + stPro.InstallState + "-" + stPro.InstallPostCode;

        Telerik.Reporting.TextBox txtKW2 = rpt.Items.Find("txtKW2", true)[0] as Telerik.Reporting.TextBox;
        txtKW2.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtKW4 = rpt.Items.Find("txtKW4", true)[0] as Telerik.Reporting.TextBox;
        txtKW4.Value = stPro.SystemCapKW;

        Telerik.Reporting.TextBox txtCustomer5 = rpt.Items.Find("txtCustomer5", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer5.Value = stPro.Customer;

        Telerik.Reporting.TextBox txtConsumerNo2 = rpt.Items.Find("txtConsumerNo2", true)[0] as Telerik.Reporting.TextBox;
        txtConsumerNo2.Value = stPro.NMINumber;

        Telerik.Reporting.TextBox txtCustomer3 = rpt.Items.Find("txtCustomer3", true)[0] as Telerik.Reporting.TextBox;
        txtCustomer3.Value = stPro.Customer;
        
        Telerik.Reporting.TextBox txtCustMobileNo = rpt.Items.Find("txtCustMobileNo", true)[0] as Telerik.Reporting.TextBox;
        txtCustMobileNo.Value = stCont.ContMobile;

        Telerik.Reporting.TextBox txtCustEmail = rpt.Items.Find("txtCustEmail", true)[0] as Telerik.Reporting.TextBox;
        txtCustEmail.Value = stCont.ContEmail;
        
        string FileName = "Agteement_" + stPro.ProjectNumber;

        ExportPdfWithFileName(rpt, FileName);
    }
}