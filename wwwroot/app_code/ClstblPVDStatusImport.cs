using System;
using System.Data;
using System.Data.Common;

public struct SttblPVDStatusImport
{
    public string PVDNumber;
    public string ReferenceNumber;
    public string ProjectNumber;
    public string ManualQuoteNumber;
    public string ProjManual;
    public string PVDStatus;
    public string PVDStatusID;
    public string STCNotes;
    public string ImportDate;
    public string UpdateDone;
    public string STCApplied;
    public string upsize_ts;
}


public class ClstblPVDStatusImport
{
    public static SttblPVDStatusImport tblPVDStatusImport_SelectByPVDStatusImportID(String PVDStatusImportID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_SelectByPVDStatusImportID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusImportID";
        param.Value = PVDStatusImportID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblPVDStatusImport details = new SttblPVDStatusImport();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.ReferenceNumber = dr["ReferenceNumber"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.ProjManual = dr["ProjManual"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.ImportDate = dr["ImportDate"].ToString();
            details.UpdateDone = dr["UpdateDone"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblPVDStatusImport_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblPVDStatusImport_Insert(String PVDNumber, String ProjectNumber, String ManualQuoteNumber, String ProjManual, String PVDStatus, String STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjManual";
        if (ProjManual != string.Empty)
            param.Value = ProjManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatus";
        if (PVDStatus != string.Empty)
            param.Value = PVDStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblPVDStatusImport_Insert2(String PVDNumber, String ProjectNumber, String ManualQuoteNumber, String ProjManual, String PVDStatus, String STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Insert2";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        if (ManualQuoteNumber != string.Empty)
            param.Value = ManualQuoteNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjManual";
        if (ProjManual != string.Empty)
            param.Value = ProjManual;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatus";
        if (PVDStatus != string.Empty)
            param.Value = PVDStatus;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        if (STCApplied != string.Empty)
            param.Value = STCApplied;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblPVDStatusImport_Update(string PVDStatusImportID, String PVDNumber, String ReferenceNumber, String ProjectNumber, String ManualQuoteNumber, String ProjManual, String PVDStatus, String PVDStatusID, String STCNotes, String ImportDate, String UpdateDone, String STCApplied)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusImportID";
        param.Value = PVDStatusImportID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        param.Value = PVDNumber;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNumber";
        param.Value = ReferenceNumber;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        param.Value = ProjectNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ManualQuoteNumber";
        param.Value = ManualQuoteNumber;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjManual";
        param.Value = ProjManual;
        param.DbType = DbType.String;
        param.Size = 1;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatus";
        param.Value = PVDStatus;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusID";
        param.Value = PVDStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCNotes";
        param.Value = STCNotes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ImportDate";
        param.Value = ImportDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdateDone";
        param.Value = UpdateDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@STCApplied";
        param.Value = STCApplied;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);




        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblPVDStatusImport_Delete(string PVDStatusImportID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PVDStatusImportID";
        param.Value = PVDStatusImportID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblPVDStatusImport_Statement(string stri)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPVDStatusImport_Statement";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@stri";
        param.Value = stri;
        param.DbType = DbType.String;
        param.Size = 8000;
        comm.Parameters.Add(param);

        int result = -1;

        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tbltest(string ProjectNumbers)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbltest";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumbers";
        param.Value = ProjectNumbers;
        param.DbType = DbType.String;
        param.Size = 1000000;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}