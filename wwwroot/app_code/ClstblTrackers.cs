using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblTrackers
	{
		public string Tracker;
		public string Active;
		public string Seq;

	}


public class ClstblTrackers
{
	public static SttblTrackers tblTrackers_SelectByTrackerID (String TrackerID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackers_SelectByTrackerID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackerID";
		param.Value = TrackerID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblTrackers details = new SttblTrackers();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.Tracker = dr["Tracker"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblTrackers_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackers_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblTrackers_Insert ( String Tracker, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackers_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@Tracker";
		param.Value = Tracker;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblTrackers_Update (string TrackerID, String Tracker, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackers_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackerID";
		param.Value = TrackerID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Tracker";
		param.Value = Tracker;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblTrackers_Delete (string TrackerID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackers_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackerID";
		param.Value = TrackerID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}



    public static int tblTrackers_Exists(string Tracker)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackers_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Tracker";
        param.Value = Tracker;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblTrackers_ExistsById(string Tracker, string TrackerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackers_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Tracker";
        param.Value = Tracker;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@TrackerID";
        param.Value = TrackerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblTrackers_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackers_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblTrackers_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblTrackers_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}