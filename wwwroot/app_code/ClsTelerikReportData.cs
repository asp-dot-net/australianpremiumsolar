﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsTelerikReportData
/// </summary>
public class ClsTelerikReportData
{
    public struct SttblProjectsReportData
    {
        public string CustomerID;
        public string ContactID;
        public string EmployeeID;
        public string SalesRep;
        public string ProjectTypeID;
        public string ProjectStatusID;
        public string ProjectCancelID;
        public string ProjectOnHoldID;
        public string ProjectOpened;
        public string ProjectCancelled;
        public string ProjectNumber;
        public string OldProjectNumber;
        public string ManualQuoteNumber;
        public string Bi_directionalDate;
        public string DiscomDate;
        public string Project;
        public string Projects;
        public string NextMaintenanceCall;
        public string InstallAddress;
        public string InstallCity;
        public string InstallState;
        public string InstallPostCode;
        public string ProjectNotes;
        public string AdditionalSystem;
        public string GridConnected;
        public string RebateApproved;
        public string ReceivedCredits;
        public string CreditEligible;
        public string MoreThanOneInstall;
        public string RequiredCompliancePaperwork;
        public string OutOfPocketDocs;
        public string OwnerGSTRegistered;
        public string OwnerABN;
        public string HouseTypeID;
        public string RoofTypeID;
        public string RoofAngleID;
        public string InstallBase;
        public string StockAllocationStore;
        public string StandardPack;
        public string PanelBrandID;
        public string PanelBrand;
        public string PanelModel;
        public string PanelOutput;
        public string PanelDetails;
        public string InverterDetailsID;
        public string SecondInverterDetailsID;
        public string ThirdInverterDetailsID;
        public string InverterBrand;
        public string InverterModel;
        public string InverterSeries;
        public string InverterOutput;
        public string SecondInverterOutput;
        public string ThirdInverterOutput;
        public string TotalInverterOutput;
        public string InverterCert;
        public string InverterDetails;
        public string SystemDetails;
        public string NumberPanels;
        public string PreviousNumberPanels;
        public string PanelConfigNW;
        public string PanelConfigOTH;
        public string SystemCapKW;
        public string STCMultiplier;
        public string STCZoneRating;
        public string STCNumber;
        public string STCValue;
        public string ElecRetailerID;
        public string ElecDistributorID;
        public string ElecDistApplied;
        public string ElecDistApplyMethod;
        public string ElecDistApplyBy;
        public string ElecDistApplySentFrom;
        public string ElecDistApproved;
        public string ElecDistApprovelRef;
        public string ElecDistOK;
        public string RegPlanNo;
        public string LotNumber;
        public string Asbestoss;
        public string MeterUG;
        public string MeterEnoughSpace;
        public string SplitSystem;
        public string CherryPicker;
        public string TravelTime;
        public string VariationOther;
        public string VarRoofType;
        public string VarRoofAngle;
        public string VarHouseType;
        public string VarAsbestos;
        public string VarMeterInstallation;
        public string VarMeterUG;
        public string VarTravelTime;
        public string HotWaterMeter;
        public string SmartMeter;
        public string VarSplitSystem;
        public string VarEnoughSpace;
        public string VarCherryPicker;
        public string VarOther;
        public string NetTotal;
        public string SpecialDiscount;
        public string collectioncharge;
        public string DepositRequired;
        public string TotalQuotePrice;
        public string PreviousTotalQuotePrice;
        public string InvoiceExGST;
        public string InvoiceGST;
        public string BalanceGST;
        public string FinanceWithID;
        public string ServiceValue;
        public string QuoteSent;
        public string QuoteSentNo;
        public string QuoteAccepted;
        public string SignedQuote;
        public string MeterBoxPhotosSaved;
        public string ElecBillSaved;
        public string ProposedDesignSaved;
        public string FollowUp;
        public string FollowUpNote;
        public string InvoiceNumber;
        public string InvoiceTag;
        public string InvoiceDoc;
        public string InvoiceDetail;
        public string InvoiceSent;
        public string InvoiceFU;
        public string InvoiceNotes;
        public string InvRefund;
        public string InvRefunded;
        public string InvRefundBy;
        public string DepositReceived;
        public string DepositAmount;
        public string ReceiptSent;
        public string SalesCommPaid;
        public string InstallBookingDate;
        public string MeterIncluded;
        public string MeterAppliedRef;
        public string MeterAppliedDate;
        public string MeterAppliedTime;
        public string MeterAppliedMethod;
        public string MeterApprovedDate;
        public string sanction;
        public string NetMeterId;
        public string MeterApprovalNo;
        public string MeterPhase;
        public string OffPeak;
        public string NMINumber;
        public string MeterNumber1;
        public string meterupgrade;
        public string MeterNumber2;
        public string MeterNumber3;
        public string MeterNumber4;
        public string DivisionId;
        public string MeterFU;
        public string MeterNotes;
        public string OldSystemDetails;
        public string REXAppliedRef;
        public string REXAppliedDate;
        public string REXStatusID;
        public string REXApprovalNotes;
        public string REXApprovalFU;
        public string STCPrice;
        public string RECRebate;
        public string BalanceRequested;
        public string Installer;
        public string Designer;
        public string Electrician;
        public string InstallAM1;
        public string InstallPM1;
        public string InstallAM2;
        public string InstallPM2;
        public string InstallDays;
        public string STCFormsDone;
        public string InstallDocsReceived;
        public string InstallerNotified;
        public string CustNotifiedInstall;
        public string InstallerFollowUp;
        public string InstallerNotes;
        public string InstallerDocsSent;
        public string InstallCompleted;
        public string InstallVerifiedBy;
        public string WelcomeLetterDone;
        public string InstallRequestSaved;
        public string PanelSerials;
        public string InverterSerial;
        public string SecondInverterSerial;
        public string CertificateIssued;
        public string CertificateSaved;
        public string STCReceivedBy;
        public string STCCheckedBy;
        public string STCFormSaved;
        public string STCUploaded;
        public string STCUploadNumber;
        public string STCFU;
        public string STCNotes;
        public string InstallationComment;
        public string PVDNumber;
        public string PVDStatusID;
        public string STCApplied;
        public string BalanceReceived;
        public string Witholding;
        public string BalanceVerified;
        public string InvoicePaid;
        public string InstallerNotifiedMeter;
        public string CustNotifiedMeter;
        public string MeterElectrician;
        public string MeterInstallerFU;
        public string MeterInstallerNotes;
        public string MeterJobBooked;
        public string MeterCompleted;
        public string MeterInvoice;
        public string MeterInvoiceNo;
        public string InstallerInvNo;
        public string InstallerInvAmnt;
        public string InstallerInvDate;
        public string InstallerPayDate;
        public string MeterElecInvNo;
        public string MeterElecInvAmnt;
        public string MeterElecInvDate;
        public string MeterElecPayDate;
        public string MeterElecFollowUp;
        public string ElectricianInvoiceNotes;
        public string upsize_ts;
        public string SQ;
        public string MP;
        public string EB;
        public string PD;
        public string InvoiceStatusID;
        public string ProjectEnteredBy;
        public string UpdatedBy;
        public string DepositeOption;
        public string ST;
        public string CE;
        public string StatusComment;
        public string IsDeduct;
        public string StockDeductBy;
        public string StockDeductDate;
        public string PaymentTypeID;
        public string ApplicationDate;
        public string AppliedBy;
        public string PurchaseNo;
        public string DocumentSentDate;
        public string DocumentSentBy;
        public string DocumentReceivedDate;
        public string DocumentReceivedBy;
        public string DocumentVerified;
        public string SentBy;
        public string SentDate;
        public string PostalTrackingNumber;
        public string Remarks;
        public string ReceivedDate;
        public string ReceivedBy;
        public string PaymentVerified;
        public string InvDoc;
        public string InvDocDoor;
        public string PreExtraWork;
        public string PreAmount;
        public string PreApprovedBy;
        public string PaymentReceipt;
        public string PR;
        public string ActiveDate;
        public string SalesComm;
        public string STCFormSign;
        public string SerialNumbers;
        public string QuotationForm;
        public string CustomerAck;
        public string ComplianceCerti;
        public string CustomerAccept;
        public string EWRNumber;
        public string PatmentMethod;
        public string FlatPanels;
        public string PitchedPanels;
        public string SurveyCerti;
        public string ElecDistAppDate;
        public string ElecDistAppBy;
        public string InstInvDoc;
        public string CertiApprove;
        public string NewDate;
        public string NewNotes;
        public string IsFormBay;
        public string financewith;
        public string inverterqty;
        public string inverterqty2;
        public string inverterqty3;
        public string StockCategoryID;
        public string StockItemID;

        public string ElecDistributor;
        public string ElecRetailer;
        public string HouseType;
        public string RoofType;
        public string RoofAngle;
        public string PanelBrandName;
        public string InverterDetailsName;
        public string SecondInverterDetails;
        public string Contact;
        public string InvoiceStatus;
        public string SalesRepName;
        public string InstallerName;
        public string DesignerName;
        public string ElectricianName;
        public string FinanceWith;
        public string PaymentType;
        public string StoreName;
        public string Customer;
        public string FormbayId;
        public string unit_type;
        public string unit_number;
        public string street_number;
        public string street_address;
        public string street_name;
        public string street_type;
        public string street_suffix;
        public string mtcepaperwork;
        public string LinkProjectID;
        public string FormbayInstallBookingDate;
        public string beatquote;
        public string beatquotedoc;
        public string nearmap;
        public string nearmapdoc;
        public string SalesType;
        public string projecttype;
        public string projectcancel;
        public string PVDStatus;
        public string DocumentSentByName;
        public string DocumentReceivedByName;
        public string Empname1;
        public string FDA;
        public string readyactive;
        public string notes;
        public string paydate;
        public string SSCompleteDate;
        public string SSActiveDate;
        public string QuickForm;
        public string IsClickCustomer;
        public string HouseStayID;
        public string HouseStayDate;
        public string ComplainceCertificate;
        public string quickformGuid;
        public string GreenBotFlag;
        public string VicAppReference;
        public string VicDate;
        public string VicRebateNote;
        public string VicRebate;
        public string IsSMReady;
        public string NearBy;
        public string StreetCity;
        public string PVCapacity;
        public string StreetState;
        public string StreetPostCode;
        public string SubsidyPCRNumber;
        public string DistributerApplicationNumber;
        public string GEDANumber;
        public string ApplicationNo;
        public string estimatevalue_id;
        public string Meter_Estimate;
        public string Estimate_paid;
        public string Payment_due_Date;
        public string Status_For_Net_Meter;
        public string Taluka;
        public string District;
        public string TotalPayableAmount;
        public string TwentySubsidy;
        public string FourtySubsidy;
        public string ProjectSTatus;
        public string DiscomMeterno;
        public string DiscomCerificateMeterno;
        public string ApplicaionStatus;
        public string DocumentURL { get; set; }
        public string StreetArea { get; set; }
        public string Circle { get; set; }
        public string GUVNL_Reg_No { get; set; }
        public string SubDivisionId { get; set; }
        public string SolarMeterNumber { get; set; }
        public string RoofTopArea;
        public string InstallerMobile;
        public string InvoicePayDate;
        public string InvoicePayTotal;
        public string InstallBookingDateformeted;
        public string CPName;
        public string CPMobile;
        public string SalesRepMobile;
        public string SalesRepEmail;
        public string DivisionName;
        public string SubDivisionName;

    }

    public struct SttblContactsReportData
    {
        public string CustomerID;
        public string EmployeeID;
        public string CustStatusID;
        public string ContLeadStatusID;
        public string ContLeadCancelReasonID;
        public string ContTag;
        public string ContTag1;
        public string ContTag2;
        public string ContTag3;
        public string PromoTag;
        public string LeadPromo;
        public string Newsletter;
        public string UploadTag;
        public string ActiveTag;
        public string ReferrerTag;
        public string ContactEntered;
        public string ContactEnteredBy;
        public string ContTitle;
        public string ContMr;
        public string ContFirst;
        public string Taluka;
        public string District;
        public string AreaType;
        public string RoofTopArea;
        public string AvgMonUnitCons;
        public string AvgMonBill;
        public string Premises;
        public string ComMeterConn;
        public string Chapanels;
        public string ContMiddle;
        public string ContLast;
        public string ContMobile;
        public string ContPhone;
        public string ContHomePhone;
        public string ContFax;
        public string ContEmail;
        public string ContEmailLink;
        public string SkypeID;
        public string ContGone;
        public string ContGoneDate;
        public string SendEmail;
        public string SendMail;
        public string SendSMS;
        public string ContNotes;
        public string LeadFollowUp;
        public string OldCustomerID;
        public string Accreditation;
        public string ElecLicence;
        public string ElecLicenceExpires;
        public string Installer;
        public string Designer;
        public string Electrician;
        public string MeterElectrician;
        public string LinkID;
        public string InstallerActive;
        public string InstallerAvailability;
        public string DocStore;
        public string DocStoreDone;
        public string WelcomeSMSDone;
        public string RefBSB;
        public string RefAccount;
        public string upsize_ts;
        public string userid;
        public string ContactID;
        public string AL;
        public string ContMr2;
        public string ContFirst2;
        public string ContLast2;
        public string referralProgram;
        public string UserName;
        public string RoleName;
        public string formbayid;
        public string groupid;
    }

    public struct SttblStockItemsReportData
    {
        public string StockItemID;
        public string StockCategoryID;
        public string StockItem;
        public string StockManufacturer;
        public string StockModel;
        public string StockSeries;
        public string StockSize;
    }

    public ClsTelerikReportData()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static SttblProjectsReportData tblProjects_SelectByProjectID_ReportData(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_SelectByProjectID_ReportData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectsReportData details = new SttblProjectsReportData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.SalesRep = dr["SalesRep"].ToString();
            details.ProjectTypeID = dr["ProjectTypeID"].ToString();
            details.ProjectStatusID = dr["ProjectStatusID"].ToString();
            details.ProjectCancelID = dr["ProjectCancelID"].ToString();
            details.ProjectOnHoldID = dr["ProjectOnHoldID"].ToString();
            details.ProjectOpened = dr["ProjectOpened"].ToString();
            details.ProjectCancelled = dr["ProjectCancelled"].ToString();
            details.ProjectNumber = dr["ProjectNumber"].ToString();
            details.OldProjectNumber = dr["OldProjectNumber"].ToString();
            details.ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
            details.Project = dr["Project"].ToString();
            details.readyactive = dr["readyactive"].ToString();
            details.NextMaintenanceCall = dr["NextMaintenanceCall"].ToString();
            details.InstallAddress = dr["InstallAddress"].ToString();
            details.InstallCity = dr["InstallCity"].ToString();
            details.InstallState = dr["InstallState"].ToString();
            details.InstallPostCode = dr["InstallPostCode"].ToString();
            details.ProjectNotes = dr["ProjectNotes"].ToString();
            details.AdditionalSystem = dr["AdditionalSystem"].ToString();
            details.GridConnected = dr["GridConnected"].ToString();
            details.RebateApproved = dr["RebateApproved"].ToString();
            details.ReceivedCredits = dr["ReceivedCredits"].ToString();
            details.CreditEligible = dr["CreditEligible"].ToString();
            details.MoreThanOneInstall = dr["MoreThanOneInstall"].ToString();
            details.RequiredCompliancePaperwork = dr["RequiredCompliancePaperwork"].ToString();
            details.OutOfPocketDocs = dr["OutOfPocketDocs"].ToString();
            details.OwnerGSTRegistered = dr["OwnerGSTRegistered"].ToString();
            details.OwnerABN = dr["OwnerABN"].ToString();
            details.HouseTypeID = dr["HouseTypeID"].ToString();
            details.RoofTypeID = dr["RoofTypeID"].ToString();
            details.NetMeterId = dr["NetMeter"].ToString();
            details.RoofAngleID = dr["RoofAngleID"].ToString();
            details.InstallBase = dr["InstallBase"].ToString();
            details.StockAllocationStore = dr["StockAllocationStore"].ToString();
            details.StandardPack = dr["StandardPack"].ToString();
            details.PanelBrandID = dr["PanelBrandID"].ToString();
            details.PanelBrand = dr["PanelBrand"].ToString();
            details.PanelModel = dr["PanelModel"].ToString();
            details.PanelOutput = dr["PanelOutput"].ToString();
            details.PanelDetails = dr["PanelDetails"].ToString();
            details.InverterDetailsID = dr["InverterDetailsID"].ToString();
            details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
            details.ThirdInverterDetailsID = dr["ThirdInverterDetailsID"].ToString();
            details.InverterBrand = dr["InverterBrand"].ToString();
            details.InverterModel = dr["InverterModel"].ToString();
            details.InverterSeries = dr["InverterSeries"].ToString();
            details.InverterOutput = dr["InverterOutput"].ToString();
            details.SecondInverterOutput = dr["SecondInverterOutput"].ToString();
            details.ThirdInverterOutput = dr["ThirdInverterOutput"].ToString();
            details.TotalInverterOutput = dr["TotalInverterOutput"].ToString();
            details.InverterCert = dr["InverterCert"].ToString();
            details.InverterDetails = dr["InverterDetails"].ToString();
            details.SystemDetails = dr["SystemDetails"].ToString();
            details.NumberPanels = dr["NumberPanels"].ToString();
            details.PreviousNumberPanels = dr["PreviousNumberPanels"].ToString();
            details.PanelConfigNW = dr["PanelConfigNW"].ToString();
            details.PanelConfigOTH = dr["PanelConfigOTH"].ToString();
            details.SystemCapKW = dr["SystemCapKW"].ToString();
            details.STCMultiplier = dr["STCMultiplier"].ToString();
            details.STCZoneRating = dr["STCZoneRating"].ToString();
            details.STCNumber = dr["STCNumber"].ToString();
            details.STCValue = dr["STCValue"].ToString();
            details.ElecRetailerID = dr["ElecRetailerID"].ToString();
            details.ElecDistributorID = dr["ElecDistributorID"].ToString();
            details.DivisionId = dr["DivisionId"].ToString();
            details.ElecDistApplied = dr["ElecDistApplied"].ToString();
            details.ElecDistApplyMethod = dr["ElecDistApplyMethod"].ToString();
            details.ElecDistApplyBy = dr["ElecDistApplyBy"].ToString();
            details.ElecDistApplySentFrom = dr["ElecDistApplySentFrom"].ToString();
            details.ElecDistApproved = dr["ElecDistApproved"].ToString();
            details.ElecDistApprovelRef = dr["ElecDistApprovelRef"].ToString();
            details.ElecDistOK = dr["ElecDistOK"].ToString();
            details.RegPlanNo = dr["RegPlanNo"].ToString();
            details.LotNumber = dr["LotNumber"].ToString();
            details.Asbestoss = dr["Asbestoss"].ToString();
            details.MeterUG = dr["MeterUG"].ToString();
            details.MeterEnoughSpace = dr["MeterEnoughSpace"].ToString();
            details.SplitSystem = dr["SplitSystem"].ToString();
            details.CherryPicker = dr["CherryPicker"].ToString();
            details.TravelTime = dr["TravelTime"].ToString();
            details.VariationOther = dr["VariationOther"].ToString();
            details.VarRoofType = dr["VarRoofType"].ToString();
            details.VarRoofAngle = dr["VarRoofAngle"].ToString();
            details.VarHouseType = dr["VarHouseType"].ToString();
            details.VarAsbestos = dr["VarAsbestos"].ToString();
            details.VarMeterInstallation = dr["VarMeterInstallation"].ToString();
            details.VarMeterUG = dr["VarMeterUG"].ToString();
            details.VarTravelTime = dr["VarTravelTime"].ToString();
            details.HotWaterMeter = dr["HotWaterMeter"].ToString();
            details.SmartMeter = dr["SmartMeter"].ToString();
            details.VarSplitSystem = dr["VarSplitSystem"].ToString();
            details.VarEnoughSpace = dr["VarEnoughSpace"].ToString();
            details.VarCherryPicker = dr["VarCherryPicker"].ToString();
            details.VarOther = dr["VarOther"].ToString();
            details.NetTotal = dr["totalQuoteprice"].ToString();
            details.SpecialDiscount = dr["SpecialDiscount"].ToString();
            details.collectioncharge = dr["collectioncharge"].ToString();
            details.DiscomDate = dr["DiscomDate"].ToString();
            details.Bi_directionalDate = dr["Bi_directionalDate"].ToString();
            details.StreetCity = dr["StreetCity"].ToString();
            details.Circle = dr["Circle"].ToString();
            details.GUVNL_Reg_No = dr["GUVNL_Registeration_No"].ToString();
            details.SubDivisionId = dr["Subdivision"].ToString();
            
            details.DepositRequired = dr["DepositRequired"].ToString();
            details.TotalQuotePrice = dr["TotalQuotePrice"].ToString();
            details.PreviousTotalQuotePrice = dr["PreviousTotalQuotePrice"].ToString();
            details.InvoiceExGST = dr["InvoiceExGST"].ToString();
            details.InvoiceGST = dr["InvoiceGST"].ToString();
            details.BalanceGST = dr["BalanceGST"].ToString();
            details.FinanceWithID = dr["FinanceWithID"].ToString();
            details.ServiceValue = dr["ServiceValue"].ToString();
            details.QuoteSent = dr["QuoteSent"].ToString();
            details.QuoteSentNo = dr["QuoteSentNo"].ToString();
            details.QuoteAccepted = dr["QuoteAccepted"].ToString();
            details.SignedQuote = dr["SignedQuote"].ToString();
            details.MeterBoxPhotosSaved = dr["MeterBoxPhotosSaved"].ToString();
            details.ElecBillSaved = dr["ElecBillSaved"].ToString();
            details.ProposedDesignSaved = dr["ProposedDesignSaved"].ToString();
            details.FollowUp = dr["FollowUp"].ToString();
            details.FollowUpNote = dr["FollowUpNote"].ToString();
            details.InvoiceNumber = dr["InvoiceNumber"].ToString();
            details.InvoiceTag = dr["InvoiceTag"].ToString();
            details.InvoiceDoc = dr["InvoiceDoc"].ToString();
            details.InvoiceDetail = dr["InvoiceDetail"].ToString();
            details.InvoiceSent = dr["InvoiceSent"].ToString();
            details.InvoiceFU = dr["InvoiceFU"].ToString();
            details.InvoiceNotes = dr["InvoiceNotes"].ToString();
            details.InvRefund = dr["InvRefund"].ToString();
            details.InvRefunded = dr["InvRefunded"].ToString();
            details.InvRefundBy = dr["InvRefundBy"].ToString();
            details.DepositReceived = dr["DepositReceived"].ToString();
            details.DepositAmount = dr["DepositAmount"].ToString();
            details.ReceiptSent = dr["ReceiptSent"].ToString();
            details.SalesCommPaid = dr["SalesCommPaid"].ToString();
            details.InstallBookingDate = dr["InstallBookingDate"].ToString();
            details.MeterIncluded = dr["MeterIncluded"].ToString();
            details.MeterAppliedRef = dr["MeterAppliedRef"].ToString();
            details.MeterAppliedDate = dr["MeterAppliedDate"].ToString();
            details.MeterAppliedTime = dr["MeterAppliedTime"].ToString();
            details.MeterAppliedMethod = dr["MeterAppliedMethod"].ToString();
            details.MeterApprovedDate = dr["MeterApprovedDate"].ToString();
            details.MeterApprovalNo = dr["MeterApprovalNo"].ToString();
            details.MeterPhase = dr["MeterPhase"].ToString();
            details.OffPeak = dr["OffPeak"].ToString();
            details.NMINumber = dr["NMINumber"].ToString();
            details.meterupgrade = dr["meterupgrade"].ToString();
            details.MeterNumber1 = dr["MeterNumber1"].ToString();
            details.MeterNumber2 = dr["MeterNumber2"].ToString();
            details.MeterNumber3 = dr["MeterNumber3"].ToString();
            details.MeterNumber4 = dr["MeterNumber4"].ToString();
            details.MeterFU = dr["MeterFU"].ToString();
            details.MeterNotes = dr["MeterNotes"].ToString();
            details.OldSystemDetails = dr["OldSystemDetails"].ToString();
            details.REXAppliedRef = dr["REXAppliedRef"].ToString();
            details.REXAppliedDate = dr["REXAppliedDate"].ToString();
            details.REXStatusID = dr["REXStatusID"].ToString();
            details.REXApprovalNotes = dr["REXApprovalNotes"].ToString();
            details.REXApprovalFU = dr["REXApprovalFU"].ToString();
            details.STCPrice = dr["STCPrice"].ToString();
            details.RECRebate = dr["RECRebate"].ToString();
            details.BalanceRequested = dr["BalanceRequested"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.InstallAM1 = dr["InstallAM1"].ToString();
            details.InstallPM1 = dr["InstallPM1"].ToString();
            details.InstallAM2 = dr["InstallAM2"].ToString();
            details.InstallPM2 = dr["InstallPM2"].ToString();
            details.InstallDays = dr["InstallDays"].ToString();
            details.STCFormsDone = dr["STCFormsDone"].ToString();
            details.InstallDocsReceived = dr["InstallDocsReceived"].ToString();
            details.InstallerNotified = dr["InstallerNotified"].ToString();
            details.CustNotifiedInstall = dr["CustNotifiedInstall"].ToString();
            details.InstallerFollowUp = dr["InstallerFollowUp"].ToString();
            details.InstallerNotes = dr["InstallerNotes"].ToString();
            details.InstallerDocsSent = dr["InstallerDocsSent"].ToString();
            details.InstallCompleted = dr["InstallCompleted"].ToString();
            details.InstallVerifiedBy = dr["InstallVerifiedBy"].ToString();
            details.WelcomeLetterDone = dr["WelcomeLetterDone"].ToString();
            details.InstallRequestSaved = dr["InstallRequestSaved"].ToString();
            details.PanelSerials = dr["PanelSerials"].ToString();
            details.InverterSerial = dr["InverterSerial"].ToString();
            details.SecondInverterSerial = dr["SecondInverterSerial"].ToString();
            details.CertificateIssued = dr["CertificateIssued"].ToString();
            details.CertificateSaved = dr["CertificateSaved"].ToString();
            details.STCReceivedBy = dr["STCReceivedBy"].ToString();
            details.STCCheckedBy = dr["STCCheckedBy"].ToString();
            details.STCFormSaved = dr["STCFormSaved"].ToString();
            details.STCUploaded = dr["STCUploaded"].ToString();
            details.STCUploadNumber = dr["STCUploadNumber"].ToString();
            details.STCFU = dr["STCFU"].ToString();
            details.STCNotes = dr["STCNotes"].ToString();
            details.InstallationComment = dr["InstallationComment"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.PVDStatusID = dr["PVDStatusID"].ToString();
            details.STCApplied = dr["STCApplied"].ToString();
            details.BalanceReceived = dr["BalanceReceived"].ToString();
            details.Witholding = dr["Witholding"].ToString();
            details.BalanceVerified = dr["BalanceVerified"].ToString();
            details.InvoicePaid = dr["InvoicePaid"].ToString();
            details.InstallerNotifiedMeter = dr["InstallerNotifiedMeter"].ToString();
            details.CustNotifiedMeter = dr["CustNotifiedMeter"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.MeterInstallerFU = dr["MeterInstallerFU"].ToString();
            details.MeterInstallerNotes = dr["MeterInstallerNotes"].ToString();
            details.MeterJobBooked = dr["MeterJobBooked"].ToString();
            details.MeterCompleted = dr["MeterCompleted"].ToString();
            details.MeterInvoice = dr["MeterInvoice"].ToString();
            details.MeterInvoiceNo = dr["MeterInvoiceNo"].ToString();
            details.InstallerInvNo = dr["InstallerInvNo"].ToString();
            details.InstallerInvAmnt = dr["InstallerInvAmnt"].ToString();
            details.InstallerInvDate = dr["InstallerInvDate"].ToString();
            details.InstallerPayDate = dr["InstallerPayDate"].ToString();
            details.MeterElecInvNo = dr["MeterElecInvNo"].ToString();
            details.MeterElecInvAmnt = dr["MeterElecInvAmnt"].ToString();
            details.MeterElecInvDate = dr["MeterElecInvDate"].ToString();
            details.MeterElecPayDate = dr["MeterElecPayDate"].ToString();
            details.MeterElecFollowUp = dr["MeterElecFollowUp"].ToString();
            details.ElectricianInvoiceNotes = dr["ElectricianInvoiceNotes"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.SQ = dr["SQ"].ToString();
            details.MP = dr["MP"].ToString();
            details.EB = dr["EB"].ToString();
            details.PD = dr["PD"].ToString();
            details.InvoiceStatusID = dr["InvoiceStatusID"].ToString();
            details.ProjectEnteredBy = dr["ProjectEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.DepositeOption = dr["DepositeOption"].ToString();
            details.ST = dr["ST"].ToString();
            details.CE = dr["CE"].ToString();
            details.StatusComment = dr["StatusComment"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();
            details.StockDeductBy = dr["StockDeductBy"].ToString();
            details.StockDeductDate = dr["StockDeductDate"].ToString();
            details.PaymentTypeID = dr["PaymentTypeID"].ToString();
            details.ApplicationDate = dr["ApplicationDate"].ToString();
            details.AppliedBy = dr["AppliedBy"].ToString();
            details.PurchaseNo = dr["PurchaseNo"].ToString();
            details.DocumentSentDate = dr["DocumentSentDate"].ToString();
            details.DocumentSentBy = dr["DocumentSentBy"].ToString();
            details.DocumentReceivedDate = dr["DocumentReceivedDate"].ToString();
            details.DocumentReceivedBy = dr["DocumentReceivedBy"].ToString();
            details.DocumentVerified = dr["DocumentVerified"].ToString();
            details.SentBy = dr["SentBy"].ToString();
            details.SentDate = dr["SentDate"].ToString();
            details.PostalTrackingNumber = dr["PostalTrackingNumber"].ToString();
            details.Remarks = dr["Remarks"].ToString();
            details.ReceivedDate = dr["ReceivedDate"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.PaymentVerified = dr["PaymentVerified"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvDocDoor = dr["InvDocDoor"].ToString();
            details.PreExtraWork = dr["PreExtraWork"].ToString();
            details.PreAmount = dr["PreAmount"].ToString();
            details.PreApprovedBy = dr["PreApprovedBy"].ToString();
            details.PaymentReceipt = dr["PaymentReceipt"].ToString();
            details.PR = dr["PR"].ToString();
            details.ActiveDate = dr["ActiveDate"].ToString();
            details.SalesComm = dr["SalesComm"].ToString();
            details.STCFormSign = dr["STCFormSign"].ToString();
            details.SerialNumbers = dr["SerialNumbers"].ToString();
            details.QuotationForm = dr["QuotationForm"].ToString();
            details.CustomerAck = dr["CustomerAck"].ToString();
            details.ComplianceCerti = dr["ComplianceCerti"].ToString();
            details.CustomerAccept = dr["CustomerAccept"].ToString();
            details.EWRNumber = dr["EWRNumber"].ToString();
            details.PatmentMethod = dr["PatmentMethod"].ToString();
            details.FlatPanels = dr["FlatPanels"].ToString();
            details.PitchedPanels = dr["PitchedPanels"].ToString();
            details.SurveyCerti = dr["SurveyCerti"].ToString();
            details.ElecDistAppDate = dr["ElecDistAppDate"].ToString();
            details.ElecDistAppBy = dr["ElecDistAppBy"].ToString();
            details.InstInvDoc = dr["InstInvDoc"].ToString();
            details.CertiApprove = dr["CertiApprove"].ToString();
            details.NewDate = dr["NewDate"].ToString();
            details.NewNotes = dr["NewNotes"].ToString();
            details.IsFormBay = dr["IsFormBay"].ToString();
            details.financewith = dr["financewith"].ToString();
            details.inverterqty = dr["inverterqty"].ToString();
            details.inverterqty2 = dr["inverterqty2"].ToString();
            details.inverterqty3 = dr["inverterqty3"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();

            details.SecondInverterDetails = dr["SecondInverterDetails"].ToString();
            details.ElecDistributor = dr["ElecDistributor"].ToString();
            details.ElecRetailer = dr["ElecRetailer"].ToString();
            details.HouseType = dr["HouseType"].ToString();
            details.RoofType = dr["RoofType"].ToString();
            details.RoofAngle = dr["RoofAngle"].ToString();
            details.PanelBrandName = dr["PanelBrandName"].ToString();
            details.InverterDetailsName = dr["InverterDetailsName"].ToString();
            details.Contact = dr["Contact"].ToString();
            details.InvoiceStatus = dr["InvoiceStatus"].ToString();
            details.SalesRepName = dr["SalesRepName"].ToString();
            details.InstallerName = dr["InstallerName"].ToString();
            details.DesignerName = dr["DesignerName"].ToString();
            details.ElectricianName = dr["ElectricianName"].ToString();
            details.FinanceWith = dr["FinanceWith"].ToString();
            details.PaymentType = dr["PaymentType"].ToString();
            details.StoreName = dr["StoreName"].ToString();
            details.Customer = dr["Customer"].ToString();

            details.FormbayId = dr["FormbayId"].ToString();
            details.unit_type = dr["unit_type"].ToString();
            details.unit_number = dr["unit_number"].ToString();
            details.street_number = dr["street_number"].ToString();
            details.street_address = dr["street_address"].ToString();
            details.street_name = dr["street_name"].ToString();
            details.street_type = dr["street_type"].ToString();
            details.street_suffix = dr["street_suffix"].ToString();
            details.mtcepaperwork = dr["mtcepaperwork"].ToString();
            details.LinkProjectID = dr["LinkProjectID"].ToString();
            details.FormbayInstallBookingDate = dr["FormbayInstallBookingDate"].ToString();
            details.beatquote = dr["beatquote"].ToString();
            details.beatquotedoc = dr["beatquotedoc"].ToString();
            details.nearmap = dr["nearmapcheck"].ToString();
            details.nearmapdoc = dr["nearmapdoc"].ToString();
            details.SalesType = dr["SalesType"].ToString();
            details.projecttype = dr["projecttype"].ToString();
            details.projectcancel = dr["projectcancel"].ToString();
            details.PVDStatus = dr["PVDStatus"].ToString();
            details.DocumentSentByName = dr["DocumentSentByName"].ToString();
            details.DocumentReceivedByName = dr["DocumentReceivedByName"].ToString();
            details.Empname1 = dr["Empname1"].ToString();
            details.FDA = dr["FDA"].ToString();
            details.notes = dr["notes"].ToString();
            details.paydate = dr["paydate"].ToString();
            details.SSActiveDate = dr["SSActiveDate"].ToString();
            details.SSCompleteDate = dr["SSCompleteDate"].ToString();
            details.QuickForm = dr["QuickForm"].ToString();
            details.IsClickCustomer = dr["IsClickCustomer"].ToString();


            details.HouseStayDate = dr["HouseStayDate"].ToString();
            details.HouseStayID = dr["HouseStayID"].ToString();
            details.ComplainceCertificate = dr["ComplainceCertificate"].ToString();
            details.quickformGuid = dr["quickformGuid"].ToString();
            details.GreenBotFlag = dr["GreenBotFlag"].ToString();
            details.VicAppReference = dr["VicAppReference"].ToString();
            details.VicDate = dr["VicDate"].ToString();
            details.VicRebateNote = dr["VicRebateNote"].ToString();
            details.VicRebate = dr["VicRebate"].ToString();
            details.IsSMReady = dr["IsSMReady"].ToString();
            details.NearBy = dr["NearBy"].ToString();
            details.StreetCity = dr["StreetCity"].ToString();
            details.PVCapacity = dr["PVCapacity"].ToString();
            details.ApplicationNo = dr["DistributerApplicationNumber"].ToString();
            details.StreetState = dr["StreetState"].ToString();
            details.StreetPostCode = dr["StreetPostCode"].ToString();

            details.SubsidyPCRNumber = dr["SubsidyPCRNumber"].ToString();
            details.DistributerApplicationNumber = dr["DistributerApplicationNumber"].ToString();
            details.GEDANumber = dr["GEDANumber"].ToString();

            details.estimatevalue_id = dr["estimatevalue_id"].ToString();
            details.Meter_Estimate = dr["Meter_Estimate"].ToString();
            details.Estimate_paid = dr["Estimate_paid"].ToString();
            details.Payment_due_Date = dr["Payment_due_Date"].ToString();
            details.Status_For_Net_Meter = dr["Status_For_Net_Meter"].ToString();
            details.Taluka = dr["Taluka"].ToString();
            details.District = dr["District"].ToString();

            details.TotalPayableAmount = dr["TotalPayableAmount"].ToString();
            details.FourtySubsidy = dr["FortySubsidy"].ToString();
            details.TwentySubsidy = dr["TwentySubsidy"].ToString();
            details.ProjectSTatus = dr["ProjectSTatus"].ToString();
            details.DiscomCerificateMeterno = dr["DiscomCerificateMeterno"].ToString();
            details.DiscomMeterno = dr["DiscomMeterno"].ToString();
            details.ApplicaionStatus = dr["Applcation_Status"].ToString();
            details.DocumentURL = dr["DocumentURL"].ToString();

            details.RoofTopArea = dr["rooftoparea"].ToString();
            details.InstallerMobile = dr["InstallerMobile"].ToString();
            details.InvoicePayDate = dr["InvoicePayDate"].ToString();
            details.InvoicePayTotal = dr["InvoicePayTotal"].ToString();
            details.InstallBookingDateformeted = dr["InstallBookingDateformeted"].ToString();

            details.CPName = dr["CPName"].ToString();
            details.CPMobile = dr["CPMobile"].ToString();
            details.SalesRepMobile = dr["SalesRepMobile"].ToString();
            details.SalesRepEmail = dr["SalesRepEmail"].ToString();
            details.DivisionName = dr["DivisionName"].ToString();
            details.SubDivisionName = dr["SubDivisionName"].ToString();


        }
        // return structure details
        return details;
    }

    public static SttblContactsReportData tblContacts_SelectByContactID_ReportData(string ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContacts_SelectByContactID_ReportData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != String.Empty)
        {
            param.Value = ContactID;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblContactsReportData details = new SttblContactsReportData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.CustStatusID = dr["CustStatusID"].ToString();
            details.ContLeadStatusID = dr["ContLeadStatusID"].ToString();
            details.ContLeadCancelReasonID = dr["ContLeadCancelReasonID"].ToString();
            details.ContTag = dr["ContTag"].ToString();
            details.ContTag1 = dr["ContTag1"].ToString();
            details.ContTag2 = dr["ContTag2"].ToString();
            details.ContTag3 = dr["ContTag3"].ToString();
            details.PromoTag = dr["PromoTag"].ToString();
            details.LeadPromo = dr["LeadPromo"].ToString();
            details.Newsletter = dr["Newsletter"].ToString();
            details.UploadTag = dr["UploadTag"].ToString();
            details.ActiveTag = dr["ActiveTag"].ToString();
            details.ReferrerTag = dr["ReferrerTag"].ToString();
            details.ContactEntered = dr["ContactEntered"].ToString();
            details.ContactEnteredBy = dr["ContactEnteredBy"].ToString();
            details.ContTitle = dr["ContTitle"].ToString();
            details.ContMr = dr["ContMr"].ToString();
            details.ContFirst = dr["ContFirst"].ToString();
            details.ContLast = dr["ContLast"].ToString();
            details.ContMiddle = dr["ContMiddle"].ToString();
            details.ContMobile = dr["ContMobile"].ToString();
            details.ContPhone = dr["ContPhone"].ToString();
            details.ContHomePhone = dr["ContHomePhone"].ToString();
            details.ContFax = dr["ContFax"].ToString();
            details.ContEmail = dr["ContEmail"].ToString();
            details.ContEmailLink = dr["ContEmailLink"].ToString();
            details.SkypeID = dr["SkypeID"].ToString();
            details.ContGone = dr["ContGone"].ToString();
            details.ContGoneDate = dr["ContGoneDate"].ToString();
            details.SendEmail = dr["SendEmail"].ToString();
            details.SendMail = dr["SendMail"].ToString();
            details.SendSMS = dr["SendSMS"].ToString();
            details.ContNotes = dr["ContNotes"].ToString();
            details.LeadFollowUp = dr["LeadFollowUp"].ToString();
            details.OldCustomerID = dr["OldCustomerID"].ToString();
            details.Accreditation = dr["Accreditation"].ToString();
            details.ElecLicence = dr["ElecLicence"].ToString();
            details.ElecLicenceExpires = dr["ElecLicenceExpires"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.Designer = dr["Designer"].ToString();
            details.Electrician = dr["Electrician"].ToString();
            details.MeterElectrician = dr["MeterElectrician"].ToString();
            details.LinkID = dr["LinkID"].ToString();
            details.InstallerActive = dr["InstallerActive"].ToString();
            details.InstallerAvailability = dr["InstallerAvailability"].ToString();
            details.DocStore = dr["DocStore"].ToString();
            details.DocStoreDone = dr["DocStoreDone"].ToString();
            details.WelcomeSMSDone = dr["WelcomeSMSDone"].ToString();
            details.RefBSB = dr["RefBSB"].ToString();
            details.RefAccount = dr["RefAccount"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.userid = dr["userid"].ToString();
            details.AL = dr["AL"].ToString();
            details.ContMr2 = dr["ContMr2"].ToString();
            details.ContFirst2 = dr["ContFirst2"].ToString();
            details.ContLast2 = dr["ContLast2"].ToString();

            details.referralProgram = dr["referralProgram"].ToString();

            details.UserName = dr["UserName"].ToString();
            details.RoleName = dr["RoleName"].ToString();
            details.formbayid = dr["formbayid"].ToString();
            details.groupid = dr["groupid"].ToString();
            
        }
        // return structure details
        return details;
    }

    public static DataTable tblInvoicePayments_GetDataByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoicePayments_GetDataByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_Installation_Project_ScannedSerial_GetPenelSerialNoByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Installation_Project_ScannedSerial_GetPenelSerialNoByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_Installation_Project_ScannedSerial_GetInverterSerialNoByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_Installation_Project_ScannedSerial_GetInverterSerialNoByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static SttblStockItemsReportData tblStockItems_SelectByStockItemID_ReportData(string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStockItems_SelectByStockItemID_ReportData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != String.Empty)
        {
            param.Value = StockItemID;
        }
        else
        {
            param.Value = DBNull.Value;
        }
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStockItemsReportData details = new SttblStockItemsReportData();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StockItemID = dr["StockItemID"].ToString();
            details.StockCategoryID = dr["StockCategoryID"].ToString();
            details.StockItem = dr["StockItem"].ToString();
            details.StockManufacturer = dr["StockManufacturer"].ToString();
            details.StockModel = dr["StockModel"].ToString();
            details.StockSeries = dr["StockSeries"].ToString();
            details.StockSize = dr["StockSize"].ToString();
        }
        // return structure details
        return details;
    }
}