﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using net.openstack.Core.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TestData
/// </summary>
/// 
public class SyncTableData
{
    static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
    static readonly string ApplicationName = "Sync tblWebDownload";
    static readonly string sheet = "Sheet1";
    static readonly string SpreadsheetId = "1tNnJ0g2KWSIFoOx8BDmDnbLrnTZMODrlop-ab-RRN-I";
    static SheetsService service;

    public static void Main()
    {
        Init();
        AddRow();
        //UpdateCell();
    }

    static void Init()
    {
        GoogleCredential credential;
        //Reading Credentials File...
        string filepath = HttpContext.Current.Server.MapPath("app_client_secret.json");
        using (var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
        {
            credential = GoogleCredential.FromStream(stream)
                .CreateScoped(Scopes);
        }

        //Creating Google Sheets API service...
        service = new SheetsService(new BaseClientService.Initializer()
        {
            HttpClientInitializer = credential,
            ApplicationName = ApplicationName,
        });
    }

    public static void AddRow()
    {
        //DataTable dt = ClstblWebDownload.GettblWebDownload();

        //List<object> list = null;
        //DataTableReader dr = dt.CreateDataReader();

        //var range = $"{sheet}!A:AF";
        //var valueRange = new ValueRange();

        //if (dr.HasRows)
        //{
        //    //Customer customer = new Customer();
        //    list = new List<object>();
        //    while (dr.Read())
        //    {
        //        list = new List<object>() { Convert.ToString(dr["WebDownloadID"]), Convert.ToString(dr["Customer"]), Convert.ToString(dr["ContFirst"]), Convert.ToString(dr["ContLast"]), Convert.ToString(dr["ContEmail"]), Convert.ToString(dr["CustPhone"]), Convert.ToString(dr["ContMobile"]), Convert.ToString(dr["Address"]), Convert.ToString(dr["City"]), Convert.ToString(dr["State"]), Convert.ToString(dr["PostCode"]), Convert.ToString(dr["Source"]), Convert.ToString(dr["System"]), Convert.ToString(dr["Roof"]), Convert.ToString(dr["Angle"]), Convert.ToString(dr["Story"]), Convert.ToString(dr["HouseAge"]), Convert.ToString(dr["Notes"]), Convert.ToString(dr["EntryDate"]), Convert.ToString(dr["PreferredTime"]), Convert.ToString(dr["Uploaded"]), Convert.ToString(dr["Duplicate"]), Convert.ToString(dr["NotDuplicate"]), Convert.ToString(dr["SalesTeamID"]), Convert.ToString(dr["EmployeeID"]), Convert.ToString(dr["LinkID"]), Convert.ToString(dr["FormatFixed"]), Convert.ToString(dr["upsize_ts"]), Convert.ToString(dr["SubSource"]), Convert.ToString(dr["ReadFlag"]), Convert.ToString(dr["CustomerLinkID"]), Convert.ToString(dr["gclid"]) };

        //        valueRange.Values = new List<IList<object>> { list };
        //        var appendRequest = service.Spreadsheets.Values.Append(valueRange, SpreadsheetId, range);
        //        appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
        //        var appendReponse = appendRequest.Execute();
        //    }
        //}
        
    }

    static void UpdateCell()
    {
        //// Setting Cell Name...
        //var range = $"{sheet}!C5";
        //var valueRange = new ValueRange();

        //// Setting Cell Value...
        //var oblist = new List<object>() { "32" };
        //valueRange.Values = new List<IList<object>> { oblist };

        //// Performing Update Operation...
        //var updateRequest = service.Spreadsheets.Values.Update(valueRange, SpreadsheetId, range);
        //updateRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
        //var appendReponse = updateRequest.Execute();
    }
}