﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsDistrictmaster
/// </summary>
public class ClsDistrictmaster
{
    public ClsDistrictmaster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public struct SttblDistrict
    {
        
        public string DistrictName;
        public string State;
        public string IsActive;

    }

    public static DataTable tblDistrict_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tblDistrict_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_GetDataBySearch" +
            "";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }


    public static int tblDistrict_Exists(string DistrictName)

    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";
        param.Value = DistrictName;

        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblDistrict_Insert(String StateId, String DistrictName, String Active)
            {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StateID";
        if (StateId != string.Empty)
            param.Value = StateId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";

        param.Value = DistrictName;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblDistrict_ExistsById(string DistrictName, string Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";
        param.Value = DistrictName;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static bool tblDistrict_Update(string Id, String StateId, String DistrictName, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StateID";
        if (StateId != string.Empty)
            param.Value = StateId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DistrictName";
        param.Value = DistrictName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static SttblDistrict tblDistrict_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblDistrict details = new SttblDistrict();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details

            details.State = dr["StateId"].ToString();
            details.DistrictName = dr["DistrictName"].ToString();
            details.IsActive = dr["Active"].ToString();
            //details.Seq = dr["Seq"].ToString();

        }

        // return structure details
        return details;
    }

    public static bool tblDistrict_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDistrict_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }



}