using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblRoofAngles
	{
		public string RoofAngle;
		public string Active;
		public string Seq;
		public string Variation;

	}


public class ClstblRoofAngles
{
	public static SttblRoofAngles tblRoofAngles_SelectByRoofAngleID (String RoofAngleID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblRoofAngles_SelectByRoofAngleID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@RoofAngleID";
		param.Value = RoofAngleID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblRoofAngles details = new SttblRoofAngles();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.RoofAngle = dr["RoofAngle"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();
			details.Variation = dr["Variation"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblRoofAngles_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblRoofAngles_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblRoofAngles_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblRoofAngles_SelectByRoofAngle(string RoofAngle)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_SelectByRoofAngle";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofAngle";
        param.Value = RoofAngle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
	public static int tblRoofAngles_Insert ( String RoofAngle, String Active, String Seq, String Variation)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblRoofAngles_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@RoofAngle";
		param.Value = RoofAngle;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Variation";
		param.Value = Variation;
		param.DbType = DbType.Decimal;
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblRoofAngles_Update (string RoofAngleID, String RoofAngle, String Active, String Seq, String Variation)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblRoofAngles_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@RoofAngleID";
		param.Value = RoofAngleID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@RoofAngle";
		param.Value = RoofAngle;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Seq";
		param.Value = Seq;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Variation";
		param.Value = Variation;
		param.DbType = DbType.Decimal;
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblRoofAngles_Delete (string RoofAngleID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblRoofAngles_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@RoofAngleID";
		param.Value = RoofAngleID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}



    public static int tblRoofAngles_Exists(string RoofAngle)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofAngle";
        param.Value = RoofAngle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblRoofAngles_ExistsById(string RoofAngle, string RoofAngleID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@RoofAngle";
        param.Value = RoofAngle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RoofAngleID";
        param.Value = RoofAngleID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblRoofAngles_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblRoofAngles_GetDataBySearch(string alpha,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblRoofAngles_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}