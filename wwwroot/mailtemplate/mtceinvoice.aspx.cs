﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class mailtemplate_mtceinvoice : System.Web.UI.Page
{
    protected string SiteURL;
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            string ProjectID = Request.QueryString["id"]; 
            //string ProjectID = "-2146260946";
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

            lblDate.Text = string.Format("{0:dd MMM yyyy}", DateTime.Now.AddHours(14));
            lblInvoiceNumber.Text = stPro.ProjectNumber;
            lblContact.Text = stCust.Customer;
            lblPostalAddress.Text = stCust.PostalAddress;
            lblPostalAddress2.Text = stCust.PostalCity + ", " + stCust.PostalState + ", " + stCust.PostalPostCode;
            lblInstallAddress.Text = stPro.InstallAddress;
            lblInstallAddress2.Text = stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;
            lblPanelDetails.Text = stPro.PanelDetails;
            lblInverterDetails.Text = stPro.InverterDetails;

            try
            {
                decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
                try
                {
                    lblTotalQuotePrice.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
                }
                catch { }
            }
            catch { }
            //try
            //{
            //    lblRECRebate.Text = SiteConfiguration.ChangeCurrencyVal(stPro.RECRebate);
            //}
            //catch { }
            //try
            //{
            //    lblNetCost.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            //}
            //catch { }

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            decimal paiddate = 0;
            decimal balown = 0;
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
                try
                {
                    lblDepositPaid.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
                }
                catch { }
                try
                {
                    lblBalanceDue.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
                }
                catch { }
            }
            else
            {
                lblDepositPaid.Text = "0";
                try
                {
                    lblBalanceDue.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
                }
                catch { }
            }
            try
            {
                lblBalanceGST.Text = SiteConfiguration.ChangeCurrencyVal(stPro.InvoiceGST);
            }
            catch { }
        }
    }
}