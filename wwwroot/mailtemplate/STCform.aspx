﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="STCform.aspx.cs" Inherits="mailtemplate_STCform" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="font-size: 11px; width: 100%;">
                <div style="font-size: 11px; width: 960px; margin: 0px auto;">
                    <div style="border: #000 solid 1px;">
                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                width="124" height="100" />
                        </div>
                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                T/A THINK GREEN SOLAR</h2>
                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                P O Box 570, Archerfield, QLD,4108<br />
                                ABN: 95 130 845 199 Pho: 1300 959 013<br />
                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a>
                            </p>
                        </div>
                        <div style="float: left; text-align: center; padding: 8px;">
                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;">STC Assignment Form<br />
                                PV Solar</h2>
                        </div>
                        <div style="float: none; clear: both;"></div>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 11px; font-family: 'OpenSansLight', sans-serif; font-size: 13px;">
                        <tr>
                            <td colspan="2" style="border-right: solid 1px #000; border-left: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 4px 10px 4px 8px; background: #d6d6d6; font-size: 12px;"><strong>OWNER DETAILS</strong></td>
                            <td colspan="4" align="center" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px 8px 5px 10px; background: #d6d6d6;"><strong>Quote No :
              <asp:Label ID="lblManualQuoteNo" runat="server"></asp:Label>
                            </strong></td>
                            <td colspan="6" align="center" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px 8px 5px 10px; background: #d6d6d6;"><strong>Customer No :
              <asp:Label ID="lblProjectNumber" runat="server"></asp:Label>
                            </strong></td>
                        </tr>
                        <tr>
                            <td width="130" align="left" valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">First Name : </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContFirst" runat="server"></asp:Label></td>
                            <td width="106" align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Last Name : </td>
                            <td width="133" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContLast" runat="server"></asp:Label></td>
                            <td width="162" align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Company : </td>
                            <td colspan="6" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <asp:Label ID="lblCustomer" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"
                                width="13%">Postal Address : </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblPostalAddressCity" runat="server"></asp:Label></td>
                            <td align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">State : </td>
                            <td colspan="2" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblPostalState" runat="server"></asp:Label></td>
                            <td width="145" align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Post Code : </td>
                            <td width="95" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblPostalPostCode" runat="server"></asp:Label></td>
                            <td width="48" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Country:</td>
                            <td width="48" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Aus</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Contact No : </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblCustPhone" runat="server"></asp:Label></td>
                            <td align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Fax No : </td>
                            <td align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblCustFax" runat="server"></asp:Label></td>
                            <td align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Email : </td>
                            <td colspan="6" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContEmail" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Install Address : </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallAddressCity" runat="server"></asp:Label></td>
                            <td align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">State : </td>
                            <td colspan="2" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallState" runat="server"></asp:Label></td>
                            <td align="right" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Post Code: </td>
                            <td align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallPostCode" runat="server"></asp:Label></td>
                            <td align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Country:</td>
                            <td align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Aus</td>
                        </tr>
                        <tr>
                            <td colspan="5" style="border-right: solid 1px #000; border-left: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; background: #d6d6d6; font-size: 12px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="70%" style="padding: 5px; border-right: solid 1px #000;"><strong>SYSTEM DETAILS:</strong>&nbsp;&nbsp;Is this system <b>ADDITIONAL</b>?</td>
                                        <td style="padding: 5px 5px 5px 10px;" bgcolor="#ffffff">
                                            <asp:Label ID="lblAdditional" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="7" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; background: #d6d6d6; font-size: 12px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="70%" style="padding: 5px; border-right: solid 1px #000;">Are you installing a COMPLETE Unit? </td>
                                        <td style="padding: 5px 5px 5px 10px;" bgcolor="#ffffff">
                                            <asp:Label ID="lblunit" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">(Are you adding extra capacity panels or completely own system which is in addition
              to an existing system for this address?)</td>
                            <td colspan="7" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">(Adding additional capacity panels to an existing system is NOT considered a complete
              unit)</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px;" bgcolor="#d6d6d6">If YES, the system is additional, please specify where the panels or system is in
              relation to the existing system: (e.g. additional 8 panels added to the end of existing
              8 panels) </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>New System Location:&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value="">
                                            &nbsp;</td>
                                        <td>E&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value=""></td>
                                        <td>N&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value=""></td>
                                        <td>W&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value="">
                                            &nbsp;</td>
                                        <td>Others</td>
                                        <td style="border-bottom: #000000  solid 1px;" width="120">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="7" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>Old System Location:&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value="">
                                            &nbsp;</td>
                                        <td>E&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value=""></td>
                                        <td>N&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value=""></td>
                                        <td>W&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="" type="checkbox" value="">
                                            &nbsp;</td>
                                        <td>Others</td>
                                        <td style="border-bottom: #000000  solid 1px;" width="120">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" bgcolor="#d6d6d6" align="center" valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Is the System installed on or as</strong></td>
                                        <td align="center" bgcolor="#d6d6d6" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Property Type</strong></td>
                                        <td align="center" bgcolor="#d6d6d6" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Story</strong></td>
                                        <td align="center" bgcolor="#d6d6d6" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>System type</strong></td>
                                        <td colspan="3" bgcolor="#d6d6d6" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;" width="492px"><strong>STC Deeming Period:</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="2" valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="23">
                                                        <asp:CheckBox ID="chkBuilding" runat="server" /></td>
                                                    <td>Building or Structure</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <asp:CheckBox ID="chkGround" runat="server" /></td>
                                                    <td>Ground Mounted or free Standing</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td rowspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="23">
                                                        <asp:CheckBox ID="chkResidential" runat="server" /></td>
                                                    <td>Residential</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>School</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <asp:CheckBox ID="chkCommercial" runat="server" /></td>
                                                    <td>Commercial</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td rowspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Single</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Multi</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td rowspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value="" checked="checked"></td>
                                                    <td>Solar Panel</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Wind</td>
                                                </tr>
                                                <tr>
                                                    <td width="23">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Hydro</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">1 Year</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">5 Years</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6"><strong>15 Years</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">15 years deeming can only be created Panel if<br />
                                            installed in the past 12 months.</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Panel Brand:</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblPanelBrand" runat="server"></asp:Label></td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Inverter Brand:</td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblInverterBrand" runat="server"></asp:Label></td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Have you received or been approved for a REBATE<br />
                                            or financial assistance (including Solar Credits) for<br />
                                            any small generation unit at this address?</td>
                                        <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblREB" runat="server" Text="REB"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Panel Model:</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblPanelModel" runat="server"></asp:Label></td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Inverter Series:</td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblInverterSeries" runat="server"></asp:Label></td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Have you received Solar Credits (multiplied STCs)<br />
                                            for a small generation unit at this<br />
                                            premises/address?</td>
                                        <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblCRED" runat="server" Text="CRED"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Rated Power<br />
                                            Output (kw):</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblSystemCap" runat="server"></asp:Label></td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Inverter Model:</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;" colspan="2">
                                            <asp:Label ID="lblInverterModel" runat="server"></asp:Label></td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;" colspan="2">Is this an ELIGIBLE premises for Solar Credits (STC<br />
                                            Multiplier)?</td>
                                        <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblELIG" Text="ELIG" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">No. of Panels:</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblNumberPanels" runat="server"></asp:Label></td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6" rowspan="2">Grid Connection</td>
                                        <td colspan="2" rowspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="23" valign="top">
                                                        <input name="" type="checkbox" value="" checked="checked"></td>
                                                    <td>Connected to an electricity grid<br />
                                                        without battery storage</td>
                                                </tr>
                                                <tr>
                                                    <td width="23" valign="top">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Connected to an electricity grid<br />
                                                        with battery storage</td>
                                                </tr>
                                                <tr>
                                                    <td width="23" valign="top">
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Stand-alone(not connected to an<br />
                                                        electricity</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Is there more than one Solar PV installation at this<br />
                                            address?</td>
                                        <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblMOR" Text="MOR" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background-color: #d6d6d6">Installation date:</td>
                                        <td valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblInstallBookingDate" runat="server"></asp:Label></td>
                                        <td colspan="2" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Do you have all of the required compliance paper<br />
                                            work?</td>
                                        <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <asp:Label ID="lblPaperwork" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-left: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>Additional comments:&nbsp;&nbsp;</td>
                                                    <td>
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>No&nbsp;&nbsp;</td>
                                                    <td>
                                                        <input name="" type="checkbox" value=""></td>
                                                    <td>Yes&nbsp;&nbsp;&nbsp;</td>
                                                    <td>If Yes Provide Details:</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; background: #d6d6d6; font-size: 12px;"><strong>CEC INSTALLER SECTION: (Here we need to know the installer, the designer and
              the electricians details)</strong></td>
                        </tr>
                        <tr>
                            <td rowspan="2" align="left" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Installer:</strong></td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerName1" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td align="left" style="font-size: 11px; width: 220px; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContMobile1" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerAddress1" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 0px 2px;">
                                <asp:Label ID="lblAccreditation1" runat="server"></asp:Label>
                                &nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Full Name </td>
                            <td align="left" style="font-size: 11px; width: 220px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Phone </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Address </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Accreditation No. </td>
                        </tr>
                        <tr>
                            <td rowspan="2" align="left" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Designer:</strong></td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerName2" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td align="left" style="font-size: 11px; width: 220px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContMobile2" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerAddress2" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblAccreditation2" runat="server"></asp:Label>
                                &nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Full Name </td>
                            <td align="left" style="font-size: 11px; width: 220px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Phone </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Address </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Accreditation No. </td>
                        </tr>
                        <tr>
                            <td rowspan="2" align="left" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Electrician:</strong></td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerName3" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td align="left" style="font-size: 11px; width: 220px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblContMobile3" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerAddress3" runat="server"></asp:Label>
                                &nbsp; </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblAccreditation3" runat="server"></asp:Label>
                                &nbsp; </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Full Name </td>
                            <td align="left" style="font-size: 11px; width: 220px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Phone </td>
                            <td colspan="4" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Address </td>
                            <td colspan="3" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Accreditation No. </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 4px 5px; background: #d6d6d6; font-size: 12px;"><strong>MANDATORY WRITTEN STATEMENT BY THE CEC INSTALLER AND DESIGNER</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 11px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">I <span style="font-size: 11px; text-decoration: underline;">
                                <asp:Label ID="lblInstallerName6" runat="server"></asp:Label>
                            </span>was the accredited CEC installer that completed the SGU installation at <span
                                style="font-size: 11px; text-decoration: underline;">
                                <asp:Label ID="lblInstallAddressCity2" runat="server"></asp:Label>
                            </span>and verify that I have installed the system and that it meets the following
              requirements; The CEC accreditation guidelines, the CEC Accreditation Code of Practice
              and am bound by their Code of Conduct; Have used panels and inverters approved by
              the CEC; Followed all of the Office of the Clean Energy Regulator's Guidelines;
              Have
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" />5M in Public Liability Insurance; and the system meets
              the following Australian Standard, where applicable... </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>PV &amp; Inverter Standards</strong><br />
                                AS/NZS 5033:2005, Installation of photovoltaic (PV) arrays;<br />
                                AS/NZS 1170:2002, Structural design actions, Part 2: Wind actions (PV Array).<br />
                                AS/NZS 5033. PV modules are compliant and the product is listed at www.cleanenergycouncil.org.au.<br />
                                The grid connected inverter used has been tested to Standard AS 4777 and the product
              is listed at www.cleanenergycouncil.org.au </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Grid connected system</strong><br />
                                AS/NZS 3000:2007, Wiring Rules.<br />
                                AS 4777, this installation complies to this standard<br />
                                AS/NZS1768:2007, Lightning Protection.<br />
                                AS 4777—2005, Grid connection of energy systems via inverters </td>
                            <td colspan="6" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Standalone systems</strong><br />
                                AS/NZS 4509:2009, Standalone Power systems, Part1: Safety &amp; installation.<br />
                                AS 4086.2:1997, Secondary batteries for use with standalone power systems, Part2:
              installation &amp; maintenance, wind system.<br />
                                AS/NZS 3000:2007, Wiring Rules </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">I verify that local, state or Territory government requirements have been met for
              1) The sitting for the unit 2) The attachment of the unit to the Building or structure
              3) The grid connections of the system for the SGU installation. I verify that the
              SGU is a <b>Grid-Connected</b> Installation and an electrical worker holding an
              unrestricted license for electrical work issued by the State or Territory authority
              for the place where the unit was installed undertook all wiring of the unit that
              involves alternating current of 50 or more volts or direct current of 120. I confirm
              that the details in the above statement are correct. </td>
                        </tr>
                        <tr>
                            <td align="left" width="94" valign="top" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; font-size: 12px; background: #d6d6d6;"><strong>CEC NUMBER:</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblAccreditation4" runat="server"></asp:Label></td>
                            <td rowspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">&nbsp;</td>
                            <td colspan="2" align="left" valign="top" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; background: #d6d6d6; font-size: 12px;"><strong>CEC NUMBER:</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblAccreditation5" runat="server"></asp:Label></td>
                            <td colspan="2" rowspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Signature of the SGUs CEC installer </td>
                            <td colspan="5" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Signature of the SGUs CEC Designer </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">PRINT NAME </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerName4" runat="server"></asp:Label></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">PRINT NAME </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblInstallerName5" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #fff; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Date:</strong></td>
                            <td colspan="5" align="right" valign="top" style="font-size: 11px; border-right: solid 1px #fff; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Date</strong>: </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 4px 5px; background: #d6d6d6; font-size: 12px;"><strong>MANDATORY DECLARATION</strong></td>
                        </tr>
                        <tr>
                            <td colspan="4" rowspan="9" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <ul style="font-size: 11px; margin-left: -15px;">
                                    <li style="font-size: 11px;">I am the legal owner of the above small generation unit
                  (SGU) and assign the right to create STCs to **P And N Pty Ltd** T/A THINK GREEN SOLAR
                  for the period stated above, commencing at the date of installation
                  <asp:Label ID="lblInstallBookingDate2" runat="server"></asp:Label>
                                        .</li>
                                    <li style="font-size: 10px;">I have not previously assigned or created any STCs for
                  this system within this period.</li>
                                    <li style="font-size: 10px;">To claim 15 years deeming for SGU, STCs must be registered
                  within 12 months of installation.</li>
                                    <li style="font-size: 10px;">I understand I am under no obligation to assign STCs to
                  **P And N Pty Ltd **T/A THINK GREEN SOLAR</li>
                                    <li style="font-size: 10px;">I agree to repay the STC payment to **P And N Pty Ltd**
                  T/A THINK GREEN SOLAR should my assignment be invalid.</li>
                                    <li style="font-size: 10px;">I understand that an agent of the Office of the Clean Energy
                  Regulator or **P And N Pty Ltd** T/A THINK GREEN SOLAR may wish to inspect the SGU within
                  the first years of certificate redemption.</li>
                                    <li style="font-size: 10px;">I must retain receipts and proof of the installation date
                  for the life of the STCs.</li>
                                    <li style="font-size: 10px;">I am aware that penalties can be applied for providing
                  misleading information in this form under the Renewable Energy (Electricity) Act
                  2000.</li>
                                </ul>
                                I further declare that the accredited CEC installer named on this form physically
              attended the installation of the unit </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; font-size: 11px; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 40%">I understand that this system is eligible for </td>
                            <td align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 5%">
                                <asp:Label ID="lblSTCNumber" runat="server"></asp:Label></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">STCs and in exchange </td>
                        </tr>
                        <tr>
                            <td colspan="8" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">my right to create these STCs, I will receive from the installer/supplier; </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 40%;">Total Costs for Installation </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />
                                <%--<asp:Label ID="lblTotalCost" runat="server"></asp:Label>--%></td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 40%">Connection Cost
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />
                                ______ , Any Other </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Costs
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />
                                ___ Deduct value of STCs, any rebates or benefits </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />
                                <%--<asp:Label ID="lblRECR" runat="server"></asp:Label>--%></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Out of Pocket Expense </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />
                                <%--<asp:Label--% ID="lblNetValue" runat="server"></asp:Label--%>></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <br />
                                <br />
                            </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <br />
                                <br />
                            </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 30%;"><strong>Owner Signature</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 30%;"><strong>Electrician Signature</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; width: 30%;"><strong>Witness Signature</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;"><strong>Date:</strong></td>
                            <td colspan="5" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="8" align="left" valign="top" style="font-size: 10px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">PRIVACY DECLARATION: **P And N Pty Ltd** T/A THINK GREEN SOLAR will only use this personal
              information as intended and will not sell or divulge this any third parties other
              than the Office of the Clean Energy Regulator. </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" bgcolor="#d6d6d6" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 4px 5px; font-size: 10px;"><strong>CUSTOMER GST DECLARATION</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="2" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Is this SGU Used for Commercial or Domestic use? </td>
                            <td rowspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblResCom" runat="server"></asp:Label></td>
                            <td rowspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">Is the owner of the SGU registered for GST? </td>
                            <td rowspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">
                                <asp:Label ID="lblGstReg" runat="server"></asp:Label></td>
                            <td colspan="3" rowspan="2" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">If so, please provide full Business Name and ABN </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">N/A </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">N/A </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 7px; border-collapse: collapse; padding: 2px;">STC Paper Approved on 28 August 2014. </td>
                        </tr>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;"> infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;"></h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="center" valign="top" bgcolor="#d6d6d6" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px; padding-left: 150px;"><strong>Customer Acknowledgement</strong></td>
                            <td colspan="5" align="center" style="border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 5px 8px 5px 10px; background: #d6d6d6;"><strong>Customer No :
              <asp:Label ID="lblcustno2" runat="server"></asp:Label>
                            </strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px; line-height: 20px;"><strong>I,
              <asp:Label ID="lblContact" runat="server"></asp:Label>
                                <br />
                                Of,
              <asp:Label ID="lblInstallAddressComplete" runat="server"></asp:Label>
                            </strong>
                                <br />
                                As the &quot;<strong>Customer</strong>&quot; I do hereby acknowledge that I have:
              <ul style="font-size: 11px; margin-left: -15px;">
                  <li>Received a copy of my invoice and User Manuals.</li>
                  <li>Been made aware by the installer of the best location for my solar panels and inverters
                  to be installed and have agreed with the location of the installation.</li>
                  <li>Satisfied with the condition that installer has left the premises after completion
                  of the work.</li>
              </ul>
                                <strong>I also confirm that:</strong>
                                <ul style="font-size: 11px; margin-left: -15px;">
                                    <li>I am completely satisfied with the Solar Power system I have been provided with,
                  including (but not limited to) the installation, positioning and location of the
                  solar panels and inverters.</li>
                                    <li>I am aware of and understand that once the system has been installed, it will inspected
                  and signed for by an independent inspector, and both the Certificate of Electrical
                  Safety and Electrical Work Request (EWR) will be sent to my electricity company
                  for processing of the Smart Meter Installation or reconfiguration (if I already
                  have a smart meter at my premises). </li>
                                    <li>I am aware that any damage caused by turning the system on before the inspection
                  has been completed is not covered by the warranty.</li>
                                    <li>I am aware that any claims for alleged damages caused by the installation of my
                  system are to be made within Five (5) business days from the date of installation.</li>
                                    <li>I received the Signage Board from the Installer and Happy to Display Outside my
                  House.</li>
                                    <li>I agree to publish my feedback for review purpose.</li>
                                    <li><strong>If I am a Certegy Ezi Pay customer I, the undersigned state that:</strong>
                                        <ul style="font-size: 11px; margin-left: -15px;">
                                            <li>I am the owner of the property or dwelling to which the goods are being installed</li>
                                            <li>I understand it is not the responsibility of the merchant to connect my solar power
                      unit/s to the electricity grid and I must commence repayments to Certegy upon completion
                      of the installation of the solar power unit/s by the merchant.</li>
                                            <li>The construction of services provided to my property or dwelling from the above
                      company merchant: has been completed to my satisfaction.</li>
                                            <li>I am happy to commence my repayments to Certegy.</li>
                                        </ul>
                                    </li>
                                </ul>
                                <br />
                                Job satisfactorily completed and installed on (date) _________________ </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="center" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 2px;">QUALITY OF SERVICE (1=POOR, 2=AVERAGE, 3=GOOD, 4=VERY GOOD) </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Overall Impression </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">1 </td>
                            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">2 </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">3 </td>
                            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">4 </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Services / Technical Support </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">1 </td>
                            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">2 </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">3 </td>
                            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">4 </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Installation </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">1 </td>
                            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">2 </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">3 </td>
                            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">4 </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Your Experience </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">1 </td>
                            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">2 </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">3 </td>
                            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">4 </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Personal View : </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">1 </td>
                            <td colspan="2" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">2 </td>
                            <td align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">3 </td>
                            <td colspan="3" align="center" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">4 </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" bgcolor="#d6d6d6" valign="top" style="border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Installation Sketch</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; height: 300px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Signed (&quot;The Customer&quot;, or authorised agent): </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 30px;">Name_____________________ </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 30px;">Signature_______________________ </td>
                            <td colspan="6" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 30px;">Date:____________________ </td>
                        </tr>
                        <!--<tr>
                            <td colspan="10" align="left" valign="top" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; height: 100px;">&nbsp;</td>
                        </tr>-->
                        <!-- <tr>
                            <td colspan="10" align="left" bgcolor="#d6d6d6" valign="top" style="font-size: 11px; margin-top: 20px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; height: 2px;"></td>
                        </tr>-->
                        <%-- <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Queensland</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Victoria</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Tasmania</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>ABN: 95 130 845 199</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108 </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;">126/45 Gilby Road,<br />
                                Mount Waverley, VIC, 3141 </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108</td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px; line-height: 20px; background: url(../images/hmico.jpg) no-repeat right bottom;">Phone : 1300 387676<br />
                                Email : <a href="mailto:infoqld@eurosolar.com.au" style="font-size: 11px; text-decoration: none; color: #016285;">infoqld@eurosolar.com.au</a><br />
                                Web : <a href="http://www.eurosolar.com.au" target="_blank" style="font-size: 11px; text-decoration: none; color: #016285;">www.eurosolar.com.au</a></td>
                        </tr>--%>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;"></h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" bgcolor="#d6d6d6" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>PANEL SERIAL NUMBERS</strong></td>
                            <td colspan="2" align="left" valign="top" bgcolor="#d6d6d6" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>
                                <asp:Literal ID="ProjectNumber3" runat="server"></asp:Literal>
                            </strong></td>
                            <td colspan="7" align="left" bgcolor="#d6d6d6" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>INVERTER SERIAL AND OTHER DETAILS</strong></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <asp:Literal ID="litPanelDetails" runat="server"></asp:Literal></td>
                            <td colspan="7" align="left" valign="top" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">1 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Inverter Sr. 01 - </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">2 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Inverter Sr. 02 - </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">3 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Inverter Sr. 03 - </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">4 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Inverter Brand - </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">5 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Inverter Series - </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">6 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">**** Inverter Brand/Model/Series - As Per Page 1 **** </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">7 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">
                                <asp:Label ID="lblInverterBrand2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">8 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">
                                <asp:Label ID="lblInverterModel2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">9 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">
                                <asp:Label ID="lblInverterSeries2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">10 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">11 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">METER BOX DETAILS </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">12 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Meter Phase:
              <asp:Label ID="lblMeterPhase" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">13 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Off Peak load :
              <asp:Label ID="lblOffPeak" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">14 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Retailer :
              <asp:Label ID="lblRetailer" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">15 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Meter type : Basic Type 6 or other
              <asp:Label ID="lblMeterType" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">16 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">if other : _____________________ </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">17 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Peak Meter No:
              <asp:Label ID="lblPeakMeterNo" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">18 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">19 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">20 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">21 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">22 - </td>
                            <td colspan="7" align="left" valign="top" bgcolor="#d6d6d6" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;"><strong>Address as per council rate notice</strong></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">23 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Address confirmation </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">24 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">25 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">Please enter the property address if it's different in the Council rate notice than
              the above address. </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">26 - </td>
                            <td colspan="7" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 0px #000; border-collapse: collapse; padding: 8px; padding-top: 12px; padding-bottom: 12px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;"></h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 0px #000; border-collapse: collapse; padding: 8px; line-height: 20px;"><strong style="font-size: 16px;">Payment Form</strong><br />
                                Project:
              <asp:Label ID="lblProjectNumber2" runat="server"></asp:Label>
                                <br />
                                Customer:
              <asp:Label ID="lblContact2" runat="server"></asp:Label>
                                <br />
                                Installation at:
              <asp:Label ID="lblInstallAddressComplete2" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>By Cheque</strong></td>
                            <td colspan="9" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="font-size: 13px; border-left: solid 1px #000; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Cheque No: ____________ </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Amount:
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />____________ </td>
                            <td colspan="4" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Bank: __________________ </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Please make cheques payable to &quot;Think Green Solar&quot; and attach to this form. </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Credit Card Authorisation</strong></td>
                            <td colspan="9" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Please complete the details below to authorize payment from your credit card to
              THINK GREEN SOLAR for the Solar panel Installation. </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Mater Card:</strong></td>
                            <td align="center" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_checkbox2.jpg") %>"
                                    width="14" height="14" style="font-size: 13px; padding-right: 5px; margin-bottom: -5px;" /></td>
                            <td align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Visa:</strong></td>
                            <td align="center" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_checkbox2.jpg") %>"
                                    width="14" height="14" style="font-size: 13px; padding-right: 5px; margin-bottom: -5px;" /></td>
                            <td colspan="8" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Please note: these are the only credit cards that may be used. </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Card Holder's Name (as on card): </td>
                            <td colspan="8" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">_________________________________ </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Card Number: </td>
                            <td colspan="8" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">__ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Card Expiry Date: </td>
                            <td colspan="8" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">__ __ / __ __ </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Amount Authorised: </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />_________________ </td>
                            <td colspan="5" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Customer Sign:__________________ </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Validation Digits (on reverse side of card) </td>
                            <td colspan="8" align="left" valign="top" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">__ __ __ </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" valign="top" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">(Please note that a 1.5% credit card surcharge will be applied to the authorised
              amount.) </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>OFFICE USE ONLY:</strong></td>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Signature of Processing Officer</strong></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">I warrant that the card holder, as stated above, did authorise the credit card payment
              in the amount stated above. </td>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px 0px;">__________________________________________ </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Date Processed:</strong> __ __ / __ __ / __ __ __ __ </td>
                            <td colspan="6" align="left" bgcolor="#d6d6d6" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Transaction Approved: Yes / No</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="center" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Once completed, please return to our office.</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>By Bank Transfer</strong></td>
                            <td colspan="9" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Receipt No: ____________ </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Amount:
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />____________ </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Date: __________________ </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">A/c Name: Euro Solar | BSB: 034037 A/c No. 250324 </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Please show the Invoice / Quote Number in the bank transaction description. </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" bgcolor="#d6d6d6" style="border-left: solid 1px #000; border-top: solid 1px #000; font-size: 13px; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>By Cash</strong></td>
                            <td colspan="9" align="left" style="font-size: 13px; border-left: solid 1px #000; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-left: solid 1px #000; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Amount:
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>"
                                    width="5" height="10" algn="absmiddle" />____________ </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Date: __________________ </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" style="border-left: solid 1px #000; border-bottom: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;"><strong>Received By: __________________________________________</strong></td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <%--<tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Queensland</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Victoria</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>Tasmania</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;"><strong>ABN: 95 130 845 199</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108 </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px;">126/45 Gilby Road,<br />
                                Mount Waverley, VIC, 3141 </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108</td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; padding: 8px; line-height: 20px; background: url(../images/hmico.jpg) no-repeat right bottom;">Phone : 1300 387676<br />
                                Email : <a href="mailto:infoqld@eurosolar.com.au" style="font-size: 11px; text-decoration: none; color: #016285;">infoqld@eurosolar.com.au</a><br />
                                Web : <a href="http://www.eurosolar.com.au" target="_blank" style="font-size: 11px; text-decoration: none; color: #016285;">www.eurosolar.com.au</a></td>
                        </tr>--%>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;"></h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="10" align="left" style="font-size: 11px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <div>
                                    <div style="font-size: 11px; float: left; padding-top: 15px;">
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/eurosolarlogo.png") %>" width="170" height="126" />
                                    </div>
                                    <div style="font-size: 11px; float: right; text-align: right; padding: 8px; width: 250px;">
                                        <table width="79%" height="33" border="0" cellpadding="0" cellspacing="0" style="font-size: 11px; border-left: solid 1px #000; border-top: solid 1px #000; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="2" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Job No </td>
                                                <td colspan="2" align="left" style="font-size: 11px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                                    <asp:Label ID="lblProjectNumber5" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>--%>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <div>
                                    <div style="font-size: 13px; float: right; text-align: right; padding: 8px; width: 250px;">
                                        <table width="79%" height="33" border="0" cellpadding="0" cellspacing="0" style="font-size: 13px; border-left: solid 1px #000; border-top: solid 1px #000; border-collapse: collapse;">
                                            <tr>
                                                <td colspan="2" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">Job No </td>
                                                <td colspan="2" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                                    <asp:Label ID="lblProjectNumber5" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Installer Work Paper</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>THINK GREEN SOLAR</strong></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>Installation Department</strong></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" style="border-left: solid 1px #000; font-size: 13px; border-collapse: collapse; padding: 8px;"><strong>Job No</strong></td>
                            <td colspan="8" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <asp:Label ID="lblProjectNumber4" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" style="border-left: solid 1px #000; font-size: 13px; padding: 8px;"><strong>Installer Name</strong></td>
                            <td colspan="8" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <asp:Label ID="lblInstallerName7" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" style="border-left: solid 1px #000; font-size: 13px; padding: 8px;"><strong>Installer Sign</strong></td>
                            <td colspan="8" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;"></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" style="border-left: solid 1px #000; font-size: 13px; padding: 8px;"><strong>Email</strong></td>
                            <td colspan="8" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border-left: solid 1px #000; font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="8" align="center" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;"><strong>PLEASE TICK OFF EACH BOX</strong></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;">STC Form Sign </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Panels Serial Numbers </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Quotation Form </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Customer Acknowledgement </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">Compliances Certificate </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;">Customer Acceptance </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 15px; height: 15px; border: 1px solid #000;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;">Finance Option </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">
                                <asp:Label ID="lblFinanceOption" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px; border-collapse: collapse; padding: 8px;"><strong>EWR Number</strong></td>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px; border-collapse: collapse; padding: 8px;"><strong>Payment Method</strong></td>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">Note : Installer must provide the above-mentioned documents </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="right" style="border-bottom: solid 1px #000; border-left: solid 1px #000; font-size: 13px; border-right: solid 1px #000; border-collapse: collapse; padding: 8px;">Copyright : Think Green Solar </td>
                        </tr>
                        <%--<tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;"><strong>Queensland</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;"><strong>Victoria</strong></td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;"><strong>Tasmania</strong></td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;"><strong>ABN: 95 130 845 199</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108 </td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;">126/45 Gilby Road,<br />
                                Mount Waverley, VIC, 3141 </td>
                            <td colspan="3" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px;">P O BOX 570,<br />
                                Archerfield, QLD, 4108</td>
                            <td colspan="2" align="left" valign="top" style="font-size: 11px; border-collapse: collapse; padding: 8px; line-height: 20px; background: url(../images/hmico.jpg) no-repeat right bottom;">Phone : 1300 387676<br />
                                Email : <a href="mailto:infoqld@eurosolar.com.au" style="font-size: 11px; text-decoration: none; color: #016285;">infoqld@eurosolar.com.au</a><br />
                                Web : <a href="http://www.eurosolar.com.au" target="_blank" style="font-size: 11px; text-decoration: none; color: #016285;">www.eurosolar.com.au</a></td>
                        </tr>--%>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;"> infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;">
                                                <br />
                                            </h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="2" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <p><strong>CERTIFICATE OF </strong></p>
                                <p>(Please mark relevant check-box) </p>
                            </td>
                            <td align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 25px; height: 25px; border: 2px solid #000;"></div>
                            </td>
                            <td colspan="5" align="left" style="font-size: 13px; border-right: solid 1px #fff; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <p><strong>TESTING AND COMPLIANCE</strong> </p>
                                <p>Issued in accordance with s227 of the Electrical Safety Regulation 2013 </p>
                            </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-top: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">[ Electrical installation ] </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="font-size: 13px; display: block; width: 25px; height: 25px; border: 2px solid #000;"></div>
                            </td>
                            <td colspan="5" align="left" style="font-size: 13px; border-right: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <p><strong>TESTING AND SAFETY</strong> </p>
                                <p>Issued in accordance with s26 of the Electrical Safety Regulation 2013 </p>
                            </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">[ Electrical Equipment ] </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">* Work performed for: </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">*Name </td>
                            <td colspan="2" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litTitle" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Title </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litFirst" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Given name/s </td>
                            <td colspan="4" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litLast" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Surname </td>
                        </tr>
                        <%--<tr>
                        <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px;
                            border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse;
                            padding: 8px;">
                            &nbsp;
                        </td>
                        <td colspan="2" align="left" style="font-size: 13px; border-right: solid 1px #fff;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            
                        </td>
                        <td colspan="3" align="left" style="font-size: 13px; border-right: solid 1px #fff;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            
                        </td>
                        <td colspan="2" align="left" style="font-size: 13px; border-right: solid 1px #000;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">*Address </td>
                            <td colspan="9" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litStreet" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Street </td>
                        </tr>
                        <%-- <tr>
                        <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px;
                            border-right: solid 1px #000; border-bottom: solid 1px #fff; border-collapse: collapse;
                            padding: 8px;">
                            &nbsp;
                        </td>
                        <td colspan="7" align="left" style="font-size: 13px; border-right: solid 1px #000;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            Street
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="3" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litSuburb" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Suburb/Town </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-bottom: solid 1px #fff; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litPostcode" runat="server"></asp:Literal>
                                </div>
                                <br />
                                Postcode </td>
                        </tr>
                        <%-- <tr>
                        <td colspan="3" align="left" style="border-left: solid 1px #000; font-size: 13px;
                            border-right: solid 1px #000; border-bottom: solid 1px #000; border-collapse: collapse;
                            padding: 8px;">
                            &nbsp;
                        </td>
                        <td colspan="3" align="left" style="font-size: 13px; border-right: solid 1px #000;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            
                        </td>
                        <td colspan="4" align="left" style="font-size: 13px; border-right: solid 1px #000;
                            border-bottom: solid 1px #000; border-collapse: collapse; padding: 8px;">
                            
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;"><strong>* Electrical installation/equipment tested</strong> (please include site
              address for electrical installation work if different from above): </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-right: solid 0px #000; border-collapse: collapse; padding: 8px; height: 200px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" width="94" style="font-size: 13px; border-collapse: collapse; padding: 8px;">* Date of test </td>
                            <td colspan="4" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">__ __ / __ __ / __ __ __ __ </td>
                            <td colspan="4" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">* Electrical contractor licence number </td>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litElectricallicence" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">Name on contractor licence </td>
                            <td colspan="9" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litcontractorlicence" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">Electrical contractor phone number </td>
                            <td colspan="9" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <div style="border-bottom: solid 1px #000; display: inline-block;">
                                    <asp:Literal ID="litcontractorphone" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="border-collapse: collapse; padding: 8px;">For <strong>electrical installations</strong>, this certifies that the electrical
              installation, to the extent it is affected by the electrical work, has been tested
              to ensure that it is electrically safe and is in accordance with the requirements
              of the wiring rules and any other standard applying under the Electrical Safety
              Regulation 2013 to the electrical installation. </td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">For <strong>electrical equipment</strong>, this certifies that the electrical equipment,
              to the extent it is affected by the electrical work, is electrically safe. </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">Name </td>
                            <td colspan="11" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">
                                <asp:Literal ID="litContractorName" runat="server"></asp:Literal>
                                <br />
                                <div style="border-top: solid 1px #000; display: inline-block;">Person who performed, or person who is responsible for, the electrical work </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="11" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px; padding-top: 30px;"><strong>Signature </strong>_______________________________________ </td>
                            <td colspan="6" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px; padding-top: 30px;"><strong>Date </strong>____/____/_____ </td>
                        </tr>
                        <tr>
                            <td colspan="9" align="left" style="font-size: 13px; border-collapse: collapse; padding: 8px;">* Indicates a mandatory field </td>
                            <td colspan="3" align="right" style="font-size: 13px; border-collapse: collapse; padding: 8px;">V4.12-201 </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 0px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="3" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 0px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                            <td colspan="6" align="left" style="font-size: 13px; border-right: solid 0px #000; border-bottom: solid 0px #000; border-collapse: collapse; padding: 8px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="clear: both; page-break-before: always;"></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <div style="font-size: 5px; width: 960px; margin: 0px auto;">
                                    <div style="border: #000 solid 1px;">
                                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 200px 0px 10px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                                width="124" height="100" />
                                        </div>
                                        <div style="float: right; text-align: left; padding: 8px; border-left: #000 solid 1px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 11px; margin-bottom: 8px; text-align: left;">** P And N Pty Ltd **<br />
                                                T/A THINK GREEN SOLAR</h2>
                                            <p style="font-family: 'OpenSansLight', sans-serif; font-size: 10px;">
                                                P O Box 570, Archerfield, QLD,4108<br />
                                                ABN: 95 130 845 199 Pho: 1300 38 76 76<br />
                                                Email : <a href="mailto:infoqld@thinkgreensolar.com.au" style="font-size: 9px; text-decoration: none; color: #016285;">infoqld@thinkgreensolar.com.au</a>
                                            </p>
                                        </div>
                                        <div style="float: left; text-align: center; padding: 8px;">
                                            <h2 style="font-family: 'OpenSansLight', sans-serif; font-size: 20px; margin-bottom: 5px; text-align: center;"></h2>
                                        </div>
                                        <div style="float: none; clear: both;"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="12">
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #333333;">
                                    <tr>
                                        <td style="padding: 20px 0px 10px 0px">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #000000; padding: 0px 0px 20px 0px; margin: 0px; text-align: center;"><strong>STOCK ALLOCATION RETURN FORM</strong></p>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>Installation:&nbsp
                          <asp:Label ID="lblProject" runat="server"></asp:Label>
                                                    </strong></td>
                                                    <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #000000;"><strong>
                                                        <asp:Label ID="lblInstallBookingDatePL" runat="server"></asp:Label>
                                                    </strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: #333333 solid 1px; padding: 10px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Name:&nbsp
                                <asp:Label ID="lblInstaller" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Installer Mobile:&nbsp
                                <asp:Label ID="lblInstallerMobile" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Customer Name:&nbsp
                                <asp:Label ID="lblCustomerPL" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Phone:&nbsp
                                <asp:Label ID="lblCustPhonePL" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Mob:&nbsp
                                <asp:Label ID="lblCustMobile" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>House Type:&nbsp
                                <asp:Label ID="lblHouseType" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Roof Type:&nbsp
                                <asp:Label ID="lblRoofType" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Angle:&nbsp
                                <asp:Label ID="lblRoofAngle" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px 0px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Manual Quote:&nbsp
                                <asp:Label ID="lblManualQuoteNumber" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="170" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000;"><strong>Project No.:&nbsp
                                <asp:Label ID="lblProjectNumber1PL" runat="server"></asp:Label>
                                                                </strong></td>
                                                                <td width="160" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000000;"><strong>*&nbsp
                                <asp:Label ID="lblProjectNumber2PL" runat="server"></asp:Label>
                                                                    *</strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding: 8px 0px 0px 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;"><strong>Store:&nbsp
                                <asp:Label ID="lblStockAllocationStore" runat="server"></asp:Label>
                                                                </strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="padding: 7px 0px 20px 0px;" height="534px">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="30" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Qty</strong></td>
                                                    <td width="130" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Item</strong></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Model</strong></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Description</strong></td>
                                                    <td width="50" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; border-top: #333333 solid 3px; border-bottom: #333333 solid 3px; padding: 5px;"><strong>Return Item</strong></td>
                                                </tr>
                                                <tr id="trPanel" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblPanelBrandName" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblPanDesc" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC" align="center"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                                <tr id="trInv1" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">1</td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvName1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvModel1" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;">
                                                        <asp:Literal ID="lblInvDesc1" runat="server"></asp:Literal></td>
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px;"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                                <tr id="trInv2" runat="server">
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">1</td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvName2" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvModel2" runat="server"></asp:Literal></td>
                                                    <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC">
                                                        <asp:Literal ID="lblInvDesc2" runat="server"></asp:Literal></td>
                                                    <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px; background: #ECECEC"><span style="display: inline-block; border: #000000 solid 2px; width: 10px; height: 10px;"></span></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: #000000 solid 3px;">
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000; padding: 10px 0px 10px 0px; margin: 0px;">
                                                <strong>Installer Notes:&nbsp;
                      <asp:Label ID="lblInstallerNotes" runat="server"></asp:Label>
                                                </strong>
                                            </p>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="93"><strong>PICKED By:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                    <td width="20px">&nbsp;</td>
                                                    <td width="50px"><strong>Date:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" height="10px"></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Collected By:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                    <td width="20px">&nbsp;</td>
                                                    <td><strong>Date:</strong></td>
                                                    <td style="border-bottom: #000000 dashed 1px;">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 20px 0px 10px 0px; margin: 0px; text-align: center;">
                                                <strong>Printed:&nbsp;
                      <asp:Label ID="lblPrinted" runat="server"></asp:Label>
                                                </strong>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%>
                    </table>
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
