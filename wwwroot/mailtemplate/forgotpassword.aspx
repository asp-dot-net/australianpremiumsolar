﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="mailtemplate_OfferMail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Mail Template</title>
    <style>
        body
        {
            margin: 0px 0px 0px 0px;
            padding: 0px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            line-height: 18px;
            color: #727076;
        }

        .tbldata tr td
        {
            height: 40px;
        }
    </style>
    <script type="text/javascript">
        WebFontConfig = {
            google: { families: ['Open+Sans::latin'] }
        };
        (function () {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
              '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <form id="form1" runat="server">
        <div class="brdemail">
            <table class="brdemail" width="100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" style="width: 600px; font-size: 14px; color: #727076; font-family: Arial, Helvetica, sans-serif; border: 1px solid #727076;">
                            <tr>
                                <td style="text-align: center; display: block;">
                                    <img id="imgtop" src="<%=Siteurl %>images/mail_top.jpg" class="mail_top" width="100%" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="mytable" class="tbldata" style="width: 550px; margin: 0px auto;" align="center">
                                        <tr>
                                            <td colspan="2" style=" font-size: 12px; color: #030303;">
                                                Dear  <%= Request["fullname"] %>,<br>
                                                Your Password is <%= Request["password"] %>.
                                            </td>
                                        </tr>
                                       
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 550px; margin: 0px auto;" align="center">
                                        <tr>
                                            <td style="background: #d6d3d3; color: #000000; width: 600px; text-align: center; border-top: 1px solid #727076; display: block; padding: 15px 0px;"><span>Copyrights © Achievers Energy <%=DateTime.Now.Year %>
                                            </span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
